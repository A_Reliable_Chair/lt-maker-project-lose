from typing import List, Tuple
import logging
from app.constants import WINHEIGHT, WINWIDTH
from app.data.database.database import DB
from app.data.resources.resources import RESOURCES
from app.engine import (background, combat_calcs, engine, equations, gui,
                        help_menu, icons, image_mods, item_funcs, item_system, 
                        skill_system, text_funcs, unit_funcs)
from app.engine.fluid_scroll import FluidScroll
from app.engine.game_menus import menu_options
from app.engine.game_menus.icon_options import BasicItemOption, ItemOptionModes
from app.engine.game_state import game
from app.engine.graphics.ingame_ui.build_groove import build_groove
from app.engine.graphics.text.text_renderer import render_text, text_width
from app.engine.info_menu.info_graph import InfoGraph, info_states
from app.engine.input_manager import get_input_manager
from app.engine.objects.unit import UnitObject
from app.engine.sound import get_sound_thread
from app.engine.sprites import SPRITES
from app.engine.state import State
from app.utilities import utils
from app.utilities.enums import HAlignment


class InfoMenuState(State):
    name = 'info_menu'
    in_level = False
    show_map = False

    def create_background(self):
        self.unit: UnitObject = game.memory.get('current_unit')
        if self.unit.team == 'player':
            panorama = RESOURCES.panoramas.get('info_menu_background_player')
        elif self.unit.team == 'enemy':
            panorama = RESOURCES.panoramas.get('info_menu_background_enemy')
        elif self.unit.team == 'enemy2':
            panorama = RESOURCES.panoramas.get('info_menu_background_enemy2')
        elif self.unit.team == 'other':
            panorama = RESOURCES.panoramas.get('info_menu_background_other')
        if not panorama:
            panorama = RESOURCES.panoramas.get('default_background')
        if panorama:
            self.bg = background.PanoramaBackground(panorama)
        else:
            self.bg = None

    def start(self):
        self.mouse_indicator = gui.MouseIndicator()
        self.create_background()

        # Unit to be displayed
        self.unit: UnitObject = game.memory.get('current_unit')
        self.scroll_units = game.memory.get('scroll_units')
        if self.scroll_units is None:
            self.scroll_units = [unit for unit in game.units if not unit.dead and unit.team == self.unit.team and unit.party == self.unit.party]
            if self.unit.position:
                self.scroll_units = [unit for unit in self.scroll_units if unit.position and game.board.in_vision(unit.position)]
        self.scroll_units = [unit for unit in self.scroll_units if 'Tile' not in unit.tags]
        game.memory['scroll_units'] = None

        self.state = game.memory.get('info_menu_state', info_states[0])
        if self.state == 'notes' and not (DB.constants.value('unit_notes') and self.unit.notes):
            self.state = 'personal_data'
        self.growth_flag = False

        self.fluid = FluidScroll(200, 1)


        self.logo = None

        self.info_graph = InfoGraph()
        self.info_flag = False
        self.info_graph.set_current_state(self.state)
        self.reset_surfs()

        # For transitions between states
        self.rescuer = None  # Keeps track of the rescuer if we are looking at the traveler
        self.next_unit = None
        self.next_state = None
        self.scroll_offset_x = 0
        self.scroll_offset_y = 0
        self.transition = None
        self.transition_counter = 0
        self.transparency = 0

        game.state.change('transition_in')
        return 'repeat'

    def reset_surfs(self):
        self.info_graph.clear()
        self.portrait_surf = None

        self.personal_data_surf: engine.Surface = None
        self.growths_surf: engine.Surface = None
        self.class_skill_surf: engine.Surface = None
        self.skill_surf: engine.Surface = None
        self.support_surf: engine.Surface = None
        self.wexp_surf: engine.Surface = None
        self.equipment_surf: engine.Surface = None
        self.fatigue_surf: engine.Surface = None
        self.notes_surf: engine.Surface = None




    def back(self):
        get_sound_thread().play_sfx('Select 4')
        game.memory['info_menu_state'] = self.state
        game.memory['current_unit'] = self.unit
        if self.unit.position and not game.is_roam():
            # Move camera to the new character unless it's a free roam, in which case we just stay on the free roamer
            game.cursor.set_pos(self.unit.position)
        game.state.change('transition_pop')

    def take_input(self, event):
        first_push = self.fluid.update()
        directions = self.fluid.get_directions()

        self.handle_mouse()
        if self.info_flag:
            if event == 'INFO' or event == 'BACK':
                get_sound_thread().play_sfx('Info Out')
                self.info_graph.set_transition_out()
                self.info_flag = False
                return

            if 'RIGHT' in directions:
                get_sound_thread().play_sfx('Select 6')
                self.info_graph.move_right()
            elif 'LEFT' in directions:
                get_sound_thread().play_sfx('Select 6')
                self.info_graph.move_left()
            elif 'UP' in directions:
                get_sound_thread().play_sfx('Select 6')
                self.info_graph.move_up()
            elif 'DOWN' in directions:
                get_sound_thread().play_sfx('Select 6')
                self.info_graph.move_down()

        elif not self.transition:  # Only takes input when not transitioning

            if event == 'INFO':
                get_sound_thread().play_sfx('Info In')
                self.info_graph.set_transition_in()
                self.info_flag = True
                return
            elif event == 'AUX':
                if self.state == 'personal_data' and self.unit.team == 'player' and DB.constants.value('growth_info'):
                    get_sound_thread().play_sfx('Select 3')
                    self.growth_flag = not self.growth_flag
                    if self.growth_flag:
                        self.info_graph.set_current_state('growths')
                    else:
                        self.info_graph.set_current_state('personal_data')
            elif event == 'BACK':
                if self.rescuer:
                    self.move_up()
                else:
                    self.back()
                    return
            elif event == 'SELECT':
                mouse_position = get_input_manager().get_mouse_position()
                if mouse_position:
                    mouse_x, mouse_y = mouse_position
                    if mouse_x <= 16:
                        self.move_left()
                    elif mouse_x >= WINWIDTH - 16:
                        self.move_right()
                    elif mouse_y <= 16:
                        self.move_up()
                    elif mouse_y >= WINHEIGHT - 16:
                        self.move_down()
                if not self.transition:  # Some of the above move commands could cause transition
                    if self.unit.traveler:
                        self.move_traveler()

            if 'RIGHT' in directions:
                self.move_right()
            elif 'LEFT' in directions:
                self.move_left()
            elif 'DOWN' in directions:
                self.move_down()
            elif 'UP' in directions:
                self.move_up()

    def move_left(self):
        if len(info_states) > 1:
            get_sound_thread().play_sfx('Status_Page_Change')
            index = info_states.index(self.state)
            new_index = (index - 1) % len(info_states)
            self.next_state = info_states[new_index]
            if self.next_state == 'notes' and not (DB.constants.value('unit_notes') and self.unit.notes):
                new_index = (new_index - 1) % len(info_states)
                self.next_state = info_states[new_index]
            self.transition = 'LEFT'



    def move_right(self):
        if len(info_states) > 1:
            get_sound_thread().play_sfx('Status_Page_Change')
            index = info_states.index(self.state)
            new_index = (index + 1) % len(info_states)
            self.next_state = info_states[new_index]
            if self.next_state == 'notes' and not (DB.constants.value('unit_notes') and self.unit.notes):
                new_index = (new_index + 1) % len(info_states)
                self.next_state = info_states[new_index]
            self.transition = 'RIGHT'



    def move_down(self):
        get_sound_thread().play_sfx('Status_Character')
        if self.rescuer:
            new_index = self.scroll_units.index(self.rescuer)
            self.rescuer = None
        elif len(self.scroll_units) > 1:
            index = self.scroll_units.index(self.unit)
            new_index = (index + 1) % len(self.scroll_units)    
        else:
            return
        self.next_unit = self.scroll_units[new_index]
        if self.state == 'notes' and not (DB.constants.value('unit_notes') and self.next_unit.notes):
            self.state = 'personal_data'
        self.transition = 'DOWN'

    def move_up(self):
        get_sound_thread().play_sfx('Status_Character')
        if self.rescuer:
            new_index = self.scroll_units.index(self.rescuer)
            self.rescuer = None
        elif len(self.scroll_units) > 1:
            index = self.scroll_units.index(self.unit)
            new_index = (index - 1) % len(self.scroll_units)
        else:
            return        
        self.next_unit = self.scroll_units[new_index]
        if self.state == 'notes' and not (DB.constants.value('unit_notes') and self.next_unit.notes):
            self.state = 'personal_data'
        self.transition = 'UP'

    def move_traveler(self):
        get_sound_thread().play_sfx('Status_Character')
        self.rescuer = self.unit
        self.next_unit = game.get_unit(self.unit.traveler)
        if self.state == 'notes' and not (DB.constants.value('unit_notes') and self.next_unit.notes):
            self.state = 'personal_data'
        self.transition = 'DOWN'

    def handle_mouse(self):
        mouse_position = get_input_manager().get_mouse_position()
        if not mouse_position:
            return
        if self.info_flag:
            self.info_graph.handle_mouse(mouse_position)

    def update(self):
        # Up and Down
        if self.next_unit:
            self.transition_counter += 1
            # Transition in
            if self.next_unit == self.unit:
                if self.transition_counter == 1:
                    self.transparency = .75
                    self.scroll_offset_y = -80 if self.transition == 'DOWN' else 80
                elif self.transition_counter == 2:
                    self.transparency = .6
                    self.scroll_offset_y = -32 if self.transition == 'DOWN' else 32
                elif self.transition_counter == 3:
                    self.transparency = .48
                    self.scroll_offset_y = -16 if self.transition == 'DOWN' else 16
                elif self.transition_counter == 4:
                    self.transparency = .15
                    self.scroll_offset_y = -4 if self.transition == 'DOWN' else 4
                elif self.transition_counter == 5:
                    self.scroll_offset_y = 0
                else:
                    self.transition = None
                    self.transparency = 0
                    self.next_unit = None
                    self.transition_counter = 0
            # Transition out
            else:
                if self.transition_counter == 1:
                    self.transparency = .15
                elif self.transition_counter == 2:
                    self.transparency = .48
                elif self.transition_counter == 3:
                    self.transparency = .6
                    self.scroll_offset_y = 8 if self.transition == 'DOWN' else -8
                elif self.transition_counter == 4:
                    self.transparency = .75
                    self.scroll_offset_y = 16 if self.transition == 'DOWN' else -16
                elif self.transition_counter < 8: # (5, 6, 7, 8):  # Pause for a bit
                    self.transparency = 1.
                    self.scroll_offset_y = 160 if self.transition == 'DOWN' else -160
                else:
                    self.unit = self.next_unit  # Now transition in
                    self.reset_surfs()
                    self.transition_counter = 0

        # Left and Right
        elif self.next_state is not None:
            self.transition_counter += 1
            # Transition in
            if self.next_state == self.state:
                idxs = (104, 72, 56, 40, 24, 8)
                counter = self.transition_counter - 1
                if 0 <= counter < len(idxs):
                    self.scroll_offset_x = idxs[counter] if self.transition == 'RIGHT' else -idxs[counter]
                else:
                    self.transition = None
                    self.scroll_offset_x = 0
                    self.next_state = None
                    self.transition_counter = 0
            else:
                idxs = (-32, -56, -80, -96, -112)
                counter = self.transition_counter - 1
                if 0 <= counter < len(idxs):
                    self.scroll_offset_x = idxs[counter] if self.transition == 'RIGHT' else -idxs[counter]
                else:
                    self.scroll_offset_x = -140 if self.transition == 'RIGHT' else 140
                    self.state = self.next_state
                    self.info_graph.set_current_state(self.state)
                    self.transition_counter = 0

    def draw(self, surf):
        if self.bg:
            self.bg.draw(surf)
        else:
            # info menu shouldn't be transparent
            surf.blit(SPRITES.get('bg_black'), (0, 0))

        # Image flashy thing at the top of the InfoMenu
        num_frames = 8
        # 8 frames long, 8 different frames
        blend_perc = abs(num_frames - ((engine.get_time()/134) % (num_frames * 2))) / float(num_frames)
        sprite = SPRITES.get('info_menu_flash')
        im = image_mods.make_translucent_blend(sprite, 128. * blend_perc)
        surf.blit(im, (98, 0), None, engine.BLEND_RGB_ADD)

        self.draw_portrait(surf)
        self.draw_slide(surf)

        if self.info_graph.current_bb:
            self.info_graph.draw(surf)

        if not self.transition:
            self.mouse_indicator.draw(surf)

        return surf

    def draw_portrait(self, surf):
        # Only create if we don't have one in memory
        if not self.portrait_surf:
            self.portrait_surf = self.create_portrait()

        # Stick it on the surface
        if self.transparency:
            im = image_mods.make_translucent(self.portrait_surf, self.transparency)
            surf.blit(im, (0, self.scroll_offset_y))
        else:
            surf.blit(self.portrait_surf, (0, self.scroll_offset_y))

        # Blit the unit's active/focus map sprite
        if not self.transparency:
            active_sprite = self.unit.sprite.create_image('active')
            x_pos = 87 - active_sprite.get_width()//2
            y_pos = WINHEIGHT - 131
            surf.blit(active_sprite, (x_pos, y_pos + self.scroll_offset_y))

    def growth_colors(self, value):
        color = 'orange'
        if value == 0:
            color = 'orange'
        elif value == 1:
            color = 'yellow'
        elif value == 2:
            color = 'yellow-green'
        elif value == 3:
            color = 'blue'
        elif value >= 4:
            color = 'purple'
        else: # > 90
            color = 'orange'
        return color

    def create_portrait(self):
        surf = engine.create_surface((WINWIDTH, WINHEIGHT), transparent=True)
        weapon = self.unit.get_weapon()

        # Blit affinity
        affinity = DB.affinities.get(self.unit.affinity)
        if self.unit.affinity == 'Fire':
            surf.blit(SPRITES.get('fire_backdrop'), (11, 0))
        if self.unit.affinity == 'Thunder':
            surf.blit(SPRITES.get('thunder_backdrop'), (11, 0))      
        if self.unit.affinity == 'Ice':
            surf.blit(SPRITES.get('ice_backdrop'), (11, 0))
        if self.unit.affinity == 'Wind':
            surf.blit(SPRITES.get('wind_backdrop'), (11, 0))   
        if self.unit.affinity == 'Light':
            surf.blit(SPRITES.get('light_backdrop'), (11, 0))
        if self.unit.affinity == 'Dark':
            surf.blit(SPRITES.get('dark_backdrop'), (11, 0))  

        im, offset = icons.get_portrait(self.unit)
        if im:
            x_pos = (im.get_width() - 80)//2
            portrait_surf = engine.subsurface(im, (x_pos, offset, 80, 72))
            surf.blit(portrait_surf, (11, 0))

        render_text(surf, ['text'], [self.unit.name], ['white'], (4, 77), HAlignment.LEFT)
        self.info_graph.register((4, 77, 52, 24), self.unit.desc, 'all')
        class_obj = DB.classes.get(self.unit.klass)
        render_text(surf, ['text'], [class_obj.name], ['white'], (4, 94))
        self.info_graph.register((4, 94, 72, 16), class_obj.desc, 'all')
        if self.unit.team == 'player':
            render_text(surf, ['text'], [text_funcs.translate('LV')], ['yellow'], (60, 77), HAlignment.LEFT)
            render_text(surf, ['text'], [str(self.unit.level)], ['blue'], (88, 77), HAlignment.RIGHT)
            self.info_graph.register((60, 77, 30, 16), 'Level_desc', 'all')
            render_text(surf, ['text'], [text_funcs.translate('XP')], ['yellow'], (60, 94), HAlignment.LEFT)
            render_text(surf, ['text'], [str(self.unit.exp)], ['blue'], (88, 94), HAlignment.RIGHT)
            self.info_graph.register((60, 94, 30, 16), 'Exp_desc', 'all')
        else:
            render_text(surf, ['text'], [text_funcs.translate('LV')], ['yellow'], (70, 77), HAlignment.LEFT)
            render_text(surf, ['text'], [str(self.unit.level)], ['blue'], (94, 77), HAlignment.RIGHT)
            self.info_graph.register((70, 77, 30, 16), 'Level_desc', 'all')
        render_text(surf, ['text'], [text_funcs.translate('HP')], ['yellow'], (4, 110), HAlignment.LEFT)
        render_text(surf, ['text'], [str(self.unit.get_hp())], ['blue'], (38, 110), HAlignment.RIGHT)
        self.info_graph.register((4, 110, 72, 16), 'HP_desc', 'all')
        max_hp = equations.parser.hitpoints(self.unit)
        render_text(surf, ['text'], [str(max_hp)], ['blue'], (46, 110), HAlignment.LEFT)

        render_text(surf, ['text'], [text_funcs.translate('Atk')], ['yellow'], (4, 126))
        self.info_graph.register((4, 126, 64, 16), 'Atk_desc', 'all')
        render_text(surf, ['text'], [text_funcs.translate('Hit')], ['yellow'], (4, 143))
        self.info_graph.register((4, 143, 64, 16), 'Hit_desc', 'all')
        if DB.constants.value('crit'):
            render_text(surf, ['text'], [text_funcs.translate('Crit')], ['yellow'], (51, 126))
            self.info_graph.register((43, 126, 56, 16), 'Crit_desc', 'all')
        else:
            render_text(surf, ['text'], [text_funcs.translate('AS')], ['yellow'], (51, 126))
            self.info_graph.register((43, 126, 56, 16), 'AS_desc', 'all')
        render_text(surf, ['text'], [text_funcs.translate('Avo')], ['yellow'], (51, 143))
        self.info_graph.register((43, 143, 56, 16), 'Avoid_desc', 'all')
        if weapon:
        #    rng = item_funcs.get_range_string(self.unit, weapon)
            dam = str(combat_calcs.damage(self.unit, weapon))
            acc = str(combat_calcs.accuracy(self.unit, weapon))
            crt = combat_calcs.crit_accuracy(self.unit, weapon)
            if crt is None:
                crt = '--'
            else:
                crt = str(crt)
        else:
            rng, dam, acc, crt = '--', '--', '--', '--'

        avo = str(combat_calcs.avoid(self.unit, weapon))
        attack_speed = str(combat_calcs.attack_speed(self.unit, weapon))
        #render_text(surf, ['text'], [rng], ['blue'], (127, top), HAlignment.RIGHT)
        render_text(surf, ['text'], [dam], ['blue'], (42, 126), HAlignment.RIGHT)
        render_text(surf, ['text'], [acc], ['blue'], (42, 143), HAlignment.RIGHT)
        if DB.constants.value('crit'):
            render_text(surf, ['text'], [crt], ['blue'], (89, 126), HAlignment.RIGHT)
        else:
            render_text(surf, ['text'], [attack_speed], ['blue'], (89, 126), HAlignment.RIGHT)
        render_text(surf, ['text'], [avo], ['blue'], (89, 143), HAlignment.RIGHT)
        
        # Blit the white status platform
        surf.blit(SPRITES.get('status_platform'), (73, 59))
        surf.blit(SPRITES.get('slash_logo'), (38, 115))           
        return surf



    def draw_slide(self, surf):
        top_surf = engine.create_surface((WINWIDTH, WINHEIGHT), transparent=True)
        main_surf = engine.copy_surface(top_surf)

        # Blit title of menu
        #top_surf.blit(SPRITES.get('info_title_background'), (112, 8))
        if self.logo:
            self.logo.update()
            self.logo.draw(top_surf)
        # Blit page numbers
        if DB.constants.value('unit_notes') and self.unit.notes:
            num_states = len(info_states)
        else:
            num_states = len(info_states) - 2
        #page = str(info_states.index(self.state) + 1) + '/' + str(num_states)
        #render_text(top_surf, ['small'], [page], [], (235, 10), HAlignment.RIGHT)


        if self.state == 'personal_data':
            if self.growth_flag:
                if not self.growths_surf:
                    self.growths_surf = self.create_personal_data_surf(growths=True)
                self.draw_growths_surf(main_surf)
            else:
                if not self.personal_data_surf:
                    self.personal_data_surf = self.create_personal_data_surf()
                self.draw_personal_data_surf(main_surf)
            if DB.constants.value('fatigue') and self.unit.team == 'player' and \
                    game.game_vars.get('_fatigue'):
                if not self.fatigue_surf:
                    self.fatigue_surf = self.create_fatigue_surf()
                self.draw_fatigue_surf(main_surf)
            if not self.equipment_surf:
                self.equipment_surf = self.create_equipment_surf()
            self.draw_equipment_surf(main_surf)

        elif self.state == 'support_skills':
            main_surf.blit(SPRITES.get('skills_logo'), (111, 9))
            main_surf.blit(SPRITES.get('status_logo'), (111, 45))
            main_surf.blit(SPRITES.get('support_level_logo'), (110, 81))
            main_surf.blit(SPRITES.get('weapon_level_logo'), (111, 116))    
            if not self.class_skill_surf:
                self.class_skill_surf = self.create_class_skill_surf()
            self.draw_class_skill_surf(main_surf)                    
            if not self.skill_surf:
                self.skill_surf = self.create_skill_surf()
            self.draw_skill_surf(main_surf)
            if not self.support_surf:
                self.support_surf = self.create_support_surf()
            self.draw_support_surf(main_surf)
            if not self.wexp_surf:
                self.wexp_surf = self.create_wexp_surf()
            self.draw_wexp_surf(main_surf)

        # Now put it in the right place
        offset_x = max(96, 96 - self.scroll_offset_x)
        main_surf = engine.subsurface(main_surf, (offset_x, 0, main_surf.get_width() - offset_x, WINHEIGHT))
        surf.blit(main_surf, (max(96, 96 + self.scroll_offset_x), self.scroll_offset_y))
        if self.transparency:
            top_surf = image_mods.make_translucent(top_surf, self.transparency)
        surf.blit(top_surf, (0, self.scroll_offset_y))

    def create_personal_data_surf(self, growths=False):
        if growths:
            state = 'growths'
        else:
            state = 'personal_data'

        menu_size = WINWIDTH, WINHEIGHT
        surf = engine.create_surface(menu_size, transparent=True)

        left_stats = [stat.nid for stat in DB.stats]

        if growths:
            icons.draw_growth(surf, 'STR', self.unit, (52, 102))
            icons.draw_growth(surf, 'MAG', self.unit, (94, 102))
            icons.draw_growth(surf, 'SPD', self.unit, (136, 102))

            icons.draw_growth(surf, 'SKL', self.unit, (52, 121))
            icons.draw_growth(surf, 'LCK', self.unit, (94, 121))
            icons.draw_growth(surf, 'MOV', self.unit, (136, 121))

            icons.draw_growth(surf, 'DEF', self.unit, (52, 140))
            icons.draw_growth(surf, 'RES', self.unit, (94, 140))
            icons.draw_growth(surf, 'HP', self.unit, (136, 140))
        else:
            icons.draw_stat(surf, 'STR', self.unit, (48, 102))
            icons.draw_stat(surf, 'MAG', self.unit, (90, 102))
            icons.draw_stat(surf, 'SPD', self.unit, (132, 102))

            icons.draw_stat(surf, 'SKL', self.unit, (48, 121))
            icons.draw_stat(surf, 'LCK', self.unit, (90, 121))
            icons.draw_stat(surf, 'MOV', self.unit, (132, 121))

            icons.draw_stat(surf, 'DEF', self.unit, (48, 140))
            icons.draw_stat(surf, 'RES', self.unit, (90, 140))
            icons.draw_stat(surf, 'CON', self.unit, (132, 140))

           
        # STR
        color = 'orange'
        strength_stat = left_stats[1]
        if DB.stats.get('STR').growth_colors and self.unit.team == 'player' and not self.unit.generic:
            logging.info("Current stat is: %s..." % self.unit.levelled_stats.get('STR'))
            color = self.growth_colors(self.unit.levelled_stats.get('STR'))
        render_text(surf, ['text'], ['Str'], [color], (18, 102))
        if growths:
            logging.info("Current stat is: %s..." % ('STR'))
            contribution = unit_funcs.growth_contribution(self.unit, strength_stat)
        else:
            base_value = self.unit.stats.get(strength_stat, 0)
            subtle_stat_bonus = self.unit.subtle_stat_bonus(strength_stat)
            base_value += subtle_stat_bonus
            contribution = self.unit.stat_contribution(strength_stat)
            contribution['Base Value'] = base_value
        help_box = help_menu.StatDialog('STR_desc' or ('%s_desc' % strength_stat), contribution)
        self.info_graph.register((96 + 18, 102, 64, 16), help_box, state)
        # MAG
        color = 'orange'
        mag_stat = left_stats[2]
        if DB.stats.get('MAG').growth_colors and self.unit.team == 'player' and not self.unit.generic:
            color = self.growth_colors(self.unit.levelled_stats.get('MAG'))
        render_text(surf, ['text'], ['Mag'], [color], (60, 102))
        if growths:
            contribution = unit_funcs.growth_contribution(self.unit, mag_stat)
        else:
            base_value = self.unit.stats.get(mag_stat, 0)
            subtle_stat_bonus = self.unit.subtle_stat_bonus(mag_stat)
            base_value += subtle_stat_bonus
            contribution = self.unit.stat_contribution(mag_stat)
            contribution['Base Value'] = base_value
        help_box = help_menu.StatDialog('MAG_desc' or ('%s_desc' % mag_stat), contribution)
        self.info_graph.register((96 + 60, 102, 64, 16), help_box, state)            
        # SPD
        color = 'orange'
        speed_stat = left_stats[4]
        if DB.stats.get('SPD').growth_colors and self.unit.team == 'player' and not self.unit.generic:
            color = self.growth_colors(self.unit.levelled_stats.get('SPD'))
        render_text(surf, ['text'], ['Spd'], [color], (102, 102))
        if growths:
            contribution = unit_funcs.growth_contribution(self.unit, speed_stat)
        else:
            base_value = self.unit.stats.get(speed_stat, 0)
            subtle_stat_bonus = self.unit.subtle_stat_bonus(speed_stat)
            base_value += subtle_stat_bonus
            contribution = self.unit.stat_contribution(speed_stat)
            contribution['Base Value'] = base_value
        help_box = help_menu.StatDialog('SPD_desc' or ('%s_desc' % speed_stat), contribution)
        self.info_graph.register((96 + 102, 102, 64, 16), help_box, state)            
        # SKL
        color = 'orange'
        skill_stat = left_stats[3]
        if DB.stats.get('SKL').growth_colors and self.unit.team == 'player' and not self.unit.generic:
            color = self.growth_colors(self.unit.levelled_stats.get('SKL'))
        render_text(surf, ['text'], ['Skl'], [color], (18, 121))
        if growths:
            contribution = unit_funcs.growth_contribution(self.unit, skill_stat)
        else:
            base_value = self.unit.stats.get(skill_stat, 0)
            subtle_stat_bonus = self.unit.subtle_stat_bonus(skill_stat)
            base_value += subtle_stat_bonus
            contribution = self.unit.stat_contribution(skill_stat)
            contribution['Base Value'] = base_value
        help_box = help_menu.StatDialog('SKL_desc' or ('%s_desc' % skill_stat), contribution)
        self.info_graph.register((96 + 18, 121, 64, 16), help_box, state)
        # LCK
        color = 'orange'
        luck_stat = left_stats[5]
        if DB.stats.get('LCK').growth_colors and self.unit.team == 'player' and not self.unit.generic:
            color = self.growth_colors(self.unit.levelled_stats.get('LCK'))
        render_text(surf, ['text'], ['Lck'], [color], (60, 121))
        if growths:
            contribution = unit_funcs.growth_contribution(self.unit, luck_stat)
        else:
            base_value = self.unit.stats.get(luck_stat, 0)
            subtle_stat_bonus = self.unit.subtle_stat_bonus(luck_stat)
            base_value += subtle_stat_bonus
            contribution = self.unit.stat_contribution(luck_stat)
            contribution['Base Value'] = base_value
        help_box = help_menu.StatDialog('LCK_desc' or ('%s_desc' % luck_stat), contribution)
        self.info_graph.register((96 + 60, 121, 64, 16), help_box, state)            
        # MOV
        color = 'orange'
        mov_stat = left_stats[9]
        if DB.stats.get('MOV').growth_colors and self.unit.team == 'player' and not self.unit.generic:
            color = self.growth_colors(self.unit.levelled_stats.get('MOV'))
        render_text(surf, ['text'], ['Mov'], [color], (102, 121))
        if growths:
            contribution = unit_funcs.growth_contribution(self.unit, mov_stat)
        else:
            base_value = self.unit.stats.get(mov_stat, 0)
            subtle_stat_bonus = self.unit.subtle_stat_bonus(mov_stat)
            base_value += subtle_stat_bonus
            contribution = self.unit.stat_contribution(mov_stat)
            contribution['Base Value'] = base_value
        help_box = help_menu.StatDialog('MOV_desc' or ('%s_desc' % mov_stat), contribution)
        self.info_graph.register((96 + 102, 121, 64, 16), help_box, state)
        # DEF
        color = 'orange'
        def_stat = left_stats[6]
        if DB.stats.get('DEF').growth_colors and self.unit.team == 'player' and not self.unit.generic:
            color = self.growth_colors(self.unit.levelled_stats.get('DEF'))
        render_text(surf, ['text'], ['Def'], [color], (18, 140))
        if growths:
            contribution = unit_funcs.growth_contribution(self.unit, def_stat)
        else:
            base_value = self.unit.stats.get(def_stat, 0)
            subtle_stat_bonus = self.unit.subtle_stat_bonus(def_stat)
            base_value += subtle_stat_bonus
            contribution = self.unit.stat_contribution(def_stat)
            contribution['Base Value'] = base_value
        help_box = help_menu.StatDialog('DEF_desc' or ('%s_desc' % def_stat), contribution)
        self.info_graph.register((96 + 18, 140, 64, 16), help_box, state)
        # RES
        color = 'orange'
        res_stat = left_stats[7]
        if DB.stats.get('RES').growth_colors and self.unit.team == 'player' and not self.unit.generic:
            color = self.growth_colors(self.unit.levelled_stats.get('RES'))
        render_text(surf, ['text'], ['Res'], [color], (60, 140))
        if growths:
            contribution = unit_funcs.growth_contribution(self.unit, res_stat)
        else:
            base_value = self.unit.stats.get(res_stat, 0)
            subtle_stat_bonus = self.unit.subtle_stat_bonus(res_stat)
            base_value += subtle_stat_bonus
            contribution = self.unit.stat_contribution(res_stat)
            contribution['Base Value'] = base_value
        help_box = help_menu.StatDialog('RES_desc' or ('%s_desc' % res_stat), contribution)
        self.info_graph.register((96 + 60, 140, 64, 16), help_box, state)            
        # CON
        color = 'orange'
        hp_stat = left_stats[0]
        con_stat = left_stats[8]
        if growths:
            if DB.stats.get('HP').growth_colors and self.unit.team == 'player':
                color = self.growth_colors(self.unit.levelled_stats.get('HP'))
            render_text(surf, ['text'], [text_funcs.translate('HP')], [color], (102, 140))
            self.info_graph.register((96 + 102, 140, 64, 16), 'HP_desc', state)
        else:
            render_text(surf, ['text'], ['Con'], [color], (102, 140))
            base_value = self.unit.stats.get(con_stat, 0)
            subtle_stat_bonus = self.unit.subtle_stat_bonus(con_stat)
            base_value += subtle_stat_bonus
            contribution = self.unit.stat_contribution(con_stat)
            contribution['Base Value'] = base_value
        help_box = help_menu.StatDialog('CON_desc' or ('%s_desc' % con_stat), contribution)
        self.info_graph.register((96 + 102, 140, 64, 16), help_box, state)


        other_stats = ['RAT']
        if DB.constants.value('talk_display'):
            other_stats.insert(0, 'TALK')
        if DB.constants.value('pairup') and DB.constants.value('attack_stance_only'):
            pass
        else:
            other_stats.insert(0, 'AID')
            other_stats.insert(0, 'TRV')
        if self.unit.get_max_mana() > 0:
            other_stats.insert(0, 'MANA')
        if DB.constants.value('pairup') and not DB.constants.value('attack_stance_only'):
            other_stats.insert(2, 'GAUGE')

        other_stats = other_stats[:6]

        #for idx, stat in enumerate(other_stats):


            #if stat == 'TRV':
            #    if growths:
            #        icons.draw_growth(surf, 'HP', self.unit, (111, 16 * idx + 24))
            #        color = 'yellow'
            #        if DB.stats.get('HP').growth_colors and self.unit.team == 'player':
            #            color = self.growth_colors(self.unit.levelled_stats.get('HP'))
            #        render_text(surf, ['text'], [text_funcs.translate('HP')], [color], (72, 16 * idx + 24))
            #        self.info_graph.register((96 + 72, 16 * idx + 24, 64, 16), 'HP_desc', state)
            #    else:
            #        if self.unit.traveler:
            #            trav = game.get_unit(self.unit.traveler)
            #            render_text(surf, ['text'], [trav.name], ['blue'], (96, 16 * idx + 24))
            #        else:
            #            render_text(surf, ['text'], ['--'], ['blue'], (96, 16 * idx + 24))
            #        render_text(surf, ['text'], [text_funcs.translate('Trv')], ['yellow'], (72, 16 * idx + 24))
            #        self.info_graph.register((96 + 72, 16 * idx + 24, 64, 16), 'Trv_desc', state)
        surf.blit(SPRITES.get('page_divider'), (19, 93))
        return surf

    def draw_personal_data_surf(self, surf):
        surf.blit(self.personal_data_surf, (96, 0))

    def draw_growths_surf(self, surf):
        surf.blit(self.growths_surf, (96, 0))

    def create_class_skill_surf(self):
        surf = engine.create_surface((WINWIDTH - 96, WINHEIGHT), transparent=True)
        class_skills = [skill for skill in self.unit.skills if skill.class_skill and not skill_system.hidden(skill, self.unit)]

        # stacked skills appear multiple times, but should be drawn only once
        skill_counter = {}
        unique_skills = list()
        for skill in class_skills:
            if skill.nid not in skill_counter:
                skill_counter[skill.nid] = 1
                unique_skills.append(skill)
            else:
                skill_counter[skill.nid] += 1
        for idx, skill in enumerate(unique_skills[:6]):
            left_pos = idx * 24
            icons.draw_skill(surf, skill, (left_pos + 22, 24), compact=False, grey=skill_system.is_grey(skill, self.unit))
            if skill_counter[skill.nid] > 1:
                text = str(skill_counter[skill.nid])
                render_text(surf, ['small'], [text], ['white'], (left_pos + 26 - 4 * len(text), 24))
            if skill.data.get('total_charge'):
                charge = ' %d / %d' % (skill.data['charge'], skill.data['total_charge'])
            else:
                charge = ''
            self.info_graph.register((idx * 24 + 22 + 96, 24, 16, 16), help_menu.HelpDialog(skill.desc, name=skill.name + charge), 'support_skills', first=True)

        return surf

    def draw_class_skill_surf(self, surf):
        surf.blit(self.class_skill_surf, (96, 0))

    def create_skill_surf(self):
        surf = engine.create_surface((WINWIDTH - 96, WINHEIGHT), transparent=True)
        skills = [skill for skill in self.unit.skills if not (skill.class_skill or skill_system.hidden(skill, self.unit))]
        # stacked skills appear multiple times, but should be drawn only once
        skill_counter = {}
        unique_skills = []
        for skill in skills:
            if skill.nid not in skill_counter:
                skill_counter[skill.nid] = 1
                unique_skills.append(skill)
            else:
                skill_counter[skill.nid] += 1
        for idx, skill in enumerate(unique_skills[:6]):
            left_pos = idx * 24
            icons.draw_skill(surf, skill, (left_pos + 22, 60), compact=False, grey=skill_system.is_grey(skill, self.unit))
            if skill_counter[skill.nid] > 1:
                text = str(skill_counter[skill.nid])
                render_text(surf, ['small'], [text], ['white'], (left_pos + 34 - 4 * len(text), 60))
            if skill.data.get('total_charge'):
                charge = ' %d / %d' % (skill.data['charge'], skill.data['total_charge'])
            else:
                charge = ''
            self.info_graph.register((idx * 24 + 22 + 96, 60, 16, 16), help_menu.HelpDialog(skill.desc, name=skill.name + charge), 'support_skills')

        return surf

    def draw_skill_surf(self, surf):
        surf.blit(self.skill_surf, (96, 0))

    def create_support_surf(self):
        surf = engine.create_surface((WINWIDTH - 96, WINHEIGHT), transparent=True)
        width = (WINWIDTH - 102) // 2

        if game.game_vars.get('_supports'):
            pairs = game.supports.get_pairs(self.unit.nid)
            pairs = [pair for pair in pairs if pair.unlocked_ranks]
        else:
            pairs = []

        pairs = pairs[:6] # max six supports displayed

        for idx, pair in enumerate(pairs):
            x, y = (idx) % 2, idx // 2
            other_unit = None
            if pair.unit1 == self.unit.nid:
                other_unit = game.get_unit(pair.unit2)
            elif pair.unit2 == self.unit.nid:
                other_unit = game.get_unit(pair.unit1)
            if not other_unit:
                continue
            affinity = DB.affinities.get(other_unit.affinity)
            if affinity:
                icons.draw_item(surf, affinity, (22, 96))
                self.info_graph.register((96 + 22, 96, WINWIDTH - 120, 16), affinity.desc, 'support_skills')
            render_text(surf, ['text'], [other_unit.name], [], (39, 96))
        return surf

    def draw_support_surf(self, surf):
        surf.blit(self.support_surf, (96, 0))

    def create_wexp_surf(self):
        wexp_to_draw: List[Tuple[str, int]] = []
        for weapon, wexp in self.unit.wexp.items():
            if wexp > 0 and weapon in unit_funcs.usable_wtypes(self.unit):
                wexp_to_draw.append((weapon, wexp))
        width = (WINWIDTH - 102) // 2
        height = WINHEIGHT

        surf = engine.create_surface((WINWIDTH - 96, height), transparent=True)
        if not wexp_to_draw:
            return surf
        counter = 0
        for y in range(0, 32, 16):
            for x in range(0, 3):
                weapon, value = wexp_to_draw[counter]
                weapon_rank = DB.weapon_ranks.get_rank_from_wexp(value)
                next_weapon_rank = DB.weapon_ranks.get_next_rank_from_wexp(value)
                if not weapon_rank:
                    perc = 0
                elif not next_weapon_rank:
                    perc = 1
                else:
                    perc = (value - weapon_rank.requirement) / (next_weapon_rank.requirement - weapon_rank.requirement)

                offset = 9 + x * width//2

                icons.draw_weapon(surf, weapon, (13 + offset, 108 + y))

                # Build Groove
                build_groove(surf, (offset + 14, 125 + y), 24, perc)
                # Add text
                pos = (35 + offset, 108 + y)
                render_text(surf, ['text'], [weapon_rank.nid], ['blue'], pos, HAlignment.LEFT)
                self.info_graph.register((96 + 13 + offset, 108 + y + 24, width, 16), "%s mastery level: %d" % (DB.weapons.get(weapon).name, value), 'support_skills')
                counter += 1
                if counter >= len(wexp_to_draw):
                    break
            if counter >= len(wexp_to_draw):
                break

        return surf

    def draw_wexp_surf(self, surf):
        surf.blit(self.wexp_surf, (96, 24))

    def create_equipment_surf(self):
        def create_item_option(idx, item):
            return BasicItemOption.from_item(idx, item, width=120, mode=ItemOptionModes.FULL_USES, text_color=item_system.text_color(None, item))

        surf = engine.create_surface((WINWIDTH - 96, WINHEIGHT), transparent=True)

        weapon = self.unit.get_weapon()
        accessory = self.unit.get_accessory()

        # Blit items
        for idx, item in enumerate(self.unit.nonaccessories):
            if item.multi_item and any(subitem is weapon for subitem in item.subitems):
                surf.blit(SPRITES.get('equipment_highlight'), (12, idx * 16 + 16))
                for subitem in item.subitems:
                    if subitem is weapon:
                        item_option = create_item_option(idx, subitem)
                        break
                else:  # Shouldn't happen
                    item_option = create_item_option(idx, item)
            else:
                if item is weapon:
                    if self.unit.team == 'player':
                        surf.blit(SPRITES.get('equipment_highlight_player'), (12, idx * 16 + 16))
                    else:
                        surf.blit(SPRITES.get('equipment_highlight_enemy'), (12, idx * 16 + 16))
                item_option = create_item_option(idx, item)
            item_option.draw(surf, 14, idx * 16 + 8)
            self.info_graph.register((104, idx * 16 + 8, 120, 16), item_option.get_help_box(), 'personal_data', first=(idx == 0))

        # Blit accessories
        for idx, item in enumerate(self.unit.accessories):
            aidx = item_funcs.get_num_items(self.unit) + idx
            y_pos = aidx * 16 + 24
            if item.multi_item and any(subitem is accessory for subitem in item.subitems):
                #surf.blit(SPRITES.get('equipment_highlight'), (8, y_pos + 8))
                for subitem in item.subitems:
                    if subitem is accessory:
                        item_option = create_item_option(aidx, subitem)
                        break
                else:  # Shouldn't happen
                    item_option = create_item_option(aidx, item)
            else:
                #if item is accessory:
                    #surf.blit(SPRITES.get('equipment_highlight'), (8, y_pos + 8))
                item_option = create_item_option(aidx, item)
            item_option.draw(surf, 14, y_pos)
            first = (idx == 0 and not self.unit.nonaccessories)
            self.info_graph.register((104, y_pos, 120, 16), item_option.get_help_box(), 'personal_data', first=first)
        return surf

    def draw_equipment_surf(self, surf):
        surf.blit(self.equipment_surf, (96, 0))