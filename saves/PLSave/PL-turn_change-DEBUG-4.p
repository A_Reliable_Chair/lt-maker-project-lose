�}q (X   unitsq]q(}q(X   nidqX   AlpinqX
   prefab_nidqhX   positionqNX   teamqX   playerq	X   partyq
X   EirikaqX   klassqX   AlpinVillagerqX   variantqX    qX   factionqNX   levelqKX   expqK_X   genericq�X
   persistentq�X   aiqX   NoneqX   roam_aiqNX   ai_groupqhX   itemsq]q(K�K�K�eX   nameqX   AlpinqX   descqXH   An energetic young lad. Dreams of joining the army of [KINGDOM] someday.qX   tagsq]q X   Youthq!aX   statsq"}q#(X   HPq$KX   STRq%KX   MAGq&K X   SKLq'KX   SPDq(KX   LCKq)KX   DEFq*KX   RESq+K X   CONq,KX   MOVq-KuX   growthsq.}q/(h$K<h%K#h&K h'Kh(Kh)Kh*Kh+Kh,K h-KuX   growth_pointsq0}q1(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uX   stat_cap_modifiersq2}q3(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uX   starting_positionq4KK�q5X   wexpq6}q7(X   Swordq8KX   Lanceq9K X   Axeq:K X   Bowq;K X   Staffq<K X   Lightq=K X   Animaq>K X   Darkq?K X   Defaultq@K uX   portrait_nidqAX   AlpinqBX   affinityqCX   FireqDX   skillsqE]qF(K�K�capp.engine.source_type
SourceType
qGX   itemqH���qI�qJRqK�qLK�X   gameqMhGX   globalqN���qO�qPRqQ�qRK�hMhQ�qSK�hhGX   personalqT���qU�qVRqW�qXK�hhW�qYeX   notesqZ]q[X
   current_hpq\K X   current_manaq]K X   current_fatigueq^K X   travelerq_NX   current_guard_gaugeq`K X   built_guardqa�X   deadqb�X   action_stateqc(��������tqdX   _fieldsqe}qfX   equipped_weaponqgK�X   equipped_accessoryqhNu}qi(hX   CarlinqjhhjhKK�qkhX   playerqlh
hhX   CarlinVillagerqmhhhNhKhK h�h�hX   NoneqnhNhhh]qo(K�K�K�ehX   CarlinqphXA   A magically gifted youngster from [TOWN]. Energetic and sporatic.qqh]qrX   Youthqsah"}qt(h$K
h%K h&Kh'Kh(Kh)Kh*Kh+Kh,Kh-Kuh.}qu(h$K2h%K h&Kh'K#h(Kh)Kh*Kh+Kh,K h-Kuh0}qv(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}qw(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4hkh6}qx(h8K h9K h:K h;K h<K h=K h>K h?Kh@K uhAX   CarlinqyhCX   WindqzhE]q{(K�K�hK�q|K�hMhQ�q}K�hMhQ�q~K�hjhW�qK�hjhW�q�M[hkhGX   terrainq����q��q�Rq��q�ehZ]q�h\K
h]K h^K h_Nh`K ha�hb�hc(��������tq�he}q�hgK�hhNu}q�(hX   Shaylaq�hh�hKK�q�hX   playerq�h
hhX   ShaylaVillagerq�hhhNhKhK h�h�hX   Noneq�hNhhh]q�(K�K�K�ehX   Shaylaq�hXI   A clumsy young lass who hopes to adventure the world. Gutsy and fearless.q�h]q�X   Youthq�ah"}q�(h$Kh%Kh&K h'Kh(Kh)Kh*Kh+K h,Kh-Kuh.}q�(h$KAh%Kh&K h'Kh(Kh)Kh*K#h+Kh,K h-Kuh0}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4h�h6}q�(h8K h9K h:Kh;K h<K h=K h>K h?K h@K uhAX   Shaylaq�hCX   Thunderq�hE]q�(K�K�hK�q�K�hMhQ�q�K�hMhQ�q�K�h�hW�q�K�h�hW�q�M\h�h��q�MoX   Fire1_1q�hGX   regionq����q��q�Rq��q�ehZ]q�h\Kh]K h^K h_Nh`K ha�hb�hc(��������tq�he}q�hgK�hhNu}q�(hX   Raelinq�hh�hKK�q�hX   playerq�h
hhX   ZoyaVillagerq�hhhNhKhK h�h�hX   Noneq�hNhhh]q�(K�K�K�ehX   Raelinq�hX8   The shut-in daughter of a fisherman. Timid and cautious.q�h]q�X   Youthq�ah"}q�(h$Kh%Kh&K h'Kh(Kh)Kh*Kh+Kh,Kh-Kuh.}q�(h$K7h%Kh&K h'Kh(K#h)Kh*Kh+Kh,K h-Kuh0}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4h�h6}q�(h8K h9Kh:K h;K h<K h=K h>K h?K h@K uhAX   Raelin (LittleKyng)q�hCX   Iceq�hE]q�(K�K�hK�q�K�hMhQ�q�K�hMhQ�q�K�h�hW�q�K�h�hW�q�M]h�h��q�ehZ]q�h\Kh]K h^K h_Nh`K ha�hb�hc(��������tq�he}q�hgK�hhNu}q�(hX   Elwynnq�hh�hKK	�q�hX   playerq�h
hhX   Monkq�hhhNhKhK h�h�hX   Noneq�hNhhh]q�(K�K�K�K�ehX   Elwynnq�hXM   Former vagabond turned holy monk of [CHURCH]. Stern and generally unpleasant.q�h]q�X   Adultq�ah"}q�(h$Kh%K h&Kh'Kh(Kh)Kh*Kh+Kh,Kh-Kuh.}q�(h$K7h%K h&Kh'Kh(Kh)K
h*Kh+K#h,K h-Kuh0}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4h�h6}q�(h8K h9K h:K h;K h<K h=Kh>K h?K h@K uhAX   Elwynn (LittleKyng)q�hCX   Darkq�hE]q�(K�hMhQ�q�M hMhQ�q�Mh�hW�q�Mh�hW�q�Mh�hW�q�M^h�h��q�ehZ]q�h\Kh]K h^K h_Nh`K ha�hb�hc(��������tq�he}q�hgK�hhNu}q�(hX   Saraidq�hh�hKK�q�hX   playerq�h
hhX   Hunterq�hhhNhKhKh�h�hX   Noneq�hNhhh]q�(K�K�K�K�ehX   Saraidq�hX0   A fledgling huntress. Unreserved and unfiltered.q�h]q�X   Adultq�ah"}q�(h$Kh%Kh&Kh'Kh(Kh)K h*K h+Kh,Kh-Kuh.}q�(h$K2h%Kh&K h'Kh(K#h)Kh*Kh+Kh,K h-Kuh0}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4KK�q�h6}q�(h8K h9K h:K h;G@*      h<K h=K h>K h?K h@K uhAX   Sloaneq�hCX   Lightq�hE]q�(MK�hK�q�MhMhQ�q�MhMhQ�q�Mh�hW�q�Mh�hW�q�M	h�hW�q�M_h�h��q�ehZ]q�h\Kh]K h^K h_Nh`K ha�hb�hc(��������tq�he}r   hgK�hhNu}r  (hX   Quinleyr  hj  hKK�r  hX   playerr  h
hhX   Mager  hX   Quinleyr  hNhKhK h�h�hX   Noner  hNhhh]r  (K�K�K�ehX   Quinleyr	  hXN   A genius magician who's grown tired of elemental magic. Outgoing and friendly.r
  h]r  X   Adultr  ah"}r  (h$Kh%K h&Kh'Kh(Kh)Kh*K h+Kh,Kh-Kuh.}r  (h$K2h%K h&K#h'Kh(Kh)Kh*Kh+Kh,K h-Kuh0}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4j  h6}r  (h8K h9K h:K h;K h<K h=K h>Kh?K h@K uhAX   Quinleyr  hCX   Darkr  hE]r  (M
K�hK�r  MhMhQ�r  MhMhQ�r  Mj  hW�r  Mj  hW�r  Mj  hW�r  M`j  h��r  MpX   Fire1_3r  h��r  ehZ]r  h\Kh]K h^K h_Nh`K ha�hb�hc(��������tr  he}r   hgK�hhNu}r!  (hX   Elspethr"  hj"  hKK�r#  hX   playerr$  h
hhX   Clericr%  hX   Elspethr&  hNhKhK h�h�hX   Noner'  hNhhh]r(  K�ahX   Elspethr)  hX2   Lifelong devotee of [CHURCH]. Humble and selfless.r*  h]r+  X   Adultr,  ah"}r-  (h$Kh%K h&Kh'K h(Kh)Kh*K h+Kh,Kh-Kuh.}r.  (h$K2h%K h&Kh'Kh(Kh)K#h*Kh+K#h,K h-Kuh0}r/  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r0  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4j#  h6}r1  (h8K h9K h:K h;K h<Kh=K h>K h?K h@K uhAX   Elspethr2  hCX   Lightr3  hE]r4  (MhMhQ�r5  MhMhQ�r6  Mj"  hW�r7  Mj"  hW�r8  Mj"  hW�r9  Maj#  h��r:  ehZ]r;  h\Kh]K h^K h_Nh`K ha�hb�hc(��������tr<  he}r=  hgNhhNu}r>  (hX   Lamonter?  hj?  hKK
�r@  hX   playerrA  h
hhX   SoldierrB  hX   LamonterC  hNhKhK h�h�hX   NonerD  hNhhh]rE  (K�K�K�ehX   LamonterF  hX,   One of Leod's guards. Kindhearted and loyal.rG  h]rH  X   AdultrI  ah"}rJ  (h$Kh%Kh&K h'Kh(Kh)Kh*Kh+K h,K
h-Kuh.}rK  (h$KAh%Kh&K h'Kh(Kh)K
h*K#h+Kh,K h-Kuh0}rL  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}rM  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4j@  h6}rN  (h8K h9Kh:K h;K h<K h=K h>K h?K h@K uhAX   LamonterO  hCX   IcerP  hE]rQ  (MhMhQ�rR  MhMhQ�rS  Mj?  hW�rT  Mj?  hW�rU  Mj?  hW�rV  MK�hK�rW  Mbj@  h��rX  ehZ]rY  h\Kh]K h^K h_Nh`K ha�hb�hc(��������trZ  he}r[  hgK�hhNu}r\  (hX   Garveyr]  hj]  hKK	�r^  hX   playerr_  h
hhX   Archerr`  hX   Garveyra  hNhKhK h�h�hX   Nonerb  hNhhh]rc  (K�K�K�ehX   Garveyrd  hXA   One of Leod's guards. Dislikes his hometown and fellow villagers.re  h]rf  X   Adultrg  ah"}rh  (h$Kh%Kh&K h'Kh(Kh)Kh*Kh+K h,K
h-Kuh.}ri  (h$K7h%K#h&K h'Kh(Kh)K
h*Kh+Kh,K h-Kuh0}rj  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}rk  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4j^  h6}rl  (h8K h9K h:K h;Kh<K h=K h>K h?K h@K uhAX   Garveyrm  hCX   Firern  hE]ro  (MhMhQ�rp  MhMhQ�rq  Mj]  hW�rr  Mj]  hW�rs  MK�hK�rt  Mcj^  h��ru  MlNhGX   defaultrv  ���rw  �rx  Rry  �rz  ehZ]r{  h\Kh]K h^K h_Nh`K ha�hb�hc(��������tr|  he}r}  hgK�hhNu}r~  (hX   Orlar  hj  hK	K�r�  hX   playerr�  h
hhX   Fighterr�  hX   Orlar�  hNhKhK h�h�hX   Noner�  hNhhh]r�  (K�K�K�ehX   Orlar�  hXK   Lumberjack, carpenter, and all around outdoorsperson. Outspoken and jovial.r�  h]r�  X   Adultr�  ah"}r�  (h$Kh%Kh&K h'Kh(Kh)Kh*Kh+K h,Kh-Kuh.}r�  (h$KAh%K#h&K h'Kh(Kh)K
h*Kh+Kh,K h-K
uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4j�  h6}r�  (h8K h9K h:Kh;K h<K h=K h>K h?K h@K uhAX   Orlar�  hCX   Windr�  hE]r�  (M hMhQ�r�  M!hMhQ�r�  M"j  hW�r�  M#j  hW�r�  M$j  hW�r�  M%K�hK�r�  Mdj�  h��r�  MtX   Fire2_2r�  h��r�  ehZ]r�  h\Kh]K h^K h_Nh`K ha�hb�hc(��������tr�  he}r�  hgK�hhNu}r�  (hX   Ailsar�  hj�  hKK�r�  hX   playerr�  h
hhX   Lancerr�  hX   Ailsar�  hNhKhKh�h�hX   Noner�  hNhhh]r�  (K�K�K�ehX   Ailsar�  hXE   A reclusive fisherman. Quick witted and sharped tongued. Zoya's aunt.r�  h]r�  X   Adultr�  ah"}r�  (h$Kh%Kh&K h'Kh(Kh)Kh*K h+Kh,K	h-Kuh.}r�  (h$K<h%Kh&K h'Kh(K#h)K
h*Kh+Kh,K h-K
uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4KK�r�  h6}r�  (h8K h9G@6      h:K h;K h<K h=K h>K h?K h@K uhAX   Ailsar�  hCX   Thunderr�  hE]r�  (M&hMhQ�r�  M'hMhQ�r�  M(j�  hW�r�  M)j�  hW�r�  M*j�  hW�r�  M+K�hK�r�  Mej�  h��r�  MnX   Fire1_0r�  h��r�  ehZ]r�  h\Kh]K h^K h_Nh`K ha�hb�hc(��������tr�  he}r�  hgK�hhNu}r�  (hX   Corridonr�  hj�  hKK�r�  hX   playerr�  h
hhX
   Blacksmithr�  hhhNhKhK h�h�hX   Noner�  hNhhh]r�  K�ahX   Corridonr�  hX;   Town blacksmith and retired war veteran. Friendly and just.r�  h]r�  X   Adultr�  ah"}r�  (h$Kh%K h&K h'K h(Kh)Kh*Kh+Kh,Kh-Kuh.}r�  (h$KAh%K h&K h'K h(Kh)K#h*K#h+K#h,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4j�  h6}r�  (h8K h9K h:K h;K h<K h=K h>K h?K h@K uhAX   Corridonr�  hCX   Lightr�  hE]r�  (M,hMhQ�r�  M-hMhQ�r�  M.j�  hW�r�  M/j�  hW�r�  M0j�  hW�r�  M1j�  hW�r�  Mfj�  h��r�  MsX   Fire2_1r�  h��r�  ehZ]r�  h\K
h]K h^K h_Nh`K ha�hb�hc(��������tr�  he}r�  hgNhhNu}r�  (hX   Laisrenr�  hj�  hKK	�r�  hX   playerr�  h
hhX   Cavalierr�  hX   Cathalr�  hNhKhK h�h�hX   Noner�  hNhhh]r�  (K�K�K�ehX   Laisrenr�  hXV   An aspiring recruit in [KINGDOM]'s army. Charming and outgoing. Alpin's older brother.r�  h]r�  X   Adultr�  ah"}r�  (h$Kh%Kh&K h'Kh(Kh)Kh*Kh+Kh,Kh-Kuh.}r�  (h$KAh%K#h&K#h'K#h(K#h)K#h*K#h+K#h,K h-Kuh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4j�  h6}r�  (h8Kh9Kh:K h;K h<K h=K h>K h?K h@K uhAX   Cathalr�  hCX   Darkr�  hE]r�  (M2hMhQ�r�  M3hMhQ�r�  M4j�  hW�r�  M5j�  hW�r�  M6j�  hGh���r�  �r�  Rr�  �r�  Mgj�  h��r�  MuX   Fire2_3r�  h��r�  ehZ]r�  h\K
h]K h^K h_Nh`K ha�hb�hc(��������tr�  he}r�  hgK�hhNu}r   (hX	   Cawthorner  hj  hKK�r  hX   enemyr  h
hhX   Generalr  hhhNhKhK h�h�hX   Noner  hNhhh]r  K�ahX	   Cawthorner  hX   Cawthorne text.r  h]r	  X   Bossr
  ah"}r  (h$Kh%Kh&K h'Kh(Kh)K h*K	h+Kh,Kh-Kuh.}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4j  h6}r  (h8K h9Kh:K)h;K h<K h=K h>K h?K h@K uhAX	   Cawthorner  hCX   Firer  hE]r  (M7hMhQ�r  M8hMhQ�r  M9j  hW�r  M:j  hW�r  Mhj  h��r  ehZ]r  h\Kh]K h^K h_Nh`K ha�hb�hc(��������tr  he}r  hgK�hhNu}r  (hX   101r  hj  hKK
�r  hX   enemyr  h
hhX   Dracoknightr  hNhX   Soldierr   hKhK h�h�hX   Noner!  hNhhh]r"  (K�K�ehX   Soldierr#  hX=   The army of Grado, the largest nation on the entire continentr$  h]r%  h"}r&  (h$K
h%Kh&K h'Kh(Kh)K h*Kh+K h,K
h-Kuh.}r'  (h$KAh%Kh&K h'K
h(Kh)K h*Kh+K h,K h-K uh0}r(  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r)  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4j  h6}r*  (h8K h9K h:Kh;K h<K h=K h>K h?K h@K uhANhCNhE]r+  (M;hMhQ�r,  M<hMhQ�r-  M=j  j�  �r.  M>j  hW�r/  ehZ]r0  h\K
h]K h^K h_Nh`K ha�hb�hc(��������tr1  he}r2  hgK�hhNu}r3  (hX   102r4  hj4  hKK�r5  hX   otherr6  h
hhX   ManDeadr7  hNhX   Villagerr8  hKhK h�h�hX   Noner9  hNhhh]r:  K�ahX   Villagerr;  hX   vilgerr<  h]r=  h"}r>  (h$K
h%K h&K h'K h(K h)K h*K h+K h,K h-K uh.}r?  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}r@  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}rA  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4j5  h6}rB  (h8K h9K h:K h;K h<K h=K h>K h?K h@K uhANhCNhE]rC  (M?hMhQ�rD  M@hMhQ�rE  ehZ]rF  h\K
h]K h^K h_Nh`K ha�hb�hc(��������trG  he}rH  hgNhhNu}rI  (hX   103rJ  hjJ  hNhX   enemyrK  h
hhX   GriffonKnightrL  hNhX   SoldierrM  hKhK h�h�hX   NonerN  hNhhh]rO  K�ahj#  hj$  h]rP  h"}rQ  (h$Kh%Kh&K h'Kh(Kh)K h*Kh+Kh,Kh-Kuh.}rR  (h$K7h%K
h&K h'Kh(Kh)K h*Kh+K
h,K h-K uh0}rS  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}rT  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh6}rU  (h8K h9Kh:K h;K h<K h=K h>K h?K h@K uhANhCNhE]rV  (MAhMhQ�rW  MBhMhQ�rX  MCjL  j�  �rY  MDK�hK�rZ  ehZ]r[  h\Kh]K h^K h_Nh`K ha�hb�hc(��������tr\  he}r]  hgK�hhNu}r^  (hX   104r_  hj_  hNhX   enemyr`  h
hhX   Fighterra  hNhX   Soldierrb  hKhK h�h�hX   Nonerc  hNhhh]rd  K�ahj#  hj$  h]re  h"}rf  (h$Kh%Kh&K h'Kh(Kh)K h*Kh+K h,Kh-Kuh.}rg  (h$KZh%Kh&K h'Kh(Kh)K h*K h+K h,K h-K uh0}rh  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}ri  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh6}rj  (h8K h9K h:Kh;K h<K h=K h>K h?K h@K uhANhCNhE]rk  (MEhMhQ�rl  MFhMhQ�rm  MGK�hK�rn  ehZ]ro  h\Kh]K h^K h_Nh`K ha�hb�hc(��������trp  he}rq  hgK�hhNu}rr  (hX   105rs  hjs  hNhX   enemyrt  h
hhX   Archerru  hNhX   Soldierrv  hKhK h�h�hX   Nonerw  hNhhh]rx  (K�K�K�ehj#  hj$  h]ry  h"}rz  (h$Kh%Kh&K h'Kh(Kh)K h*Kh+Kh,Kh-Kuh.}r{  (h$K7h%Kh&K h'Kh(Kh)K h*K h+K h,K h-K uh0}r|  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r}  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4KK�r~  h6}r  (h8K h9K h:K h;G?�      h<K h=K h>K h?K h@K uhANhCNhE]r�  (MHhMhQ�r�  MIhMhQ�r�  ehZ]r�  h\K h]K h^K h_Nh`K ha�hb�hc(��������tr�  he}r�  hgK�hhNu}r�  (hX   106r�  hj�  hNhX   enemyr�  h
hhX   Soldierr�  hNhX   Soldierr�  hKhK h�h�hX   Yolor�  hNhhh]r�  K�ahj#  hj$  h]r�  h"}r�  (h$Kh%Kh&K h'Kh(Kh)K h*Kh+K h,Kh-Kuh.}r�  (h$KFh%Kh&K h'Kh(Kh)K h*Kh+K h,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4KK�r�  h6}r�  (h8K h9Kh:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r�  (MJhMhQ�r�  MKhMhQ�r�  MLK�hK�r�  ehZ]r�  h\K h]K h^K h_Nh`K ha�hb�hc(��������tr�  he}r�  hgK�hhNu}r�  (hX	   Davenportr�  hj�  hNhX   enemyr�  h
hhX   Great_Knightr�  hhhNhKhK h�h�hX   Noner�  hNhhh]r�  (K�K�K�K�ehX	   Davenportr�  hX   Davenport text.r�  h]r�  X   Bossr�  ah"}r�  (h$Kh%Kh&K h'Kh(Kh)K h*Kh+Kh,Kh-Kuh.}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh6}r�  (h8Kh9Kh:Kh;K h<K h=K h>K h?K h@K uhAX	   Davenportr�  hCX   Icer�  hE]r�  (MMhMhQ�r�  MNhMhQ�r�  MOj�  hW�r�  MPj�  j�  �r�  MQK�hK�r�  ehZ]r�  h\Kh]K h^K h_Nh`K ha�hb�hc(��������tr�  he}r�  hgK�hhNu}r�  (hX
   Brassfieldr�  hj�  hKK�r�  hX   enemyr�  h
hhX   Heror�  hhhNhKhK h�h�hX   Noner�  hNhhh]r�  (K�K�K�ehX
   Brassfieldr�  hX   Brassfield text.r�  h]r�  X   Bossr�  ah"}r�  (h$Kh%Kh&Kh'Kh(Kh)K h*Kh+Kh,Kh-Kuh.}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4j�  h6}r�  (h8Kh9K h:Kh;K h<K h=K h>K h?K h@K uhAX
   Brassfieldr�  hCX   Lightr�  hE]r�  (MRhMhQ�r�  MShMhQ�r�  MTj�  hW�r�  MUK�hK�r�  Mkj�  h��r�  ehZ]r�  h\Kh]K h^K h_Nh`K ha�hb�hc(��������tr�  he}r�  hgK�hhNu}r�  (hX   Ackermanr�  hj�  hNhX   enemyr�  h
hhX   Warriorr�  hhhNhKhK h�h�hX   Noner�  hNhhh]r�  (K�K�K�ehX   Ackermanr�  hX   Ackerman text.r�  h]r�  X   Bossr�  ah"}r�  (h$Kh%Kh&K h'Kh(Kh)K h*Kh+K h,Kh-Kuh.}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh6}r�  (h8K h9K h:Kh;Kh<K h=K h>K h?K h@K uhAX   Ackermanr�  hCX   Firer�  hE]r�  (MVhMhQ�r�  MWhMhQ�r�  MXj�  hW�r�  MYK�hK�r�  ehZ]r�  h\Kh]K h^K h_Nh`K ha�hb�hc(��������tr�  he}r�  hgK�hhNueh]r�  (}r�  (X   uidr�  K�hX
   Iron Swordr�  hX
   Iron Swordr�  hhX	   owner_nidr�  hX	   droppabler�  �X   datar�  }r�  X   subitemsr�  ]r�  X
   componentsr�  ]r�  (X   weaponr�  N�r�  X   target_enemyr�  N�r�  X	   min_ranger�  K�r�  X	   max_ranger�  K�r�  X   damager�  K�r�  X   hitr   K
�r  X	   level_expr  N�r  X   weapon_typer  X   Swordr  �r  X   critr  K �r  eu}r	  (j�  K�hX   HeartOfFirer
  hX    <orange>Heart Of Flames</orange>r  hX   +1 Damage. Fire affinity only.r  j�  hj�  �j�  }r  j�  ]r  j�  ]r  (X   status_on_holdr  X   HeartofFirer  �r  X
   text_colorr  X   oranger  �r  eu}r  (j�  K�hX   Herbsr  hX   Herbsr  hX(   Restores 4 HP. Unit can act after using.r  j�  hj�  �j�  }r  (X   usesr  KX   starting_usesr  Kuj�  ]r  j�  ]r  (X   usabler  N�r   X   target_allyr!  N�r"  j  K�r#  X   uses_optionsr$  ]r%  ]r&  (X   LoseUsesOnMiss (T/F)r'  X   Fr(  X   Lose uses even on missr)  ea�r*  X   healr+  K�r,  X   map_hit_add_blendr-  ]r.  (K`K�K�e�r/  X   attack_after_combatr0  N�r1  eu}r2  (j�  K�hX   Jinxr3  hX   Jinxr4  hX,   Lowers target's Crit Avoid by 20 for 1 turn.r5  j�  hjj�  �j�  }r6  j�  ]r7  j�  ]r8  (j�  N�r9  j�  N�r:  j�  K�r;  j�  K�r<  j�  K�r=  j   K�r>  j  N�r?  j  X   Darkr@  �rA  j-  ]rB  (KHK KAe�rC  X   magicrD  N�rE  X   unrepairablerF  N�rG  X   status_on_hitrH  X	   TerrifiedrI  �rJ  j  K �rK  X   battle_cast_animrL  X   FluxrM  �rN  eu}rO  (j�  K�hX   DevilryrP  hX   DevilryrQ  hX   Can't miss. -2 SPD.
HP Cost: 4.rR  j�  hjj�  �j�  }rS  (j  Kj  Kuj�  ]rT  j�  ]rU  (j�  N�rV  j�  N�rW  j�  K�rX  j�  K�rY  j�  K�rZ  j  N�r[  j  X   Darkr\  �r]  j-  ]r^  (KHK KAe�r_  jD  N�r`  jF  N�ra  j  K�rb  j$  ]rc  (]rd  (X   LoseUsesOnMiss (T/F)re  j(  X   Lose uses even on missrf  e]rg  (X   OneLossPerCombat (T/F)rh  j(  X    Doubling doesn't cost extra usesri  ee�rj  j  K �rk  X   status_on_equiprl  X   SPD2rm  �rn  h6K�ro  X   hp_costrp  K�rq  jL  X   Fluxrr  �rs  eu}rt  (j�  K�hX   HeartOfGalesru  hX   <orange>Heart Of Gales</orange>rv  hX   +1 SPD. Wind affinity only.rw  j�  hjj�  �j�  }rx  j�  ]ry  j�  ]rz  (j  X   HeartofGalesr{  �r|  j  X   oranger}  �r~  eu}r  (j�  K�hX   Iron Axer�  hX   Iron Axer�  hhj�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j   K �r�  j  N�r�  j  X   Axer�  �r�  j  K �r�  eu}r�  (j�  K�hX   HeartOfLightningr�  hX#   <orange>Heart Of Lightning</orange>r�  hX    +10 Crit. Thunder affinity only.r�  j�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j  X   HeartofLightningr�  �r�  j  X   oranger�  �r�  eu}r�  (j�  K�hj  hj  hj  j�  h�j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j  N�r�  j!  N�r�  j  K�r�  j$  j%  �r�  j+  K�r�  j-  j.  �r�  j0  N�r�  eu}r�  (j�  K�hX
   Iron Lancer�  hX
   Iron Lancer�  hhj�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j   K�r�  j  N�r�  j  X   Lancer�  �r�  j  K �r�  eu}r�  (j�  K�hX   Javelinr�  hX   Javelinr�  hX   Cannot Counter. SPD -2.r�  j�  h�j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j   J�����r�  j  K �r�  j  N�r�  j  X   Lancer�  �r�  j  K�r�  j$  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j(  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j(  X    Doubling doesn't cost extra usesr�  ee�r�  X   cannot_counterr�  N�r�  h6K�r�  jl  X   SPD2r�  �r�  eu}r�  (j�  K�hX   HeartOfFrostr�  hX   <orange>Heart Of Frost</orange>r�  hX   +1 DEF/RES. Ice affinity only.r�  j�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j  X   HeartofFrostr�  �r�  j  X   oranger�  �r�  eu}r�  (j�  K�hX   Flashr�  hX   Flashr�  hX*   Lowers target's Accuracy by 20 for 1 turn.r�  j�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j   K
�r�  j  K
�r�  j  N�r�  j  X   Lightr�  �r�  j-  ]r�  (K�K�K e�r�  jD  N�r�  jF  N�r�  jL  X   Glimmerr�  �r�  jH  X   Dazzledr�  �r�  eu}r�  (j�  K�hX   Shimmerr�  hX   Shimmerr�  hX   Cannot miss.r�  j�  h�j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r   j�  K�r  j�  K�r  j�  K�r  j  K
�r  j  N�r  j  X   Lightr  �r  j-  ]r  (K�K�K e�r	  jD  N�r
  jF  N�r  h6K�r  j  K�r  j$  ]r  (]r  (X   LoseUsesOnMiss (T/F)r  j(  X   Lose uses even on missr  e]r  (X   OneLossPerCombat (T/F)r  j(  X    Doubling doesn't cost extra usesr  ee�r  jL  X   Glimmerr  �r  eu}r  (j�  K�hj  hj  hj  j�  h�j�  �j�  }r  (j  Kj  Kuj�  ]r  j�  ]r  (j  N�r  j!  N�r  j  K�r  j$  j%  �r  j+  K�r   j-  j.  �r!  j0  N�r"  eu}r#  (j�  K�hX   Iron Bowr$  hX   Iron Bowr%  hhj�  h�j�  �j�  }r&  j�  ]r'  j�  ]r(  (j�  N�r)  j�  N�r*  j�  K�r+  j�  K�r,  j�  K�r-  j   K
�r.  j  N�r/  j  X   Bowr0  �r1  X   effective_damager2  }r3  (X   effective_tagsr4  ]r5  X   Flyingr6  aX   effective_multiplierr7  G@      X   effective_bonus_damager8  K X   show_effectiveness_flashr9  �u�r:  j  K �r;  X   eval_warningr<  X   'Flying' in unit.tagsr=  �r>  eu}r?  (j�  K�hX   Longbowr@  hX   LongbowrA  hX   Cannot Counter. SPD -1.rB  j�  h�j�  �j�  }rC  (j  K
j  K
uj�  ]rD  j�  ]rE  (j�  N�rF  j�  N�rG  j  N�rH  j�  K�rI  j   J�����rJ  j  X   BowrK  �rL  j  K
�rM  j$  ]rN  (]rO  (X   LoseUsesOnMiss (T/F)rP  j(  X   Lose uses even on missrQ  e]rR  (X   OneLossPerCombat (T/F)rS  j(  X    Doubling doesn't cost extra usesrT  ee�rU  j  K�rV  j�  K�rW  j�  K�rX  j�  N�rY  j2  }rZ  (j4  ]r[  X   Flyingr\  aj7  G@      j8  K j9  �u�r]  jl  X   SPD1r^  �r_  j<  X   'Flying' in unit.tagsr`  �ra  eu}rb  (j�  K�hX   HeartOfRadiancerc  hX"   <orange>Heart Of Radiance</orange>rd  hX   +15 Avoid. Holy affinity only.re  j�  h�j�  �j�  }rf  j�  ]rg  j�  ]rh  (j  X   HeartofRadianceri  �rj  j  X   orangerk  �rl  eu}rm  (j�  K�hX   Firestarterrn  hX   <purple>Firestarter</purple>ro  hX=   Saraid Only.
Sets terrain ablaze after combat.
Cannot double.rp  j�  h�j�  �j�  }rq  (j  K
j  Kuj�  ]rr  j�  ]rs  (j�  N�rt  j�  N�ru  j�  K�rv  j�  K�rw  j�  K�rx  j   K�ry  j  K �rz  j  K�r{  j$  ]r|  (]r}  (X   LoseUsesOnMiss (T/F)r~  j(  X   Lose uses even on missr  e]r�  (X   OneLossPerCombat (T/F)r�  j(  X    Doubling doesn't cost extra usesr�  ee�r�  j  N�r�  j  X   Bowr�  �r�  X   prf_unitr�  ]r�  X   Saraidr�  a�r�  j  X   purpler�  �r�  j2  }r�  (j4  ]r�  X   Flyingr�  aj7  G@       j8  K j9  �u�r�  X   event_after_combat_even_missr�  X   Global Firestarter_Originr�  �r�  X	   no_doubler�  N�r�  eu}r�  (j�  K�hX   Entangler�  hX   Entangler�  hX'   Lowers target's Avoid by 20 for 1 turn.r�  j�  j  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j   K�r�  j  N�r�  j  X   Animar�  �r�  j-  ]r�  (KUKUK e�r�  jD  N�r�  jF  N�r�  jH  X	   Entangledr�  �r�  j  K �r�  jL  X   Firer�  �r�  eu}r�  (j�  K�hX   Firer�  hX   Firer�  hX*   Effective against horseback units. -1 SPD.r�  j�  j  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j   K �r�  j  K�r�  j  K�r�  j$  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j(  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j(  X    Doubling doesn't cost extra usesr�  ee�r�  j  N�r�  j  X   Animar�  �r�  j-  ]r�  (K�K`K e�r�  jD  N�r�  jF  N�r�  j2  }r�  (j4  ]r�  X   Horser�  aj7  G@      j8  K j9  �u�r�  h6K�r�  jl  X   SPD1r�  �r�  j<  X   'Horse' in unit.tagsr�  �r�  jL  X   Firer�  �r�  eu}r�  (j�  K�hX   HeartOfDarknessr�  hX"   <orange>Heart Of Darkness</orange>r�  hX   +15 Hit. Dark affinity only.r�  j�  j  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j  X   HeartofDarknessr�  �r�  j  X   oranger�  �r�  eu}r�  (j�  K�hX	   ChurchKeyr�  hX
   Church Keyr�  hX   Opens any door in the church.r�  j�  j"  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j  K�r�  j$  ]r�  ]r�  (X   LoseUsesOnMiss (T/F)r�  j(  X   Lose uses even on missr�  ea�r�  X
   can_unlockr�  X7   region.nid.startswith('Door') and game.level.nid == '2'r�  �r�  eu}r�  (j�  K�hX   Steel Lancer�  hX   Steel Lancer�  hX   SPD -1.r�  j�  j?  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j  N�r�  j�  K�r�  j   K �r�  j  X   Lancer�  �r   j  K�r  j$  ]r  (]r  (X   LoseUsesOnMiss (T/F)r  j(  X   Lose uses even on missr  e]r  (X   OneLossPerCombat (T/F)r  j(  X    Doubling doesn't cost extra usesr  ee�r	  j  K �r
  j�  K�r  j�  K�r  jl  X   SPD1r  �r  eu}r  (j�  K�hX	   Axereaverr  hX	   Axereaverr  hX%   Reverses the weapon triangle. SPD -1.r  j�  j?  j�  �j�  }r  (j  Kj  Kuj�  ]r  j�  ]r  (j�  N�r  j�  N�r  j  N�r  j�  K�r  j   K �r  j  X   Lancer  �r  j  K�r  j$  ]r  (]r  (X   LoseUsesOnMiss (T/F)r   j(  X   Lose uses even on missr!  e]r"  (X   OneLossPerCombat (T/F)r#  j(  X    Doubling doesn't cost extra usesr$  ee�r%  X   reaverr&  N�r'  h6K�r(  j  K
�r)  j�  K�r*  j�  K�r+  j<  XU   target.get_weapon() and item_system.weapon_type(target, target.get_weapon()) == 'Axe'r,  �r-  eu}r.  (j�  K�hj  hj  hj  j�  j?  j�  �j�  }r/  (j  Kj  Kuj�  ]r0  j�  ]r1  (j  N�r2  j!  N�r3  j  K�r4  j$  j%  �r5  j+  K�r6  j-  j.  �r7  j0  N�r8  eu}r9  (j�  K�hX   Greatbowr:  hX	   Steel Bowr;  hX   SPD -2.r<  j�  j]  j�  �j�  }r=  (j  Kj  Kuj�  ]r>  j�  ]r?  (j�  N�r@  j�  N�rA  j  N�rB  j�  K�rC  j   K �rD  j  X   BowrE  �rF  j  K�rG  j$  ]rH  (]rI  (X   LoseUsesOnMiss (T/F)rJ  j(  X   Lose uses even on missrK  e]rL  (X   OneLossPerCombat (T/F)rM  j(  X    Doubling doesn't cost extra usesrN  ee�rO  j  K �rP  j�  K�rQ  j�  K�rR  jl  X   SPD2rS  �rT  j2  }rU  (j4  ]rV  X   FlyingrW  aj7  G@      j8  K j9  �u�rX  j<  X   'Flying' in unit.tagsrY  �rZ  eu}r[  (j�  K�hX   Mini_Bowr\  hX   Mini Bowr]  hX   SPD -1.r^  j�  j]  j�  �j�  }r_  (j  K
j  K
uj�  ]r`  j�  ]ra  (j�  N�rb  j�  N�rc  j  N�rd  j�  K�re  j   J�����rf  j  X   Bowrg  �rh  j  K
�ri  j$  ]rj  (]rk  (X   LoseUsesOnMiss (T/F)rl  j(  X   Lose uses even on missrm  e]rn  (X   OneLossPerCombat (T/F)ro  j(  X    Doubling doesn't cost extra usesrp  ee�rq  j  K�rr  h6K�rs  j�  K�rt  j�  K�ru  j2  }rv  (j4  ]rw  X   Flyingrx  aj7  G@      j8  K j9  �u�ry  jl  X   SPD1rz  �r{  j<  X   'Flying' in unit.tagsr|  �r}  eu}r~  (j�  K�hj  hj  hj  j�  j]  j�  �j�  }r  (j  Kj  Kuj�  ]r�  j�  ]r�  (j  N�r�  j!  N�r�  j  K�r�  j$  j%  �r�  j+  K�r�  j-  j.  �r�  j0  N�r�  eu}r�  (j�  K�hX	   Steel Axer�  hX	   Steel Axer�  hX   SPD -2.r�  j�  j  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j   J�����r�  j  K �r�  j  K�r�  j$  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j(  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j(  X    Doubling doesn't cost extra usesr�  ee�r�  j  N�r�  j  X   Axer�  �r�  h6K�r�  jl  X   SPD2r�  �r�  eu}r�  (j�  K�hX
   EmeraldAxer�  hX   Emerald Axer�  hX$   Doubles the weapon triangle. SPD -1.r�  j�  j  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j   J�����r�  j  K
�r�  j  K�r�  j$  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j(  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j(  X    Doubling doesn't cost extra usesr�  ee�r�  j  N�r�  j  X   Axer�  �r�  h6K�r�  X   double_triangler�  N�r�  jl  X   SPD1r�  �r�  X   warningr�  N�r�  eu}r�  (j�  K�hj  hj  hj  j�  j  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j  N�r�  j!  N�r�  j  K�r�  j$  j%  �r�  j+  K�r�  j-  j.  �r�  j0  N�r�  eu}r�  (j�  K�hj�  hj�  hj�  j�  j�  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j  N�r�  j�  K�r�  j   K �r�  j  j�  �r�  j  K�r�  j$  j  �r�  j  K �r�  j�  K�r�  j�  K�r�  jl  j  �r�  eu}r�  (j�  K�hX   SapphireLancer�  hX   Sapphire Lancer�  hX$   Doubles the weapon triangle. SPD -1.r�  j�  j�  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j  N�r�  j�  K�r�  j   K �r�  j  X   Lancer�  �r�  j  K�r�  j$  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j(  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j(  X    Doubling doesn't cost extra usesr�  ee�r�  h6K�r�  j  K
�r�  j�  K�r�  j�  K�r�  j�  N�r�  jl  X   SPD1r�  �r�  j�  N�r   eu}r  (j�  K�hj  hj  hj  j�  j�  j�  �j�  }r  (j  Kj  Kuj�  ]r  j�  ]r  (j  N�r  j!  N�r  j  K�r  j$  j%  �r  j+  K�r	  j-  j.  �r
  j0  N�r  eu}r  (j�  K�hX   Elixirr  hX   Elixirr  hX,   Fully recovers HP. Unit can act after using.r  j�  j�  j�  �j�  }r  (j  Kj  Kuj�  ]r  j�  ]r  (j  N�r  j!  N�r  j  K�r  j$  ]r  ]r  (X   LoseUsesOnMiss (T/F)r  j(  X   Lose uses even on missr  ea�r  j+  Kc�r  j-  ]r  (K`K�K�e�r  j0  N�r  eu}r  (j�  K�hX   Silver Swordr   hX   Silver Swordr!  hhj�  j�  j�  �j�  }r"  (j  K
j  K
uj�  ]r#  j�  ]r$  (j�  N�r%  j�  N�r&  j�  K�r'  j�  K�r(  j�  K�r)  j   K
�r*  j  K �r+  j  N�r,  j  X   Swordr-  �r.  j  K
�r/  j$  ]r0  (]r1  (X   LoseUsesOnMiss (T/F)r2  j(  X   Lose uses even on missr3  e]r4  (X   OneLossPerCombat (T/F)r5  j(  X    Doubling doesn't cost extra usesr6  ee�r7  eu}r8  (j�  K�hX   Silver Lancer9  hX   Silver Lancer:  hhj�  j�  j�  �j�  }r;  (j  K
j  K
uj�  ]r<  j�  ]r=  (j�  N�r>  j�  N�r?  j�  K�r@  j�  K�rA  j�  K�rB  j   K�rC  j  K �rD  j  N�rE  j  X   LancerF  �rG  j  K
�rH  j$  ]rI  (]rJ  (X   LoseUsesOnMiss (T/F)rK  j(  X   Lose uses even on missrL  e]rM  (X   OneLossPerCombat (T/F)rN  j(  X    Doubling doesn't cost extra usesrO  ee�rP  eu}rQ  (j�  K�hX   TheReclaimerrR  hX   <blue>The Reclaimer<blue>rS  hX6   Cavalier line only.
50% Lifelink. Effective Vs. Armor.rT  j�  j�  j�  �j�  }rU  j�  ]rV  j�  ]rW  (j�  N�rX  j�  N�rY  j�  K�rZ  j�  K�r[  j�  K�r\  j   J�����r]  j  K
�r^  j  N�r_  j  X   Lancer`  �ra  X	   prf_classrb  ]rc  (X   Cavalierrd  X   Paladinre  X   Great_Knightrf  e�rg  j2  }rh  (j4  ]ri  X   Armorrj  aj7  G@      j8  K j9  �u�rk  j  X   bluerl  �rm  X   lifelinkrn  G?�      �ro  eu}rp  (j�  K�hX
   TheDefilerrq  hX   <blue>The Defiler</blue>rr  hXF   Knight line only.
Inflicts half damage on miss. Effective Vs. Clergy. rs  j�  j  j�  �j�  }rt  j�  ]ru  j�  ]rv  (j�  N�rw  j�  N�rx  j�  K�ry  j�  K�rz  j�  K�r{  j   J�����r|  j  K�r}  j  N�r~  j  X   Axer  �r�  jb  ]r�  (X   Knightr�  X   Generalr�  X   Great_Knightr�  e�r�  j2  }r�  (j4  ]r�  X   Clergyr�  aj7  G@      j8  K j9  �u�r�  j  X   bluer�  �r�  X   damage_on_missr�  G?�      �r�  eu}r�  (j�  K�hj�  hj�  hhj�  j  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j   K �r�  j  N�r�  j  j�  �r�  j  K �r�  eu}r�  (j�  K�hX   MultiPhysicr�  hX   Multiphysicr�  hXT   Restores an ally's HP from up to (MAG * 2) tiles away. Casts twice. Chapter Uses: 2.r�  j�  j  j�  �j�  }r�  (X   c_usesr�  KX   starting_c_usesr�  Kuj�  ]r�  j�  ]r�  (X   spellr�  N�r�  j!  N�r�  j  X   Staffr�  �r�  jD  N�r�  h6K�r�  hK!�r�  j-  ]r�  (K`K�K�e�r�  X
   magic_healr�  K�r�  j�  K�r�  X   max_equation_ranger�  X   MAGIC_RANGEr�  �r�  jF  N�r�  j�  K�r�  j$  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j(  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j(  X    Doubling doesn't cost extra usesr�  ee�r�  X   multi_targetr�  K�r�  X   allow_same_targetr�  N�r�  jL  X   Healr�  �r�  X   ignore_line_of_sightr�  N�r�  eu}r�  (j�  K�hj   hj!  hhj�  j4  j�  �j�  }r�  (j  K
j  K
uj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j   K
�r�  j  K �r�  j  N�r�  j  j-  �r�  j  K
�r�  j$  j0  �r�  eu}r�  (j�  K�hj�  hj�  hj�  j�  jJ  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j   J�����r�  j  K �r�  j  N�r�  j  j�  �r�  j  K�r�  j$  j�  �r�  j�  N�r�  h6K�r�  jl  j�  �r�  eu}r�  (j�  K�hX   Hand Axer�  hX   Hand Axer�  hX   Cannot Counter. SPD -3.r�  j�  j_  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j   J�����r�  j  K �r�  j  K�r�  j$  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j(  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j(  X    Doubling doesn't cost extra usesr�  ee�r�  j  N�r�  j  X   Axer�  �r   j�  N�r  h6K�r  jl  X   SPD3r  �r  eu}r  (j�  K�hj$  hj%  hhj�  js  j�  �j�  }r  j�  ]r  j�  ]r  (j�  N�r	  j�  N�r
  j�  K�r  j�  K�r  j�  K�r  j   K
�r  j  N�r  j  j0  �r  j2  }r  (j4  j5  j7  G@      j8  K j9  �u�r  j  K �r  j<  j=  �r  eu}r  (j�  K�hj�  hj�  hhj�  js  j�  �j�  }r  j�  ]r  j�  ]r  (j�  N�r  j�  N�r  j�  K�r  j�  K�r  j�  K�r  j   K
�r  j  N�r  j  j  �r   j  K �r!  eu}r"  (j�  K�hX   EtherealBlader#  hX   Ethereal Blader$  hX   Targets RES.r%  j�  js  j�  �j�  }r&  (j  K
j  K
uj�  ]r'  j�  ]r(  (j�  N�r)  j�  N�r*  j�  K�r+  j�  K�r,  j�  K�r-  j   K�r.  j  K�r/  j  K
�r0  j$  ]r1  (]r2  (X   LoseUsesOnMiss (T/F)r3  j(  X   Lose uses even on missr4  e]r5  (X   OneLossPerCombat (T/F)r6  j(  X    Doubling doesn't cost extra usesr7  ee�r8  j  N�r9  j  X   Swordr:  �r;  X   alternate_resist_formular<  X   MAGIC_DEFENSEr=  �r>  h6K�r?  eu}r@  (j�  K�hj�  hj�  hj�  j�  j�  j�  �j�  }rA  (j  Kj  Kuj�  ]rB  j�  ]rC  (j�  N�rD  j�  N�rE  j  N�rF  j�  K�rG  j   K �rH  j  j�  �rI  j  K�rJ  j$  j  �rK  j  K �rL  j�  K�rM  j�  K�rN  jl  j  �rO  eu}rP  (j�  K�hX   WardensCleaverrQ  hX   <blue>Warden's Cleaver<blue>rR  hX&   While equipped unit cannot be doubled.rS  j�  j�  j�  �j�  }rT  j�  ]rU  j�  ]rV  (j�  N�rW  j�  N�rX  j�  K�rY  j�  K�rZ  j�  K�r[  j   K�r\  j  K
�r]  j  N�r^  j  X   Axer_  �r`  j  X   bluera  �rb  jl  X   Wardenrc  �rd  eu}re  (j�  K�hX
   BloodLancerf  hX   Blood Lancerg  hX   Has Lifelink. SPD -1.rh  j�  j�  j�  �j�  }ri  (j  K
j  K
uj�  ]rj  j�  ]rk  (j�  N�rl  j�  N�rm  j  N�rn  j�  K�ro  j   J�����rp  j  X   Lancerq  �rr  j  K
�rs  j$  ]rt  (]ru  (X   LoseUsesOnMiss (T/F)rv  j(  X   Lose uses even on missrw  e]rx  (X   OneLossPerCombat (T/F)ry  j(  X    Doubling doesn't cost extra usesrz  ee�r{  j  K�r|  j�  K�r}  j�  K�r~  jn  G?�      �r  h6K�r�  jl  X   SPD1r�  �r�  eu}r�  (j�  K�hj#  hj$  hj%  j�  j�  j�  �j�  }r�  (j  K
j  K
uj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j   K�r�  j  K�r�  j  K
�r�  j$  j1  �r�  j  N�r�  j  j:  �r�  j<  j=  �r�  h6K�r�  eu}r�  (j�  K�hX   AngelFeatherr�  hX   <yellow>Angel Feather</yellow>r�  hX&   Gives unit +10% growths. Cannot stack.r�  j�  j�  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j  N�r�  j!  N�r�  j  K�r�  j$  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j(  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j(  X    Doubling doesn't cost extra usesr�  ee�r�  X   usable_in_baser�  N�r�  j  X   yellowr�  �r�  j-  ]r�  (K�K�K�e�r�  jH  X   AngelFeatherr�  �r�  j�  K �r�  j�  K �r�  X   eval_availabler�  X#   not has_skill(unit, 'AngelFeather')r�  �r�  X   event_on_hitr�  X   Global AngelFeatherr�  �r�  eu}r�  (j�  K�hX   HowlingBlader�  hX   <blue>Howling Blade</blue>r�  hX4   Grants Distant Counter. Deals magic damage at range.r�  j�  j�  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j   K�r�  j  K
�r�  j  N�r�  j  X   Swordr�  �r�  j  X   bluer�  �r�  X   magic_at_ranger�  N�r�  jl  X   DistantCounterr�  �r�  eu}r�  (j�  K�hX   Macer�  hX   Macer�  hX   Cannot be Countered. SPD -2.r�  j�  j�  j�  �j�  }r�  (j  K
j  K
uj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j  N�r�  j�  K�r�  j   J�����r�  j  X   Axer�  �r�  j  K
�r�  j$  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j(  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j(  X    Doubling doesn't cost extra usesr�  ee�r�  j  K�r�  h6K�r�  j�  K�r�  j�  K�r�  X   cannot_be_counteredr�  N�r�  jl  X   SPD2r�  �r�  eu}r�  (j�  K�hj�  hj�  hj�  j�  j�  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j  N�r�  j!  N�r�  j  K�r�  j$  j�  �r�  j�  N�r�  j  j�  �r�  j-  j�  �r�  jH  j�  �r�  j�  K �r�  j�  K �r�  j�  j�  �r�  j�  j�  �r�  eu}r�  (j�  K�hX   LunarArcr�  hX	   Lunar Arcr�  hX%   Ignores DEF and grants Close Counter.r�  j�  j�  j�  �j�  }r   j�  ]r  j�  ]r  (jl  X   CloseCounterr  �r  j�  N�r  j�  N�r  j  N�r  j�  K�r  j<  X   ZEROr	  �r
  j   K�r  j  X   Bowr  �r  j  K
�r  j�  K�r  j�  K�r  j2  }r  (j4  ]r  X   Flyingr  aj7  G@      j8  K j9  �u�r  j<  X   'Flying' in unit.tagsr  �r  eu}r  (j�  K�hj�  hj�  hj�  j�  j�  j�  �j�  }r  (j  Kj  Kuj�  ]r  j�  ]r  (j�  N�r  j�  N�r  j�  K�r  j�  K�r  j�  K�r  j   J�����r   j  K
�r!  j  K�r"  j$  j�  �r#  j  N�r$  j  j�  �r%  h6K�r&  j�  N�r'  jl  j�  �r(  j�  N�r)  eu}r*  (j�  K�hj�  hj�  hj�  j�  j�  j�  �j�  }r+  (j  Kj  Kuj�  ]r,  j�  ]r-  (j  N�r.  j!  N�r/  j  K�r0  j$  j�  �r1  j�  N�r2  j  j�  �r3  j-  j�  �r4  jH  j�  �r5  j�  K �r6  j�  K �r7  j�  j�  �r8  j�  j�  �r9  eu}r:  (j�  K�hX   Flairr;  hX   <purple>Flair</purple>r<  hX`   Elwynn Only.
Steals any unequipped item from target.
Can't miss, double, kill, or target bosses.r=  j�  h�j�  �j�  }r>  (j  K
j  K
X   target_itemr?  Nuj�  ]r@  j�  ]rA  (j�  N�rB  j�  N�rC  j�  K�rD  j�  K�rE  j�  K�rF  j  K
�rG  j$  ]rH  (]rI  (X   LoseUsesOnMiss (T/F)rJ  j(  X   Lose uses even on missrK  e]rL  (X   OneLossPerCombat (T/F)rM  j(  X    Doubling doesn't cost extra usesrN  ee�rO  j  N�rP  j  X   LightrQ  �rR  j-  ]rS  (K�K�K e�rT  jD  N�rU  j�  ]rV  X   ElwynnrW  a�rX  j  X   purplerY  �rZ  jH  X	   NonLethalr[  �r\  X   stealr]  N�r^  jL  X   Glimmerr_  �r`  X   eval_target_restrict_2ra  X   'Boss' not in target.tagsrb  �rc  eu}rd  (j�  K�hX   Shovere  hX   <green>Shove</>rf  hX!   Free action. Shove target 1 tile.rg  j�  j�  j�  �j�  }rh  (X   cooldownri  K X   starting_cooldownrj  Kuj�  ]rk  j�  ]rl  (X   target_unitrm  N�rn  j�  K�ro  j�  K�rp  ja  X   target.team == 'player'rq  �rr  j0  N�rs  ji  K�rt  X   shove_on_end_combatru  K�rv  eu}rw  (j�  K�hX   Bolsterrx  hX   Bolsterry  hX/   Grants max HP +5 for 2 turns and restores 5 HP.rz  j�  j�  j�  �j�  }r{  j�  ]r|  j�  ]r}  (j!  N�r~  j�  K�r  j�  K�r�  X   map_hit_sfxr�  X   HealInitr�  �r�  j-  ]r�  (K K�Ke�r�  j�  X   Global SkillCorridonBolsterr�  �r�  hK�r�  eu}r�  (j�  K�hX   Repairr�  hX   Repairr�  hXE   Repairs selected weapon in target's inventory. Does not affect tomes.r�  j�  j�  j�  �j�  }r�  j?  Nsj�  ]r�  j�  ]r�  (j�  K�r�  j�  K�r�  j!  N�r�  X   repairr�  N�r�  X   never_use_battle_animationr�  N�r�  X   map_cast_sfxr�  X   Hammerner�  �r�  j-  ]r�  (K!K�Kke�r�  hK�r�  eu}r�  (j�  K�hje  hjf  hjg  j�  j�  j�  �j�  }r�  (ji  K jj  Kuj�  ]r�  j�  ]r�  (jm  N�r�  j�  K�r�  j�  K�r�  ja  jq  �r�  j0  N�r�  ji  K�r�  ju  K�r�  eu}r�  (j�  K�hje  hjf  hjg  j�  j  j�  �j�  }r�  (ji  K jj  Kuj�  ]r�  j�  ]r�  (jm  N�r�  j�  K�r�  j�  K�r�  ja  jq  �r�  j0  N�r�  ji  K�r�  ju  K�r�  eu}r�  (j�  K�hje  hjf  hjg  j�  j]  j�  �j�  }r�  (ji  K jj  Kuj�  ]r�  j�  ]r�  (jm  N�r�  j�  K�r�  j�  K�r�  ja  jq  �r�  j0  N�r�  ji  K�r�  ju  K�r�  eu}r�  (j�  K�hje  hjf  hjg  j�  j?  j�  �j�  }r�  (ji  K jj  Kuj�  ]r�  j�  ]r�  (jm  N�r�  j�  K�r�  j�  K�r�  ja  jq  �r�  j0  N�r�  ji  K�r�  ju  K�r�  eu}r�  (j�  K�hje  hjf  hjg  j�  j"  j�  �j�  }r�  (ji  K jj  Kuj�  ]r�  j�  ]r�  (jm  N�r�  j�  K�r�  j�  K�r�  ja  jq  �r�  j0  N�r�  ji  K�r�  ju  K�r�  eu}r�  (j�  K�hX   Healr�  hX   <blue>Heal</>r�  hX   Restores (User's MAG * 2) HP.r�  j�  j"  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j!  N�r�  j�  K�r�  j�  K�r�  j  X   Staffr�  �r�  hK�r�  h6K�r�  j-  ]r�  (K`K�K�e�r�  jF  N�r�  jL  X   Healr�  �r�  X   equation_healr�  X   MAG2r�  �r�  eu}r�  (j�  K�hX   Exchanger�  hX   Exchanger�  hX:   Free action. Trade with allies up to (MAG * 2) tiles away.r�  j�  j"  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j!  N�r�  j�  K�r�  j�  X   MAG2r�  �r�  j-  ]r�  (K�K�Ke�r�  X   trader�  N�r�  X   menu_after_combatr�  N�r�  j0  N�r�  j�  N�r�  j�  X   Warpr�  �r 	  X   map_cast_animr	  X   Warpr	  �r	  eu}r	  (j�  K�hje  hjf  hjg  j�  j  j�  �j�  }r	  (ji  K jj  Kuj�  ]r	  j�  ]r	  (jm  N�r	  j�  K�r		  j�  K�r
	  ja  jq  �r	  j0  N�r	  ji  K�r	  ju  K�r	  eu}r	  (j�  K�hje  hjf  hjg  j�  h�j�  �j�  }r	  (ji  K jj  Kuj�  ]r	  j�  ]r	  (jm  N�r	  j�  K�r	  j�  K�r	  ja  jq  �r	  j0  N�r	  ji  K�r	  ju  K�r	  eu}r	  (j�  K�hje  hjf  hjg  j�  h�j�  �j�  }r	  (ji  K jj  Kuj�  ]r	  j�  ]r	  (jm  N�r	  j�  K�r	  j�  K�r 	  ja  jq  �r!	  j0  N�r"	  ji  K�r#	  ju  K�r$	  eu}r%	  (j�  K�hX   Treatr&	  hX   Treatr'	  hX*   Free Action. Restores (User's MAG + 2) HP.r(	  j�  h�j�  �j�  }r)	  (ji  K jj  Kuj�  ]r*	  j�  ]r+	  (j!  N�r,	  j�  N�r-	  j�  K�r.	  j�  K�r/	  j�  X   TREATr0	  �r1	  j0  N�r2	  hK
�r3	  ji  K�r4	  eu}r5	  (j�  K�hje  hjf  hjg  j�  h�j�  �j�  }r6	  (ji  K jj  Kuj�  ]r7	  j�  ]r8	  (jm  N�r9	  j�  K�r:	  j�  K�r;	  ja  jq  �r<	  j0  N�r=	  ji  K�r>	  ju  K�r?	  eu}r@	  (j�  K�hX   TriprA	  hX   TriprB	  hX/   Free Action. Set target's move to 0 for 1 turn.rC	  j�  h�j�  �j�  }rD	  (ji  K jj  Kuj�  ]rE	  j�  ]rF	  (j�  N�rG	  j�  K�rH	  j�  K�rI	  jH  X   TrippedrJ	  �rK	  j0  N�rL	  ji  K�rM	  eu}rN	  (j�  K�hje  hjf  hjg  j�  h�j�  �j�  }rO	  (ji  K jj  Kuj�  ]rP	  j�  ]rQ	  (jm  N�rR	  j�  K�rS	  j�  K�rT	  ja  jq  �rU	  j0  N�rV	  ji  K�rW	  ju  K�rX	  eu}rY	  (j�  M hje  hjf  hjg  j�  hjj�  �j�  }rZ	  (ji  K jj  Kuj�  ]r[	  j�  ]r\	  (jm  N�r]	  j�  K�r^	  j�  K�r_	  ja  jq  �r`	  j0  N�ra	  ji  K�rb	  ju  K�rc	  eu}rd	  (j�  Mhje  hjf  hjg  j�  hj�  �j�  }re	  (ji  K jj  Kuj�  ]rf	  j�  ]rg	  (jm  N�rh	  j�  K�ri	  j�  K�rj	  ja  jq  �rk	  j0  N�rl	  ji  K�rm	  ju  K�rn	  euehE]ro	  (}rp	  (j�  K�hX   HeartofFirerq	  j�  hj�  }rr	  X   initiator_nidrs	  NX   subskillrt	  Nu}ru	  (j�  K�hX   HeartofGalesrv	  j�  hjj�  }rw	  js	  Njt	  Nu}rx	  (j�  K�hX   HeartofLightningry	  j�  h�j�  }rz	  js	  Njt	  Nu}r{	  (j�  K�hX   HeartofFrostr|	  j�  h�j�  }r}	  js	  Njt	  Nu}r~	  (j�  MhX   HeartofRadiancer	  j�  h�j�  }r�	  js	  Njt	  Nu}r�	  (j�  M
hX   HeartofDarknessr�	  j�  j  j�  }r�	  js	  Njt	  Nu}r�	  (j�  MhX   SPD1r�	  j�  j?  j�  }r�	  js	  Njt	  Nu}r�	  (j�  MhX   SPD2r�	  j�  j]  j�  }r�	  js	  Njt	  Nu}r�	  (j�  M%hj�	  j�  j  j�  }r�	  js	  Njt	  Nu}r�	  (j�  M+hj�	  j�  j�  j�  }r�	  js	  Njt	  Nu}r�	  (j�  MDhj�	  j�  jJ  j�  }r�	  js	  Njt	  Nu}r�	  (j�  MGhX   SPD3r�	  j�  j_  j�  }r�	  js	  Njt	  Nu}r�	  (j�  MLhj�	  j�  j�  j�  }r�	  js	  Njt	  Nu}r�	  (j�  MQhX   Wardenr�	  j�  j�  j�  }r�	  js	  Njt	  Nu}r�	  (j�  MUhX   DistantCounterr�	  j�  j�  j�  }r�	  js	  Njt	  Nu}r�	  (j�  MYhX   CloseCounterr�	  j�  j�  j�  }r�	  js	  Njt	  Nu}r�	  (j�  K�hX   NoEXPr�	  j�  hj�  }r�	  js	  Njt	  Nu}r�	  (j�  K�hX   Lootr�	  j�  hj�  }r�	  js	  Njt	  Nu}r�	  (j�  K�hX   Shover�	  j�  hj�  }r�	  X   ability_item_uidr�	  Msjs	  Njt	  Nu}r�	  (j�  K�hX   Flailr�	  j�  hj�  }r�	  js	  Njt	  Nu}r�	  (j�  K�hj�	  j�  hjj�  }r�	  js	  Njt	  Nu}r�	  (j�  K�hj�	  j�  hjj�  }r�	  js	  Njt	  Nu}r�	  (j�  K�hj�	  j�  hjj�  }r�	  j�	  M sjs	  Njt	  Nu}r�	  (j�  K�hX   RiskyBusinessr�	  j�  hjj�  }r�	  js	  Njt	  Nu}r�	  (j�  K�hj�	  j�  h�j�  }r�	  js	  Njt	  Nu}r�	  (j�  K�hj�	  j�  h�j�  }r�	  js	  Njt	  Nu}r�	  (j�  K�hj�	  j�  h�j�  }r�	  j�	  K�sjs	  Njt	  Nu}r�	  (j�  K�hX   Bungler�	  j�  h�j�  }r�	  js	  Njt	  Nu}r�	  (j�  K�hj�	  j�  h�j�  }r�	  js	  Njt	  Nu}r�	  (j�  K�hj�	  j�  h�j�  }r�	  js	  Njt	  Nu}r�	  (j�  K�hj�	  j�  h�j�  }r�	  j�	  K�sjs	  Njt	  Nu}r�	  (j�  K�hX   Tripr�	  j�  h�j�  }r�	  j�	  K�sjs	  Njt	  Nu}r�	  (j�  K�hj�	  j�  h�j�  }r�	  js	  Njt	  Nu}r�	  (j�  M hj�	  j�  h�j�  }r�	  js	  Njt	  Nu}r�	  (j�  MhX	   Protectorr�	  j�  h�j�  }r�	  js	  Njt	  Nu}r�	  (j�  Mhj�	  j�  h�j�  }r�	  j�	  K�sjs	  Njt	  Nu}r�	  (j�  MhX   ElwynnSkillr�	  j�  h�j�  }r�	  j�	  K�sjs	  Njt	  Nu}r�	  (j�  Mhj�	  j�  h�j�  }r�	  js	  Njt	  Nu}r�	  (j�  Mhj�	  j�  h�j�  }r�	  js	  Njt	  Nu}r�	  (j�  Mhj�	  j�  h�j�  }r�	  js	  Njt	  Nu}r�	  (j�  Mhj�	  j�  h�j�  }r�	  j�	  K�sjs	  Njt	  Nu}r�	  (j�  M	hX   Potshotr�	  j�  h�j�  }r�	  js	  Njt	  Nu}r�	  (j�  Mhj�	  j�  j  j�  }r�	  js	  Njt	  Nu}r�	  (j�  Mhj�	  j�  j  j�  }r�	  js	  Njt	  Nu}r�	  (j�  Mhj�	  j�  j  j�  }r�	  js	  Njt	  Nu}r�	  (j�  Mhj�	  j�  j  j�  }r�	  j�	  K�sjs	  Njt	  Nu}r�	  (j�  MhX   Enthrallr�	  j�  j  j�  }r�	  js	  Njt	  Nu}r�	  (j�  Mhj�	  j�  j"  j�  }r�	  js	  Njt	  Nu}r�	  (j�  Mhj�	  j�  j"  j�  }r�	  js	  Njt	  Nu}r�	  (j�  Mhj�	  j�  j"  j�  }r�	  j�	  K�sjs	  Njt	  Nu}r�	  (j�  MhX   DivineConduitr�	  j�  j"  j�  }r�	  j�	  K�sjs	  Njt	  Nu}r�	  (j�  MhX   Exchanger�	  j�  j"  j�  }r�	  (X   charger�	  KX   total_charger�	  Kj�	  K�ujs	  Njt	  Nu}r�	  (j�  Mhj�	  j�  j?  j�  }r�	  js	  Njt	  Nu}r�	  (j�  Mhj�	  j�  j?  j�  }r�	  js	  Njt	  Nu}r�	  (j�  Mhj�	  j�  j?  j�  }r�	  js	  Njt	  Nu}r�	  (j�  Mhj�	  j�  j?  j�  }r�	  j�	  K�sjs	  Njt	  Nu}r�	  (j�  MhX
   Instructorr�	  j�  j?  j�  }r 
  js	  Njt	  Nu}r
  (j�  Mhj�	  j�  j]  j�  }r
  js	  Njt	  Nu}r
  (j�  Mhj�	  j�  j]  j�  }r
  js	  Njt	  Nu}r
  (j�  Mhj�	  j�  j]  j�  }r
  js	  Njt	  Nu}r
  (j�  Mhj�	  j�  j]  j�  }r
  j�	  K�sjs	  Njt	  Nu}r	
  (j�  M hj�	  j�  j  j�  }r

  js	  Njt	  Nu}r
  (j�  M!hj�	  j�  j  j�  }r
  js	  Njt	  Nu}r
  (j�  M"hj�	  j�  j  j�  }r
  js	  Njt	  Nu}r
  (j�  M#hj�	  j�  j  j�  }r
  j�	  K�sjs	  Njt	  Nu}r
  (j�  M$hX   Nemophilistr
  j�  j  j�  }r
  js	  Njt	  Nu}r
  (j�  M&hj�	  j�  j�  j�  }r
  js	  Njt	  Nu}r
  (j�  M'hj�	  j�  j�  j�  }r
  js	  Njt	  Nu}r
  (j�  M(hj�	  j�  j�  j�  }r
  js	  Njt	  Nu}r
  (j�  M)hj�	  j�  j�  j�  }r
  j�	  K�sjs	  Njt	  Nu}r
  (j�  M*hX   Beachcomberr
  j�  j�  j�  }r
  js	  Njt	  Nu}r
  (j�  M,hj�	  j�  j�  j�  }r 
  js	  Njt	  Nu}r!
  (j�  M-hj�	  j�  j�  j�  }r"
  js	  Njt	  Nu}r#
  (j�  M.hj�	  j�  j�  j�  }r$
  js	  Njt	  Nu}r%
  (j�  M/hj�	  j�  j�  j�  }r&
  j�	  K�sjs	  Njt	  Nu}r'
  (j�  M0hX   Bolsterr(
  j�  j�  j�  }r)
  j�	  K�sjs	  Njt	  Nu}r*
  (j�  M1hX   Repairr+
  j�  j�  j�  }r,
  j�	  K�sjs	  Njt	  Nu}r-
  (j�  M2hj�	  j�  j�  j�  }r.
  js	  Njt	  Nu}r/
  (j�  M3hj�	  j�  j�  j�  }r0
  js	  Njt	  Nu}r1
  (j�  M4hj�	  j�  j�  j�  }r2
  js	  Njt	  Nu}r3
  (j�  M5hX   AspiringLeaderr4
  j�  j�  j�  }r5
  js	  Njt	  Nu}r6
  (j�  M6hX   Cantor7
  j�  j�  j�  }r8
  js	  Njt	  Nu}r9
  (j�  M7hj�	  j�  j  j�  }r:
  js	  Njt	  Nu}r;
  (j�  M8hj�	  j�  j  j�  }r<
  js	  Njt	  Nu}r=
  (j�  M9hX   Anticritr>
  j�  j  j�  }r?
  js	  Njt	  Nu}r@
  (j�  M:hX   DivineBlessingrA
  j�  j  j�  }rB
  js	  Njt	  Nu}rC
  (j�  M;hj�	  j�  j  j�  }rD
  js	  Njt	  Nu}rE
  (j�  M<hj�	  j�  j  j�  }rF
  js	  Njt	  Nu}rG
  (j�  M=hX   FlyingrH
  j�  j  j�  }rI
  js	  Njt	  Nu}rJ
  (j�  M>hX	   BloodlustrK
  j�  j  j�  }rL
  js	  Njt	  Nu}rM
  (j�  M?hj�	  j�  j4  j�  }rN
  js	  Njt	  Nu}rO
  (j�  M@hj�	  j�  j4  j�  }rP
  js	  Njt	  Nu}rQ
  (j�  MAhj�	  j�  jJ  j�  }rR
  js	  Njt	  Nu}rS
  (j�  MBhj�	  j�  jJ  j�  }rT
  js	  Njt	  Nu}rU
  (j�  MChjH
  j�  jJ  j�  }rV
  js	  Njt	  Nu}rW
  (j�  MEhj�	  j�  j_  j�  }rX
  js	  Njt	  Nu}rY
  (j�  MFhj�	  j�  j_  j�  }rZ
  js	  Njt	  Nu}r[
  (j�  MHhj�	  j�  js  j�  }r\
  js	  Njt	  Nu}r]
  (j�  MIhj�	  j�  js  j�  }r^
  js	  Njt	  Nu}r_
  (j�  MJhj�	  j�  j�  j�  }r`
  js	  Njt	  Nu}ra
  (j�  MKhj�	  j�  j�  j�  }rb
  js	  Njt	  Nu}rc
  (j�  MMhj�	  j�  j�  j�  }rd
  js	  Njt	  Nu}re
  (j�  MNhj�	  j�  j�  j�  }rf
  js	  Njt	  Nu}rg
  (j�  MOhj>
  j�  j�  j�  }rh
  js	  Njt	  Nu}ri
  (j�  MPhj7
  j�  j�  j�  }rj
  js	  Njt	  Nu}rk
  (j�  MRhj�	  j�  j�  j�  }rl
  js	  Njt	  Nu}rm
  (j�  MShj�	  j�  j�  j�  }rn
  js	  Njt	  Nu}ro
  (j�  MThj>
  j�  j�  j�  }rp
  js	  Njt	  Nu}rq
  (j�  MVhj�	  j�  j�  j�  }rr
  js	  Njt	  Nu}rs
  (j�  MWhj�	  j�  j�  j�  }rt
  js	  Njt	  Nu}ru
  (j�  MXhj>
  j�  j�  j�  }rv
  js	  Njt	  Nu}rw
  (j�  MZhX   Avoid10rx
  j�  Nj�  }ry
  js	  Njt	  Nu}rz
  (j�  M[hX   Avoid5r{
  j�  hjj�  }r|
  js	  Njt	  Nu}r}
  (j�  M\hj{
  j�  h�j�  }r~
  js	  Njt	  Nu}r
  (j�  M]hj{
  j�  h�j�  }r�
  js	  Njt	  Nu}r�
  (j�  M^hj{
  j�  h�j�  }r�
  js	  Njt	  Nu}r�
  (j�  M_hj{
  j�  h�j�  }r�
  js	  Njt	  Nu}r�
  (j�  M`hjx
  j�  j  j�  }r�
  js	  Njt	  Nu}r�
  (j�  Mahj{
  j�  j"  j�  }r�
  js	  Njt	  Nu}r�
  (j�  Mbhj{
  j�  j?  j�  }r�
  js	  Njt	  Nu}r�
  (j�  Mchj{
  j�  j]  j�  }r�
  js	  Njt	  Nu}r�
  (j�  Mdhj{
  j�  j  j�  }r�
  js	  Njt	  Nu}r�
  (j�  Mehj{
  j�  j�  j�  }r�
  js	  Njt	  Nu}r�
  (j�  Mfhj{
  j�  j�  j�  }r�
  js	  Njt	  Nu}r�
  (j�  Mghj{
  j�  j�  j�  }r�
  js	  Njt	  Nu}r�
  (j�  Mhhjx
  j�  j  j�  }r�
  js	  Njt	  Nu}r�
  (j�  Mihj{
  j�  Nj�  }r�
  js	  Njt	  Nu}r�
  (j�  Mjhj{
  j�  Nj�  }r�
  js	  Njt	  Nu}r�
  (j�  Mkhj{
  j�  j�  j�  }r�
  js	  Njt	  Nu}r�
  (j�  MlhX   SuppressiveFirer�
  j�  j]  j�  }r�
  js	  Njt	  Nu}r�
  (j�  MmhX   Burningr�
  j�  Nj�  }r�
  js	  Njt	  Nu}r�
  (j�  Mnhj�
  j�  j�  j�  }r�
  js	  Njt	  Nu}r�
  (j�  Mohj�
  j�  h�j�  }r�
  js	  Njt	  Nu}r�
  (j�  Mphj�
  j�  j  j�  }r�
  js	  Njt	  Nu}r�
  (j�  Mqhj�
  j�  Nj�  }r�
  js	  Njt	  Nu}r�
  (j�  Mrhj�
  j�  Nj�  }r�
  js	  Njt	  Nu}r�
  (j�  Mshj�
  j�  j�  j�  }r�
  js	  Njt	  Nu}r�
  (j�  Mthj�
  j�  j  j�  }r�
  js	  Njt	  Nu}r�
  (j�  Muhj�
  j�  j�  j�  }r�
  js	  Njt	  NueX   terrain_status_registryr�
  }r�
  (KKX   Avoid10r�
  �r�
  MZKKX   Avoid5r�
  �r�
  M[KKj�
  �r�
  M\KKj�
  �r�
  M]KK	j�
  �r�
  M^KKj�
  �r�
  M_KKj�
  �r�
  M`KKj�
  �r�
  MaKK
j�
  �r�
  MbKK	j�
  �r�
  McK	Kj�
  �r�
  MdKKj�
  �r�
  MeKKj�
  �r�
  MfKK	j�
  �r�
  MgKKj�
  �r�
  MhKKj�
  �r�
  MiKKj�
  �r�
  MjKKj�
  �r�
  MkKKX   Burningr�
  �r�
  MmKKX   Burningr�
  �r�
  MnKKj�
  �r�
  MoKKj�
  �r�
  MpKKX   Burningr�
  �r�
  MqKKX   Burningr�
  �r�
  MrKKj�
  �r�
  MsK	Kj�
  �r�
  MtKK	j�
  �r�
  MuuX   regionsr�
  ]r�
  (}r�
  (hX   Door1r�
  X   region_typer�
  capp.events.regions
RegionType
r�
  X   eventr�
  �r�
  Rr�
  hKK�r�
  X   sizer�
  ]r�
  (KKeX   sub_nidr�
  X   Doorr�
  X	   conditionr�
  X   unit.can_unlock(region)r�
  X	   time_leftr�
  NX	   only_oncer�
  �X   interrupt_mover�
  �j�  }r�
  u}r�
  (hX   Fire1r�
  j�
  j�
  X   statusr�
  �r�
  Rr�
  hj�  j�
  KK�r�
  j�
  j�
  j�
  X   Truer�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_0r�
  j�
  j�
  hKK	�r�
  j�
  KK�r�
  j�
  X   Garvey_Shotr�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_1r�
  j�
  j�
  hKK	�r�
  j�
  KK�r�
  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_2r�
  j�
  j�
  hKK
�r�
  j�
  KK�r   j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r  u}r  (hX   Garve_3r  j�
  j�
  hKK	�r  j�
  KK�r  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r  u}r  (hX   Garve_4r  j�
  j�
  hKK
�r	  j�
  KK�r
  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r  u}r  (hX   Garve_5r  j�
  j�
  hKK�r  j�
  KK�r  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r  u}r  (hX   Garve_6r  j�
  j�
  hKK�r  j�
  KK�r  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r  u}r  (hX   Garve_7r  j�
  j�
  hKK�r  j�
  KK�r  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r  u}r  (hX   Garve_8r  j�
  j�
  hKK�r  j�
  KK�r  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r  u}r   (hX   Garve_9r!  j�
  j�
  hKK�r"  j�
  KK�r#  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r$  u}r%  (hX   Garve_10r&  j�
  j�
  hKK	�r'  j�
  KK�r(  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r)  u}r*  (hX   Garve_11r+  j�
  j�
  hKK
�r,  j�
  KK�r-  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r.  u}r/  (hX   Garve_12r0  j�
  j�
  hKK�r1  j�
  KK�r2  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r3  u}r4  (hX   Garve_13r5  j�
  j�
  hKK	�r6  j�
  KK�r7  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r8  u}r9  (hX   Garve_14r:  j�
  j�
  hKK�r;  j�
  KK�r<  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r=  u}r>  (hX   Garve_15r?  j�
  j�
  hKK�r@  j�
  KK�rA  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }rB  u}rC  (hj�  j�
  j�
  hKK�rD  j�
  KK�rE  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }rF  u}rG  (hh�j�
  j�
  hKK�rH  j�
  KK�rI  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }rJ  u}rK  (hX   Fire1_2rL  j�
  j�
  hKK�rM  j�
  KK�rN  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }rO  u}rP  (hj  j�
  j�
  hKK�rQ  j�
  KK�rR  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }rS  u}rT  (hX   Fire2rU  j�
  j�
  hj~  j�
  KK�rV  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }rW  u}rX  (hX   Fire2_0rY  j�
  j�
  hKK�rZ  j�
  KK�r[  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r\  u}r]  (hj�  j�
  j�
  hKK�r^  j�
  KK�r_  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r`  u}ra  (hj�  j�
  j�
  hK	K�rb  j�
  KK�rc  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }rd  u}re  (hj�  j�
  j�
  hKK	�rf  j�
  KK�rg  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }rh  ueh}ri  (hX   DEBUGrj  hX   DEBUGrk  X   tilemaprl  }rm  (hX   Ch5rn  X   layersro  ]rp  (}rq  (hX   baserr  X   visiblers  �u}rt  (hX   Leftru  js  �u}rv  (hX   Rightrw  js  �ueX   weatherrx  ]ry  X
   animationsrz  ]r{  uX
   bg_tilemapr|  Nh
hX   musicr}  }r~  (X   player_phaser  X   BattleFateMainr�  X   enemy_phaser�  NX   other_phaser�  NX   enemy2_phaser�  NX   player_battler�  X   Attackr�  X   enemy_battler�  NX   other_battler�  NX   enemy2_battler�  NuX	   objectiver�  }r�  (X   simpler�  X   DEBUGr�  X   winr�  X   Don't Crashr�  X   lossr�  X   Crashr�  uh]r�  (hhjh�h�h�h�j  j"  j?  j]  j  j�  j�  j�  j  j  j4  jJ  j_  js  j�  j�  j�  j�  ej�
  ]r�  (j�
  j�
  j�  h�jL  j  jU  jY  j�  j�  j�  eX   unit_groupsr�  ]r�  X	   ai_groupsr�  ]r�  uX
   overworldsr�  ]r�  }r�  (jl  NX   enabled_nodesr�  ]r�  X   enabled_roadsr�  ]r�  hX   0r�  X   overworld_entitiesr�  ]r�  }r�  (hhX   dtyper�  X   PARTYr�  X   dnidr�  hX   on_node_nidr�  NhNhX   playerr�  uaX   selected_party_nidr�  NX   node_propertiesr�  }r�  X   enabled_menu_optionsr�  }r�  (j�  }r�  X   1r�  }r�  X   2r�  }r�  X   3r�  }r�  X   4r�  }r�  X   5r�  }r�  X   6r�  }r�  X   7r�  }r�  X   8r�  }r�  X   9r�  }r�  X   10r�  }r�  X   11r�  }r�  X   12r�  }r�  X   13r�  }r�  X   14r�  }r�  X   15r�  }r�  X   16r�  }r�  X   17r�  }r�  X   18r�  }r�  X   19r�  }r�  X   20r�  }r�  X   21r�  }r�  X   22r�  }r�  X   23r�  }r�  X   24r�  }r�  X   25r�  }r�  uX   visible_menu_optionsr�  }r�  (j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  uuaX	   turncountr�  KX   playtimer�  MD�X	   game_varsr�  ccollections
Counter
r�  }r�  (X   _random_seedr   M�X   _chapter_testr  �X   _convoyr  �X   _custom_options_disabledr  ]r  �aX   _custom_info_descr  ]r  X   Open the Codexr  aX   _custom_options_eventsr  ]r	  X
   Menu_Codexr
  aX   _custom_additional_optionsr  ]r  X   Codexr  aX   _base_bg_namer  X   Hillr  X   _base_musicr  X    Before An Impossible Battlefieldr  X   _base_options_disabledr  ]r  X   _base_options_eventsr  ]r  X   _base_additional_optionsr  ]r  X   _base_transparentr  �u�r  Rr  X
   level_varsr  j�  }r  (X
   FireStart1r  KX   FireOrigin1r  j�  X   FireComplete1r  KX
   FireStart2r   KX   FireOrigin2r!  j~  X   FireComplete2r"  KX
   FireStart3r#  K X   FireOrigin3r$  K X   FireComplete3r%  K X
   FireStart4r&  K X   FireOrigin4r'  K X   FireComplete4r(  K X
   FireStart5r)  K X   FireOrigin5r*  K X   FireComplete5r+  K X
   FireStart6r,  K X   FireOrigin6r-  K X   FireComplete6r.  K X
   FireStart7r/  K X   FireOrigin7r0  K X   FireComplete7r1  K X
   FireStart8r2  K X   FireOrigin8r3  K X   FireComplete8r4  K X
   FireStart9r5  K X   FireOrigin9r6  K X   FireComplete9r7  K X   FireStart10r8  K X   FireOrigin10r9  K X   FireComplete10r:  K X   FireStart11r;  K X   FireOrigin11r<  K X   FireComplete11r=  K X   FireStart12r>  K X   FireOrigin12r?  K X   FireComplete12r@  K X   SaraidSkillrA  K X   QuinleySkillrB  K X   RoutEnemiesrC  K X   ValidFire1LocsrD  ]rE  (KK�rF  KK�rG  KK�rH  KK�rI  eX   GarveySpotsrJ  KX	   ValidLocsrK  ]rL  (KK�rM  KK	�rN  KK	�rO  KK
�rP  KK�rQ  KK�rR  KK�rS  KK	�rT  KK	�rU  KK
�rV  KK	�rW  KK
�rX  KK�rY  KK�rZ  KK�r[  KK�r\  KK�r]  KK�r^  KK	�r_  KK
�r`  KK�ra  KK	�rb  KK
�rc  KK�rd  KK�re  eX   GarvPositionrf  K X	   whocares1rg  KX   ValidFire2Locsrh  ]ri  (KK�rj  KK�rk  K	K�rl  KK	�rm  eX	   whocares2rn  Ku�ro  Rrp  X   current_moderq  }rr  (hX   Normalrs  X
   permadeathrt  �h.X   Randomru  X   enemy_autolevelsrv  K X   enemy_truelevelsrw  K X   boss_autolevelsrx  K X   boss_truelevelsry  K uX   partiesrz  ]r{  }r|  (hhhX   Eirika's Groupr}  X
   leader_nidr~  X   Alpinr  X   party_prep_manage_sort_orderr�  ]r�  X   moneyr�  K X   convoyr�  ]r�  X   bexpr�  K uaX   current_partyr�  hX   stater�  ]r�  (X   freer�  X   status_upkeepr�  X   phase_changer�  e]r�  �r�  X
   action_logr�  ]r�  (X   AddSkillr�  }r�  (X   unitr�  j�  h�r�  X	   initiatorr�  hN�r�  X	   skill_objr�  X   skillr�  K�r�  X   sourcer�  hK��r�  X   source_typer�  hhK�r�  X
   subactionsr�  X   listr�  ]r�  �r�  X   reset_actionr�  X   actionr�  X   ResetUnitVarsr�  }r�  (j�  j�  h�r�  X   old_current_hpr�  hK�r�  X   old_current_manar�  hK �r�  u�r�  �r�  X   did_somethingr�  h��r�  u�r�  j�  }r�  (j�  j�  hj�r�  j�  hN�r�  j�  j�  K��r�  j�  hK��r�  j�  hhK�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  hj�r�  j�  hK
�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  h��r�  j�  hN�r�  j�  j�  K��r�  j�  hK��r�  j�  hhK�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  h��r�  j�  hN�r�  j�  j�  K��r�  j�  hK��r�  j�  hhK�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  h�r�  j�  hN�r�  j�  j�  M�r�  j�  hKĆr�  j�  hhK�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  h�r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  j  �r�  j�  hN�r�  j�  j�  M
�r�  j�  hKȆr�  j�  hhK�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  j?  �r   j�  hN�r  j�  j�  M�r  j�  hKʆr  j�  hhK�r  j�  j�  ]r  �r  j�  j�  j�  }r  (j�  j�  j?  �r  j�  hK�r	  j�  hK �r
  u�r  �r  j�  h��r  u�r  j�  }r  (j�  j�  j]  �r  j�  hN�r  j�  j�  M�r  j�  hK͆r  j�  hhK�r  j�  j�  ]r  �r  j�  j�  j�  }r  (j�  j�  j]  �r  j�  hK�r  j�  hK �r  u�r  �r  j�  h��r  u�r  j�  }r  (j�  j�  j  �r   j�  hN�r!  j�  j�  M%�r"  j�  hKІr#  j�  hhK�r$  j�  j�  ]r%  �r&  j�  j�  j�  }r'  (j�  j�  j  �r(  j�  hK�r)  j�  hK �r*  u�r+  �r,  j�  h��r-  u�r.  j�  }r/  (j�  j�  j�  �r0  j�  hN�r1  j�  j�  M+�r2  j�  hKӆr3  j�  hhK�r4  j�  j�  ]r5  �r6  j�  j�  j�  }r7  (j�  j�  j�  �r8  j�  hK�r9  j�  hK �r:  u�r;  �r<  j�  h��r=  u�r>  j�  }r?  (j�  j�  jJ  �r@  j�  hN�rA  j�  j�  MD�rB  j�  hKކrC  j�  hhK�rD  j�  j�  ]rE  �rF  j�  j�  j�  }rG  (j�  j�  jJ  �rH  j�  hK�rI  j�  hK �rJ  u�rK  �rL  j�  h��rM  u�rN  j�  }rO  (j�  j�  j_  �rP  j�  hN�rQ  j�  j�  MG�rR  j�  hK߆rS  j�  hhK�rT  j�  j�  ]rU  �rV  j�  j�  j�  }rW  (j�  j�  j_  �rX  j�  hK�rY  j�  hK �rZ  u�r[  �r\  j�  h��r]  u�r^  j�  }r_  (j�  j�  j�  �r`  j�  hN�ra  j�  j�  ML�rb  j�  hK�rc  j�  hhK�rd  j�  j�  ]re  �rf  j�  j�  j�  }rg  (j�  j�  j�  �rh  j�  hK�ri  j�  hK �rj  u�rk  �rl  j�  h��rm  u�rn  j�  }ro  (j�  j�  j�  �rp  j�  hN�rq  j�  j�  MQ�rr  j�  hK�rs  j�  hhK�rt  j�  j�  ]ru  �rv  j�  j�  j�  }rw  (j�  j�  j�  �rx  j�  hK�ry  j�  hK �rz  u�r{  �r|  j�  h��r}  u�r~  j�  }r  (j�  j�  j�  �r�  j�  hN�r�  j�  j�  MU�r�  j�  hK�r�  j�  hhK�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  j�  �r�  j�  hN�r�  j�  j�  MY�r�  j�  hK�r�  j�  hhK�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  h�r�  j�  hN�r�  j�  j�  MZ�r�  j�  hh5�r�  j�  hh��r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  h�r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  hj�r�  j�  hN�r�  j�  j�  M[�r�  j�  hhk�r�  j�  hh��r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  hj�r�  j�  hK
�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  h��r�  j�  hN�r�  j�  j�  M\�r�  j�  hh��r�  j�  hh��r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  h��r�  j�  hN�r�  j�  j�  M]�r�  j�  hh��r�  j�  hh��r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  hȆr�  j�  hN�r�  j�  j�  M^�r�  j�  hhɆr�  j�  hh��r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  hȆr�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  h�r�  j�  hN�r�  j�  j�  M_�r�  j�  hh�r�  j�  hh��r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  h�r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  j  �r   j�  hN�r  j�  j�  M`�r  j�  hj  �r  j�  hh��r  j�  j�  ]r  �r  j�  j�  j�  }r  (j�  j�  j  �r  j�  hK�r	  j�  hK �r
  u�r  �r  j�  h��r  u�r  j�  }r  (j�  j�  j"  �r  j�  hN�r  j�  j�  Ma�r  j�  hj#  �r  j�  hh��r  j�  j�  ]r  �r  j�  j�  j�  }r  (j�  j�  j"  �r  j�  hK�r  j�  hK �r  u�r  �r  j�  h��r  u�r  j�  }r  (j�  j�  j?  �r   j�  hN�r!  j�  j�  Mb�r"  j�  hj@  �r#  j�  hh��r$  j�  j�  ]r%  �r&  j�  j�  j�  }r'  (j�  j�  j?  �r(  j�  hK�r)  j�  hK �r*  u�r+  �r,  j�  h��r-  u�r.  j�  }r/  (j�  j�  j]  �r0  j�  hN�r1  j�  j�  Mc�r2  j�  hj^  �r3  j�  hh��r4  j�  j�  ]r5  �r6  j�  j�  j�  }r7  (j�  j�  j]  �r8  j�  hK�r9  j�  hK �r:  u�r;  �r<  j�  h��r=  u�r>  j�  }r?  (j�  j�  j  �r@  j�  hN�rA  j�  j�  Md�rB  j�  hj�  �rC  j�  hh��rD  j�  j�  ]rE  �rF  j�  j�  j�  }rG  (j�  j�  j  �rH  j�  hK�rI  j�  hK �rJ  u�rK  �rL  j�  h��rM  u�rN  j�  }rO  (j�  j�  j�  �rP  j�  hN�rQ  j�  j�  Me�rR  j�  hj�  �rS  j�  hh��rT  j�  j�  ]rU  �rV  j�  j�  j�  }rW  (j�  j�  j�  �rX  j�  hK�rY  j�  hK �rZ  u�r[  �r\  j�  h��r]  u�r^  j�  }r_  (j�  j�  j�  �r`  j�  hN�ra  j�  j�  Mf�rb  j�  hj�  �rc  j�  hh��rd  j�  j�  ]re  �rf  j�  j�  j�  }rg  (j�  j�  j�  �rh  j�  hK�ri  j�  hK �rj  u�rk  �rl  j�  h��rm  u�rn  j�  }ro  (j�  j�  j�  �rp  j�  hN�rq  j�  j�  Mg�rr  j�  hj�  �rs  j�  hh��rt  j�  j�  ]ru  �rv  j�  j�  j�  }rw  (j�  j�  j�  �rx  j�  hK�ry  j�  hK �rz  u�r{  �r|  j�  h��r}  u�r~  j�  }r  (j�  j�  j  �r�  j�  hN�r�  j�  j�  Mh�r�  j�  hj  �r�  j�  hh��r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  js  �r�  j�  hN�r�  j�  j�  Mi�r�  j�  hj~  �r�  j�  hh��r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  js  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  j�  �r�  j�  hN�r�  j�  j�  Mj�r�  j�  hj�  �r�  j�  hh��r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  j�  �r�  j�  hN�r�  j�  j�  Mk�r�  j�  hj�  �r�  j�  hh��r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  X   IncrementTurnr�  }r�  �r�  X   UpdateRecordsr�  }r�  (X   record_typer�  hX   turnr�  �r�  j�  hN�r�  u�r�  X   ChangeFatiguer�  }r�  (j�  j�  h�r�  X   numr�  hK �r�  X   old_fatiguer�  hK �r�  j�  j�  ]r�  �r�  u�r�  j�  }r�  (j�  j�  hj�r�  j�  hK �r�  j�  hK �r�  j�  j�  ]r�  �r�  u�r�  j�  }r�  (j�  j�  h��r�  j�  hK �r�  j�  hK �r�  j�  j�  ]r�  �r�  u�r�  j�  }r�  (j�  j�  h��r�  j�  hK �r�  j�  hK �r�  j�  j�  ]r�  �r�  u�r�  j�  }r�  (j�  j�  hȆr�  j�  hK �r�  j�  hK �r�  j�  j�  ]r�  �r�  u�r�  j�  }r�  (j�  j�  h�r�  j�  hK �r�  j�  hK �r�  j�  j�  ]r�  �r�  u�r�  j�  }r�  (j�  j�  j  �r�  j�  hK �r�  j�  hK �r�  j�  j�  ]r�  �r�  u�r�  j�  }r�  (j�  j�  j"  �r�  j�  hK �r�  j�  hK �r   j�  j�  ]r  �r  u�r  j�  }r  (j�  j�  j?  �r  j�  hK �r  j�  hK �r  j�  j�  ]r  �r	  u�r
  j�  }r  (j�  j�  j]  �r  j�  hK �r  j�  hK �r  j�  j�  ]r  �r  u�r  j�  }r  (j�  j�  j  �r  j�  hK �r  j�  hK �r  j�  j�  ]r  �r  u�r  j�  }r  (j�  j�  j�  �r  j�  hK �r  j�  hK �r  j�  j�  ]r  �r  u�r  j�  }r   (j�  j�  j�  �r!  j�  hK �r"  j�  hK �r#  j�  j�  ]r$  �r%  u�r&  j�  }r'  (j�  j�  j�  �r(  j�  hK �r)  j�  hK �r*  j�  j�  ]r+  �r,  u�r-  X   SetLevelVarr.  }r/  (hhj  �r0  X   valr1  hK �r2  X   old_valr3  hK �r4  u�r5  j.  }r6  (hhj  �r7  j1  hK �r8  j3  hK �r9  u�r:  j.  }r;  (hhj  �r<  j1  hK �r=  j3  hK �r>  u�r?  j.  }r@  (hhj   �rA  j1  hK �rB  j3  hK �rC  u�rD  j.  }rE  (hhj!  �rF  j1  hK �rG  j3  hK �rH  u�rI  j.  }rJ  (hhj"  �rK  j1  hK �rL  j3  hK �rM  u�rN  j.  }rO  (hhj#  �rP  j1  hK �rQ  j3  hK �rR  u�rS  j.  }rT  (hhj$  �rU  j1  hK �rV  j3  hK �rW  u�rX  j.  }rY  (hhj%  �rZ  j1  hK �r[  j3  hK �r\  u�r]  j.  }r^  (hhj&  �r_  j1  hK �r`  j3  hK �ra  u�rb  j.  }rc  (hhj'  �rd  j1  hK �re  j3  hK �rf  u�rg  j.  }rh  (hhj(  �ri  j1  hK �rj  j3  hK �rk  u�rl  j.  }rm  (hhj)  �rn  j1  hK �ro  j3  hK �rp  u�rq  j.  }rr  (hhj*  �rs  j1  hK �rt  j3  hK �ru  u�rv  j.  }rw  (hhj+  �rx  j1  hK �ry  j3  hK �rz  u�r{  j.  }r|  (hhj,  �r}  j1  hK �r~  j3  hK �r  u�r�  j.  }r�  (hhj-  �r�  j1  hK �r�  j3  hK �r�  u�r�  j.  }r�  (hhj.  �r�  j1  hK �r�  j3  hK �r�  u�r�  j.  }r�  (hhj/  �r�  j1  hK �r�  j3  hK �r�  u�r�  j.  }r�  (hhj0  �r�  j1  hK �r�  j3  hK �r�  u�r�  j.  }r�  (hhj1  �r�  j1  hK �r�  j3  hK �r�  u�r�  j.  }r�  (hhj2  �r�  j1  hK �r�  j3  hK �r�  u�r�  j.  }r�  (hhj3  �r�  j1  hK �r�  j3  hK �r�  u�r�  j.  }r�  (hhj4  �r�  j1  hK �r�  j3  hK �r�  u�r�  j.  }r�  (hhj5  �r�  j1  hK �r�  j3  hK �r�  u�r�  j.  }r�  (hhj6  �r�  j1  hK �r�  j3  hK �r�  u�r�  j.  }r�  (hhj7  �r�  j1  hK �r�  j3  hK �r�  u�r�  j.  }r�  (hhj8  �r�  j1  hK �r�  j3  hK �r�  u�r�  j.  }r�  (hhj9  �r�  j1  hK �r�  j3  hK �r�  u�r�  j.  }r�  (hhj:  �r�  j1  hK �r�  j3  hK �r�  u�r�  j.  }r�  (hhj;  �r�  j1  hK �r�  j3  hK �r�  u�r�  j.  }r�  (hhj<  �r�  j1  hK �r�  j3  hK �r�  u�r�  j.  }r�  (hhj=  �r�  j1  hK �r�  j3  hK �r�  u�r�  j.  }r�  (hhj>  �r�  j1  hK �r�  j3  hK �r�  u�r�  j.  }r�  (hhj?  �r�  j1  hK �r�  j3  hK �r�  u�r�  j.  }r�  (hhj@  �r�  j1  hK �r�  j3  hK �r�  u�r�  X
   SetGameVarr�  }r�  (hhj  �r�  j1  h��r�  j3  hK �r�  u�r�  X   SetHPr�  }r�  (j�  j�  h�r�  X   new_hpr�  hK�r�  X   old_hpr�  hK�r�  u�r�  j�  }r�  (j�  j�  j]  �r�  j�  hN�r�  j�  j�  Ml�r�  j�  hN�r�  j�  hjy  �r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  j]  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r   j�  h��r  u�r  j.  }r  (hhjA  �r  j1  hK �r  j3  hK �r  u�r  j.  }r  (hhjB  �r	  j1  hK �r
  j3  hK �r  u�r  X   SetExpr  }r  (j�  j�  h�r  X   old_expr  hK �r  X   exp_gainr  hK_�r  u�r  X   ChangeAIr  }r  (j�  j�  j�  �r  hhj�  �r  X   old_air  hX   AttackNoMover  �r  u�r  j.  }r  (hhjC  �r  j1  hK �r  j3  hK �r   u�r!  X   SetDroppabler"  }r#  (hHhHK�r$  X   was_droppabler%  h��r&  X   valuer'  h��r(  u�r)  X   GiveItemr*  }r+  (j�  j�  hȆr,  hHhHK�r-  u�r.  X   AddLorer/  }r0  X   lore_nidr1  hX   WeaponTriangler2  �r3  s�r4  j/  }r5  j1  hX   Statsr6  �r7  s�r8  j/  }r9  j1  hX	   Equationsr:  �r;  s�r<  j/  }r=  j1  hX   WeaponRanksr>  �r?  s�r@  j/  }rA  j1  hX   BurningrB  �rC  s�rD  j/  }rE  j1  hX   FreeActionsrF  �rG  s�rH  j�  }rI  (hhj  �rJ  j1  j�  ]rK  h��rL  a�rM  j3  hK �rN  u�rO  j�  }rP  (hhj  �rQ  j1  j�  ]rR  hj  �rS  a�rT  j3  hK �rU  u�rV  j�  }rW  (hhj  �rX  j1  j�  ]rY  hj
  �rZ  a�r[  j3  hK �r\  u�r]  j�  }r^  (hhj  �r_  j1  j�  ]r`  hj  �ra  a�rb  j3  hK �rc  u�rd  j�  }re  (hhj  �rf  j1  hj  �rg  j3  hK �rh  u�ri  j�  }rj  (hhj  �rk  j1  hj  �rl  j3  hK �rm  u�rn  j�  }ro  (hhj  �rp  j1  j�  ]rq  �rr  j3  hK �rs  u�rt  j�  }ru  (hhj  �rv  j1  j�  ]rw  �rx  j3  hK �ry  u�rz  j�  }r{  (hhj  �r|  j1  j�  ]r}  �r~  j3  hK �r  u�r�  j�  }r�  (hhj  �r�  j1  h��r�  j3  hK �r�  u�r�  X   LockTurnwheelr�  }r�  X   lockr�  h��r�  s�r�  X   ResetAllr�  }r�  X   actionsr�  j�  ]r�  (j�  X   Resetr�  }r�  (j�  j�  h�r�  X   movement_leftr�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  hj�r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  hȆr�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h�r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j"  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j?  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j]  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r   j�  j�  }r  (j�  j�  j4  �r  j�  hK �r  hch(��������tr  �r  u�r  �r  j�  j�  }r  (j�  j�  jJ  �r	  j�  hK�r
  hch(��������tr  �r  u�r  �r  j�  j�  }r  (j�  j�  j_  �r  j�  hK�r  hch(��������tr  �r  u�r  �r  j�  j�  }r  (j�  j�  js  �r  j�  hK�r  hch(��������tr  �r  u�r  �r  j�  j�  }r  (j�  j�  j�  �r  j�  hK�r  hch(��������tr   �r!  u�r"  �r#  j�  j�  }r$  (j�  j�  j�  �r%  j�  hK�r&  hch(��������tr'  �r(  u�r)  �r*  j�  j�  }r+  (j�  j�  j�  �r,  j�  hK�r-  hch(��������tr.  �r/  u�r0  �r1  j�  j�  }r2  (j�  j�  j�  �r3  j�  hK�r4  hch(��������tr5  �r6  u�r7  �r8  e�r9  s�r:  X	   MarkPhaser;  }r<  X
   phase_namer=  hX   playerr>  �r?  s�r@  X   MoverA  }rB  (j�  j�  h�rC  X   old_posrD  hh�rE  X   new_posrF  hKK�rG  �rH  X   prev_movement_leftrI  hK�rJ  X   new_movement_leftrK  hN�rL  X   pathrM  hN�rN  X	   has_movedrO  h��rP  j�
  h��rQ  X   followrR  h��rS  X   speedrT  hK�rU  u�rV  X	   EquipItemrW  }rX  (j�  j�  h�rY  hHhHKņrZ  X   current_equippedr[  hHKr\  u�r]  jW  }r^  (j�  j�  h�r_  hHhHKņr`  j[  hHKņra  u�rb  X   BringToTopItemrc  }rd  (j�  j�  h�re  hHhHKņrf  X   old_idxrg  hK�rh  u�ri  X   ChangeHPrj  }rk  (j�  j�  j�  �rl  j�  hJ�����rm  j�  hK�rn  u�ro  X
   SetObjDatarp  }rq  (X   objrr  hHKņrs  X   keywordrt  hj  �ru  j'  hK�rv  X	   old_valuerw  hK�rx  u�ry  j�  }rz  (j�  hX   item_user{  �r|  j�  hh�jn  �r}  �r~  u�r  X   HasAttackedr�  }r�  (j�  j�  h�r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  X   Messager�  }r�  X   messager�  hX   Saraid attacked Soldierr�  �r�  s�r�  X   GainWexpr�  }r�  (j�  j�  h�r�  hHhHKņr�  X	   wexp_gainr�  hG?�      �r�  jw  hK�r�  X   current_valuer�  hG@(      �r�  u�r�  j�  }r�  (j�  hj   �r�  j�  hh�j�  �r�  �r�  u�r�  j�  }r�  (j�  hj�  �r�  j�  h(h�j�  jn  KKj   tr�  �r�  u�r�  X   RecordRandomStater�  }r�  (X   oldr�  hM��r�  X   newr�  hJ�4�r�  u�r�  X	   AddRegionr�  }r�  (h�h�j�
  �r�  X   did_addr�  h��r�  j�  j�  ]r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hN�r�  j�  j�  Mm�r�  j�  hj�
  �r�  j�  hh��r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  �r�  a�r�  u�r�  X
   AddMapAnimr�  }r�  (hhX   Burningr�  �r�  X   posr�  hj�  �r�  X
   speed_multr�  hK�r�  X
   blend_moder�  hcapp.engine.engine
BlendMode
r�  K �r�  Rr�  �r�  X   is_upper_layerr�  h��r�  u�r�  j.  }r�  (hhjD  �r�  j1  j�  ]r�  (hjF  �r�  hjG  �r�  hjH  �r�  hjI  �r�  e�r�  j3  hK �r�  u�r�  j.  }r�  (hhX
   FireStart1r�  �r�  j1  hK�r�  j3  hK �r�  u�r�  j.  }r�  (hhX   FireOrigin1r�  �r�  j1  hj�  �r�  j3  hK �r�  u�r�  X   GainExpr�  }r�  (j�  j�  h�r�  j  hK �r�  j  hK
�r�  u�r�  j�  }r�  (j�  hj  �r�  j�  hh�K
h�r�  �r�  u�r�  X   Waitr�  }r�  (j�  j�  h�r�  hch(��������tr�  �r�  X   update_fow_actionr�  j�  X   UpdateFogOfWarr�  }r�  (j�  j�  h�r�  X   prev_posr�  hN�r�  u�r�  �r�  X   regions_removedr�  j�  ]r   �r  u�r  jA  }r  (j�  j�  j�  �r  jD  hj�  �r  jF  hj�  �r  jI  hK�r  jK  hN�r  jM  hN�r	  jO  h��r
  j�
  h��r  jR  h��r  jT  hK�r  u�r  jW  }r  (j�  j�  j�  �r  hHhHKӆr  j[  hHKӆr  u�r  jc  }r  (j�  j�  j�  �r  hHhHKӆr  jg  hK �r  u�r  jj  }r  (j�  j�  j�  �r  j�  hJ�����r  j�  hK�r  u�r  jp  }r  (jr  hHKӆr  jt  hj  �r   j'  hK�r!  jw  hK�r"  u�r#  j�  }r$  (j�  hj{  �r%  j�  hj�  j�  �r&  �r'  u�r(  j�  }r)  (j�  j�  j�  �r*  j�  hK�r+  hch(��������tr,  �r-  u�r.  j�  }r/  j�  hX   Ailsa attacked Soldierr0  �r1  s�r2  j�  }r3  j�  hX   Prevailed over Soldierr4  �r5  s�r6  j�  }r7  (j�  j�  j�  �r8  hHhHKӆr9  j�  hG?�      �r:  jw  hK�r;  j�  hG@6      �r<  u�r=  j�  }r>  (j�  hj  �r?  j�  hj�  j�  �r@  �rA  u�rB  j�  }rC  (j�  hj�  �rD  j�  h(j�  j�  j�  K
Kj  trE  �rF  u�rG  j�  }rH  (j�  hX   killrI  �rJ  j�  hj�  j�  �rK  �rL  u�rM  j�  }rN  (j�  hJ�4�rO  j�  hJo��rP  u�rQ  X   DierR  }rS  (j�  j�  j�  �rT  jD  hj�  �rU  X	   leave_maprV  j�  X   LeaveMaprW  }rX  (j�  j�  j�  �rY  X   remove_from_maprZ  j�  X   RemoveFromMapr[  }r\  (j�  j�  j�  �r]  jD  hj�  �r^  j�  j�  j�  }r_  (j�  j�  j�  �r`  j�  hN�ra  u�rb  �rc  u�rd  �re  u�rf  �rg  X   lock_all_support_ranksrh  j�  ]ri  �rj  X   droprk  hN�rl  X   initiative_actionrm  hN�rn  u�ro  j�  }rp  (j�  j�  j�  �rq  j  hK �rr  j  hK�rs  u�rt  j�  }ru  (j�  hj  �rv  j�  hj�  Kj�  �rw  �rx  u�ry  j�  }rz  (j�  j�  j�  �r{  hch(��������tr|  �r}  j�  j�  j�  }r~  (j�  j�  j�  �r  j�  hN�r�  u�r�  �r�  j�  j�  ]r�  �r�  u�r�  jp  }r�  (jr  j�  M�r�  jt  hj�	  �r�  j'  hK�r�  jw  hK�r�  u�r�  j.  }r�  (hhjJ  �r�  j1  hK �r�  j3  hK �r�  u�r�  j.  }r�  (hhjK  �r�  j1  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  e�r�  j3  hK �r�  u�r�  j.  }r�  (hhjf  �r�  j1  hj^  �r�  j3  hK �r�  u�r�  j.  }r�  (hhX	   ValidLocsr�  �r�  j1  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  e�r�  j3  j�  ]r�  (hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j.  }r�  (hhj�  �r�  j1  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  e�r�  j3  j�  ]r�  (hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j.  }r�  (hhj�  �r�  j1  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  e�r�  j3  j�  ]r�  (hj�  �r   hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r	  e�r
  u�r  j.  }r  (hhj�  �r  j1  j�  ]r  (hKK�r  �r  hKK	�r  �r  hKK	�r  �r  hKK
�r  �r  hKK�r  �r  hKK�r  �r  hKK�r  �r  hKK	�r  �r  hKK	�r  �r   hKK
�r!  �r"  hKK	�r#  �r$  hKK
�r%  �r&  hKK�r'  �r(  e�r)  j3  j�  ]r*  (hj�  �r+  hj�  �r,  hj�  �r-  hj�  �r.  hj�  �r/  hj�  �r0  hj�  �r1  hj�  �r2  hj�  �r3  hj�  �r4  hj�  �r5  hj�  �r6  e�r7  u�r8  j.  }r9  (hhj�  �r:  j1  j�  ]r;  (hKK�r<  �r=  hKK	�r>  �r?  hKK	�r@  �rA  hKK
�rB  �rC  hKK�rD  �rE  hKK�rF  �rG  hKK�rH  �rI  hKK	�rJ  �rK  hKK	�rL  �rM  hKK
�rN  �rO  hKK	�rP  �rQ  hKK
�rR  �rS  hKK�rT  �rU  e�rV  j3  j�  ]rW  (hj  �rX  hj  �rY  hj  �rZ  hj  �r[  hj  �r\  hj  �r]  hj  �r^  hj  �r_  hj  �r`  hj!  �ra  hj#  �rb  hj%  �rc  hj'  �rd  e�re  u�rf  j.  }rg  (hhj�  �rh  j1  j�  ]ri  (hKK�rj  �rk  hKK	�rl  �rm  hKK	�rn  �ro  hKK
�rp  �rq  hKK�rr  �rs  hKK�rt  �ru  hKK�rv  �rw  hKK	�rx  �ry  hKK	�rz  �r{  hKK
�r|  �r}  hKK	�r~  �r  hKK
�r�  �r�  hKK�r�  �r�  e�r�  j3  j�  ]r�  (hj<  �r�  hj>  �r�  hj@  �r�  hjB  �r�  hjD  �r�  hjF  �r�  hjH  �r�  hjJ  �r�  hjL  �r�  hjN  �r�  hjP  �r�  hjR  �r�  hjT  �r�  e�r�  u�r�  j.  }r�  (hhj�  �r�  j1  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  e�r�  j3  j�  ]r�  (hjj  �r�  hjl  �r�  hjn  �r�  hjp  �r�  hjr  �r�  hjt  �r�  hjv  �r�  hjx  �r�  hjz  �r�  hj|  �r�  hj~  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j.  }r�  (hhj�  �r�  j1  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  e�r�  j3  j�  ]r�  (hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j.  }r�  (hhj�  �r�  j1  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r   �r  hKK	�r  �r  hKK	�r  �r  hKK
�r  �r  hKK	�r  �r	  hKK
�r
  �r  hKK�r  �r  hKK�r  �r  hKK�r  �r  hKK�r  �r  e�r  j3  j�  ]r  (hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r   hj�  �r!  hj�  �r"  e�r#  u�r$  j.  }r%  (hhj�  �r&  j1  j�  ]r'  (hKK�r(  �r)  hKK	�r*  �r+  hKK	�r,  �r-  hKK
�r.  �r/  hKK�r0  �r1  hKK�r2  �r3  hKK�r4  �r5  hKK	�r6  �r7  hKK	�r8  �r9  hKK
�r:  �r;  hKK	�r<  �r=  hKK
�r>  �r?  hKK�r@  �rA  hKK�rB  �rC  hKK�rD  �rE  hKK�rF  �rG  hKK�rH  �rI  e�rJ  j3  j�  ]rK  (hj�  �rL  hj�  �rM  hj�  �rN  hj�  �rO  hj�  �rP  hj�  �rQ  hj   �rR  hj  �rS  hj  �rT  hj  �rU  hj  �rV  hj
  �rW  hj  �rX  hj  �rY  hj  �rZ  hj  �r[  e�r\  u�r]  j.  }r^  (hhj�  �r_  j1  j�  ]r`  (hKK�ra  �rb  hKK	�rc  �rd  hKK	�re  �rf  hKK
�rg  �rh  hKK�ri  �rj  hKK�rk  �rl  hKK�rm  �rn  hKK	�ro  �rp  hKK	�rq  �rr  hKK
�rs  �rt  hKK	�ru  �rv  hKK
�rw  �rx  hKK�ry  �rz  hKK�r{  �r|  hKK�r}  �r~  hKK�r  �r�  hKK�r�  �r�  hKK�r�  �r�  e�r�  j3  j�  ]r�  (hj(  �r�  hj*  �r�  hj,  �r�  hj.  �r�  hj0  �r�  hj2  �r�  hj4  �r�  hj6  �r�  hj8  �r�  hj:  �r�  hj<  �r�  hj>  �r�  hj@  �r�  hjB  �r�  hjD  �r�  hjF  �r�  hjH  �r�  e�r�  u�r�  j.  }r�  (hhj�  �r�  j1  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  e�r�  j3  j�  ]r�  (hja  �r�  hjc  �r�  hje  �r�  hjg  �r�  hji  �r�  hjk  �r�  hjm  �r�  hjo  �r�  hjq  �r�  hjs  �r�  hju  �r�  hjw  �r�  hjy  �r�  hj{  �r�  hj}  �r�  hj  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j.  }r�  (hhj�  �r�  j1  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK
�r   �r  e�r  j3  j�  ]r  (hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r	  hj�  �r
  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  e�r  u�r  j.  }r  (hhj�  �r  j1  j�  ]r  (hKK�r  �r  hKK	�r  �r  hKK	�r  �r   hKK
�r!  �r"  hKK�r#  �r$  hKK�r%  �r&  hKK�r'  �r(  hKK	�r)  �r*  hKK	�r+  �r,  hKK
�r-  �r.  hKK	�r/  �r0  hKK
�r1  �r2  hKK�r3  �r4  hKK�r5  �r6  hKK�r7  �r8  hKK�r9  �r:  hKK�r;  �r<  hKK�r=  �r>  hKK	�r?  �r@  hKK
�rA  �rB  hKK�rC  �rD  e�rE  j3  j�  ]rF  (hj�  �rG  hj�  �rH  hj�  �rI  hj�  �rJ  hj�  �rK  hj�  �rL  hj�  �rM  hj�  �rN  hj�  �rO  hj�  �rP  hj�  �rQ  hj�  �rR  hj�  �rS  hj�  �rT  hj�  �rU  hj�  �rV  hj�  �rW  hj�  �rX  hj�  �rY  hj   �rZ  e�r[  u�r\  j.  }r]  (hhj�  �r^  j1  j�  ]r_  (hKK�r`  �ra  hKK	�rb  �rc  hKK	�rd  �re  hKK
�rf  �rg  hKK�rh  �ri  hKK�rj  �rk  hKK�rl  �rm  hKK	�rn  �ro  hKK	�rp  �rq  hKK
�rr  �rs  hKK	�rt  �ru  hKK
�rv  �rw  hKK�rx  �ry  hKK�rz  �r{  hKK�r|  �r}  hKK�r~  �r  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  e�r�  j3  j�  ]r�  (hj  �r�  hj  �r�  hj  �r�  hj!  �r�  hj#  �r�  hj%  �r�  hj'  �r�  hj)  �r�  hj+  �r�  hj-  �r�  hj/  �r�  hj1  �r�  hj3  �r�  hj5  �r�  hj7  �r�  hj9  �r�  hj;  �r�  hj=  �r�  hj?  �r�  hjA  �r�  hjC  �r�  e�r�  u�r�  j.  }r�  (hhj�  �r�  j1  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  e�r�  j3  j�  ]r�  (hj`  �r�  hjb  �r�  hjd  �r�  hjf  �r�  hjh  �r�  hjj  �r�  hjl  �r�  hjn  �r�  hjp  �r�  hjr  �r�  hjt  �r�  hjv  �r�  hjx  �r�  hjz  �r�  hj|  �r�  hj~  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j.  }r�  (hhj�  �r�  j1  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r   �r  hKK�r  �r  hKK�r  �r  hKK	�r  �r  hKK	�r  �r	  hKK
�r
  �r  hKK	�r  �r  hKK
�r  �r  hKK�r  �r  hKK�r  �r  hKK�r  �r  hKK�r  �r  hKK�r  �r  hKK�r  �r  hKK	�r  �r  hKK
�r  �r  hKK�r   �r!  hKK	�r"  �r#  hKK
�r$  �r%  hKK�r&  �r'  hKK�r(  �r)  e�r*  j3  j�  ]r+  (hj�  �r,  hj�  �r-  hj�  �r.  hj�  �r/  hj�  �r0  hj�  �r1  hj�  �r2  hj�  �r3  hj�  �r4  hj�  �r5  hj�  �r6  hj�  �r7  hj�  �r8  hj�  �r9  hj�  �r:  hj�  �r;  hj�  �r<  hj�  �r=  hj�  �r>  hj�  �r?  hj�  �r@  hj�  �rA  hj�  �rB  hj�  �rC  e�rD  u�rE  j�  }rF  (hhX
   Target_3x3rG  �rH  j�  hG@      G@"      �rI  �rJ  j�  hK�rK  j�  hj�  �rL  j�  h��rM  u�rN  j�  }rO  (h�h�X   Garve_0rP  �rQ  j�  h��rR  j�  j�  ]rS  �rT  u�rU  j.  }rV  (hhX   GarveySpotsrW  �rX  j1  hK�rY  j3  hK �rZ  u�r[  j�  }r\  (h�h�X   Garve_1r]  �r^  j�  h��r_  j�  j�  ]r`  �ra  u�rb  j.  }rc  (hhjW  �rd  j1  hK�re  j3  hK�rf  u�rg  j�  }rh  (h�h�X   Garve_2ri  �rj  j�  h��rk  j�  j�  ]rl  �rm  u�rn  j.  }ro  (hhjW  �rp  j1  hK�rq  j3  hK�rr  u�rs  j�  }rt  (h�h�X   Garve_3ru  �rv  j�  h��rw  j�  j�  ]rx  �ry  u�rz  j.  }r{  (hhjW  �r|  j1  hK�r}  j3  hK�r~  u�r  j�  }r�  (h�h�X   Garve_4r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j.  }r�  (hhjW  �r�  j1  hK�r�  j3  hK�r�  u�r�  j�  }r�  (h�h�X   Garve_5r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j.  }r�  (hhjW  �r�  j1  hK�r�  j3  hK�r�  u�r�  j�  }r�  (h�h�X   Garve_6r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j.  }r�  (hhjW  �r�  j1  hK�r�  j3  hK�r�  u�r�  j�  }r�  (h�h�X   Garve_7r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j.  }r�  (hhjW  �r�  j1  hK�r�  j3  hK�r�  u�r�  j�  }r�  (h�h�X   Garve_8r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j.  }r�  (hhjW  �r�  j1  hK	�r�  j3  hK�r�  u�r�  j�  }r�  (h�h�X   Garve_9r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j.  }r�  (hhjW  �r�  j1  hK
�r�  j3  hK	�r�  u�r�  j�  }r�  (h�h�X   Garve_10r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j.  }r�  (hhjW  �r�  j1  hK�r�  j3  hK
�r�  u�r�  j�  }r�  (h�h�X   Garve_11r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j.  }r�  (hhjW  �r�  j1  hK�r�  j3  hK�r�  u�r�  j�  }r�  (h�h�X   Garve_12r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j.  }r�  (hhjW  �r�  j1  hK�r�  j3  hK�r�  u�r�  j�  }r�  (h�h�X   Garve_13r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j.  }r�  (hhjW  �r�  j1  hK�r�  j3  hK�r�  u�r�  j�  }r�  (h�h�X   Garve_14r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j.  }r�  (hhjW  �r   j1  hK�r  j3  hK�r  u�r  j�  }r  (h�h�X   Garve_15r  �r  j�  h��r  j�  j�  ]r  �r	  u�r
  j.  }r  (hhjW  �r  j1  hK�r  j3  hK�r  u�r  j�  }r  j�  h��r  s�r  j�  }r  j�  j�  ]r  (j�  j�  }r  (j�  j�  h�r  j�  hK�r  hch(��������tr  �r  u�r  �r  j�  j�  }r  (j�  j�  hj�r  j�  hK�r  hch(��������tr  �r   u�r!  �r"  j�  j�  }r#  (j�  j�  h��r$  j�  hK�r%  hch(��������tr&  �r'  u�r(  �r)  j�  j�  }r*  (j�  j�  h��r+  j�  hK�r,  hch(��������tr-  �r.  u�r/  �r0  j�  j�  }r1  (j�  j�  hȆr2  j�  hK�r3  hch(��������tr4  �r5  u�r6  �r7  j�  j�  }r8  (j�  j�  h�r9  j�  hK�r:  hch(��������tr;  �r<  u�r=  �r>  j�  j�  }r?  (j�  j�  j  �r@  j�  hK�rA  hch(��������trB  �rC  u�rD  �rE  j�  j�  }rF  (j�  j�  j"  �rG  j�  hK�rH  hch(��������trI  �rJ  u�rK  �rL  j�  j�  }rM  (j�  j�  j?  �rN  j�  hK�rO  hch(��������trP  �rQ  u�rR  �rS  j�  j�  }rT  (j�  j�  j]  �rU  j�  hK�rV  hch(��������trW  �rX  u�rY  �rZ  j�  j�  }r[  (j�  j�  j  �r\  j�  hK�r]  hch(��������tr^  �r_  u�r`  �ra  j�  j�  }rb  (j�  j�  j�  �rc  j�  hK�rd  hch(��������tre  �rf  u�rg  �rh  j�  j�  }ri  (j�  j�  j�  �rj  j�  hK�rk  hch(��������trl  �rm  u�rn  �ro  j�  j�  }rp  (j�  j�  j�  �rq  j�  hK�rr  hch(��������trs  �rt  u�ru  �rv  j�  j�  }rw  (j�  j�  j  �rx  j�  hK�ry  hch(��������trz  �r{  u�r|  �r}  j�  j�  }r~  (j�  j�  j  �r  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j4  �r�  j�  hK �r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  jJ  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j_  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  js  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  e�r�  s�r�  j;  }r�  j=  hX   enemyr�  �r�  s�r�  j�  }r�  j�  h��r�  s�r�  j�  }r�  j�  j�  ]r�  (j�  j�  }r�  (j�  j�  h�r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  hj�r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  hȆr�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h�r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j"  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j?  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r   (j�  j�  j]  �r  j�  hK�r  hch(��������tr  �r  u�r  �r  j�  j�  }r  (j�  j�  j  �r  j�  hK�r	  hch(��������tr
  �r  u�r  �r  j�  j�  }r  (j�  j�  j�  �r  j�  hK�r  hch(��������tr  �r  u�r  �r  j�  j�  }r  (j�  j�  j�  �r  j�  hK�r  hch(��������tr  �r  u�r  �r  j�  j�  }r  (j�  j�  j�  �r  j�  hK�r  hch(��������tr  �r   u�r!  �r"  j�  j�  }r#  (j�  j�  j  �r$  j�  hK�r%  hch(��������tr&  �r'  u�r(  �r)  j�  j�  }r*  (j�  j�  j  �r+  j�  hK�r,  hch(��������tr-  �r.  u�r/  �r0  j�  j�  }r1  (j�  j�  j4  �r2  j�  hK �r3  hch(��������tr4  �r5  u�r6  �r7  j�  j�  }r8  (j�  j�  jJ  �r9  j�  hK�r:  hch(��������tr;  �r<  u�r=  �r>  j�  j�  }r?  (j�  j�  j_  �r@  j�  hK�rA  hch(��������trB  �rC  u�rD  �rE  j�  j�  }rF  (j�  j�  js  �rG  j�  hK�rH  hch(��������trI  �rJ  u�rK  �rL  j�  j�  }rM  (j�  j�  j�  �rN  j�  hK�rO  hch(��������trP  �rQ  u�rR  �rS  j�  j�  }rT  (j�  j�  j�  �rU  j�  hK�rV  hch(��������trW  �rX  u�rY  �rZ  j�  j�  }r[  (j�  j�  j�  �r\  j�  hK�r]  hch(��������tr^  �r_  u�r`  �ra  e�rb  s�rc  j;  }rd  j=  hX   otherre  �rf  s�rg  j�  }rh  �ri  j�  }rj  (j�  hj�  �rk  j�  hN�rl  u�rm  j.  }rn  (hhjg  �ro  j1  hK �rp  j3  hK �rq  u�rr  j�  }rs  (h�h�j�  �rt  j�  h��ru  j�  j�  ]rv  j�  j�  }rw  (j�  j�  j�  �rx  j�  hN�ry  j�  j�  Mn�rz  j�  hj�  �r{  j�  hh��r|  j�  j�  ]r}  �r~  j�  j�  j�  }r  (j�  j�  j�  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  �r�  a�r�  u�r�  j�  }r�  (hhX   Burningr�  �r�  j�  hG@      G@      �r�  �r�  j�  hK�r�  j�  hj�  �r�  j�  h��r�  u�r�  j.  }r�  (hhX	   whocares1r�  �r�  j1  hK�r�  j3  hK �r�  u�r�  j�  }r�  (h�h�h��r�  j�  h��r�  j�  j�  ]r�  j�  j�  }r�  (j�  j�  h��r�  j�  hN�r�  j�  j�  Mo�r�  j�  hh��r�  j�  hh��r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  �r�  a�r�  u�r�  j�  }r�  (hhj�  �r�  j�  hG@      G@      �r�  �r�  j�  hK�r�  j�  hj�  �r�  j�  h��r�  u�r�  j.  }r�  (hhj�  �r�  j1  hK�r�  j3  hK�r�  u�r�  j�  }r�  (h�h�jL  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j�  }r�  (hhj�  �r�  j�  hG@      G@      �r�  �r�  j�  hK�r�  j�  hj�  �r�  j�  h��r�  u�r�  j.  }r�  (hhj�  �r�  j1  hK�r�  j3  hK�r�  u�r�  j�  }r�  (h�h�j  �r�  j�  h��r�  j�  j�  ]r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hN�r�  j�  j�  Mp�r�  j�  hj  �r�  j�  hh��r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  �r�  a�r�  u�r�  j�  }r�  (hhj�  �r�  j�  hG@      G@      �r�  �r�  j�  hK�r�  j�  hj�  �r�  j�  h��r�  u�r�  j.  }r�  (hhj�  �r�  j1  hK�r�  j3  hK�r�  u�r�  j.  }r�  (hhX   FireComplete1r�  �r�  j1  hK�r�  j3  hK �r�  u�r�  X   RemoveRegionr�  }r�  (h�h�jP  �r�  X
   did_remover�  h��r�  j�  j�  ]r�  �r   u�r  j�  }r  (h�h�j]  �r  j�  h��r  j�  j�  ]r  �r  u�r  j�  }r  (h�h�ji  �r	  j�  h��r
  j�  j�  ]r  �r  u�r  j�  }r  (h�h�ju  �r  j�  h��r  j�  j�  ]r  �r  u�r  j�  }r  (h�h�j�  �r  j�  h��r  j�  j�  ]r  �r  u�r  j�  }r  (h�h�j�  �r  j�  h��r  j�  j�  ]r  �r  u�r  j�  }r   (h�h�j�  �r!  j�  h��r"  j�  j�  ]r#  �r$  u�r%  j�  }r&  (h�h�j�  �r'  j�  h��r(  j�  j�  ]r)  �r*  u�r+  j�  }r,  (h�h�j�  �r-  j�  h��r.  j�  j�  ]r/  �r0  u�r1  j�  }r2  (h�h�j�  �r3  j�  h��r4  j�  j�  ]r5  �r6  u�r7  j�  }r8  (h�h�j�  �r9  j�  h��r:  j�  j�  ]r;  �r<  u�r=  j�  }r>  (h�h�j�  �r?  j�  h��r@  j�  j�  ]rA  �rB  u�rC  j�  }rD  (h�h�j�  �rE  j�  h��rF  j�  j�  ]rG  �rH  u�rI  j�  }rJ  (h�h�j�  �rK  j�  h��rL  j�  j�  ]rM  �rN  u�rO  j�  }rP  (h�h�j�  �rQ  j�  h��rR  j�  j�  ]rS  �rT  u�rU  j�  }rV  (h�h�j  �rW  j�  h��rX  j�  j�  ]rY  �rZ  u�r[  X   RemoveMapAnimr\  }r]  (hhX
   Target_3x3r^  �r_  j�  hG@      G@"      �r`  �ra  j�  hK�rb  X   blendrc  hj�  �rd  j�  h��re  j�  h��rf  u�rg  j.  }rh  (hhX   GarvPositionri  �rj  j1  hK �rk  j3  hj^  �rl  u�rm  j�  }rn  j�  h��ro  s�rp  j�  }rq  j�  j�  ]rr  (j�  j�  }rs  (j�  j�  h�rt  j�  hK�ru  hch(��������trv  �rw  u�rx  �ry  j�  j�  }rz  (j�  j�  hj�r{  j�  hK�r|  hch(��������tr}  �r~  u�r  �r�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  hȆr�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h�r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j"  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j?  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j]  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j4  �r�  j�  hK �r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  jJ  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j_  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  js  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r   j�  hK�r  hch(��������tr  �r  u�r  �r  j�  j�  }r  (j�  j�  j�  �r  j�  hK�r  hch(��������tr	  �r
  u�r  �r  j�  j�  }r  (j�  j�  j�  �r  j�  hK�r  hch(��������tr  �r  u�r  �r  e�r  s�r  jj  }r  (j�  j�  j�  �r  j�  hJ�����r  j�  hK�r  u�r  X   TriggerCharger  }r  (j�  j�  j�  �r  j�  j�  Mn�r  X
   old_charger  hN�r   X
   new_charger!  hN�r"  u�r#  jj  }r$  (j�  j�  j  �r%  j�  hJ�����r&  j�  hK�r'  u�r(  j  }r)  (j�  j�  j  �r*  j�  j�  Mp�r+  j  hN�r,  j!  hN�r-  u�r.  jj  }r/  (j�  j�  h��r0  j�  hJ�����r1  j�  hK�r2  u�r3  j  }r4  (j�  j�  h��r5  j�  j�  Mo�r6  j  hN�r7  j!  hN�r8  u�r9  j;  }r:  j=  hj>  �r;  s�r<  jA  }r=  (j�  j�  h�r>  jD  hjG  �r?  jF  hh�r@  jI  hK�rA  jK  hN�rB  jM  hN�rC  jO  h��rD  j�
  h��rE  jR  h��rF  jT  hK�rG  u�rH  jW  }rI  (j�  j�  h�rJ  hHhHKņrK  j[  hHKņrL  u�rM  jc  }rN  (j�  j�  h�rO  hHhHKņrP  jg  hK �rQ  u�rR  jj  }rS  (j�  j�  js  �rT  j�  hJ�����rU  j�  hK�rV  u�rW  jp  }rX  (jr  hHKņrY  jt  hj  �rZ  j'  hK
�r[  jw  hK�r\  u�r]  j�  }r^  (j�  hj{  �r_  j�  hh�jn  �r`  �ra  u�rb  jj  }rc  (j�  j�  h�rd  j�  hJ�����re  j�  hK�rf  u�rg  j�  }rh  (j�  j�  h�ri  j�  hK�rj  hch(��������trk  �rl  u�rm  j�  }rn  j�  hX   Saraid attacked Soldierro  �rp  s�rq  j�  }rr  (j�  j�  h�rs  hHhHKņrt  j�  hG?�      �ru  jw  hG@(      �rv  j�  hG@*      �rw  u�rx  j�  }ry  (j�  j�  js  �rz  hHhHK��r{  j�  hG        �r|  jw  hK�r}  j�  hG?�      �r~  u�r  j�  }r�  (j�  hj   �r�  j�  hh�js  �r�  �r�  u�r�  j�  }r�  (j�  hj   �r�  j�  hjs  h�r�  �r�  u�r�  j�  }r�  (j�  hj�  �r�  j�  h(h�js  jn  KKj   tr�  �r�  u�r�  j�  }r�  (j�  hj�  �r�  j�  h(js  h�j$  KKj   tr�  �r�  u�r�  j�  }r�  (j�  hJo��r�  j�  hJa�F�r�  u�r�  j�  }r�  (h�h�jU  �r�  j�  h��r�  j�  j�  ]r�  j�  j�  }r�  (j�  j�  js  �r�  j�  hN�r�  j�  j�  Mq�r�  j�  hjU  �r�  j�  hh��r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  js  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  �r�  a�r�  u�r�  j�  }r�  (hhX   Burningr�  �r�  j�  hj~  �r�  j�  hK�r�  j�  hj�  �r�  j�  h��r�  u�r�  j.  }r�  (hhjh  �r�  j1  j�  ]r�  (hjj  �r�  hjk  �r�  hjl  �r�  hjm  �r�  e�r�  j3  hK �r�  u�r�  j.  }r�  (hhX
   FireStart2r�  �r�  j1  hK�r�  j3  hK �r�  u�r�  j.  }r�  (hhX   FireOrigin2r�  �r�  j1  hj~  �r�  j3  hK �r�  u�r�  j�  }r�  (j�  j�  h�r�  j  hK
�r�  j  hK�r�  u�r�  j�  }r�  (j�  hj  �r�  j�  hh�Kh�r�  �r�  u�r�  j�  }r�  (j�  j�  h�r�  hch(��������tr�  �r�  j�  j�  j�  }r�  (j�  j�  h�r�  j�  hjG  �r�  u�r�  �r�  j�  j�  ]r�  �r�  u�r�  jp  }r�  (jr  j�  M�r�  jt  hj�	  �r�  j'  hK�r�  jw  hK�r�  u�r�  j.  }r�  (hhX   GarveySpotsr�  �r�  j1  hK �r�  j3  hK�r�  u�r�  j.  }r�  (hhX	   ValidLocsr�  �r�  j1  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  e�r�  j3  j�  ]r�  (hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r   hj   �r  hj  �r  hj  �r  hj  �r  hj  �r  hj
  �r  hj  �r  hj  �r  hj  �r	  hj  �r
  hj  �r  hj  �r  hj  �r  hj  �r  hj  �r  hj  �r  hj   �r  hj"  �r  hj$  �r  hj&  �r  hj(  �r  e�r  u�r  j.  }r  (hhX   GarvPositionr  �r  j1  hj^  �r  j3  hK �r  u�r  j.  }r  (hhX	   ValidLocsr  �r   j1  j�  ]r!  (hKK�r"  �r#  hKK	�r$  �r%  hKK	�r&  �r'  hKK
�r(  �r)  hKK�r*  �r+  hKK�r,  �r-  hKK�r.  �r/  hKK	�r0  �r1  e�r2  j3  j�  ]r3  (hj�  �r4  hj�  �r5  hj�  �r6  hj�  �r7  e�r8  u�r9  j.  }r:  (hhj  �r;  j1  j�  ]r<  (hKK�r=  �r>  hKK	�r?  �r@  hKK	�rA  �rB  hKK
�rC  �rD  hKK�rE  �rF  hKK�rG  �rH  hKK�rI  �rJ  hKK	�rK  �rL  hKK	�rM  �rN  hKK
�rO  �rP  e�rQ  j3  j�  ]rR  (hj"  �rS  hj$  �rT  hj&  �rU  hj(  �rV  hj*  �rW  hj,  �rX  hj.  �rY  hj0  �rZ  e�r[  u�r\  j.  }r]  (hhj  �r^  j1  j�  ]r_  (hKK�r`  �ra  hKK	�rb  �rc  hKK	�rd  �re  hKK
�rf  �rg  hKK�rh  �ri  hKK�rj  �rk  hKK�rl  �rm  hKK	�rn  �ro  hKK	�rp  �rq  hKK
�rr  �rs  hKK	�rt  �ru  hKK
�rv  �rw  e�rx  j3  j�  ]ry  (hj=  �rz  hj?  �r{  hjA  �r|  hjC  �r}  hjE  �r~  hjG  �r  hjI  �r�  hjK  �r�  hjM  �r�  hjO  �r�  e�r�  u�r�  j.  }r�  (hhj  �r�  j1  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  e�r�  j3  j�  ]r�  (hj`  �r�  hjb  �r�  hjd  �r�  hjf  �r�  hjh  �r�  hjj  �r�  hjl  �r�  hjn  �r�  hjp  �r�  hjr  �r�  hjt  �r�  hjv  �r�  e�r�  u�r�  j.  }r�  (hhj  �r�  j1  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  e�r�  j3  j�  ]r�  (hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j.  }r�  (hhj  �r�  j1  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  e�r�  j3  j�  ]r�  (hj�  �r   hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r	  hj�  �r
  hj�  �r  hj�  �r  e�r  u�r  j.  }r  (hhj  �r  j1  j�  ]r  (hKK�r  �r  hKK	�r  �r  hKK	�r  �r  hKK
�r  �r  hKK�r  �r  hKK�r  �r  hKK�r  �r  hKK	�r   �r!  hKK	�r"  �r#  hKK
�r$  �r%  hKK	�r&  �r'  hKK
�r(  �r)  hKK�r*  �r+  e�r,  j3  j�  ]r-  (hj�  �r.  hj�  �r/  hj�  �r0  hj�  �r1  hj�  �r2  hj�  �r3  hj�  �r4  hj�  �r5  hj�  �r6  hj�  �r7  hj�  �r8  hj�  �r9  hj�  �r:  e�r;  u�r<  j.  }r=  (hhj  �r>  j1  j�  ]r?  (hKK�r@  �rA  hKK	�rB  �rC  hKK	�rD  �rE  hKK
�rF  �rG  hKK�rH  �rI  hKK�rJ  �rK  hKK�rL  �rM  hKK	�rN  �rO  hKK	�rP  �rQ  hKK
�rR  �rS  hKK	�rT  �rU  hKK
�rV  �rW  hKK�rX  �rY  e�rZ  j3  j�  ]r[  (hj  �r\  hj  �r]  hj  �r^  hj  �r_  hj  �r`  hj  �ra  hj  �rb  hj   �rc  hj"  �rd  hj$  �re  hj&  �rf  hj(  �rg  hj*  �rh  e�ri  u�rj  j.  }rk  (hhj  �rl  j1  j�  ]rm  (hKK�rn  �ro  hKK	�rp  �rq  hKK	�rr  �rs  hKK
�rt  �ru  hKK�rv  �rw  hKK�rx  �ry  hKK�rz  �r{  hKK	�r|  �r}  hKK	�r~  �r  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  e�r�  j3  j�  ]r�  (hj@  �r�  hjB  �r�  hjD  �r�  hjF  �r�  hjH  �r�  hjJ  �r�  hjL  �r�  hjN  �r�  hjP  �r�  hjR  �r�  hjT  �r�  hjV  �r�  hjX  �r�  e�r�  u�r�  j.  }r�  (hhj  �r�  j1  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  e�r�  j3  j�  ]r�  (hjn  �r�  hjp  �r�  hjr  �r�  hjt  �r�  hjv  �r�  hjx  �r�  hjz  �r�  hj|  �r�  hj~  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j.  }r�  (hhj  �r�  j1  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  e�r�  j3  j�  ]r   (hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r	  hj�  �r
  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  e�r  u�r  j.  }r  (hhj  �r  j1  j�  ]r  (hKK�r  �r  hKK	�r  �r  hKK	�r  �r  hKK
�r  �r  hKK�r  �r   hKK�r!  �r"  hKK�r#  �r$  hKK	�r%  �r&  hKK	�r'  �r(  hKK
�r)  �r*  hKK	�r+  �r,  hKK
�r-  �r.  hKK�r/  �r0  hKK�r1  �r2  hKK�r3  �r4  hKK�r5  �r6  hKK�r7  �r8  hKK�r9  �r:  e�r;  j3  j�  ]r<  (hj�  �r=  hj�  �r>  hj�  �r?  hj�  �r@  hj�  �rA  hj�  �rB  hj�  �rC  hj�  �rD  hj�  �rE  hj�  �rF  hj�  �rG  hj�  �rH  hj�  �rI  hj�  �rJ  hj�  �rK  hj�  �rL  hj�  �rM  hj�  �rN  e�rO  u�rP  j.  }rQ  (hhj  �rR  j1  j�  ]rS  (hKK�rT  �rU  hKK	�rV  �rW  hKK	�rX  �rY  hKK
�rZ  �r[  hKK�r\  �r]  hKK�r^  �r_  hKK�r`  �ra  hKK	�rb  �rc  hKK	�rd  �re  hKK
�rf  �rg  hKK	�rh  �ri  hKK
�rj  �rk  hKK�rl  �rm  hKK�rn  �ro  hKK�rp  �rq  hKK�rr  �rs  hKK�rt  �ru  hKK�rv  �rw  hKK	�rx  �ry  hKK
�rz  �r{  e�r|  j3  j�  ]r}  (hj  �r~  hj  �r  hj  �r�  hj  �r�  hj  �r�  hj!  �r�  hj#  �r�  hj%  �r�  hj'  �r�  hj)  �r�  hj+  �r�  hj-  �r�  hj/  �r�  hj1  �r�  hj3  �r�  hj5  �r�  hj7  �r�  hj9  �r�  e�r�  u�r�  j.  }r�  (hhj  �r�  j1  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  e�r�  j3  j�  ]r�  (hjT  �r�  hjV  �r�  hjX  �r�  hjZ  �r�  hj\  �r�  hj^  �r�  hj`  �r�  hjb  �r�  hjd  �r�  hjf  �r�  hjh  �r�  hjj  �r�  hjl  �r�  hjn  �r�  hjp  �r�  hjr  �r�  hjt  �r�  hjv  �r�  hjx  �r�  hjz  �r�  e�r�  u�r�  j.  }r�  (hhj  �r�  j1  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK
�r   �r  hKK�r  �r  hKK	�r  �r  hKK
�r  �r  e�r  j3  j�  ]r	  (hj�  �r
  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  e�r  u�r   j.  }r!  (hhj  �r"  j1  j�  ]r#  (hKK�r$  �r%  hKK	�r&  �r'  hKK	�r(  �r)  hKK
�r*  �r+  hKK�r,  �r-  hKK�r.  �r/  hKK�r0  �r1  hKK	�r2  �r3  hKK	�r4  �r5  hKK
�r6  �r7  hKK	�r8  �r9  hKK
�r:  �r;  hKK�r<  �r=  hKK�r>  �r?  hKK�r@  �rA  hKK�rB  �rC  hKK�rD  �rE  hKK�rF  �rG  hKK	�rH  �rI  hKK
�rJ  �rK  hKK�rL  �rM  hKK	�rN  �rO  hKK
�rP  �rQ  hKK�rR  �rS  e�rT  j3  j�  ]rU  (hj�  �rV  hj�  �rW  hj�  �rX  hj�  �rY  hj�  �rZ  hj�  �r[  hj�  �r\  hj�  �r]  hj�  �r^  hj�  �r_  hj�  �r`  hj�  �ra  hj�  �rb  hj�  �rc  hj�  �rd  hj�  �re  hj�  �rf  hj�  �rg  hj�  �rh  hj   �ri  hj  �rj  hj  �rk  hj  �rl  e�rm  u�rn  j.  }ro  (hhj  �rp  j1  j�  ]rq  (hKK�rr  �rs  hKK	�rt  �ru  hKK	�rv  �rw  hKK
�rx  �ry  hKK�rz  �r{  hKK�r|  �r}  hKK�r~  �r  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  e�r�  j3  j�  ]r�  (hj$  �r�  hj&  �r�  hj(  �r�  hj*  �r�  hj,  �r�  hj.  �r�  hj0  �r�  hj2  �r�  hj4  �r�  hj6  �r�  hj8  �r�  hj:  �r�  hj<  �r�  hj>  �r�  hj@  �r�  hjB  �r�  hjD  �r�  hjF  �r�  hjH  �r�  hjJ  �r�  hjL  �r�  hjN  �r�  hjP  �r�  hjR  �r�  e�r�  u�r�  j�  }r�  (hhX
   Target_3x3r�  �r�  j�  hG@      G@"      �r�  �r�  j�  hK�r�  j�  hj�  �r�  j�  h��r�  u�r�  j�  }r�  (h�h�X   Garve_0r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j.  }r�  (hhX   GarveySpotsr�  �r�  j1  hK�r�  j3  hK �r�  u�r�  j�  }r�  (h�h�X   Garve_1r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j.  }r�  (hhj�  �r�  j1  hK�r�  j3  hK�r�  u�r�  j�  }r�  (h�h�X   Garve_2r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j.  }r�  (hhj�  �r�  j1  hK�r�  j3  hK�r�  u�r�  j�  }r�  (h�h�X   Garve_3r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j.  }r�  (hhj�  �r�  j1  hK�r�  j3  hK�r�  u�r�  j�  }r�  (h�h�X   Garve_4r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r   j.  }r  (hhj�  �r  j1  hK�r  j3  hK�r  u�r  j�  }r  (h�h�X   Garve_5r  �r  j�  h��r	  j�  j�  ]r
  �r  u�r  j.  }r  (hhj�  �r  j1  hK�r  j3  hK�r  u�r  j�  }r  (h�h�X   Garve_6r  �r  j�  h��r  j�  j�  ]r  �r  u�r  j.  }r  (hhj�  �r  j1  hK�r  j3  hK�r  u�r  j�  }r  (h�h�X   Garve_7r  �r   j�  h��r!  j�  j�  ]r"  �r#  u�r$  j.  }r%  (hhj�  �r&  j1  hK�r'  j3  hK�r(  u�r)  j�  }r*  (h�h�X   Garve_8r+  �r,  j�  h��r-  j�  j�  ]r.  �r/  u�r0  j.  }r1  (hhj�  �r2  j1  hK	�r3  j3  hK�r4  u�r5  j�  }r6  (h�h�X   Garve_9r7  �r8  j�  h��r9  j�  j�  ]r:  �r;  u�r<  j.  }r=  (hhj�  �r>  j1  hK
�r?  j3  hK	�r@  u�rA  j�  }rB  (h�h�X   Garve_10rC  �rD  j�  h��rE  j�  j�  ]rF  �rG  u�rH  j.  }rI  (hhj�  �rJ  j1  hK�rK  j3  hK
�rL  u�rM  j�  }rN  (h�h�X   Garve_11rO  �rP  j�  h��rQ  j�  j�  ]rR  �rS  u�rT  j.  }rU  (hhj�  �rV  j1  hK�rW  j3  hK�rX  u�rY  j�  }rZ  (h�h�X   Garve_12r[  �r\  j�  h��r]  j�  j�  ]r^  �r_  u�r`  j.  }ra  (hhj�  �rb  j1  hK�rc  j3  hK�rd  u�re  j�  }rf  (h�h�X   Garve_13rg  �rh  j�  h��ri  j�  j�  ]rj  �rk  u�rl  j.  }rm  (hhj�  �rn  j1  hK�ro  j3  hK�rp  u�rq  j�  }rr  (h�h�X   Garve_14rs  �rt  j�  h��ru  j�  j�  ]rv  �rw  u�rx  j.  }ry  (hhj�  �rz  j1  hK�r{  j3  hK�r|  u�r}  j�  }r~  (h�h�X   Garve_15r  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j.  }r�  (hhj�  �r�  j1  hK�r�  j3  hK�r�  u�r�  j�  }r�  j�  h��r�  s�r�  j�  }r�  j�  j�  ]r�  (j�  j�  }r�  (j�  j�  h�r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  hj�r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  hȆr�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h�r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j"  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j?  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j]  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hch(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j4  �r    j�  hK �r   hch(��������tr   �r   u�r   �r   j�  j�  }r   (j�  j�  jJ  �r   j�  hK�r   hch(��������tr	   �r
   u�r   �r   j�  j�  }r   (j�  j�  j_  �r   j�  hK�r   hch(��������tr   �r   u�r   �r   j�  j�  }r   (j�  j�  js  �r   j�  hK�r   hch(��������tr   �r   u�r   �r   j�  j�  }r   (j�  j�  j�  �r   j�  hK�r   hch(��������tr   �r   u�r    �r!   j�  j�  }r"   (j�  j�  j�  �r#   j�  hK�r$   hch(��������tr%   �r&   u�r'   �r(   j�  j�  }r)   (j�  j�  j�  �r*   j�  hK�r+   hch(��������tr,   �r-   u�r.   �r/   e�r0   s�r1   jj  }r2   (j�  j�  js  �r3   j�  hJ�����r4   j�  hK�r5   u�r6   j  }r7   (j�  j�  js  �r8   j�  j�  Mq�r9   j  hN�r:   j!  hN�r;   u�r<   jR  }r=   (j�  j�  js  �r>   jD  hj~  �r?   jV  j�  jW  }r@   (j�  j�  js  �rA   jZ  j�  j[  }rB   (j�  j�  js  �rC   jD  hj~  �rD   j�  j�  j�  }rE   (j�  j�  js  �rF   j�  hN�rG   u�rH   �rI   u�rJ   �rK   u�rL   �rM   jh  j�  ]rN   �rO   jk  hN�rP   jm  hN�rQ   u�rR   j;  }rS   j=  hj�  �rT   s�rU   j�  }rV   j�  h��rW   s�rX   j�  }rY   j�  j�  ]rZ   (j�  j�  }r[   (j�  j�  h�r\   j�  hK�r]   hch(��������tr^   �r_   u�r`   �ra   j�  j�  }rb   (j�  j�  hj�rc   j�  hK�rd   hch(��������tre   �rf   u�rg   �rh   j�  j�  }ri   (j�  j�  h��rj   j�  hK�rk   hch(��������trl   �rm   u�rn   �ro   j�  j�  }rp   (j�  j�  h��rq   j�  hK�rr   hch(��������trs   �rt   u�ru   �rv   j�  j�  }rw   (j�  j�  hȆrx   j�  hK�ry   hch(��������trz   �r{   u�r|   �r}   j�  j�  }r~   (j�  j�  h�r   j�  hK�r�   hch(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j  �r�   j�  hK�r�   hch(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j"  �r�   j�  hK�r�   hch(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j?  �r�   j�  hK�r�   hch(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j]  �r�   j�  hK�r�   hch(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j  �r�   j�  hK�r�   hch(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j�  �r�   j�  hK�r�   hch(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j�  �r�   j�  hK�r�   hch(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j�  �r�   j�  hK�r�   hch(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j  �r�   j�  hK�r�   hch(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j  �r�   j�  hK�r�   hch(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j4  �r�   j�  hK �r�   hch(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  jJ  �r�   j�  hK�r�   hch(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j_  �r�   j�  hK�r�   hch(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j�  �r�   j�  hK�r�   hch(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j�  �r�   j�  hK�r�   hch(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j�  �r�   j�  hK�r�   hch(��������tr�   �r�   u�r�   �r�   e�r�   s�r�   j;  }r�   j=  hje  �r�   s�r�   j�  }r�   �r�   j�  }r�   (j�  hj�  �r�   j�  hN�r�   u�r�   j.  }r !  (hhjn  �r!  j1  hK �r!  j3  hK �r!  u�r!  j�  }r!  (h�h�jY  �r!  j�  h��r!  j�  j�  ]r!  j�  j�  }r	!  (j�  j�  h�r
!  j�  hN�r!  j�  j�  Mr�r!  j�  hjY  �r!  j�  hh��r!  j�  j�  ]r!  �r!  j�  j�  j�  }r!  (j�  j�  h�r!  j�  hK�r!  j�  hK �r!  u�r!  �r!  j�  h��r!  u�r!  �r!  a�r!  u�r!  j�  }r!  (hhX   Burningr!  �r!  j�  hG@       G@      �r!  �r !  j�  hK�r!!  j�  hj�  �r"!  j�  h��r#!  u�r$!  j.  }r%!  (hhX	   whocares2r&!  �r'!  j1  hK�r(!  j3  hK �r)!  u�r*!  j�  }r+!  (h�h�j�  �r,!  j�  h��r-!  j�  j�  ]r.!  j�  j�  }r/!  (j�  j�  j�  �r0!  j�  hN�r1!  j�  j�  Ms�r2!  j�  hj�  �r3!  j�  hh��r4!  j�  j�  ]r5!  �r6!  j�  j�  j�  }r7!  (j�  j�  j�  �r8!  j�  hK�r9!  j�  hK �r:!  u�r;!  �r<!  j�  h��r=!  u�r>!  �r?!  a�r@!  u�rA!  j�  }rB!  (hhj!  �rC!  j�  hG@      G@       �rD!  �rE!  j�  hK�rF!  j�  hj�  �rG!  j�  h��rH!  u�rI!  j.  }rJ!  (hhj&!  �rK!  j1  hK�rL!  j3  hK�rM!  u�rN!  j�  }rO!  (h�h�j�  �rP!  j�  h��rQ!  j�  j�  ]rR!  j�  j�  }rS!  (j�  j�  j  �rT!  j�  hN�rU!  j�  j�  Mt�rV!  j�  hj�  �rW!  j�  hh��rX!  j�  j�  ]rY!  �rZ!  j�  j�  j�  }r[!  (j�  j�  j  �r\!  j�  hK�r]!  j�  hK �r^!  u�r_!  �r`!  j�  h��ra!  u�rb!  �rc!  a�rd!  u�re!  j�  }rf!  (hhj!  �rg!  j�  hG@"      G@       �rh!  �ri!  j�  hK�rj!  j�  hj�  �rk!  j�  h��rl!  u�rm!  j.  }rn!  (hhj&!  �ro!  j1  hK�rp!  j3  hK�rq!  u�rr!  j�  }rs!  (h�h�j�  �rt!  j�  h��ru!  j�  j�  ]rv!  j�  j�  }rw!  (j�  j�  j�  �rx!  j�  hN�ry!  j�  j�  Mu�rz!  j�  hj�  �r{!  j�  hh��r|!  j�  j�  ]r}!  �r~!  j�  j�  j�  }r!  (j�  j�  j�  �r�!  j�  hK�r�!  j�  hK �r�!  u�r�!  �r�!  j�  h��r�!  u�r�!  �r�!  a�r�!  u�r�!  j�  }r�!  (hhj!  �r�!  j�  hG@       G@"      �r�!  �r�!  j�  hK�r�!  j�  hj�  �r�!  j�  h��r�!  u�r�!  j.  }r�!  (hhj&!  �r�!  j1  hK�r�!  j3  hK�r�!  u�r�!  j.  }r�!  (hhX   FireComplete2r�!  �r�!  j1  hK�r�!  j3  hK �r�!  u�r�!  j�  }r�!  (h�h�j�  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h�h�j�  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h�h�j�  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h�h�j�  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h�h�j�  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h�h�j  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h�h�j  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h�h�j  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h�h�j+  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h�h�j7  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h�h�jC  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h�h�jO  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h�h�j[  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h�h�jg  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h�h�js  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h�h�j  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j\  }r�!  (hhX
   Target_3x3r�!  �r�!  j�  hG@      G@"      �r "  �r"  j�  hK�r"  jc  hj�  �r"  j�  h��r"  j�  h��r"  u�r"  j.  }r"  (hhX   GarvPositionr"  �r	"  j1  hK �r
"  j3  hj^  �r"  u�r"  j�  }r"  j�  h��r"  s�r"  j�  }r"  j�  j�  ]r"  (j�  j�  }r"  (j�  j�  h�r"  j�  hK�r"  hch(��������tr"  �r"  u�r"  �r"  j�  j�  }r"  (j�  j�  hj�r"  j�  hK�r"  hch(��������tr"  �r"  u�r"  �r"  j�  j�  }r "  (j�  j�  h��r!"  j�  hK�r""  hch(��������tr#"  �r$"  u�r%"  �r&"  j�  j�  }r'"  (j�  j�  h��r("  j�  hK�r)"  hch(��������tr*"  �r+"  u�r,"  �r-"  j�  j�  }r."  (j�  j�  hȆr/"  j�  hK�r0"  hch(��������tr1"  �r2"  u�r3"  �r4"  j�  j�  }r5"  (j�  j�  h�r6"  j�  hK�r7"  hch(��������tr8"  �r9"  u�r:"  �r;"  j�  j�  }r<"  (j�  j�  j  �r="  j�  hK�r>"  hch(��������tr?"  �r@"  u�rA"  �rB"  j�  j�  }rC"  (j�  j�  j"  �rD"  j�  hK�rE"  hch(��������trF"  �rG"  u�rH"  �rI"  j�  j�  }rJ"  (j�  j�  j?  �rK"  j�  hK�rL"  hch(��������trM"  �rN"  u�rO"  �rP"  j�  j�  }rQ"  (j�  j�  j]  �rR"  j�  hK�rS"  hch(��������trT"  �rU"  u�rV"  �rW"  j�  j�  }rX"  (j�  j�  j  �rY"  j�  hK�rZ"  hch(��������tr["  �r\"  u�r]"  �r^"  j�  j�  }r_"  (j�  j�  j�  �r`"  j�  hK�ra"  hch(��������trb"  �rc"  u�rd"  �re"  j�  j�  }rf"  (j�  j�  j�  �rg"  j�  hK�rh"  hch(��������tri"  �rj"  u�rk"  �rl"  j�  j�  }rm"  (j�  j�  j�  �rn"  j�  hK�ro"  hch(��������trp"  �rq"  u�rr"  �rs"  j�  j�  }rt"  (j�  j�  j  �ru"  j�  hK�rv"  hch(��������trw"  �rx"  u�ry"  �rz"  j�  j�  }r{"  (j�  j�  j  �r|"  j�  hK�r}"  hch(��������tr~"  �r"  u�r�"  �r�"  j�  j�  }r�"  (j�  j�  j4  �r�"  j�  hK �r�"  hch(��������tr�"  �r�"  u�r�"  �r�"  j�  j�  }r�"  (j�  j�  jJ  �r�"  j�  hK�r�"  hch(��������tr�"  �r�"  u�r�"  �r�"  j�  j�  }r�"  (j�  j�  j_  �r�"  j�  hK�r�"  hch(��������tr�"  �r�"  u�r�"  �r�"  j�  j�  }r�"  (j�  j�  j�  �r�"  j�  hK�r�"  hch(��������tr�"  �r�"  u�r�"  �r�"  j�  j�  }r�"  (j�  j�  j�  �r�"  j�  hK�r�"  hch(��������tr�"  �r�"  u�r�"  �r�"  j�  j�  }r�"  (j�  j�  j�  �r�"  j�  hK�r�"  hch(��������tr�"  �r�"  u�r�"  �r�"  e�r�"  s�r�"  jj  }r�"  (j�  j�  j�  �r�"  j�  hJ�����r�"  j�  hK�r�"  u�r�"  j  }r�"  (j�  j�  j�  �r�"  j�  j�  Mu�r�"  j  hN�r�"  j!  hN�r�"  u�r�"  jj  }r�"  (j�  j�  j�  �r�"  j�  hJ�����r�"  j�  hK�r�"  u�r�"  j  }r�"  (j�  j�  j�  �r�"  j�  j�  Ms�r�"  j  hN�r�"  j!  hN�r�"  u�r�"  jj  }r�"  (j�  j�  j�  �r�"  j�  hJ�����r�"  j�  hK
�r�"  u�r�"  j  }r�"  (j�  j�  j�  �r�"  j�  j�  Mn�r�"  j  hN�r�"  j!  hN�r�"  u�r�"  jj  }r�"  (j�  j�  j  �r�"  j�  hJ�����r�"  j�  hK�r�"  u�r�"  j  }r�"  (j�  j�  j  �r�"  j�  j�  Mt�r�"  j  hN�r�"  j!  hN�r�"  u�r�"  jj  }r�"  (j�  j�  j  �r�"  j�  hJ�����r�"  j�  hK�r�"  u�r�"  j  }r�"  (j�  j�  j  �r�"  j�  j�  Mp�r�"  j  hN�r�"  j!  hN�r�"  u�r�"  jj  }r�"  (j�  j�  h��r�"  j�  hJ�����r�"  j�  hK
�r�"  u�r�"  j  }r�"  (j�  j�  h��r�"  j�  j�  Mo�r�"  j  hN�r�"  j!  hN�r�"  u�r�"  jj  }r�"  (j�  j�  h�r�"  j�  hJ�����r�"  j�  hK�r�"  u�r�"  j  }r�"  (j�  j�  h�r�"  j�  j�  Mr�r�"  j  hN�r�"  j!  hN�r�"  u�r�"  X	   LogDialogr�"  }r�"  (X   speakerr�"  hh�r�"  X
   plain_textr�"  hX   regular deathquote.r #  �r#  u�r#  jR  }r#  (j�  j�  h�r#  jD  hh5�r#  jV  j�  jW  }r#  (j�  j�  h�r#  jZ  j�  j[  }r#  (j�  j�  h�r	#  jD  hh5�r
#  j�  j�  j�  }r#  (j�  j�  h�r#  j�  hN�r#  u�r#  �r#  u�r#  �r#  u�r#  �r#  jh  j�  ]r#  �r#  jk  hN�r#  jm  hN�r#  u�r#  j;  }r#  j=  hj>  �r#  s�r#  jp  }r#  (jr  j�  M�r#  jt  hj�	  �r#  j'  hK�r#  jw  hK�r #  u�r!#  j.  }r"#  (hhX   GarveySpotsr##  �r$#  j1  hK �r%#  j3  hK�r&#  u�r'#  j.  }r(#  (hhX	   ValidLocsr)#  �r*#  j1  j�  ]r+#  (hKK�r,#  �r-#  hKK	�r.#  �r/#  hKK	�r0#  �r1#  hKK
�r2#  �r3#  e�r4#  j3  j�  ]r5#  (hjr  �r6#  hjt  �r7#  hjv  �r8#  hjx  �r9#  hjz  �r:#  hj|  �r;#  hj~  �r<#  hj�  �r=#  hj�  �r>#  hj�  �r?#  hj�  �r@#  hj�  �rA#  hj�  �rB#  hj�  �rC#  hj�  �rD#  hj�  �rE#  hj�  �rF#  hj�  �rG#  hj�  �rH#  hj�  �rI#  hj�  �rJ#  hj�  �rK#  hj�  �rL#  hj�  �rM#  hj�  �rN#  e�rO#  u�rP#  j.  }rQ#  (hhX   GarvPositionrR#  �rS#  j1  hj^  �rT#  j3  hK �rU#  u�rV#  j.  }rW#  (hhX	   ValidLocsrX#  �rY#  j1  j�  ]rZ#  (hKK�r[#  �r\#  hKK	�r]#  �r^#  hKK	�r_#  �r`#  hKK
�ra#  �rb#  hKK�rc#  �rd#  hKK�re#  �rf#  hKK�rg#  �rh#  hKK	�ri#  �rj#  e�rk#  j3  j�  ]rl#  (hj,#  �rm#  hj.#  �rn#  hj0#  �ro#  hj2#  �rp#  e�rq#  u�rr#  j.  }rs#  (hhjX#  �rt#  j1  j�  ]ru#  (hKK�rv#  �rw#  hKK	�rx#  �ry#  hKK	�rz#  �r{#  hKK
�r|#  �r}#  hKK�r~#  �r#  hKK�r�#  �r�#  hKK�r�#  �r�#  hKK	�r�#  �r�#  hKK	�r�#  �r�#  hKK
�r�#  �r�#  e�r�#  j3  j�  ]r�#  (hj[#  �r�#  hj]#  �r�#  hj_#  �r�#  hja#  �r�#  hjc#  �r�#  hje#  �r�#  hjg#  �r�#  hji#  �r�#  e�r�#  u�r�#  j.  }r�#  (hhjX#  �r�#  j1  j�  ]r�#  (hKK�r�#  �r�#  hKK	�r�#  �r�#  hKK	�r�#  �r�#  hKK
�r�#  �r�#  hKK�r�#  �r�#  hKK�r�#  �r�#  hKK�r�#  �r�#  hKK	�r�#  �r�#  hKK	�r�#  �r�#  hKK
�r�#  �r�#  hKK	�r�#  �r�#  hKK
�r�#  �r�#  e�r�#  j3  j�  ]r�#  (hjv#  �r�#  hjx#  �r�#  hjz#  �r�#  hj|#  �r�#  hj~#  �r�#  hj�#  �r�#  hj�#  �r�#  hj�#  �r�#  hj�#  �r�#  hj�#  �r�#  e�r�#  u�r�#  j.  }r�#  (hhjX#  �r�#  j1  j�  ]r�#  (hKK�r�#  �r�#  hKK	�r�#  �r�#  hKK	�r�#  �r�#  hKK
�r�#  �r�#  hKK�r�#  �r�#  hKK�r�#  �r�#  hKK�r�#  �r�#  hKK	�r�#  �r�#  hKK	�r�#  �r�#  hKK
�r�#  �r�#  hKK	�r�#  �r�#  hKK
�r�#  �r�#  hKK�r�#  �r�#  e�r�#  j3  j�  ]r�#  (hj�#  �r�#  hj�#  �r�#  hj�#  �r�#  hj�#  �r�#  hj�#  �r�#  hj�#  �r�#  hj�#  �r�#  hj�#  �r�#  hj�#  �r�#  hj�#  �r�#  hj�#  �r�#  hj�#  �r�#  e�r�#  u�r�#  j.  }r�#  (hhjX#  �r�#  j1  j�  ]r�#  (hKK�r�#  �r�#  hKK	�r�#  �r�#  hKK	�r�#  �r�#  hKK
�r�#  �r�#  hKK�r�#  �r�#  hKK�r�#  �r�#  hKK�r�#  �r�#  hKK	�r�#  �r�#  hKK	�r�#  �r $  hKK
�r$  �r$  hKK	�r$  �r$  hKK
�r$  �r$  hKK�r$  �r$  e�r	$  j3  j�  ]r
$  (hj�#  �r$  hj�#  �r$  hj�#  �r$  hj�#  �r$  hj�#  �r$  hj�#  �r$  hj�#  �r$  hj�#  �r$  hj�#  �r$  hj�#  �r$  hj�#  �r$  hj�#  �r$  hj�#  �r$  e�r$  u�r$  j.  }r$  (hhjX#  �r$  j1  j�  ]r$  (hKK�r$  �r$  hKK	�r$  �r $  hKK	�r!$  �r"$  hKK
�r#$  �r$$  hKK�r%$  �r&$  hKK�r'$  �r($  hKK�r)$  �r*$  hKK	�r+$  �r,$  hKK	�r-$  �r.$  hKK
�r/$  �r0$  hKK	�r1$  �r2$  hKK
�r3$  �r4$  hKK�r5$  �r6$  e�r7$  j3  j�  ]r8$  (hj�#  �r9$  hj�#  �r:$  hj�#  �r;$  hj�#  �r<$  hj�#  �r=$  hj�#  �r>$  hj�#  �r?$  hj�#  �r@$  hj�#  �rA$  hj$  �rB$  hj$  �rC$  hj$  �rD$  hj$  �rE$  e�rF$  u�rG$  j.  }rH$  (hhjX#  �rI$  j1  j�  ]rJ$  (hKK�rK$  �rL$  hKK	�rM$  �rN$  hKK	�rO$  �rP$  hKK
�rQ$  �rR$  hKK�rS$  �rT$  hKK�rU$  �rV$  hKK�rW$  �rX$  hKK	�rY$  �rZ$  hKK	�r[$  �r\$  hKK
�r]$  �r^$  hKK	�r_$  �r`$  hKK
�ra$  �rb$  hKK�rc$  �rd$  e�re$  j3  j�  ]rf$  (hj$  �rg$  hj$  �rh$  hj!$  �ri$  hj#$  �rj$  hj%$  �rk$  hj'$  �rl$  hj)$  �rm$  hj+$  �rn$  hj-$  �ro$  hj/$  �rp$  hj1$  �rq$  hj3$  �rr$  hj5$  �rs$  e�rt$  u�ru$  j.  }rv$  (hhjX#  �rw$  j1  j�  ]rx$  (hKK�ry$  �rz$  hKK	�r{$  �r|$  hKK	�r}$  �r~$  hKK
�r$  �r�$  hKK�r�$  �r�$  hKK�r�$  �r�$  hKK�r�$  �r�$  hKK	�r�$  �r�$  hKK	�r�$  �r�$  hKK
�r�$  �r�$  hKK	�r�$  �r�$  hKK
�r�$  �r�$  hKK�r�$  �r�$  e�r�$  j3  j�  ]r�$  (hjK$  �r�$  hjM$  �r�$  hjO$  �r�$  hjQ$  �r�$  hjS$  �r�$  hjU$  �r�$  hjW$  �r�$  hjY$  �r�$  hj[$  �r�$  hj]$  �r�$  hj_$  �r�$  hja$  �r�$  hjc$  �r�$  e�r�$  u�r�$  j.  }r�$  (hhjX#  �r�$  j1  j�  ]r�$  (hKK�r�$  �r�$  hKK	�r�$  �r�$  hKK	�r�$  �r�$  hKK
�r�$  �r�$  hKK�r�$  �r�$  hKK�r�$  �r�$  hKK�r�$  �r�$  hKK	�r�$  �r�$  hKK	�r�$  �r�$  hKK
�r�$  �r�$  hKK	�r�$  �r�$  hKK
�r�$  �r�$  hKK�r�$  �r�$  hKK�r�$  �r�$  hKK�r�$  �r�$  hKK�r�$  �r�$  e�r�$  j3  j�  ]r�$  (hjy$  �r�$  hj{$  �r�$  hj}$  �r�$  hj$  �r�$  hj�$  �r�$  hj�$  �r�$  hj�$  �r�$  hj�$  �r�$  hj�$  �r�$  hj�$  �r�$  hj�$  �r�$  hj�$  �r�$  hj�$  �r�$  e�r�$  u�r�$  j.  }r�$  (hhjX#  �r�$  j1  j�  ]r�$  (hKK�r�$  �r�$  hKK	�r�$  �r�$  hKK	�r�$  �r�$  hKK
�r�$  �r�$  hKK�r�$  �r�$  hKK�r�$  �r�$  hKK�r�$  �r�$  hKK	�r�$  �r�$  hKK	�r�$  �r�$  hKK
�r�$  �r�$  hKK	�r�$  �r�$  hKK
�r�$  �r�$  hKK�r�$  �r�$  hKK�r�$  �r�$  hKK�r�$  �r�$  hKK�r�$  �r�$  hKK�r�$  �r�$  e�r�$  j3  j�  ]r�$  (hj�$  �r�$  hj�$  �r %  hj�$  �r%  hj�$  �r%  hj�$  �r%  hj�$  �r%  hj�$  �r%  hj�$  �r%  hj�$  �r%  hj�$  �r%  hj�$  �r	%  hj�$  �r
%  hj�$  �r%  hj�$  �r%  hj�$  �r%  hj�$  �r%  e�r%  u�r%  j.  }r%  (hhjX#  �r%  j1  j�  ]r%  (hKK�r%  �r%  hKK	�r%  �r%  hKK	�r%  �r%  hKK
�r%  �r%  hKK�r%  �r%  hKK�r%  �r%  hKK�r %  �r!%  hKK	�r"%  �r#%  hKK	�r$%  �r%%  hKK
�r&%  �r'%  hKK	�r(%  �r)%  hKK
�r*%  �r+%  hKK�r,%  �r-%  hKK�r.%  �r/%  hKK�r0%  �r1%  hKK�r2%  �r3%  hKK�r4%  �r5%  hKK�r6%  �r7%  e�r8%  j3  j�  ]r9%  (hj�$  �r:%  hj�$  �r;%  hj�$  �r<%  hj�$  �r=%  hj�$  �r>%  hj�$  �r?%  hj�$  �r@%  hj�$  �rA%  hj�$  �rB%  hj�$  �rC%  hj�$  �rD%  hj�$  �rE%  hj�$  �rF%  hj�$  �rG%  hj�$  �rH%  hj�$  �rI%  hj�$  �rJ%  e�rK%  u�rL%  j.  }rM%  (hhjX#  �rN%  j1  j�  ]rO%  (hKK�rP%  �rQ%  hKK	�rR%  �rS%  hKK	�rT%  �rU%  hKK
�rV%  �rW%  hKK�rX%  �rY%  hKK�rZ%  �r[%  hKK�r\%  �r]%  hKK	�r^%  �r_%  hKK	�r`%  �ra%  hKK
�rb%  �rc%  hKK	�rd%  �re%  hKK
�rf%  �rg%  hKK�rh%  �ri%  hKK�rj%  �rk%  hKK�rl%  �rm%  hKK�rn%  �ro%  hKK�rp%  �rq%  hKK�rr%  �rs%  e�rt%  j3  j�  ]ru%  (hj%  �rv%  hj%  �rw%  hj%  �rx%  hj%  �ry%  hj%  �rz%  hj%  �r{%  hj %  �r|%  hj"%  �r}%  hj$%  �r~%  hj&%  �r%  hj(%  �r�%  hj*%  �r�%  hj,%  �r�%  hj.%  �r�%  hj0%  �r�%  hj2%  �r�%  hj4%  �r�%  hj6%  �r�%  e�r�%  u�r�%  j.  }r�%  (hhjX#  �r�%  j1  j�  ]r�%  (hKK�r�%  �r�%  hKK	�r�%  �r�%  hKK	�r�%  �r�%  hKK
�r�%  �r�%  hKK�r�%  �r�%  hKK�r�%  �r�%  hKK�r�%  �r�%  hKK	�r�%  �r�%  hKK	�r�%  �r�%  hKK
�r�%  �r�%  hKK	�r�%  �r�%  hKK
�r�%  �r�%  hKK�r�%  �r�%  hKK�r�%  �r�%  hKK�r�%  �r�%  hKK�r�%  �r�%  hKK�r�%  �r�%  hKK�r�%  �r�%  hKK	�r�%  �r�%  hKK
�r�%  �r�%  e�r�%  j3  j�  ]r�%  (hjP%  �r�%  hjR%  �r�%  hjT%  �r�%  hjV%  �r�%  hjX%  �r�%  hjZ%  �r�%  hj\%  �r�%  hj^%  �r�%  hj`%  �r�%  hjb%  �r�%  hjd%  �r�%  hjf%  �r�%  hjh%  �r�%  hjj%  �r�%  hjl%  �r�%  hjn%  �r�%  hjp%  �r�%  hjr%  �r�%  e�r�%  u�r�%  j.  }r�%  (hhjX#  �r�%  j1  j�  ]r�%  (hKK�r�%  �r�%  hKK	�r�%  �r�%  hKK	�r�%  �r�%  hKK
�r�%  �r�%  hKK�r�%  �r�%  hKK�r�%  �r�%  hKK�r�%  �r�%  hKK	�r�%  �r�%  hKK	�r�%  �r�%  hKK
�r�%  �r�%  hKK	�r�%  �r�%  hKK
�r�%  �r�%  hKK�r�%  �r�%  hKK�r�%  �r�%  hKK�r�%  �r�%  hKK�r�%  �r�%  hKK�r�%  �r�%  hKK�r�%  �r�%  hKK	�r�%  �r�%  hKK
�r�%  �r�%  hKK�r�%  �r�%  e�r�%  j3  j�  ]r�%  (hj�%  �r�%  hj�%  �r�%  hj�%  �r�%  hj�%  �r�%  hj�%  �r�%  hj�%  �r�%  hj�%  �r &  hj�%  �r&  hj�%  �r&  hj�%  �r&  hj�%  �r&  hj�%  �r&  hj�%  �r&  hj�%  �r&  hj�%  �r&  hj�%  �r	&  hj�%  �r
&  hj�%  �r&  hj�%  �r&  hj�%  �r&  e�r&  u�r&  j.  }r&  (hhjX#  �r&  j1  j�  ]r&  (hKK�r&  �r&  hKK	�r&  �r&  hKK	�r&  �r&  hKK
�r&  �r&  hKK�r&  �r&  hKK�r&  �r&  hKK�r&  �r &  hKK	�r!&  �r"&  hKK	�r#&  �r$&  hKK
�r%&  �r&&  hKK	�r'&  �r(&  hKK
�r)&  �r*&  hKK�r+&  �r,&  hKK�r-&  �r.&  hKK�r/&  �r0&  hKK�r1&  �r2&  hKK�r3&  �r4&  hKK�r5&  �r6&  hKK	�r7&  �r8&  hKK
�r9&  �r:&  hKK�r;&  �r<&  hKK	�r=&  �r>&  hKK
�r?&  �r@&  e�rA&  j3  j�  ]rB&  (hj�%  �rC&  hj�%  �rD&  hj�%  �rE&  hj�%  �rF&  hj�%  �rG&  hj�%  �rH&  hj�%  �rI&  hj�%  �rJ&  hj�%  �rK&  hj�%  �rL&  hj�%  �rM&  hj�%  �rN&  hj�%  �rO&  hj�%  �rP&  hj�%  �rQ&  hj�%  �rR&  hj�%  �rS&  hj�%  �rT&  hj�%  �rU&  hj�%  �rV&  hj�%  �rW&  e�rX&  u�rY&  j.  }rZ&  (hhjX#  �r[&  j1  j�  ]r\&  (hKK�r]&  �r^&  hKK	�r_&  �r`&  hKK	�ra&  �rb&  hKK
�rc&  �rd&  hKK�re&  �rf&  hKK�rg&  �rh&  hKK�ri&  �rj&  hKK	�rk&  �rl&  hKK	�rm&  �rn&  hKK
�ro&  �rp&  hKK	�rq&  �rr&  hKK
�rs&  �rt&  hKK�ru&  �rv&  hKK�rw&  �rx&  hKK�ry&  �rz&  hKK�r{&  �r|&  hKK�r}&  �r~&  hKK�r&  �r�&  hKK	�r�&  �r�&  hKK
�r�&  �r�&  hKK�r�&  �r�&  hKK	�r�&  �r�&  hKK
�r�&  �r�&  hKK�r�&  �r�&  e�r�&  j3  j�  ]r�&  (hj&  �r�&  hj&  �r�&  hj&  �r�&  hj&  �r�&  hj&  �r�&  hj&  �r�&  hj&  �r�&  hj!&  �r�&  hj#&  �r�&  hj%&  �r�&  hj'&  �r�&  hj)&  �r�&  hj+&  �r�&  hj-&  �r�&  hj/&  �r�&  hj1&  �r�&  hj3&  �r�&  hj5&  �r�&  hj7&  �r�&  hj9&  �r�&  hj;&  �r�&  hj=&  �r�&  hj?&  �r�&  e�r�&  u�r�&  j.  }r�&  (hhjX#  �r�&  j1  j�  ]r�&  (hjM  �r�&  hjN  �r�&  hjO  �r�&  hjP  �r�&  hjQ  �r�&  hjR  �r�&  hjS  �r�&  hjT  �r�&  hjU  �r�&  hjV  �r�&  hjW  �r�&  hjX  �r�&  hjY  �r�&  hjZ  �r�&  hj[  �r�&  hj\  �r�&  hj]  �r�&  hj^  �r�&  hj_  �r�&  hj`  �r�&  hja  �r�&  hjb  �r�&  hjc  �r�&  hjd  �r�&  hje  �r�&  e�r�&  j3  j�  ]r�&  (hj]&  �r�&  hj_&  �r�&  hja&  �r�&  hjc&  �r�&  hje&  �r�&  hjg&  �r�&  hji&  �r�&  hjk&  �r�&  hjm&  �r�&  hjo&  �r�&  hjq&  �r�&  hjs&  �r�&  hju&  �r�&  hjw&  �r�&  hjy&  �r�&  hj{&  �r�&  hj}&  �r�&  hj&  �r�&  hj�&  �r�&  hj�&  �r�&  hj�&  �r�&  hj�&  �r�&  hj�&  �r�&  hj�&  �r�&  e�r�&  u�r�&  j�  }r�&  (hhX
   Target_3x3r�&  �r�&  j�  hG@      G@"      �r�&  �r�&  j�  hK�r�&  j�  hj�  �r�&  j�  h��r�&  u�r�&  j�  }r�&  (h�h�j�
  �r�&  j�  h��r�&  j�  j�  ]r�&  �r�&  u�r�&  j.  }r�&  (hhX   GarveySpotsr�&  �r�&  j1  hK�r�&  j3  hK �r�&  u�r�&  j�  }r�&  (h�h�j�
  �r�&  j�  h��r�&  j�  j�  ]r�&  �r�&  u�r�&  j.  }r�&  (hhj�&  �r�&  j1  hK�r�&  j3  hK�r�&  u�r�&  j�  }r '  (h�h�j�
  �r'  j�  h��r'  j�  j�  ]r'  �r'  u�r'  j.  }r'  (hhj�&  �r'  j1  hK�r'  j3  hK�r	'  u�r
'  j�  }r'  (h�h�j  �r'  j�  h��r'  j�  j�  ]r'  �r'  u�r'  j.  }r'  (hhj�&  �r'  j1  hK�r'  j3  hK�r'  u�r'  j�  }r'  (h�h�j  �r'  j�  h��r'  j�  j�  ]r'  �r'  u�r'  j.  }r'  (hhj�&  �r'  j1  hK�r'  j3  hK�r'  u�r '  j�  }r!'  (h�h�j  �r"'  j�  h��r#'  j�  j�  ]r$'  �r%'  u�r&'  j.  }r''  (hhj�&  �r('  j1  hK�r)'  j3  hK�r*'  u�r+'  j�  }r,'  (h�h�j  �r-'  j�  h��r.'  j�  j�  ]r/'  �r0'  u�r1'  j.  }r2'  (hhj�&  �r3'  j1  hK�r4'  j3  hK�r5'  u�r6'  j�  }r7'  (h�h�j  �r8'  j�  h��r9'  j�  j�  ]r:'  �r;'  u�r<'  j.  }r='  (hhj�&  �r>'  j1  hK�r?'  j3  hK�r@'  u�rA'  j�  }rB'  (h�h�j  �rC'  j�  h��rD'  j�  j�  ]rE'  �rF'  u�rG'  j.  }rH'  (hhj�&  �rI'  j1  hK	�rJ'  j3  hK�rK'  u�rL'  j�  }rM'  (h�h�j!  �rN'  j�  h��rO'  j�  j�  ]rP'  �rQ'  u�rR'  j.  }rS'  (hhj�&  �rT'  j1  hK
�rU'  j3  hK	�rV'  u�rW'  j�  }rX'  (h�h�j&  �rY'  j�  h��rZ'  j�  j�  ]r['  �r\'  u�r]'  j.  }r^'  (hhj�&  �r_'  j1  hK�r`'  j3  hK
�ra'  u�rb'  j�  }rc'  (h�h�j+  �rd'  j�  h��re'  j�  j�  ]rf'  �rg'  u�rh'  j.  }ri'  (hhj�&  �rj'  j1  hK�rk'  j3  hK�rl'  u�rm'  j�  }rn'  (h�h�j0  �ro'  j�  h��rp'  j�  j�  ]rq'  �rr'  u�rs'  j.  }rt'  (hhj�&  �ru'  j1  hK�rv'  j3  hK�rw'  u�rx'  j�  }ry'  (h�h�j5  �rz'  j�  h��r{'  j�  j�  ]r|'  �r}'  u�r~'  j.  }r'  (hhj�&  �r�'  j1  hK�r�'  j3  hK�r�'  u�r�'  j�  }r�'  (h�h�j:  �r�'  j�  h��r�'  j�  j�  ]r�'  �r�'  u�r�'  j.  }r�'  (hhj�&  �r�'  j1  hK�r�'  j3  hK�r�'  u�r�'  j�  }r�'  (h�h�j?  �r�'  j�  h��r�'  j�  j�  ]r�'  �r�'  u�r�'  j.  }r�'  (hhj�&  �r�'  j1  hK�r�'  j3  hK�r�'  u�r�'  j�  }r�'  j�  h��r�'  s�r�'  j�  }r�'  j�  j�  ]r�'  (j�  j�  }r�'  (j�  j�  hj�r�'  j�  hK�r�'  hch(��������tr�'  �r�'  u�r�'  �r�'  j�  j�  }r�'  (j�  j�  h��r�'  j�  hK�r�'  hch(��������tr�'  �r�'  u�r�'  �r�'  j�  j�  }r�'  (j�  j�  h��r�'  j�  hK�r�'  hch(��������tr�'  �r�'  u�r�'  �r�'  j�  j�  }r�'  (j�  j�  hȆr�'  j�  hK�r�'  hch(��������tr�'  �r�'  u�r�'  �r�'  j�  j�  }r�'  (j�  j�  h�r�'  j�  hK�r�'  hch(��������tr�'  �r�'  u�r�'  �r�'  j�  j�  }r�'  (j�  j�  j  �r�'  j�  hK�r�'  hch(��������tr�'  �r�'  u�r�'  �r�'  j�  j�  }r�'  (j�  j�  j"  �r�'  j�  hK�r�'  hch(��������tr�'  �r�'  u�r�'  �r�'  j�  j�  }r�'  (j�  j�  j?  �r�'  j�  hK�r�'  hch(��������tr�'  �r�'  u�r�'  �r�'  j�  j�  }r�'  (j�  j�  j]  �r�'  j�  hK�r�'  hch(��������tr�'  �r�'  u�r�'  �r�'  j�  j�  }r�'  (j�  j�  j  �r�'  j�  hK�r�'  hch(��������tr�'  �r�'  u�r�'  �r�'  j�  j�  }r�'  (j�  j�  j�  �r�'  j�  hK�r�'  hch(��������tr�'  �r�'  u�r�'  �r�'  j�  j�  }r�'  (j�  j�  j�  �r�'  j�  hK�r�'  hch(��������tr�'  �r�'  u�r�'  �r�'  j�  j�  }r�'  (j�  j�  j�  �r�'  j�  hK�r�'  hch(��������tr�'  �r�'  u�r�'  �r�'  j�  j�  }r�'  (j�  j�  j  �r�'  j�  hK�r�'  hch(��������tr�'  �r�'  u�r�'  �r (  j�  j�  }r(  (j�  j�  j  �r(  j�  hK�r(  hch(��������tr(  �r(  u�r(  �r(  j�  j�  }r(  (j�  j�  j4  �r	(  j�  hK �r
(  hch(��������tr(  �r(  u�r(  �r(  j�  j�  }r(  (j�  j�  jJ  �r(  j�  hK�r(  hch(��������tr(  �r(  u�r(  �r(  j�  j�  }r(  (j�  j�  j_  �r(  j�  hK�r(  hch(��������tr(  �r(  u�r(  �r(  j�  j�  }r(  (j�  j�  j�  �r(  j�  hK�r(  hch(��������tr (  �r!(  u�r"(  �r#(  j�  j�  }r$(  (j�  j�  j�  �r%(  j�  hK�r&(  hch(��������tr'(  �r((  u�r)(  �r*(  j�  j�  }r+(  (j�  j�  j�  �r,(  j�  hK�r-(  hch(��������tr.(  �r/(  u�r0(  �r1(  e�r2(  s�r3(  j;  }r4(  j=  hj�  �r5(  s�r6(  j�  }r7(  j�  h��r8(  s�r9(  j�  }r:(  j�  j�  ]r;(  (j�  j�  }r<(  (j�  j�  hj�r=(  j�  hK�r>(  hch(��������tr?(  �r@(  u�rA(  �rB(  j�  j�  }rC(  (j�  j�  h��rD(  j�  hK�rE(  hch(��������trF(  �rG(  u�rH(  �rI(  j�  j�  }rJ(  (j�  j�  h��rK(  j�  hK�rL(  hch(��������trM(  �rN(  u�rO(  �rP(  j�  j�  }rQ(  (j�  j�  hȆrR(  j�  hK�rS(  hch(��������trT(  �rU(  u�rV(  �rW(  j�  j�  }rX(  (j�  j�  h�rY(  j�  hK�rZ(  hch(��������tr[(  �r\(  u�r](  �r^(  j�  j�  }r_(  (j�  j�  j  �r`(  j�  hK�ra(  hch(��������trb(  �rc(  u�rd(  �re(  j�  j�  }rf(  (j�  j�  j"  �rg(  j�  hK�rh(  hch(��������tri(  �rj(  u�rk(  �rl(  j�  j�  }rm(  (j�  j�  j?  �rn(  j�  hK�ro(  hch(��������trp(  �rq(  u�rr(  �rs(  j�  j�  }rt(  (j�  j�  j]  �ru(  j�  hK�rv(  hch(��������trw(  �rx(  u�ry(  �rz(  j�  j�  }r{(  (j�  j�  j  �r|(  j�  hK�r}(  hch(��������tr~(  �r(  u�r�(  �r�(  j�  j�  }r�(  (j�  j�  j�  �r�(  j�  hK�r�(  hch(��������tr�(  �r�(  u�r�(  �r�(  j�  j�  }r�(  (j�  j�  j�  �r�(  j�  hK�r�(  hch(��������tr�(  �r�(  u�r�(  �r�(  j�  j�  }r�(  (j�  j�  j�  �r�(  j�  hK�r�(  hch(��������tr�(  �r�(  u�r�(  �r�(  j�  j�  }r�(  (j�  j�  j  �r�(  j�  hK�r�(  hch(��������tr�(  �r�(  u�r�(  �r�(  j�  j�  }r�(  (j�  j�  j  �r�(  j�  hK�r�(  hch(��������tr�(  �r�(  u�r�(  �r�(  j�  j�  }r�(  (j�  j�  j4  �r�(  j�  hK �r�(  hch(��������tr�(  �r�(  u�r�(  �r�(  j�  j�  }r�(  (j�  j�  jJ  �r�(  j�  hK�r�(  hch(��������tr�(  �r�(  u�r�(  �r�(  j�  j�  }r�(  (j�  j�  j_  �r�(  j�  hK�r�(  hch(��������tr�(  �r�(  u�r�(  �r�(  j�  j�  }r�(  (j�  j�  j�  �r�(  j�  hK�r�(  hch(��������tr�(  �r�(  u�r�(  �r�(  j�  j�  }r�(  (j�  j�  j�  �r�(  j�  hK�r�(  hch(��������tr�(  �r�(  u�r�(  �r�(  j�  j�  }r�(  (j�  j�  j�  �r�(  j�  hK�r�(  hch(��������tr�(  �r�(  u�r�(  �r�(  e�r�(  s�r�(  j;  }r�(  j=  hje  �r�(  s�r�(  j�  }r�(  �r�(  j�  }r�(  (j�  hj�  �r�(  j�  hN�r�(  u�r�(  j�  }r�(  (h�h�j�
  �r�(  j�  h��r�(  j�  j�  ]r�(  �r�(  u�r�(  j�  }r�(  (h�h�j�
  �r�(  j�  h��r�(  j�  j�  ]r�(  �r�(  u�r�(  j�  }r�(  (h�h�j�
  �r�(  j�  h��r�(  j�  j�  ]r�(  �r�(  u�r�(  j�  }r�(  (h�h�j  �r�(  j�  h��r�(  j�  j�  ]r�(  �r�(  u�r�(  j�  }r�(  (h�h�j  �r�(  j�  h��r�(  j�  j�  ]r�(  �r�(  u�r�(  j�  }r�(  (h�h�j  �r�(  j�  h��r�(  j�  j�  ]r�(  �r�(  u�r�(  j�  }r�(  (h�h�j  �r�(  j�  h��r )  j�  j�  ]r)  �r)  u�r)  j�  }r)  (h�h�j  �r)  j�  h��r)  j�  j�  ]r)  �r)  u�r	)  j�  }r
)  (h�h�j  �r)  j�  h��r)  j�  j�  ]r)  �r)  u�r)  j�  }r)  (h�h�j!  �r)  j�  h��r)  j�  j�  ]r)  �r)  u�r)  j�  }r)  (h�h�j&  �r)  j�  h��r)  j�  j�  ]r)  �r)  u�r)  j�  }r)  (h�h�j+  �r)  j�  h��r)  j�  j�  ]r)  �r )  u�r!)  j�  }r")  (h�h�j0  �r#)  j�  h��r$)  j�  j�  ]r%)  �r&)  u�r')  j�  }r()  (h�h�j5  �r))  j�  h��r*)  j�  j�  ]r+)  �r,)  u�r-)  j�  }r.)  (h�h�j:  �r/)  j�  h��r0)  j�  j�  ]r1)  �r2)  u�r3)  j�  }r4)  (h�h�j?  �r5)  j�  h��r6)  j�  j�  ]r7)  �r8)  u�r9)  j\  }r:)  (hhX
   Target_3x3r;)  �r<)  j�  hG@      G@"      �r=)  �r>)  j�  hK�r?)  jc  hj�  �r@)  j�  h��rA)  j�  h��rB)  u�rC)  j.  }rD)  (hhX   GarvPositionrE)  �rF)  j1  hK �rG)  j3  hj^  �rH)  u�rI)  eKqK �rJ)  X   eventsrK)  ]rL)  X   supportsrM)  ]rN)  X   recordsrO)  }rP)  (X   killsrQ)  ]rR)  X
   KillRecordrS)  }rT)  (j�  KX	   level_nidrU)  jj  X   killerrV)  j�  X   killeerW)  j�  u�rX)  aj�  ]rY)  (X   DamageRecordrZ)  }r[)  (j�  KjU)  jj  X   dealerr\)  h�X   receiverr])  j�  X   item_nidr^)  jn  X   over_damager_)  Kj�  KX   kindr`)  j   u�ra)  jZ)  }rb)  (j�  KjU)  jj  j\)  j�  j])  j�  j^)  j�  j_)  K
j�  Kj`)  j  u�rc)  jZ)  }rd)  (j�  KjU)  jj  j\)  h�j])  js  j^)  jn  j_)  Kj�  Kj`)  j   u�re)  jZ)  }rf)  (j�  KjU)  jj  j\)  js  j])  h�j^)  j$  j_)  Kj�  Kj`)  j   u�rg)  eX   healingrh)  ]ri)  X   deathrj)  ]rk)  (jS)  }rl)  (j�  KjU)  jj  jV)  NjW)  js  u�rm)  jS)  }rn)  (j�  KjU)  jj  jV)  NjW)  hu�ro)  ej{  ]rp)  (X
   ItemRecordrq)  }rr)  (j�  KjU)  jj  X   userrs)  h�j^)  jn  u�rt)  jq)  }ru)  (j�  KjU)  jj  js)  j�  j^)  j�  u�rv)  jq)  }rw)  (j�  KjU)  jj  js)  h�j^)  jn  u�rx)  ej]  ]ry)  X   combat_resultsrz)  ]r{)  (X   CombatRecordr|)  }r})  (j�  KjU)  jj  X   attackerr~)  h�X   defenderr)  j�  X   resultr�)  j   u�r�)  j|)  }r�)  (j�  KjU)  jj  j~)  j�  j)  j�  j�)  j  u�r�)  j|)  }r�)  (j�  KjU)  jj  j~)  h�j)  js  j�)  j   u�r�)  j|)  }r�)  (j�  KjU)  jj  j~)  js  j)  h�j�)  j   u�r�)  eX   turns_takenr�)  ]r�)  (X   Recordr�)  }r�)  (j�  KjU)  jj  u�r�)  j�)  }r�)  (j�  KjU)  jj  u�r�)  j�)  }r�)  (j�  KjU)  jj  u�r�)  j�)  }r�)  (j�  KjU)  jj  u�r�)  eX   levelsr�)  ]r�)  h]r�)  (X   LevelRecordr�)  }r�)  (j�  KjU)  jj  X   unit_nidr�)  h�j�  K
hh�u�r�)  j�)  }r�)  (j�  KjU)  jj  j�)  j�  j�  Khj�  u�r�)  j�)  }r�)  (j�  KjU)  jj  j�)  h�j�  Khh�u�r�)  ej�  ]r�)  uX   speak_stylesr�)  }r�)  (X	   __defaultr�)  }r�)  (hj�)  j�"  NhNX   widthr�)  NjT  KX
   font_colorr�)  NX	   font_typer�)  X   convor�)  X
   backgroundr�)  X   message_bg_baser�)  X	   num_linesr�)  KX   draw_cursorr�)  �X   message_tailr�)  X   message_bg_tailr�)  X   transparencyr�)  G?�������X   name_tag_bgr�)  X   name_tagr�)  X   flagsr�)  cbuiltins
set
r�)  ]r�)  �r�)  Rr�)  uX   __default_textr�)  }r�)  (hj�)  j�"  NhNj�)  NjT  G?�      j�)  Nj�)  X   textr�)  j�)  X   menu_bg_baser�)  j�)  K j�)  Nj�)  Nj�)  G?�������j�)  j�)  j�)  j�)  ]r�)  �r�)  Rr�)  uX   __default_helpr�)  }r�)  (hj�)  j�"  NhNj�)  NjT  G?�      j�)  Nj�)  j�)  j�)  X   Noner�)  j�)  Kj�)  �j�)  Nj�)  G?�������j�)  j�)  j�)  j�)  ]r�)  �r�)  Rr�)  uX   noirr�)  }r�)  (hj�)  j�"  NhNj�)  NjT  Nj�)  X   whiter�)  j�)  Nj�)  X   menu_bg_darkr�)  j�)  Nj�)  Nj�)  j�)  j�)  Nj�)  Nj�)  j�)  ]r�)  �r�)  Rr�)  uX   hintr�)  }r�)  (hj�)  j�"  Nhcapp.utilities.enums
Alignments
r�)  X   centerr�)  �r�)  Rr�)  j�)  K�jT  Nj�)  Nj�)  Nj�)  X   menu_bg_parchmentr�)  j�)  Kj�)  Nj�)  j�)  j�)  Nj�)  Nj�)  j�)  ]r�)  �r�)  Rr�)  uX	   cinematicr�)  }r�)  (hj�)  j�"  Nhj�)  j�)  NjT  Nj�)  X   greyr�)  j�)  X   chapterr�)  j�)  j�)  j�)  Kj�)  �j�)  j�)  j�)  Nj�)  Nj�)  j�)  ]r�)  �r�)  Rr�)  uX	   narrationr�)  }r�)  (hj�)  j�"  NhKKn�r�)  j�)  K�jT  Nj�)  j�)  j�)  Nj�)  j�)  j�)  Nj�)  Nj�)  j�)  j�)  Nj�)  Nj�)  j�)  ]r�)  �r�)  Rr�)  uX   narration_topr�)  }r�)  (hj�)  j�"  NhKK�r�)  j�)  K�jT  Nj�)  j�)  j�)  Nj�)  j�)  j�)  Nj�)  Nj�)  j�)  j�)  Nj�)  Nj�)  j�)  ]r�)  �r�)  Rr�)  uX   clearr�)  }r�)  (hj�)  j�"  NhNj�)  NjT  Nj�)  j�)  j�)  Nj�)  j�)  j�)  Nj�)  �j�)  j�)  j�)  Nj�)  Nj�)  j�)  ]r�)  �r�)  Rr�)  uX   thought_bubbler�)  }r�)  (hj�)  j�"  NhNj�)  NjT  Nj�)  Nj�)  Nj�)  Nj�)  Nj�)  Nj�)  X   message_bg_thought_tailr�)  j�)  Nj�)  Nj�)  j�)  ]r�)  X   no_talkr�)  a�r�)  Rr�)  uX   boss_convo_leftr�)  }r�)  (hj�)  j�"  NhKHKp�r�)  j�)  K�jT  Kj�)  Nj�)  j�)  j�)  j�)  j�)  Kj�)  �j�)  j�)  j�)  G        j�)  Nj�)  j�)  ]r�)  �r�)  Rr�)  uX   boss_convo_rightr�)  }r�)  (hj�)  j�"  NhKKp�r�)  j�)  K�jT  Kj�)  Nj�)  j�)  j�)  j�)  j�)  Kj�)  �j�)  j�)  j�)  G        j�)  Nj�)  j�)  ]r�)  �r�)  Rr�)  uuX   market_itemsr�)  }r�)  X   unlocked_lorer *  ]r*  (j2  j6  j:  j>  jB  jF  eX
   dialog_logr*  ]r*  hX   regular deathquote.r*  �r*  aX   already_triggered_eventsr*  ]r*  X   talk_optionsr*  ]r	*  X   base_convosr
*  }r*  X   current_random_stater*  Ja�FX   boundsr*  (K K KKtr*  X	   roam_infor*  capp.engine.roam.roam_info
RoamInfo
r*  )�r*  }r*  (X   roamr*  �X   roam_unit_nidr*  Nubu.