�}q (X   unitsq]q(}q(X   nidqX   AlpinqX
   prefab_nidqhX   positionqNX   teamqX   playerq	X   partyq
X   EirikaqX   klassqX   AlpinVillagerqX   variantqX    qX   factionqNX   levelqKX   expqK X   genericq�X
   persistentq�X   aiqX   NoneqX   roam_aiqNX   ai_groupqhX   itemsq]q(M�M�M�eX   nameqX   AlpinqX   descqXH   An energetic young lad. Dreams of joining the army of [KINGDOM] someday.qX   tagsq]q X   Youthq!aX   statsq"}q#(X   HPq$KX   STRq%KX   MAGq&K X   SKLq'KX   SPDq(KX   LCKq)KX   DEFq*KX   RESq+KX   CONq,KX   MOVq-KuX   growthsq.}q/(h$K<h%K#h&K h'Kh(Kh)Kh*Kh+Kh,K h-KuX   growth_pointsq0}q1(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uX   stat_cap_modifiersq2}q3(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uX   starting_positionq4NX   wexpq5}q6(X   Swordq7G@       X   Lanceq8K X   Axeq9K X   Bowq:K X   Staffq;K X   Lightq<K X   Animaq=K X   Darkq>K X   Defaultq?K uX   portrait_nidq@X   AlpinqAX   affinityqBX   FireqCX   skillsqD]qE(M�M�capp.engine.source_type
SourceType
qFX   itemqG���qH�qIRqJ�qKM�X   gameqLhFX   globalqM���qN�qORqP�qQM�hLhP�qRM�hhFX   personalqS���qT�qURqV�qWM�hhV�qXeX   notesqY]qZX
   current_hpq[KX   current_manaq\K X   current_fatigueq]K X   travelerq^NX   current_guard_gaugeq_K X   built_guardq`�X   deadqa�X   action_stateqb(��������tqcX   _fieldsqd}qeX   equipped_weaponqfM�X   equipped_accessoryqgNu}qh(hX   CarlinqihhihNhX   playerqjh
hhX   CarlinVillagerqkhhhNhKhK h�h�hhhNhhh]ql(M�M�M�ehX   CarlinqmhXA   A magically gifted youngster from [TOWN]. Energetic and sporatic.qnh]qoX   Youthqpah"}qq(h$Kh%K h&Kh'Kh(Kh)Kh*Kh+Kh,Kh-Kuh.}qr(h$K2h%K h&Kh'K#h(Kh)Kh*Kh+Kh,K h-Kuh0}qs(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}qt(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}qu(h7K h8K h9K h:K h;K h<K h=K h>Kh?K uh@X   CarlinqvhBX   WindqwhD]qx(M�M�hJ�qyM�hLhP�qzM�hLhP�q{M�hihV�q|M�hihV�q}ehY]q~h[Kh\K h]K h^Nh_K h`�ha�hb(��������tqhd}q�hfM�hgNu}q�(hX   Shaylaq�hh�hNhX   playerq�h
hhX   ShaylaVillagerq�hhhNhKhK h�h�hhhNhhh]q�(M�M�M�ehX   Shaylaq�hXI   A clumsy young lass who hopes to adventure the world. Gutsy and fearless.q�h]q�X   Youthq�ah"}q�(h$Kh%Kh&K h'Kh(Kh)Kh*Kh+K h,Kh-Kuh.}q�(h$KAh%Kh&K h'Kh(Kh)Kh*K#h+Kh,K h-Kuh0}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}q�(h7K h8K h9Kh:K h;K h<K h=K h>K h?K uh@X   Shaylaq�hBX   Thunderq�hD]q�(M�M�hJ�q�M�hLhP�q�M�hLhP�q�M�h�hV�q�M�h�hV�q�ehY]q�h[Kh\K h]K h^Nh_K h`�ha�hb(��������tq�hd}q�hfM�hgNu}q�(hX   Raelinq�hh�hNhX   playerq�h
hhX   RaelinVillagerq�hhhNhKhK h�h�hhhNhhh]q�(M�M�M�ehX   Raelinq�hX8   The shut-in daughter of a fisherman. Timid and cautious.q�h]q�X   Youthq�ah"}q�(h$Kh%Kh&K h'Kh(Kh)Kh*Kh+Kh,Kh-Kuh.}q�(h$K7h%Kh&K h'Kh(K#h)Kh*Kh+Kh,K h-Kuh0}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}q�(h7K h8Kh9K h:K h;K h<K h=K h>K h?K uh@X   Raelin (LittleKyng)q�hBX   Iceq�hD]q�(M�M�hJ�q�M�hLhP�q�M�hLhP�q�M�h�hV�q�M�h�hV�q�M�NhFX   defaultq����q��q�Rq��q�ehY]q�h[Kh\K h]K h^Nh_K h`�ha�hb(��������tq�hd}q�hfM�hgNu}q�(hX   Elwynnq�hh�hNhX   playerq�h
hhX   Monkq�hhhNhKhK h�h�hhhNhhh]q�(M�M�M�ehX   Elwynnq�hXM   Former vagabond turned holy monk of [CHURCH]. Stern and generally unpleasant.q�h]q�X   Adultq�ah"}q�(h$Kh%K h&Kh'Kh(Kh)Kh*Kh+Kh,Kh-Kuh.}q�(h$K7h%K h&Kh'Kh(Kh)K
h*Kh+K#h,K h-Kuh0}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}q�(h7K h8K h9K h:K h;K h<Kh=K h>K h?K uh@X   Elwynn (LittleKyng)q�hBX   Darkq�hD]q�(M�hLhP�q�M�hLhP�q�M�h�hV�q�M�h�hV�q�M�h�hV�q�ehY]q�h[Kh\K h]K h^Nh_K h`�ha�hb(��������tq�hd}q�hfM�hgNu}q�(hX   Elspethq�hh�hNhX   playerq�h
hhX   Clericq�hX   Elspethq�hNhKhK h�h�hhhNhhh]q�(M�MxehX   Elspethq�hX2   Lifelong devotee of [CHURCH]. Humble and selfless.q�h]q�X   Adultq�ah"}q�(h$Kh%K h&Kh'Kh(Kh)Kh*K h+Kh,Kh-Kuh.}q�(h$K2h%K h&Kh'Kh(Kh)K#h*Kh+K#h,K h-Kuh0}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}q�(h7K h8K h9K h:K h;Kh<K h=K h>K h?K uh@X   Elspethq�hBX   Lightq�hD]q�(M�hLhP�q�M�hLhP�q�M�h�hV�q�M�h�hV�q�M�h�hV�q�MyNh��q�ehY]q�h[Kh\K h]K h^Nh_K h`�ha�hb(��������tq�hd}q�hfNhgNu}q�(hX   Quinleyq�hh�hNhX   playerq�h
hhX   Mageq�hX   Quinleyq�hNhKhK h�h�hhhNhhh]q�(M�M�M�ehX   Quinleyq�hXN   A genius magician who's grown tired of elemental magic. Outgoing and friendly.q�h]q�X   Adultq�ah"}q�(h$Kh%Kh&Kh'Kh(Kh)Kh*K h+Kh,Kh-Kuh.}q�(h$K2h%K h&K#h'Kh(Kh)Kh*Kh+Kh,K h-Kuh0}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}q�(h7K h8K h9K h:K h;K h<K h=Kh>K h?K uh@X   Quinleyq�hBX   Darkq�hD]q�(M�M�hJ�q�M�hLhP�q�M�hLhP�r   M�h�hV�r  M�h�hV�r  M�h�hV�r  M|Nh��r  ehY]r  h[Kh\K h]K h^Nh_K h`�ha�hb(��������tr  hd}r  hfM�hgNu}r  (hX   Saraidr	  hj	  hNhX   playerr
  h
hhX   Hunterr  hhhNhKhK h�h�hhhNhhh]r  (M�M�M�ehX   Saraidr  hX0   A fledgling huntress. Unreserved and unfiltered.r  h]r  X   Adultr  ah"}r  (h$Kh%Kh&K h'Kh(Kh)Kh*K h+Kh,Kh-Kuh.}r  (h$K2h%Kh&K h'Kh(K#h)Kh*Kh+Kh,K h-Kuh0}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r  (h7K h8K h9K h:Kh;K h<K h=K h>K h?K uh@X   Sloaner  hBX   Lightr  hD]r  (M�M�hJ�r  M�hLhP�r  M�hLhP�r  M�j	  hV�r  M�j	  hV�r  M�j	  hV�r  ehY]r  h[Kh\K h]K h^Nh_K h`�ha�hb(��������tr   hd}r!  hfM�hgNu}r"  (hX   Lamonter#  hj#  hNhX   playerr$  h
hhX   Soldierr%  hX   Lamonter&  hNhKhK h�h�hhhNhhh]r'  (M�M�M�ehX   Lamonter(  hX,   One of Leod's guards. Kindhearted and loyal.r)  h]r*  X   Adultr+  ah"}r,  (h$Kh%Kh&K h'Kh(Kh)Kh*Kh+K h,K
h-Kuh.}r-  (h$KAh%Kh&K h'Kh(Kh)K
h*K#h+Kh,K h-Kuh0}r.  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r/  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r0  (h7K h8G@6      h9K h:K h;K h<K h=K h>K h?K uh@X   Lamonter1  hBX   Icer2  hD]r3  (M�hLhP�r4  M�hLhP�r5  M�j#  hV�r6  M�j#  hV�r7  M�j#  hV�r8  M�M�hJ�r9  ehY]r:  h[Kh\K h]K h^Nh_K h`�ha�hb(��������tr;  hd}r<  hfM�hgNu}r=  (hX   Garveyr>  hj>  hNhX   playerr?  h
hhX   Archerr@  hX   GarveyrA  hNhKhK h�h�hhhNhhh]rB  (M�M�M�ehX   GarveyrC  hXA   One of Leod's guards. Dislikes his hometown and fellow villagers.rD  h]rE  X   AdultrF  ah"}rG  (h$Kh%Kh&K h'Kh(Kh)Kh*Kh+K h,K
h-Kuh.}rH  (h$K7h%K#h&K h'Kh(Kh)K
h*Kh+Kh,K h-Kuh0}rI  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}rJ  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}rK  (h7K h8K h9K h:G@6      h;K h<K h=K h>K h?K uh@X   GarveyrL  hBX   FirerM  hD]rN  (M�hLhP�rO  M�hLhP�rP  M�j>  hV�rQ  M�j>  hV�rR  M�M�hJ�rS  M{Nh��rT  ehY]rU  h[Kh\K h]K h^Nh_K h`�ha�hb(��������trV  hd}rW  hfM�hgNu}rX  (hX   LeodrY  hjY  hNhX   otherrZ  h
hhX   Manr[  hhhNhKhK h�h�hX   Noner\  hNhhh]r]  hX   Leodr^  hX0   The mayor of [TOWN]. Pompous and short tempered.r_  h]r`  X	   Ch2Ignorera  ah"}rb  (h$Kh%Kh&K h'Kh(Kh)K h*K h+K h,Kh-Kuh.}rc  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}rd  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}re  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}rf  (h7K h8K h9K h:K h;K h<K h=K h>K h?K uh@X   Leodrg  hBX   Darkrh  hD]ri  (M�hLhP�rj  M�hLhP�rk  ehY]rl  h[Kh\K h]K h^Nh_K h`�ha�hb(��������trm  hd}rn  hfNhgNu}ro  (hX   Orlarp  hjp  hNhX   otherrq  h
hhX   Fighterrr  hX   Orlars  hNhKhK h�h�hX   Nonert  hNhhh]ru  (M�M�M�ehX   Orlarv  hXK   Lumberjack, carpenter, and all around outdoorsperson. Outspoken and jovial.rw  h]rx  X   Adultry  ah"}rz  (h$Kh%Kh&K h'Kh(Kh)Kh*Kh+Kh,Kh-Kuh.}r{  (h$KAh%K#h&K h'Kh(Kh)K
h*Kh+Kh,K h-K
uh0}r|  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r}  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r~  (h7K h8K h9Kh:K h;K h<K h=K h>K h?K uh@X   Orlar  hBX   Windr�  hD]r�  (M�hLhP�r�  M hLhP�r�  Mjp  hV�r�  Mjp  hV�r�  Mjp  hV�r�  MM�hJ�r�  ehY]r�  h[Kh\K h]K h^Nh_K h`�ha�hb(��������tr�  hd}r�  hfM�hgNu}r�  (hX   Ailsar�  hj�  hNhX   otherr�  h
hhX   Lancerr�  hX   Ailsar�  hNhKhK h�h�hX   Noner�  hNhhh]r�  (M�M�M�ehX   Ailsar�  hXE   A reclusive fisherman. Quick witted and sharped tongued. Zoya's aunt.r�  h]r�  X   Adultr�  ah"}r�  (h$Kh%Kh&K h'Kh(Kh)Kh*Kh+Kh,K	h-Kuh.}r�  (h$K<h%Kh&K h'Kh(K#h)K
h*Kh+Kh,K h-K
uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7K h8Kh9K h:K h;K h<K h=K h>K h?K uh@X   Ailsar�  hBX   Thunderr�  hD]r�  (MhLhP�r�  MhLhP�r�  Mj�  hV�r�  Mj�  hV�r�  M	j�  hV�r�  M
M�hJ�r�  ehY]r�  h[Kh\K h]K h^Nh_K h`�ha�hb(��������tr�  hd}r�  hfM�hgNu}r�  (hX   Corridonr�  hj�  hNhX   otherr�  h
hhX
   Blacksmithr�  hhhNhKhK h�h�hX   Noner�  hNhhh]r�  M�ahX   Corridonr�  hX;   Town blacksmith and retired war veteran. Friendly and just.r�  h]r�  X   Adultr�  ah"}r�  (h$Kh%K h&K h'K h(Kh)Kh*Kh+Kh,Kh-Kuh.}r�  (h$KAh%K h&K h'K h(Kh)K#h*K#h+K#h,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7K h8K h9K h:K h;K h<K h=K h>K h?K uh@X   Corridonr�  hBX   Lightr�  hD]r�  (MhLhP�r�  MhLhP�r�  Mj�  hV�r�  Mj�  hV�r�  Mj�  hV�r�  Mj�  hV�r�  ehY]r�  h[Kh\K h]K h^Nh_K h`�ha�hb(��������tr�  hd}r�  hfNhgNu}r�  (hX   Laisrenr�  hj�  hNhX   otherr�  h
hhX   Cavalierr�  hX   Cathalr�  hNhKhK h�h�hX   Noner�  hNhhh]r�  (M�M�M�ehX   Laisrenr�  hXV   An aspiring recruit in [KINGDOM]'s army. Charming and outgoing. Alpin's older brother.r�  h]r�  X   Adultr�  ah"}r�  (h$Kh%Kh&K h'Kh(Kh)Kh*Kh+Kh,Kh-Kuh.}r�  (h$KAh%K#h&K#h'K#h(K#h)K#h*K#h+K#h,K h-Kuh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7G@6      h8Kh9K h:K h;K h<K h=K h>K h?K uh@X   Cathalr�  hBX   Darkr�  hD]r�  (MhLhP�r�  MhLhP�r�  Mj�  hV�r�  Mj�  hV�r�  Mj�  hFh���r�  �r�  Rr�  �r�  ehY]r�  h[Kh\K h]K h^Nh_K h`�ha�hb(��������tr�  hd}r�  hfM�hgNu}r�  (hX	   Cawthorner�  hj�  hNhX   enemyr�  h
hhX   Generalr�  hhhNhKhK h�h�hX   Noner�  hNhhh]r�  MahX	   Cawthorner�  hX   Capt. Aelfric Cawthorne:r�  h]r�  X   Bossr�  ah"}r�  (h$Kh%Kh&K h'Kh(Kh)K h*K	h+Kh,Kh-Kuh.}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7K h8Kh9K)h:K h;K h<K h=K h>K h?K uh@X	   Cawthorner�  hBX   Firer�  hD]r�  (MhLhP�r�  MhLhP�r�  M j�  hV�r�  M!j�  hV�r�  ehY]r�  h[Kh\K h]K h^Nh_K h`�ha�hb(��������tr�  hd}r�  hfMhgNu}r�  (hX   Ackermanr�  hj�  hNhX   enemyr�  h
hhX   Warriorr�  hhhNhKhK h�h�hX   AttackNoMover�  hNhhh]r�  (MMMehX   Ackermanr�  hX   Lt. Botwulf Ackerman:r   h]r  X   Bossr  ah"}r  (h$Kh%Kh&K h'Kh(Kh)K h*Kh+K h,Kh-Kuh.}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r  (h7K h8K h9Kh:Kh;K h<K h=K h>K h?K uh@X   Ackermanr  hBX   Firer	  hD]r
  (MMhLhP�r  MNhLhP�r  MOj�  hV�r  MPMhJ�r  ehY]r  h[Kh\K h]K h^Nh_K h`�ha�hb(��������tr  hd}r  hfMhgNu}r  (hX
   Brassfieldr  hj  hNhX   enemyr  h
hhX   Heror  hhhNhKhK h�h�hX   Noner  hNhhh]r  (MMMehX
   Brassfieldr  hX   Lt. Wynnflaed Brassfield:r  h]r  X   Bossr  ah"}r  (h$Kh%Kh&Kh'Kh(Kh)K h*Kh+Kh,Kh-Kuh.}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r   (h7Kh8K h9Kh:K h;K h<K h=K h>K h?K uh@X
   Brassfieldr!  hBX   Lightr"  hD]r#  (MQhLhP�r$  MRhLhP�r%  MSj  hV�r&  MTMhJ�r'  ehY]r(  h[Kh\K h]K h^Nh_K h`�ha�hb(��������tr)  hd}r*  hfMhgNu}r+  (hX   Embersonr,  hj,  hNhX   enemyr-  h
hhX   Sager.  hhhNhKhK h�h�hX   AttackInRanger/  hNhhh]r0  (M	M
MehX   Embersonr1  hX   Lt. Wilfrith Emberson:r2  h]r3  X   Bossr4  ah"}r5  (h$Kh%K h&Kh'Kh(Kh)K h*Kh+Kh,K	h-Kuh.}r6  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}r7  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r8  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r9  (h7K h8K h9K h:K h;Kh<Kh=Kh>K h?K uh@X   Embersonr:  hBX   Firer;  hD]r<  (MUhLhP�r=  MVhLhP�r>  MWj,  hV�r?  ehY]r@  h[Kh\K h]K h^Nh_K h`�ha�hb(��������trA  hd}rB  hfM	hgNu}rC  (hX	   DavenportrD  hjD  hNhX   enemyrE  h
hhX   Great_KnightrF  hhhNhKhK h�h�hX   NonerG  hNhhh]rH  (MMMMehX	   DavenportrI  hX   Lt. Osgar Davenport:rJ  h]rK  X   BossrL  ah"}rM  (h$Kh%Kh&K h'Kh(Kh)K h*Kh+Kh,Kh-Kuh.}rN  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}rO  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}rP  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}rQ  (h7Kh8Kh9Kh:K h;K h<K h=K h>K h?K uh@X	   DavenportrR  hBX   IcerS  hD]rT  (MXhLhP�rU  MYhLhP�rV  MZjD  hV�rW  M[jF  j�  �rX  M\MhJ�rY  ehY]rZ  h[Kh\K h]K h^Nh_K h`�ha�hb(��������tr[  hd}r\  hfMhgNu}r]  (hX   Kingr^  hj^  hNhX   otherr_  h
hhX   Generalr`  hX   Kingra  hNhK
hK h�h�hX   Nonerb  hNhhh]rc  M`ahX   Kingrd  hX
   King text.re  h]rf  h"}rg  (h$Kh%Kh&K h'Kh(Kh)K h*Kh+Kh,Kh-Kuh.}rh  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}ri  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}rj  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}rk  (h7Kh8Kh9Kh:K h;K h<K h=K h>K h?K uh@X   Kingrl  hBX   Lightrm  hD]rn  (MQhLhP�ro  MRhLhP�rp  ehY]rq  h[Kh\K h]K h^Nh_K h`�ha�hb(��������trr  hd}rs  hfM`hgNu}rt  (hX   Keevaru  hju  hNhX   otherrv  h
hhX   Bishoprw  hhhNhK
hK h�h�hX   Nonerx  hNhhh]ry  MaahX   Keevarz  hX.   [KINGDOM]'s Court mage who holds many secrets.r{  h]r|  h"}r}  (h$Kh%K h&Kh'Kh(Kh)K h*Kh+K	h,Kh-Kuh.}r~  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7K h8K h9K h:K h;Kh<Kh=K h>K h?K uh@X   Keevar�  hBX   Darkr�  hD]r�  (MShLhP�r�  MThLhP�r�  ehY]r�  h[Kh\K h]K h^Nh_K h`�ha�hb(��������tr�  hd}r�  hfMahgNueh]r�  (}r�  (X   uidr�  M�hX
   Iron Swordr�  hX
   Iron Swordr�  hhX	   owner_nidr�  hX	   droppabler�  �X   datar�  }r�  X   subitemsr�  ]r�  X
   componentsr�  ]r�  (X   weaponr�  N�r�  X   target_enemyr�  N�r�  X	   min_ranger�  K�r�  X	   max_ranger�  K�r�  X   damager�  K�r�  X   hitr�  K
�r�  X	   level_expr�  N�r�  X   weapon_typer�  X   Swordr�  �r�  X   critr�  K �r�  eu}r�  (j�  M�hX   HeartOfFirer�  hX    <orange>Heart Of Flames</orange>r�  hX   Fire affinity only.
+1 Damage. r�  j�  hj�  �j�  }r�  j�  ]r�  j�  ]r�  (X   status_on_holdr�  X   HeartofFirer�  �r�  X
   text_colorr�  X   oranger�  �r�  eu}r�  (j�  M�hX   Herbsr�  hX   Herbsr�  hX   Free Action.
Restores 4 HP.r�  j�  hj�  �j�  }r�  (X   usesr�  KX   starting_usesr�  Kuj�  ]r�  j�  ]r�  (X   usabler�  N�r�  X   target_allyr�  N�r�  j�  K�r�  X   uses_optionsr�  ]r�  ]r�  (X   LoseUsesOnMiss (T/F)r�  X   Fr�  X   Lose uses even on missr�  ea�r�  X   healr�  K�r�  X   map_hit_add_blendr�  ]r�  (K`K�K�e�r�  X   attack_after_combatr�  N�r�  eu}r�  (j�  M�hX   Jinxr�  hX   Jinxr�  hX,   Lowers target's Crit Avoid by 20 for 1 turn.r�  j�  hij�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  N�r�  j�  X   Darkr�  �r�  j�  ]r�  (KHK KAe�r�  X   magicr�  N�r�  X   unrepairabler�  N�r�  X   status_on_hitr�  X	   Terrifiedr�  �r�  j�  K �r�  X   battle_cast_animr�  X   Fluxr�  �r�  eu}r�  (j�  M�hX   Devilryr�  hX   Devilryr�  hX   Can't miss.
HP Cost: 4.
-2 SPD.r�  j�  hij�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  N�r�  j�  X   Darkr�  �r�  j�  ]r�  (KHK KAe�r   j�  N�r  j�  N�r  j�  K�r  j�  ]r  (]r  (X   LoseUsesOnMiss (T/F)r  j�  X   Lose uses even on missr  e]r  (X   OneLossPerCombat (T/F)r	  j�  X    Doubling doesn't cost extra usesr
  ee�r  j�  K �r  X   status_on_equipr  X   SPD2r  �r  h5K�r  X   hp_costr  K�r  j�  X   Fluxr  �r  eu}r  (j�  M�hX   HeartOfGalesr  hX   <orange>Heart Of Gales</orange>r  hX   Wind affinity only.
+1 SPD. r  j�  hij�  �j�  }r  j�  ]r  j�  ]r  (j�  X   HeartofGalesr  �r  j�  X   oranger  �r  eu}r   (j�  M�hX   Iron Axer!  hX   Iron Axer"  hhj�  h�j�  �j�  }r#  j�  ]r$  j�  ]r%  (j�  N�r&  j�  N�r'  j�  K�r(  j�  K�r)  j�  K�r*  j�  K �r+  j�  N�r,  j�  X   Axer-  �r.  j�  K �r/  eu}r0  (j�  M�hX   HeartOfLightningr1  hX#   <orange>Heart Of Lightning</orange>r2  hX!   Thunder affinity only.
+10 Crit. r3  j�  h�j�  �j�  }r4  j�  ]r5  j�  ]r6  (j�  X   HeartofLightningr7  �r8  j�  X   oranger9  �r:  eu}r;  (j�  M�hj�  hj�  hj�  j�  h�j�  �j�  }r<  (j�  Kj�  Kuj�  ]r=  j�  ]r>  (j�  N�r?  j�  N�r@  j�  K�rA  j�  j�  �rB  j�  K�rC  j�  j�  �rD  j�  N�rE  eu}rF  (j�  M�hX
   Iron LancerG  hX
   Iron LancerH  hhj�  h�j�  �j�  }rI  j�  ]rJ  j�  ]rK  (j�  N�rL  j�  N�rM  j�  K�rN  j�  K�rO  j�  K�rP  j�  K�rQ  j�  N�rR  j�  X   LancerS  �rT  j�  K �rU  eu}rV  (j�  M�hX   JavelinrW  hX   JavelinrX  hX   Cannot Counter.
SPD -2.rY  j�  h�j�  �j�  }rZ  (j�  Kj�  Kuj�  ]r[  j�  ]r\  (j�  N�r]  j�  N�r^  j�  K�r_  j�  K�r`  j�  K�ra  j�  J�����rb  j�  K �rc  j�  N�rd  j�  X   Lancere  �rf  j�  K�rg  j�  ]rh  (]ri  (X   LoseUsesOnMiss (T/F)rj  j�  X   Lose uses even on missrk  e]rl  (X   OneLossPerCombat (T/F)rm  j�  X    Doubling doesn't cost extra usesrn  ee�ro  X   cannot_counterrp  N�rq  h5K�rr  j  X   SPD2rs  �rt  eu}ru  (j�  M�hX   HeartOfFrostrv  hX   <orange>Heart Of Frost</orange>rw  hX   Ice affinity only.
+1 DEF/RES. rx  j�  h�j�  �j�  }ry  j�  ]rz  j�  ]r{  (j�  X   HeartofFrostr|  �r}  j�  X   oranger~  �r  eu}r�  (j�  M�hX   Flashr�  hX   Flashr�  hX*   Lowers target's Accuracy by 20 for 1 turn.r�  j�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K
�r�  j�  K
�r�  j�  N�r�  j�  X   Lightr�  �r�  j�  ]r�  (K�K�K e�r�  j�  N�r�  j�  N�r�  j�  X   Glimmerr�  �r�  j�  X   Dazzledr�  �r�  eu}r�  (j�  M�hX   Shimmerr�  hX   Shimmerr�  hX   Cannot miss.r�  j�  h�j�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K
�r�  j�  N�r�  j�  X   Lightr�  �r�  j�  ]r�  (K�K�K e�r�  j�  N�r�  j�  N�r�  h5K�r�  j�  K�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  j�  X   Glimmerr�  �r�  eu}r�  (j�  M�hj�  hj�  hj�  j�  h�j�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  j�  �r�  j�  K�r�  j�  j�  �r�  j�  N�r�  eu}r�  (j�  M�hX	   ChurchKeyr�  hX
   Church Keyr�  hX   Opens any door in the church.r�  j�  h�j�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  K�r�  j�  ]r�  ]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  ea�r�  X
   can_unlockr�  X7   region.nid.startswith('Door') and game.level.nid == '2'r�  �r�  eu}r�  (j�  M�hX   Entangler�  hX   Entangler�  hX'   Lowers target's Avoid by 20 for 1 turn.r�  j�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  N�r�  j�  X   Animar�  �r�  j�  ]r�  (KUKUK e�r�  j�  N�r�  j�  N�r�  j�  X	   Entangledr�  �r�  j�  K �r�  j�  X   Firer�  �r�  eu}r�  (j�  M�hX   Firer�  hX   Firer�  hX*   Effective against horseback units.
-1 SPD.r�  j�  h�j�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K �r�  j�  K�r�  j�  K�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r   (X   OneLossPerCombat (T/F)r  j�  X    Doubling doesn't cost extra usesr  ee�r  j�  N�r  j�  X   Animar  �r  j�  ]r  (K�K`K e�r  j�  N�r	  j�  N�r
  X   effective_damager  }r  (X   effective_tagsr  ]r  X   Horser  aX   effective_multiplierr  G@      X   effective_bonus_damager  K X   show_effectiveness_flashr  �u�r  h5K�r  j  X   SPD1r  �r  X   eval_warningr  X   'Horse' in unit.tagsr  �r  j�  X   Firer  �r  eu}r  (j�  M�hX   HeartOfDarknessr  hX"   <orange>Heart Of Darkness</orange>r  hX   Dark affinity only.
+15 Hit. r  j�  h�j�  �j�  }r   j�  ]r!  j�  ]r"  (j�  X   HeartofDarknessr#  �r$  j�  X   oranger%  �r&  eu}r'  (j�  M�hX   Iron Bowr(  hX   Iron Bowr)  hhj�  j	  j�  �j�  }r*  j�  ]r+  j�  ]r,  (j�  N�r-  j�  N�r.  j�  K�r/  j�  K�r0  j�  K�r1  j�  K�r2  j�  N�r3  j�  X   Bowr4  �r5  j  }r6  (j  ]r7  X   Flyingr8  aj  G@      j  K j  �u�r9  j�  K �r:  j  X   'Flying' in unit.tagsr;  �r<  eu}r=  (j�  M�hX   Longbowr>  hX   Longbowr?  hX   Cannot Counter.
SPD -1.r@  j�  j	  j�  �j�  }rA  (j�  K
j�  K
uj�  ]rB  j�  ]rC  (j�  N�rD  j�  N�rE  j�  N�rF  j�  K�rG  j�  J�����rH  j�  X   BowrI  �rJ  j�  K
�rK  j�  ]rL  (]rM  (X   LoseUsesOnMiss (T/F)rN  j�  X   Lose uses even on missrO  e]rP  (X   OneLossPerCombat (T/F)rQ  j�  X    Doubling doesn't cost extra usesrR  ee�rS  j�  K�rT  j�  K�rU  j�  K�rV  jp  N�rW  j  }rX  (j  ]rY  X   FlyingrZ  aj  G@      j  K j  �u�r[  j  X   SPD1r\  �r]  j  X   'Flying' in unit.tagsr^  �r_  eu}r`  (j�  M�hX   HeartOfRadiancera  hX"   <orange>Heart Of Radiance</orange>rb  hX   Holy affinity only.
+15 Avoid. rc  j�  j	  j�  �j�  }rd  j�  ]re  j�  ]rf  (j�  X   HeartofRadiancerg  �rh  j�  X   orangeri  �rj  eu}rk  (j�  M�hX   Steel Lancerl  hX   Steel Lancerm  hX   SPD -1.rn  j�  j#  j�  �j�  }ro  (j�  Kj�  Kuj�  ]rp  j�  ]rq  (j�  N�rr  j�  N�rs  j�  N�rt  j�  K�ru  j�  K �rv  j�  X   Lancerw  �rx  j�  K�ry  j�  ]rz  (]r{  (X   LoseUsesOnMiss (T/F)r|  j�  X   Lose uses even on missr}  e]r~  (X   OneLossPerCombat (T/F)r  j�  X    Doubling doesn't cost extra usesr�  ee�r�  j�  K �r�  j�  K�r�  j�  K�r�  j  X   SPD1r�  �r�  eu}r�  (j�  M�hX	   Axereaverr�  hX	   Axereaverr�  hX%   Reverses the weapon triangle.
SPD -1.r�  j�  j#  j�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  N�r�  j�  K�r�  j�  K �r�  j�  X   Lancer�  �r�  j�  K�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  X   reaverr�  N�r�  h5K�r�  j�  K
�r�  j�  K�r�  j�  K�r�  j  XU   target.get_weapon() and item_system.weapon_type(target, target.get_weapon()) == 'Axe'r�  �r�  eu}r�  (j�  M�hj�  hj�  hj�  j�  j#  j�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  j�  �r�  j�  K�r�  j�  j�  �r�  j�  N�r�  eu}r�  (j�  M�hX   Greatbowr�  hX	   Steel Bowr�  hX   SPD -2.r�  j�  j>  j�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  N�r�  j�  K�r�  j�  J�����r�  j�  X   Bowr�  �r�  j�  K�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  j�  K �r�  j�  K�r�  j�  K�r�  j  X   SPD2r�  �r�  j  }r�  (j  ]r�  X   Flyingr�  aj  G@      j  K j  �u�r�  j  X   'Flying' in unit.tagsr�  �r�  eu}r�  (j�  M�hX   Mini_Bowr�  hX   Mini Bowr�  hX   SPD -1.r�  j�  j>  j�  �j�  }r�  (j�  K
j�  K
uj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  N�r�  j�  K�r�  j�  J�����r�  j�  X   Bowr�  �r�  j�  K
�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  j�  K�r�  h5K�r�  j�  K�r�  j�  K�r�  j  }r�  (j  ]r�  X   Flyingr�  aj  G@      j  K j  �u�r�  j  X   SPD1r�  �r�  j  X   'Flying' in unit.tagsr�  �r�  eu}r�  (j�  M�hj�  hj�  hj�  j�  j>  j�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  j�  �r�  j�  K�r�  j�  j�  �r�  j�  N�r   eu}r  (j�  M�hX	   Steel Axer  hX	   Steel Axer  hX   SPD -2.r  j�  jp  j�  �j�  }r  (j�  Kj�  Kuj�  ]r  j�  ]r  (j�  N�r  j�  N�r	  j�  K�r
  j�  K�r  j�  K�r  j�  J�����r  j�  K �r  j�  K�r  j�  ]r  (]r  (X   LoseUsesOnMiss (T/F)r  j�  X   Lose uses even on missr  e]r  (X   OneLossPerCombat (T/F)r  j�  X    Doubling doesn't cost extra usesr  ee�r  j�  N�r  j�  X   Axer  �r  h5K�r  j  X   SPD2r  �r  eu}r  (j�  M�hX
   EmeraldAxer  hX   Emerald Axer   hX$   Doubles the weapon triangle.
SPD -1.r!  j�  jp  j�  �j�  }r"  (j�  Kj�  Kuj�  ]r#  j�  ]r$  (j�  N�r%  j�  N�r&  j�  K�r'  j�  K�r(  j�  K�r)  j�  J�����r*  j�  K
�r+  j�  K�r,  j�  ]r-  (]r.  (X   LoseUsesOnMiss (T/F)r/  j�  X   Lose uses even on missr0  e]r1  (X   OneLossPerCombat (T/F)r2  j�  X    Doubling doesn't cost extra usesr3  ee�r4  j�  N�r5  j�  X   Axer6  �r7  h5K�r8  X   double_triangler9  N�r:  j  X   SPD1r;  �r<  X   warningr=  N�r>  eu}r?  (j�  M�hj�  hj�  hj�  j�  jp  j�  �j�  }r@  (j�  Kj�  Kuj�  ]rA  j�  ]rB  (j�  N�rC  j�  N�rD  j�  K�rE  j�  j�  �rF  j�  K�rG  j�  j�  �rH  j�  N�rI  eu}rJ  (j�  M�hjl  hjm  hjn  j�  j�  j�  �j�  }rK  (j�  Kj�  Kuj�  ]rL  j�  ]rM  (j�  N�rN  j�  N�rO  j�  N�rP  j�  K�rQ  j�  K �rR  j�  jw  �rS  j�  K�rT  j�  jz  �rU  j�  K �rV  j�  K�rW  j�  K�rX  j  j�  �rY  eu}rZ  (j�  M�hX   SapphireLancer[  hX   Sapphire Lancer\  hX$   Doubles the weapon triangle.
SPD -1.r]  j�  j�  j�  �j�  }r^  (j�  Kj�  Kuj�  ]r_  j�  ]r`  (j�  N�ra  j�  N�rb  j�  N�rc  j�  K�rd  j�  K �re  j�  X   Lancerf  �rg  j�  K�rh  j�  ]ri  (]rj  (X   LoseUsesOnMiss (T/F)rk  j�  X   Lose uses even on missrl  e]rm  (X   OneLossPerCombat (T/F)rn  j�  X    Doubling doesn't cost extra usesro  ee�rp  h5K�rq  j�  K
�rr  j�  K�rs  j�  K�rt  j9  N�ru  j  X   SPD1rv  �rw  j=  N�rx  eu}ry  (j�  M�hj�  hj�  hj�  j�  j�  j�  �j�  }rz  (j�  Kj�  Kuj�  ]r{  j�  ]r|  (j�  N�r}  j�  N�r~  j�  K�r  j�  j�  �r�  j�  K�r�  j�  j�  �r�  j�  N�r�  eu}r�  (j�  M�hX   Elixirr�  hX   Elixirr�  hX   Free Action.
Fully recovers HP.r�  j�  j�  j�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  ]r�  ]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  ea�r�  j�  Kc�r�  j�  ]r�  (K`K�K�e�r�  j�  N�r�  eu}r�  (j�  M�hX   Silver Swordr�  hX   Silver Swordr�  hhj�  j�  j�  �j�  }r�  (j�  K	j�  K
uj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K
�r�  j�  K �r�  j�  N�r�  j�  X   Swordr�  �r�  j�  K
�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  eu}r�  (j�  M�hX   Silver Lancer�  hX   Silver Lancer�  hhj�  j�  j�  �j�  }r�  (j�  K
j�  K
uj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K �r�  j�  N�r�  j�  X   Lancer�  �r�  j�  K
�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  eu}r�  (j�  M�hX   TheReclaimerr�  hX   <blue>The Reclaimer<blue>r�  hX7   Cavalier line only.
Effective Vs. Armor.
50% Lifelink. r�  j�  j�  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  J�����r�  j�  K
�r�  j�  N�r�  j�  X   Lancer�  �r�  X	   prf_classr�  ]r�  (X   Cavalierr�  X   Paladinr�  X   Great_Knightr�  e�r�  j  }r�  (j  ]r�  X   Armorr�  aj  G@      j  K j  �u�r�  j�  X   bluer�  �r�  X   lifelinkr�  G?�      �r�  eu}r�  (j�  MhX
   TheDefilerr�  hX   <blue>The Defiler</blue>r�  hXF   Knight line only.
Effective Vs. Clergy.
Inflicts half damage on miss. r�  j�  j�  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  J�����r�  j�  K�r�  j�  N�r�  j�  X   Axer�  �r�  j�  ]r�  (X   Knightr�  X   Generalr�  X   Great_Knightr�  e�r�  j  }r�  (j  ]r�  X   Clergyr   aj  G@      j  K j  �u�r  j�  X   bluer  �r  X   damage_on_missr  G?�      �r  eu}r  (j�  MhX   LunarArcr  hX	   Lunar Arcr  hX"   Ignores DEF.
Grants Close Counter.r	  j�  j�  j�  �j�  }r
  j�  ]r  j�  ]r  (j  X   CloseCounterr  �r  j�  N�r  j�  N�r  j�  N�r  j�  K�r  X   alternate_resist_formular  X   ZEROr  �r  j�  K�r  j�  X   Bowr  �r  j�  K
�r  j�  K�r  j�  K�r  j  }r  (j  ]r  X   Flyingr  aj  G@      j  K j  �u�r  j  X   'Flying' in unit.tagsr   �r!  eu}r"  (j�  Mhj  hj   hj!  j�  j�  j�  �j�  }r#  (j�  Kj�  Kuj�  ]r$  j�  ]r%  (j�  N�r&  j�  N�r'  j�  K�r(  j�  K�r)  j�  K�r*  j�  J�����r+  j�  K
�r,  j�  K�r-  j�  j-  �r.  j�  N�r/  j�  j6  �r0  h5K�r1  j9  N�r2  j  j;  �r3  j=  N�r4  eu}r5  (j�  MhX   AngelFeatherr6  hX   <yellow>Angel Feather</yellow>r7  hX&   Gives unit +10% growths. Cannot stack.r8  j�  j�  j�  �j�  }r9  (j�  Kj�  Kuj�  ]r:  j�  ]r;  (j�  N�r<  j�  N�r=  j�  K�r>  j�  ]r?  (]r@  (X   LoseUsesOnMiss (T/F)rA  j�  X   Lose uses even on missrB  e]rC  (X   OneLossPerCombat (T/F)rD  j�  X    Doubling doesn't cost extra usesrE  ee�rF  X   usable_in_baserG  N�rH  j�  X   yellowrI  �rJ  j�  ]rK  (K�K�K�e�rL  j�  X   AngelFeatherrM  �rN  j�  K �rO  j�  K �rP  X   eval_availablerQ  X#   not has_skill(unit, 'AngelFeather')rR  �rS  X   event_on_hitrT  X   Global AngelFeatherrU  �rV  eu}rW  (j�  MhX   HowlingBladerX  hX   <blue>Howling Blade</blue>rY  hX4   Grants Distant Counter.
Deals magic damage at range.rZ  j�  j  j�  �j�  }r[  j�  ]r\  j�  ]r]  (j�  N�r^  j�  N�r_  j�  K�r`  j�  K�ra  j�  K�rb  j�  K�rc  j�  K
�rd  j�  N�re  j�  X   Swordrf  �rg  j�  X   bluerh  �ri  X   magic_at_rangerj  N�rk  j  X   DistantCounterrl  �rm  eu}rn  (j�  MhX   Macero  hX   Macerp  hX   Cannot be Countered.
SPD -2.rq  j�  j  j�  �j�  }rr  (j�  K
j�  K
uj�  ]rs  j�  ]rt  (j�  N�ru  j�  N�rv  j�  N�rw  j�  K�rx  j�  J�����ry  j�  X   Axerz  �r{  j�  K
�r|  j�  ]r}  (]r~  (X   LoseUsesOnMiss (T/F)r  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  j�  K�r�  h5K�r�  j�  K�r�  j�  K�r�  X   cannot_be_counteredr�  N�r�  j  X   SPD2r�  �r�  eu}r�  (j�  Mhj6  hj7  hj8  j�  j  j�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  j?  �r�  jG  N�r�  j�  jI  �r�  j�  jK  �r�  j�  jM  �r�  j�  K �r�  j�  K �r�  jQ  jR  �r�  jT  jU  �r�  eu}r�  (j�  M	hX
   Incinerater�  hX
   Incinerater�  hX   Ignores RES.
Cooldown: 1.r�  j�  j,  j�  �j�  }r�  (X   cooldownr�  K X   starting_cooldownr�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K �r�  j�  N�r�  j�  X   Animar�  �r�  j�  ]r�  (K�K K e�r�  j�  N�r�  j  X   ZEROr�  �r�  j�  N�r�  j�  K
�r�  j�  X   Firer�  �r�  j�  K�r�  eu}r�  (j�  M
hj�  hj�  hj�  j�  j,  j�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K �r�  j�  K�r�  j�  K�r�  j�  j�  �r�  j�  N�r�  j�  j  �r�  j�  j  �r�  j�  N�r�  j�  N�r�  j  }r�  (j  j  j  G@      j  K j  �u�r�  h5K�r�  j  j  �r�  j  j  �r�  j�  j  �r�  eu}r�  (j�  Mhj6  hj7  hj8  j�  j,  j�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  j?  �r�  jG  N�r�  j�  jI  �r�  j�  jK  �r�  j�  jM  �r�  j�  K �r�  j�  K �r�  jQ  jR  �r�  jT  jU  �r�  eu}r�  (j�  MhX   WardensCleaverr�  hX   <blue>Warden's Cleaver<blue>r�  hX&   While equipped unit cannot be doubled.r�  j�  jD  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K
�r�  j�  N�r�  j�  X   Axer�  �r�  j�  X   bluer�  �r�  j  X   Wardenr�  �r�  eu}r�  (j�  MhX
   BloodLancer�  hX   Blood Lancer�  hX   Has Lifelink.
SPD -1.r�  j�  jD  j�  �j�  }r�  (j�  K
j�  K
uj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  N�r�  j�  K�r   j�  J�����r  j�  X   Lancer  �r  j�  K
�r  j�  ]r  (]r  (X   LoseUsesOnMiss (T/F)r  j�  X   Lose uses even on missr  e]r	  (X   OneLossPerCombat (T/F)r
  j�  X    Doubling doesn't cost extra usesr  ee�r  j�  K�r  j�  K�r  j�  K�r  j�  G?�      �r  h5K�r  j  X   SPD1r  �r  eu}r  (j�  MhX   EtherealBlader  hX   Ethereal Blader  hX   Targets RES.r  j�  jD  j�  �j�  }r  (j�  K
j�  K
uj�  ]r  j�  ]r  (j�  N�r  j�  N�r  j�  K�r  j�  K�r  j�  K�r  j�  K�r   j�  K�r!  j�  K
�r"  j�  ]r#  (]r$  (X   LoseUsesOnMiss (T/F)r%  j�  X   Lose uses even on missr&  e]r'  (X   OneLossPerCombat (T/F)r(  j�  X    Doubling doesn't cost extra usesr)  ee�r*  j�  N�r+  j�  X   Swordr,  �r-  j  X   MAGIC_DEFENSEr.  �r/  h5K�r0  eu}r1  (j�  Mhj6  hj7  hj8  j�  jD  j�  �j�  }r2  (j�  Kj�  Kuj�  ]r3  j�  ]r4  (j�  N�r5  j�  N�r6  j�  K�r7  j�  j?  �r8  jG  N�r9  j�  jI  �r:  j�  jK  �r;  j�  jM  �r<  j�  K �r=  j�  K �r>  jQ  jR  �r?  jT  jU  �r@  eu}rA  (j�  M`hj�  hj�  hhj�  j^  j�  �j�  }rB  (j�  K
j�  K
uj�  ]rC  j�  ]rD  (j�  N�rE  j�  N�rF  j�  K�rG  j�  K�rH  j�  K�rI  j�  K
�rJ  j�  K �rK  j�  N�rL  j�  j�  �rM  j�  K
�rN  j�  j�  �rO  eu}rP  (j�  Mahj�  hj�  hj�  j�  ju  j�  �j�  }rQ  j�  ]rR  j�  ]rS  (j�  N�rT  j�  N�rU  j�  K�rV  j�  K�rW  j�  K�rX  j�  K
�rY  j�  K
�rZ  j�  N�r[  j�  j�  �r\  j�  j�  �r]  j�  N�r^  j�  N�r_  j�  j�  �r`  j�  j�  �ra  eu}rb  (j�  MbhX
   Debilitaterc  hX
   Debilitaterd  hX9   Reduces target's stats by 1 for 2 turns.
Chapter Uses: 3.re  j�  Nj�  �j�  }rf  (X   c_usesrg  KX   starting_c_usesrh  Kuj�  ]ri  j�  ]rj  (X   spellrk  N�rl  j�  X   Staffrm  �rn  j�  N�ro  h5K�rp  hK!�rq  j�  N�rr  jg  K�rs  j�  ]rt  (]ru  (X   LoseUsesOnMiss (T/F)rv  j�  X   Lose uses even on missrw  e]rx  (X   OneLossPerCombat (T/F)ry  j�  X    Doubling doesn't cost extra usesrz  ee�r{  j�  K�r|  X   max_equation_ranger}  X   MAGIC_RANGEr~  �r  j�  X   Debilitatedr�  �r�  X   map_hit_sub_blendr�  ]r�  (K�K0Ke�r�  j�  X   Stunr�  �r�  X   ignore_line_of_sightr�  N�r�  eu}r�  (j�  Mdhj6  hj7  hj8  j�  Nj�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  ]r�  (]r�  (jA  j�  jB  e]r�  (jD  j�  jE  ee�r�  jG  N�r�  j�  jI  �r�  j�  ]r�  (K�K�K�e�r�  j�  jM  �r�  j�  K �r�  j�  K �r�  jQ  jR  �r�  jT  jU  �r�  eu}r�  (j�  MehX   Steel Swordr�  hX   Steel Swordr�  hX   SPD -1.r�  j�  Nj�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K �r�  j�  K�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  j�  N�r�  j�  X   Swordr�  �r�  j  X   SPD1r�  �r�  eu}r�  (j�  Mfhj  hj  hj  j�  Nj�  �j�  }r�  (j�  K
j�  K
uj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K
�r�  j�  ]r�  (]r�  (j%  j�  j&  e]r�  (j(  j�  j)  ee�r�  j�  N�r�  j�  j,  �r�  j  j.  �r�  h5K�r�  eu}r�  (j�  MghX	   RubySwordr�  hX
   Ruby Swordr�  hX   Doubles the weapon triangle.r�  j�  Nj�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K
�r�  j�  N�r�  j�  X   Swordr�  �r�  j�  K�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  h5K�r�  j9  N�r�  j=  N�r�  eu}r�  (j�  MhhX   Hand Axer�  hX   Hand Axer�  hX   Cannot Counter.
SPD -3.r�  j�  Nj�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  J�����r�  j�  K �r�  j�  K�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r   j�  N�r  j�  X   Axer  �r  jp  N�r  h5K�r  j  X   SPD3r  �r  eu}r  (j�  Mihjo  hjp  hjq  j�  Nj�  �j�  }r	  (j�  K
j�  K
uj�  ]r
  j�  ]r  (j�  N�r  j�  N�r  j�  N�r  j�  K�r  j�  J�����r  j�  jz  �r  j�  K
�r  j�  ]r  (]r  (j  j�  j�  e]r  (j�  j�  j�  ee�r  j�  K�r  h5K�r  j�  K�r  j�  K�r  j�  N�r  j  j�  �r  eu}r  (j�  MjhX   AxeOfWoer  hX   <purple>Axe Of Woe</purple>r  hX   Shayla Only.
+2 DEF.r   j�  Nj�  �j�  }r!  (j�  Kj�  Kuj�  ]r"  j�  ]r#  (j�  N�r$  j�  N�r%  j�  K�r&  j�  K�r'  j�  K�r(  j�  K�r)  j�  K�r*  j�  N�r+  j�  X   Axer,  �r-  X   prf_unitr.  ]r/  X   Shaylar0  a�r1  j  X   DEFr2  �r3  j�  X   purpler4  �r5  j�  K�r6  j�  ]r7  (]r8  (X   LoseUsesOnMiss (T/F)r9  j�  X   Lose uses even on missr:  e]r;  (X   OneLossPerCombat (T/F)r<  j�  X    Doubling doesn't cost extra usesr=  ee�r>  eu}r?  (j�  MkhX   Hammerr@  hX   HammerrA  hX(   Effective against armored units.
SPD -1.rB  j�  Nj�  �j�  }rC  (j�  Kj�  Kuj�  ]rD  j�  ]rE  (j�  N�rF  j�  N�rG  j�  N�rH  j�  K�rI  j�  J�����rJ  j�  X   AxerK  �rL  j�  K�rM  j�  ]rN  (]rO  (X   LoseUsesOnMiss (T/F)rP  j�  X   Lose uses even on missrQ  e]rR  (X   OneLossPerCombat (T/F)rS  j�  X    Doubling doesn't cost extra usesrT  ee�rU  j�  K�rV  h5K�rW  j�  K�rX  j�  K�rY  j  }rZ  (j  ]r[  X   Armorr\  aj  G@      j  K j  �u�r]  j  X   SPD1r^  �r_  j  X   'Armor' in unit.tagsr`  �ra  eu}rb  (j�  Mlhj�  hj�  hj�  j�  Nj�  �j�  }rc  (j�  K
j�  K
uj�  ]rd  j�  ]re  (j�  N�rf  j�  N�rg  j�  N�rh  j�  K�ri  j�  J�����rj  j�  j  �rk  j�  K
�rl  j�  ]rm  (]rn  (j  j�  j  e]ro  (j
  j�  j  ee�rp  j�  K�rq  j�  K�rr  j�  K�rs  j�  G?�      �rt  h5K�ru  j  j  �rv  eu}rw  (j�  MmhX   Horseslayerrx  hX   Horseslayerry  hX*   Effective against horseback units.
SPD -1.rz  j�  Nj�  �j�  }r{  (j�  Kj�  Kuj�  ]r|  j�  ]r}  (j�  N�r~  j�  N�r  j�  N�r�  j�  K�r�  j�  K �r�  j�  X   Lancer�  �r�  j�  K�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  j�  K�r�  j�  K�r�  j�  K�r�  h5K�r�  j  }r�  (j  ]r�  X   Horser�  aj  G@      j  K j  �u�r�  j  X   SPD1r�  �r�  j  X   'Horse' in unit.tagsr�  �r�  eu}r�  (j�  Mnhj�  hj�  hj�  j�  Nj�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  J�����r�  j�  K
�r�  j�  N�r�  j�  j�  �r�  j�  ]r�  (j�  j�  j�  e�r�  j  }r�  (j  j�  j  G@      j  K j  �u�r�  j�  j�  �r�  j�  G?�      �r�  eu}r�  (j�  MohX	   Steel Bowr�  hX	   Steel Bowr�  hX   SPD -1.r�  j�  Nj�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  N�r�  j�  K�r�  j�  K �r�  j�  X   Bowr�  �r�  j�  K�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  j�  K �r�  j�  K�r�  j�  K�r�  j  X   SPD1r�  �r�  j  }r�  (j  ]r�  X   Flyingr�  aj  G@      j  K j  �u�r�  j  X   'Flying' in unit.tagsr�  �r�  eu}r�  (j�  MphX   EnchantedBowr�  hX   Enchanted Bowr�  hX   Targets RES.
SPD -1.r�  j�  Nj�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  N�r�  j�  K�r�  j�  J�����r�  j�  X   Bowr�  �r�  j�  K�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  j�  K
�r�  j�  K�r�  j�  K�r�  j  X   MAGIC_DEFENSEr�  �r�  j  X   SPD1r�  �r�  j  }r�  (j  ]r�  X   Flyingr�  aj  Kj  K j  �u�r�  eu}r�  (j�  MqhX   Firestarterr�  hX   <purple>Firestarter</purple>r�  hX=   Saraid Only.
Sets terrain ablaze after combat.
Cannot double.r�  j�  Nj�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K �r�  j�  K�r�  j�  ]r 	  (]r	  (X   LoseUsesOnMiss (T/F)r	  j�  X   Lose uses even on missr	  e]r	  (X   OneLossPerCombat (T/F)r	  j�  X    Doubling doesn't cost extra usesr	  ee�r	  j�  N�r	  j�  X   Bowr		  �r
	  j.  ]r	  X   Saraidr	  a�r	  j�  X   purpler	  �r	  j  }r	  (j  ]r	  X   Flyingr	  aj  G@       j  K j  �u�r	  X   event_after_combat_even_missr	  X   Global Firestarter_Originr	  �r	  X	   no_doubler	  N�r	  eu}r	  (j�  MrhX   Omnipotencer	  hX   Omnipotencer	  hX'   Effective against caster units.
SPD -1.r	  j�  Nj�  �j�  }r	  (j�  Kj�  Kuj�  ]r	  j�  ]r	  (j�  N�r 	  j�  N�r!	  j�  N�r"	  j�  K�r#	  j�  K �r$	  j�  X   Lightr%	  �r&	  j�  N�r'	  j�  K�r(	  j�  ]r)	  (]r*	  (X   LoseUsesOnMiss (T/F)r+	  j�  X   Lose uses even on missr,	  e]r-	  (X   OneLossPerCombat (T/F)r.	  j�  X    Doubling doesn't cost extra usesr/	  ee�r0	  j�  K�r1	  h5K�r2	  j�  K�r3	  j�  K�r4	  j�  ]r5	  (K�K�K e�r6	  j�  N�r7	  j  }r8	  (j  ]r9	  X   Casterr:	  aj  G@      j  K j  �u�r;	  j  X   SPD1r<	  �r=	  j  X   'Caster' in unit.tagsr>	  �r?	  j�  X   Glimmerr@	  �rA	  eu}rB	  (j�  MshX   FlairrC	  hX   <purple>Flair</purple>rD	  hX`   Elwynn Only.
Steals any unequipped item from target.
Can't miss, double, kill, or target bosses.rE	  j�  Nj�  �j�  }rF	  (j�  K
j�  K
X   target_itemrG	  Nuj�  ]rH	  j�  ]rI	  (j�  N�rJ	  j�  N�rK	  j�  K�rL	  j�  K�rM	  j�  K�rN	  j�  K
�rO	  j�  ]rP	  (]rQ	  (X   LoseUsesOnMiss (T/F)rR	  j�  X   Lose uses even on missrS	  e]rT	  (X   OneLossPerCombat (T/F)rU	  j�  X    Doubling doesn't cost extra usesrV	  ee�rW	  j�  N�rX	  j�  X   LightrY	  �rZ	  j�  ]r[	  (K�K�K e�r\	  j�  N�r]	  j.  ]r^	  X   Elwynnr_	  a�r`	  j�  X   purplera	  �rb	  j�  X	   NonLethalrc	  �rd	  X   stealre	  N�rf	  j�  X   Glimmerrg	  �rh	  X   eval_target_restrict_2ri	  X   'Boss' not in target.tagsrj	  �rk	  eu}rl	  (j�  MthX   Shadowflamerm	  hX   <purple>Shadowflame</purple>rn	  hXI   Quinley Only.
Permanently reduces target's LUK, DEF and RES by 1. Stacks.ro	  j�  Nj�  �j�  }rp	  (j�  K
j�  K
uj�  ]rq	  j�  ]rr	  (j�  N�rs	  j�  N�rt	  j�  K�ru	  j�  K�rv	  j�  K�rw	  j�  K �rx	  j�  K
�ry	  j�  ]rz	  (]r{	  (X   LoseUsesOnMiss (T/F)r|	  j�  X   Lose uses even on missr}	  e]r~	  (X   OneLossPerCombat (T/F)r	  j�  X    Doubling doesn't cost extra usesr�	  ee�r�	  j�  N�r�	  j�  X   Animar�	  �r�	  j�  ]r�	  (K�K`K e�r�	  j�  N�r�	  j.  ]r�	  X   Quinleyr�	  a�r�	  j�  X   purpler�	  �r�	  j�  K �r�	  j�  X   Firer�	  �r�	  j�  X   Cursedr�	  �r�	  eu}r�	  (j�  MuhX   Thunderr�	  hX   Thunderr�	  hX(   Effective against armored units.
SPD -2.r�	  j�  Nj�  �j�  }r�	  (j�  K
j�  K
uj�  ]r�	  j�  ]r�	  (j�  N�r�	  j�  N�r�	  j�  K�r�	  j�  K�r�	  j�  K�r�	  j�  J�����r�	  j�  K�r�	  j�  N�r�	  j�  X   Animar�	  �r�	  j�  ]r�	  (K�K�K e�r�	  j�  N�r�	  j�  K
�r�	  j�  ]r�	  (]r�	  (X   LoseUsesOnMiss (T/F)r�	  j�  X   Lose uses even on missr�	  e]r�	  (X   OneLossPerCombat (T/F)r�	  j�  X    Doubling doesn't cost extra usesr�	  ee�r�	  j�  N�r�	  j  }r�	  (j  ]r�	  X   Armorr�	  aj  G@      j  K j  �u�r�	  h5K�r�	  j  X   SPD2r�	  �r�	  j  X   'Armor' in unit.tagsr�	  �r�	  j�  X   Thunderr�	  �r�	  eu}r�	  (j�  MvhX   Lunar�	  hX   Lunar�	  hX"   Ignore's enemy resistance.
SPD -1.r�	  j�  Nj�  �j�  }r�	  (j�  Kj�  Kuj�  ]r�	  j�  ]r�	  (j�  N�r�	  j�  N�r�	  j�  N�r�	  j�  K�r�	  j�  K �r�	  j�  X   Darkr�	  �r�	  j�  N�r�	  j  X   ZEROr�	  �r�	  j�  K�r�	  j�  ]r�	  (]r�	  (X   LoseUsesOnMiss (T/F)r�	  j�  X   Lose uses even on missr�	  e]r�	  (X   OneLossPerCombat (T/F)r�	  j�  X    Doubling doesn't cost extra usesr�	  ee�r�	  j�  K
�r�	  j�  K�r�	  j�  K�r�	  j�  N�r�	  h5K�r�	  j  X   SPD1r�	  �r�	  j�  X   Fluxr�	  �r�	  eu}r�	  (j�  MwhX   Disasterr�	  hX   <purple>Disaster</purple>r�	  hX3   Carlin Only.
Backdash: 1.
Cannot Counter or Double.r�	  j�  Nj�  �j�  }r�	  (j�  Kj�  Kuj�  ]r�	  j�  ]r�	  (j�  N�r�	  j�  N�r�	  j�  K�r�	  j�  K�r�	  j�  K�r�	  j�  K
�r�	  j�  K#�r�	  j�  N�r�	  j�  X   Darkr�	  �r�	  j�  ]r�	  (KHK KAe�r�	  j�  N�r�	  j�  K�r�	  j�  ]r�	  (]r�	  (X   LoseUsesOnMiss (T/F)r�	  j�  X   Lose uses even on missr�	  e]r�	  (X   OneLossPerCombat (T/F)r�	  j�  X    Doubling doesn't cost extra usesr�	  ee�r�	  j.  ]r�	  X   Carlinr�	  a�r�	  j�  X   purpler�	  �r�	  jp  N�r 
  j	  N�r
  j�  X   Fluxr
  �r
  X   backdash_on_end_combatr
  K�r
  eu}r
  (j�  MxhX   Physicr
  hX   Physicr
  hXG   Restores an ally's HP from up to (MAG * 2) tiles away.
Chapter Uses: 4.r	
  j�  h�j�  �j�  }r

  (jg  Kjh  Kuj�  ]r
  j�  ]r
  (jk  N�r
  j�  N�r
  j�  X   Staffr
  �r
  j�  N�r
  h5K�r
  hK�r
  j�  ]r
  (K`K�K�e�r
  X
   magic_healr
  K�r
  j�  K�r
  j}  X   MAG2r
  �r
  j�  N�r
  jg  K�r
  j�  ]r
  (]r
  (X   LoseUsesOnMiss (T/F)r
  j�  X   Lose uses even on missr 
  e]r!
  (X   OneLossPerCombat (T/F)r"
  j�  X    Doubling doesn't cost extra usesr#
  ee�r$
  j�  X   Healr%
  �r&
  j�  N�r'
  eu}r(
  (j�  MyhX   Inspirer)
  hX   Inspirer*
  hXB   Target ally gains +1 STR/MAG/SKL/SPD for 2 turns.
Chapter Uses: 2.r+
  j�  Nj�  �j�  }r,
  (jg  Kjh  Kuj�  ]r-
  j�  ]r.
  (jk  N�r/
  j�  X   Staffr0
  �r1
  j�  N�r2
  h5K�r3
  hK!�r4
  j�  K�r5
  j�  X   Inspiredr6
  �r7
  j}  X   MAGIC_RANGEr8
  �r9
  j�  N�r:
  j�  ]r;
  (K�K�K�e�r<
  jg  K�r=
  j�  ]r>
  (]r?
  (X   LoseUsesOnMiss (T/F)r@
  j�  X   Lose uses even on missrA
  e]rB
  (X   OneLossPerCombat (T/F)rC
  j�  X    Doubling doesn't cost extra usesrD
  ee�rE
  j�  X   HealrF
  �rG
  j�  N�rH
  eu}rI
  (j�  MzhX   BlessedRingrJ
  hX   <orange>Blessed Ring</orange>rK
  hX    Trainees Only.
+1 to all stats. rL
  j�  Nj�  �j�  }rM
  j�  ]rN
  j�  ]rO
  (j�  X   AnointedrP
  �rQ
  j�  X   orangerR
  �rS
  eu}rT
  (j�  M{hX   ShoverU
  hX   <green>Shove</>rV
  hX!   Free action. Shove target 1 tile.rW
  j�  j>  j�  �j�  }rX
  (j�  K j�  Kuj�  ]rY
  j�  ]rZ
  (X   target_unitr[
  N�r\
  j�  K�r]
  j�  K�r^
  ji	  X   target.team == 'player'r_
  �r`
  j�  N�ra
  j�  K�rb
  X   shove_on_end_combatrc
  K�rd
  eu}re
  (j�  M|hjU
  hjV
  hjW
  j�  j#  j�  �j�  }rf
  (j�  K j�  Kuj�  ]rg
  j�  ]rh
  (j[
  N�ri
  j�  K�rj
  j�  K�rk
  ji	  j_
  �rl
  j�  N�rm
  j�  K�rn
  jc
  K�ro
  eu}rp
  (j�  M}hjU
  hjV
  hjW
  j�  j	  j�  �j�  }rq
  (j�  K j�  Kuj�  ]rr
  j�  ]rs
  (j[
  N�rt
  j�  K�ru
  j�  K�rv
  ji	  j_
  �rw
  j�  N�rx
  j�  K�ry
  jc
  K�rz
  eu}r{
  (j�  M~hjU
  hjV
  hjW
  j�  h�j�  �j�  }r|
  (j�  K j�  Kuj�  ]r}
  j�  ]r~
  (j[
  N�r
  j�  K�r�
  j�  K�r�
  ji	  j_
  �r�
  j�  N�r�
  j�  K�r�
  jc
  K�r�
  eu}r�
  (j�  MhjU
  hjV
  hjW
  j�  h�j�  �j�  }r�
  (j�  K j�  Kuj�  ]r�
  j�  ]r�
  (j[
  N�r�
  j�  K�r�
  j�  K�r�
  ji	  j_
  �r�
  j�  N�r�
  j�  K�r�
  jc
  K�r�
  eu}r�
  (j�  M�hX   Healr�
  hX   <blue>Heal</>r�
  hX   Restores (User's MAG * 2) HP.r�
  j�  h�j�  �j�  }r�
  j�  ]r�
  j�  ]r�
  (jk  N�r�
  j�  N�r�
  j�  K�r�
  j�  K�r�
  j�  X   Staffr�
  �r�
  hK�r�
  h5K�r�
  j�  ]r�
  (K`K�K�e�r�
  j�  N�r�
  j�  X   Healr�
  �r�
  X   equation_healr�
  X   MAG2r�
  �r�
  eu}r�
  (j�  M�hX   Exchanger�
  hX   Exchanger�
  hX:   Free action. Trade with allies up to (MAG * 2) tiles away.r�
  j�  h�j�  �j�  }r�
  j�  ]r�
  j�  ]r�
  (jk  N�r�
  j�  N�r�
  j�  K�r�
  j}  X   MAG2r�
  �r�
  j�  ]r�
  (K�K�Ke�r�
  X   trader�
  N�r�
  X   menu_after_combatr�
  N�r�
  j�  N�r�
  j�  N�r�
  X   map_cast_sfxr�
  X   Warpr�
  �r�
  X   map_cast_animr�
  X   Warpr�
  �r�
  eu}r�
  (j�  M�hjU
  hjV
  hjW
  j�  h�j�  �j�  }r�
  (j�  K j�  Kuj�  ]r�
  j�  ]r�
  (j[
  N�r�
  j�  K�r�
  j�  K�r�
  ji	  j_
  �r�
  j�  N�r�
  j�  K�r�
  jc
  K�r�
  eu}r�
  (j�  M�hX   Treatr�
  hX   Treatr�
  hX*   Free Action. Restores (User's MAG + 2) HP.r�
  j�  h�j�  �j�  }r�
  (j�  K j�  Kuj�  ]r�
  j�  ]r�
  (j�  N�r�
  X   never_use_battle_animationr�
  N�r�
  j�  K�r�
  j�  K�r�
  j�
  X   TREATr�
  �r�
  j�  N�r�
  hK
�r�
  j�  K�r�
  eu}r�
  (j�  M�hjU
  hjV
  hjW
  j�  h�j�  �j�  }r�
  (j�  K j�  Kuj�  ]r�
  j�  ]r�
  (j[
  N�r�
  j�  K�r�
  j�  K�r�
  ji	  j_
  �r�
  j�  N�r�
  j�  K�r�
  jc
  K�r�
  eu}r�
  (j�  M�hX   Tripr�
  hX   Tripr�
  hX/   Free Action. Set target's move to 0 for 1 turn.r�
  j�  h�j�  �j�  }r�
  (j�  K j�  Kuj�  ]r�
  j�  ]r�
  (j�  N�r�
  j�  K�r�
  j�  K�r�
  j�  X   Trippedr�
  �r�
  j�  N�r�
  j�  K�r�
  eu}r�
  (j�  M�hjU
  hjV
  hjW
  j�  h�j�  �j�  }r�
  (j�  K j�  Kuj�  ]r�
  j�  ]r�
  (j[
  N�r�
  j�  K�r�
  j�  K�r�
  ji	  j_
  �r�
  j�  N�r�
  j�  K�r   jc
  K�r  euehD]r  (}r  (j�  M�hX   HeartofFirer  j�  hj�  }r  X   initiator_nidr  NX   subskillr  Nu}r  (j�  M�hX   HeartofGalesr	  j�  hij�  }r
  j  Nj  Nu}r  (j�  M�hX   HeartofLightningr  j�  h�j�  }r  j  Nj  Nu}r  (j�  M�hX   HeartofFrostr  j�  h�j�  }r  j  Nj  Nu}r  (j�  M�hX   HeartofDarknessr  j�  h�j�  }r  j  Nj  Nu}r  (j�  M�hX   HeartofRadiancer  j�  j	  j�  }r  j  Nj  Nu}r  (j�  M�hX   SPD1r  j�  j#  j�  }r  j  Nj  Nu}r  (j�  M�hX   SPD2r  j�  j>  j�  }r  j  Nj  Nu}r  (j�  Mhj  j�  jp  j�  }r  j  Nj  Nu}r  (j�  M
hj  j�  j�  j�  }r   j  Nj  Nu}r!  (j�  MPhX   CloseCounterr"  j�  j�  j�  }r#  j  Nj  Nu}r$  (j�  MThX   DistantCounterr%  j�  j  j�  }r&  j  Nj  Nu}r'  (j�  M\hX   Wardenr(  j�  jD  j�  }r)  j  Nj  Nu}r*  (j�  M�hX   NoEXPr+  j�  hj�  }r,  j  Nj  Nu}r-  (j�  M�hX   Lootr.  j�  hj�  }r/  j  Nj  Nu}r0  (j�  M�hX   Shover1  j�  hj�  }r2  j  Nj  Nu}r3  (j�  M�hX   Flailr4  j�  hj�  }r5  j  Nj  Nu}r6  (j�  M�hj+  j�  hij�  }r7  j  Nj  Nu}r8  (j�  M�hj.  j�  hij�  }r9  j  Nj  Nu}r:  (j�  M�hj1  j�  hij�  }r;  j  Nj  Nu}r<  (j�  M�hX   RiskyBusinessr=  j�  hij�  }r>  j  Nj  Nu}r?  (j�  M�hj+  j�  h�j�  }r@  j  Nj  Nu}rA  (j�  M�hj.  j�  h�j�  }rB  j  Nj  Nu}rC  (j�  M�hj1  j�  h�j�  }rD  X   ability_item_uidrE  M�sj  Nj  Nu}rF  (j�  M�hX   BunglerG  j�  h�j�  }rH  j  Nj  Nu}rI  (j�  M�hj+  j�  h�j�  }rJ  j  Nj  Nu}rK  (j�  M�hj.  j�  h�j�  }rL  j  Nj  Nu}rM  (j�  M�hj1  j�  h�j�  }rN  jE  M�sj  Nj  Nu}rO  (j�  M�hX   TriprP  j�  h�j�  }rQ  jE  M�sj  Nj  Nu}rR  (j�  M�hj+  j�  h�j�  }rS  j  Nj  Nu}rT  (j�  M�hj.  j�  h�j�  }rU  j  Nj  Nu}rV  (j�  M�hX	   ProtectorrW  j�  h�j�  }rX  j  Nj  Nu}rY  (j�  M�hj1  j�  h�j�  }rZ  jE  M�sj  Nj  Nu}r[  (j�  M�hX   ElwynnSkillr\  j�  h�j�  }r]  jE  M�sj  Nj  Nu}r^  (j�  M�hj+  j�  h�j�  }r_  j  Nj  Nu}r`  (j�  M�hj.  j�  h�j�  }ra  j  Nj  Nu}rb  (j�  M�hj1  j�  h�j�  }rc  jE  Msj  Nj  Nu}rd  (j�  M�hX   DivineConduitre  j�  h�j�  }rf  jE  M�sj  Nj  Nu}rg  (j�  M�hX   Exchangerh  j�  h�j�  }ri  (X   chargerj  KX   total_chargerk  KjE  M�uj  Nj  Nu}rl  (j�  M�hj+  j�  h�j�  }rm  j  Nj  Nu}rn  (j�  M�hj.  j�  h�j�  }ro  j  Nj  Nu}rp  (j�  M�hjW  j�  h�j�  }rq  j  Nj  Nu}rr  (j�  M�hj1  j�  h�j�  }rs  jE  M~sj  Nj  Nu}rt  (j�  M�hX   Enthrallru  j�  h�j�  }rv  j  Nj  Nu}rw  (j�  M�hj+  j�  j	  j�  }rx  j  Nj  Nu}ry  (j�  M�hj.  j�  j	  j�  }rz  j  Nj  Nu}r{  (j�  M�hjW  j�  j	  j�  }r|  j  Nj  Nu}r}  (j�  M�hj1  j�  j	  j�  }r~  jE  M}sj  Nj  Nu}r  (j�  M�hX   Potshotr�  j�  j	  j�  }r�  j  Nj  Nu}r�  (j�  M�hj+  j�  j#  j�  }r�  j  Nj  Nu}r�  (j�  M�hj.  j�  j#  j�  }r�  j  Nj  Nu}r�  (j�  M�hjW  j�  j#  j�  }r�  j  Nj  Nu}r�  (j�  M�hj1  j�  j#  j�  }r�  jE  M|sj  Nj  Nu}r�  (j�  M�hX
   Instructorr�  j�  j#  j�  }r�  j  Nj  Nu}r�  (j�  M�hj+  j�  j>  j�  }r�  j  Nj  Nu}r�  (j�  M�hj.  j�  j>  j�  }r�  j  Nj  Nu}r�  (j�  M�hjW  j�  j>  j�  }r�  j  Nj  Nu}r�  (j�  M�hj1  j�  j>  j�  }r�  jE  M{sj  Nj  Nu}r�  (j�  M�hj+  j�  jY  j�  }r�  j  Nj  Nu}r�  (j�  M�hj.  j�  jY  j�  }r�  j  Nj  Nu}r�  (j�  M�hj+  j�  jp  j�  }r�  j  Nj  Nu}r�  (j�  M hj.  j�  jp  j�  }r�  j  Nj  Nu}r�  (j�  MhjW  j�  jp  j�  }r�  j  Nj  Nu}r�  (j�  Mhj1  j�  jp  j�  }r�  j  Nj  Nu}r�  (j�  MhX   Nemophilistr�  j�  jp  j�  }r�  j  Nj  Nu}r�  (j�  Mhj+  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  Mhj.  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  MhjW  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  Mhj1  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  M	hX   Beachcomberr�  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  Mhj+  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  Mhj.  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  MhjW  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  Mhj1  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  MhX   Bolsterr�  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  MhX   Repairr�  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  Mhj+  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  Mhj.  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  MhjW  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  MhX   AspiringLeaderr�  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  MhX   Cantor�  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  Mhj+  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  Mhj.  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  M hX   Anticritr�  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  M!hX   DivineBlessingr�  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  MMhj+  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  MNhj.  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  MOhj�  j�  j�  j�  }r�  j  Nj  Nu}r�  (j�  MQhj+  j�  j  j�  }r�  j  Nj  Nu}r�  (j�  MRhj.  j�  j  j�  }r�  j  Nj  Nu}r�  (j�  MShj�  j�  j  j�  }r�  j  Nj  Nu}r�  (j�  MUhj+  j�  j,  j�  }r�  j  Nj  Nu}r�  (j�  MVhj.  j�  j,  j�  }r�  j  Nj  Nu}r�  (j�  MWhj�  j�  j,  j�  }r�  j  Nj  Nu}r�  (j�  MXhj+  j�  jD  j�  }r�  j  Nj  Nu}r�  (j�  MYhj.  j�  jD  j�  }r�  j  Nj  Nu}r�  (j�  MZhj�  j�  jD  j�  }r�  j  Nj  Nu}r�  (j�  M[hj�  j�  jD  j�  }r�  j  Nj  Nu}r�  (j�  M�hX   AngelFeatherr�  j�  h�j�  }r�  j  h�j  Nu}r�  (j�  MQhj+  j�  j^  j�  }r�  j  Nj  Nu}r�  (j�  MRhj.  j�  j^  j�  }r�  j  Nj  Nu}r�  (j�  MShj+  j�  ju  j�  }r�  j  Nj  Nu}r�  (j�  MThj.  j�  ju  j�  }r�  j  Nj  Nu}r�  (j�  MyhX   HolyVeilr�  j�  h�j�  }r�  j  Nj  Mzu}r�  (j�  MzhX   Veiledr�  j�  Nj�  }r�  j  Nj  Nu}r�  (j�  M{hX   SuppressiveFirer�  j�  j>  j�  }r   j  Nj  Nu}r  (j�  M|hj�  j�  h�j�  }r  j  h�j  NueX   terrain_status_registryr  }r  X   regionsr  ]r  hNX
   overworldsr  ]r  }r	  (X   tilemapr
  NX   enabled_nodesr  ]r  X   enabled_roadsr  ]r  hX   0r  X   overworld_entitiesr  ]r  }r  (hhX   dtyper  X   PARTYr  X   dnidr  hX   on_node_nidr  NhNhX   playerr  uaX   selected_party_nidr  NX   node_propertiesr  }r  X   enabled_menu_optionsr  }r  (j  }r  X   1r  }r  X   2r   }r!  X   3r"  }r#  X   4r$  }r%  X   5r&  }r'  X   6r(  }r)  X   7r*  }r+  X   8r,  }r-  X   9r.  }r/  X   10r0  }r1  X   11r2  }r3  X   12r4  }r5  X   13r6  }r7  X   14r8  }r9  X   15r:  }r;  X   16r<  }r=  X   17r>  }r?  X   18r@  }rA  X   19rB  }rC  X   20rD  }rE  X   21rF  }rG  X   22rH  }rI  X   23rJ  }rK  X   24rL  }rM  X   25rN  }rO  uX   visible_menu_optionsrP  }rQ  (j  }rR  j  }rS  j   }rT  j"  }rU  j$  }rV  j&  }rW  j(  }rX  j*  }rY  j,  }rZ  j.  }r[  j0  }r\  j2  }r]  j4  }r^  j6  }r_  j8  }r`  j:  }ra  j<  }rb  j>  }rc  j@  }rd  jB  }re  jD  }rf  jF  }rg  jH  }rh  jJ  }ri  jL  }rj  jN  }rk  uuaX	   turncountrl  K X   playtimerm  J�J X	   game_varsrn  ccollections
Counter
ro  }rp  (X   _random_seedrq  M�X   _next_level_nidrr  X   3rs  X   _convoyrt  �X   AckermanDeadru  K X   _prep_musicrv  X    Before An Impossible Battlefieldrw  X   _prep_options_enabledrx  ]ry  X   _prep_options_eventsrz  ]r{  X   _prep_additional_optionsr|  ]r}  X   _current_turnwheel_usesr~  J����X   _should_go_to_overworldr  �X   LeodDeadr�  KX   BrassfieldDeadr�  K X   GotReclaimerr�  K X   EmbersonDeadr�  K u�r�  Rr�  X
   level_varsr�  jo  }r�  �r�  Rr�  X   current_moder�  }r�  (hX   Normalr�  X
   permadeathr�  �h.X   Randomr�  X   enemy_autolevelsr�  K X   enemy_truelevelsr�  K X   boss_autolevelsr�  K X   boss_truelevelsr�  K uX   partiesr�  ]r�  }r�  (hhhX   Eirika's Groupr�  X
   leader_nidr�  X   Alpinr�  X   party_prep_manage_sort_orderr�  ]r�  (j#  h�h�h�j	  h�h�j>  hhieX   moneyr�  K X   convoyr�  ]r�  (MfMgMjMkMnMqMhMiMlMmMrMsMoMpMtMuMzMbMyMdMeMvMweX   bexpr�  K uaX   current_partyr�  hX   stater�  ]r�  (X
   title_saver�  X   start_level_asset_loadingr�  e]r�  �r�  X
   action_logr�  ]r�  J����K �r�  X   eventsr�  ]r�  X   supportsr�  ]r�  X   recordsr�  }r�  (X   killsr�  ]r�  (X
   KillRecordr�  }r�  (X   turnr�  KX	   level_nidr�  X   0r�  X   killerr�  X   Temp_9r�  X   killeer�  X   Kid_5r�  u�r�  j�  }r�  (j�  Kj�  j�  j�  hj�  j�  u�r�  j�  }r�  (j�  Kj�  j�  j�  j�  j�  X   Tempr�  u�r�  j�  }r�  (j�  Kj�  j  j�  j>  j�  X
   Cutscene_1r�  u�r�  j�  }r�  (j�  Kj�  j  j�  j#  j�  X
   Cutscene_2r�  u�r�  j�  }r�  (j�  Kj�  j  j�  X
   Cutscene_6r�  j�  j#  u�r�  ej�  ]r�  (X   DamageRecordr�  }r�  (j�  Kj�  j�  X   dealerr�  j�  X   receiverr�  j�  X   item_nidr�  j!  X   over_damager�  Kj�  KX   kindr�  j�  u�r�  j�  }r�  (j�  Kj�  j�  j�  hj�  j�  j�  j�  j�  Kj�  Kj�  j�  u�r�  j�  }r�  (j�  Kj�  j�  j�  j�  j�  j�  j�  j�  j�  Kj�  Kj�  j�  u�r�  j�  }r�  (j�  Kj�  j  j�  j>  j�  j�  j�  j�  j�  Kj�  Kj�  j�  u�r�  j�  }r�  (j�  Kj�  j  j�  j#  j�  j�  j�  jl  j�  Kj�  Kj�  j�  u�r�  j�  }r�  (j�  Kj�  j  j�  j�  j�  j#  j�  j  j�  K
j�  Kj�  j�  u�r�  eX   healingr�  ]r�  X   deathr�  ]r�  X   item_user�  ]r�  (X
   ItemRecordr�  }r�  (j�  Kj�  j�  X   userr�  j�  j�  j�  u�r�  j�  }r�  (j�  Kj�  j  j�  j>  j�  j�  u�r�  j�  }r�  (j�  Kj�  j  j�  j#  j�  jl  u�r�  j�  }r�  (j�  Kj�  j  j�  h�j�  j6  u�r�  j�  }r�  (j�  Kj�  j  j�  j�  j�  j  u�r�  j�  }r�  (j�  Kj�  X   2r�  j�  h�j�  j6  u�r�  eje	  ]r�  X   combat_resultsr�  ]r�  (X   CombatRecordr�  }r�  (j�  Kj�  j�  X   attackerr�  j�  X   defenderr�  j�  X   resultr�  j�  u�r�  j�  }r�  (j�  Kj�  j�  j�  hj�  j�  j�  j�  u�r�  j�  }r�  (j�  Kj�  j�  j�  j�  j�  j�  j�  j�  u�r�  j�  }r�  (j�  Kj�  j  j�  j�  j�  j#  j�  X   missr�  u�r   j�  }r  (j�  Kj�  j  j�  j>  j�  j�  j�  j�  u�r  j�  }r  (j�  Kj�  j  j�  j#  j�  j�  j�  j�  u�r  j�  }r  (j�  Kj�  j  j�  h�j�  h�j�  j�  u�r  j�  }r  (j�  Kj�  j  j�  j�  j�  j#  j�  j�  u�r  j�  }r	  (j�  Kj�  j�  j�  h�j�  h�j�  j�  u�r
  eX   turns_takenr  ]r  (X   Recordr  }r  (j�  Kj�  j�  u�r  j  }r  (j�  Kj�  j  u�r  j  }r  (j�  Kj�  j�  u�r  eX   levelsr  ]r  h]r  j�  ]r  uX   speak_stylesr  }r  (X	   __defaultr  }r  (hj  X   speakerr  NhNX   widthr  NX   speedr  KX
   font_colorr  NX	   font_typer   X   convor!  X
   backgroundr"  X   message_bg_baser#  X	   num_linesr$  KX   draw_cursorr%  �X   message_tailr&  X   message_bg_tailr'  X   transparencyr(  G?�������X   name_tag_bgr)  X   name_tagr*  X   flagsr+  cbuiltins
set
r,  ]r-  �r.  Rr/  uX   __default_textr0  }r1  (hj0  j  NhNj  Nj  G?�      j  Nj   X   textr2  j"  X   menu_bg_baser3  j$  K j%  Nj&  Nj(  G?�������j)  j3  j+  j,  ]r4  �r5  Rr6  uX   __default_helpr7  }r8  (hj7  j  NhNj  Nj  G?�      j  Nj   j!  j"  hj$  Kj%  �j&  Nj(  G?�������j)  j*  j+  j,  ]r9  �r:  Rr;  uX   noirr<  }r=  (hj<  j  NhNj  Nj  Nj  X   whiter>  j   Nj"  X   menu_bg_darkr?  j$  Nj%  Nj&  hj(  Nj)  Nj+  j,  ]r@  �rA  RrB  uX   hintrC  }rD  (hjC  j  Nhcapp.utilities.enums
Alignments
rE  X   centerrF  �rG  RrH  j  K�j  Nj  Nj   Nj"  X   menu_bg_parchmentrI  j$  Kj%  Nj&  hj(  Nj)  Nj+  j,  ]rJ  �rK  RrL  uX	   cinematicrM  }rN  (hjM  j  NhjH  j  Nj  Nj  X   greyrO  j   X   chapterrP  j"  hj$  Kj%  �j&  hj(  Nj)  Nj+  j,  ]rQ  �rR  RrS  uX	   narrationrT  }rU  (hjT  j  NhKKn�rV  j  K�j  Nj  j>  j   Nj"  j3  j$  Nj%  Nj&  hj(  Nj)  Nj+  j,  ]rW  �rX  RrY  uX   narration_toprZ  }r[  (hjZ  j  NhKK�r\  j  K�j  Nj  j>  j   Nj"  j3  j$  Nj%  Nj&  hj(  Nj)  Nj+  j,  ]r]  �r^  Rr_  uX   clearr`  }ra  (hj`  j  NhNj  Nj  Nj  j>  j   Nj"  hj$  Nj%  �j&  hj(  Nj)  Nj+  j,  ]rb  �rc  Rrd  uX   thought_bubblere  }rf  (hje  j  NhNj  Nj  Nj  Nj   Nj"  Nj$  Nj%  Nj&  X   message_bg_thought_tailrg  j(  Nj)  Nj+  j,  ]rh  X   no_talkri  a�rj  Rrk  uX   boss_convo_leftrl  }rm  (hjl  j  NhKHKp�rn  j  K�j  Kj  Nj   j!  j"  j#  j$  Kj%  �j&  j'  j(  G        j)  Nj+  j,  ]ro  �rp  Rrq  uX   boss_convo_rightrr  }rs  (hjr  j  NhKKp�rt  j  K�j  Kj  Nj   j!  j"  j#  j$  Kj%  �j&  j'  j(  G        j)  Nj+  j,  ]ru  �rv  Rrw  uuX   market_itemsrx  }ry  X   unlocked_lorerz  ]r{  (X   WeaponTriangler|  X   Statsr}  X	   Equationsr~  X   WeaponRanksr  eX
   dialog_logr�  ]r�  X   already_triggered_eventsr�  ]r�  X   talk_optionsr�  ]r�  X   base_convosr�  }r�  X   current_random_stater�  J�9�nX   boundsr�  (K K KKtr�  X	   roam_infor�  capp.engine.roam.roam_info
RoamInfo
r�  )�r�  }r�  (X   roamr�  �X   roam_unit_nidr�  NX	   roam_unitr�  Nubu.