�}q (X   unitsq]q(}q(X   nidqX   AlpinqX
   prefab_nidqhX   positionqNX   teamqX   playerq	X   partyq
X   EirikaqX   klassqX   AlpinVillagerqX   variantqX    qX   factionqNX   levelqKX   expqK3X   genericq�X
   persistentq�X   aiqX   NoneqX   roam_aiqNX   ai_groupqhX   itemsq]q(M�M2	M9	M�eX   nameqX   AlpinqX   descqXH   An energetic young lad. Dreams of joining the army of [KINGDOM] someday.qX   tagsq]q X   Youthq!aX   statsq"}q#(X   HPq$KX   STRq%KX   MAGq&K X   SKLq'KX   SPDq(KX   LCKq)KX   DEFq*KX   RESq+KX   CONq,KX   MOVq-KuX   growthsq.}q/(h$K<h%K#h&K h'Kh(Kh)Kh*Kh+Kh,K h-KuX   growth_pointsq0}q1(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uX   stat_cap_modifiersq2}q3(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uX   starting_positionq4NX   wexpq5}q6(X   Swordq7G@*      X   Lanceq8K X   Axeq9K X   Bowq:K X   Staffq;K X   Lightq<K X   Animaq=K X   Darkq>K X   Defaultq?K uX   portrait_nidq@X   AlpinqAX   affinityqBX   FireqCX   skillsqD]qE(M�M�capp.engine.source_type
SourceType
qFX   itemqG���qH�qIRqJ�qKM�X   gameqLhFX   globalqM���qN�qORqP�qQM�hhFX   personalqR���qS�qTRqU�qVM�hhU�qWeX   notesqX]qYX
   current_hpqZKX   current_manaq[K X   current_fatigueq\K X   travelerq]NX   current_guard_gaugeq^K X   built_guardq_�X   deadq`�X   action_stateqa(��������tqbX   _fieldsqc}qdX   equipped_weaponqeM�X   equipped_accessoryqfNu}qg(hX   CarlinqhhhhhNhX   playerqih
hhX   CarlinVillagerqjhhhNhKhK'h�h�hhhNhhh]qk(MF	M�M�M�M 	ehX   CarlinqlhXA   A magically gifted youngster from [TOWN]. Energetic and sporatic.qmh]qnX   Youthqoah"}qp(h$K
h%K h&Kh'Kh(Kh)Kh*Kh+Kh,Kh-Kuh.}qq(h$K2h%K h&Kh'K#h(Kh)Kh*Kh+Kh,K h-Kuh0}qr(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}qs(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}qt(h7K h8K h9K h:K h;K h<K h=K h>G@      h?K uh@X   CarlinquhBX   WindqvhD]qw(M�M�hJ�qxM�hLhP�qyM�hhhU�qzM�hhhU�q{ehX]q|hZK
h[K h\K h]Nh^K h_�h`�ha(��������tq}hc}q~heMF	hfNu}q(hX   Shaylaq�hh�hNhX   playerq�h
hhX   ShaylaVillagerq�hhhNhKhK
h�h�hhhNhhh]q�(M�MD	M�M�ehX   Shaylaq�hXI   A clumsy young lass who hopes to adventure the world. Gutsy and fearless.q�h]q�X   Youthq�ah"}q�(h$Kh%Kh&K h'Kh(Kh)Kh*Kh+K h,Kh-Kuh.}q�(h$KAh%Kh&K h'Kh(Kh)Kh*K#h+Kh,K h-Kuh0}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}q�(h7K h8K h9G@      h:K h;K h<K h=K h>K h?K uh@X   Shaylaq�hBX   Thunderq�hD]q�(M�M�hJ�q�M�hLhP�q�M�h�hU�q�M�h�hU�q�ehX]q�hZKh[K h\K h]Nh^K h_�h`�ha(��������tq�hc}q�heM�hfNu}q�(hX   Raelinq�hh�hNhX   playerq�h
hhX   ZoyaVillagerq�hhhNhKhKh�h�hhhNhhh]q�(M�ME	M�M�M	ehX   Raelinq�hX8   The shut-in daughter of a fisherman. Timid and cautious.q�h]q�X   Youthq�ah"}q�(h$Kh%Kh&K h'Kh(Kh)Kh*Kh+Kh,Kh-Kuh.}q�(h$K7h%Kh&K h'Kh(K#h)Kh*Kh+Kh,K h-Kuh0}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}q�(h7K h8G@"      h9K h:K h;K h<K h=K h>K h?K uh@X   Raelin (LittleKyng)q�hBX   Iceq�hD]q�(M�M�hJ�q�M�hLhP�q�M�h�hU�q�M�h�hU�q�ehX]q�hZKh[K h\K h]Nh^K h_�h`�ha(��������tq�hc}q�heM�hfNu}q�(hX   Elwynnq�hh�hNhX   playerq�h
hhX   Monkq�hhhNhKhK`h�h�hhhNhhh]q�(M�M�M�M6	ehX   Elwynnq�hXM   Former vagabond turned holy monk of [CHURCH]. Stern and generally unpleasant.q�h]q�X   Adultq�ah"}q�(h$Kh%K h&Kh'Kh(Kh)Kh*Kh+Kh,Kh-Kuh.}q�(h$K7h%K h&Kh'Kh(Kh)K
h*Kh+K#h,K h-Kuh0}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}q�(h7K h8K h9K h:K h;K h<G@7      h=K h>K h?K uh@X   Elwynnq�hBX   Darkq�hD]q�(M�hLhP�q�M�h�hU�q�M�h�hU�q�M�h�hU�q�ehX]q�hZKh[K h\K h]Nh^K h_�h`�ha(��������tq�hc}q�heM�hfNu}q�(hX   Elspethq�hh�hNhX   otherq�h
hhX   Clericq�hX   Elspethq�hNhKhK h�h�hX   Noneq�hNhhh]q�M	ahX   Elspethq�hX2   Lifelong devotee of [CHURCH]. Humble and selfless.q�h]q�X   Adultq�ah"}q�(h$Kh%K h&Kh'K h(Kh)Kh*K h+Kh,Kh-Kuh.}q�(h$K2h%K h&Kh'Kh(Kh)K#h*Kh+K#h,K h-Kuh0}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}q�(h7K h8K h9K h:K h;Kh<K h=K h>K h?K uh@X   Elspethq�hBX   Lightq�hD]q�(M�hLhP�q�M�h�hU�q�M�h�hU�q�M�h�hU�q�ehX]q�hZKh[K h\K h]Nh^K h_�h`�ha(��������tq�hc}q�heNhfNu}q�(hX   Quinleyq�hh�hNhX   otherq�h
hhX   Mageq�hX   Quinleyq�hNhKhK h�h�hX   Noneq�hNhhh]q�(M	M	M	ehX   Quinleyq�hXN   A genius magician who's grown tired of elemental magic. Outgoing and friendly.q�h]q�X   Adultq�ah"}q�(h$Kh%K h&Kh'Kh(Kh)Kh*K h+Kh,Kh-Kuh.}q�(h$K2h%K h&K#h'Kh(Kh)Kh*Kh+Kh,K h-Kuh0}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}q�(h7K h8K h9K h:K h;K h<K h=Kh>K h?K uh@X   Quinleyq�hBX   Darkq�hD]q�(M�M	hJ�q�M�hLhP�q�M�h�hU�q�M�h�hU�q�M�h�hU�q�ehX]q�hZKh[K h\K h]Nh^K h_�h`�ha(��������tq�hc}q�heM	hfNu}q�(hX   Saraidq�hh�hNhX   otherq�h
hhX   Hunterq�hhhNhKhK h�h�hX   Noner   hNhhh]r  (M	M	M		ehX   Saraidr  hX0   A fledgling huntress. Unreserved and unfiltered.r  h]r  X   Adultr  ah"}r  (h$Kh%Kh&Kh'Kh(Kh)K h*K h+Kh,Kh-Kuh.}r  (h$K2h%Kh&Kh'Kh(K#h)Kh*Kh+Kh,K h-Kuh0}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r	  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r
  (h7K h8K h9K h:Kh;K h<K h=K h>K h?K uh@X   Sloaner  hBX   Lightr  hD]r  (M�M		hJ�r  M�hLhP�r  M�h�hU�r  M�h�hU�r  M�h�hU�r  ehX]r  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr  hc}r  heM	hfNu}r  (hX   Lamonter  hj  hNhX   otherr  h
hhX   Soldierr  hX   Lamonter  hNhKhK h�h�hX   Noner  hNhhh]r  (M
	M	M	ehX   Lamonter  hX,   One of Leod's guards. Kindhearted and loyal.r  h]r  X   Adultr   ah"}r!  (h$Kh%Kh&K h'Kh(Kh)Kh*Kh+K h,K
h-Kuh.}r"  (h$KAh%Kh&K h'Kh(Kh)K
h*K#h+Kh,K h-Kuh0}r#  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r$  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r%  (h7K h8Kh9K h:K h;K h<K h=K h>K h?K uh@X   Lamonter&  hBX   Icer'  hD]r(  (M�hLhP�r)  M�j  hU�r*  M�j  hU�r+  M�j  hU�r,  M�M
	hJ�r-  ehX]r.  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr/  hc}r0  heM
	hfNu}r1  (hX   Garveyr2  hj2  hNhX   otherr3  h
hhX   Archerr4  hX   Garveyr5  hNhKhK h�h�hX   Noner6  hNhhh]r7  (M	M	M	ehX   Garveyr8  hXA   One of Leod's guards. Dislikes his hometown and fellow villagers.r9  h]r:  X   Adultr;  ah"}r<  (h$Kh%Kh&K h'Kh(Kh)Kh*Kh+K h,K
h-Kuh.}r=  (h$K7h%K#h&Kh'Kh(Kh)K
h*Kh+Kh,K h-Kuh0}r>  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r?  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r@  (h7K h8K h9K h:Kh;K h<K h=K h>K h?K uh@X   GarveyrA  hBX   FirerB  hD]rC  (M�hLhP�rD  M�j2  hU�rE  M�j2  hU�rF  M�M	hJ�rG  ehX]rH  hZKh[K h\K h]Nh^K h_�h`�ha(��������trI  hc}rJ  heM	hfNu}rK  (hX   LeodrL  hjL  hNhX   otherrM  h
hhX   ManrN  hhhNhKhK h�h�hX   NonerO  hNhhh]rP  hX   LeodrQ  hX0   The mayor of [TOWN]. Pompous and short tempered.rR  h]rS  X	   Ch2IgnorerT  ah"}rU  (h$Kh%Kh&K h'Kh(Kh)Kh*Kh+K h,Kh-Kuh.}rV  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}rW  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}rX  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}rY  (h7K h8K h9K h:K h;K h<K h=K h>K h?K uh@X   LeodrZ  hBX   Darkr[  hD]r\  M�hLhP�r]  ahX]r^  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr_  hc}r`  heNhfNu}ra  (hX   Orlarb  hjb  hNhX   otherrc  h
hhX   Fighterrd  hX   Orlare  hNhKhK h�h�hX   Nonerf  hNhhh]rg  (M	M	M	ehX   Orlarh  hXK   Lumberjack, carpenter, and all around outdoorsperson. Outspoken and jovial.ri  h]rj  X   Adultrk  ah"}rl  (h$Kh%Kh&K h'Kh(Kh)Kh*Kh+K h,Kh-Kuh.}rm  (h$KAh%K#h&K h'Kh(Kh)K
h*Kh+Kh,K h-K
uh0}rn  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}ro  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}rp  (h7K h8K h9Kh:K h;K h<K h=K h>K h?K uh@X   Orlarq  hBX   Windrr  hD]rs  (M�hLhP�rt  M�jb  hU�ru  M�jb  hU�rv  M�jb  hU�rw  M�M	hJ�rx  ehX]ry  hZKh[K h\K h]Nh^K h_�h`�ha(��������trz  hc}r{  heM	hfNu}r|  (hX   Ailsar}  hj}  hNhX   otherr~  h
hhX   Lancerr  hX   Ailsar�  hNhKhK h�h�hX   Noner�  hNhhh]r�  (M	M	M	ehX   Ailsar�  hXE   A reclusive fisherman. Quick witted and sharped tongued. Zoya's aunt.r�  h]r�  X   Adultr�  ah"}r�  (h$Kh%Kh&K h'Kh(Kh)Kh*K h+Kh,K	h-Kuh.}r�  (h$K<h%Kh&K h'Kh(K#h)K
h*Kh+Kh,K h-K
uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7K h8Kh9K h:K h;K h<K h=K h>K h?K uh@X   Ailsar�  hBX   Thunderr�  hD]r�  (M�hLhP�r�  M�j}  hU�r�  M�j}  hU�r�  M�j}  hU�r�  M�M	hJ�r�  ehX]r�  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heM	hfNu}r�  (hX   Corridonr�  hj�  hNhX   otherr�  h
hhX
   Blacksmithr�  hhhNhKhK h�h�hX   Noner�  hNhhh]r�  M	ahX   Corridonr�  hX;   Town blacksmith and retired war veteran. Friendly and just.r�  h]r�  X   Adultr�  ah"}r�  (h$Kh%K h&K h'K h(Kh)Kh*Kh+Kh,Kh-Kuh.}r�  (h$KAh%K h&K h'K h(Kh)K#h*K#h+K#h,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7K h8K h9K h:K h;K h<K h=K h>K h?K uh@X   Corridonr�  hBX   Lightr�  hD]r�  (M�hLhP�r�  M�j�  hU�r�  M�j�  hU�r�  M�j�  hU�r�  M�j�  hU�r�  ehX]r�  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heNhfNu}r�  (hX   Laisrenr�  hj�  hNhX   otherr�  h
hhX   Cavalierr�  hX   Cathalr�  hNhKhK h�h�hX   Noner�  hNhhh]r�  (M	M	M	ehX   Laisrenr�  hXV   An aspiring recruit in [KINGDOM]'s army. Charming and outgoing. Alpin's older brother.r�  h]r�  X   Adultr�  ah"}r�  (h$Kh%Kh&K h'Kh(Kh)Kh*Kh+Kh,Kh-Kuh.}r�  (h$KAh%K#h&K#h'K#h(K#h)K#h*K#h+K#h,K h-Kuh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7G@6      h8Kh9K h:K h;K h<K h=K h>K h?K uh@X   Cathalr�  hBX   Darkr�  hD]r�  (M�hLhP�r�  M�j�  hU�r�  M�j�  hU�r�  M�j�  hFh���r�  �r�  Rr�  �r�  ehX]r�  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heM	hfNu}r�  (hX	   Cawthorner�  hj�  hNhX   enemyr�  h
hhX   Generalr�  hhhNhKhK h�h�hX   Noner�  hNhhh]r�  M	ahX	   Cawthorner�  hX   Cawthorne text.r�  h]r�  X   Bossr�  ah"}r�  (h$Kh%Kh&K h'Kh(Kh)K h*K	h+Kh,Kh-Kuh.}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7K h8Kh9K)h:K h;K h<K h=K h>K h?K uh@X	   Cawthorner�  hBX   Firer�  hD]r�  (M�hLhP�r�  M�j�  hU�r�  M�j�  hU�r�  ehX]r�  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heM	hfNu}r�  (hX   Ackermanr�  hj�  hNhX   enemyr�  h
hhX   Warriorr�  hhhNhKhK h�h�hX   AttackNoMover�  hNhhh]r�  (M	M	ehX   Ackermanr�  hX   Ackerman text.r�  h]r�  X   Bossr�  ah"}r�  (h$Kh%Kh&K h'Kh(Kh)K h*Kh+K h,Kh-Kuh.}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7K h8K h9Kh:G@6      h;K h<K h=K h>K h?K uh@X   Ackermanr�  hBX   Firer�  hD]r�  (MhLhP�r�  Mj�  hU�r�  MlM	hJ�r�  ehX]r�  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heM	hfNu}r�  (hX
   Brassfieldr�  hj�  hNhX   enemyr   h
hhX   Heror  hhhNhKhK h�h�hX   Noner  hNhhh]r  (M 	M!	M"	ehX
   Brassfieldr  hX   Brassfield text.r  h]r  X   Bossr  ah"}r  (h$Kh%Kh&Kh'Kh(Kh)K h*Kh+Kh,Kh-Kuh.}r	  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}r
  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r  (h7Kh8K h9Kh:K h;K h<K h=K h>K h?K uh@X
   Brassfieldr  hBX   Lightr  hD]r  (MhLhP�r  Mj�  hU�r  MM 	hJ�r  ehX]r  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr  hc}r  heM 	hfNu}r  (hX   Embersonr  hj  hNhX   enemyr  h
hhX   Sager  hhhNhKhK h�h�hX   Noner  hNhhh]r  (M#	M$	M%	ehX   Embersonr  hX   Emberson text.r  h]r  X   Bossr  ah"}r   (h$Kh%K h&Kh'Kh(Kh)K h*Kh+Kh,K	h-Kuh.}r!  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}r"  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r#  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r$  (h7K h8K h9K h:K h;Kh<Kh=Kh>K h?K uh@X   Embersonr%  hBX   Firer&  hD]r'  (MhLhP�r(  Mj  hU�r)  ehX]r*  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr+  hc}r,  heM#	hfNu}r-  (hX	   Davenportr.  hj.  hNhX   enemyr/  h
hhX   Great_Knightr0  hhhNhKhK h�h�hX   Noner1  hNhhh]r2  (M&	M'	M(	M)	ehX	   Davenportr3  hX   Davenport text.r4  h]r5  X   Bossr6  ah"}r7  (h$Kh%Kh&K h'Kh(Kh)K h*Kh+Kh,Kh-Kuh.}r8  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}r9  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r:  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r;  (h7Kh8Kh9Kh:K h;K h<K h=K h>K h?K uh@X	   Davenportr<  hBX   Icer=  hD]r>  (MhLhP�r?  Mj.  hU�r@  Mj0  j�  �rA  MM&	hJ�rB  ehX]rC  hZKh[K h\K h]Nh^K h_�h`�ha(��������trD  hc}rE  heM&	hfNueh]rF  (}rG  (X   uidrH  M�hX
   Iron SwordrI  hX
   Iron SwordrJ  hhX	   owner_nidrK  hX	   droppablerL  �X   datarM  }rN  X   subitemsrO  ]rP  X
   componentsrQ  ]rR  (X   weaponrS  N�rT  X   target_enemyrU  N�rV  X	   min_rangerW  K�rX  X	   max_rangerY  K�rZ  X   damager[  K�r\  X   hitr]  K
�r^  X	   level_expr_  N�r`  X   weapon_typera  X   Swordrb  �rc  X   critrd  K �re  eu}rf  (jH  M�hX   HeartOfFirerg  hX    <orange>Heart Of Flames</orange>rh  hX   +1 Damage. Fire affinity only.ri  jK  hjL  �jM  }rj  jO  ]rk  jQ  ]rl  (X   status_on_holdrm  X   HeartofFirern  �ro  X
   text_colorrp  X   orangerq  �rr  eu}rs  (jH  M�hX   Herbsrt  hX   Herbsru  hX(   Restores 4 HP. Unit can act after using.rv  jK  h�jL  �jM  }rw  (X   usesrx  KX   starting_usesry  KujO  ]rz  jQ  ]r{  (X   usabler|  N�r}  X   target_allyr~  N�r  jx  K�r�  X   uses_optionsr�  ]r�  ]r�  (X   LoseUsesOnMiss (T/F)r�  X   Fr�  X   Lose uses even on missr�  ea�r�  X   healr�  K�r�  X   map_hit_add_blendr�  ]r�  (K`K�K�e�r�  X   attack_after_combatr�  N�r�  eu}r�  (jH  M�hX   Jinxr�  hX   Jinxr�  hX,   Lowers target's Crit Avoid by 20 for 1 turn.r�  jK  hhjL  �jM  }r�  jO  ]r�  jQ  ]r�  (jS  N�r�  jU  N�r�  jW  K�r�  jY  K�r�  j[  K�r�  j]  K�r�  j_  N�r�  ja  X   Darkr�  �r�  j�  ]r�  (KHK KAe�r�  X   magicr�  N�r�  X   unrepairabler�  N�r�  X   status_on_hitr�  X	   Terrifiedr�  �r�  jd  K �r�  X   battle_cast_animr�  X   Fluxr�  �r�  eu}r�  (jH  M�hX   Devilryr�  hX   Devilryr�  hX   Can't miss. -2 SPD.
HP Cost: 4.r�  jK  hhjL  �jM  }r�  (jx  Kjy  KujO  ]r�  jQ  ]r�  (jS  N�r�  jU  N�r�  jW  K�r�  jY  K�r�  j[  K�r�  j_  N�r�  ja  X   Darkr�  �r�  j�  ]r�  (KHK KAe�r�  j�  N�r�  j�  N�r�  jx  K�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  jd  K �r�  X   status_on_equipr�  X   SPD2r�  �r�  h5K�r�  X   hp_costr�  K�r�  j�  X   Fluxr�  �r�  eu}r�  (jH  M�hX   HeartOfGalesr�  hX   <orange>Heart Of Gales</orange>r�  hX   +1 SPD. Wind affinity only.r�  jK  hhjL  �jM  }r�  jO  ]r�  jQ  ]r�  (jm  X   HeartofGalesr�  �r�  jp  X   oranger�  �r�  eu}r�  (jH  M�hX   Iron Axer�  hX   Iron Axer�  hhjK  h�jL  �jM  }r�  jO  ]r�  jQ  ]r�  (jS  N�r�  jU  N�r�  jW  K�r�  jY  K�r�  j[  K�r�  j]  K �r�  j_  N�r�  ja  X   Axer�  �r�  jd  K �r�  eu}r�  (jH  M�hX   HeartOfLightningr�  hX#   <orange>Heart Of Lightning</orange>r�  hX    +10 Crit. Thunder affinity only.r�  jK  h�jL  �jM  }r�  jO  ]r�  jQ  ]r�  (jm  X   HeartofLightningr�  �r�  jp  X   oranger�  �r�  eu}r�  (jH  M�hjt  hju  hjv  jK  h�jL  �jM  }r�  (jx  Kjy  KujO  ]r�  jQ  ]r�  (j|  N�r�  j~  N�r�  jx  K�r�  j�  j�  �r�  j�  K�r�  j�  j�  �r   j�  N�r  eu}r  (jH  M�hX
   Iron Lancer  hX
   Iron Lancer  hhjK  h�jL  �jM  }r  jO  ]r  jQ  ]r  (jS  N�r  jU  N�r	  jW  K�r
  jY  K�r  j[  K�r  j]  K�r  j_  N�r  ja  X   Lancer  �r  jd  K �r  eu}r  (jH  M�hX   Javelinr  hX   Javelinr  hX   Cannot Counter. SPD -2.r  jK  h�jL  �jM  }r  (jx  Kjy  KujO  ]r  jQ  ]r  (jS  N�r  jU  N�r  jW  K�r  jY  K�r  j[  K�r  j]  J�����r  jd  K �r  j_  N�r   ja  X   Lancer!  �r"  jx  K�r#  j�  ]r$  (]r%  (X   LoseUsesOnMiss (T/F)r&  j�  X   Lose uses even on missr'  e]r(  (X   OneLossPerCombat (T/F)r)  j�  X    Doubling doesn't cost extra usesr*  ee�r+  X   cannot_counterr,  N�r-  h5K�r.  j�  X   SPD2r/  �r0  eu}r1  (jH  M�hX   HeartOfFrostr2  hX   <orange>Heart Of Frost</orange>r3  hX   +1 DEF/RES. Ice affinity only.r4  jK  h�jL  �jM  }r5  jO  ]r6  jQ  ]r7  (jm  X   HeartofFrostr8  �r9  jp  X   oranger:  �r;  eu}r<  (jH  M�hX   Flashr=  hX   Flashr>  hX*   Lowers target's Accuracy by 20 for 1 turn.r?  jK  h�jL  �jM  }r@  jO  ]rA  jQ  ]rB  (jS  N�rC  jU  N�rD  jW  K�rE  jY  K�rF  j[  K�rG  j]  K
�rH  jd  K
�rI  j_  N�rJ  ja  X   LightrK  �rL  j�  ]rM  (K�K�K e�rN  j�  N�rO  j�  N�rP  j�  X   GlimmerrQ  �rR  j�  X   DazzledrS  �rT  eu}rU  (jH  M�hX   ShimmerrV  hX   ShimmerrW  hX   Cannot miss.rX  jK  h�jL  �jM  }rY  (jx  Kjy  KujO  ]rZ  jQ  ]r[  (jS  N�r\  jU  N�r]  jW  K�r^  jY  K�r_  j[  K�r`  jd  K
�ra  j_  N�rb  ja  X   Lightrc  �rd  j�  ]re  (K�K�K e�rf  j�  N�rg  j�  N�rh  h5K�ri  jx  K�rj  j�  ]rk  (]rl  (X   LoseUsesOnMiss (T/F)rm  j�  X   Lose uses even on missrn  e]ro  (X   OneLossPerCombat (T/F)rp  j�  X    Doubling doesn't cost extra usesrq  ee�rr  j�  X   Glimmerrs  �rt  eu}ru  (jH  M 	hjt  hju  hjv  jK  hhjL  �jM  }rv  (jx  Kjy  KujO  ]rw  jQ  ]rx  (j|  N�ry  j~  N�rz  jx  K�r{  j�  j�  �r|  j�  K�r}  j�  j�  �r~  j�  N�r  eu}r�  (jH  M	hX	   ChurchKeyr�  hX
   Church Keyr�  hX   Opens any door in the church.r�  jK  h�jL  �jM  }r�  (jx  Kjy  KujO  ]r�  jQ  ]r�  (jx  K�r�  j�  ]r�  ]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  ea�r�  X
   can_unlockr�  X   region.nid.startswith('Door')r�  �r�  eu}r�  (jH  M	hX   Entangler�  hX   Entangler�  hX'   Lowers target's Avoid by 20 for 1 turn.r�  jK  h�jL  �jM  }r�  jO  ]r�  jQ  ]r�  (jS  N�r�  jU  N�r�  jW  K�r�  jY  K�r�  j[  K�r�  j]  K�r�  j_  N�r�  ja  X   Animar�  �r�  j�  ]r�  (KUKUK e�r�  j�  N�r�  j�  N�r�  j�  X	   Entangledr�  �r�  jd  K �r�  j�  X   Firer�  �r�  eu}r�  (jH  M	hX   Firer�  hX   Firer�  hX*   Effective against horseback units. -1 SPD.r�  jK  h�jL  �jM  }r�  (jx  Kjy  KujO  ]r�  jQ  ]r�  (jS  N�r�  jU  N�r�  jW  K�r�  jY  K�r�  j[  K�r�  j]  K �r�  jd  K�r�  jx  K�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  j_  N�r�  ja  X   Animar�  �r�  j�  ]r�  (K�K`K e�r�  j�  N�r�  j�  N�r�  X   effective_damager�  }r�  (X   effective_tagsr�  ]r�  X   Horser�  aX   effective_multiplierr�  G@       X   effective_bonus_damager�  K X   show_effectiveness_flashr�  �u�r�  h5K�r�  j�  X   SPD1r�  �r�  X   eval_warningr�  X   'Horse' in unit.tagsr�  �r�  j�  X   Firer�  �r�  eu}r�  (jH  M	hX   HeartOfDarknessr�  hX"   <orange>Heart Of Darkness</orange>r�  hX   +15 Hit. Dark affinity only.r�  jK  h�jL  �jM  }r�  jO  ]r�  jQ  ]r�  (jm  X   HeartofDarknessr�  �r�  jp  X   oranger�  �r�  eu}r�  (jH  M	hX   Iron Bowr�  hX   Iron Bowr�  hhjK  h�jL  �jM  }r�  jO  ]r�  jQ  ]r�  (jS  N�r�  jU  N�r�  jW  K�r�  jY  K�r�  j[  K�r�  j]  K
�r�  j_  N�r�  ja  X   Bowr�  �r�  j�  }r�  (j�  ]r�  X   Flyingr�  aj�  G@       j�  K j�  �u�r�  jd  K �r�  j�  X   'Flying' in unit.tagsr�  �r�  eu}r�  (jH  M	hX   Longbowr�  hX   Longbowr�  hX   Cannot Counter. SPD -1.r�  jK  h�jL  �jM  }r�  (jx  K
jy  K
ujO  ]r�  jQ  ]r�  (jS  N�r   jU  N�r  j_  N�r  j[  K�r  j]  J�����r  ja  X   Bowr  �r  jx  K
�r  j�  ]r  (]r	  (X   LoseUsesOnMiss (T/F)r
  j�  X   Lose uses even on missr  e]r  (X   OneLossPerCombat (T/F)r  j�  X    Doubling doesn't cost extra usesr  ee�r  jd  K�r  jW  K�r  jY  K�r  j,  N�r  j�  }r  (j�  ]r  X   Flyingr  aj�  G@       j�  K j�  �u�r  j�  X   SPD1r  �r  j�  X   'Flying' in unit.tagsr  �r  eu}r  (jH  M		hX   HeartOfRadiancer  hX"   <orange>Heart Of Radiance</orange>r  hX   +15 Avoid. Holy affinity only.r  jK  h�jL  �jM  }r   jO  ]r!  jQ  ]r"  (jm  X   HeartofRadiancer#  �r$  jp  X   oranger%  �r&  eu}r'  (jH  M
	hX   Steel Lancer(  hX   Steel Lancer)  hX   SPD -1.r*  jK  j  jL  �jM  }r+  (jx  Kjy  KujO  ]r,  jQ  ]r-  (jS  N�r.  jU  N�r/  j_  N�r0  j[  K�r1  j]  K �r2  ja  X   Lancer3  �r4  jx  K�r5  j�  ]r6  (]r7  (X   LoseUsesOnMiss (T/F)r8  j�  X   Lose uses even on missr9  e]r:  (X   OneLossPerCombat (T/F)r;  j�  X    Doubling doesn't cost extra usesr<  ee�r=  jd  K �r>  jW  K�r?  jY  K�r@  j�  X   SPD1rA  �rB  eu}rC  (jH  M	hX	   AxereaverrD  hX	   AxereaverrE  hX%   Reverses the weapon triangle. SPD -1.rF  jK  j  jL  �jM  }rG  (jx  Kjy  KujO  ]rH  jQ  ]rI  (jS  N�rJ  jU  N�rK  j_  N�rL  j[  K�rM  j]  K �rN  ja  X   LancerO  �rP  jx  K�rQ  j�  ]rR  (]rS  (X   LoseUsesOnMiss (T/F)rT  j�  X   Lose uses even on missrU  e]rV  (X   OneLossPerCombat (T/F)rW  j�  X    Doubling doesn't cost extra usesrX  ee�rY  X   reaverrZ  N�r[  h5K�r\  jd  K
�r]  jW  K�r^  jY  K�r_  j�  XU   target.get_weapon() and item_system.weapon_type(target, target.get_weapon()) == 'Axe'r`  �ra  eu}rb  (jH  M	hjt  hju  hjv  jK  j  jL  �jM  }rc  (jx  Kjy  KujO  ]rd  jQ  ]re  (j|  N�rf  j~  N�rg  jx  K�rh  j�  j�  �ri  j�  K�rj  j�  j�  �rk  j�  N�rl  eu}rm  (jH  M	hX   Greatbowrn  hX	   Steel Bowro  hX   SPD -2.rp  jK  j2  jL  �jM  }rq  (jx  Kjy  KujO  ]rr  jQ  ]rs  (jS  N�rt  jU  N�ru  j_  N�rv  j[  K�rw  j]  K �rx  ja  X   Bowry  �rz  jx  K�r{  j�  ]r|  (]r}  (X   LoseUsesOnMiss (T/F)r~  j�  X   Lose uses even on missr  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  jd  K �r�  jW  K�r�  jY  K�r�  j�  X   SPD2r�  �r�  j�  }r�  (j�  ]r�  X   Flyingr�  aj�  G@       j�  K j�  �u�r�  j�  X   'Flying' in unit.tagsr�  �r�  eu}r�  (jH  M	hX   Mini_Bowr�  hX   Mini Bowr�  hX   Cannot Counter. SPD -1.r�  jK  j2  jL  �jM  }r�  (jx  K
jy  K
ujO  ]r�  jQ  ]r�  (jS  N�r�  jU  N�r�  j_  N�r�  j[  K�r�  j]  J�����r�  ja  X   Bowr�  �r�  jx  K
�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  jd  K�r�  h5K�r�  jW  K�r�  jY  K�r�  j,  N�r�  j�  }r�  (j�  ]r�  X   Flyingr�  aj�  G@       j�  K j�  �u�r�  j�  X   SPD1r�  �r�  j�  X   'Flying' in unit.tagsr�  �r�  eu}r�  (jH  M	hjt  hju  hjv  jK  j2  jL  �jM  }r�  (jx  Kjy  KujO  ]r�  jQ  ]r�  (j|  N�r�  j~  N�r�  jx  K�r�  j�  j�  �r�  j�  K�r�  j�  j�  �r�  j�  N�r�  eu}r�  (jH  M	hX	   Steel Axer�  hX	   Steel Axer�  hX   SPD -2.r�  jK  jb  jL  �jM  }r�  (jx  Kjy  KujO  ]r�  jQ  ]r�  (jS  N�r�  jU  N�r�  jW  K�r�  jY  K�r�  j[  K�r�  j]  J�����r�  jd  K �r�  jx  K�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  j_  N�r�  ja  X   Axer�  �r�  h5K�r�  j�  X   SPD2r�  �r�  eu}r�  (jH  M	hX
   EmeraldAxer�  hX   Emerald Axer�  hX$   Doubles the weapon triangle. SPD -1.r�  jK  jb  jL  �jM  }r�  (jx  Kjy  KujO  ]r�  jQ  ]r�  (jS  N�r�  jU  N�r�  jW  K�r�  jY  K�r�  j[  K�r�  j]  J�����r�  jd  K
�r�  jx  K�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  j_  N�r�  ja  X   Axer�  �r�  h5K�r�  X   double_triangler�  N�r�  j�  X   SPD1r�  �r�  X   warningr�  N�r�  eu}r�  (jH  M	hjt  hju  hjv  jK  jb  jL  �jM  }r�  (jx  Kjy  KujO  ]r�  jQ  ]r�  (j|  N�r   j~  N�r  jx  K�r  j�  j�  �r  j�  K�r  j�  j�  �r  j�  N�r  eu}r  (jH  M	hj(  hj)  hj*  jK  j}  jL  �jM  }r  (jx  Kjy  KujO  ]r	  jQ  ]r
  (jS  N�r  jU  N�r  j_  N�r  j[  K�r  j]  K �r  ja  j3  �r  jx  K�r  j�  j6  �r  jd  K �r  jW  K�r  jY  K�r  j�  jA  �r  eu}r  (jH  M	hX   SapphireLancer  hX   Sapphire Lancer  hX$   Doubles the weapon triangle. SPD -1.r  jK  j}  jL  �jM  }r  (jx  Kjy  KujO  ]r  jQ  ]r  (jS  N�r  jU  N�r  j_  N�r   j[  K�r!  j]  K �r"  ja  X   Lancer#  �r$  jx  K�r%  j�  ]r&  (]r'  (X   LoseUsesOnMiss (T/F)r(  j�  X   Lose uses even on missr)  e]r*  (X   OneLossPerCombat (T/F)r+  j�  X    Doubling doesn't cost extra usesr,  ee�r-  h5K�r.  jd  K
�r/  jW  K�r0  jY  K�r1  j�  N�r2  j�  X   SPD1r3  �r4  j�  N�r5  eu}r6  (jH  M	hjt  hju  hjv  jK  j}  jL  �jM  }r7  (jx  Kjy  KujO  ]r8  jQ  ]r9  (j|  N�r:  j~  N�r;  jx  K�r<  j�  j�  �r=  j�  K�r>  j�  j�  �r?  j�  N�r@  eu}rA  (jH  M	hX   ElixirrB  hX   ElixirrC  hX,   Fully recovers HP. Unit can act after using.rD  jK  j�  jL  �jM  }rE  (jx  Kjy  KujO  ]rF  jQ  ]rG  (j|  N�rH  j~  N�rI  jx  K�rJ  j�  ]rK  ]rL  (X   LoseUsesOnMiss (T/F)rM  j�  X   Lose uses even on missrN  ea�rO  j�  Kc�rP  j�  ]rQ  (K`K�K�e�rR  j�  N�rS  eu}rT  (jH  M	hX   Silver SwordrU  hX   Silver SwordrV  hhjK  j�  jL  �jM  }rW  (jx  K	jy  K
ujO  ]rX  jQ  ]rY  (jS  N�rZ  jU  N�r[  jW  K�r\  jY  K�r]  j[  K�r^  j]  K
�r_  jd  K �r`  j_  N�ra  ja  X   Swordrb  �rc  jx  K
�rd  j�  ]re  (]rf  (X   LoseUsesOnMiss (T/F)rg  j�  X   Lose uses even on missrh  e]ri  (X   OneLossPerCombat (T/F)rj  j�  X    Doubling doesn't cost extra usesrk  ee�rl  eu}rm  (jH  M	hX   Silver Lancern  hX   Silver Lancero  hhjK  j�  jL  �jM  }rp  (jx  K
jy  K
ujO  ]rq  jQ  ]rr  (jS  N�rs  jU  N�rt  jW  K�ru  jY  K�rv  j[  K�rw  j]  K�rx  jd  K �ry  j_  N�rz  ja  X   Lancer{  �r|  jx  K
�r}  j�  ]r~  (]r  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  eu}r�  (jH  M	hX   TheReclaimerr�  hX   <blue>The Reclaimer<blue>r�  hX6   Cavalier line only. 50% Lifelink. Effective Vs. Armor.r�  jK  j�  jL  �jM  }r�  jO  ]r�  jQ  ]r�  (jS  N�r�  jU  N�r�  jW  K�r�  jY  K�r�  j[  K�r�  j]  J�����r�  jd  K
�r�  j_  N�r�  ja  X   Lancer�  �r�  X	   prf_classr�  ]r�  (X   Cavalierr�  X   Paladinr�  X   Great_Knightr�  e�r�  j�  }r�  (j�  ]r�  X   Armorr�  aj�  G@       j�  K j�  �u�r�  jp  X   bluer�  �r�  X   lifelinkr�  G?�      �r�  eu}r�  (jH  M	hX
   TheDefilerr�  hX   <blue>The Defiler</blue>r�  hXF   Knight line only. Inflicts half damage on miss. Effective Vs. Clergy. r�  jK  j�  jL  �jM  }r�  jO  ]r�  jQ  ]r�  (jS  N�r�  jU  N�r�  jW  K�r�  jY  K�r�  j[  K�r�  j]  J�����r�  jd  K�r�  j_  N�r�  ja  X   Axer�  �r�  j�  ]r�  (X   Knightr�  X   Generalr�  X   Great_Knightr�  e�r�  j�  }r�  (j�  ]r�  X   Clergyr�  aj�  G@       j�  K j�  �u�r�  jp  X   bluer�  �r�  X   damage_on_missr�  G?�      �r�  eu}r�  (jH  M	hX   LunarArcr�  hX	   Lunar Arcr�  hX%   Ignores DEF and grants Close Counter.r�  jK  j�  jL  �jM  }r�  jO  ]r�  jQ  ]r�  (j�  X   CloseCounterr�  �r�  jS  N�r�  jU  N�r�  j_  N�r�  j[  K�r�  X   alternate_resist_formular�  X   ZEROr�  �r�  j]  K�r�  ja  X   Bowr�  �r�  jd  K
�r�  jW  K�r�  jY  K�r�  j�  }r�  (j�  ]r�  X   Flyingr�  aj�  G@       j�  K j�  �u�r�  j�  X   'Flying' in unit.tagsr�  �r�  eu}r�  (jH  M	hj�  hj�  hj�  jK  j�  jL  �jM  }r�  (jx  Kjy  KujO  ]r�  jQ  ]r�  (jS  N�r�  jU  N�r�  jW  K�r�  jY  K�r�  j[  K�r�  j]  J�����r�  jd  K
�r�  jx  K�r�  j�  j�  �r�  j_  N�r�  ja  j�  �r�  h5K�r�  j�  N�r�  j�  j�  �r�  j�  N�r�  eu}r�  (jH  M	hX   AngelFeatherr�  hX   <yellow>Angel Feather</yellow>r�  hX&   Gives unit +10% growths. Cannot stack.r�  jK  h�jL  �jM  }r�  (jx  Kjy  KujO  ]r�  jQ  ]r�  (j|  N�r�  j~  N�r�  jx  K�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r   (X   OneLossPerCombat (T/F)r  j�  X    Doubling doesn't cost extra usesr  ee�r  X   usable_in_baser  N�r  jp  X   yellowr  �r  j�  ]r  (K�K�K�e�r	  j�  X   AngelFeatherr
  �r  jW  K �r  jY  K �r  X   eval_availabler  X#   not has_skill(unit, 'AngelFeather')r  �r  X   event_on_hitr  X   Global AngelFeatherr  �r  eu}r  (jH  M 	hX   HowlingBlader  hX   <blue>Howling Blade</blue>r  hX4   Grants Distant Counter. Deals magic damage at range.r  jK  j�  jL  �jM  }r  jO  ]r  jQ  ]r  (jS  N�r  jU  N�r  jW  K�r  jY  K�r  j[  K�r  j]  K�r   jd  K
�r!  j_  N�r"  ja  X   Swordr#  �r$  jp  X   bluer%  �r&  X   magic_at_ranger'  N�r(  j�  X   DistantCounterr)  �r*  eu}r+  (jH  M!	hX   Macer,  hX   Macer-  hX   Cannot be Countered. SPD -2.r.  jK  j�  jL  �jM  }r/  (jx  K
jy  K
ujO  ]r0  jQ  ]r1  (jS  N�r2  jU  N�r3  j_  N�r4  j[  K�r5  j]  J�����r6  ja  X   Axer7  �r8  jx  K
�r9  j�  ]r:  (]r;  (X   LoseUsesOnMiss (T/F)r<  j�  X   Lose uses even on missr=  e]r>  (X   OneLossPerCombat (T/F)r?  j�  X    Doubling doesn't cost extra usesr@  ee�rA  jd  K�rB  h5K�rC  jW  K�rD  jY  K�rE  X   cannot_be_counteredrF  N�rG  j�  X   SPD2rH  �rI  eu}rJ  (jH  M"	hj�  hj�  hj�  jK  j�  jL  �jM  }rK  (jx  Kjy  KujO  ]rL  jQ  ]rM  (j|  N�rN  j~  N�rO  jx  K�rP  j�  j�  �rQ  j  N�rR  jp  j  �rS  j�  j  �rT  j�  j
  �rU  jW  K �rV  jY  K �rW  j  j  �rX  j  j  �rY  eu}rZ  (jH  M#	hX
   Incinerater[  hX
   Incinerater\  hX   Ignores RES. Cooldown: 1.r]  jK  j  jL  �jM  }r^  (X   cooldownr_  K X   starting_cooldownr`  KujO  ]ra  jQ  ]rb  (jS  N�rc  jU  N�rd  jW  K�re  jY  K�rf  j[  K�rg  j]  K �rh  j_  N�ri  ja  X   Animarj  �rk  j�  ]rl  (K�K K e�rm  j�  N�rn  j�  X   ZEROro  �rp  j�  N�rq  jd  K
�rr  j�  X   Firers  �rt  j_  K�ru  eu}rv  (jH  M$	hj�  hj�  hj�  jK  j  jL  �jM  }rw  (jx  Kjy  KujO  ]rx  jQ  ]ry  (jS  N�rz  jU  N�r{  jW  K�r|  jY  K�r}  j[  K�r~  j]  K �r  jd  K�r�  jx  K�r�  j�  j�  �r�  j_  N�r�  ja  j�  �r�  j�  j�  �r�  j�  N�r�  j�  N�r�  j�  }r�  (j�  j�  j�  G@       j�  K j�  �u�r�  h5K�r�  j�  j�  �r�  j�  j�  �r�  j�  j�  �r�  eu}r�  (jH  M%	hj�  hj�  hj�  jK  j  jL  �jM  }r�  (jx  Kjy  KujO  ]r�  jQ  ]r�  (j|  N�r�  j~  N�r�  jx  K�r�  j�  j�  �r�  j  N�r�  jp  j  �r�  j�  j  �r�  j�  j
  �r�  jW  K �r�  jY  K �r�  j  j  �r�  j  j  �r�  eu}r�  (jH  M&	hX   WardensCleaverr�  hX   <blue>Warden's Cleaver<blue>r�  hX&   While equipped unit cannot be doubled.r�  jK  j.  jL  �jM  }r�  jO  ]r�  jQ  ]r�  (jS  N�r�  jU  N�r�  jW  K�r�  jY  K�r�  j[  K�r�  j]  K�r�  jd  K
�r�  j_  N�r�  ja  X   Axer�  �r�  jp  X   bluer�  �r�  j�  X   Wardenr�  �r�  eu}r�  (jH  M'	hX
   BloodLancer�  hX   Blood Lancer�  hX   Has Lifelink. SPD -1.r�  jK  j.  jL  �jM  }r�  (jx  K
jy  K
ujO  ]r�  jQ  ]r�  (jS  N�r�  jU  N�r�  j_  N�r�  j[  K�r�  j]  J�����r�  ja  X   Lancer�  �r�  jx  K
�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  jd  K�r�  jW  K�r�  jY  K�r�  j�  G?�      �r�  h5K�r�  j�  X   SPD1r�  �r�  eu}r�  (jH  M(	hX   EtherealBlader�  hX   Ethereal Blader�  hX   Targets RES.r�  jK  j.  jL  �jM  }r�  (jx  K
jy  K
ujO  ]r�  jQ  ]r�  (jS  N�r�  jU  N�r�  jW  K�r�  jY  K�r�  j[  K�r�  j]  K�r�  jd  K�r�  jx  K
�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  j_  N�r�  ja  X   Swordr�  �r�  j�  X   MAGIC_DEFENSEr�  �r�  h5K�r�  eu}r�  (jH  M)	hj�  hj�  hj�  jK  j.  jL  �jM  }r�  (jx  Kjy  KujO  ]r�  jQ  ]r�  (j|  N�r�  j~  N�r�  jx  K�r�  j�  j�  �r�  j  N�r�  jp  j  �r�  j�  j  �r�  j�  j
  �r�  jW  K �r�  jY  K �r�  j  j  �r�  j  j  �r�  eu}r�  (jH  M2	hX   Steel Swordr�  hX   Steel Swordr   hX   SPD -1.r  jK  hjL  �jM  }r  (jx  Kjy  KujO  ]r  jQ  ]r  (jS  N�r  jU  N�r  jW  K�r  jY  K�r  j[  K�r	  j]  K�r
  jd  K �r  jx  K�r  j�  ]r  (]r  (X   LoseUsesOnMiss (T/F)r  j�  X   Lose uses even on missr  e]r  (X   OneLossPerCombat (T/F)r  j�  X    Doubling doesn't cost extra usesr  ee�r  j_  N�r  ja  X   Swordr  �r  j�  X   SPD1r  �r  eu}r  (jH  M6	hX   Lunar  hX   Lunar  hX"   Ignore's enemy resistance. SPD -1.r  jK  h�jL  �jM  }r  (jx  Kjy  KujO  ]r  jQ  ]r   (jS  N�r!  jU  N�r"  j_  N�r#  j[  K�r$  j]  K �r%  ja  X   Darkr&  �r'  j�  N�r(  j�  X   ZEROr)  �r*  jx  K�r+  j�  ]r,  (]r-  (X   LoseUsesOnMiss (T/F)r.  j�  X   Lose uses even on missr/  e]r0  (X   OneLossPerCombat (T/F)r1  j�  X    Doubling doesn't cost extra usesr2  ee�r3  jd  K
�r4  jW  K�r5  jY  K�r6  j�  N�r7  h5K�r8  j�  X   SPD1r9  �r:  j�  X   Fluxr;  �r<  eu}r=  (jH  M9	hj�  hj�  hj�  jK  hjL  �jM  }r>  (jx  K
jy  K
ujO  ]r?  jQ  ]r@  (jS  N�rA  jU  N�rB  jW  K�rC  jY  K�rD  j[  K�rE  j]  K�rF  jd  K�rG  jx  K
�rH  j�  j�  �rI  j_  N�rJ  ja  j�  �rK  j�  j�  �rL  h5K�rM  eu}rN  (jH  M=	hX   ShoverO  hX   ShoverP  hX!   Free action. Shove target 1 tile.rQ  jK  h�jL  �jM  }rR  (j_  K j`  KujO  ]rS  jQ  ]rT  (X   target_unitrU  N�rV  jW  K�rW  jY  K�rX  X   eval_target_restrict_2rY  X   target.team == 'player'rZ  �r[  j�  N�r\  j_  K�r]  X   shove_on_end_combatr^  K�r_  eu}r`  (jH  M>	hX   Treatra  hX   Treatrb  hX*   Free Action. Restores (User's MAG + 2) HP.rc  jK  h�jL  �jM  }rd  (j_  K j`  KujO  ]re  jQ  ]rf  (j~  N�rg  X   never_use_battle_animationrh  N�ri  jW  K�rj  jY  K�rk  X   equation_healrl  X   TREATrm  �rn  j�  N�ro  hK�rp  j_  K�rq  eu}rr  (jH  M?	hjO  hjP  hjQ  jK  h�jL  �jM  }rs  (j_  K j`  KujO  ]rt  jQ  ]ru  (jU  N�rv  jW  K�rw  jY  K�rx  jY  jZ  �ry  j�  N�rz  j_  K�r{  j^  K�r|  eu}r}  (jH  M@	hX   Tripr~  hX   Tripr  hX/   Free Action. Set target's move to 0 for 1 turn.r�  jK  h�jL  �jM  }r�  (j_  K j`  KujO  ]r�  jQ  ]r�  (jU  N�r�  jW  K�r�  jY  K�r�  j�  X   Trippedr�  �r�  j�  N�r�  j_  K�r�  eu}r�  (jH  MA	hjO  hjP  hjQ  jK  h�jL  �jM  }r�  (j_  K j`  KujO  ]r�  jQ  ]r�  (jU  N�r�  jW  K�r�  jY  K�r�  jY  jZ  �r�  j�  N�r�  j_  K�r�  j^  K�r�  eu}r�  (jH  MB	hjO  hjP  hjQ  jK  hhjL  �jM  }r�  (j_  K j`  KujO  ]r�  jQ  ]r�  (jU  N�r�  jW  K�r�  jY  K�r�  jY  jZ  �r�  j�  N�r�  j_  K�r�  j^  K�r�  eu}r�  (jH  MC	hjO  hjP  hjQ  jK  hjL  �jM  }r�  (j_  K j`  KujO  ]r�  jQ  ]r�  (jU  N�r�  jW  K�r�  jY  K�r�  jY  jZ  �r�  j�  N�r�  j_  K�r�  j^  K�r�  eu}r�  (jH  MD	hX   AxeOfWoer�  hX   <purple>Axe Of Woe</purple>r�  hX   Shayla Only. +2 DEF.r�  jK  h�jL  �jM  }r�  (jx  Kjy  KujO  ]r�  jQ  ]r�  (jS  N�r�  jU  N�r�  jW  K�r�  jY  K�r�  j[  K�r�  j]  K�r�  jd  K�r�  j_  N�r�  ja  X   Axer�  �r�  X   prf_unitr�  ]r�  X   Shaylar�  a�r�  j�  X   DEFr�  �r�  jp  X   purpler�  �r�  jx  K�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  eu}r�  (jH  ME	hX   Canterlanzer�  hX   <purple>Canterlanze</purple>r�  hX>   Zoya Only. Draw Back: 2. Grants Canto and Pass while equipped.r�  jK  h�jL  �jM  }r�  (jx  Kjy  KujO  ]r�  jQ  ]r�  (jS  N�r�  jU  N�r�  jW  K�r�  jY  K�r�  j[  K�r�  j]  K
�r�  j_  N�r�  ja  X   Lancer�  �r�  j�  ]r�  X   Raelinr�  a�r�  jp  X   purpler�  �r�  jx  K�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  X    draw_back_on_end_combat_initiater�  K�r�  X   multi_status_on_equipr�  ]r�  (X
   CantoSharpr�  X   Passr�  e�r�  jd  K �r�  eu}r�  (jH  MF	hX   Disasterr�  hX   <purple>Disaster</purple>r�  hX3   Carlin Only. Backdash: 1. Cannot Counter or Double.r�  jK  hhjL  �jM  }r�  (jx  Kjy  KujO  ]r�  jQ  ]r�  (jS  N�r�  jU  N�r�  jW  K�r�  jY  K�r�  j[  K�r�  j]  K
�r   jd  K#�r  j_  N�r  ja  X   Darkr  �r  j�  ]r  (KHK KAe�r  j�  N�r  jx  K�r  j�  ]r	  (]r
  (X   LoseUsesOnMiss (T/F)r  j�  X   Lose uses even on missr  e]r  (X   OneLossPerCombat (T/F)r  j�  X    Doubling doesn't cost extra usesr  ee�r  j�  ]r  X   Carlinr  a�r  jp  X   purpler  �r  j,  N�r  X	   no_doubler  N�r  j�  X   Fluxr  �r  X   backdash_on_end_combatr  K�r  euehD]r  (}r  (jH  M�hX   HeartofFirer  jK  hjM  }r   X   initiator_nidr!  NX   subskillr"  Nu}r#  (jH  M�hX   HeartofGalesr$  jK  hhjM  }r%  j!  Nj"  Nu}r&  (jH  M�hX   HeartofLightningr'  jK  h�jM  }r(  j!  Nj"  Nu}r)  (jH  M�hX   HeartofFrostr*  jK  h�jM  }r+  j!  Nj"  Nu}r,  (jH  M�hX   HeartofDarknessr-  jK  h�jM  }r.  j!  Nj"  Nu}r/  (jH  M�hX   HeartofRadiancer0  jK  h�jM  }r1  j!  Nj"  Nu}r2  (jH  M�hX   SPD1r3  jK  j  jM  }r4  j!  Nj"  Nu}r5  (jH  M�hX   SPD2r6  jK  j2  jM  }r7  j!  Nj"  Nu}r8  (jH  M�hj6  jK  jb  jM  }r9  j!  Nj"  Nu}r:  (jH  M�hj3  jK  j}  jM  }r;  j!  Nj"  Nu}r<  (jH  MhX   DistantCounterr=  jK  j�  jM  }r>  j!  Nj"  Nu}r?  (jH  MhX   Wardenr@  jK  j.  jM  }rA  j!  Nj"  Nu}rB  (jH  M�hX   traderC  jK  hjM  }rD  j!  Nj"  Nu}rE  (jH  M�hX   ShoverF  jK  hjM  }rG  X   ability_item_uidrH  MC	sj!  Nj"  Nu}rI  (jH  M�hX   FlailrJ  jK  hjM  }rK  j!  Nj"  Nu}rL  (jH  M�hjC  jK  hhjM  }rM  j!  Nj"  Nu}rN  (jH  M�hjF  jK  hhjM  }rO  jH  MB	sj!  Nj"  Nu}rP  (jH  M�hX   RiskyBusinessrQ  jK  hhjM  }rR  j!  Nj"  Nu}rS  (jH  M�hjC  jK  h�jM  }rT  j!  Nj"  Nu}rU  (jH  M�hjF  jK  h�jM  }rV  jH  MA	sj!  Nj"  Nu}rW  (jH  M�hX   BunglerX  jK  h�jM  }rY  j!  Nj"  Nu}rZ  (jH  M�hjC  jK  h�jM  }r[  j!  Nj"  Nu}r\  (jH  M�hjF  jK  h�jM  }r]  jH  M?	sj!  Nj"  Nu}r^  (jH  M�hX   Tripr_  jK  h�jM  }r`  jH  M@	sj!  Nj"  Nu}ra  (jH  M�hjC  jK  h�jM  }rb  j!  Nj"  Nu}rc  (jH  M�hX	   Protectorrd  jK  h�jM  }re  j!  Nj"  Nu}rf  (jH  M�hjF  jK  h�jM  }rg  jH  M=	sj!  Nj"  Nu}rh  (jH  M�hX   ElwynnSkillri  jK  h�jM  }rj  jH  M>	sj!  Nj"  Nu}rk  (jH  M�hjC  jK  h�jM  }rl  j!  Nj"  Nu}rm  (jH  M�hjF  jK  h�jM  }rn  j!  Nj"  Nu}ro  (jH  M�hX   DivineConduitrp  jK  h�jM  }rq  j!  Nj"  Nu}rr  (jH  M�hX   Exchangers  jK  h�jM  }rt  j!  Nj"  Nu}ru  (jH  M�hjC  jK  h�jM  }rv  j!  Nj"  Nu}rw  (jH  M�hjd  jK  h�jM  }rx  j!  Nj"  Nu}ry  (jH  M�hjF  jK  h�jM  }rz  j!  Nj"  Nu}r{  (jH  M�hX   Enthrallr|  jK  h�jM  }r}  j!  Nj"  Nu}r~  (jH  M�hjC  jK  h�jM  }r  j!  Nj"  Nu}r�  (jH  M�hjd  jK  h�jM  }r�  j!  Nj"  Nu}r�  (jH  M�hjF  jK  h�jM  }r�  j!  Nj"  Nu}r�  (jH  M�hX   Potshotr�  jK  h�jM  }r�  j!  Nj"  Nu}r�  (jH  M�hjC  jK  j  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hjd  jK  j  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hjF  jK  j  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hX
   Instructorr�  jK  j  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hjC  jK  j2  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hjd  jK  j2  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hjF  jK  j2  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hjC  jK  jL  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hjC  jK  jb  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hjd  jK  jb  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hjF  jK  jb  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hX   Nemophilistr�  jK  jb  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hjC  jK  j}  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hjd  jK  j}  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hjF  jK  j}  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hX   Beachcomberr�  jK  j}  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hjC  jK  j�  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hjd  jK  j�  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hjF  jK  j�  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hX   Bolsterr�  jK  j�  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hX   Repairr�  jK  j�  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hjC  jK  j�  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hjd  jK  j�  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hX   AspiringLeaderr�  jK  j�  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hX   Cantor�  jK  j�  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hjC  jK  j�  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hX   Anticritr�  jK  j�  jM  }r�  j!  Nj"  Nu}r�  (jH  M�hX   DivineBlessingr�  jK  j�  jM  }r�  j!  Nj"  Nu}r�  (jH  MhjC  jK  j�  jM  }r�  j!  Nj"  Nu}r�  (jH  Mhj�  jK  j�  jM  }r�  j!  Nj"  Nu}r�  (jH  MhjC  jK  j�  jM  }r�  j!  Nj"  Nu}r�  (jH  Mhj�  jK  j�  jM  }r�  j!  Nj"  Nu}r�  (jH  MhjC  jK  j  jM  }r�  j!  Nj"  Nu}r�  (jH  Mhj�  jK  j  jM  }r�  j!  Nj"  Nu}r�  (jH  MhjC  jK  j.  jM  }r�  j!  Nj"  Nu}r�  (jH  Mhj�  jK  j.  jM  }r�  j!  Nj"  Nu}r�  (jH  Mhj�  jK  j.  jM  }r�  j!  Nj"  Nu}r�  (jH  MlhX   CloseCounterr�  jK  j�  jM  }r�  j!  Nj"  NueX   terrain_status_registryr�  }r�  X   regionsr�  ]r�  hNX
   overworldsr�  ]r�  }r�  (X   tilemapr�  NX   enabled_nodesr�  ]r�  X   enabled_roadsr�  ]r�  hX   0r�  X   overworld_entitiesr�  ]r�  }r�  (hhX   dtyper�  X   PARTYr�  X   dnidr�  hX   on_node_nidr�  NhNhX   playerr�  uaX   selected_party_nidr�  NX   node_propertiesr�  }r�  X   enabled_menu_optionsr�  }r�  (j�  }r�  X   1r�  }r�  X   2r�  }r�  X   3r�  }r�  X   4r�  }r�  X   5r 	  }r	  X   6r	  }r	  X   7r	  }r	  X   8r	  }r	  X   9r	  }r		  X   10r
	  }r	  X   11r	  }r	  X   12r	  }r	  X   13r	  }r	  X   14r	  }r	  X   15r	  }r	  X   16r	  }r	  X   17r	  }r	  X   18r	  }r	  X   19r	  }r	  X   20r	  }r	  X   21r 	  }r!	  X   22r"	  }r#	  X   23r$	  }r%	  X   24r&	  }r'	  X   25r(	  }r)	  uX   visible_menu_optionsr*	  }r+	  (j�  }r,	  j�  }r-	  j�  }r.	  j�  }r/	  j�  }r0	  j 	  }r1	  j	  }r2	  j	  }r3	  j	  }r4	  j	  }r5	  j
	  }r6	  j	  }r7	  j	  }r8	  j	  }r9	  j	  }r:	  j	  }r;	  j	  }r<	  j	  }r=	  j	  }r>	  j	  }r?	  j	  }r@	  j 	  }rA	  j"	  }rB	  j$	  }rC	  j&	  }rD	  j(	  }rE	  uuaX	   turncountrF	  K X   playtimerG	  J�G X	   game_varsrH	  ccollections
Counter
rI	  }rJ	  (X   _random_seedrK	  K�X   _next_level_nidrL	  j�  X   AckermanDeadrM	  KX   _prep_musicrN	  X    Before An Impossible BattlefieldrO	  X   _prep_options_enabledrP	  ]rQ	  X   _prep_options_eventsrR	  ]rS	  X   _prep_additional_optionsrT	  ]rU	  X   _current_turnwheel_usesrV	  J����X   _should_go_to_overworldrW	  �u�rX	  RrY	  X
   level_varsrZ	  jI	  }r[	  �r\	  Rr]	  X   current_moder^	  }r_	  (hX   Normalr`	  X
   permadeathra	  �h.X   Randomrb	  X   enemy_autolevelsrc	  K X   enemy_truelevelsrd	  K X   boss_autolevelsre	  K X   boss_truelevelsrf	  K uX   partiesrg	  ]rh	  }ri	  (hhhX   Eirika's Grouprj	  X
   leader_nidrk	  X   Alpinrl	  X   party_prep_manage_sort_orderrm	  ]rn	  X   moneyro	  K X   convoyrp	  ]rq	  X   bexprr	  K uaX   current_partyrs	  hX   statert	  ]ru	  (X
   title_saverv	  X   start_level_asset_loadingrw	  e]rx	  �ry	  X
   action_logrz	  ]r{	  J����K �r|	  X   eventsr}	  ]r~	  X   supportsr	  ]r�	  X   recordsr�	  }r�	  (X   killsr�	  ]r�	  (X
   KillRecordr�	  }r�	  (X   turnr�	  KX	   level_nidr�	  X   0r�	  X   killerr�	  X   Temp_9r�	  X   killeer�	  X   Kid_5r�	  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  hj�	  j�	  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  hhj�	  X   Enemy_2r�	  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  h�j�	  X   Enemy_1r�	  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  h�j�	  X   Enemy_3r�	  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  h�j�	  X   Enemy_4r�	  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  h�j�	  X   Enemy_10r�	  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  hj�	  X   Enemy_7r�	  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  hj�	  X   Enemy_8r�	  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  hj�	  X   Enemy_5r�	  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  hhj�	  X   Rein_2r�	  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  h�j�	  X   Enemy_9r�	  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  h�j�	  X   Enemy_6r�	  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  hj�	  X   Rein_4r�	  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  h�j�	  X   Enemy_11r�	  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  hhj�	  X   Rein_5r�	  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  h�j�	  X   Rein_1r�	  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  hj�	  X   Rein_6r�	  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  h�j�	  j�  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  h�j�	  X   Rein_3r�	  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  j�  j�	  X   Tempr�	  u�r�	  ej[  ]r�	  (X   DamageRecordr�	  }r�	  (j�	  Kj�	  j�	  X   dealerr�	  j�	  X   receiverr�	  j�	  X   item_nidr�	  j�  X   over_damager�	  Kj[  KX   kindr�	  jd  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  hj�	  j�	  j�	  jI  j�	  Kj[  Kj�	  jd  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  hhj�	  j�	  j�	  j�  j�	  Kj[  Kj�	  j]  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  j�	  j�	  h�j�	  j(  j�	  Kj[  Kj�	  j]  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  h�j�	  j�	  j�	  j�  j�	  Kj[  Kj�	  j]  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  h�j�	  j�	  j�	  j�  j�	  K
j[  Kj�	  jd  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  h�j�	  j�	  j�	  jV  j�	  Kj[  Kj�	  j]  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  h�j�	  j�	  j�	  jV  j�	  Kj[  Kj�	  jd  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  h�j�	  j�	  j�	  j  j�	  K
j[  K	j�	  jd  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  j�	  j�	  h�j�	  j�  j�	  Kj[  Kj�	  j]  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  h�j�	  j�	  j�	  jV  j�	  Kj[  Kj�	  j]  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  h�j�	  j�	  j�	  j  j�	  Kj[  Kj�	  j]  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  h�j�	  j�	  j�	  j  j�	  Kj[  Kj�	  j]  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  h�j�	  j�	  j�	  j  j�	  Kj[  Kj�	  j]  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  hj�	  j�	  j�	  jI  j�	  Kj[  Kj�	  j]  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  hj�	  j�	  j�	  jI  j�	  Kj[  Kj�	  j]  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  hj�	  j�	  j�	  jI  j�	  Kj[  Kj�	  j]  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  hj�	  j�	  j�	  jI  j�	  Kj[  Kj�	  j]  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  j�	  j�	  hj�	  j�  j�	  Kj[  Kj�	  j]  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  hj�	  j�	  j�	  jI  j�	  Kj[  Kj�	  j]  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  hj�	  j�	  j�	  jI  j�	  Kj[  Kj�	  j]  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  hj�	  j�	  j�	  j�  j�	  Kj[  Kj�	  j]  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  hj�	  j�	  j�	  j�  j�	  Kj[  Kj�	  j]  u�r�	  j�	  }r�	  (j�	  Kj�	  j�	  j�	  hhj�	  j�	  j�	  j�  j�	  Kj[  Kj�	  j]  u�r�	  j�	  }r 
  (j�	  Kj�	  j�	  j�	  hhj�	  j�	  j�	  j�  j�	  Kj[  Kj�	  jd  u�r
  j�	  }r
  (j�	  Kj�	  j�	  j�	  h�j�	  j�	  j�	  j=  j�	  Kj[  Kj�	  j]  u�r
  j�	  }r
  (j�	  Kj�	  j�	  j�	  h�j�	  j�	  j�	  j  j�	  Kj[  Kj�	  j]  u�r
  j�	  }r
  (j�	  Kj�	  j�	  j�	  j�	  j�	  h�j�	  j�  j�	  Kj[  Kj�	  j]  u�r
  j�	  }r
  (j�	  Kj�	  j�	  j�	  j�	  j�	  h�j�	  j�  j�	  Kj[  Kj�	  j]  u�r	
  j�	  }r

  (j�	  Kj�	  j�	  j�	  j�	  j�	  h�j�	  j�  j�	  Kj[  Kj�	  j]  u�r
  j�	  }r
  (j�	  Kj�	  j�	  j�	  h�j�	  j�	  j�	  j=  j�	  Kj[  Kj�	  j]  u�r
  j�	  }r
  (j�	  Kj�	  j�	  j�	  j�	  j�	  h�j�	  j�  j�	  Kj[  Kj�	  j]  u�r
  j�	  }r
  (j�	  Kj�	  j�	  j�	  hj�	  j�	  j�	  jI  j�	  Kj[  Kj�	  j]  u�r
  j�	  }r
  (j�	  Kj�	  j�	  j�	  h�j�	  j�	  j�	  j�  j�	  Kj[  Kj�	  jd  u�r
  j�	  }r
  (j�	  Kj�	  j�	  j�	  j�	  j�	  hj�	  j  j�	  Kj[  Kj�	  j]  u�r
  j�	  }r
  (j�	  Kj�	  j�	  j�	  j�	  j�	  h�j�	  j  j�	  Kj[  Kj�	  j]  u�r
  j�	  }r
  (j�	  Kj�	  j�	  j�	  hhj�	  j�	  j�	  j�  j�	  Kj[  Kj�	  jd  u�r
  j�	  }r
  (j�	  Kj�	  j�	  j�	  h�j�	  j�	  j�	  j=  j�	  Kj[  Kj�	  j]  u�r
  j�	  }r
  (j�	  Kj�	  j�	  j�	  h�j�	  j�	  j�	  j=  j�	  Kj[  Kj�	  j]  u�r
  j�	  }r
  (j�	  Kj�	  j�	  j�	  hj�	  j�	  j�	  jI  j�	  Kj[  Kj�	  j]  u�r
  j�	  }r 
  (j�	  Kj�	  j�	  j�	  hj�	  j�	  j�	  jI  j�	  Kj[  Kj�	  j]  u�r!
  j�	  }r"
  (j�	  Kj�	  j�	  j�	  j�	  j�	  hj�	  j  j�	  Kj[  Kj�	  j]  u�r#
  j�	  }r$
  (j�	  Kj�	  j�	  j�	  hhj�	  j�  j�	  j�  j�	  Kj[  Kj�	  j]  u�r%
  j�	  }r&
  (j�	  Kj�	  j�	  j�	  j�  j�	  h�j�	  j�  j�	  Kj[  Kj�	  j]  u�r'
  j�	  }r(
  (j�	  Kj�	  j�	  j�	  h�j�	  j�  j�	  j�  j�	  Kj[  Kj�	  j]  u�r)
  j�	  }r*
  (j�	  Kj�	  j�	  j�	  h�j�	  j�  j�	  j  j�	  Kj[  Kj�	  j]  u�r+
  j�	  }r,
  (j�	  Kj�	  j�	  j�	  j�  j�	  h�j�	  j�  j�	  Kj[  Kj�	  j]  u�r-
  j�	  }r.
  (j�	  Kj�	  j�	  j�	  h�j�	  j�  j�	  j  j�	  Kj[  Kj�	  j]  u�r/
  j�	  }r0
  (j�	  Kj�	  j�	  j�	  h�j�	  j�	  j�	  j=  j�	  Kj[  Kj�	  j]  u�r1
  j�	  }r2
  (j�	  Kj�	  j�	  j�	  h�j�	  j�	  j�	  j=  j�	  Kj[  Kj�	  j]  u�r3
  j�	  }r4
  (j�	  Kj�	  j�	  j�	  j�  j�	  j�	  j�	  jU  j�	  Kj[  Kj�	  jd  u�r5
  eX   healingr6
  ]r7
  (j�	  }r8
  (j�	  Kj�	  j�	  j�	  h�j�	  h�j�	  jt  j�	  Kj[  Kj�	  j]  u�r9
  j�	  }r:
  (j�	  Kj�	  j�	  j�	  j�	  j�	  j�	  j�	  j�  j�	  K j[  K j�	  j]  u�r;
  j�	  }r<
  (j�	  Kj�	  j�	  j�	  h�j�	  hj�	  ja  j�	  Kj[  Kj�	  j]  u�r=
  j�	  }r>
  (j�	  Kj�	  j�	  j�	  h�j�	  hj�	  ja  j�	  Kj[  Kj�	  j]  u�r?
  j�	  }r@
  (j�	  Kj�	  j�	  j�	  h�j�	  hj�	  ja  j�	  Kj[  Kj�	  j]  u�rA
  j�	  }rB
  (j�	  Kj�	  j�	  j�	  h�j�	  h�j�	  jt  j�	  Kj[  Kj�	  j]  u�rC
  j�	  }rD
  (j�	  Kj�	  j�	  j�	  h�j�	  h�j�	  ja  j�	  Kj[  Kj�	  j]  u�rE
  eX   deathrF
  ]rG
  X   item_userH
  ]rI
  (X
   ItemRecordrJ
  }rK
  (j�	  Kj�	  j�	  X   userrL
  j�	  j�	  j(  u�rM
  jJ
  }rN
  (j�	  Kj�	  j�	  jL
  h�j�	  jV  u�rO
  jJ
  }rP
  (j�	  Kj�	  j�	  jL
  h�j�	  jV  u�rQ
  jJ
  }rR
  (j�	  Kj�	  j�	  jL
  h�j�	  jV  u�rS
  jJ
  }rT
  (j�	  Kj�	  j�	  jL
  h�j�	  j  u�rU
  jJ
  }rV
  (j�	  Kj�	  j�	  jL
  h�j�	  jt  u�rW
  jJ
  }rX
  (j�	  Kj�	  j�	  jL
  j�	  j�	  j�  u�rY
  jJ
  }rZ
  (j�	  Kj�	  j�	  jL
  hj�	  j�  u�r[
  jJ
  }r\
  (j�	  Kj�	  j�	  jL
  hj�	  j�  u�r]
  jJ
  }r^
  (j�	  Kj�	  j�	  jL
  j�	  j�	  j�  u�r_
  jJ
  }r`
  (j�	  Kj�	  j�	  jL
  j�	  j�	  j�  u�ra
  jJ
  }rb
  (j�	  Kj�	  j�	  jL
  h�j�	  j�  u�rc
  jJ
  }rd
  (j�	  Kj�	  j�	  jL
  j�	  j�	  j  u�re
  jJ
  }rf
  (j�	  Kj�	  j�	  jL
  j�	  j�	  j  u�rg
  jJ
  }rh
  (j�	  Kj�	  j�	  jL
  h�j�	  jt  u�ri
  jJ
  }rj
  (j�	  Kj�	  j�	  jL
  j�	  j�	  j  u�rk
  jJ
  }rl
  (j�	  Kj�	  j�	  jL
  hhj�	  j�  u�rm
  jJ
  }rn
  (j�	  Kj�	  j�	  jL
  j�  j�	  jU  u�ro
  eX   stealrp
  ]rq
  X   combat_resultsrr
  ]rs
  (X   CombatRecordrt
  }ru
  (j�	  Kj�	  j�	  X   attackerrv
  j�	  X   defenderrw
  j�	  X   resultrx
  jd  u�ry
  jt
  }rz
  (j�	  Kj�	  j�	  jv
  hjw
  j�	  jx
  jd  u�r{
  jt
  }r|
  (j�	  Kj�	  j�	  jv
  hhjw
  j�	  jx
  j]  u�r}
  jt
  }r~
  (j�	  Kj�	  j�	  jv
  j�	  jw
  h�jx
  j]  u�r
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�	  jx
  jd  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�	  jx
  jd  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�	  jx
  jd  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  j�	  jw
  h�jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  h�jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  h�jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  j�	  jw
  h�jx
  X   missr�
  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  j�	  jw
  hjx
  j�
  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  hjw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  hjw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  hjw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  hjw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  j�	  jw
  hjx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  hjw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  hjw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  j�	  jw
  hjx
  j�
  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  j�	  jw
  hjx
  j�
  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  hjw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  hjw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  hhjw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  hhjw
  j�	  jx
  jd  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  hjx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  j�	  jw
  h�jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  j�	  jw
  h�jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  j�	  jw
  h�jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  j�	  jw
  h�jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  hjw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�	  jx
  jd  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  hjx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  j�	  jw
  hjx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  j�	  jw
  h�jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  j�	  jw
  hhjx
  j�
  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  hhjw
  j�	  jx
  jd  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  hjx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  h�jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  j�	  jw
  h�jx
  j�
  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  j�	  jw
  hjx
  j�
  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  hjw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  hjw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  j�	  jw
  hjx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  hhjw
  j�  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  h�jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  j�  jw
  h�jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  j�  jw
  h�jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  j�	  jw
  h�jx
  j�
  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  h�jw
  j�	  jx
  j]  u�r�
  jt
  }r�
  (j�	  Kj�	  j�	  jv
  j�  jw
  j�	  jx
  jd  u�r   eX   turns_takenr  ]r  (X   Recordr  }r  (j�	  Kj�	  j�	  u�r  j  }r  (j�	  Kj�	  j�	  u�r  j  }r  (j�	  Kj�	  j�	  u�r	  j  }r
  (j�	  Kj�	  j�	  u�r  j  }r  (j�	  Kj�	  j�	  u�r  j  }r  (j�	  Kj�	  j�	  u�r  j  }r  (j�	  Kj�	  j�	  u�r  j  }r  (j�	  Kj�	  j�	  u�r  eX   levelsr  ]r  (X   LevelRecordr  }r  (j�	  Kj�	  j�	  X   unit_nidr  h�X   numr  Khh�u�r  j  }r  (j�	  Kj�	  j�	  j  hj  Khhu�r  j  }r  (j�	  Kj�	  j�	  j  h�j  Khh�u�r  j  }r  (j�	  Kj�	  j�	  j  h�j  Khh�u�r   j  }r!  (j�	  Kj�	  j�	  j  hhj  Khhju�r"  j  }r#  (j�	  Kj�	  j�	  j  h�j  Khh�u�r$  eh]r%  (j  }r&  (j�	  Kj�	  j�	  j  hhj  Khhju�r'  j  }r(  (j�	  Kj�	  j�	  j  h�j  Khh�u�r)  j  }r*  (j�	  Kj�	  j�	  j  h�j  Khh�u�r+  j  }r,  (j�	  Kj�	  j�	  j  h�j  Khh�u�r-  j  }r.  (j�	  Kj�	  j�	  j  h�j  K
hh�u�r/  j  }r0  (j�	  Kj�	  j�	  j  h�j  Khh�u�r1  j  }r2  (j�	  Kj�	  j�	  j  h�j  K(hh�u�r3  j  }r4  (j�	  Kj�	  j�	  j  h�j  K
hh�u�r5  j  }r6  (j�	  Kj�	  j�	  j  hj  Khhu�r7  j  }r8  (j�	  Kj�	  j�	  j  hj  Khhu�r9  j  }r:  (j�	  Kj�	  j�	  j  hj  Khhu�r;  j  }r<  (j�	  Kj�	  j�	  j  hj  Khhu�r=  j  }r>  (j�	  Kj�	  j�	  j  hhj  Khhju�r?  j  }r@  (j�	  Kj�	  j�	  j  h�j  Khh�u�rA  j  }rB  (j�	  Kj�	  j�	  j  h�j  K(hh�u�rC  j  }rD  (j�	  Kj�	  j�	  j  h�j  Khh�u�rE  j  }rF  (j�	  Kj�	  j�	  j  h�j  Khh�u�rG  j  }rH  (j�	  Kj�	  j�	  j  h�j  Khh�u�rI  j  }rJ  (j�	  Kj�	  j�	  j  h�j  K
hh�u�rK  j  }rL  (j�	  Kj�	  j�	  j  hj  Khhu�rM  j  }rN  (j�	  Kj�	  j�	  j  h�j  Khh�u�rO  j  }rP  (j�	  Kj�	  j�	  j  h�j  Khh�u�rQ  j  }rR  (j�	  Kj�	  j�	  j  hj  Khhu�rS  j  }rT  (j�	  Kj�	  j�	  j  h�j  Khh�u�rU  j  }rV  (j�	  Kj�	  j�	  j  hhj  Khhju�rW  j  }rX  (j�	  Kj�	  j�	  j  hhj  K(hhju�rY  j  }rZ  (j�	  Kj�	  j�	  j  h�j  Khh�u�r[  j  }r\  (j�	  Kj�	  j�	  j  h�j  Khh�u�r]  j  }r^  (j�	  Kj�	  j�	  j  hj  Khhu�r_  j  }r`  (j�	  Kj�	  j�	  j  hj  Khhu�ra  j  }rb  (j�	  Kj�	  j�	  j  hhj  K	hhju�rc  j  }rd  (j�	  Kj�	  j�	  j  h�j  Khh�u�re  j  }rf  (j�	  Kj�	  j�	  j  h�j  K	hh�u�rg  j  }rh  (j�	  Kj�	  j�	  j  h�j  KDhh�u�ri  j  }rj  (j�	  Kj�	  j�	  j  h�j  Khh�u�rk  ejo	  ]rl  uX   speak_stylesrm  }rn  (X	   __defaultro  }rp  (hjo  X   speakerrq  NhNX   widthrr  NX   speedrs  KX
   font_colorrt  NX	   font_typeru  X   convorv  X
   backgroundrw  X   message_bg_baserx  X	   num_linesry  KX   draw_cursorrz  �X   message_tailr{  X   message_bg_tailr|  X   transparencyr}  G?�������X   name_tag_bgr~  X   name_tagr  X   flagsr�  cbuiltins
set
r�  ]r�  �r�  Rr�  uX   __default_textr�  }r�  (hj�  jq  NhNjr  Njs  G?�      jt  Nju  X   textr�  jw  X   menu_bg_baser�  jy  K jz  Nj{  Nj}  G?�������j~  j�  j�  j�  ]r�  �r�  Rr�  uX   __default_helpr�  }r�  (hj�  jq  NhNjr  Njs  G?�      jt  Nju  jv  jw  hjy  Kjz  �j{  Nj}  G?�������j~  j  j�  j�  ]r�  �r�  Rr�  uX   noirr�  }r�  (hj�  jq  NhNjr  Njs  Njt  X   whiter�  ju  Njw  X   menu_bg_darkr�  jy  Njz  Nj{  hj}  Nj~  Nj�  j�  ]r�  �r�  Rr�  uX   hintr�  }r�  (hj�  jq  Nhcapp.utilities.enums
Alignments
r�  X   centerr�  �r�  Rr�  jr  K�js  Njt  Nju  Njw  X   menu_bg_parchmentr�  jy  Kjz  Nj{  hj}  Nj~  Nj�  j�  ]r�  �r�  Rr�  uX	   cinematicr�  }r�  (hj�  jq  Nhj�  jr  Njs  Njt  X   greyr�  ju  X   chapterr�  jw  hjy  Kjz  �j{  hj}  Nj~  Nj�  j�  ]r�  �r�  Rr�  uX	   narrationr�  }r�  (hj�  jq  NhKKn�r�  jr  K�js  Njt  j�  ju  Njw  j�  jy  Njz  Nj{  hj}  Nj~  Nj�  j�  ]r�  �r�  Rr�  uX   narration_topr�  }r�  (hj�  jq  NhKK�r�  jr  K�js  Njt  j�  ju  Njw  j�  jy  Njz  Nj{  hj}  Nj~  Nj�  j�  ]r�  �r�  Rr�  uX   clearr�  }r�  (hj�  jq  NhNjr  Njs  Njt  j�  ju  Njw  hjy  Njz  �j{  hj}  Nj~  Nj�  j�  ]r�  �r�  Rr�  uX   thought_bubbler�  }r�  (hj�  jq  NhNjr  Njs  Njt  Nju  Njw  Njy  Njz  Nj{  X   message_bg_thought_tailr�  j}  Nj~  Nj�  j�  ]r�  X   no_talkr�  a�r�  Rr�  uX   boss_convo_leftr�  }r�  (hj�  jq  NhKHKp�r�  jr  K�js  Kjt  Nju  jv  jw  jx  jy  Kjz  �j{  j|  j}  G        j~  Nj�  j�  ]r�  �r�  Rr�  uX   boss_convo_rightr�  }r�  (hj�  jq  NhKKp�r�  jr  K�js  Kjt  Nju  jv  jw  jx  jy  Kjz  �j{  j|  j}  G        j~  Nj�  j�  ]r�  �r�  Rr�  uuX   market_itemsr�  }r�  X   unlocked_lorer�  ]r�  X
   dialog_logr�  ]r�  X   already_triggered_eventsr�  ]r�  (X   0 House1Shaylar�  X   0 House2Raelinr�  X   0 House3Carlinr�  X   0 FightAckermanr�  eX   talk_optionsr�  ]r�  X   base_convosr�  }r�  X   current_random_stater�  JvX   boundsr�  (K K KKtr�  X	   roam_infor�  capp.engine.roam.roam_info
RoamInfo
r�  )�r�  }r�  (X   roamr�  �X   roam_unit_nidr�  X   Eirikar�  X	   roam_unitr�  Nubu.