�}q (X   unitsq]q(}q(X   nidqX   AlpinqX
   prefab_nidqhX   positionqKK �qX   teamq	X   playerq
X   partyqX   EirikaqX   klassqX   AlpinVillagerqX   variantqX    qX   factionqNX   levelqKX   expqK	X   genericq�X
   persistentq�X   aiqX   NoneqX   roam_aiqNX   ai_groupqhX   itemsq]q(Ma	M�	Mb	Mc	M�	eX   nameqX   AlpinqX   descqXH   An energetic young lad. Dreams of joining the army of [KINGDOM] someday.qX   tagsq ]q!X   Youthq"aX   statsq#}q$(X   HPq%KX   STRq&KX   MAGq'K X   SKLq(KX   SPDq)KX   LCKq*KX   DEFq+KX   RESq,K X   CONq-KX   MOVq.KuX   growthsq/}q0(h%K<h&K#h'K h(Kh)Kh*Kh+Kh,Kh-K h.KuX   growth_pointsq1}q2(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uX   stat_cap_modifiersq3}q4(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uX   starting_positionq5NX   wexpq6}q7(X   Swordq8G@*      X   Lanceq9K X   Axeq:K X   Bowq;K X   Staffq<K X   Lightq=K X   Animaq>K X   Darkq?K X   Defaultq@K uX   portrait_nidqAX   AlpinqBX   affinityqCX   FireqDX   skillsqE]qF(M�Mb	capp.engine.source_type
SourceType
qGX   itemqH���qI�qJRqK�qLM�X   gameqMhGX   globalqN���qO�qPRqQ�qRM�hhGX   personalqS���qT�qURqV�qWM�hhV�qXM�NhGX   defaultqY���qZ�q[Rq\�q]eX   notesq^]q_X
   current_hpq`KX   current_manaqaK X   current_fatigueqbK X   travelerqcNX   current_guard_gaugeqdK X   built_guardqe�X   deadqf�X   action_stateqg(��������tqhX   _fieldsqi}qjX   equipped_weaponqkMa	X   equipped_accessoryqlNu}qm(hX   CarlinqnhhnhNh	X   playerqohhhX   CarlinVillagerqphhhNhKhKbh�h�hhhNhhh]qq(M�	Md	Me	Mf	M�	ehX   CarlinqrhXA   A magically gifted youngster from [TOWN]. Energetic and sporatic.qsh ]qtX   Youthquah#}qv(h%K
h&K h'Kh(Kh)Kh*Kh+Kh,Kh-Kh.Kuh/}qw(h%K2h&K h'Kh(K#h)Kh*Kh+Kh,Kh-K h.Kuh1}qx(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}qy(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}qz(h8K h9K h:K h;K h<K h=K h>K h?G@       h@K uhAX   Carlinq{hCX   Windq|hE]q}(M�Mf	hK�q~M�hMhQ�qM�hnhV�q�M�hnhV�q�eh^]q�h`K
haK hbK hcNhdK he�hf�hg(��������tq�hi}q�hkM�	hlNu}q�(hX   Shaylaq�hh�hKK�q�h	X   playerq�hhhX   ShaylaVillagerq�hhhNhKhK	h�h�hhhNhhh]q�(Mg	M�	Mh	Mi	M�	ehX   Shaylaq�hXI   A clumsy young lass who hopes to adventure the world. Gutsy and fearless.q�h ]q�X   Youthq�ah#}q�(h%Kh&Kh'K h(Kh)Kh*Kh+Kh,K h-Kh.Kuh/}q�(h%KAh&Kh'K h(Kh)Kh*Kh+K#h,Kh-K h.Kuh1}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}q�(h8K h9K h:G@      h;K h<K h=K h>K h?K h@K uhAX   Shaylaq�hCX   Thunderq�hE]q�(M�Mh	hK�q�M�hMhQ�q�M�h�hV�q�M�h�hV�q�eh^]q�h`KhaK hbK hcNhdK he�hf�hg(��������tq�hi}q�hkMg	hlNu}q�(hX   Raelinq�hh�hKK�q�h	X   playerq�hhhX   ZoyaVillagerq�hhhNhKhKh�h�hhhNhhh]q�(Mj	Mk	Ml	M�	ehX   Raelinq�hX8   The shut-in daughter of a fisherman. Timid and cautious.q�h ]q�X   Youthq�ah#}q�(h%Kh&Kh'K h(Kh)Kh*Kh+Kh,Kh-Kh.Kuh/}q�(h%K7h&Kh'K h(Kh)K#h*Kh+Kh,Kh-K h.Kuh1}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}q�(h8K h9G@$      h:K h;K h<K h=K h>K h?K h@K uhAX   Raelin (LittleKyng)q�hCX   Iceq�hE]q�(M�Ml	hK�q�M�hMhQ�q�M�h�hV�q�M�h�hV�q�eh^]q�h`KhaK hbK hcNhdK he�hf�hg(��������tq�hi}q�hkMj	hlNu}q�(hX   Elwynnq�hh�hKK �q�h	X   playerq�hhhX   Monkq�hhhNhKhKPh�h�hhhNhhh]q�(Mm	Mn	ehX   Elwynnq�hXM   Former vagabond turned holy monk of [CHURCH]. Stern and generally unpleasant.q�h ]q�X   Adultq�ah#}q�(h%Kh&K h'Kh(Kh)Kh*Kh+Kh,Kh-Kh.Kuh/}q�(h%K7h&K h'Kh(Kh)Kh*K
h+Kh,K#h-K h.Kuh1}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}q�(h8K h9K h:K h;K h<K h=G@:      h>K h?K h@K uhAX   Elwynnq�hCX   Darkq�hE]q�(M�hMhQ�q�M�h�hV�q�M�h�hV�q�M�h�hV�q�eh^]q�h`KhaK hbK hcNhdK he�hf�hg(��������tq�hi}q�hkMm	hlNu}q�(hX   Elspethq�hh�hNh	X   otherq�hhhX   Clericq�hX   Elspethq�hNhKhK h�h�hX   Noneq�hNhhh]q�Mp	ahX   Elspethq�hX2   Lifelong devotee of [CHURCH]. Humble and selfless.q�h ]q�X   Adultq�ah#}q�(h%Kh&K h'Kh(K h)Kh*Kh+K h,Kh-Kh.Kuh/}q�(h%K2h&K h'Kh(Kh)Kh*K#h+Kh,K#h-K h.Kuh1}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}q�(h8K h9K h:K h;K h<Kh=K h>K h?K h@K uhAX   Elspethq�hCX   Lightq�hE]q�(M�hMhQ�q�M�h�hV�q�M�h�hV�q�M�h�hV�q�eh^]q�h`KhaK hbK hcNhdK he�hf�hg(��������tq�hi}q�hkNhlNu}q�(hX   Quinleyq�hh�hKK�q�h	X   playerq�hhhX   Mageq�hX   Quinleyq�hNhKhK h�h�hhhNhhh]q�(Ms	Mt	Mu	ehX   Quinleyq�hXN   A genius magician who's grown tired of elemental magic. Outgoing and friendly.q�h ]q�X   Adultq�ah#}q�(h%Kh&K h'Kh(Kh)Kh*Kh+K h,Kh-Kh.Kuh/}q�(h%K2h&K h'K#h(Kh)Kh*Kh+Kh,Kh-K h.Kuh1}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}q�(h8K h9K h:K h;K h<K h=K h>Kh?K h@K uhAX   Quinleyq�hCX   Darkq�hE]q�(M�Mu	hK�q�M�hMhQ�q�M�h�hV�q�M�h�hV�r   M�h�hV�r  eh^]r  h`KhaK hbK hcNhdK he�hf�hg(��������tr  hi}r  hkMs	hlNu}r  (hX   Saraidr  hj  hKK�r  h	X   playerr  hhhX   Hunterr	  hhhNhKhK h�h�hhhNhhh]r
  (Mv	Mw	Mx	ehX   Saraidr  hX0   A fledgling huntress. Unreserved and unfiltered.r  h ]r  X   Adultr  ah#}r  (h%Kh&Kh'Kh(Kh)Kh*K h+K h,Kh-Kh.Kuh/}r  (h%K2h&Kh'Kh(Kh)K#h*Kh+Kh,Kh-K h.Kuh1}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r  (h8K h9K h:K h;Kh<K h=K h>K h?K h@K uhAX   Sloaner  hCX   Lightr  hE]r  (M�Mx	hK�r  M�hMhQ�r  M�j  hV�r  M�j  hV�r  M�j  hV�r  M�j  hGX   terrainr  ���r  �r  Rr  �r   eh^]r!  h`KhaK hbK hcNhdK he�hf�hg(��������tr"  hi}r#  hkMv	hlNu}r$  (hX   Lamonter%  hj%  hNh	X   otherr&  hhhX   Soldierr'  hX   Lamonter(  hNhKhK h�h�hX   Noner)  hNhhh]r*  (My	Mz	M{	ehX   Lamonter+  hX,   One of Leod's guards. Kindhearted and loyal.r,  h ]r-  X   Adultr.  ah#}r/  (h%Kh&Kh'K h(Kh)Kh*Kh+Kh,K h-K
h.Kuh/}r0  (h%KAh&Kh'K h(Kh)Kh*K
h+K#h,Kh-K h.Kuh1}r1  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r2  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r3  (h8K h9G@6      h:K h;K h<K h=K h>K h?K h@K uhAX   Lamonter4  hCX   Icer5  hE]r6  (M�hMhQ�r7  M�j%  hV�r8  M�j%  hV�r9  M�j%  hV�r:  M�My	hK�r;  eh^]r<  h`KhaK hbK hcNhdK he�hf�hg(��������tr=  hi}r>  hkMy	hlNu}r?  (hX   Garveyr@  hj@  hNh	X   otherrA  hhhX   ArcherrB  hX   GarveyrC  hNhKhK h�h�hX   NonerD  hNhhh]rE  (M|	M}	M~	ehX   GarveyrF  hXA   One of Leod's guards. Dislikes his hometown and fellow villagers.rG  h ]rH  X   AdultrI  ah#}rJ  (h%Kh&Kh'K h(Kh)Kh*Kh+Kh,K h-K
h.Kuh/}rK  (h%K7h&K#h'Kh(Kh)Kh*K
h+Kh,Kh-K h.Kuh1}rL  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}rM  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}rN  (h8K h9K h:K h;G@6      h<K h=K h>K h?K h@K uhAX   GarveyrO  hCX   FirerP  hE]rQ  (M�hMhQ�rR  M�j@  hV�rS  M�j@  hV�rT  M�M|	hK�rU  eh^]rV  h`KhaK hbK hcNhdK he�hf�hg(��������trW  hi}rX  hkM|	hlNu}rY  (hX   LeodrZ  hjZ  hKK�r[  h	X   otherr\  hhhX   Manr]  hhhNhKhK h�h�hX   Noner^  hNhhh]r_  hX   Leodr`  hX0   The mayor of [TOWN]. Pompous and short tempered.ra  h ]rb  X	   Ch2Ignorerc  ah#}rd  (h%Kh&Kh'K h(Kh)Kh*K h+K h,K h-Kh.Kuh/}re  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}rf  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}rg  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}rh  (h8K h9K h:K h;K h<K h=K h>K h?K h@K uhAX   Leodri  hCX   Darkrj  hE]rk  M�hMhQ�rl  ah^]rm  h`KhaK hbK hcNhdK he�hf�hg(��������trn  hi}ro  hkNhlNu}rp  (hX   Orlarq  hjq  hNh	X   otherrr  hhhX   Fighterrs  hX   Orlart  hNhKhK h�h�hX   Noneru  hNhhh]rv  (M	M�	M�	ehX   Orlarw  hXK   Lumberjack, carpenter, and all around outdoorsperson. Outspoken and jovial.rx  h ]ry  X   Adultrz  ah#}r{  (h%Kh&Kh'K h(Kh)Kh*Kh+Kh,K h-Kh.Kuh/}r|  (h%KAh&K#h'K h(Kh)Kh*K
h+Kh,Kh-K h.K
uh1}r}  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r~  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r  (h8K h9K h:Kh;K h<K h=K h>K h?K h@K uhAX   Orlar�  hCX   Windr�  hE]r�  (M�hMhQ�r�  M�jq  hV�r�  M�jq  hV�r�  M�jq  hV�r�  M�M	hK�r�  eh^]r�  h`KhaK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkM	hlNu}r�  (hX   Ailsar�  hj�  hNh	X   otherr�  hhhX   Lancerr�  hX   Ailsar�  hNhKhK h�h�hX   Noner�  hNhhh]r�  (M�	M�	M�	ehX   Ailsar�  hXE   A reclusive fisherman. Quick witted and sharped tongued. Zoya's aunt.r�  h ]r�  X   Adultr�  ah#}r�  (h%Kh&Kh'K h(Kh)Kh*Kh+K h,Kh-K	h.Kuh/}r�  (h%K<h&Kh'K h(Kh)K#h*K
h+Kh,Kh-K h.K
uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8K h9Kh:K h;K h<K h=K h>K h?K h@K uhAX   Ailsar�  hCX   Thunderr�  hE]r�  (M�hMhQ�r�  M�j�  hV�r�  M�j�  hV�r�  M�j�  hV�r�  M�M�	hK�r�  eh^]r�  h`KhaK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkM�	hlNu}r�  (hX   Corridonr�  hj�  hNh	X   otherr�  hhhX
   Blacksmithr�  hhhNhKhK h�h�hX   Noner�  hNhhh]r�  M�	ahX   Corridonr�  hX;   Town blacksmith and retired war veteran. Friendly and just.r�  h ]r�  X   Adultr�  ah#}r�  (h%Kh&K h'K h(K h)Kh*Kh+Kh,Kh-Kh.Kuh/}r�  (h%KAh&K h'K h(K h)Kh*K#h+K#h,K#h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8K h9K h:K h;K h<K h=K h>K h?K h@K uhAX   Corridonr�  hCX   Lightr�  hE]r�  (M�hMhQ�r�  M�j�  hV�r�  M�j�  hV�r�  M�j�  hV�r�  M�j�  hV�r�  eh^]r�  h`KhaK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkNhlNu}r�  (hX   Laisrenr�  hj�  hNh	X   otherr�  hhhX   Cavalierr�  hX   Cathalr�  hNhKhK h�h�hX   Noner�  hNhhh]r�  (M�	M�	M�	ehX   Laisrenr�  hXV   An aspiring recruit in [KINGDOM]'s army. Charming and outgoing. Alpin's older brother.r�  h ]r�  X   Adultr�  ah#}r�  (h%Kh&Kh'K h(Kh)Kh*Kh+Kh,Kh-Kh.Kuh/}r�  (h%KAh&K#h'K#h(K#h)K#h*K#h+K#h,K#h-K h.Kuh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8G@6      h9Kh:K h;K h<K h=K h>K h?K h@K uhAX   Cathalr�  hCX   Darkr�  hE]r�  (M�hMhQ�r�  M�j�  hV�r�  M�j�  hV�r�  M�j�  hGh���r�  �r�  Rr�  �r�  eh^]r�  h`KhaK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkM�	hlNu}r�  (hX	   Cawthorner�  hj�  hNh	X   enemyr�  hhhX   Generalr�  hhhNhKhK h�h�hX   Noner�  hNhhh]r�  M�	ahX	   Cawthorner�  hX   Cawthorne text.r�  h ]r�  X   Bossr�  ah#}r�  (h%Kh&Kh'K h(Kh)Kh*K h+K	h,Kh-Kh.Kuh/}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8K h9Kh:K)h;K h<K h=K h>K h?K h@K uhAX	   Cawthorner�  hCX   Firer�  hE]r�  (M�hMhQ�r�  M�j�  hV�r�  M�j�  hV�r�  eh^]r�  h`KhaK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkM�	hlNu}r�  (hX   Ackermanr�  hj�  hNh	X   enemyr�  hhhX   Warriorr�  hhhNhKhK h�h�hX   AttackNoMover�  hNhhh]r�  (M�	M�	ehX   Ackermanr�  hX   Ackerman text.r�  h ]r�  X   Bossr�  ah#}r�  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,K h-Kh.Kuh/}r   (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r  (h8K h9K h:Kh;G@7      h<K h=K h>K h?K h@K uhAX   Ackermanr  hCX   Firer  hE]r  (MhMhQ�r  Mj�  hV�r  McM�	hK�r	  eh^]r
  h`KhaK hbK hcNhdK he�hf�hg(��������tr  hi}r  hkM�	hlNu}r  (hX
   Brassfieldr  hj  hKK�r  h	X   enemyr  hhhX   Heror  hhhNhKhK h�h�hX   Noner  hNhhh]r  (M�	M�	M�	ehX
   Brassfieldr  hX   Brassfield text.r  h ]r  X   Bossr  ah#}r  (h%Kh&Kh'Kh(Kh)Kh*K h+Kh,Kh-Kh.Kuh/}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r  (h8Kh9K h:Kh;K h<K h=K h>K h?K h@K uhAX
   Brassfieldr  hCX   Lightr  hE]r  (MhMhQ�r   Mj  hV�r!  MM�	hK�r"  eh^]r#  h`KhaK hbK hcNhdK he�hf�hg(��������tr$  hi}r%  hkM�	hlNu}r&  (hX   Embersonr'  hj'  hNh	X   enemyr(  hhhX   Sager)  hhhNhKhK h�h�hX   Noner*  hNhhh]r+  (M�	M�	M�	ehX   Embersonr,  hX   Emberson text.r-  h ]r.  X   Bossr/  ah#}r0  (h%Kh&K h'Kh(Kh)Kh*K h+Kh,Kh-K	h.Kuh/}r1  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}r2  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r3  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r4  (h8K h9K h:K h;K h<Kh=Kh>Kh?K h@K uhAX   Embersonr5  hCX   Firer6  hE]r7  (MhMhQ�r8  Mj'  hV�r9  eh^]r:  h`KhaK hbK hcNhdK he�hf�hg(��������tr;  hi}r<  hkM�	hlNu}r=  (hX	   Davenportr>  hj>  hNh	X   enemyr?  hhhX   Great_Knightr@  hhhNhKhK h�h�hX   NonerA  hNhhh]rB  (M�	M�	M�	M�	ehX	   DavenportrC  hX   Davenport text.rD  h ]rE  X   BossrF  ah#}rG  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,Kh-Kh.Kuh/}rH  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}rI  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}rJ  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}rK  (h8Kh9Kh:Kh;K h<K h=K h>K h?K h@K uhAX	   DavenportrL  hCX   IcerM  hE]rN  (MhMhQ�rO  Mj>  hV�rP  Mj@  j�  �rQ  MM�	hK�rR  eh^]rS  h`KhaK hbK hcNhdK he�hf�hg(��������trT  hi}rU  hkM�	hlNu}rV  (hX   Barrel_1rW  hjW  hK
K�rX  h	X   enemy2rY  hhhX   Wall15rZ  hNhX   Wallr[  hKhK h�h�hX   Noner\  hNhhh]r]  hX   Wallr^  hhh ]r_  h#}r`  (h%Kh&K h'K h(K h)K(h*K h+K h,K h-K h.K uh/}ra  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}rb  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}rc  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5K
K�rd  h6}re  (h8K h9K h:K h;K h<K h=K h>K h?K h@K uhANhCNhE]rf  (MghMhQ�rg  MhjZ  j�  �rh  eh^]ri  h`KhaK hbK hcNhdK he�hf�hg(��������trj  hi}rk  hkNhlNu}rl  (hX   Barrel_2rm  hjm  hNh	X   enemy2rn  hhhX   Wall15ro  hNhX   Wallrp  hKhK h�h�hX   Nonerq  hNhhh]rr  hj^  hhh ]rs  h#}rt  (h%Kh&K h'K h(K h)K(h*K h+K h,K h-K h.K uh/}ru  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}rv  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}rw  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5K
K�rx  h6}ry  (h8K h9K h:K h;K h<K h=K h>K h?K h@K uhANhCNhE]rz  (MihMhQ�r{  Mjjo  j�  �r|  eh^]r}  h`KhaK hbK hcNhdK he�hf�hg(��������tr~  hi}r  hkNhlNu}r�  (hX   Barrel_3r�  hj�  hKK�r�  h	X   enemy2r�  hhhX   Wall15r�  hNhX   Wallr�  hKhK h�h�hX   Noner�  hNhhh]r�  hj^  hhh ]r�  h#}r�  (h%Kh&K h'K h(K h)K(h*K h+K h,K h-K h.K uh/}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5KK�r�  h6}r�  (h8K h9K h:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r�  (MkhMhQ�r�  Mlj�  j�  �r�  eh^]r�  h`KhaK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkNhlNu}r�  (hX   Barrel_4r�  hj�  hNh	X   enemy2r�  hhhX   Wall15r�  hNhX   Wallr�  hKhK h�h�hX   Noner�  hNhhh]r�  hj^  hhh ]r�  h#}r�  (h%Kh&K h'K h(K h)K(h*K h+K h,K h-K h.K uh/}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5KK�r�  h6}r�  (h8K h9K h:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r�  (MmhMhQ�r�  Mnj�  j�  �r�  eh^]r�  h`KhaK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkNhlNu}r�  (hX   Barrel_5r�  hj�  hKK�r�  h	X   enemy2r�  hhhX   Wall15r�  hNhX   Wallr�  hKhK h�h�hX   Noner�  hNhhh]r�  hj^  hhh ]r�  h#}r�  (h%Kh&K h'K h(K h)K(h*K h+K h,K h-K h.K uh/}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5KK�r�  h6}r�  (h8K h9K h:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r�  (MohMhQ�r�  Mpj�  j�  �r�  eh^]r�  h`KhaK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkNhlNu}r�  (hX
   Cutscene_1r�  hj�  hNh	X   enemyr�  hhhX   Soldierr�  hNhX   Soldierr�  hKhK h�h�hX   Noner�  hNhhh]r�  hX   Soldierr�  hX=   The army of Grado, the largest nation on the entire continentr�  h ]r�  h#}r�  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,K h-Kh.Kuh/}r�  (h%KFh&Kh'K h(Kh)Kh*K h+Kh,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8K h9Kh:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r�  MqhMhQ�r�  ah^]r�  h`K haK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkNhlNu}r�  (hX
   Cutscene_2r�  hj�  hNh	X   enemyr�  hhhX   Fighterr�  hNhX   Soldierr�  hKhK h�h�hX   Noner�  hNhhh]r�  hj�  hj�  h ]r�  h#}r�  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,K h-Kh.Kuh/}r�  (h%KZh&Kh'K h(Kh)Kh*K h+K h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8K h9K h:Kh;K h<K h=K h>K h?K h@K uhANhCNhE]r�  (MrhMhQ�r�  M�Nh\�r�  eh^]r�  h`K haK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkNhlNu}r�  (hX
   Cutscene_3r�  hj�  hNh	X   enemyr�  hhhX   Fighterr�  hNhX   Soldierr�  hKhK h�h�hX   Noner�  hNhhh]r�  hj�  hj�  h ]r�  h#}r�  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,K h-Kh.Kuh/}r�  (h%KZh&Kh'K h(Kh)Kh*K h+K h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8K h9K h:Kh;K h<K h=K h>K h?K h@K uhANhCNhE]r�  MshMhQ�r�  ah^]r�  h`KhaK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkNhlNu}r�  (hX
   Cutscene_4r�  hj�  hNh	X   enemyr�  hhhX   Knightr�  hNhX   Soldierr�  hKhK h�h�hX   Noner�  hNhhh]r�  hj�  hj�  h ]r�  h#}r�  (h%K	h&Kh'K h(Kh)Kh*K h+Kh,K h-Kh.Kuh/}r   (h%KPh&K
h'K h(K
h)K
h*K h+K#h,K h-K h.K uh1}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r  (h8K h9Kh:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r  MthMhQ�r  ah^]r  h`K	haK hbK hcNhdK he�hf�hg(��������tr  hi}r  hkNhlNu}r	  (hX
   Cutscene_5r
  hj
  hNh	X   enemyr  hhhX	   ManDead_1r  hNhX   Soldierr  hKhK h�h�hX   Noner  hNhhh]r  M�	ahj�  hj�  h ]r  h#}r  (h%K
h&K h'K h(K h)K h*K h+K h,K h-Kh.K uh/}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r  (h8K h9K h:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r  MuhMhQ�r  ah^]r  h`K
haK hbK hcNhdK he�hf�hg(��������tr  hi}r  hkNhlNu}r  (hX
   Cutscene_6r  hj  hNh	X   enemyr  hhhX   Fighterr  hNhX   Soldierr  hKhK h�h�hX   Noner   hNhhh]r!  M�	ahj�  hj�  h ]r"  h#}r#  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,K h-Kh.Kuh/}r$  (h%KZh&Kh'K h(Kh)Kh*K h+K h,K h-K h.K uh1}r%  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r&  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r'  (h8K h9K h:Kh;K h<K h=K h>K h?K h@K uhANhCNhE]r(  MvhMhQ�r)  ah^]r*  h`KhaK hbK hcNhdK he�hf�hg(��������tr+  hi}r,  hkM�	hlNu}r-  (hX   DeadVillager_1r.  hj.  hNh	X   otherr/  hhhX   ManDeadr0  hNhX   Villagerr1  hKhK h�h�hX   Noner2  hNhhh]r3  hX   Villagerr4  hX   vilgerr5  h ]r6  h#}r7  (h%K
h&K h'K h(K h)K h*K h+K h,K h-K h.K uh/}r8  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}r9  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r:  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r;  (h8K h9K h:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r<  MwhMhQ�r=  ah^]r>  h`K
haK hbK hcNhdK he�hf�hg(��������tr?  hi}r@  hkNhlNu}rA  (hX   DeadVillager_2rB  hjB  hNh	X   otherrC  hhhX	   ManDead_1rD  hNhX   VillagerrE  hKhK h�h�hX   NonerF  hNhhh]rG  hj4  hj5  h ]rH  h#}rI  (h%K
h&K h'K h(K h)K h*K h+K h,K h-Kh.K uh/}rJ  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}rK  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}rL  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}rM  (h8K h9K h:K h;K h<K h=K h>K h?K h@K uhANhCNhE]rN  MxhMhQ�rO  ah^]rP  h`K
haK hbK hcNhdK he�hf�hg(��������trQ  hi}rR  hkNhlNu}rS  (hX   DeadVillager_3rT  hjT  hNh	X   otherrU  hhhX   WomanDead_1rV  hNhX   VillagerrW  hKhK h�h�hX   NonerX  hNhhh]rY  hj4  hj5  h ]rZ  h#}r[  (h%K
h&K h'K h(K h)K h*K h+K h,K h-K h.K uh/}r\  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}r]  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r^  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r_  (h8K h9K h:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r`  MyhMhQ�ra  ah^]rb  h`K
haK hbK hcNhdK he�hf�hg(��������trc  hi}rd  hkNhlNu}re  (hX   Cratesrf  hjf  hNh	X   enemy2rg  hhhX   Wall15rh  hNhX   Wallri  hKhK h�h�hX   Nonerj  hNhhh]rk  hj^  hhh ]rl  h#}rm  (h%Kh&K h'K h(K h)K(h*K h+K h,K h-K h.K uh/}rn  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}ro  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}rp  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}rq  (h8K h9K h:K h;K h<K h=K h>K h?K h@K uhANhCNhE]rr  (MzhMhQ�rs  M{jh  j�  �rt  eh^]ru  h`KhaK hbK hcNhdK he�hf�hg(��������trv  hi}rw  hkNhlNu}rx  (hX   Enemy_1ry  hjy  hKK�rz  h	X   enemyr{  hhhX   Shamanr|  hNhX   Soldierr}  hKhK h�h�hX
   YoloIgnorer~  hNhhh]r  M�	ahj�  hj�  h ]r�  h#}r�  (h%K
h&K h'Kh(Kh)Kh*K h+Kh,K h-Kh.Kuh/}r�  (h%KAh&K h'Kh(K
h)K
h*K h+Kh,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8K h9K h:K h;K h<K h=K h>K h?Kh@K uhANhCNhE]r�  M|hMhQ�r�  ah^]r�  h`K
haK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkM�	hlNu}r�  (hX   Enemy_2r�  hj�  hKK�r�  h	X   enemyr�  hhhX   Cavalierr�  hNhX   Soldierr�  hKhK h�h�hX
   YoloIgnorer�  hNhhh]r�  (M�	M�	ehj�  hj�  h ]r�  h#}r�  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,Kh-K	h.Kuh/}r�  (h%K<h&Kh'K h(Kh)Kh*K h+Kh,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8Kh9Kh:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r�  (M}hMhQ�r�  M~j�  j�  �r�  MM�	hK�r�  eh^]r�  h`KhaK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkM�	hlNu}r�  (hX   Enemy_3r�  hj�  hKK�r�  h	X   enemyr�  hhhX   Fighterr�  hNhX   Soldierr�  hKhK h�h�hX
   YoloIgnorer�  hNhhh]r�  M�	ahj�  hj�  h ]r�  h#}r�  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,K h-Kh.Kuh/}r�  (h%KZh&Kh'K h(Kh)Kh*K h+K h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8K h9K h:Kh;K h<K h=K h>K h?K h@K uhANhCNhE]r�  M�hMhQ�r�  ah^]r�  h`KhaK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkM�	hlNu}r�  (hX   Enemy_4r�  hj�  hKK�r�  h	X   enemyr�  hhhX   Myrmidonr�  hNhX   Soldierr�  hKhK h�h�hX   AttackInRangeIgnorer�  hNhhh]r�  M�	ahj�  hj�  h ]r�  h#}r�  (h%Kh&Kh'K h(Kh)Kh*K h+K h,K h-Kh.Kuh/}r�  (h%K2h&K
h'K h(Kh)Kh*K h+K h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8Kh9K h:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r�  M�hMhQ�r�  ah^]r�  h`KhaK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkM�	hlNu}r�  (hX   Enemy_5r�  hj�  hKK�r�  h	X   enemyr�  hhhX   Knightr�  hNhX   Soldierr�  hKhK h�h�hX   AttackInRangeIgnorer�  hNhhh]r�  M�	ahj�  hj�  h ]r�  h#}r�  (h%K
h&Kh'K h(Kh)Kh*K h+Kh,K h-Kh.Kuh/}r�  (h%KPh&K
h'K h(K
h)K
h*K h+K#h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8K h9Kh:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r�  (M�hMhQ�r�  M�M�	hK�r�  eh^]r�  h`K
haK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkM�	hlNu}r�  (hX   Enemy_6r�  hj�  hKK�r�  h	X   enemyr�  hhhX   Fighterr�  hNhX   Soldierr�  hKhK h�h�hX   AttackNoMover�  hNhhh]r�  M�	ahj�  hj�  h ]r�  h#}r�  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,K h-Kh.Kuh/}r�  (h%KZh&Kh'K h(Kh)Kh*K h+K h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8K h9K h:Kh;K h<K h=K h>K h?K h@K uhANhCNhE]r�  (M�hMhQ�r�  M�M�	hK�r�  eh^]r�  h`KhaK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkM�	hlNu}r�  (hX   Enemy_7r�  hj�  hKK
�r�  h	X   enemyr�  hhhX   Fighterr�  hNhX   Soldierr�  hKhK h�h�hX   AttackInRangeIgnorer�  hNhhh]r�  M�	ahj�  hj�  h ]r�  h#}r�  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,K h-Kh.Kuh/}r�  (h%KZh&Kh'K h(Kh)Kh*K h+K h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8K h9K h:Kh;K h<K h=K h>K h?K h@K uhANhCNhE]r�  M�hMhQ�r�  ah^]r�  h`KhaK hbK hcNhdK he�hf�hg(��������tr�  hi}r   hkM�	hlNu}r  (hX   Enemy_8r  hj  hKK
�r  h	X   enemyr  hhhX   Knightr  hNhX   Soldierr  hKhK h�h�hX   AttackNoMover  hNhhh]r  (M�	M�	ehj�  hj�  h ]r	  h#}r
  (h%K
h&Kh'K h(Kh)Kh*K h+Kh,K h-Kh.Kuh/}r  (h%KPh&K
h'K h(K
h)K
h*K h+K#h,K h-K h.K uh1}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r  (h8K h9Kh:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r  (M�hMhQ�r  M�M�	hK�r  eh^]r  h`K
haK hbK hcNhdK he�hf�hg(��������tr  hi}r  hkM�	hlNu}r  (hX   Enemy_9r  hj  hKK�r  h	X   enemyr  hhhX   Monkr  hNhX   Soldierr  hKhK h�h�hX   AttackInRangeIgnorer  hNhhh]r  M�	ahj�  hj�  h ]r  h#}r  (h%Kh&K h'Kh(Kh)Kh*K h+K h,Kh-Kh.Kuh/}r  (h%K7h&K h'Kh(K
h)Kh*K h+K h,Kh-K h.K uh1}r   (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r!  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r"  (h8K h9K h:K h;K h<K h=Kh>K h?K h@K uhANhCNhE]r#  M�hMhQ�r$  ah^]r%  h`KhaK hbK hcNhdK he�hf�hg(��������tr&  hi}r'  hkM�	hlNu}r(  (hX   Enemy_10r)  hj)  hKK
�r*  h	X   enemyr+  hhhX   Archerr,  hNhX   Soldierr-  hKhK h�h�hX   AttackInRangeIgnorer.  hNhhh]r/  M�	ahj�  hj�  h ]r0  h#}r1  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,Kh-Kh.Kuh/}r2  (h%K7h&Kh'K h(Kh)Kh*K h+K h,K h-K h.K uh1}r3  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r4  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r5  (h8K h9K h:K h;Kh<K h=K h>K h?K h@K uhANhCNhE]r6  (M�hMhQ�r7  M�M�	hK�r8  eh^]r9  h`KhaK hbK hcNhdK he�hf�hg(��������tr:  hi}r;  hkM�	hlNu}r<  (hX   Enemy_11r=  hj=  hKK�r>  h	X   enemyr?  hhhX   Cavalierr@  hNhX   SoldierrA  hKhK h�h�hX   AttackInTwoTurnsIgnorerB  hNhhh]rC  M�	ahj�  hj�  h ]rD  h#}rE  (h%K	h&Kh'K h(Kh)Kh*K h+Kh,Kh-K	h.Kuh/}rF  (h%K<h&Kh'K h(Kh)Kh*K h+Kh,K h-K h.K uh1}rG  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}rH  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}rI  (h8Kh9Kh:K h;K h<K h=K h>K h?K h@K uhANhCNhE]rJ  (M�hMhQ�rK  M�j@  j�  �rL  eh^]rM  h`K	haK hbK hcNhdK he�hf�hg(��������trN  hi}rO  hkM�	hlNu}rP  (hX   Enemy_12rQ  hjQ  hKK�rR  h	X   enemyrS  hhhX   MyrmidonrT  hNhX   SoldierrU  hKhK h�h�hX   AttackInRangeIgnorerV  hNhhh]rW  M�	ahj�  hj�  h ]rX  h#}rY  (h%Kh&Kh'K h(Kh)Kh*K h+K h,K h-Kh.Kuh/}rZ  (h%K2h&K
h'K h(Kh)Kh*K h+K h,K h-K h.K uh1}r[  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r\  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r]  (h8Kh9K h:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r^  M�hMhQ�r_  ah^]r`  h`KhaK hbK hcNhdK he�hf�hg(��������tra  hi}rb  hkM�	hlNu}rc  (hX   Enemy_13rd  hjd  hNh	X   enemyre  hhhX   Soldierrf  hNhX   Soldierrg  hKhK h�h�hX   YoloTargetLeodrh  hNhhh]ri  M�	ahj�  hj�  h ]rj  h#}rk  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,K h-Kh.Kuh/}rl  (h%KFh&Kh'K h(Kh)Kh*K h+Kh,K h-K h.K uh1}rm  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}rn  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}ro  (h8K h9Kh:K h;K h<K h=K h>K h?K h@K uhANhCNhE]rp  (M�hMhQ�rq  M�jd  hV�rr  M�M�	hK�rs  eh^]rt  h`KhaK hbK hcNhdK he�hf�hg(��������tru  hi}rv  hkM�	hlNu}rw  (hX   Enemy_14rx  hjx  hNh	X   enemyry  hhhX   Myrmidonrz  hNhX   Soldierr{  hKhK h�h�hX   YoloTargetLeodr|  hNhhh]r}  M�	ahj�  hj�  h ]r~  h#}r  (h%Kh&Kh'K h(Kh)Kh*K h+K h,K h-Kh.Kuh/}r�  (h%K2h&K
h'K h(Kh)Kh*K h+K h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8Kh9K h:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r�  (M�hMhQ�r�  M�jx  hV�r�  eh^]r�  h`KhaK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkM�	hlNu}r�  (hX   Enemy_15r�  hj�  hNh	X   enemyr�  hhhX   Knightr�  hNhX   Soldierr�  hKhK h�h�hX   YoloTargetLeodr�  hNhhh]r�  M�	ahj�  hj�  h ]r�  h#}r�  (h%K
h&Kh'K h(Kh)Kh*K h+Kh,K h-Kh.Kuh/}r�  (h%KPh&K
h'K h(K
h)K
h*K h+K#h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8K h9Kh:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r�  (M�hMhQ�r�  M�j�  hV�r�  M�M�	hK�r�  eh^]r�  h`K
haK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkM�	hlNu}r�  (hX   Enemy_16r�  hj�  hNh	X   enemyr�  hhhX   Fighterr�  hNhX   Soldierr�  hKhK h�h�hX   YoloTargetLeodr�  hNhhh]r�  M�	ahj�  hj�  h ]r�  h#}r�  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,K h-Kh.Kuh/}r�  (h%KZh&Kh'K h(Kh)Kh*K h+K h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8K h9K h:Kh;K h<K h=K h>K h?K h@K uhANhCNhE]r�  (M�hMhQ�r�  M�j�  hV�r�  M�M�	hK�r�  eh^]r�  h`KhaK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkM�	hlNu}r�  (hX   Enemy_17r�  hj�  hNh	X   enemyr�  hhhX   Mager�  hNhX   Soldierr�  hKhK h�h�hX   YoloTargetLeodr�  hNhhh]r�  M�	ahj�  hj�  h ]r�  h#}r�  (h%K	h&K h'Kh(Kh)Kh*K h+K h,Kh-Kh.Kuh/}r�  (h%K<h&K h'Kh(Kh)Kh*K h+K h,Kh-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8K h9K h:K h;K h<K h=K h>Kh?K h@K uhANhCNhE]r�  (M�hMhQ�r�  M�j�  hV�r�  M�M�	hK�r�  eh^]r�  h`K	haK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkM�	hlNu}r�  (hX   Enemy_18r�  hj�  hNh	X   enemyr�  hhhX	   Mercenaryr�  hNhX   Soldierr�  hKhK h�h�hX   YoloTargetLeodr�  hNhhh]r�  M�	ahj�  hj�  h ]r�  h#}r�  (h%K
h&Kh'K h(Kh)Kh*K h+Kh,K h-K	h.Kuh/}r�  (h%KAh&Kh'K h(Kh)Kh*K h+K
h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8Kh9K h:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r�  (M�hMhQ�r�  M�j�  hV�r�  eh^]r�  h`K
haK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkM�	hlNu}r�  (hX   Enemy_19r�  hj�  hNh	X   enemyr�  hhhX   Dracoknightr�  hNhX   Soldierr�  hKhK h�h�hX   Yolor�  hNhhh]r�  M�	ahj�  hj�  h ]r�  h#}r�  (h%K	h&Kh'K h(Kh)Kh*K h+Kh,K h-K
h.Kuh/}r�  (h%KAh&Kh'K h(K
h)Kh*K h+Kh,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8K h9K h:Kh;K h<K h=K h>K h?K h@K uhANhCNhE]r�  (M�hMhQ�r�  M�j�  j�  �r�  M�M�	hK�r�  eh^]r�  h`K	haK hbK hcNhdK he�hf�hg(��������tr�  hi}r�  hkM�	hlNu}r�  (hX   Enemy_20r�  hj�  hNh	X   enemyr�  hhhX   PegasusKnightr�  hNhX   Soldierr�  hKhK h�h�hX   Yolor�  hNhhh]r�  M�	ahj�  hj�  h ]r�  h#}r�  (h%Kh&Kh'K h(Kh)Kh*K h+K h,Kh-Kh.Kuh/}r�  (h%K-h&Kh'K h(Kh)Kh*K h+K h,Kh-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8Kh9K h:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r�  (M�hMhQ�r�  M�j�  j�  �r�  M�M�	hK�r�  eh^]r�  h`KhaK hbK hcNhdK he�hf�hg(��������tr�  hi}r   hkM�	hlNueh]r  (}r  (X   uidr  Ma	hX
   Iron Swordr  hX
   Iron Swordr  hhX	   owner_nidr  hX	   droppabler  �X   datar  }r	  X   subitemsr
  ]r  X
   componentsr  ]r  (X   weaponr  N�r  X   target_enemyr  N�r  X	   min_ranger  K�r  X	   max_ranger  K�r  X   damager  K�r  X   hitr  K
�r  X	   level_expr  N�r  X   weapon_typer  X   Swordr  �r  X   critr  K �r   eu}r!  (j  Mb	hX   HeartOfFirer"  hX    <orange>Heart Of Flames</orange>r#  hX   +1 Damage. Fire affinity only.r$  j  hj  �j  }r%  j
  ]r&  j  ]r'  (X   status_on_holdr(  X   HeartofFirer)  �r*  X
   text_colorr+  X   oranger,  �r-  eu}r.  (j  Mc	hX   Herbsr/  hX   Herbsr0  hX(   Restores 4 HP. Unit can act after using.r1  j  hj  �j  }r2  (X   usesr3  KX   starting_usesr4  Kuj
  ]r5  j  ]r6  (X   usabler7  N�r8  X   target_allyr9  N�r:  X   usesr;  K�r<  X   uses_optionsr=  ]r>  ]r?  (X   LoseUsesOnMiss (T/F)r@  X   FrA  X   Lose uses even on missrB  ea�rC  X   healrD  K�rE  X   map_hit_add_blendrF  ]rG  (K`K�K�e�rH  X   attack_after_combatrI  N�rJ  eu}rK  (j  Md	hX   JinxrL  hX   JinxrM  hX,   Lowers target's Crit Avoid by 20 for 1 turn.rN  j  hnj  �j  }rO  j
  ]rP  j  ]rQ  (j  N�rR  j  N�rS  j  K�rT  j  K�rU  j  K�rV  j  K�rW  j  N�rX  j  X   DarkrY  �rZ  jF  ]r[  (KHK KAe�r\  X   magicr]  N�r^  X   unrepairabler_  N�r`  X   status_on_hitra  X	   Terrifiedrb  �rc  j  K �rd  X   battle_cast_animre  X   Fluxrf  �rg  eu}rh  (j  Me	hX   Devilryri  hX   Devilryrj  hX   Can't miss. -2 SPD.
HP Cost: 4.rk  j  hnj  �j  }rl  (j3  Kj4  Kuj
  ]rm  j  ]rn  (j  N�ro  j  N�rp  j  K�rq  j  K�rr  j  K�rs  j  N�rt  j  X   Darkru  �rv  jF  ]rw  (KHK KAe�rx  j]  N�ry  j_  N�rz  j;  K�r{  j=  ]r|  (]r}  (X   LoseUsesOnMiss (T/F)r~  jA  X   Lose uses even on missr  e]r�  (X   OneLossPerCombat (T/F)r�  jA  X    Doubling doesn't cost extra usesr�  ee�r�  j  K �r�  X   status_on_equipr�  X   SPD2r�  �r�  h6K�r�  X   hp_costr�  K�r�  je  X   Fluxr�  �r�  eu}r�  (j  Mf	hX   HeartOfGalesr�  hX   <orange>Heart Of Gales</orange>r�  hX   +1 SPD. Wind affinity only.r�  j  hnj  �j  }r�  j
  ]r�  j  ]r�  (j(  X   HeartofGalesr�  �r�  j+  X   oranger�  �r�  eu}r�  (j  Mg	hX   Iron Axer�  hX   Iron Axer�  hhj  h�j  �j  }r�  j
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  K�r�  j  K�r�  j  K�r�  j  K �r�  j  N�r�  j  X   Axer�  �r�  j  K �r�  eu}r�  (j  Mh	hX   HeartOfLightningr�  hX#   <orange>Heart Of Lightning</orange>r�  hX    +10 Crit. Thunder affinity only.r�  j  h�j  �j  }r�  j
  ]r�  j  ]r�  (j(  X   HeartofLightningr�  �r�  j+  X   oranger�  �r�  eu}r�  (j  Mi	hj/  hj0  hj1  j  h�j  �j  }r�  (j3  Kj4  Kuj
  ]r�  j  ]r�  (j7  N�r�  j9  N�r�  j;  K�r�  j=  j>  �r�  jD  K�r�  jF  jG  �r�  jI  N�r�  eu}r�  (j  Mj	hX
   Iron Lancer�  hX
   Iron Lancer�  hhj  h�j  �j  }r�  j
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  K�r�  j  K�r�  j  K�r�  j  K�r�  j  N�r�  j  X   Lancer�  �r�  j  K �r�  eu}r�  (j  Mk	hX   Javelinr�  hX   Javelinr�  hX   Cannot Counter. SPD -2.r�  j  h�j  �j  }r�  (j3  Kj4  Kuj
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  K�r�  j  K�r�  j  K�r�  j  J�����r�  j  K �r�  j  N�r�  j  X   Lancer�  �r�  j;  K�r�  j=  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  jA  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  jA  X    Doubling doesn't cost extra usesr�  ee�r�  X   cannot_counterr�  N�r�  h6K�r�  j�  X   SPD2r�  �r�  eu}r�  (j  Ml	hX   HeartOfFrostr�  hX   <orange>Heart Of Frost</orange>r�  hX   +1 DEF/RES. Ice affinity only.r�  j  h�j  �j  }r�  j
  ]r�  j  ]r�  (j(  X   HeartofFrostr�  �r�  j+  X   oranger�  �r�  eu}r�  (j  Mm	hX   Flashr�  hX   Flashr�  hX*   Lowers target's Accuracy by 20 for 1 turn.r�  j  h�j  �j  }r�  j
  ]r�  j  ]r�  (j  N�r�  j  N�r   j  K�r  j  K�r  j  K�r  j  K
�r  j  K
�r  j  N�r  j  X   Lightr  �r  jF  ]r	  (K�K�K e�r
  j]  N�r  j_  N�r  je  X   Glimmerr  �r  ja  X   Dazzledr  �r  eu}r  (j  Mn	hX   Shimmerr  hX   Shimmerr  hX   Cannot miss.r  j  h�j  �j  }r  (j3  Kj4  Kuj
  ]r  j  ]r  (j  N�r  j  N�r  j  K�r  j  K�r  j  K�r  j  K
�r  j  N�r  j  X   Lightr  �r   jF  ]r!  (K�K�K e�r"  j]  N�r#  j_  N�r$  h6K�r%  j;  K�r&  j=  ]r'  (]r(  (X   LoseUsesOnMiss (T/F)r)  jA  X   Lose uses even on missr*  e]r+  (X   OneLossPerCombat (T/F)r,  jA  X    Doubling doesn't cost extra usesr-  ee�r.  je  X   Glimmerr/  �r0  eu}r1  (j  Mp	hX	   ChurchKeyr2  hX
   Church Keyr3  hX   Opens any door in the church.r4  j  h�j  �j  }r5  (j3  Kj4  Kuj
  ]r6  j  ]r7  (j;  K�r8  j=  ]r9  ]r:  (X   LoseUsesOnMiss (T/F)r;  jA  X   Lose uses even on missr<  ea�r=  X
   can_unlockr>  X   region.nid.startswith('Door')r?  �r@  eu}rA  (j  Ms	hX   EntanglerB  hX   EntanglerC  hX'   Lowers target's Avoid by 20 for 1 turn.rD  j  h�j  �j  }rE  j
  ]rF  j  ]rG  (j  N�rH  j  N�rI  j  K�rJ  j  K�rK  j  K�rL  j  K�rM  j  N�rN  j  X   AnimarO  �rP  jF  ]rQ  (KUKUK e�rR  j]  N�rS  j_  N�rT  ja  X	   EntangledrU  �rV  j  K �rW  je  X   FirerX  �rY  eu}rZ  (j  Mt	hX   Firer[  hX   Firer\  hX*   Effective against horseback units. -1 SPD.r]  j  h�j  �j  }r^  (j3  Kj4  Kuj
  ]r_  j  ]r`  (j  N�ra  j  N�rb  j  K�rc  j  K�rd  j  K�re  j  K �rf  j  K�rg  j;  K�rh  j=  ]ri  (]rj  (X   LoseUsesOnMiss (T/F)rk  jA  X   Lose uses even on missrl  e]rm  (X   OneLossPerCombat (T/F)rn  jA  X    Doubling doesn't cost extra usesro  ee�rp  j  N�rq  j  X   Animarr  �rs  jF  ]rt  (K�K`K e�ru  j]  N�rv  j_  N�rw  X   effective_damagerx  }ry  (X   effective_tagsrz  ]r{  X   Horser|  aX   effective_multiplierr}  G@       X   effective_bonus_damager~  K X   show_effectiveness_flashr  �u�r�  h6K�r�  j�  X   SPD1r�  �r�  X   eval_warningr�  X   'Horse' in unit.tagsr�  �r�  je  X   Firer�  �r�  eu}r�  (j  Mu	hX   HeartOfDarknessr�  hX"   <orange>Heart Of Darkness</orange>r�  hX   +15 Hit. Dark affinity only.r�  j  h�j  �j  }r�  j
  ]r�  j  ]r�  (j(  X   HeartofDarknessr�  �r�  j+  X   oranger�  �r�  eu}r�  (j  Mv	hX   Iron Bowr�  hX   Iron Bowr�  hhj  j  j  �j  }r�  j
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  K�r�  j  K�r�  j  K�r�  j  K
�r�  j  N�r�  j  X   Bowr�  �r�  jx  }r�  (jz  ]r�  X   Flyingr�  aj}  G@       j~  K j  �u�r�  j  K �r�  j�  X   'Flying' in unit.tagsr�  �r�  eu}r�  (j  Mw	hX   Longbowr�  hX   Longbowr�  hX   Cannot Counter. SPD -1.r�  j  j  j  �j  }r�  (j3  K
j4  K
uj
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  N�r�  j  K�r�  j  J�����r�  j  X   Bowr�  �r�  j;  K
�r�  j=  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  jA  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  jA  X    Doubling doesn't cost extra usesr�  ee�r�  j  K�r�  j  K�r�  j  K�r�  j�  N�r�  jx  }r�  (jz  ]r�  X   Flyingr�  aj}  G@       j~  K j  �u�r�  j�  X   SPD1r�  �r�  j�  X   'Flying' in unit.tagsr�  �r�  eu}r�  (j  Mx	hX   HeartOfRadiancer�  hX"   <orange>Heart Of Radiance</orange>r�  hX   +15 Avoid. Holy affinity only.r�  j  j  j  �j  }r�  j
  ]r�  j  ]r�  (j(  X   HeartofRadiancer�  �r�  j+  X   oranger�  �r�  eu}r�  (j  My	hX   Steel Lancer�  hX   Steel Lancer�  hX   SPD -1.r�  j  j%  j  �j  }r�  (j3  Kj4  Kuj
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  N�r�  j  K�r�  j  K �r�  j  X   Lancer�  �r�  j;  K�r�  j=  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  jA  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  jA  X    Doubling doesn't cost extra usesr�  ee�r�  j  K �r�  j  K�r�  j  K�r�  j�  X   SPD1r�  �r�  eu}r�  (j  Mz	hX	   Axereaverr�  hX	   Axereaverr�  hX%   Reverses the weapon triangle. SPD -1.r�  j  j%  j  �j  }r�  (j3  Kj4  Kuj
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  N�r�  j  K�r�  j  K �r�  j  X   Lancer   �r  j;  K�r  j=  ]r  (]r  (X   LoseUsesOnMiss (T/F)r  jA  X   Lose uses even on missr  e]r  (X   OneLossPerCombat (T/F)r  jA  X    Doubling doesn't cost extra usesr	  ee�r
  X   reaverr  N�r  h6K�r  j  K
�r  j  K�r  j  K�r  j�  XU   target.get_weapon() and item_system.weapon_type(target, target.get_weapon()) == 'Axe'r  �r  eu}r  (j  M{	hj/  hj0  hj1  j  j%  j  �j  }r  (j3  Kj4  Kuj
  ]r  j  ]r  (j7  N�r  j9  N�r  j;  K�r  j=  j>  �r  jD  K�r  jF  jG  �r  jI  N�r  eu}r  (j  M|	hX   Greatbowr  hX	   Steel Bowr   hX   SPD -2.r!  j  j@  j  �j  }r"  (j3  Kj4  Kuj
  ]r#  j  ]r$  (j  N�r%  j  N�r&  j  N�r'  j  K�r(  j  K �r)  j  X   Bowr*  �r+  j;  K�r,  j=  ]r-  (]r.  (X   LoseUsesOnMiss (T/F)r/  jA  X   Lose uses even on missr0  e]r1  (X   OneLossPerCombat (T/F)r2  jA  X    Doubling doesn't cost extra usesr3  ee�r4  j  K �r5  j  K�r6  j  K�r7  j�  X   SPD2r8  �r9  jx  }r:  (jz  ]r;  X   Flyingr<  aj}  G@       j~  K j  �u�r=  j�  X   'Flying' in unit.tagsr>  �r?  eu}r@  (j  M}	hX   Mini_BowrA  hX   Mini BowrB  hX   Cannot Counter. SPD -1.rC  j  j@  j  �j  }rD  (j3  K
j4  K
uj
  ]rE  j  ]rF  (j  N�rG  j  N�rH  j  N�rI  j  K�rJ  j  J�����rK  j  X   BowrL  �rM  j;  K
�rN  j=  ]rO  (]rP  (X   LoseUsesOnMiss (T/F)rQ  jA  X   Lose uses even on missrR  e]rS  (X   OneLossPerCombat (T/F)rT  jA  X    Doubling doesn't cost extra usesrU  ee�rV  j  K�rW  h6K�rX  j  K�rY  j  K�rZ  j�  N�r[  jx  }r\  (jz  ]r]  X   Flyingr^  aj}  G@       j~  K j  �u�r_  j�  X   SPD1r`  �ra  j�  X   'Flying' in unit.tagsrb  �rc  eu}rd  (j  M~	hj/  hj0  hj1  j  j@  j  �j  }re  (j3  Kj4  Kuj
  ]rf  j  ]rg  (j7  N�rh  j9  N�ri  j;  K�rj  j=  j>  �rk  jD  K�rl  jF  jG  �rm  jI  N�rn  eu}ro  (j  M	hX	   Steel Axerp  hX	   Steel Axerq  hX   SPD -2.rr  j  jq  j  �j  }rs  (j3  Kj4  Kuj
  ]rt  j  ]ru  (j  N�rv  j  N�rw  j  K�rx  j  K�ry  j  K�rz  j  J�����r{  j  K �r|  j;  K�r}  j=  ]r~  (]r  (X   LoseUsesOnMiss (T/F)r�  jA  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  jA  X    Doubling doesn't cost extra usesr�  ee�r�  j  N�r�  j  X   Axer�  �r�  h6K�r�  j�  X   SPD2r�  �r�  eu}r�  (j  M�	hX
   EmeraldAxer�  hX   Emerald Axer�  hX$   Doubles the weapon triangle. SPD -1.r�  j  jq  j  �j  }r�  (j3  Kj4  Kuj
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  K�r�  j  K�r�  j  K�r�  j  J�����r�  j  K
�r�  j;  K�r�  j=  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  jA  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  jA  X    Doubling doesn't cost extra usesr�  ee�r�  j  N�r�  j  X   Axer�  �r�  h6K�r�  X   double_triangler�  N�r�  j�  X   SPD1r�  �r�  X   warningr�  N�r�  eu}r�  (j  M�	hj/  hj0  hj1  j  jq  j  �j  }r�  (j3  Kj4  Kuj
  ]r�  j  ]r�  (j7  N�r�  j9  N�r�  j;  K�r�  j=  j>  �r�  jD  K�r�  jF  jG  �r�  jI  N�r�  eu}r�  (j  M�	hj�  hj�  hj�  j  j�  j  �j  }r�  (j3  Kj4  Kuj
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  N�r�  j  K�r�  j  K �r�  j  j�  �r�  j;  K�r�  j=  j�  �r�  j  K �r�  j  K�r�  j  K�r�  j�  j�  �r�  eu}r�  (j  M�	hX   SapphireLancer�  hX   Sapphire Lancer�  hX$   Doubles the weapon triangle. SPD -1.r�  j  j�  j  �j  }r�  (j3  Kj4  Kuj
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  N�r�  j  K�r�  j  K �r�  j  X   Lancer�  �r�  j;  K�r�  j=  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  jA  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  jA  X    Doubling doesn't cost extra usesr�  ee�r�  h6K�r�  j  K
�r�  j  K�r�  j  K�r�  j�  N�r�  j�  X   SPD1r�  �r�  j�  N�r�  eu}r�  (j  M�	hj/  hj0  hj1  j  j�  j  �j  }r�  (j3  Kj4  Kuj
  ]r�  j  ]r�  (j7  N�r�  j9  N�r�  j;  K�r�  j=  j>  �r�  jD  K�r�  jF  jG  �r�  jI  N�r�  eu}r�  (j  M�	hX   Elixirr�  hX   Elixirr�  hX,   Fully recovers HP. Unit can act after using.r�  j  j�  j  �j  }r�  (j3  Kj4  Kuj
  ]r�  j  ]r�  (j7  N�r�  j9  N�r�  j;  K�r�  j=  ]r�  ]r�  (X   LoseUsesOnMiss (T/F)r�  jA  X   Lose uses even on missr�  ea�r   jD  Kc�r  jF  ]r  (K`K�K�e�r  jI  N�r  eu}r  (j  M�	hX   Silver Swordr  hX   Silver Swordr  hhj  j�  j  �j  }r  (j3  K	j4  K
uj
  ]r	  j  ]r
  (j  N�r  j  N�r  j  K�r  j  K�r  j  K�r  j  K
�r  j  K �r  j  N�r  j  X   Swordr  �r  j;  K
�r  j=  ]r  (]r  (X   LoseUsesOnMiss (T/F)r  jA  X   Lose uses even on missr  e]r  (X   OneLossPerCombat (T/F)r  jA  X    Doubling doesn't cost extra usesr  ee�r  eu}r  (j  M�	hX   Silver Lancer  hX   Silver Lancer   hhj  j�  j  �j  }r!  (j3  K
j4  K
uj
  ]r"  j  ]r#  (j  N�r$  j  N�r%  j  K�r&  j  K�r'  j  K�r(  j  K�r)  j  K �r*  j  N�r+  j  X   Lancer,  �r-  j;  K
�r.  j=  ]r/  (]r0  (X   LoseUsesOnMiss (T/F)r1  jA  X   Lose uses even on missr2  e]r3  (X   OneLossPerCombat (T/F)r4  jA  X    Doubling doesn't cost extra usesr5  ee�r6  eu}r7  (j  M�	hX   TheReclaimerr8  hX   <blue>The Reclaimer<blue>r9  hX6   Cavalier line only. 50% Lifelink. Effective Vs. Armor.r:  j  j�  j  �j  }r;  j
  ]r<  j  ]r=  (j  N�r>  j  N�r?  j  K�r@  j  K�rA  j  K�rB  j  J�����rC  j  K
�rD  j  N�rE  j  X   LancerF  �rG  X	   prf_classrH  ]rI  (X   CavalierrJ  X   PaladinrK  X   Great_KnightrL  e�rM  jx  }rN  (jz  ]rO  X   ArmorrP  aj}  G@       j~  K j  �u�rQ  j+  X   bluerR  �rS  X   lifelinkrT  G?�      �rU  eu}rV  (j  M�	hX
   TheDefilerrW  hX   <blue>The Defiler</blue>rX  hXF   Knight line only. Inflicts half damage on miss. Effective Vs. Clergy. rY  j  j�  j  �j  }rZ  j
  ]r[  j  ]r\  (j  N�r]  j  N�r^  j  K�r_  j  K�r`  j  K�ra  j  J�����rb  j  K�rc  j  N�rd  j  X   Axere  �rf  jH  ]rg  (X   Knightrh  X   Generalri  X   Great_Knightrj  e�rk  jx  }rl  (jz  ]rm  X   Clergyrn  aj}  G@       j~  K j  �u�ro  j+  X   bluerp  �rq  X   damage_on_missrr  G?�      �rs  eu}rt  (j  M�	hX   LunarArcru  hX	   Lunar Arcrv  hX%   Ignores DEF and grants Close Counter.rw  j  j�  j  �j  }rx  j
  ]ry  j  ]rz  (j�  X   CloseCounterr{  �r|  j  N�r}  j  N�r~  j  N�r  j  K�r�  X   alternate_resist_formular�  X   ZEROr�  �r�  j  K�r�  j  X   Bowr�  �r�  j  K
�r�  j  K�r�  j  K�r�  jx  }r�  (jz  ]r�  X   Flyingr�  aj}  G@       j~  K j  �u�r�  j�  X   'Flying' in unit.tagsr�  �r�  eu}r�  (j  M�	hj�  hj�  hj�  j  j�  j  �j  }r�  (j3  Kj4  Kuj
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  K�r�  j  K�r�  j  K�r�  j  J�����r�  j  K
�r�  j;  K�r�  j=  j�  �r�  j  N�r�  j  j�  �r�  h6K�r�  j�  N�r�  j�  j�  �r�  j�  N�r�  eu}r�  (j  M�	hX   AngelFeatherr�  hX   <yellow>Angel Feather</yellow>r�  hX&   Gives unit +10% growths. Cannot stack.r�  j  Nj  �j  }r�  (j3  K j4  Kuj
  ]r�  j  ]r�  (j7  N�r�  j9  N�r�  j;  K�r�  j=  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  jA  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  jA  X    Doubling doesn't cost extra usesr�  ee�r�  X   usable_in_baser�  N�r�  j+  X   yellowr�  �r�  jF  ]r�  (K�K�K�e�r�  ja  X   AngelFeatherr�  �r�  j  K �r�  j  K �r�  X   eval_availabler�  X#   not has_skill(unit, 'AngelFeather')r�  �r�  X   event_on_hitr�  X   Global AngelFeatherr�  �r�  eu}r�  (j  M�	hX   HowlingBlader�  hX   <blue>Howling Blade</blue>r�  hX4   Grants Distant Counter. Deals magic damage at range.r�  j  j  j  �j  }r�  j
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  K�r�  j  K�r�  j  K�r�  j  K�r�  j  K
�r�  j  N�r�  j  X   Swordr�  �r�  j+  X   bluer�  �r�  X   magic_at_ranger�  N�r�  j�  X   DistantCounterr�  �r�  eu}r�  (j  M�	hX   Macer�  hX   Macer�  hX   Cannot be Countered. SPD -2.r�  j  j  j  �j  }r�  (j3  K
j4  K
uj
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  N�r�  j  K�r�  j  J�����r�  j  X   Axer�  �r�  j;  K
�r�  j=  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  jA  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  jA  X    Doubling doesn't cost extra usesr�  ee�r�  j  K�r�  h6K�r�  j  K�r�  j  K�r�  X   cannot_be_counteredr�  N�r�  j�  X   SPD2r�  �r�  eu}r�  (j  M�	hj�  hj�  hj�  j  j  j  �j  }r�  (j3  Kj4  Kuj
  ]r�  j  ]r�  (j7  N�r�  j9  N�r 	  j;  K�r	  j=  j�  �r	  j�  N�r	  j+  j�  �r	  jF  j�  �r	  ja  j�  �r	  j  K �r	  j  K �r	  j�  j�  �r		  j�  j�  �r
	  eu}r	  (j  M�	hX
   Incinerater	  hX
   Incinerater	  hX   Ignores RES. Cooldown: 1.r	  j  j'  j  �j  }r	  (X   cooldownr	  K X   starting_cooldownr	  Kuj
  ]r	  j  ]r	  (j  N�r	  j  N�r	  j  K�r	  j  K�r	  j  K�r	  j  K �r	  j  N�r	  j  X   Animar	  �r	  jF  ]r	  (K�K K e�r	  j]  N�r	  j�  X   ZEROr 	  �r!	  j_  N�r"	  j  K
�r#	  je  X   Firer$	  �r%	  X   cooldownr&	  K�r'	  eu}r(	  (j  M�	hj[  hj\  hj]  j  j'  j  �j  }r)	  (j3  Kj4  Kuj
  ]r*	  j  ]r+	  (j  N�r,	  j  N�r-	  j  K�r.	  j  K�r/	  j  K�r0	  j  K �r1	  j  K�r2	  j;  K�r3	  j=  ji  �r4	  j  N�r5	  j  jr  �r6	  jF  jt  �r7	  j]  N�r8	  j_  N�r9	  jx  }r:	  (jz  j{  j}  G@       j~  K j  �u�r;	  h6K�r<	  j�  j�  �r=	  j�  j�  �r>	  je  j�  �r?	  eu}r@	  (j  M�	hj�  hj�  hj�  j  j'  j  �j  }rA	  (j3  Kj4  Kuj
  ]rB	  j  ]rC	  (j7  N�rD	  j9  N�rE	  j;  K�rF	  j=  j�  �rG	  j�  N�rH	  j+  j�  �rI	  jF  j�  �rJ	  ja  j�  �rK	  j  K �rL	  j  K �rM	  j�  j�  �rN	  j�  j�  �rO	  eu}rP	  (j  M�	hX   WardensCleaverrQ	  hX   <blue>Warden's Cleaver<blue>rR	  hX&   While equipped unit cannot be doubled.rS	  j  j>  j  �j  }rT	  j
  ]rU	  j  ]rV	  (j  N�rW	  j  N�rX	  j  K�rY	  j  K�rZ	  j  K�r[	  j  K�r\	  j  K
�r]	  j  N�r^	  j  X   Axer_	  �r`	  j+  X   bluera	  �rb	  j�  X   Wardenrc	  �rd	  eu}re	  (j  M�	hX
   BloodLancerf	  hX   Blood Lancerg	  hX   Has Lifelink. SPD -1.rh	  j  j>  j  �j  }ri	  (j3  K
j4  K
uj
  ]rj	  j  ]rk	  (j  N�rl	  j  N�rm	  j  N�rn	  j  K�ro	  j  J�����rp	  j  X   Lancerq	  �rr	  j;  K
�rs	  j=  ]rt	  (]ru	  (X   LoseUsesOnMiss (T/F)rv	  jA  X   Lose uses even on missrw	  e]rx	  (X   OneLossPerCombat (T/F)ry	  jA  X    Doubling doesn't cost extra usesrz	  ee�r{	  j  K�r|	  j  K�r}	  j  K�r~	  jT  G?�      �r	  h6K�r�	  j�  X   SPD1r�	  �r�	  eu}r�	  (j  M�	hX   EtherealBlader�	  hX   Ethereal Blader�	  hX   Targets RES.r�	  j  j>  j  �j  }r�	  (j3  K
j4  K
uj
  ]r�	  j  ]r�	  (j  N�r�	  j  N�r�	  j  K�r�	  j  K�r�	  j  K�r�	  j  K�r�	  j  K�r�	  j;  K
�r�	  j=  ]r�	  (]r�	  (X   LoseUsesOnMiss (T/F)r�	  jA  X   Lose uses even on missr�	  e]r�	  (X   OneLossPerCombat (T/F)r�	  jA  X    Doubling doesn't cost extra usesr�	  ee�r�	  j  N�r�	  j  X   Swordr�	  �r�	  j�  X   MAGIC_DEFENSEr�	  �r�	  h6K�r�	  eu}r�	  (j  M�	hj�  hj�  hj�  j  j>  j  �j  }r�	  (j3  Kj4  Kuj
  ]r�	  j  ]r�	  (j7  N�r�	  j9  N�r�	  j;  K�r�	  j=  j�  �r�	  j�  N�r�	  j+  j�  �r�	  jF  j�  �r�	  ja  j�  �r�	  j  K �r�	  j  K �r�	  j�  j�  �r�	  j�  j�  �r�	  eu}r�	  (j  M�	hj�  hj�  hj�  j  h�j  �j  }r�	  (j3  K
j4  K
uj
  ]r�	  j  ]r�	  (j  N�r�	  j  N�r�	  j  N�r�	  j  K�r�	  j  J�����r�	  j  j�  �r�	  j;  K
�r�	  j=  j�  �r�	  j  K�r�	  h6K�r�	  j  K�r�	  j  K�r�	  j�  N�r�	  j�  j�  �r�	  eu}r�	  (j  M�	hX   Steel Swordr�	  hX   Steel Swordr�	  hX   SPD -1.r�	  j  hj  �j  }r�	  (j3  Kj4  Kuj
  ]r�	  j  ]r�	  (j  N�r�	  j  N�r�	  j  K�r�	  j  K�r�	  j  K�r�	  j  K�r�	  j  K �r�	  j;  K�r�	  j=  ]r�	  (]r�	  (X   LoseUsesOnMiss (T/F)r�	  jA  X   Lose uses even on missr�	  e]r�	  (X   OneLossPerCombat (T/F)r�	  jA  X    Doubling doesn't cost extra usesr�	  ee�r�	  j  N�r�	  j  X   Swordr�	  �r�	  j�  X   SPD1r�	  �r�	  eu}r�	  (j  M�	hX   Lunar�	  hX   Lunar�	  hX"   Ignore's enemy resistance. SPD -1.r�	  j  hnj  �j  }r�	  (j3  Kj4  Kuj
  ]r�	  j  ]r�	  (j  N�r�	  j  N�r�	  j  N�r�	  j  K�r�	  j  K �r�	  j  X   Darkr�	  �r�	  j]  N�r�	  j�  X   ZEROr�	  �r�	  j;  K�r�	  j=  ]r�	  (]r�	  (X   LoseUsesOnMiss (T/F)r�	  jA  X   Lose uses even on missr�	  e]r�	  (X   OneLossPerCombat (T/F)r�	  jA  X    Doubling doesn't cost extra usesr�	  ee�r�	  j  K
�r�	  j  K�r�	  j  K�r�	  j_  N�r�	  h6K�r�	  j�  X   SPD1r�	  �r�	  je  X   Fluxr�	  �r 
  eu}r
  (j  M�	hj�	  hj�	  hj�	  j  hj  �j  }r
  (j3  K
j4  K
uj
  ]r
  j  ]r
  (j  N�r
  j  N�r
  j  K�r
  j  K�r
  j  K�r	
  j  K�r

  j  K�r
  j;  K
�r
  j=  j�	  �r
  j  N�r
  j  j�	  �r
  j�  j�	  �r
  h6K�r
  eu}r
  (j  M�	hX   Shover
  hX   <green>Shove</>r
  hX!   Free action. Shove target 1 tile.r
  j  h�j  �j  }r
  (j	  K j	  Kuj
  ]r
  j  ]r
  (X   target_unitr
  N�r
  j  K�r
  j  K�r
  X   eval_target_restrict_2r
  X   target.team == 'player'r
  �r
  jI  N�r 
  j&	  K�r!
  X   shove_on_end_combatr"
  K�r#
  eu}r$
  (j  M�	hX   Treatr%
  hX   Treatr&
  hX*   Free Action. Restores (User's MAG + 2) HP.r'
  j  h�j  �j  }r(
  (j	  K j	  Kuj
  ]r)
  j  ]r*
  (j9  N�r+
  X   never_use_battle_animationr,
  N�r-
  j  K�r.
  j  K�r/
  X   equation_healr0
  X   TREATr1
  �r2
  jI  N�r3
  hK�r4
  j&	  K�r5
  eu}r6
  (j  M�	hj
  hj
  hj
  j  h�j  �j  }r7
  (j	  K j	  Kuj
  ]r8
  j  ]r9
  (j
  N�r:
  j  K�r;
  j  K�r<
  j
  j
  �r=
  jI  N�r>
  j&	  K�r?
  j"
  K�r@
  eu}rA
  (j  M�	hX   TriprB
  hX   TriprC
  hX/   Free Action. Set target's move to 0 for 1 turn.rD
  j  h�j  �j  }rE
  (j	  K j	  Kuj
  ]rF
  j  ]rG
  (j  N�rH
  j  K�rI
  j  K�rJ
  ja  X   TrippedrK
  �rL
  jI  N�rM
  j&	  K�rN
  eu}rO
  (j  M�	hj
  hj
  hj
  j  h�j  �j  }rP
  (j	  K j	  Kuj
  ]rQ
  j  ]rR
  (j
  N�rS
  j  K�rT
  j  K�rU
  j
  j
  �rV
  jI  N�rW
  j&	  K�rX
  j"
  K�rY
  eu}rZ
  (j  M�	hj
  hj
  hj
  j  hnj  �j  }r[
  (j	  K j	  Kuj
  ]r\
  j  ]r]
  (j
  N�r^
  j  K�r_
  j  K�r`
  j
  j
  �ra
  jI  N�rb
  j&	  K�rc
  j"
  K�rd
  eu}re
  (j  M�	hj
  hj
  hj
  j  hj  �j  }rf
  (j	  K j	  Kuj
  ]rg
  j  ]rh
  (j
  N�ri
  j  K�rj
  j  K�rk
  j
  j
  �rl
  jI  N�rm
  j&	  K�rn
  j"
  K�ro
  eu}rp
  (j  M�	hX   AxeOfWoerq
  hX   <purple>Axe Of Woe</purple>rr
  hX   Shayla Only. +2 DEF.rs
  j  h�j  �j  }rt
  (j3  Kj4  Kuj
  ]ru
  j  ]rv
  (j  N�rw
  j  N�rx
  j  K�ry
  j  K�rz
  j  K�r{
  j  K�r|
  j  K�r}
  j  N�r~
  j  X   Axer
  �r�
  X   prf_unitr�
  ]r�
  X   Shaylar�
  a�r�
  j�  X   DEFr�
  �r�
  j+  X   purpler�
  �r�
  j;  K�r�
  j=  ]r�
  (]r�
  (X   LoseUsesOnMiss (T/F)r�
  jA  X   Lose uses even on missr�
  e]r�
  (X   OneLossPerCombat (T/F)r�
  jA  X    Doubling doesn't cost extra usesr�
  ee�r�
  eu}r�
  (j  M�	hX   Canterlanzer�
  hX   <purple>Canterlanze</purple>r�
  hX>   Zoya Only. Draw Back: 2. Grants Canto and Pass while equipped.r�
  j  h�j  �j  }r�
  (j3  Kj4  Kuj
  ]r�
  j  ]r�
  (j  N�r�
  j  N�r�
  j  K�r�
  j  K�r�
  j  K�r�
  j  K
�r�
  j  N�r�
  j  X   Lancer�
  �r�
  j�
  ]r�
  X   Raelinr�
  a�r�
  j+  X   purpler�
  �r�
  j;  K�r�
  j=  ]r�
  (]r�
  (X   LoseUsesOnMiss (T/F)r�
  jA  X   Lose uses even on missr�
  e]r�
  (X   OneLossPerCombat (T/F)r�
  jA  X    Doubling doesn't cost extra usesr�
  ee�r�
  X    draw_back_on_end_combat_initiater�
  K�r�
  X   multi_status_on_equipr�
  ]r�
  (X
   CantoSharpr�
  X   Passr�
  e�r�
  j  K �r�
  eu}r�
  (j  M�	hX   Disasterr�
  hX   <purple>Disaster</purple>r�
  hX3   Carlin Only. Backdash: 1. Cannot Counter or Double.r�
  j  hnj  �j  }r�
  (j3  Kj4  Kuj
  ]r�
  j  ]r�
  (j  N�r�
  j  N�r�
  j  K�r�
  j  K�r�
  j  K�r�
  j  K
�r�
  j  K#�r�
  j  N�r�
  j  X   Darkr�
  �r�
  jF  ]r�
  (KHK KAe�r�
  j]  N�r�
  j;  K�r�
  j=  ]r�
  (]r�
  (X   LoseUsesOnMiss (T/F)r�
  jA  X   Lose uses even on missr�
  e]r�
  (X   OneLossPerCombat (T/F)r�
  jA  X    Doubling doesn't cost extra usesr�
  ee�r�
  j�
  ]r�
  X   Carlinr�
  a�r�
  j+  X   purpler�
  �r�
  j�  N�r�
  X	   no_doubler�
  N�r�
  je  X   Fluxr�
  �r�
  X   backdash_on_end_combatr�
  K�r�
  eu}r�
  (j  M�	hj%
  hj&
  hj'
  j  j
  j  �j  }r�
  (j	  K j	  Kuj
  ]r�
  j  ]r�
  (j9  N�r�
  j,
  N�r�
  j  K�r�
  j  K�r�
  j0
  j1
  �r�
  jI  N�r�
  hK�r�
  j&	  K�r�
  eu}r�
  (j  M�	hj�  hj�  hhj  j  j  �j  }r�
  j
  ]r�
  j  ]r�
  (j  N�r�
  j  N�r�
  j  K�r�
  j  K�r�
  j  K�r�
  j  K �r�
  j  N�r�
  j  j�  �r�
  j  K �r�
  eu}r�
  (j  M�	hjL  hjM  hjN  j  jy  j  �j  }r�
  j
  ]r�
  j  ]r�
  (j  N�r�
  j  N�r�
  j  K�r   j  K�r  j  K�r  j  K�r  j  N�r  j  jY  �r  jF  j[  �r  j]  N�r  j_  N�r  ja  jb  �r	  j  K �r
  je  jf  �r  eu}r  (j  M�	hj�	  hj�	  hj�	  j  j�  j  �j  }r  (j3  Kj4  Kuj
  ]r  j  ]r  (j  N�r  j  N�r  j  K�r  j  K�r  j  K�r  j  K�r  j  K �r  j;  K�r  j=  j�	  �r  j  N�r  j  j�	  �r  j�  j�	  �r  eu}r  (j  M�	hj�  hj�  hj�  j  j�  j  �j  }r  (j3  Kj4  Kuj
  ]r  j  ]r  (j  N�r   j  N�r!  j  K�r"  j  K�r#  j  K�r$  j  J�����r%  j  K �r&  j  N�r'  j  j�  �r(  j;  K�r)  j=  j�  �r*  j�  N�r+  h6K�r,  j�  j�  �r-  eu}r.  (j  M�	hj�  hj�  hhj  j�  j  �j  }r/  j
  ]r0  j  ]r1  (j  N�r2  j  N�r3  j  K�r4  j  K�r5  j  K�r6  j  K �r7  j  N�r8  j  j�  �r9  j  K �r:  eu}r;  (j  M�	hj  hj  hhj  j�  j  �j  }r<  j
  ]r=  j  ]r>  (j  N�r?  j  N�r@  j  K�rA  j  K�rB  j  K�rC  j  K
�rD  j  N�rE  j  j  �rF  j  K �rG  eu}rH  (j  M�	hj�  hj�  hj�  j  j�  j  �j  }rI  (j3  Kj4  Kuj
  ]rJ  j  ]rK  (j  N�rL  j  N�rM  j  N�rN  j  K�rO  j  K �rP  j  j�  �rQ  j;  K�rR  j=  j�  �rS  h6K�rT  j  K
�rU  j  K�rV  j  K�rW  j�  N�rX  j�  j�  �rY  j�  N�rZ  eu}r[  (j  M�	hX   Hammerr\  hX   Hammerr]  hX(   Effective against armored units. SPD -1.r^  j  j�  j  �j  }r_  (j3  Kj4  Kuj
  ]r`  j  ]ra  (j  N�rb  j  N�rc  j  N�rd  j  K�re  j  J�����rf  j  X   Axerg  �rh  j;  K�ri  j=  ]rj  (]rk  (X   LoseUsesOnMiss (T/F)rl  jA  X   Lose uses even on missrm  e]rn  (X   OneLossPerCombat (T/F)ro  jA  X    Doubling doesn't cost extra usesrp  ee�rq  j  K�rr  h6K�rs  j  K�rt  j  K�ru  jx  }rv  (jz  ]rw  X   Armorrx  aj}  G@       j~  K j  �u�ry  j�  X   SPD1rz  �r{  j�  X   'Armor' in unit.tagsr|  �r}  eu}r~  (j  M�	hj�  hj�  hhj  j�  j  �j  }r  j
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  K�r�  j  K�r�  j  K�r�  j  K �r�  j  N�r�  j  j�  �r�  j  K �r�  eu}r�  (j  M�	hj�  hj�  hj�  j  j  j  �j  }r�  (j3  Kj4  Kuj
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  N�r�  j  K�r�  j  K �r�  j  j�  �r�  j;  K�r�  j=  j�  �r�  j  K �r�  j  K�r�  j  K�r�  j�  j�  �r�  eu}r�  (j  M�	hj�  hj�  hj�  j  j  j  �j  }r�  (j3  Kj4  Kuj
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  K�r�  j  K�r�  j  K�r�  j  J�����r�  j  K �r�  j  N�r�  j  j�  �r�  j;  K�r�  j=  j�  �r�  j�  N�r�  h6K�r�  j�  j�  �r�  eu}r�  (j  M�	hj  hj  hj  j  j  j  �j  }r�  (j3  Kj4  Kuj
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  K�r�  j  K�r�  j  K�r�  j  K
�r�  j  N�r�  j  j  �r�  jF  j!  �r�  j]  N�r�  j_  N�r�  h6K�r�  j;  K�r�  j=  j'  �r�  je  j/  �r�  eu}r�  (j  M�	hX	   Steel Bowr�  hX	   Steel Bowr�  hX   SPD -1.r�  j  j)  j  �j  }r�  (j3  Kj4  Kuj
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  N�r�  j  K�r�  j  K�r�  j  X   Bowr�  �r�  j;  K�r�  j=  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  jA  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  jA  X    Doubling doesn't cost extra usesr�  ee�r�  j  K �r�  j  K�r�  j  K�r�  j�  X   SPD1r�  �r�  jx  }r�  (jz  ]r�  X   Flyingr�  aj}  G@       j~  K j  �u�r�  j�  X   'Flying' in unit.tagsr�  �r�  eu}r�  (j  M�	hX   Lancereaverr�  hX   Lancereaverr�  hX   Reverses the weapon triangle.r�  j  j=  j  �j  }r�  (j3  Kj4  Kuj
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  K�r�  j  K�r�  j  K�r�  j  K�r�  j  K
�r�  j  N�r�  j  X   Swordr�  �r�  j;  K�r�  j=  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  jA  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  jA  X    Doubling doesn't cost extra usesr�  ee�r�  h6K�r�  j  N�r�  j�  XW   target.get_weapon() and item_system.weapon_type(target, target.get_weapon()) == 'Lance'r�  �r�  eu}r   (j  M�	hX
   Mageslayerr  hX
   Mageslayerr  hX   Effective against caster units.r  j  jQ  j  �j  }r  (j3  Kj4  Kuj
  ]r  j  ]r  (j  N�r  j  N�r  j  N�r	  j  K�r
  j  K�r  j  X   Swordr  �r  j;  K�r  j=  ]r  (]r  (X   LoseUsesOnMiss (T/F)r  jA  X   Lose uses even on missr  e]r  (X   OneLossPerCombat (T/F)r  jA  X    Doubling doesn't cost extra usesr  ee�r  j  K�r  j  K�r  j  K�r  h6K�r  jx  }r  (jz  ]r  X   Casterr  aj}  G@       j~  K j  �u�r  j�  X   'Caster' in unit.tagsr  �r   eu}r!  (j  M�	hjf	  hjg	  hjh	  j  jd  j  �j  }r"  (j3  K
j4  K
uj
  ]r#  j  ]r$  (j  N�r%  j  N�r&  j  N�r'  j  K�r(  j  J�����r)  j  jq	  �r*  j;  K
�r+  j=  jt	  �r,  j  K�r-  j  K�r.  j  K�r/  jT  G?�      �r0  h6K�r1  j�  j�	  �r2  eu}r3  (j  M�	hj  hj  hhj  jx  j  �j  }r4  j
  ]r5  j  ]r6  (j  N�r7  j  N�r8  j  K�r9  j  K�r:  j  K�r;  j  K
�r<  j  N�r=  j  j  �r>  j  K �r?  eu}r@  (j  M�	hj�  hj�  hj�  j  j�  j  �j  }rA  (j3  Kj4  Kuj
  ]rB  j  ]rC  (j  N�rD  j  N�rE  j  K�rF  j  K�rG  j  K�rH  j  J�����rI  j  K �rJ  j  N�rK  j  j�  �rL  j;  K�rM  j=  j�  �rN  j�  N�rO  h6K�rP  j�  j�  �rQ  eu}rR  (j  M�	hjp  hjq  hjr  j  j�  j  �j  }rS  (j3  Kj4  Kuj
  ]rT  j  ]rU  (j  N�rV  j  N�rW  j  K�rX  j  K�rY  j  K�rZ  j  J�����r[  j  K �r\  j;  K�r]  j=  j~  �r^  j  N�r_  j  j�  �r`  h6K�ra  j�  j�  �rb  eu}rc  (j  M�	hj[  hj\  hj]  j  j�  j  �j  }rd  (j3  Kj4  Kuj
  ]re  j  ]rf  (j  N�rg  j  N�rh  j  K�ri  j  K�rj  j  K�rk  j  K �rl  j  K�rm  j;  K�rn  j=  ji  �ro  j  N�rp  j  jr  �rq  jF  jt  �rr  j]  N�rs  j_  N�rt  jx  }ru  (jz  j{  j}  G@       j~  K j  �u�rv  h6K�rw  j�  j�  �rx  j�  j�  �ry  je  j�  �rz  eu}r{  (j  M�	hj  hj  hhj  j�  j  �j  }r|  j
  ]r}  j  ]r~  (j  N�r  j  N�r�  j  K�r�  j  K�r�  j  K�r�  j  K
�r�  j  N�r�  j  j  �r�  j  K �r�  eu}r�  (j  M�	hjp  hjq  hjr  j  j�  j  �j  }r�  (j3  Kj4  Kuj
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  K�r�  j  K�r�  j  K�r�  j  J�����r�  j  K �r�  j;  K�r�  j=  j~  �r�  j  N�r�  j  j�  �r�  h6K�r�  j�  j�  �r�  eu}r�  (j  M�	hj�	  hj�	  hj�	  j  j�  j  �j  }r�  (j3  Kj4  Kuj
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  K�r�  j  K�r�  j  K�r�  j  K�r�  j  K �r�  j;  K�r�  j=  j�	  �r�  j  N�r�  j  j�	  �r�  j�  j�	  �r�  eu}r�  (j  M�	hj�  hj�  hhj  Nj  �j  }r�  j
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  K�r�  j  K�r�  j  K�r�  j  K�r�  j  N�r�  j  j�  �r�  j  K �r�  eu}r�  (j  M�	hj�  hj�  hj�  j  Nj  �j  }r�  (j3  Kj4  Kuj
  ]r�  j  ]r�  (j  N�r�  j  N�r�  j  N�r�  j  K�r�  j  K�r�  j  j�  �r�  j;  K�r�  j=  j�  �r�  j  K �r�  j  K�r�  j  K�r�  j�  j�  �r�  jx  }r�  (jz  j�  j}  G@       j~  K j  �u�r�  j�  j�  �r�  euehE]r�  (}r�  (j  M�hX   HeartofFirer�  j  hj  }r�  X   initiator_nidr�  NX   subskillr�  Nu}r�  (j  M�hX   HeartofGalesr�  j  hnj  }r�  j�  Nj�  Nu}r�  (j  M�hX   HeartofLightningr�  j  h�j  }r�  j�  Nj�  Nu}r�  (j  M�hX   HeartofFrostr�  j  h�j  }r�  j�  Nj�  Nu}r�  (j  M�hX   HeartofDarknessr�  j  h�j  }r�  j�  Nj�  Nu}r�  (j  M�hX   HeartofRadiancer�  j  j  j  }r�  j�  Nj�  Nu}r�  (j  M�hX   SPD1r�  j  j%  j  }r�  j�  Nj�  Nu}r�  (j  M�hX   SPD2r�  j  j@  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  jq  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  MhX   DistantCounterr�  j  j  j  }r�  j�  Nj�  Nu}r�  (j  MhX   Wardenr�  j  j>  j  }r�  j�  Nj�  Nu}r�  (j  M�hX   trader�  j  hj  }r�  j�  Nj�  Nu}r�  (j  M�hX   Shover�  j  hj  }r�  X   ability_item_uidr�  M�	sj�  Nj�  Nu}r�  (j  M�hX   Flailr�  j  hj  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  hnj  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  hnj  }r�  j�  M�	sj�  Nj�  Nu}r�  (j  M�hX   RiskyBusinessr�  j  hnj  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  h�j  }r   j�  Nj�  Nu}r  (j  M�hj�  j  h�j  }r  j�  M�	sj�  Nj�  Nu}r  (j  M�hX   Bungler  j  h�j  }r  j�  Nj�  Nu}r  (j  M�hj�  j  h�j  }r  j�  Nj�  Nu}r  (j  M�hj�  j  h�j  }r	  j�  M�	sj�  Nj�  Nu}r
  (j  M�hX   Tripr  j  h�j  }r  j�  M�	sj�  Nj�  Nu}r  (j  M�hj�  j  h�j  }r  j�  Nj�  Nu}r  (j  M�hX	   Protectorr  j  h�j  }r  j�  Nj�  Nu}r  (j  M�hj�  j  h�j  }r  j�  M�	sj�  Nj�  Nu}r  (j  M�hX   ElwynnSkillr  j  h�j  }r  j�  M�	sj�  Nj�  Nu}r  (j  M�hj�  j  h�j  }r  j�  Nj�  Nu}r  (j  M�hj�  j  h�j  }r  j�  Nj�  Nu}r  (j  M�hX   DivineConduitr  j  h�j  }r  j�  Nj�  Nu}r  (j  M�hX   Exchanger  j  h�j  }r   j�  Nj�  Nu}r!  (j  M�hj�  j  h�j  }r"  j�  Nj�  Nu}r#  (j  M�hj  j  h�j  }r$  j�  Nj�  Nu}r%  (j  M�hj�  j  h�j  }r&  j�  Nj�  Nu}r'  (j  M�hX   Enthrallr(  j  h�j  }r)  j�  Nj�  Nu}r*  (j  M�hj�  j  j  j  }r+  j�  Nj�  Nu}r,  (j  M�hj  j  j  j  }r-  j�  Nj�  Nu}r.  (j  M�hj�  j  j  j  }r/  j�  Nj�  Nu}r0  (j  M�hX   Potshotr1  j  j  j  }r2  j�  Nj�  Nu}r3  (j  M�hj�  j  j%  j  }r4  j�  Nj�  Nu}r5  (j  M�hj  j  j%  j  }r6  j�  Nj�  Nu}r7  (j  M�hj�  j  j%  j  }r8  j�  Nj�  Nu}r9  (j  M�hX
   Instructorr:  j  j%  j  }r;  j�  Nj�  Nu}r<  (j  M�hj�  j  j@  j  }r=  j�  Nj�  Nu}r>  (j  M�hj  j  j@  j  }r?  j�  Nj�  Nu}r@  (j  M�hj�  j  j@  j  }rA  j�  Nj�  Nu}rB  (j  M�hj�  j  jZ  j  }rC  j�  Nj�  Nu}rD  (j  M�hj�  j  jq  j  }rE  j�  Nj�  Nu}rF  (j  M�hj  j  jq  j  }rG  j�  Nj�  Nu}rH  (j  M�hj�  j  jq  j  }rI  j�  Nj�  Nu}rJ  (j  M�hX   NemophilistrK  j  jq  j  }rL  j�  Nj�  Nu}rM  (j  M�hj�  j  j�  j  }rN  j�  Nj�  Nu}rO  (j  M�hj  j  j�  j  }rP  j�  Nj�  Nu}rQ  (j  M�hj�  j  j�  j  }rR  j�  Nj�  Nu}rS  (j  M�hX   BeachcomberrT  j  j�  j  }rU  j�  Nj�  Nu}rV  (j  M�hj�  j  j�  j  }rW  j�  Nj�  Nu}rX  (j  M�hj  j  j�  j  }rY  j�  Nj�  Nu}rZ  (j  M�hj�  j  j�  j  }r[  j�  Nj�  Nu}r\  (j  M�hX   Bolsterr]  j  j�  j  }r^  j�  Nj�  Nu}r_  (j  M�hX   Repairr`  j  j�  j  }ra  j�  Nj�  Nu}rb  (j  M�hj�  j  j�  j  }rc  j�  Nj�  Nu}rd  (j  M�hj  j  j�  j  }re  j�  Nj�  Nu}rf  (j  M�hX   AspiringLeaderrg  j  j�  j  }rh  j�  Nj�  Nu}ri  (j  M�hX   Cantorj  j  j�  j  }rk  j�  Nj�  Nu}rl  (j  M�hj�  j  j�  j  }rm  j�  Nj�  Nu}rn  (j  M�hX   Anticritro  j  j�  j  }rp  j�  Nj�  Nu}rq  (j  M�hX   DivineBlessingrr  j  j�  j  }rs  j�  Nj�  Nu}rt  (j  Mhj�  j  j�  j  }ru  j�  Nj�  Nu}rv  (j  Mhjo  j  j�  j  }rw  j�  Nj�  Nu}rx  (j  Mhj�  j  j  j  }ry  j�  Nj�  Nu}rz  (j  Mhjo  j  j  j  }r{  j�  Nj�  Nu}r|  (j  Mhj�  j  j'  j  }r}  j�  Nj�  Nu}r~  (j  Mhjo  j  j'  j  }r  j�  Nj�  Nu}r�  (j  Mhj�  j  j>  j  }r�  j�  Nj�  Nu}r�  (j  Mhjo  j  j>  j  }r�  j�  Nj�  Nu}r�  (j  Mhjj  j  j>  j  }r�  j�  Nj�  Nu}r�  (j  MchX   CloseCounterr�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  Mhj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j)  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  jd  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  Mghj�  j  jW  j  }r�  j�  Nj�  Nu}r�  (j  MhhX   NoAvoidr�  j  jW  j  }r�  j�  Nj�  Nu}r�  (j  Mihj�  j  jm  j  }r�  j�  Nj�  Nu}r�  (j  Mjhj�  j  jm  j  }r�  j�  Nj�  Nu}r�  (j  Mkhj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  Mlhj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  Mmhj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  Mnhj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  Mohj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  Mphj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  Mqhj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  Mrhj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  Mshj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  Mthj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  Muhj�  j  j
  j  }r�  j�  Nj�  Nu}r�  (j  Mvhj�  j  j  j  }r�  j�  Nj�  Nu}r�  (j  Mwhj�  j  j.  j  }r�  j�  Nj�  Nu}r�  (j  Mxhj�  j  jB  j  }r�  j�  Nj�  Nu}r�  (j  Myhj�  j  jT  j  }r�  j�  Nj�  Nu}r�  (j  Mzhj�  j  jf  j  }r�  j�  Nj�  Nu}r�  (j  M{hj�  j  jf  j  }r�  j�  Nj�  Nu}r�  (j  M|hj�  j  jy  j  }r�  j�  Nj�  Nu}r�  (j  M}hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M~hjj  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j)  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j=  j  }r�  j�  Nj�  Nu}r�  (j  M�hjj  j  j=  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  jQ  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  jd  j  }r�  j�  Nj�  Nu}r�  (j  M�hX	   Bloodlustr�  j  jd  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  jx  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  jx  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r�  j�  Nj�  Nu}r�  (j  M�hj�  j  j�  j  }r   j�  Nj�  Nu}r  (j  M�hX   Flyingr  j  j�  j  }r  j�  Nj�  Nu}r  (j  M�hj�  j  j�  j  }r  j�  Nj�  Nu}r  (j  M�hj  j  j�  j  }r  j�  Nj�  Nu}r  (j  M�hX	   NPCVisionr	  j  Nj  }r
  j�  Nj�  Nu}r  (j  M�hj	  j  Nj  }r  j�  Nj�  Nu}r  (j  M�hj	  j  Nj  }r  j�  Nj�  Nu}r  (j  M�hX   Exploitabler  j  j�  j  }r  (X   turnsr  KX   starting_turnsr  Kuj�  j%  j�  Nu}r  (j  M�hX   Forestr  j  j  j  }r  j�  Nj�  Nu}r  (j  M�hX   AngelFeatherr  j  hj  }r  j�  hj�  NueX   terrain_status_registryr  }r  KKX   Forestr  �r  M�sX   regionsr  ]r  (}r   (hX   House1r!  X   region_typer"  capp.events.regions
RegionType
r#  X   eventr$  �r%  Rr&  hKK�r'  X   sizer(  KK�r)  X   sub_nidr*  X   Visitr+  X	   conditionr,  X   Truer-  X	   time_leftr.  NX	   only_oncer/  �X   interrupt_mover0  �j  }r1  u}r2  (hX   House2r3  j"  j&  hKK�r4  j(  j)  j*  X   Visitr5  j,  j-  j.  Nj/  �j0  �j  }r6  u}r7  (hX   VisionTopLeftr8  j"  j#  X   visionr9  �r:  Rr;  hKK�r<  j(  KK�r=  j*  X   2r>  j,  j-  j.  Nj/  �j0  �j  }r?  u}r@  (hX   VisionTopRightrA  j"  j;  hKK�rB  j(  KK�rC  j*  j>  j,  j-  j.  Nj/  �j0  �j  }rD  u}rE  (hX   VisionBotLeftrF  j"  j;  hKK�rG  j(  KK�rH  j*  j>  j,  j-  j.  Nj/  �j0  �j  }rI  u}rJ  (hX   VisionBotMidrK  j"  j;  hKK�rL  j(  KK�rM  j*  X   3rN  j,  j-  j.  Nj/  �j0  �j  }rO  u}rP  (hX   VisionBotRightrQ  j"  j;  hKK�rR  j(  KK�rS  j*  j>  j,  j-  j.  Nj/  �j0  �j  }rT  u}rU  (hX   Form1rV  j"  j#  X	   formationrW  �rX  RrY  hKK �rZ  j(  j)  j*  Nj,  j-  j.  Nj/  �j0  �j  }r[  u}r\  (hX   Form2r]  j"  jY  hKK �r^  j(  j)  j*  Nj,  j-  j.  Nj/  �j0  �j  }r_  u}r`  (hX   Form3ra  j"  jY  hKK�rb  j(  j)  j*  Nj,  j-  j.  Nj/  �j0  �j  }rc  u}rd  (hX   Form4re  j"  jY  hKK�rf  j(  j)  j*  Nj,  j-  j.  Nj/  �j0  �j  }rg  u}rh  (hX   Form5ri  j"  jY  hKK�rj  j(  j)  j*  Nj,  j-  j.  Nj/  �j0  �j  }rk  u}rl  (hX   Form6rm  j"  jY  hKK�rn  j(  j)  j*  Nj,  j-  j.  Nj/  �j0  �j  }ro  u}rp  (hX   Alleyway1_1rq  j"  j;  hKK�rr  j(  KK�rs  j*  j>  j,  j-  j.  Nj/  �j0  �j  }rt  u}ru  (hX   Alleyway1_2rv  j"  j;  hK
K�rw  j(  KK�rx  j*  j>  j,  j-  j.  Nj/  �j0  �j  }ry  u}rz  (hX   Alleyway1_3r{  j"  j;  hKK�r|  j(  KK�r}  j*  j>  j,  j-  j.  Nj/  �j0  �j  }r~  u}r  (hX   Alleyway1_4r�  j"  j;  hKK	�r�  j(  KK�r�  j*  j>  j,  j-  j.  Nj/  �j0  �j  }r�  u}r�  (hX   Alleyway1_5r�  j"  j;  hKK	�r�  j(  KK�r�  j*  j>  j,  j-  j.  Nj/  �j0  �j  }r�  u}r�  (hX   Alleyway1_6r�  j"  j;  hK
K	�r�  j(  KK�r�  j*  j>  j,  j-  j.  Nj/  �j0  �j  }r�  u}r�  (hX   Alleyway1_7r�  j"  j;  hKK	�r�  j(  KK�r�  j*  j>  j,  j-  j.  Nj/  �j0  �j  }r�  ueh}r�  (hX   1r�  hX   Ch. 2: Skirmish In The Shadowsr�  X   tilemapr�  }r�  (hX   Ch2r�  X   layersr�  ]r�  (}r�  (hX   baser�  X   visibler�  �u}r�  (hX   Boxesr�  j�  �u}r�  (hX   TopLeftr�  j�  �u}r�  (hX   TopRightr�  j�  �u}r�  (hX   Leftr�  j�  �u}r�  (hX   Midr�  j�  �u}r�  (hX   Rightr�  j�  �u}r�  (hX   Box1r�  j�  �u}r�  (hX   Box3r�  j�  �u}r�  (hX   Box5r�  j�  �ueX   weatherr�  ]r�  X   nightr�  N�r�  aX
   animationsr�  ]r�  uX
   bg_tilemapr�  NhhX   musicr�  }r�  (X   player_phaser�  X   Skirmish In The Shadowsr�  X   enemy_phaser�  X   Skirmish In The Shadowsr�  X   other_phaser�  X   Skirmish In The Shadowsr�  X   enemy2_phaser�  X   Skirmish In The Shadowsr�  X   player_battler�  NX   enemy_battler�  NX   other_battler�  NX   enemy2_battler�  NX   boss_battler�  NuX	   objectiver�  }r�  (X   simpler�  X   Rout Fodderr�  X   winr�  X   Defeat All,(Boss Optional)r�  X   lossr�  X   Two Player Units Dier�  uh]r�  (hhnh�h�h�h�j  jZ  j%  j@  h�j'  jW  jm  j�  j�  j�  j�  j�  j�  j�  j
  j  j.  jB  jT  jf  j  j�  jy  j�  j�  j�  j�  j�  j�  j  j  j)  j=  jQ  jd  jx  j�  j�  j�  j�  j�  j�  ej  ]r�  (j!  j3  j8  jA  jF  jK  jQ  jV  j]  ja  je  ji  jm  eX   unit_groupsr�  ]r�  (}r�  (hX   Enemy1r�  h]r�  (X   Enemy_1r�  X   Enemy_2r�  X   Enemy_3r�  eX	   positionsr�  }r�  (X   Enemy_1r�  ]r�  (K KeX   Enemy_2r�  ]r�  (K KeX   Enemy_3r�  ]r�  (K Keuu}r�  (hX   Enemy1Placementr�  h]r�  (X   Enemy_1r�  X   Enemy_2r�  X   Enemy_3r�  ej�  }r�  (j�  ]r�  (KKej�  ]r�  (KKej�  ]r�  (KKeuu}r�  (hX
   Stationaryr�  h]r�  (X
   Brassfieldr�  X   Enemy_4r�  X   Enemy_5r�  X   Enemy_6r�  X   Enemy_7r�  X   Enemy_8r�  X   Enemy_9r�  X   Enemy_10r�  X   Enemy_11r�  X   Enemy_12r�  ej�  }r�  (X
   Brassfieldr�  ]r�  (KKeX   Enemy_4r�  ]r�  (KKeX   Enemy_5r�  ]r�  (KKeX   Enemy_6r�  ]r�  (KKeX   Enemy_7r   ]r  (KK
eX   Enemy_8r  ]r  (KK
eX   Enemy_9r  ]r  (KKeX   Enemy_10r  ]r  (KK
eX   Enemy_11r  ]r	  (KKeX   Enemy_12r
  ]r  (KKeuu}r  (hX   Rein1r  h]r  (X   Enemy_13r  X   Enemy_14r  ej�  }r  (X   Enemy_13r  ]r  (K KeX   Enemy_14r  ]r  (KKeuu}r  (hX   Rein2r  h]r  (X   Enemy_15r  X   Enemy_16r  ej�  }r  (X   Enemy_15r  ]r  (K KeX   Enemy_16r  ]r  (K Keuu}r   (hX   Rein3r!  h]r"  (X   Enemy_17r#  X   Enemy_18r$  ej�  }r%  (X   Enemy_17r&  ]r'  (KKeX   Enemy_18r(  ]r)  (KKeuu}r*  (hX   Rein4r+  h]r,  (X   Enemy_19r-  X   Enemy_20r.  ej�  }r/  (X   Enemy_19r0  ]r1  (KK	eX   Enemy_20r2  ]r3  (KK
euueX	   ai_groupsr4  ]r5  uX
   overworldsr6  ]r7  }r8  (j�  NX   enabled_nodesr9  ]r:  X   enabled_roadsr;  ]r<  hX   0r=  X   overworld_entitiesr>  ]r?  }r@  (hhX   dtyperA  X   PARTYrB  X   dnidrC  hX   on_node_nidrD  NhNh	X   playerrE  uaX   selected_party_nidrF  NX   node_propertiesrG  }rH  X   enabled_menu_optionsrI  }rJ  (j=  }rK  j�  }rL  j>  }rM  jN  }rN  X   4rO  }rP  X   5rQ  }rR  X   6rS  }rT  X   7rU  }rV  X   8rW  }rX  X   9rY  }rZ  X   10r[  }r\  X   11r]  }r^  X   12r_  }r`  X   13ra  }rb  X   14rc  }rd  X   15re  }rf  X   16rg  }rh  X   17ri  }rj  X   18rk  }rl  X   19rm  }rn  X   20ro  }rp  X   21rq  }rr  X   22rs  }rt  X   23ru  }rv  X   24rw  }rx  X   25ry  }rz  uX   visible_menu_optionsr{  }r|  (j=  }r}  j�  }r~  j>  }r  jN  }r�  jO  }r�  jQ  }r�  jS  }r�  jU  }r�  jW  }r�  jY  }r�  j[  }r�  j]  }r�  j_  }r�  ja  }r�  jc  }r�  je  }r�  jg  }r�  ji  }r�  jk  }r�  jm  }r�  jo  }r�  jq  }r�  js  }r�  ju  }r�  jw  }r�  jy  }r�  uuaX	   turncountr�  KX   playtimer�  GA _$    X	   game_varsr�  ccollections
Counter
r�  }r�  (X   _random_seedr�  K�X   _next_level_nidr�  j�  X   AckermanDeadr�  KX   _prep_musicr�  X    Before An Impossible Battlefieldr�  X   _prep_options_enabledr�  ]r�  X   _prep_options_eventsr�  ]r�  X   _prep_additional_optionsr�  ]r�  X   _current_turnwheel_usesr�  J����X   _should_go_to_overworldr�  �X   LeodDeadr�  KX   BrassfieldDeadr�  K u�r�  Rr�  X
   level_varsr�  j�  }r�  (X   DeadPlayersr�  K X   QuinleySkillr�  K X   SaraidSkillr�  K X   RoutEnemiesr�  K X   _prev_pos_Ch2r�  }r�  (jW  jd  jm  jx  j�  j�  j�  j�  j�  j�  uX   _prev_region_Ch2r�  }r�  (X   House1r�  KK�r�  X   House2r�  KK�r�  X   VisionTopLeftr�  KK�r�  X   VisionTopRightr�  KK�r�  X   VisionBotLeftr�  KK�r�  X   VisionBotMidr�  KK�r�  X   VisionBotRightr�  KK�r�  X   Form1r�  KK�r�  X   Form2r�  KK�r�  X   Form3r�  KK �r�  X   Form4r�  KK�r�  X   Form5r�  KK �r�  X   Form6r�  KK�r�  X   Alleyway1_1r�  KK�r�  X   Alleyway1_2r�  K
K�r�  X   Alleyway1_3r�  KK�r�  X   Alleyway1_4r�  KK	�r�  X   Alleyway1_5r�  KK	�r�  X   Alleyway1_6r�  K
K	�r�  X   Alleyway1_7r�  KK	�r�  uX   _prev_pos_Ch4r�  }r�  X   _prev_region_Ch4r�  }r�  X   _fog_of_war_typer�  KX   _fog_of_war_radiusr�  KX   _fog_of_warr�  �X   _prev_pos_Alleywayr�  }r�  X   _prev_region_Alleywayr�  }r�  (jq  jr  jv  jw  j{  j|  j�  j�  j�  j�  j�  j�  j�  j�  uX
   _prep_pickr�  �u�r�  Rr�  X   current_moder�  }r�  (hX   Normalr�  X
   permadeathr�  �h/X   Randomr�  X   enemy_autolevelsr�  K X   enemy_truelevelsr�  K X   boss_autolevelsr�  K X   boss_truelevelsr�  K uX   partiesr�  ]r�  }r�  (hhhX   Eirika's Groupr�  X
   leader_nidr�  X   Alpinr�  X   party_prep_manage_sort_orderr�  ]r�  (hh�h�h�j  h�hneX   moneyr�  K X   convoyr�  ]r   X   bexpr  K uaX   current_partyr  hX   stater  ]r  (X   freer  X   status_upkeepr  X   phase_changer  e]r  �r	  X
   action_logr
  ]r  (X   AddSkillr  }r  (X   unitr  j  j�  �r  X	   initiatorr  hN�r  X	   skill_objr  X   skillr  M�r  X   sourcer  hM�	�r  X   source_typer  hhK�r  X
   subactionsr  X   listr  ]r  �r  X   reset_actionr  X   actionr  X   ResetUnitVarsr  }r   (j  j  j�  �r!  X   old_current_hpr"  hK�r#  X   old_current_manar$  hK �r%  u�r&  �r'  X   did_somethingr(  h��r)  u�r*  j  }r+  (j  j  j�  �r,  j  hN�r-  j  j  M��r.  j  hM�	�r/  j  hhK�r0  j  j  ]r1  �r2  j  j  j  }r3  (j  j  j�  �r4  j"  hK
�r5  j$  hK �r6  u�r7  �r8  j(  h��r9  u�r:  j  }r;  (j  j  j�  �r<  j  hN�r=  j  j  M��r>  j  hM�	�r?  j  hhK�r@  j  j  ]rA  �rB  j  j  j  }rC  (j  j  j�  �rD  j"  hK�rE  j$  hK �rF  u�rG  �rH  j(  h��rI  u�rJ  j  }rK  (j  j  j  �rL  j  hN�rM  j  j  M��rN  j  hM�	�rO  j  hhK�rP  j  j  ]rQ  �rR  j  j  j  }rS  (j  j  j  �rT  j"  hK
�rU  j$  hK �rV  u�rW  �rX  j(  h��rY  u�rZ  j  }r[  (j  j  j)  �r\  j  hN�r]  j  j  M��r^  j  hM�	�r_  j  hhK�r`  j  j  ]ra  �rb  j  j  j  }rc  (j  j  j)  �rd  j"  hK�re  j$  hK �rf  u�rg  �rh  j(  h��ri  u�rj  j  }rk  (j  j  jd  �rl  j  hN�rm  j  j  M��rn  j  hM�	�ro  j  hhK�rp  j  j  ]rq  �rr  j  j  j  }rs  (j  j  jd  �rt  j"  hK�ru  j$  hK �rv  u�rw  �rx  j(  h��ry  u�rz  j  }r{  (j  j  j�  �r|  j  hN�r}  j  j  M��r~  j  hM�	�r  j  hhK�r�  j  j  ]r�  �r�  j  j  j  }r�  (j  j  j�  �r�  j"  hK
�r�  j$  hK �r�  u�r�  �r�  j(  h��r�  u�r�  j  }r�  (j  j  j�  �r�  j  hN�r�  j  j  M��r�  j  hM�	�r�  j  hhK�r�  j  j  ]r�  �r�  j  j  j  }r�  (j  j  j�  �r�  j"  hK�r�  j$  hK �r�  u�r�  �r�  j(  h��r�  u�r�  j  }r�  (j  j  j�  �r�  j  hN�r�  j  j  M��r�  j  hM�	�r�  j  hhK�r�  j  j  ]r�  �r�  j  j  j  }r�  (j  j  j�  �r�  j"  hK	�r�  j$  hK �r�  u�r�  �r�  j(  h��r�  u�r�  j  }r�  (j  j  j�  �r�  j  hN�r�  j  j  M��r�  j  hM�	�r�  j  hhK�r�  j  j  ]r�  �r�  j  j  j  }r�  (j  j  j�  �r�  j"  hK	�r�  j$  hK �r�  u�r�  �r�  j(  h��r�  u�r�  j  }r�  (j  j  j�  �r�  j  hN�r�  j  j  M��r�  j  hM�	�r�  j  hhK�r�  j  j  ]r�  �r�  j  j  j  }r�  (j  j  j�  �r�  j"  hK�r�  j$  hK �r�  u�r�  �r�  j(  h��r�  u�r�  X   IncrementTurnr�  }r�  �r�  X   UpdateRecordsr�  }r�  (X   record_typer�  hX   turnr�  �r�  j  hN�r�  u�r�  X   ChangeFatiguer�  }r�  (j  j  h�r�  X   numr�  hK �r�  X   old_fatiguer�  hK �r�  j  j  ]r�  �r�  u�r�  j�  }r�  (j  j  hn�r�  j�  hK �r�  j�  hK �r�  j  j  ]r�  �r�  u�r�  j�  }r�  (j  j  h��r�  j�  hK �r�  j�  hK �r�  j  j  ]r�  �r�  u�r�  j�  }r�  (j  j  h��r�  j�  hK �r�  j�  hK �r�  j  j  ]r�  �r�  u�r�  j�  }r�  (j  j  h��r�  j�  hK �r�  j�  hK �r�  j  j  ]r�  �r�  u�r�  j  }r�  (j  j  jZ  �r�  j  hN�r�  j  j  M��r�  j  hN�r�  j  hh\�r   j  j  ]r  �r  j  j  j  }r  (j  j  jZ  �r  j"  hK�r  j$  hK �r  u�r  �r  j(  h��r	  u�r
  j  }r  (j  j  j@  �r  j  hN�r  j  j  M��r  j  hN�r  j  hh\�r  j  j  ]r  �r  j  j  j  }r  (j  j  j@  �r  j"  hK�r  j$  hK �r  u�r  �r  j(  h��r  u�r  j  }r  (j  j  j%  �r  j  hN�r  j  j  M��r  j  hN�r  j  hh\�r   j  j  ]r!  �r"  j  j  j  }r#  (j  j  j%  �r$  j"  hK�r%  j$  hK �r&  u�r'  �r(  j(  h��r)  u�r*  X
   SetGameVarr+  }r,  (hhj�  �r-  X   valr.  hK �r/  X   old_valr0  hK �r1  u�r2  j+  }r3  (hhj�  �r4  j.  hK �r5  j0  hK �r6  u�r7  X   SetLevelVarr8  }r9  (hhj�  �r:  j.  hK �r;  j0  hK �r<  u�r=  j8  }r>  (hhj�  �r?  j.  hK �r@  j0  hK �rA  u�rB  j8  }rC  (hhj�  �rD  j.  hK �rE  j0  hK �rF  u�rG  j8  }rH  (hhj�  �rI  j.  hK�rJ  j0  hK �rK  u�rL  X
   AddWeatherrM  }rN  (X   weather_nidrO  hX   sunsetrP  �rQ  hhN�rR  u�rS  X   ArriveOnMaprT  }rU  (j  j  j.  �rV  X   place_on_maprW  j  X
   PlaceOnMaprX  }rY  (j  j  j.  �rZ  X   posr[  hKK�r\  �r]  X   update_fow_actionr^  j  X   UpdateFogOfWarr_  }r`  (j  j  j.  �ra  X   prev_posrb  hN�rc  u�rd  �re  u�rf  �rg  u�rh  jT  }ri  (j  j  jB  �rj  jW  j  jX  }rk  (j  j  jB  �rl  j[  hKK�rm  �rn  j^  j  j_  }ro  (j  j  jB  �rp  jb  hN�rq  u�rr  �rs  u�rt  �ru  u�rv  jT  }rw  (j  j  jT  �rx  jW  j  jX  }ry  (j  j  jT  �rz  j[  hKK�r{  �r|  j^  j  j_  }r}  (j  j  jT  �r~  jb  hN�r  u�r�  �r�  u�r�  �r�  u�r�  jT  }r�  (j  j  j�  �r�  jW  j  jX  }r�  (j  j  j�  �r�  j[  hKK�r�  �r�  j^  j  j_  }r�  (j  j  j�  �r�  jb  hN�r�  u�r�  �r�  u�r�  �r�  u�r�  jT  }r�  (j  j  j�  �r�  jW  j  jX  }r�  (j  j  j�  �r�  j[  hKK�r�  �r�  j^  j  j_  }r�  (j  j  j�  �r�  jb  hN�r�  u�r�  �r�  u�r�  �r�  u�r�  jT  }r�  (j  j  jZ  �r�  jW  j  jX  }r�  (j  j  jZ  �r�  j[  hKK�r�  �r�  j^  j  j_  }r�  (j  j  jZ  �r�  jb  hN�r�  u�r�  �r�  u�r�  �r�  u�r�  jT  }r�  (j  j  j%  �r�  jW  j  jX  }r�  (j  j  j%  �r�  j[  hKK�r�  �r�  j^  j  j_  }r�  (j  j  j%  �r�  jb  hN�r�  u�r�  �r�  u�r�  �r�  u�r�  X   ForcedMovementr�  }r�  (j  j  jZ  �r�  X   old_posr�  hj�  �r�  X   new_posr�  hKK�r�  �r�  j^  j  j_  }r�  (j  j  jZ  �r�  jb  hj�  �r�  u�r�  �r�  u�r�  jT  }r�  (j  j  j@  �r�  jW  j  jX  }r�  (j  j  j@  �r�  j[  hKK�r�  �r�  j^  j  j_  }r�  (j  j  j@  �r�  jb  hN�r�  u�r�  �r�  u�r�  �r�  u�r�  X   Mover�  }r�  (j  j  jZ  �r�  j�  hj�  �r�  j�  hKK�r�  �r�  X   prev_movement_leftr�  hK�r�  X   new_movement_leftr�  hN�r�  X   pathr�  j  ]r�  �r�  X	   has_movedr�  h��r�  j$  h��r�  X   followr�  h��r�  X   speedr�  hK�r�  u�r�  j�  }r�  (j  j  j%  �r�  j�  hj�  �r�  j�  hKK�r�  �r�  j�  hK�r�  j�  hN�r�  j�  j  ]r�  �r�  j�  h��r�  j$  h��r�  j�  h��r�  j�  hK�r�  u�r�  j�  }r�  (j  j  j@  �r�  j�  hj�  �r�  j�  hKK�r�  �r   j�  hK�r  j�  hN�r  j�  j  ]r  �r  j�  h��r  j$  h��r  j�  h��r  j�  hK�r  u�r	  X   Resetr
  }r  (j  j  jZ  �r  X   movement_leftr  hK�r  hgh(��������tr  �r  u�r  j_  }r  (j  j  jZ  �r  jb  hj�  �r  u�r  j
  }r  (j  j  j@  �r  j  hK�r  hgh(��������tr  �r  u�r  j_  }r  (j  j  j@  �r  jb  hj�  �r  u�r  j
  }r   (j  j  j%  �r!  j  hK�r"  hgh(��������tr#  �r$  u�r%  j_  }r&  (j  j  j%  �r'  jb  hj�  �r(  u�r)  X   Teleportr*  }r+  (j  j  j�  �r,  j�  hj�  �r-  j�  hKK�r.  �r/  j^  j  j_  }r0  (j  j  j�  �r1  jb  hj�  �r2  u�r3  �r4  u�r5  j*  }r6  (j  j  j�  �r7  j�  hj�  �r8  j�  hKK�r9  �r:  j^  j  j_  }r;  (j  j  j�  �r<  jb  hj�  �r=  u�r>  �r?  u�r@  X   HasAttackedrA  }rB  (j  j  j�  �rC  j  hK�rD  hgh(��������trE  �rF  u�rG  X   MessagerH  }rI  X   messagerJ  hX   Soldier attacked LamonterK  �rL  s�rM  j�  }rN  (j�  hX   missrO  �rP  j  hj�  j%  �rQ  �rR  u�rS  X   RecordRandomStaterT  }rU  (X   oldrV  hJ#�EQ�rW  X   newrX  hJ#�EQ�rY  u�rZ  j*  }r[  (j  j  jZ  �r\  j�  hKK�r]  �r^  j�  hKK�r_  �r`  j^  j  j_  }ra  (j  j  jZ  �rb  jb  hj]  �rc  u�rd  �re  u�rf  j*  }rg  (j  j  j@  �rh  j�  hKK�ri  �rj  j�  hKK�rk  �rl  j^  j  j_  }rm  (j  j  j@  �rn  jb  hji  �ro  u�rp  �rq  u�rr  j*  }rs  (j  j  jZ  �rt  j�  hj_  �ru  j�  hKK�rv  �rw  j^  j  j_  }rx  (j  j  jZ  �ry  jb  hj_  �rz  u�r{  �r|  u�r}  X   ChangeHPr~  }r  (j  j  j�  �r�  j�  hJ�����r�  X   old_hpr�  hK�r�  u�r�  X
   SetObjDatar�  }r�  (X   objr�  hHM�	�r�  X   keywordr�  hj3  �r�  X   valuer�  hK�r�  X	   old_valuer�  hK�r�  u�r�  j�  }r�  (j�  hX   item_user�  �r�  j  hj@  X	   Steel Bowr�  �r�  �r�  u�r�  jA  }r�  (j  j  j@  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  jH  }r�  jJ  hX   Garvey attacked Soldierr�  �r�  s�r�  jH  }r�  jJ  hX   Prevailed over Soldierr�  �r�  s�r�  X   GainWexpr�  }r�  (j  j  j@  �r�  hHhHM�	�r�  X	   wexp_gainr�  hG?�      �r�  j�  hK�r�  X   current_valuer�  hG@6      �r�  u�r�  j�  }r�  (j�  hX   critr�  �r�  j  hj@  j�  �r�  �r�  u�r�  j�  }r�  (j�  hX   damager�  �r�  j  h(j@  j�  j�  KKj�  tr�  �r�  u�r�  j�  }r�  (j�  hX   killr�  �r�  j  hj@  j�  �r�  �r�  u�r�  jT  }r�  (jV  hJ#�EQ�r�  jX  hJ#�EQ�r�  u�r�  X   Dier�  }r�  (j  j  j�  �r�  j�  hj.  �r�  X	   leave_mapr�  j  X   LeaveMapr�  }r�  (j  j  j�  �r�  X   remove_from_mapr�  j  X   RemoveFromMapr�  }r�  (j  j  j�  �r�  j�  hj.  �r�  j^  j  j_  }r�  (j  j  j�  �r�  jb  hj.  �r�  u�r�  �r�  u�r�  �r�  u�r�  �r�  X   lock_all_support_ranksr�  j  ]r�  �r�  X   dropr�  hN�r�  X   initiative_actionr�  hN�r�  u�r�  j*  }r�  (j  j  jZ  �r�  j�  hjv  �r�  j�  hKK�r�  �r�  j^  j  j_  }r�  (j  j  jZ  �r�  jb  hjv  �r�  u�r�  �r�  u�r�  j�  }r�  (j  j  jZ  �r�  j�  j  j�  }r�  (j  j  jZ  �r�  j�  hj�  �r�  j^  j  j_  }r�  (j  j  jZ  �r�  jb  hj�  �r�  u�r�  �r�  u�r�  �r�  u�r�  j*  }r�  (j  j  j%  �r�  j�  hKK�r�  �r�  j�  hKK�r�  �r   j^  j  j_  }r  (j  j  j%  �r  jb  hj�  �r  u�r  �r  u�r  j~  }r  (j  j  j�  �r  j�  hJ�����r	  j�  hK�r
  u�r  j�  }r  (j�  hHMy	�r  j�  hj3  �r  j�  hK�r  j�  hK�r  u�r  j�  }r  (j�  hj�  �r  j  hj%  X   Steel Lancer  �r  �r  u�r  j  }r  (j  j  j�  �r  j  j  j%  �r  j  j  M��r  j  hN�r  j  hh\�r  j  j  ]r  �r  j  j  j  }r   (j  j  j�  �r!  j"  hK�r"  j$  hK �r#  u�r$  �r%  j(  h��r&  u�r'  X   TriggerCharger(  }r)  (j  j  j%  �r*  j  j  M��r+  X
   old_charger,  hN�r-  X
   new_charger.  hN�r/  u�r0  jA  }r1  (j  j  j%  �r2  j  hK�r3  hgh(��������tr4  �r5  u�r6  jH  }r7  jJ  hX   Lamonte attacked Soldierr8  �r9  s�r:  jH  }r;  jJ  hX   Prevailed over Soldierr<  �r=  s�r>  j�  }r?  (j  j  j%  �r@  hHhHMy	�rA  j�  hG?�      �rB  j�  hK�rC  j�  hG@6      �rD  u�rE  j�  }rF  (j�  hj�  �rG  j  hj%  j�  �rH  �rI  u�rJ  j�  }rK  (j�  hj�  �rL  j  h(j%  j�  j  KKj�  trM  �rN  u�rO  j�  }rP  (j�  hj�  �rQ  j  hj%  j�  �rR  �rS  u�rT  jT  }rU  (jV  hJ#�EQ�rV  jX  hJ#�EQ�rW  u�rX  j�  }rY  (j  j  j�  �rZ  j�  hj9  �r[  j�  j  j�  }r\  (j  j  j�  �r]  j�  j  j�  }r^  (j  j  j�  �r_  j�  hj9  �r`  j^  j  j_  }ra  (j  j  j�  �rb  jb  hj9  �rc  u�rd  �re  u�rf  �rg  u�rh  �ri  j�  j  ]rj  �rk  j�  hN�rl  j�  hN�rm  u�rn  j*  }ro  (j  j  j%  �rp  j�  hj�  �rq  j�  hKK�rr  �rs  j^  j  j_  }rt  (j  j  j%  �ru  jb  hj�  �rv  u�rw  �rx  u�ry  j*  }rz  (j  j  j@  �r{  j�  hjk  �r|  j�  hKK�r}  �r~  j^  j  j_  }r  (j  j  j@  �r�  jb  hjk  �r�  u�r�  �r�  u�r�  j�  }r�  (j  j  j@  �r�  j�  j  j�  }r�  (j  j  j@  �r�  j�  hj}  �r�  j^  j  j_  }r�  (j  j  j@  �r�  jb  hj}  �r�  u�r�  �r�  u�r�  �r�  u�r�  j�  }r�  (j  j  j%  �r�  j�  j  j�  }r�  (j  j  j%  �r�  j�  hjr  �r�  j^  j  j_  }r�  (j  j  j%  �r�  jb  hjr  �r�  u�r�  �r�  u�r�  �r�  u�r�  j�  }r�  (j  j  j.  �r�  j�  j  j�  }r�  (j  j  j.  �r�  j�  hj\  �r�  j^  j  j_  }r�  (j  j  j.  �r�  jb  hj\  �r�  u�r�  �r�  u�r�  �r�  u�r�  j�  }r�  (j  j  jB  �r�  j�  j  j�  }r�  (j  j  jB  �r�  j�  hjm  �r�  j^  j  j_  }r�  (j  j  jB  �r�  jb  hjm  �r�  u�r�  �r�  u�r�  �r�  u�r�  j�  }r�  (j  j  jT  �r�  j�  j  j�  }r�  (j  j  jT  �r�  j�  hj{  �r�  j^  j  j_  }r�  (j  j  jT  �r�  jb  hj{  �r�  u�r�  �r�  u�r�  �r�  u�r�  jM  }r�  (jO  hX   nightr�  �r�  hhN�r�  u�r�  j8  }r�  (hhj�  �r�  j.  hK�r�  j0  hK �r�  u�r�  j8  }r�  (hhj�  �r�  j.  hK�r�  j0  hK �r�  u�r�  j8  }r�  (hhj�  �r�  j.  h��r�  j0  hK �r�  u�r�  X	   AddRegionr�  }r�  (X   regionr�  j�  jq  �r�  X   did_addr�  h��r�  j  j  ]r�  j  X   AddVisionRegionr�  }r�  j�  j�  jq  �r�  s�r�  �r�  a�r�  u�r�  j�  }r�  (j�  j�  jv  �r�  j�  h��r�  j  j  ]r�  j  j�  }r�  j�  j�  jv  �r�  s�r�  �r�  a�r�  u�r�  j�  }r�  (j�  j�  j{  �r�  j�  h��r�  j  j  ]r�  j  j�  }r�  j�  j�  j{  �r�  s�r�  �r�  a�r�  u�r�  j�  }r�  (j�  j�  j�  �r�  j�  h��r�  j  j  ]r�  j  j�  }r   j�  j�  j�  �r  s�r  �r  a�r  u�r  j�  }r  (j�  j�  j�  �r  j�  h��r  j  j  ]r	  j  j�  }r
  j�  j�  j�  �r  s�r  �r  a�r  u�r  j�  }r  (j�  j�  j�  �r  j�  h��r  j  j  ]r  j  j�  }r  j�  j�  j�  �r  s�r  �r  a�r  u�r  j�  }r  (j�  j�  j�  �r  j�  h��r  j  j  ]r  j  j�  }r  j�  j�  j�  �r  s�r   �r!  a�r"  u�r#  jT  }r$  (j  j  jZ  �r%  jW  j  jX  }r&  (j  j  jZ  �r'  j[  hKK	�r(  �r)  j^  j  j_  }r*  (j  j  jZ  �r+  jb  hN�r,  u�r-  �r.  u�r/  �r0  u�r1  j*  }r2  (j  j  jZ  �r3  j�  hj(  �r4  j�  hK K�r5  �r6  j^  j  j_  }r7  (j  j  jZ  �r8  jb  hj(  �r9  u�r:  �r;  u�r<  jT  }r=  (j  j  j@  �r>  jW  j  jX  }r?  (j  j  j@  �r@  j[  hKK	�rA  �rB  j^  j  j_  }rC  (j  j  j@  �rD  jb  hN�rE  u�rF  �rG  u�rH  �rI  u�rJ  j*  }rK  (j  j  j@  �rL  j�  hjA  �rM  j�  hKK�rN  �rO  j^  j  j_  }rP  (j  j  j@  �rQ  jb  hjA  �rR  u�rS  �rT  u�rU  jT  }rV  (j  j  j%  �rW  jW  j  jX  }rX  (j  j  j%  �rY  j[  hKK	�rZ  �r[  j^  j  j_  }r\  (j  j  j%  �r]  jb  hN�r^  u�r_  �r`  u�ra  �rb  u�rc  X	   ShowLayerrd  }re  (X	   layer_nidrf  hX   Cratesrg  �rh  X
   transitionri  hX   faderj  �rk  u�rl  j�  }rm  (j  j  jZ  �rn  j�  j  j�  }ro  (j  j  jZ  �rp  j�  hj5  �rq  j^  j  j_  }rr  (j  j  jZ  �rs  jb  hj5  �rt  u�ru  �rv  u�rw  �rx  u�ry  j*  }rz  (j  j  j@  �r{  j�  hjN  �r|  j�  hKK	�r}  �r~  j^  j  j_  }r  (j  j  j@  �r�  jb  hjN  �r�  u�r�  �r�  u�r�  j�  }r�  (j  j  j%  �r�  j�  j  j�  }r�  (j  j  j%  �r�  j�  hjZ  �r�  j^  j  j_  }r�  (j  j  j%  �r�  jb  hjZ  �r�  u�r�  �r�  u�r�  �r�  u�r�  j�  }r�  (j  j  j@  �r�  j�  j  j�  }r�  (j  j  j@  �r�  j�  hj}  �r�  j^  j  j_  }r�  (j  j  j@  �r�  jb  hj}  �r�  u�r�  �r�  u�r�  �r�  u�r�  jM  }r�  (jO  hj�  �r�  hhN�r�  u�r�  j8  }r�  (hhj�  �r�  j.  hK�r�  j0  hK�r�  u�r�  j8  }r�  (hhj�  �r�  j.  hK�r�  j0  hK�r�  u�r�  j8  }r�  (hhj�  �r�  j.  h��r�  j0  h��r�  u�r�  j�  }r�  (j�  j�  j!  �r�  j�  h��r�  j  j  ]r�  �r�  u�r�  j�  }r�  (j�  j�  j3  �r�  j�  h��r�  j  j  ]r�  �r�  u�r�  j�  }r�  (j�  j�  j8  �r�  j�  h��r�  j  j  ]r�  j  j�  }r�  j�  j�  j8  �r�  s�r�  �r�  a�r�  u�r�  j�  }r�  (j�  j�  jA  �r�  j�  h��r�  j  j  ]r�  j  j�  }r�  j�  j�  jA  �r�  s�r�  �r�  a�r�  u�r�  j�  }r�  (j�  j�  jF  �r�  j�  h��r�  j  j  ]r�  j  j�  }r�  j�  j�  jF  �r�  s�r�  �r�  a�r�  u�r�  j�  }r�  (j�  j�  jK  �r�  j�  h��r�  j  j  ]r�  j  j�  }r�  j�  j�  jK  �r�  s�r�  �r�  a�r�  u�r�  j�  }r�  (j�  j�  jQ  �r�  j�  h��r�  j  j  ]r�  j  j�  }r�  j�  j�  jQ  �r�  s�r�  �r�  a�r�  u�r�  j�  }r�  (j�  j�  jV  �r�  j�  h��r�  j  j  ]r�  �r�  u�r�  j�  }r�  (j�  j�  j]  �r�  j�  h��r�  j  j  ]r�  �r�  u�r�  j�  }r�  (j�  j�  ja  �r�  j�  h��r�  j  j  ]r�  �r   u�r  j�  }r  (j�  j�  je  �r  j�  h��r  j  j  ]r  �r  u�r  j�  }r  (j�  j�  ji  �r	  j�  h��r
  j  j  ]r  �r  u�r  j�  }r  (j�  j�  jm  �r  j�  h��r  j  j  ]r  �r  u�r  jd  }r  (jf  hX   Boxesr  �r  ji  hjj  �r  u�r  jT  }r  (j  j  jZ  �r  jW  j  jX  }r  (j  j  jZ  �r  j[  hKK
�r  �r  j^  j  j_  }r  (j  j  jZ  �r   jb  hN�r!  u�r"  �r#  u�r$  �r%  u�r&  j*  }r'  (j  j  jZ  �r(  j�  hj  �r)  j�  hj[  �r*  j^  j  j_  }r+  (j  j  jZ  �r,  jb  hj  �r-  u�r.  �r/  u�r0  X	   HideLayerr1  }r2  (jf  hX   Boxesr3  �r4  ji  hjj  �r5  u�r6  jT  }r7  (j  j  jW  �r8  jW  j  jX  }r9  (j  j  jW  �r:  j[  hjX  �r;  j^  j  j_  }r<  (j  j  jW  �r=  jb  hN�r>  u�r?  �r@  u�rA  �rB  u�rC  jT  }rD  (j  j  j�  �rE  jW  j  jX  }rF  (j  j  j�  �rG  j[  hj�  �rH  j^  j  j_  }rI  (j  j  j�  �rJ  jb  hN�rK  u�rL  �rM  u�rN  �rO  u�rP  jT  }rQ  (j  j  j�  �rR  jW  j  jX  }rS  (j  j  j�  �rT  j[  hj�  �rU  j^  j  j_  }rV  (j  j  j�  �rW  jb  hN�rX  u�rY  �rZ  u�r[  �r\  u�r]  jT  }r^  (j  j  h�r_  jW  j  jX  }r`  (j  j  h�ra  j[  hKK �rb  �rc  j^  j  j_  }rd  (j  j  h�re  jb  hN�rf  u�rg  �rh  u�ri  �rj  u�rk  jT  }rl  (j  j  hn�rm  jW  j  jX  }rn  (j  j  hn�ro  j[  hKK �rp  �rq  j^  j  j_  }rr  (j  j  hn�rs  jb  hN�rt  u�ru  �rv  u�rw  �rx  u�ry  jT  }rz  (j  j  h��r{  jW  j  jX  }r|  (j  j  h��r}  j[  hKK �r~  �r  j^  j  j_  }r�  (j  j  h��r�  jb  hN�r�  u�r�  �r�  u�r�  �r�  u�r�  jT  }r�  (j  j  h��r�  jW  j  jX  }r�  (j  j  h��r�  j[  hKK �r�  �r�  j^  j  j_  }r�  (j  j  h��r�  jb  hN�r�  u�r�  �r�  u�r�  �r�  u�r�  j�  }r�  (j  j  h��r�  j�  hj�  �r�  j�  hKK�r�  �r�  j^  j  j_  }r�  (j  j  h��r�  jb  hj�  �r�  u�r�  �r�  u�r�  jT  }r�  (j  j  h��r�  jW  j  jX  }r�  (j  j  h��r�  j[  hKK �r�  �r�  j^  j  j_  }r�  (j  j  h��r�  jb  hN�r�  u�r�  �r�  u�r�  �r�  u�r�  j*  }r�  (j  j  h�r�  j�  hjb  �r�  j�  hKK�r�  �r�  j^  j  j_  }r�  (j  j  h�r�  jb  hjb  �r�  u�r�  �r�  u�r�  j*  }r�  (j  j  h��r�  j�  hj�  �r�  j�  hKK�r�  �r�  j^  j  j_  }r�  (j  j  h��r�  jb  hj�  �r�  u�r�  �r�  u�r�  j*  }r�  (j  j  h��r�  j�  hj~  �r�  j�  hKK�r�  �r�  j^  j  j_  }r�  (j  j  h��r�  jb  hj~  �r�  u�r�  �r�  u�r�  jd  }r�  (jf  hX   TopRightr�  �r�  ji  hjj  �r�  u�r�  jT  }r�  (j  j  h�r�  jW  j  jX  }r�  (j  j  h�r�  j[  hKK�r�  �r�  j^  j  j_  }r�  (j  j  h�r�  jb  hN�r�  u�r�  �r�  u�r�  �r�  u�r�  jd  }r�  (jf  hX   TopLeftr�  �r�  ji  hjj  �r�  u�r�  jT  }r�  (j  j  j  �r�  jW  j  jX  }r�  (j  j  j  �r�  j[  hKK�r�  �r�  j^  j  j_  }r�  (j  j  j  �r�  jb  hN�r�  u�r�  �r�  u�r�  �r�  u�r�  j*  }r�  (j  j  h�r�  j�  hj�  �r�  j�  hKK�r�  �r�  j^  j  j_  }r�  (j  j  h�r�  jb  hj�  �r�  u�r�  �r�  u�r   j*  }r  (j  j  j  �r  j�  hj�  �r  j�  hKK�r  �r  j^  j  j_  }r  (j  j  j  �r  jb  hj�  �r  u�r	  �r
  u�r  j1  }r  (jf  hX   TopLeftr  �r  ji  hjj  �r  u�r  j1  }r  (jf  hX   TopRightr  �r  ji  hjj  �r  u�r  j*  }r  (j  j  j  �r  j�  hj  �r  j�  hKK�r  �r  j^  j  j_  }r  (j  j  j  �r  jb  hj  �r  u�r  �r  u�r   j*  }r!  (j  j  h�r"  j�  hj�  �r#  j�  hKK�r$  �r%  j^  j  j_  }r&  (j  j  h�r'  jb  hj�  �r(  u�r)  �r*  u�r+  X
   ChangeTeamr,  }r-  (j  j  h�r.  h	hh�r/  X   old_teamr0  hX   otherr1  �r2  j  j  j
  }r3  (j  j  h�r4  j  hK�r5  hgh(��������tr6  �r7  u�r8  �r9  X	   ai_actionr:  j  X   ChangeAIr;  }r<  (j  j  h�r=  hhh�r>  X   old_air?  hX   Noner@  �rA  u�rB  �rC  X   fog_action1rD  j  j_  }rE  (j  j  h�rF  jb  hj$  �rG  u�rH  �rI  X   fog_action2rJ  j  j_  }rK  (j  j  h�rL  jb  hN�rM  u�rN  �rO  u�rP  j,  }rQ  (j  j  j  �rR  h	hj  �rS  j0  hX   otherrT  �rU  j  j  j
  }rV  (j  j  j  �rW  j  hK�rX  hgh(��������trY  �rZ  u�r[  �r\  j:  j  j;  }r]  (j  j  j  �r^  hhh�r_  j?  hX   Noner`  �ra  u�rb  �rc  jD  j  j_  }rd  (j  j  j  �re  jb  hj  �rf  u�rg  �rh  jJ  j  j_  }ri  (j  j  j  �rj  jb  hN�rk  u�rl  �rm  u�rn  jT  }ro  (j  j  jy  �rp  jW  j  jX  }rq  (j  j  jy  �rr  j[  hK K�rs  �rt  j^  j  j_  }ru  (j  j  jy  �rv  jb  hN�rw  u�rx  �ry  u�rz  �r{  u�r|  j*  }r}  (j  j  jy  �r~  j�  hjs  �r  j�  hK K�r�  �r�  j^  j  j_  }r�  (j  j  jy  �r�  jb  hjs  �r�  u�r�  �r�  u�r�  jT  }r�  (j  j  j�  �r�  jW  j  jX  }r�  (j  j  j�  �r�  j[  hK K�r�  �r�  j^  j  j_  }r�  (j  j  j�  �r�  jb  hN�r�  u�r�  �r�  u�r�  �r�  u�r�  j*  }r�  (j  j  j�  �r�  j�  hj�  �r�  j�  hK K�r�  �r�  j^  j  j_  }r�  (j  j  j�  �r�  jb  hj�  �r�  u�r�  �r�  u�r�  jT  }r�  (j  j  j�  �r�  jW  j  jX  }r�  (j  j  j�  �r�  j[  hK K�r�  �r�  j^  j  j_  }r�  (j  j  j�  �r�  jb  hN�r�  u�r�  �r�  u�r�  �r�  u�r�  j*  }r�  (j  j  j�  �r�  j�  hj�  �r�  j�  hK K�r�  �r�  j^  j  j_  }r�  (j  j  j�  �r�  jb  hj�  �r�  u�r�  �r�  u�r�  j*  }r�  (j  j  jy  �r�  j�  hj�  �r�  j�  hjz  �r�  j^  j  j_  }r�  (j  j  jy  �r�  jb  hj�  �r�  u�r�  �r�  u�r�  j*  }r�  (j  j  j�  �r�  j�  hj�  �r�  j�  hj�  �r�  j^  j  j_  }r�  (j  j  j�  �r�  jb  hj�  �r�  u�r�  �r�  u�r�  j*  }r�  (j  j  j�  �r�  j�  hj�  �r�  j�  hj�  �r�  j^  j  j_  }r�  (j  j  j�  �r�  jb  hj�  �r�  u�r�  �r�  u�r�  jT  }r�  (j  j  j  �r�  jW  j  jX  }r�  (j  j  j  �r�  j[  hj  �r�  j^  j  j_  }r�  (j  j  j  �r�  jb  hN�r�  u�r�  �r�  u�r�  �r�  u�r�  jT  }r�  (j  j  j�  �r�  jW  j  jX  }r�  (j  j  j�  �r�  j[  hj�  �r�  j^  j  j_  }r�  (j  j  j�  �r�  jb  hN�r�  u�r�  �r�  u�r�  �r�  u�r�  jT  }r�  (j  j  j�  �r�  jW  j  jX  }r�  (j  j  j�  �r�  j[  hj�  �r�  j^  j  j_  }r�  (j  j  j�  �r�  jb  hN�r�  u�r�  �r�  u�r�  �r�  u�r�  jT  }r�  (j  j  j�  �r   jW  j  jX  }r  (j  j  j�  �r  j[  hj�  �r  j^  j  j_  }r  (j  j  j�  �r  jb  hN�r  u�r  �r  u�r	  �r
  u�r  jT  }r  (j  j  j�  �r  jW  j  jX  }r  (j  j  j�  �r  j[  hj�  �r  j^  j  j_  }r  (j  j  j�  �r  jb  hN�r  u�r  �r  u�r  �r  u�r  jT  }r  (j  j  j  �r  jW  j  jX  }r  (j  j  j  �r  j[  hj  �r  j^  j  j_  }r  (j  j  j  �r  jb  hN�r   u�r!  �r"  u�r#  �r$  u�r%  jT  }r&  (j  j  j  �r'  jW  j  jX  }r(  (j  j  j  �r)  j[  hj  �r*  j^  j  j_  }r+  (j  j  j  �r,  jb  hN�r-  u�r.  �r/  u�r0  �r1  u�r2  jT  }r3  (j  j  j)  �r4  jW  j  jX  }r5  (j  j  j)  �r6  j[  hj*  �r7  j^  j  j_  }r8  (j  j  j)  �r9  jb  hN�r:  u�r;  �r<  u�r=  �r>  u�r?  jT  }r@  (j  j  j=  �rA  jW  j  jX  }rB  (j  j  j=  �rC  j[  hj>  �rD  j^  j  j_  }rE  (j  j  j=  �rF  jb  hN�rG  u�rH  �rI  u�rJ  �rK  u�rL  jT  }rM  (j  j  jQ  �rN  jW  j  jX  }rO  (j  j  jQ  �rP  j[  hjR  �rQ  j^  j  j_  }rR  (j  j  jQ  �rS  jb  hN�rT  u�rU  �rV  u�rW  �rX  u�rY  j�  }rZ  (j  j  h�r[  j�  j  j�  }r\  (j  j  h�r]  j�  hj$  �r^  j^  j  j_  }r_  (j  j  h�r`  jb  hj$  �ra  u�rb  �rc  u�rd  �re  u�rf  X   RemoveSkillrg  }rh  (j  j  jZ  �ri  j  hX	   NPCVisionrj  �rk  X   removed_skillsrl  j  ]rm  hcapp.engine.objects.skill
SkillObject
rn  )�ro  }rp  (j  M�hX	   NPCVisionrq  hX	   NPCVisionrr  j  NhX   Blind in the dark!rs  X   icon_nidrt  NX
   icon_indexru  ]rv  (K K ej  capp.utilities.data
Data
rw  )�rx  }ry  (X   _listrz  ]r{  (capp.engine.skill_components.base_components
SightRangeBonus
r|  )�r}  }r~  (j�  J����j  jo  ubcapp.engine.skill_components.attribute_components
Hidden
r  )�r�  }r�  j  jo  sbeX   _dictr�  }r�  (X   sight_range_bonusr�  j}  X   hiddenr�  j�  uubX   sight_range_bonusr�  j}  X   hiddenr�  j�  j  j
  j�  Nj�  NX   subskill_uidr�  NX   parent_skillr�  NubNh\�r�  �r�  a�r�  X   countr�  hJ�����r�  j  hN�r�  j  hh\�r�  X   old_owner_nidr�  hN�r�  j  j  j  }r�  (j  j  jZ  �r�  j"  hK�r�  j$  hK �r�  u�r�  �r�  u�r�  jg  }r�  (j  j  j@  �r�  j  hX	   NPCVisionr�  �r�  jl  j  ]r�  hjn  )�r�  }r�  (j  M�hjq  hjr  j  Nhjs  jt  Nju  jv  j  jw  )�r�  }r�  (jz  ]r�  (j|  )�r�  }r�  (j�  J����j  j�  ubj  )�r�  }r�  j  j�  sbej�  }r�  (j�  j�  j�  j�  uubj�  j�  j�  j�  j  j  j�  Nj�  Nj�  Nj�  NubNh\�r�  �r�  a�r�  j�  hJ�����r�  j  hN�r�  j  hh\�r�  j�  hN�r�  j  j  j  }r�  (j  j  j@  �r�  j"  hK�r�  j$  hK �r�  u�r�  �r�  u�r�  jg  }r�  (j  j  j%  �r�  j  hX	   NPCVisionr�  �r�  jl  j  ]r�  hjn  )�r�  }r�  (j  M�hjq  hjr  j  Nhjs  jt  Nju  jv  j  jw  )�r�  }r�  (jz  ]r�  (j|  )�r�  }r�  (j�  J����j  j�  ubj  )�r�  }r�  j  j�  sbej�  }r�  (j�  j�  j�  j�  uubj�  j�  j�  j�  j  j  j�  Nj�  Nj�  Nj�  NubNh\�r�  �r�  a�r�  j�  hJ�����r�  j  hN�r�  j  hh\�r�  j�  hN�r�  j  j  j  }r�  (j  j  j%  �r�  j"  hK�r�  j$  hK �r�  u�r�  �r�  u�r�  j8  }r�  (hhX   RoutEnemiesr�  �r�  j.  hK �r�  j0  hK�r�  u�r�  j+  }r�  (hhX   LeodDeadr�  �r�  j.  hK�r�  j0  hK �r�  u�r�  j8  }r�  (hhj�  �r�  j.  h��r�  j0  hK �r�  u�r�  j+  }r�  (hhj�  �r�  j.  hj�  �r�  j0  hX    Before An Impossible Battlefieldr�  �r�  u�r�  j+  }r�  (hhj�  �r�  j.  j  ]r�  �r�  j0  j  ]r�  �r�  u�r�  j+  }r�  (hhj�  �r�  j.  j  ]r�  �r�  j0  j  ]r�  �r�  u�r�  j+  }r�  (hhj�  �r�  j.  j  ]r�  �r�  j0  j  ]r�  �r�  u�r�  X   ResetAllr   }r  X   actionsr  j  ]r  (j  j
  }r  (j  j  h�r  j  hK�r  hgh(��������tr  �r  u�r	  �r
  j  j
  }r  (j  j  hn�r  j  hK�r  hgh(��������tr  �r  u�r  �r  j  j
  }r  (j  j  h��r  j  hK�r  hgh(��������tr  �r  u�r  �r  j  j
  }r  (j  j  h��r  j  hK�r  hgh(��������tr  �r  u�r  �r  j  j
  }r   (j  j  h��r!  j  hK�r"  hgh(��������tr#  �r$  u�r%  �r&  j  j
  }r'  (j  j  hцr(  j  hK�r)  hgh(��������tr*  �r+  u�r,  �r-  j  j
  }r.  (j  j  h�r/  j  hK�r0  hgh(��������tr1  �r2  u�r3  �r4  j  j
  }r5  (j  j  j  �r6  j  hK�r7  hgh(��������tr8  �r9  u�r:  �r;  j  j
  }r<  (j  j  j%  �r=  j  hK�r>  hgh(��������tr?  �r@  u�rA  �rB  j  j
  }rC  (j  j  j@  �rD  j  hK�rE  hgh(��������trF  �rG  u�rH  �rI  j  j
  }rJ  (j  j  jZ  �rK  j  hK�rL  hgh(��������trM  �rN  u�rO  �rP  j  j
  }rQ  (j  j  jq  �rR  j  hK�rS  hgh(��������trT  �rU  u�rV  �rW  j  j
  }rX  (j  j  j�  �rY  j  hK�rZ  hgh(��������tr[  �r\  u�r]  �r^  j  j
  }r_  (j  j  j�  �r`  j  hK�ra  hgh(��������trb  �rc  u�rd  �re  j  j
  }rf  (j  j  j�  �rg  j  hK�rh  hgh(��������tri  �rj  u�rk  �rl  j  j
  }rm  (j  j  j�  �rn  j  hK�ro  hgh(��������trp  �rq  u�rr  �rs  j  j
  }rt  (j  j  j  �ru  j  hK�rv  hgh(��������trw  �rx  u�ry  �rz  j  j
  }r{  (j  j  j'  �r|  j  hK�r}  hgh(��������tr~  �r  u�r�  �r�  j  j
  }r�  (j  j  j>  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  jW  �r�  j  hK �r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  jm  �r�  j  hK �r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK �r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK �r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK �r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j
  �r�  j  hK �r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j.  �r�  j  hK �r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  jB  �r�  j  hK �r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  jT  �r�  j  hK �r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  jf  �r�  j  hK �r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  jy  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r   (j  j  j�  �r  j  hK�r  hgh(��������tr  �r  u�r  �r  j  j
  }r  (j  j  j�  �r  j  hK�r	  hgh(��������tr
  �r  u�r  �r  j  j
  }r  (j  j  j�  �r  j  hK�r  hgh(��������tr  �r  u�r  �r  j  j
  }r  (j  j  j  �r  j  hK�r  hgh(��������tr  �r  u�r  �r  j  j
  }r  (j  j  j  �r  j  hK�r  hgh(��������tr  �r   u�r!  �r"  j  j
  }r#  (j  j  j)  �r$  j  hK�r%  hgh(��������tr&  �r'  u�r(  �r)  j  j
  }r*  (j  j  j=  �r+  j  hK�r,  hgh(��������tr-  �r.  u�r/  �r0  j  j
  }r1  (j  j  jQ  �r2  j  hK�r3  hgh(��������tr4  �r5  u�r6  �r7  j  j
  }r8  (j  j  jd  �r9  j  hK�r:  hgh(��������tr;  �r<  u�r=  �r>  j  j
  }r?  (j  j  jx  �r@  j  hK�rA  hgh(��������trB  �rC  u�rD  �rE  j  j
  }rF  (j  j  j�  �rG  j  hK�rH  hgh(��������trI  �rJ  u�rK  �rL  j  j
  }rM  (j  j  j�  �rN  j  hK�rO  hgh(��������trP  �rQ  u�rR  �rS  j  j
  }rT  (j  j  j�  �rU  j  hK�rV  hgh(��������trW  �rX  u�rY  �rZ  j  j
  }r[  (j  j  j�  �r\  j  hK�r]  hgh(��������tr^  �r_  u�r`  �ra  j  j
  }rb  (j  j  j�  �rc  j  hK�rd  hgh(��������tre  �rf  u�rg  �rh  j  j
  }ri  (j  j  j�  �rj  j  hK�rk  hgh(��������trl  �rm  u�rn  �ro  e�rp  s�rq  X	   TradeItemrr  }rs  (X   unit1rt  j  hn�ru  X   unit2rv  j  h�rw  X   item1rx  hHM�	�ry  X   item2rz  hh�r{  X   item_index1r|  hK�r}  X   item_index2r~  hK�r  u�r�  jr  }r�  (jt  j  h��r�  jv  j  hn�r�  jx  hHM�	�r�  jz  hh�r�  j|  hK�r�  j~  hK�r�  u�r�  jr  }r�  (jt  j  h��r�  jv  j  h��r�  jx  hHM�	�r�  jz  hh�r�  j|  hK�r�  j~  hK�r�  u�r�  j   }r�  j  j  ]r�  (j  j
  }r�  (j  j  h�r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  hn�r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  h��r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  h��r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  h��r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  hцr�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  h�r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j%  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j@  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  jZ  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  jq  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r   u�r  �r  j  j
  }r  (j  j  j  �r  j  hK�r  hgh(��������tr  �r  u�r  �r	  j  j
  }r
  (j  j  j'  �r  j  hK�r  hgh(��������tr  �r  u�r  �r  j  j
  }r  (j  j  j>  �r  j  hK�r  hgh(��������tr  �r  u�r  �r  j  j
  }r  (j  j  jW  �r  j  hK �r  hgh(��������tr  �r  u�r  �r  j  j
  }r  (j  j  jm  �r   j  hK �r!  hgh(��������tr"  �r#  u�r$  �r%  j  j
  }r&  (j  j  j�  �r'  j  hK �r(  hgh(��������tr)  �r*  u�r+  �r,  j  j
  }r-  (j  j  j�  �r.  j  hK �r/  hgh(��������tr0  �r1  u�r2  �r3  j  j
  }r4  (j  j  j�  �r5  j  hK �r6  hgh(��������tr7  �r8  u�r9  �r:  j  j
  }r;  (j  j  j�  �r<  j  hK�r=  hgh(��������tr>  �r?  u�r@  �rA  j  j
  }rB  (j  j  j�  �rC  j  hK�rD  hgh(��������trE  �rF  u�rG  �rH  j  j
  }rI  (j  j  j
  �rJ  j  hK �rK  hgh(��������trL  �rM  u�rN  �rO  j  j
  }rP  (j  j  j  �rQ  j  hK�rR  hgh(��������trS  �rT  u�rU  �rV  j  j
  }rW  (j  j  j.  �rX  j  hK �rY  hgh(��������trZ  �r[  u�r\  �r]  j  j
  }r^  (j  j  jB  �r_  j  hK �r`  hgh(��������tra  �rb  u�rc  �rd  j  j
  }re  (j  j  jT  �rf  j  hK �rg  hgh(��������trh  �ri  u�rj  �rk  j  j
  }rl  (j  j  jf  �rm  j  hK �rn  hgh(��������tro  �rp  u�rq  �rr  j  j
  }rs  (j  j  jy  �rt  j  hK�ru  hgh(��������trv  �rw  u�rx  �ry  j  j
  }rz  (j  j  j�  �r{  j  hK�r|  hgh(��������tr}  �r~  u�r  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j)  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j=  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  jQ  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  jd  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  jx  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  e�r�  s�r   jr  }r  (jt  j  h��r  jv  j  h�r  jx  hHM�	�r  jz  hHM�	�r  j|  hK�r  j~  hK�r  u�r  X	   HasTradedr	  }r
  (j  j  h�r  j  hK�r  hgh(��������tr  �r  u�r  j�  }r  (j�  hHM�	�r  j�  hX   usesr  �r  j�  hK �r  j�  hK�r  u�r  j�  }r  (j�  hX   item_user  �r  j  hhX   AngelFeatherr  �r  �r  u�r  j  }r  (j  j  h�r  j  j  h�r   j  j  M��r!  j  hN�r"  j  hh\�r#  j  j  ]r$  �r%  j  j  j  }r&  (j  j  h�r'  j"  hK�r(  j$  hK �r)  u�r*  �r+  j(  h��r,  u�r-  X	   LogDialogr.  }r/  (X   speakerr0  hh�r1  X
   plain_textr2  hX   Growths boosted!r3  �r4  u�r5  jA  }r6  (j  j  h�r7  j  hK�r8  hgh(��������tr9  �r:  u�r;  jH  }r<  jJ  hX)   Alpin used <yellow>Angel Feather</yellow>r=  �r>  s�r?  j�  }r@  (j  j  h�rA  hHhHM�	�rB  j�  hG        �rC  j�  hK �rD  j�  hK �rE  u�rF  j�  }rG  (j�  hX   hitrH  �rI  j  hhh�rJ  �rK  u�rL  jT  }rM  (jV  hJ#�EQ�rN  jX  hJ ��y�rO  u�rP  X
   RemoveItemrQ  }rR  (j  j  h�rS  hHhHM�	�rT  X
   item_indexrU  hK�rV  u�rW  jr  }rX  (jt  j  h��rY  jv  j  h�rZ  jx  hHM�	�r[  jz  hh�r\  j|  hK�r]  j~  hK�r^  u�r_  jT  }r`  (j  j  hn�ra  jW  j  jX  }rb  (j  j  hn�rc  j[  hKK �rd  �re  j^  j  j_  }rf  (j  j  hn�rg  jb  hN�rh  u�ri  �rj  u�rk  �rl  u�rm  j
  }rn  (j  j  hn�ro  j  hK�rp  hgh(��������trq  �rr  u�rs  j   }rt  j  j  ]ru  (j  j
  }rv  (j  j  h�rw  j  hK�rx  hgh(��������try  �rz  u�r{  �r|  j  j
  }r}  (j  j  hn�r~  j  hK�r  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  h��r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  h��r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  h��r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  hцr�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  h�r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j%  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j@  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  jZ  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  jq  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j'  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j>  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  jW  �r�  j  hK �r�  hgh(��������tr�  �r�  u�r   �r  j  j
  }r  (j  j  jm  �r  j  hK �r  hgh(��������tr  �r  u�r  �r  j  j
  }r	  (j  j  j�  �r
  j  hK �r  hgh(��������tr  �r  u�r  �r  j  j
  }r  (j  j  j�  �r  j  hK �r  hgh(��������tr  �r  u�r  �r  j  j
  }r  (j  j  j�  �r  j  hK �r  hgh(��������tr  �r  u�r  �r  j  j
  }r  (j  j  j�  �r  j  hK�r   hgh(��������tr!  �r"  u�r#  �r$  j  j
  }r%  (j  j  j�  �r&  j  hK�r'  hgh(��������tr(  �r)  u�r*  �r+  j  j
  }r,  (j  j  j
  �r-  j  hK �r.  hgh(��������tr/  �r0  u�r1  �r2  j  j
  }r3  (j  j  j  �r4  j  hK�r5  hgh(��������tr6  �r7  u�r8  �r9  j  j
  }r:  (j  j  j.  �r;  j  hK �r<  hgh(��������tr=  �r>  u�r?  �r@  j  j
  }rA  (j  j  jB  �rB  j  hK �rC  hgh(��������trD  �rE  u�rF  �rG  j  j
  }rH  (j  j  jT  �rI  j  hK �rJ  hgh(��������trK  �rL  u�rM  �rN  j  j
  }rO  (j  j  jf  �rP  j  hK �rQ  hgh(��������trR  �rS  u�rT  �rU  j  j
  }rV  (j  j  jy  �rW  j  hK�rX  hgh(��������trY  �rZ  u�r[  �r\  j  j
  }r]  (j  j  j�  �r^  j  hK�r_  hgh(��������tr`  �ra  u�rb  �rc  j  j
  }rd  (j  j  j�  �re  j  hK�rf  hgh(��������trg  �rh  u�ri  �rj  j  j
  }rk  (j  j  j�  �rl  j  hK�rm  hgh(��������trn  �ro  u�rp  �rq  j  j
  }rr  (j  j  j�  �rs  j  hK�rt  hgh(��������tru  �rv  u�rw  �rx  j  j
  }ry  (j  j  j�  �rz  j  hK�r{  hgh(��������tr|  �r}  u�r~  �r  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j)  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j=  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  jQ  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  jd  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  jx  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  j  j
  }r�  (j  j  j�  �r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  �r�  e�r�  s�r�  jT  }r�  (j  j  h�r�  jW  j  jX  }r�  (j  j  h�r�  j[  hKK �r�  �r�  j^  j  j_  }r�  (j  j  h�r�  jb  hN�r�  u�r�  �r�  u�r�  �r�  u�r�  j
  }r�  (j  j  h�r�  j  hK�r�  hgh(��������tr�  �r�  u�r�  jg  }r�  (j  j  h��r�  j  j  M��r�  jl  j  ]r�  hjn  )�r�  }r�  (j  M�hj  hX   Forestr�  j  j  hhjt  X   Skillsr�  ju  ]r   (K K ej  jw  )�r  }r  (jz  ]r  (j  )�r  }r  j  j�  sbcapp.engine.skill_components.combat_components
Avoid
r  )�r  }r  (j�  Kj  j�  ubcapp.engine.skill_components.combat_components
StatChange
r	  )�r
  }r  (j�  ]r  ]r  (X   DEFr  Keaj  j�  ubej�  }r  (j�  j  X   avoidr  j  X   stat_changer  j
  uubj�  j  j  j  j  j
  j  j  j�  Nj�  Nj�  Nj�  Nubj�  j  �r  �r  a�r  j�  hJ�����r  j  hj�  �r  j  hj  �r  j�  hN�r  j  j  j  }r  (j  j  h��r  j"  hK�r  j$  hK �r  u�r  �r  u�r  j  }r   (j  j  j  �r!  j  hN�r"  j  j  M��r#  j  hj  �r$  j  hj  �r%  j  j  ]r&  �r'  j  j  j  }r(  (j  j  j  �r)  j"  hK�r*  j$  hK �r+  u�r,  �r-  j(  h��r.  u�r/  eKoK �r0  X   eventsr1  ]r2  X   supportsr3  ]r4  X   recordsr5  }r6  (X   killsr7  ]r8  (X
   KillRecordr9  }r:  (X   turnr;  KX	   level_nidr<  j=  X   killerr=  X   Temp_9r>  X   killeer?  X   Kid_5r@  u�rA  j9  }rB  (j;  Kj<  j=  j=  hj?  j>  u�rC  j9  }rD  (j;  Kj<  j=  j=  hnj?  X   Enemy_2rE  u�rF  j9  }rG  (j;  Kj<  j=  j=  h�j?  X   Enemy_1rH  u�rI  j9  }rJ  (j;  Kj<  j=  j=  h�j?  X   Enemy_3rK  u�rL  j9  }rM  (j;  Kj<  j=  j=  h�j?  X   Enemy_4rN  u�rO  j9  }rP  (j;  Kj<  j=  j=  hnj?  X   Enemy_10rQ  u�rR  j9  }rS  (j;  Kj<  j=  j=  h�j?  X   Enemy_6rT  u�rU  j9  }rV  (j;  Kj<  j=  j=  hnj?  X   Enemy_9rW  u�rX  j9  }rY  (j;  Kj<  j=  j=  h�j?  X   Enemy_5rZ  u�r[  j9  }r\  (j;  Kj<  j=  j=  h�j?  X   Enemy_7r]  u�r^  j9  }r_  (j;  Kj<  j=  j=  hj?  X   Enemy_8r`  u�ra  j9  }rb  (j;  Kj<  j=  j=  hj?  X   Rein_2rc  u�rd  j9  }re  (j;  Kj<  j=  j=  hnj?  X   Rein_4rf  u�rg  j9  }rh  (j;  Kj<  j=  j=  h�j?  X   Rein_1ri  u�rj  j9  }rk  (j;  Kj<  j=  j=  h�j?  X   Enemy_11rl  u�rm  j9  }rn  (j;  Kj<  j=  j=  h�j?  X   Rein_3ro  u�rp  j9  }rq  (j;  Kj<  j=  j=  hj?  X   Rein_6rr  u�rs  j9  }rt  (j;  Kj<  j=  j=  hnj?  X   Rein_5ru  u�rv  j9  }rw  (j;  Kj<  j=  j=  h�j?  j�  u�rx  j9  }ry  (j;  Kj<  j=  j=  j�  j?  X   Temprz  u�r{  j9  }r|  (j;  Kj<  j�  j=  j@  j?  j�  u�r}  j9  }r~  (j;  Kj<  j�  j=  j%  j?  j�  u�r  ej  ]r�  (X   DamageRecordr�  }r�  (j;  Kj<  j=  X   dealerr�  j>  X   receiverr�  j@  X   item_nidr�  X   Iron Axer�  X   over_damager�  Kj  KX   kindr�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  hj�  j>  j�  X
   Iron Swordr�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  hnj�  jE  j�  X   Jinxr�  j�  Kj  Kj�  X   hitr�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  jH  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  jH  j�  j�  j�  K
j  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  jK  j�  X   Shimmerr�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  jK  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  jN  j�  X
   Iron Lancer�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  jN  j�  h�j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  jN  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  jQ  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  hnj�  jQ  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  jW  j�  h�j�  X	   Axereaverr�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  jW  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  jW  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  hj�  j]  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  hj�  j]  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  jT  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  hnj�  jW  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  jZ  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  jZ  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  j]  j�  X   AxeOfWoer�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  hj�  j`  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  hj�  j`  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  hj�  jc  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  hj�  jc  j�  j�  j�  K
j  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  jl  j�  h�j�  X   Iron Bowr�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  jf  j�  hnj�  X   EtherealBlader�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  hnj�  jf  j�  X   Devilryr�  j�  K	j  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  ji  j�  X   Flashr�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  ji  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  jl  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  jl  j�  X   Javelinr�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  jo  j�  h�j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  jo  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  jo  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  ju  j�  hnj�  X   SapphireLancer�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  hnj�  ju  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  hj�  jr  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  hj�  jr  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  hj�  jr  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  hnj�  ju  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  hj�  j�  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  j�  j�  hj�  X   LunarArcr�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  hj�  j�  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  j�  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  j�  j�  h�j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  j�  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  j�  j�  h�j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  j�  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j=  j�  j�  j�  jz  j�  X   Silver Swordr�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j�  j�  j@  j�  j�  j�  j�  j�  Kj  Kj�  j�  u�r�  j�  }r�  (j;  Kj<  j�  j�  j%  j�  j�  j�  j  j�  Kj  Kj�  j�  u�r   eX   healingr  ]r  (j�  }r  (j;  Kj<  j=  j�  h�j�  h�j�  X   Treatr  j�  Kj  Kj�  j�  u�r  j�  }r  (j;  Kj<  j=  j�  j]  j�  j]  j�  X
   BloodLancer  j�  K j  K j�  j�  u�r  j�  }r	  (j;  Kj<  j=  j�  j]  j�  j]  j�  j  j�  K j  K j�  j�  u�r
  j�  }r  (j;  Kj<  j=  j�  h�j�  h�j�  j  j�  Kj  Kj�  j�  u�r  j�  }r  (j;  Kj<  j=  j�  hnj�  hnj�  X   Herbsr  j�  Kj  Kj�  j�  u�r  j�  }r  (j;  Kj<  j=  j�  h�j�  hj�  j  j�  Kj  Kj�  j�  u�r  eX   deathr  ]r  X   item_user  ]r  (X
   ItemRecordr  }r  (j;  Kj<  j=  X   userr  h�j�  j�  u�r  j  }r  (j;  Kj<  j=  j  h�j�  j�  u�r  j  }r  (j;  Kj<  j=  j  h�j�  j�  u�r  j  }r  (j;  Kj<  j=  j  jW  j�  j�  u�r  j  }r   (j;  Kj<  j=  j  h�j�  j�  u�r!  j  }r"  (j;  Kj<  j=  j  h�j�  j�  u�r#  j  }r$  (j;  Kj<  j=  j  h�j�  j�  u�r%  j  }r&  (j;  Kj<  j=  j  jf  j�  j�  u�r'  j  }r(  (j;  Kj<  j=  j  hnj�  j�  u�r)  j  }r*  (j;  Kj<  j=  j  hnj�  j  u�r+  j  }r,  (j;  Kj<  j=  j  h�j�  j�  u�r-  j  }r.  (j;  Kj<  j=  j  jo  j�  j�  u�r/  j  }r0  (j;  Kj<  j=  j  ju  j�  j�  u�r1  j  }r2  (j;  Kj<  j=  j  j�  j�  j�  u�r3  j  }r4  (j;  Kj<  j�  j  j@  j�  j�  u�r5  j  }r6  (j;  Kj<  j�  j  j%  j�  j  u�r7  j  }r8  (j;  Kj<  j�  j  hj�  j  u�r9  eX   stealr:  ]r;  X   combat_resultsr<  ]r=  (X   CombatRecordr>  }r?  (j;  Kj<  j=  X   attackerr@  j>  X   defenderrA  j@  X   resultrB  j�  u�rC  j>  }rD  (j;  Kj<  j=  j@  hjA  j>  jB  j�  u�rE  j>  }rF  (j;  Kj<  j=  j@  hnjA  jE  jB  j�  u�rG  j>  }rH  (j;  Kj<  j=  j@  jH  jA  h�jB  jO  u�rI  j>  }rJ  (j;  Kj<  j=  j@  h�jA  jH  jB  j�  u�rK  j>  }rL  (j;  Kj<  j=  j@  h�jA  jH  jB  j�  u�rM  j>  }rN  (j;  Kj<  j=  j@  h�jA  jK  jB  j�  u�rO  j>  }rP  (j;  Kj<  j=  j@  h�jA  jK  jB  j�  u�rQ  j>  }rR  (j;  Kj<  j=  j@  h�jA  jN  jB  j�  u�rS  j>  }rT  (j;  Kj<  j=  j@  jN  jA  h�jB  j�  u�rU  j>  }rV  (j;  Kj<  j=  j@  h�jA  jN  jB  j�  u�rW  j>  }rX  (j;  Kj<  j=  j@  jQ  jA  h�jB  jO  u�rY  j>  }rZ  (j;  Kj<  j=  j@  h�jA  jQ  jB  j�  u�r[  j>  }r\  (j;  Kj<  j=  j@  hnjA  jQ  jB  j�  u�r]  j>  }r^  (j;  Kj<  j=  j@  h�jA  h�jB  j�  u�r_  j>  }r`  (j;  Kj<  j=  j@  jW  jA  h�jB  j�  u�ra  j>  }rb  (j;  Kj<  j=  j@  h�jA  jW  jB  j�  u�rc  j>  }rd  (j;  Kj<  j=  j@  h�jA  jW  jB  j�  u�re  j>  }rf  (j;  Kj<  j=  j@  j]  jA  hjB  jO  u�rg  j>  }rh  (j;  Kj<  j=  j@  hjA  j]  jB  jO  u�ri  j>  }rj  (j;  Kj<  j=  j@  hjA  j]  jB  jO  u�rk  j>  }rl  (j;  Kj<  j=  j@  hjA  j]  jB  j�  u�rm  j>  }rn  (j;  Kj<  j=  j@  hjA  j]  jB  j�  u�ro  j>  }rp  (j;  Kj<  j=  j@  jT  jA  h�jB  jO  u�rq  j>  }rr  (j;  Kj<  j=  j@  h�jA  jT  jB  j�  u�rs  j>  }rt  (j;  Kj<  j=  j@  hnjA  jW  jB  j�  u�ru  j>  }rv  (j;  Kj<  j=  j@  h�jA  jZ  jB  j�  u�rw  j>  }rx  (j;  Kj<  j=  j@  h�jA  jZ  jB  j�  u�ry  j>  }rz  (j;  Kj<  j=  j@  j]  jA  h�jB  jO  u�r{  j>  }r|  (j;  Kj<  j=  j@  h�jA  j]  jB  j�  u�r}  j>  }r~  (j;  Kj<  j=  j@  j`  jA  hjB  jO  u�r  j>  }r�  (j;  Kj<  j=  j@  hjA  j`  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  hjA  j`  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  jc  jA  hjB  jO  u�r�  j>  }r�  (j;  Kj<  j=  j@  hjA  jc  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  hjA  jc  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  h�jA  h�jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  jl  jA  h�jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  jf  jA  hnjB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  hnjA  jf  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  hnjA  hnjB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  ji  jA  h�jB  jO  u�r�  j>  }r�  (j;  Kj<  j=  j@  h�jA  ji  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  h�jA  ji  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  h�jA  jl  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  h�jA  jl  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  jo  jA  h�jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  h�jA  jo  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  h�jA  jo  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  hnjA  ju  jB  jO  u�r�  j>  }r�  (j;  Kj<  j=  j@  ju  jA  hnjB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  hnjA  ju  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  jr  jA  hjB  jO  u�r�  j>  }r�  (j;  Kj<  j=  j@  hjA  jr  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  hjA  jr  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  hjA  jr  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  hnjA  ju  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  hnjA  j�  jB  jO  u�r�  j>  }r�  (j;  Kj<  j=  j@  hjA  j�  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  j�  jA  hjB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  hjA  j�  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  h�jA  hjB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  h�jA  j�  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  j�  jA  h�jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  h�jA  j�  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  j�  jA  h�jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  h�jA  j�  jB  j�  u�r�  j>  }r�  (j;  Kj<  j=  j@  j�  jA  jz  jB  j�  u�r�  j>  }r�  (j;  Kj<  j�  j@  j�  jA  j%  jB  jO  u�r�  j>  }r�  (j;  Kj<  j�  j@  j@  jA  j�  jB  j�  u�r�  j>  }r�  (j;  Kj<  j�  j@  j%  jA  j�  jB  j�  u�r�  j>  }r�  (j;  Kj<  j�  j@  hjA  hjB  jH  u�r�  eX   turns_takenr�  ]r�  (X   Recordr�  }r�  (j;  Kj<  j=  u�r�  j�  }r�  (j;  Kj<  j=  u�r�  j�  }r�  (j;  Kj<  j=  u�r�  j�  }r�  (j;  Kj<  j=  u�r�  j�  }r�  (j;  Kj<  j=  u�r�  j�  }r�  (j;  Kj<  j=  u�r�  j�  }r�  (j;  Kj<  j=  u�r�  j�  }r�  (j;  Kj<  j=  u�r�  j�  }r�  (j;  Kj<  j�  u�r�  eX   levelsr�  ]r�  (X   LevelRecordr�  }r�  (j;  Kj<  j=  X   unit_nidr�  h�j�  Khh�u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  Khh�u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  Khh�u�r�  j�  }r�  (j;  Kj<  j=  j�  hnj�  Khhpu�r�  j�  }r�  (j;  Kj<  j=  j�  hj�  Khhu�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  Khh�u�r�  eh]r�  (j�  }r�  (j;  Kj<  j=  j�  hnj�  Khhpu�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  Khh�u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  Khh�u�r�  j�  }r�  (j;  Kj<  j=  j�  h�j�  Khh�u�r�  j�  }r   (j;  Kj<  j=  j�  h�j�  K
hh�u�r  j�  }r  (j;  Kj<  j=  j�  hnj�  Khhpu�r  j�  }r  (j;  Kj<  j=  j�  h�j�  K(hh�u�r  j�  }r  (j;  Kj<  j=  j�  h�j�  Khh�u�r  j�  }r  (j;  Kj<  j=  j�  h�j�  K
hh�u�r	  j�  }r
  (j;  Kj<  j=  j�  hj�  K
hhu�r  j�  }r  (j;  Kj<  j=  j�  h�j�  Khh�u�r  j�  }r  (j;  Kj<  j=  j�  hnj�  Khhpu�r  j�  }r  (j;  Kj<  j=  j�  h�j�  Khh�u�r  j�  }r  (j;  Kj<  j=  j�  h�j�  Khh�u�r  j�  }r  (j;  Kj<  j=  j�  h�j�  K(hh�u�r  j�  }r  (j;  Kj<  j=  j�  hj�  Khhu�r  j�  }r  (j;  Kj<  j=  j�  hj�  Khhu�r  j�  }r  (j;  Kj<  j=  j�  h�j�  Khh�u�r  j�  }r  (j;  Kj<  j=  j�  h�j�  Khh�u�r  j�  }r  (j;  Kj<  j=  j�  hnj�  Khhpu�r  j�  }r   (j;  Kj<  j=  j�  hnj�  K(hhpu�r!  j�  }r"  (j;  Kj<  j=  j�  h�j�  Khh�u�r#  j�  }r$  (j;  Kj<  j=  j�  h�j�  K	hh�u�r%  j�  }r&  (j;  Kj<  j=  j�  h�j�  Khh�u�r'  j�  }r(  (j;  Kj<  j=  j�  h�j�  Khh�u�r)  j�  }r*  (j;  Kj<  j=  j�  hnj�  K	hhpu�r+  j�  }r,  (j;  Kj<  j=  j�  hj�  Khhu�r-  j�  }r.  (j;  Kj<  j=  j�  hnj�  Khhpu�r/  j�  }r0  (j;  Kj<  j=  j�  hnj�  Khhpu�r1  j�  }r2  (j;  Kj<  j=  j�  hj�  K	hhu�r3  j�  }r4  (j;  Kj<  j=  j�  h�j�  Khh�u�r5  j�  }r6  (j;  Kj<  j=  j�  h�j�  K	hh�u�r7  j�  }r8  (j;  Kj<  j=  j�  h�j�  KDhh�u�r9  ej�  ]r:  uX   speak_stylesr;  }r<  (X	   __defaultr=  }r>  (hX	   __defaultr?  j0  NhNX   widthr@  Nj�  KX
   font_colorrA  NX	   font_typerB  X   convorC  X
   backgroundrD  X   message_bg_baserE  X	   num_linesrF  KX   draw_cursorrG  �X   message_tailrH  X   message_bg_tailrI  X   transparencyrJ  G?�������X   name_tag_bgrK  X   name_tagrL  X   flagsrM  cbuiltins
set
rN  ]rO  �rP  RrQ  uX   __default_textrR  }rS  (hX   __default_textrT  j0  NhNj@  Nj�  G?�      jA  NjB  X   textrU  jD  X   menu_bg_baserV  jF  K jG  NjH  NjJ  G?�������jK  jV  jM  jN  ]rW  �rX  RrY  uX   __default_helprZ  }r[  (hX   __default_helpr\  j0  NhNj@  Nj�  G?�      jA  NjB  jC  jD  hjF  KjG  �jH  NjJ  G?�������jK  jL  jM  jN  ]r]  �r^  Rr_  uX   noirr`  }ra  (hX   noirrb  j0  NhNj@  Nj�  NjA  X   whiterc  jB  NjD  X   menu_bg_darkrd  jF  NjG  NjH  hjJ  NjK  NjM  jN  ]re  �rf  Rrg  uX   hintrh  }ri  (hX   hintrj  j0  Nhcapp.utilities.enums
Alignments
rk  X   centerrl  �rm  Rrn  j@  K�j�  NjA  NjB  NjD  X   menu_bg_parchmentro  jF  KjG  NjH  hjJ  NjK  NjM  jN  ]rp  �rq  Rrr  uX	   cinematicrs  }rt  (hX	   cinematicru  j0  Nhjn  j@  Nj�  NjA  X   greyrv  jB  X   chapterrw  jD  hjF  KjG  �jH  hjJ  NjK  NjM  jN  ]rx  �ry  Rrz  uX	   narrationr{  }r|  (hX	   narrationr}  j0  NhKKn�r~  j@  K�j�  NjA  jc  jB  NjD  jV  jF  NjG  NjH  hjJ  NjK  NjM  jN  ]r  �r�  Rr�  uX   narration_topr�  }r�  (hX   narration_topr�  j0  NhKK�r�  j@  K�j�  NjA  jc  jB  NjD  jV  jF  NjG  NjH  hjJ  NjK  NjM  jN  ]r�  �r�  Rr�  uX   clearr�  }r�  (hX   clearr�  j0  NhNj@  Nj�  NjA  jc  jB  NjD  hjF  NjG  �jH  hjJ  NjK  NjM  jN  ]r�  �r�  Rr�  uX   thought_bubbler�  }r�  (hX   thought_bubbler�  j0  NhNj@  Nj�  NjA  NjB  NjD  NjF  NjG  NjH  X   message_bg_thought_tailr�  jJ  NjK  NjM  jN  ]r�  X   no_talkr�  a�r�  Rr�  uX   boss_convo_leftr�  }r�  (hX   boss_convo_leftr�  j0  NhKHKp�r�  j@  K�j�  KjA  NjB  jC  jD  jE  jF  KjG  �jH  jI  jJ  G        jK  NjM  jN  ]r�  �r�  Rr�  uX   boss_convo_rightr�  }r�  (hX   boss_convo_rightr�  j0  NhKKp�r�  j@  K�j�  KjA  NjB  jC  jD  jE  jF  KjG  �jH  jI  jJ  G        jK  NjM  jN  ]r�  �r�  Rr�  uuX   market_itemsr�  }r�  X   unlocked_lorer�  ]r�  X
   dialog_logr�  ]r�  hX   Growths boosted!r�  �r�  aX   already_triggered_eventsr�  ]r�  (X   0 House1Shaylar�  X   0 House2Raelinr�  X   0 House3Carlinr�  X   0 FightAckermanr�  eX   talk_optionsr�  ]r�  X   base_convosr�  }r�  X   current_random_stater�  J ��yX   boundsr�  (K K KKtr�  X	   roam_infor�  capp.engine.roam.roam_info
RoamInfo
r�  )�r�  }r�  (X   roamr�  �X   roam_unit_nidr�  Nubu.