�}q (X   unitsq]q(}q(X   nidqX   AlpinqX
   prefab_nidqhX   positionqKK�qX   teamq	X   playerq
X   partyqX   EirikaqX   klassqX   AlpinVillagerqX   variantqX    qX   factionqNX   levelqKX   expqK_X   genericq�X
   persistentq�X   aiqX   NoneqX   roam_aiqNX   ai_groupqhX   itemsq]q(K�K�K�eX   nameqX   AlpinqX   descqXH   An energetic young lad. Dreams of joining the army of [KINGDOM] someday.qX   tagsq ]q!X   Youthq"aX   statsq#}q$(X   HPq%KX   STRq&KX   MAGq'K X   SKLq(KX   SPDq)KX   LCKq*KX   DEFq+KX   RESq,K X   CONq-KX   MOVq.KuX   growthsq/}q0(h%K<h&K#h'K h(Kh)Kh*Kh+Kh,Kh-K h.KuX   growth_pointsq1}q2(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uX   stat_cap_modifiersq3}q4(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uX   starting_positionq5hX   wexpq6}q7(X   Swordq8KX   Lanceq9K X   Axeq:K X   Bowq;K X   Staffq<K X   Lightq=K X   Animaq>K X   Darkq?K X   Defaultq@K uX   portrait_nidqAX   AlpinqBX   affinityqCX   FireqDX   skillsqE]qF(K�K�capp.engine.source_type
SourceType
qGX   itemqH���qI�qJRqK�qLK�X   gameqMhGX   globalqN���qO�qPRqQ�qRK�hMhQ�qSK�hhGX   personalqT���qU�qVRqW�qXK�hhW�qYMZhhGX   terrainqZ���q[�q\Rq]�q^MrX   Fire2_0q_hGX   regionq`���qa�qbRqc�qdeX   notesqe]qfX
   current_hpqgKX   current_manaqhK X   current_fatigueqiK X   travelerqjNX   current_guard_gaugeqkK X   built_guardql�X   deadqm�X   action_stateqn(��������tqoX   _fieldsqp}qqX   equipped_weaponqrK�X   equipped_accessoryqsNu}qt(hX   CarlinquhhuhKK�qvh	X   playerqwhhhX   CarlinVillagerqxhhhNhKhK h�h�hX   NoneqyhNhhh]qz(K�K�K�ehX   Carlinq{hXA   A magically gifted youngster from [TOWN]. Energetic and sporatic.q|h ]q}X   Youthq~ah#}q(h%K
h&K h'Kh(Kh)Kh*Kh+Kh,Kh-Kh.Kuh/}q�(h%K2h&K h'Kh(K#h)Kh*Kh+Kh,Kh-K h.Kuh1}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5hvh6}q�(h8K h9K h:K h;K h<K h=K h>K h?Kh@K uhAX   Carlinq�hCX   Windq�hE]q�(K�K�hK�q�K�hMhQ�q�K�hMhQ�q�K�huhW�q�K�huhW�q�M[hvh]�q�ehe]q�hgK
hhK hiK hjNhkK hl�hm�hn(��������tq�hp}q�hrK�hsNu}q�(hX   Shaylaq�hh�hKK�q�h	X   playerq�hhhX   ShaylaVillagerq�hhhNhKhK h�h�hX   Noneq�hNhhh]q�(K�K�K�ehX   Shaylaq�hXI   A clumsy young lass who hopes to adventure the world. Gutsy and fearless.q�h ]q�X   Youthq�ah#}q�(h%Kh&Kh'K h(Kh)Kh*Kh+Kh,K h-Kh.Kuh/}q�(h%KAh&Kh'K h(Kh)Kh*Kh+K#h,Kh-K h.Kuh1}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5h�h6}q�(h8K h9K h:Kh;K h<K h=K h>K h?K h@K uhAX   Shaylaq�hCX   Thunderq�hE]q�(K�K�hK�q�K�hMhQ�q�K�hMhQ�q�K�h�hW�q�K�h�hW�q�M\h�h]�q�MoX   Fire1_1q�hc�q�ehe]q�hgK
hhK hiK hjNhkK hl�hm�hn(��������tq�hp}q�hrK�hsNu}q�(hX   Raelinq�hh�hKK�q�h	X   playerq�hhhX   ZoyaVillagerq�hhhNhKhK h�h�hX   Noneq�hNhhh]q�(K�K�K�ehX   Raelinq�hX8   The shut-in daughter of a fisherman. Timid and cautious.q�h ]q�X   Youthq�ah#}q�(h%Kh&Kh'K h(Kh)Kh*Kh+Kh,Kh-Kh.Kuh/}q�(h%K7h&Kh'K h(Kh)K#h*Kh+Kh,Kh-K h.Kuh1}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5h�h6}q�(h8K h9Kh:K h;K h<K h=K h>K h?K h@K uhAX   Raelin (LittleKyng)q�hCX   Iceq�hE]q�(K�K�hK�q�K�hMhQ�q�K�hMhQ�q�K�h�hW�q�K�h�hW�q�M]h�h]�q�ehe]q�hgKhhK hiK hjNhkK hl�hm�hn(��������tq�hp}q�hrK�hsNu}q�(hX   Elwynnq�hh�hKK	�q�h	X   playerq�hhhX   Monkq�hhhNhKhK h�h�hX   Noneq�hNhhh]q�(K�K�K�K�ehX   Elwynnq�hXM   Former vagabond turned holy monk of [CHURCH]. Stern and generally unpleasant.q�h ]q�X   Adultq�ah#}q�(h%Kh&K h'Kh(Kh)Kh*Kh+Kh,Kh-Kh.Kuh/}q�(h%K7h&K h'Kh(Kh)Kh*K
h+Kh,K#h-K h.Kuh1}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5h�h6}q�(h8K h9K h:K h;K h<K h=Kh>K h?K h@K uhAX   Elwynn (LittleKyng)q�hCX   Darkq�hE]q�(K�hMhQ�q�M hMhQ�q�Mh�hW�q�Mh�hW�q�Mh�hW�q�M^h�h]�q�ehe]q�hgKhhK hiK hjNhkK hl�hm�hn(��������tq�hp}q�hrK�hsNu}q�(hX   Saraidq�hh�hKK�q�h	X   playerq�hhhX   Hunterq�hhhNhKhKh�h�hX   Noneq�hNhhh]q�(K�K�K�K�ehX   Saraidq�hX0   A fledgling huntress. Unreserved and unfiltered.q�h ]q�X   Adultq�ah#}q�(h%Kh&Kh'Kh(Kh)Kh*K h+K h,Kh-Kh.Kuh/}q�(h%K2h&Kh'K h(Kh)K#h*Kh+Kh,Kh-K h.Kuh1}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5KK�q�h6}q�(h8K h9K h:K h;G@*      h<K h=K h>K h?K h@K uhAX   Sloaneq�hCX   Lightq�hE]q�(MK�hK�q�MhMhQ�q�MhMhQ�q�Mh�hW�q�Mh�hW�q�M	h�hW�q�M_h�h]�r   ehe]r  hgKhhK hiK hjNhkK hl�hm�hn(��������tr  hp}r  hrK�hsNu}r  (hX   Quinleyr  hj  hKK�r  h	X   playerr  hhhX   Mager  hX   Quinleyr	  hNhKhK h�h�hX   Noner
  hNhhh]r  (K�K�K�ehX   Quinleyr  hXN   A genius magician who's grown tired of elemental magic. Outgoing and friendly.r  h ]r  X   Adultr  ah#}r  (h%Kh&K h'Kh(Kh)Kh*Kh+K h,Kh-Kh.Kuh/}r  (h%K2h&K h'K#h(Kh)Kh*Kh+Kh,Kh-K h.Kuh1}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5j  h6}r  (h8K h9K h:K h;K h<K h=K h>Kh?K h@K uhAX   Quinleyr  hCX   Darkr  hE]r  (M
K�hK�r  MhMhQ�r  MhMhQ�r  Mj  hW�r  Mj  hW�r  Mj  hW�r  M`j  h]�r  MpX   Fire1_3r  hc�r   ehe]r!  hgKhhK hiK hjNhkK hl�hm�hn(��������tr"  hp}r#  hrK�hsNu}r$  (hX   Elspethr%  hj%  hKK�r&  h	X   playerr'  hhhX   Clericr(  hX   Elspethr)  hNhKhK h�h�hX   Noner*  hNhhh]r+  K�ahX   Elspethr,  hX2   Lifelong devotee of [CHURCH]. Humble and selfless.r-  h ]r.  X   Adultr/  ah#}r0  (h%Kh&K h'Kh(K h)Kh*Kh+K h,Kh-Kh.Kuh/}r1  (h%K2h&K h'Kh(Kh)Kh*K#h+Kh,K#h-K h.Kuh1}r2  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r3  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5j&  h6}r4  (h8K h9K h:K h;K h<Kh=K h>K h?K h@K uhAX   Elspethr5  hCX   Lightr6  hE]r7  (MhMhQ�r8  MhMhQ�r9  Mj%  hW�r:  Mj%  hW�r;  Mj%  hW�r<  Maj&  h]�r=  ehe]r>  hgKhhK hiK hjNhkK hl�hm�hn(��������tr?  hp}r@  hrNhsNu}rA  (hX   LamonterB  hjB  hKK
�rC  h	X   playerrD  hhhX   SoldierrE  hX   LamonterF  hNhKhK h�h�hX   NonerG  hNhhh]rH  (K�K�K�ehX   LamonterI  hX,   One of Leod's guards. Kindhearted and loyal.rJ  h ]rK  X   AdultrL  ah#}rM  (h%Kh&Kh'K h(Kh)Kh*Kh+Kh,K h-K
h.Kuh/}rN  (h%KAh&Kh'K h(Kh)Kh*K
h+K#h,Kh-K h.Kuh1}rO  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}rP  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5jC  h6}rQ  (h8K h9Kh:K h;K h<K h=K h>K h?K h@K uhAX   LamonterR  hCX   IcerS  hE]rT  (MhMhQ�rU  MhMhQ�rV  MjB  hW�rW  MjB  hW�rX  MjB  hW�rY  MK�hK�rZ  MbjC  h]�r[  ehe]r\  hgKhhK hiK hjNhkK hl�hm�hn(��������tr]  hp}r^  hrK�hsNu}r_  (hX   Garveyr`  hj`  hKK	�ra  h	X   playerrb  hhhX   Archerrc  hX   Garveyrd  hNhKhK h�h�hX   Nonere  hNhhh]rf  (K�K�K�ehX   Garveyrg  hXA   One of Leod's guards. Dislikes his hometown and fellow villagers.rh  h ]ri  X   Adultrj  ah#}rk  (h%Kh&Kh'K h(Kh)Kh*Kh+Kh,K h-K
h.Kuh/}rl  (h%K7h&K#h'K h(Kh)Kh*K
h+Kh,Kh-K h.Kuh1}rm  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}rn  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5ja  h6}ro  (h8K h9K h:K h;Kh<K h=K h>K h?K h@K uhAX   Garveyrp  hCX   Firerq  hE]rr  (MhMhQ�rs  MhMhQ�rt  Mj`  hW�ru  Mj`  hW�rv  MK�hK�rw  Mcja  h]�rx  MlNhGX   defaultry  ���rz  �r{  Rr|  �r}  ehe]r~  hgKhhK hiK hjNhkK hl�hm�hn(��������tr  hp}r�  hrK�hsNu}r�  (hX   Orlar�  hj�  hK	K�r�  h	X   playerr�  hhhX   Fighterr�  hX   Orlar�  hNhKhK h�h�hX   Noner�  hNhhh]r�  (K�K�K�ehX   Orlar�  hXK   Lumberjack, carpenter, and all around outdoorsperson. Outspoken and jovial.r�  h ]r�  X   Adultr�  ah#}r�  (h%Kh&Kh'K h(Kh)Kh*Kh+Kh,K h-Kh.Kuh/}r�  (h%KAh&K#h'K h(Kh)Kh*K
h+Kh,Kh-K h.K
uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5j�  h6}r�  (h8K h9K h:Kh;K h<K h=K h>K h?K h@K uhAX   Orlar�  hCX   Windr�  hE]r�  (M hMhQ�r�  M!hMhQ�r�  M"j�  hW�r�  M#j�  hW�r�  M$j�  hW�r�  M%K�hK�r�  Mdj�  h]�r�  MtX   Fire2_2r�  hc�r�  ehe]r�  hgKhhK hiK hjNhkK hl�hm�hn(��������tr�  hp}r�  hrK�hsNu}r�  (hX   Ailsar�  hj�  hKK�r�  h	X   playerr�  hhhX   Lancerr�  hX   Ailsar�  hNhKhKh�h�hX   Noner�  hNhhh]r�  (K�K�K�ehX   Ailsar�  hXE   A reclusive fisherman. Quick witted and sharped tongued. Zoya's aunt.r�  h ]r�  X   Adultr�  ah#}r�  (h%Kh&Kh'K h(Kh)Kh*Kh+K h,Kh-K	h.Kuh/}r�  (h%K<h&Kh'K h(Kh)K#h*K
h+Kh,Kh-K h.K
uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5KK�r�  h6}r�  (h8K h9G@6      h:K h;K h<K h=K h>K h?K h@K uhAX   Ailsar�  hCX   Thunderr�  hE]r�  (M&hMhQ�r�  M'hMhQ�r�  M(j�  hW�r�  M)j�  hW�r�  M*j�  hW�r�  M+K�hK�r�  Mej�  h]�r�  MnX   Fire1_0r�  hc�r�  ehe]r�  hgK
hhK hiK hjNhkK hl�hm�hn(��������tr�  hp}r�  hrK�hsNu}r�  (hX   Corridonr�  hj�  hKK�r�  h	X   playerr�  hhhX
   Blacksmithr�  hhhNhKhK h�h�hX   Noner�  hNhhh]r�  K�ahX   Corridonr�  hX;   Town blacksmith and retired war veteran. Friendly and just.r�  h ]r�  X   Adultr�  ah#}r�  (h%Kh&K h'K h(K h)Kh*Kh+Kh,Kh-Kh.Kuh/}r�  (h%KAh&K h'K h(K h)Kh*K#h+K#h,K#h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5j�  h6}r�  (h8K h9K h:K h;K h<K h=K h>K h?K h@K uhAX   Corridonr�  hCX   Lightr�  hE]r�  (M,hMhQ�r�  M-hMhQ�r�  M.j�  hW�r�  M/j�  hW�r�  M0j�  hW�r�  M1j�  hW�r�  Mfj�  h]�r�  MsX   Fire2_1r�  hc�r�  ehe]r�  hgKhhK hiK hjNhkK hl�hm�hn(��������tr�  hp}r�  hrNhsNu}r�  (hX   Laisrenr�  hj�  hKK	�r�  h	X   playerr�  hhhX   Cavalierr�  hX   Cathalr�  hNhKhK h�h�hX   Noner�  hNhhh]r�  (K�K�K�ehX   Laisrenr�  hXV   An aspiring recruit in [KINGDOM]'s army. Charming and outgoing. Alpin's older brother.r�  h ]r�  X   Adultr�  ah#}r�  (h%Kh&Kh'K h(Kh)Kh*Kh+Kh,Kh-Kh.Kuh/}r�  (h%KAh&K#h'K#h(K#h)K#h*K#h+K#h,K#h-K h.Kuh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5j�  h6}r�  (h8Kh9Kh:K h;K h<K h=K h>K h?K h@K uhAX   Cathalr�  hCX   Darkr�  hE]r�  (M2hMhQ�r�  M3hMhQ�r�  M4j�  hW�r�  M5j�  hW�r�  M6j�  hGh���r�  �r�  Rr�  �r�  Mgj�  h]�r�  MuX   Fire2_3r�  hc�r�  ehe]r   hgKhhK hiK hjNhkK hl�hm�hn(��������tr  hp}r  hrK�hsNu}r  (hX	   Cawthorner  hj  hKK�r  h	X   enemyr  hhhX   Generalr  hhhNhKhK h�h�hX   Noner  hNhhh]r	  K�ahX	   Cawthorner
  hX   Cawthorne text.r  h ]r  X   Bossr  ah#}r  (h%Kh&Kh'K h(Kh)Kh*K h+K	h,Kh-Kh.Kuh/}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5j  h6}r  (h8K h9Kh:K)h;K h<K h=K h>K h?K h@K uhAX	   Cawthorner  hCX   Firer  hE]r  (M7hMhQ�r  M8hMhQ�r  M9j  hW�r  M:j  hW�r  Mhj  h]�r  ehe]r  hgKhhK hiK hjNhkK hl�hm�hn(��������tr  hp}r  hrK�hsNu}r  (hX   101r  hj  hKK
�r   h	X   enemyr!  hhhX   Dracoknightr"  hNhX   Soldierr#  hKhK h�h�hX   Noner$  hNhhh]r%  (K�K�ehX   Soldierr&  hX=   The army of Grado, the largest nation on the entire continentr'  h ]r(  h#}r)  (h%K
h&Kh'K h(Kh)Kh*K h+Kh,K h-K
h.Kuh/}r*  (h%KAh&Kh'K h(K
h)Kh*K h+Kh,K h-K h.K uh1}r+  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r,  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5j   h6}r-  (h8K h9K h:Kh;K h<K h=K h>K h?K h@K uhANhCNhE]r.  (M;hMhQ�r/  M<hMhQ�r0  M=j"  j�  �r1  M>j  hW�r2  ehe]r3  hgK
hhK hiK hjNhkK hl�hm�hn(��������tr4  hp}r5  hrK�hsNu}r6  (hX   102r7  hj7  hKK�r8  h	X   otherr9  hhhX   ManDeadr:  hNhX   Villagerr;  hKhK h�h�hX   Noner<  hNhhh]r=  K�ahX   Villagerr>  hX   vilgerr?  h ]r@  h#}rA  (h%K
h&K h'K h(K h)K h*K h+K h,K h-K h.K uh/}rB  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}rC  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}rD  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5j8  h6}rE  (h8K h9K h:K h;K h<K h=K h>K h?K h@K uhANhCNhE]rF  (M?hMhQ�rG  M@hMhQ�rH  ehe]rI  hgK
hhK hiK hjNhkK hl�hm�hn(��������trJ  hp}rK  hrNhsNu}rL  (hX   103rM  hjM  hNh	X   enemyrN  hhhX   GriffonKnightrO  hNhX   SoldierrP  hKhK h�h�hX   NonerQ  hNhhh]rR  K�ahj&  hj'  h ]rS  h#}rT  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,Kh-Kh.Kuh/}rU  (h%K7h&K
h'K h(Kh)Kh*K h+Kh,K
h-K h.K uh1}rV  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}rW  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}rX  (h8K h9Kh:K h;K h<K h=K h>K h?K h@K uhANhCNhE]rY  (MAhMhQ�rZ  MBhMhQ�r[  MCjO  j�  �r\  MDK�hK�r]  ehe]r^  hgKhhK hiK hjNhkK hl�hm�hn(��������tr_  hp}r`  hrK�hsNu}ra  (hX   104rb  hjb  hNh	X   enemyrc  hhhX   Fighterrd  hNhX   Soldierre  hKhK h�h�hX   Nonerf  hNhhh]rg  K�ahj&  hj'  h ]rh  h#}ri  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,K h-Kh.Kuh/}rj  (h%KZh&Kh'K h(Kh)Kh*K h+K h,K h-K h.K uh1}rk  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}rl  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}rm  (h8K h9K h:Kh;K h<K h=K h>K h?K h@K uhANhCNhE]rn  (MEhMhQ�ro  MFhMhQ�rp  MGK�hK�rq  ehe]rr  hgKhhK hiK hjNhkK hl�hm�hn(��������trs  hp}rt  hrK�hsNu}ru  (hX   105rv  hjv  hNh	X   enemyrw  hhhX   Archerrx  hNhX   Soldierry  hKhK h�h�hX   Nonerz  hNhhh]r{  (K�K�K�ehj&  hj'  h ]r|  h#}r}  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,Kh-Kh.Kuh/}r~  (h%K7h&Kh'K h(Kh)Kh*K h+K h,K h-K h.K uh1}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5KK�r�  h6}r�  (h8K h9K h:K h;G?�      h<K h=K h>K h?K h@K uhANhCNhE]r�  (MHhMhQ�r�  MIhMhQ�r�  ehe]r�  hgK hhK hiK hjNhkK hl�hm�hn(��������tr�  hp}r�  hrK�hsNu}r�  (hX   106r�  hj�  hNh	X   enemyr�  hhhX   Soldierr�  hNhX   Soldierr�  hKhK h�h�hX   Yolor�  hNhhh]r�  K�ahj&  hj'  h ]r�  h#}r�  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,K h-Kh.Kuh/}r�  (h%KFh&Kh'K h(Kh)Kh*K h+Kh,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5KK�r�  h6}r�  (h8K h9Kh:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r�  (MJhMhQ�r�  MKhMhQ�r�  MLK�hK�r�  ehe]r�  hgK hhK hiK hjNhkK hl�hm�hn(��������tr�  hp}r�  hrK�hsNu}r�  (hX	   Davenportr�  hj�  hNh	X   enemyr�  hhhX   Great_Knightr�  hhhNhKhK h�h�hX   Noner�  hNhhh]r�  (K�K�K�K�ehX	   Davenportr�  hX   Davenport text.r�  h ]r�  X   Bossr�  ah#}r�  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,Kh-Kh.Kuh/}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8Kh9Kh:Kh;K h<K h=K h>K h?K h@K uhAX	   Davenportr�  hCX   Icer�  hE]r�  (MMhMhQ�r�  MNhMhQ�r�  MOj�  hW�r�  MPj�  j�  �r�  MQK�hK�r�  ehe]r�  hgKhhK hiK hjNhkK hl�hm�hn(��������tr�  hp}r�  hrK�hsNu}r�  (hX
   Brassfieldr�  hj�  hKK�r�  h	X   enemyr�  hhhX   Heror�  hhhNhKhK h�h�hX   Noner�  hNhhh]r�  (K�K�K�ehX
   Brassfieldr�  hX   Brassfield text.r�  h ]r�  X   Bossr�  ah#}r�  (h%Kh&Kh'Kh(Kh)Kh*K h+Kh,Kh-Kh.Kuh/}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5j�  h6}r�  (h8Kh9K h:Kh;K h<K h=K h>K h?K h@K uhAX
   Brassfieldr�  hCX   Lightr�  hE]r�  (MRhMhQ�r�  MShMhQ�r�  MTj�  hW�r�  MUK�hK�r�  Mkj�  h]�r�  ehe]r�  hgKhhK hiK hjNhkK hl�hm�hn(��������tr�  hp}r�  hrK�hsNu}r�  (hX   Ackermanr�  hj�  hNh	X   enemyr�  hhhX   Warriorr�  hhhNhKhK h�h�hX   Noner�  hNhhh]r�  (K�K�K�ehX   Ackermanr�  hX   Ackerman text.r�  h ]r�  X   Bossr�  ah#}r�  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,K h-Kh.Kuh/}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8K h9K h:Kh;Kh<K h=K h>K h?K h@K uhAX   Ackermanr�  hCX   Firer�  hE]r�  (MVhMhQ�r�  MWhMhQ�r�  MXj�  hW�r�  MYK�hK�r�  ehe]r�  hgKhhK hiK hjNhkK hl�hm�hn(��������tr�  hp}r�  hrK�hsNueh]r�  (}r�  (X   uidr�  K�hX
   Iron Swordr�  hX
   Iron Swordr�  hhX	   owner_nidr�  hX	   droppabler�  �X   datar�  }r�  X   subitemsr�  ]r�  X
   componentsr�  ]r�  (X   weaponr�  N�r�  X   target_enemyr�  N�r�  X	   min_ranger�  K�r�  X	   max_ranger�  K�r   X   damager  K�r  X   hitr  K
�r  X	   level_expr  N�r  X   weapon_typer  X   Swordr  �r	  X   critr
  K �r  eu}r  (j�  K�hX   HeartOfFirer  hX    <orange>Heart Of Flames</orange>r  hX   +1 Damage. Fire affinity only.r  j�  hj�  �j�  }r  j�  ]r  j�  ]r  (X   status_on_holdr  X   HeartofFirer  �r  X
   text_colorr  X   oranger  �r  eu}r  (j�  K�hX   Herbsr  hX   Herbsr  hX(   Restores 4 HP. Unit can act after using.r  j�  hj�  �j�  }r  (X   usesr  KX   starting_usesr  Kuj�  ]r   j�  ]r!  (X   usabler"  N�r#  X   target_allyr$  N�r%  j  K�r&  X   uses_optionsr'  ]r(  ]r)  (X   LoseUsesOnMiss (T/F)r*  X   Fr+  X   Lose uses even on missr,  ea�r-  X   healr.  K�r/  X   map_hit_add_blendr0  ]r1  (K`K�K�e�r2  X   attack_after_combatr3  N�r4  eu}r5  (j�  K�hX   Jinxr6  hX   Jinxr7  hX,   Lowers target's Crit Avoid by 20 for 1 turn.r8  j�  huj�  �j�  }r9  j�  ]r:  j�  ]r;  (j�  N�r<  j�  N�r=  j�  K�r>  j�  K�r?  j  K�r@  j  K�rA  j  N�rB  j  X   DarkrC  �rD  j0  ]rE  (KHK KAe�rF  X   magicrG  N�rH  X   unrepairablerI  N�rJ  X   status_on_hitrK  X	   TerrifiedrL  �rM  j
  K �rN  X   battle_cast_animrO  X   FluxrP  �rQ  eu}rR  (j�  K�hX   DevilryrS  hX   DevilryrT  hX   Can't miss. -2 SPD.
HP Cost: 4.rU  j�  huj�  �j�  }rV  (j  Kj  Kuj�  ]rW  j�  ]rX  (j�  N�rY  j�  N�rZ  j�  K�r[  j�  K�r\  j  K�r]  j  N�r^  j  X   Darkr_  �r`  j0  ]ra  (KHK KAe�rb  jG  N�rc  jI  N�rd  j  K�re  j'  ]rf  (]rg  (X   LoseUsesOnMiss (T/F)rh  j+  X   Lose uses even on missri  e]rj  (X   OneLossPerCombat (T/F)rk  j+  X    Doubling doesn't cost extra usesrl  ee�rm  j
  K �rn  X   status_on_equipro  X   SPD2rp  �rq  h6K�rr  X   hp_costrs  K�rt  jO  X   Fluxru  �rv  eu}rw  (j�  K�hX   HeartOfGalesrx  hX   <orange>Heart Of Gales</orange>ry  hX   +1 SPD. Wind affinity only.rz  j�  huj�  �j�  }r{  j�  ]r|  j�  ]r}  (j  X   HeartofGalesr~  �r  j  X   oranger�  �r�  eu}r�  (j�  K�hX   Iron Axer�  hX   Iron Axer�  hhj�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j  K�r�  j  K �r�  j  N�r�  j  X   Axer�  �r�  j
  K �r�  eu}r�  (j�  K�hX   HeartOfLightningr�  hX#   <orange>Heart Of Lightning</orange>r�  hX    +10 Crit. Thunder affinity only.r�  j�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j  X   HeartofLightningr�  �r�  j  X   oranger�  �r�  eu}r�  (j�  K�hj  hj  hj  j�  h�j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j"  N�r�  j$  N�r�  j  K�r�  j'  j(  �r�  j.  K�r�  j0  j1  �r�  j3  N�r�  eu}r�  (j�  K�hX
   Iron Lancer�  hX
   Iron Lancer�  hhj�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j  K�r�  j  K�r�  j  N�r�  j  X   Lancer�  �r�  j
  K �r�  eu}r�  (j�  K�hX   Javelinr�  hX   Javelinr�  hX   Cannot Counter. SPD -2.r�  j�  h�j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j  K�r�  j  J�����r�  j
  K �r�  j  N�r�  j  X   Lancer�  �r�  j  K�r�  j'  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j+  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j+  X    Doubling doesn't cost extra usesr�  ee�r�  X   cannot_counterr�  N�r�  h6K�r�  jo  X   SPD2r�  �r�  eu}r�  (j�  K�hX   HeartOfFrostr�  hX   <orange>Heart Of Frost</orange>r�  hX   +1 DEF/RES. Ice affinity only.r�  j�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j  X   HeartofFrostr�  �r�  j  X   oranger�  �r�  eu}r�  (j�  K�hX   Flashr�  hX   Flashr�  hX*   Lowers target's Accuracy by 20 for 1 turn.r�  j�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j  K�r�  j  K
�r�  j
  K
�r�  j  N�r�  j  X   Lightr�  �r�  j0  ]r�  (K�K�K e�r�  jG  N�r�  jI  N�r�  jO  X   Glimmerr�  �r�  jK  X   Dazzledr�  �r�  eu}r�  (j�  K�hX   Shimmerr�  hX   Shimmerr�  hX   Cannot miss.r�  j�  h�j�  �j�  }r�  (j  Kj  Kuj�  ]r   j�  ]r  (j�  N�r  j�  N�r  j�  K�r  j�  K�r  j  K�r  j
  K
�r  j  N�r  j  X   Lightr	  �r
  j0  ]r  (K�K�K e�r  jG  N�r  jI  N�r  h6K�r  j  K�r  j'  ]r  (]r  (X   LoseUsesOnMiss (T/F)r  j+  X   Lose uses even on missr  e]r  (X   OneLossPerCombat (T/F)r  j+  X    Doubling doesn't cost extra usesr  ee�r  jO  X   Glimmerr  �r  eu}r  (j�  K�hj  hj  hj  j�  h�j�  �j�  }r  (j  Kj  Kuj�  ]r  j�  ]r  (j"  N�r  j$  N�r   j  K�r!  j'  j(  �r"  j.  K�r#  j0  j1  �r$  j3  N�r%  eu}r&  (j�  K�hX   Iron Bowr'  hX   Iron Bowr(  hhj�  h�j�  �j�  }r)  j�  ]r*  j�  ]r+  (j�  N�r,  j�  N�r-  j�  K�r.  j�  K�r/  j  K�r0  j  K
�r1  j  N�r2  j  X   Bowr3  �r4  X   effective_damager5  }r6  (X   effective_tagsr7  ]r8  X   Flyingr9  aX   effective_multiplierr:  G@      X   effective_bonus_damager;  K X   show_effectiveness_flashr<  �u�r=  j
  K �r>  X   eval_warningr?  X   'Flying' in unit.tagsr@  �rA  eu}rB  (j�  K�hX   LongbowrC  hX   LongbowrD  hX   Cannot Counter. SPD -1.rE  j�  h�j�  �j�  }rF  (j  K
j  K
uj�  ]rG  j�  ]rH  (j�  N�rI  j�  N�rJ  j  N�rK  j  K�rL  j  J�����rM  j  X   BowrN  �rO  j  K
�rP  j'  ]rQ  (]rR  (X   LoseUsesOnMiss (T/F)rS  j+  X   Lose uses even on missrT  e]rU  (X   OneLossPerCombat (T/F)rV  j+  X    Doubling doesn't cost extra usesrW  ee�rX  j
  K�rY  j�  K�rZ  j�  K�r[  j�  N�r\  j5  }r]  (j7  ]r^  X   Flyingr_  aj:  G@      j;  K j<  �u�r`  jo  X   SPD1ra  �rb  j?  X   'Flying' in unit.tagsrc  �rd  eu}re  (j�  K�hX   HeartOfRadiancerf  hX"   <orange>Heart Of Radiance</orange>rg  hX   +15 Avoid. Holy affinity only.rh  j�  h�j�  �j�  }ri  j�  ]rj  j�  ]rk  (j  X   HeartofRadiancerl  �rm  j  X   orangern  �ro  eu}rp  (j�  K�hX   Firestarterrq  hX   <purple>Firestarter</purple>rr  hX=   Saraid Only.
Sets terrain ablaze after combat.
Cannot double.rs  j�  h�j�  �j�  }rt  (j  K
j  Kuj�  ]ru  j�  ]rv  (j�  N�rw  j�  N�rx  j�  K�ry  j�  K�rz  j  K�r{  j  K�r|  j
  K �r}  j  K�r~  j'  ]r  (]r�  (X   LoseUsesOnMiss (T/F)r�  j+  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j+  X    Doubling doesn't cost extra usesr�  ee�r�  j  N�r�  j  X   Bowr�  �r�  X   prf_unitr�  ]r�  X   Saraidr�  a�r�  j  X   purpler�  �r�  j5  }r�  (j7  ]r�  X   Flyingr�  aj:  G@       j;  K j<  �u�r�  X   event_after_combat_even_missr�  X   Global Firestarter_Originr�  �r�  X	   no_doubler�  N�r�  eu}r�  (j�  K�hX   Entangler�  hX   Entangler�  hX'   Lowers target's Avoid by 20 for 1 turn.r�  j�  j  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j  K�r�  j  K�r�  j  N�r�  j  X   Animar�  �r�  j0  ]r�  (KUKUK e�r�  jG  N�r�  jI  N�r�  jK  X	   Entangledr�  �r�  j
  K �r�  jO  X   Firer�  �r�  eu}r�  (j�  K�hX   Firer�  hX   Firer�  hX*   Effective against horseback units. -1 SPD.r�  j�  j  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j  K�r�  j  K �r�  j
  K�r�  j  K�r�  j'  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j+  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j+  X    Doubling doesn't cost extra usesr�  ee�r�  j  N�r�  j  X   Animar�  �r�  j0  ]r�  (K�K`K e�r�  jG  N�r�  jI  N�r�  j5  }r�  (j7  ]r�  X   Horser�  aj:  G@      j;  K j<  �u�r�  h6K�r�  jo  X   SPD1r�  �r�  j?  X   'Horse' in unit.tagsr�  �r�  jO  X   Firer�  �r�  eu}r�  (j�  K�hX   HeartOfDarknessr�  hX"   <orange>Heart Of Darkness</orange>r�  hX   +15 Hit. Dark affinity only.r�  j�  j  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j  X   HeartofDarknessr�  �r�  j  X   oranger�  �r�  eu}r�  (j�  K�hX	   ChurchKeyr�  hX
   Church Keyr�  hX   Opens any door in the church.r�  j�  j%  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j  K�r�  j'  ]r�  ]r�  (X   LoseUsesOnMiss (T/F)r�  j+  X   Lose uses even on missr�  ea�r�  X
   can_unlockr�  X7   region.nid.startswith('Door') and game.level.nid == '2'r�  �r�  eu}r�  (j�  K�hX   Steel Lancer�  hX   Steel Lancer�  hX   SPD -1.r�  j�  jB  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j  N�r�  j  K�r   j  K �r  j  X   Lancer  �r  j  K�r  j'  ]r  (]r  (X   LoseUsesOnMiss (T/F)r  j+  X   Lose uses even on missr  e]r	  (X   OneLossPerCombat (T/F)r
  j+  X    Doubling doesn't cost extra usesr  ee�r  j
  K �r  j�  K�r  j�  K�r  jo  X   SPD1r  �r  eu}r  (j�  K�hX	   Axereaverr  hX	   Axereaverr  hX%   Reverses the weapon triangle. SPD -1.r  j�  jB  j�  �j�  }r  (j  Kj  Kuj�  ]r  j�  ]r  (j�  N�r  j�  N�r  j  N�r  j  K�r  j  K �r  j  X   Lancer  �r  j  K�r   j'  ]r!  (]r"  (X   LoseUsesOnMiss (T/F)r#  j+  X   Lose uses even on missr$  e]r%  (X   OneLossPerCombat (T/F)r&  j+  X    Doubling doesn't cost extra usesr'  ee�r(  X   reaverr)  N�r*  h6K�r+  j
  K
�r,  j�  K�r-  j�  K�r.  j?  XU   target.get_weapon() and item_system.weapon_type(target, target.get_weapon()) == 'Axe'r/  �r0  eu}r1  (j�  K�hj  hj  hj  j�  jB  j�  �j�  }r2  (j  Kj  Kuj�  ]r3  j�  ]r4  (j"  N�r5  j$  N�r6  j  K�r7  j'  j(  �r8  j.  K�r9  j0  j1  �r:  j3  N�r;  eu}r<  (j�  K�hX   Greatbowr=  hX	   Steel Bowr>  hX   SPD -2.r?  j�  j`  j�  �j�  }r@  (j  Kj  Kuj�  ]rA  j�  ]rB  (j�  N�rC  j�  N�rD  j  N�rE  j  K�rF  j  K �rG  j  X   BowrH  �rI  j  K�rJ  j'  ]rK  (]rL  (X   LoseUsesOnMiss (T/F)rM  j+  X   Lose uses even on missrN  e]rO  (X   OneLossPerCombat (T/F)rP  j+  X    Doubling doesn't cost extra usesrQ  ee�rR  j
  K �rS  j�  K�rT  j�  K�rU  jo  X   SPD2rV  �rW  j5  }rX  (j7  ]rY  X   FlyingrZ  aj:  G@      j;  K j<  �u�r[  j?  X   'Flying' in unit.tagsr\  �r]  eu}r^  (j�  K�hX   Mini_Bowr_  hX   Mini Bowr`  hX   SPD -1.ra  j�  j`  j�  �j�  }rb  (j  K
j  K
uj�  ]rc  j�  ]rd  (j�  N�re  j�  N�rf  j  N�rg  j  K�rh  j  J�����ri  j  X   Bowrj  �rk  j  K
�rl  j'  ]rm  (]rn  (X   LoseUsesOnMiss (T/F)ro  j+  X   Lose uses even on missrp  e]rq  (X   OneLossPerCombat (T/F)rr  j+  X    Doubling doesn't cost extra usesrs  ee�rt  j
  K�ru  h6K�rv  j�  K�rw  j�  K�rx  j5  }ry  (j7  ]rz  X   Flyingr{  aj:  G@      j;  K j<  �u�r|  jo  X   SPD1r}  �r~  j?  X   'Flying' in unit.tagsr  �r�  eu}r�  (j�  K�hj  hj  hj  j�  j`  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j"  N�r�  j$  N�r�  j  K�r�  j'  j(  �r�  j.  K�r�  j0  j1  �r�  j3  N�r�  eu}r�  (j�  K�hX	   Steel Axer�  hX	   Steel Axer�  hX   SPD -2.r�  j�  j�  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j  K�r�  j  J�����r�  j
  K �r�  j  K�r�  j'  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j+  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j+  X    Doubling doesn't cost extra usesr�  ee�r�  j  N�r�  j  X   Axer�  �r�  h6K�r�  jo  X   SPD2r�  �r�  eu}r�  (j�  K�hX
   EmeraldAxer�  hX   Emerald Axer�  hX$   Doubles the weapon triangle. SPD -1.r�  j�  j�  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j  K�r�  j  J�����r�  j
  K
�r�  j  K�r�  j'  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j+  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j+  X    Doubling doesn't cost extra usesr�  ee�r�  j  N�r�  j  X   Axer�  �r�  h6K�r�  X   double_triangler�  N�r�  jo  X   SPD1r�  �r�  X   warningr�  N�r�  eu}r�  (j�  K�hj  hj  hj  j�  j�  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j"  N�r�  j$  N�r�  j  K�r�  j'  j(  �r�  j.  K�r�  j0  j1  �r�  j3  N�r�  eu}r�  (j�  K�hj�  hj�  hj�  j�  j�  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j  N�r�  j  K�r�  j  K �r�  j  j  �r�  j  K�r�  j'  j  �r�  j
  K �r�  j�  K�r�  j�  K�r�  jo  j  �r�  eu}r�  (j�  K�hX   SapphireLancer�  hX   Sapphire Lancer�  hX$   Doubles the weapon triangle. SPD -1.r�  j�  j�  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j  N�r�  j  K�r�  j  K �r�  j  X   Lancer�  �r�  j  K�r�  j'  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j+  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j+  X    Doubling doesn't cost extra usesr�  ee�r�  h6K�r�  j
  K
�r�  j�  K�r�  j�  K�r�  j�  N�r   jo  X   SPD1r  �r  j�  N�r  eu}r  (j�  K�hj  hj  hj  j�  j�  j�  �j�  }r  (j  Kj  Kuj�  ]r  j�  ]r  (j"  N�r  j$  N�r	  j  K�r
  j'  j(  �r  j.  K�r  j0  j1  �r  j3  N�r  eu}r  (j�  K�hX   Elixirr  hX   Elixirr  hX,   Fully recovers HP. Unit can act after using.r  j�  j�  j�  �j�  }r  (j  Kj  Kuj�  ]r  j�  ]r  (j"  N�r  j$  N�r  j  K�r  j'  ]r  ]r  (X   LoseUsesOnMiss (T/F)r  j+  X   Lose uses even on missr  ea�r  j.  Kc�r  j0  ]r  (K`K�K�e�r   j3  N�r!  eu}r"  (j�  K�hX   Silver Swordr#  hX   Silver Swordr$  hhj�  j�  j�  �j�  }r%  (j  K
j  K
uj�  ]r&  j�  ]r'  (j�  N�r(  j�  N�r)  j�  K�r*  j�  K�r+  j  K�r,  j  K
�r-  j
  K �r.  j  N�r/  j  X   Swordr0  �r1  j  K
�r2  j'  ]r3  (]r4  (X   LoseUsesOnMiss (T/F)r5  j+  X   Lose uses even on missr6  e]r7  (X   OneLossPerCombat (T/F)r8  j+  X    Doubling doesn't cost extra usesr9  ee�r:  eu}r;  (j�  K�hX   Silver Lancer<  hX   Silver Lancer=  hhj�  j�  j�  �j�  }r>  (j  K
j  K
uj�  ]r?  j�  ]r@  (j�  N�rA  j�  N�rB  j�  K�rC  j�  K�rD  j  K�rE  j  K�rF  j
  K �rG  j  N�rH  j  X   LancerI  �rJ  j  K
�rK  j'  ]rL  (]rM  (X   LoseUsesOnMiss (T/F)rN  j+  X   Lose uses even on missrO  e]rP  (X   OneLossPerCombat (T/F)rQ  j+  X    Doubling doesn't cost extra usesrR  ee�rS  eu}rT  (j�  K�hX   TheReclaimerrU  hX   <blue>The Reclaimer<blue>rV  hX6   Cavalier line only.
50% Lifelink. Effective Vs. Armor.rW  j�  j�  j�  �j�  }rX  j�  ]rY  j�  ]rZ  (j�  N�r[  j�  N�r\  j�  K�r]  j�  K�r^  j  K�r_  j  J�����r`  j
  K
�ra  j  N�rb  j  X   Lancerc  �rd  X	   prf_classre  ]rf  (X   Cavalierrg  X   Paladinrh  X   Great_Knightri  e�rj  j5  }rk  (j7  ]rl  X   Armorrm  aj:  G@      j;  K j<  �u�rn  j  X   bluero  �rp  X   lifelinkrq  G?�      �rr  eu}rs  (j�  K�hX
   TheDefilerrt  hX   <blue>The Defiler</blue>ru  hXF   Knight line only.
Inflicts half damage on miss. Effective Vs. Clergy. rv  j�  j  j�  �j�  }rw  j�  ]rx  j�  ]ry  (j�  N�rz  j�  N�r{  j�  K�r|  j�  K�r}  j  K�r~  j  J�����r  j
  K�r�  j  N�r�  j  X   Axer�  �r�  je  ]r�  (X   Knightr�  X   Generalr�  X   Great_Knightr�  e�r�  j5  }r�  (j7  ]r�  X   Clergyr�  aj:  G@      j;  K j<  �u�r�  j  X   bluer�  �r�  X   damage_on_missr�  G?�      �r�  eu}r�  (j�  K�hj�  hj�  hhj�  j  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j  K�r�  j  K �r�  j  N�r�  j  j�  �r�  j
  K �r�  eu}r�  (j�  K�hX   MultiPhysicr�  hX   Multiphysicr�  hXT   Restores an ally's HP from up to (MAG * 2) tiles away. Casts twice. Chapter Uses: 2.r�  j�  j  j�  �j�  }r�  (X   c_usesr�  KX   starting_c_usesr�  Kuj�  ]r�  j�  ]r�  (X   spellr�  N�r�  j$  N�r�  j  X   Staffr�  �r�  jG  N�r�  h6K�r�  hK!�r�  j0  ]r�  (K`K�K�e�r�  X
   magic_healr�  K�r�  j�  K�r�  X   max_equation_ranger�  X   MAGIC_RANGEr�  �r�  jI  N�r�  j�  K�r�  j'  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j+  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j+  X    Doubling doesn't cost extra usesr�  ee�r�  X   multi_targetr�  K�r�  X   allow_same_targetr�  N�r�  jO  X   Healr�  �r�  X   ignore_line_of_sightr�  N�r�  eu}r�  (j�  K�hj#  hj$  hhj�  j7  j�  �j�  }r�  (j  K
j  K
uj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j  K�r�  j  K
�r�  j
  K �r�  j  N�r�  j  j0  �r�  j  K
�r�  j'  j3  �r�  eu}r�  (j�  K�hj�  hj�  hj�  j�  jM  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j  K�r�  j  J�����r�  j
  K �r�  j  N�r�  j  j�  �r�  j  K�r�  j'  j�  �r�  j�  N�r�  h6K�r�  jo  j�  �r�  eu}r�  (j�  K�hX   Hand Axer�  hX   Hand Axer�  hX   Cannot Counter. SPD -3.r�  j�  jb  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j  K�r�  j  J�����r�  j
  K �r�  j  K�r�  j'  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j+  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j+  X    Doubling doesn't cost extra usesr�  ee�r   j  N�r  j  X   Axer  �r  j�  N�r  h6K�r  jo  X   SPD3r  �r  eu}r  (j�  K�hj'  hj(  hhj�  jv  j�  �j�  }r	  j�  ]r
  j�  ]r  (j�  N�r  j�  N�r  j�  K�r  j�  K�r  j  K�r  j  K
�r  j  N�r  j  j3  �r  j5  }r  (j7  j8  j:  G@      j;  K j<  �u�r  j
  K �r  j?  j@  �r  eu}r  (j�  K�hj�  hj�  hhj�  jv  j�  �j�  }r  j�  ]r  j�  ]r  (j�  N�r  j�  N�r  j�  K�r  j�  K�r  j  K�r   j  K
�r!  j  N�r"  j  j  �r#  j
  K �r$  eu}r%  (j�  K�hX   EtherealBlader&  hX   Ethereal Blader'  hX   Targets RES.r(  j�  jv  j�  �j�  }r)  (j  K
j  K
uj�  ]r*  j�  ]r+  (j�  N�r,  j�  N�r-  j�  K�r.  j�  K�r/  j  K�r0  j  K�r1  j
  K�r2  j  K
�r3  j'  ]r4  (]r5  (X   LoseUsesOnMiss (T/F)r6  j+  X   Lose uses even on missr7  e]r8  (X   OneLossPerCombat (T/F)r9  j+  X    Doubling doesn't cost extra usesr:  ee�r;  j  N�r<  j  X   Swordr=  �r>  X   alternate_resist_formular?  X   MAGIC_DEFENSEr@  �rA  h6K�rB  eu}rC  (j�  K�hj�  hj�  hj�  j�  j�  j�  �j�  }rD  (j  Kj  Kuj�  ]rE  j�  ]rF  (j�  N�rG  j�  N�rH  j  N�rI  j  K�rJ  j  K �rK  j  j  �rL  j  K�rM  j'  j  �rN  j
  K �rO  j�  K�rP  j�  K�rQ  jo  j  �rR  eu}rS  (j�  K�hX   WardensCleaverrT  hX   <blue>Warden's Cleaver<blue>rU  hX&   While equipped unit cannot be doubled.rV  j�  j�  j�  �j�  }rW  j�  ]rX  j�  ]rY  (j�  N�rZ  j�  N�r[  j�  K�r\  j�  K�r]  j  K�r^  j  K�r_  j
  K
�r`  j  N�ra  j  X   Axerb  �rc  j  X   bluerd  �re  jo  X   Wardenrf  �rg  eu}rh  (j�  K�hX
   BloodLanceri  hX   Blood Lancerj  hX   Has Lifelink. SPD -1.rk  j�  j�  j�  �j�  }rl  (j  K
j  K
uj�  ]rm  j�  ]rn  (j�  N�ro  j�  N�rp  j  N�rq  j  K�rr  j  J�����rs  j  X   Lancert  �ru  j  K
�rv  j'  ]rw  (]rx  (X   LoseUsesOnMiss (T/F)ry  j+  X   Lose uses even on missrz  e]r{  (X   OneLossPerCombat (T/F)r|  j+  X    Doubling doesn't cost extra usesr}  ee�r~  j
  K�r  j�  K�r�  j�  K�r�  jq  G?�      �r�  h6K�r�  jo  X   SPD1r�  �r�  eu}r�  (j�  K�hj&  hj'  hj(  j�  j�  j�  �j�  }r�  (j  K
j  K
uj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j  K�r�  j  K�r�  j
  K�r�  j  K
�r�  j'  j4  �r�  j  N�r�  j  j=  �r�  j?  j@  �r�  h6K�r�  eu}r�  (j�  K�hX   AngelFeatherr�  hX   <yellow>Angel Feather</yellow>r�  hX&   Gives unit +10% growths. Cannot stack.r�  j�  j�  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j"  N�r�  j$  N�r�  j  K�r�  j'  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j+  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j+  X    Doubling doesn't cost extra usesr�  ee�r�  X   usable_in_baser�  N�r�  j  X   yellowr�  �r�  j0  ]r�  (K�K�K�e�r�  jK  X   AngelFeatherr�  �r�  j�  K �r�  j�  K �r�  X   eval_availabler�  X#   not has_skill(unit, 'AngelFeather')r�  �r�  X   event_on_hitr�  X   Global AngelFeatherr�  �r�  eu}r�  (j�  K�hX   HowlingBlader�  hX   <blue>Howling Blade</blue>r�  hX4   Grants Distant Counter. Deals magic damage at range.r�  j�  j�  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j  K�r�  j  K�r�  j
  K
�r�  j  N�r�  j  X   Swordr�  �r�  j  X   bluer�  �r�  X   magic_at_ranger�  N�r�  jo  X   DistantCounterr�  �r�  eu}r�  (j�  K�hX   Macer�  hX   Macer�  hX   Cannot be Countered. SPD -2.r�  j�  j�  j�  �j�  }r�  (j  K
j  K
uj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j  N�r�  j  K�r�  j  J�����r�  j  X   Axer�  �r�  j  K
�r�  j'  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j+  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j+  X    Doubling doesn't cost extra usesr�  ee�r�  j
  K�r�  h6K�r�  j�  K�r�  j�  K�r�  X   cannot_be_counteredr�  N�r�  jo  X   SPD2r�  �r�  eu}r�  (j�  K�hj�  hj�  hj�  j�  j�  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j"  N�r�  j$  N�r�  j  K�r�  j'  j�  �r�  j�  N�r�  j  j�  �r�  j0  j�  �r�  jK  j�  �r�  j�  K �r�  j�  K �r�  j�  j�  �r�  j�  j�  �r�  eu}r�  (j�  K�hX   LunarArcr   hX	   Lunar Arcr  hX%   Ignores DEF and grants Close Counter.r  j�  j�  j�  �j�  }r  j�  ]r  j�  ]r  (jo  X   CloseCounterr  �r  j�  N�r  j�  N�r	  j  N�r
  j  K�r  j?  X   ZEROr  �r  j  K�r  j  X   Bowr  �r  j
  K
�r  j�  K�r  j�  K�r  j5  }r  (j7  ]r  X   Flyingr  aj:  G@      j;  K j<  �u�r  j?  X   'Flying' in unit.tagsr  �r  eu}r  (j�  K�hj�  hj�  hj�  j�  j�  j�  �j�  }r  (j  Kj  Kuj�  ]r  j�  ]r  (j�  N�r  j�  N�r  j�  K�r   j�  K�r!  j  K�r"  j  J�����r#  j
  K
�r$  j  K�r%  j'  j�  �r&  j  N�r'  j  j�  �r(  h6K�r)  j�  N�r*  jo  j�  �r+  j�  N�r,  eu}r-  (j�  K�hj�  hj�  hj�  j�  j�  j�  �j�  }r.  (j  Kj  Kuj�  ]r/  j�  ]r0  (j"  N�r1  j$  N�r2  j  K�r3  j'  j�  �r4  j�  N�r5  j  j�  �r6  j0  j�  �r7  jK  j�  �r8  j�  K �r9  j�  K �r:  j�  j�  �r;  j�  j�  �r<  eu}r=  (j�  K�hX   Flairr>  hX   <purple>Flair</purple>r?  hX`   Elwynn Only.
Steals any unequipped item from target.
Can't miss, double, kill, or target bosses.r@  j�  h�j�  �j�  }rA  (j  K
j  K
X   target_itemrB  Nuj�  ]rC  j�  ]rD  (j�  N�rE  j�  N�rF  j�  K�rG  j�  K�rH  j  K�rI  j  K
�rJ  j'  ]rK  (]rL  (X   LoseUsesOnMiss (T/F)rM  j+  X   Lose uses even on missrN  e]rO  (X   OneLossPerCombat (T/F)rP  j+  X    Doubling doesn't cost extra usesrQ  ee�rR  j  N�rS  j  X   LightrT  �rU  j0  ]rV  (K�K�K e�rW  jG  N�rX  j�  ]rY  X   ElwynnrZ  a�r[  j  X   purpler\  �r]  jK  X	   NonLethalr^  �r_  X   stealr`  N�ra  jO  X   Glimmerrb  �rc  X   eval_target_restrict_2rd  X   'Boss' not in target.tagsre  �rf  eu}rg  (j�  K�hX   Shoverh  hX   <green>Shove</>ri  hX!   Free action. Shove target 1 tile.rj  j�  j�  j�  �j�  }rk  (X   cooldownrl  K X   starting_cooldownrm  Kuj�  ]rn  j�  ]ro  (X   target_unitrp  N�rq  j�  K�rr  j�  K�rs  jd  X   target.team == 'player'rt  �ru  j3  N�rv  jl  K�rw  X   shove_on_end_combatrx  K�ry  eu}rz  (j�  K�hX   Bolsterr{  hX   Bolsterr|  hX/   Grants max HP +5 for 2 turns and restores 5 HP.r}  j�  j�  j�  �j�  }r~  j�  ]r  j�  ]r�  (j$  N�r�  j�  K�r�  j�  K�r�  X   map_hit_sfxr�  X   HealInitr�  �r�  j0  ]r�  (K K�Ke�r�  j�  X   Global SkillCorridonBolsterr�  �r�  hK�r�  eu}r�  (j�  K�hX   Repairr�  hX   Repairr�  hXE   Repairs selected weapon in target's inventory. Does not affect tomes.r�  j�  j�  j�  �j�  }r�  jB  Nsj�  ]r�  j�  ]r�  (j�  K�r�  j�  K�r�  j$  N�r�  X   repairr�  N�r�  X   never_use_battle_animationr�  N�r�  X   map_cast_sfxr�  X   Hammerner�  �r�  j0  ]r�  (K!K�Kke�r�  hK�r�  eu}r�  (j�  K�hjh  hji  hjj  j�  j�  j�  �j�  }r�  (jl  K jm  Kuj�  ]r�  j�  ]r�  (jp  N�r�  j�  K�r�  j�  K�r�  jd  jt  �r�  j3  N�r�  jl  K�r�  jx  K�r�  eu}r�  (j�  K�hjh  hji  hjj  j�  j�  j�  �j�  }r�  (jl  K jm  Kuj�  ]r�  j�  ]r�  (jp  N�r�  j�  K�r�  j�  K�r�  jd  jt  �r�  j3  N�r�  jl  K�r�  jx  K�r�  eu}r�  (j�  K�hjh  hji  hjj  j�  j`  j�  �j�  }r�  (jl  K jm  Kuj�  ]r�  j�  ]r�  (jp  N�r�  j�  K�r�  j�  K�r�  jd  jt  �r�  j3  N�r�  jl  K�r�  jx  K�r�  eu}r�  (j�  K�hjh  hji  hjj  j�  jB  j�  �j�  }r�  (jl  K jm  Kuj�  ]r�  j�  ]r�  (jp  N�r�  j�  K�r�  j�  K�r�  jd  jt  �r�  j3  N�r�  jl  K�r�  jx  K�r�  eu}r�  (j�  K�hjh  hji  hjj  j�  j%  j�  �j�  }r�  (jl  K jm  Kuj�  ]r�  j�  ]r�  (jp  N�r�  j�  K�r�  j�  K�r�  jd  jt  �r�  j3  N�r�  jl  K�r�  jx  K�r�  eu}r�  (j�  K�hX   Healr�  hX   <blue>Heal</>r�  hX   Restores (User's MAG * 2) HP.r�  j�  j%  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j$  N�r�  j�  K�r�  j�  K�r�  j  X   Staffr�  �r�  hK�r�  h6K�r�  j0  ]r�  (K`K�K�e�r�  jI  N�r�  jO  X   Healr�  �r�  X   equation_healr�  X   MAG2r�  �r�  eu}r�  (j�  K�hX   Exchanger�  hX   Exchanger�  hX:   Free action. Trade with allies up to (MAG * 2) tiles away.r�  j�  j%  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j$  N�r�  j�  K�r�  j�  X   MAG2r�  �r�  j0  ]r�  (K�K�Ke�r�  X   trader�  N�r�  X   menu_after_combatr�  N�r�  j3  N�r 	  j�  N�r	  j�  X   Warpr	  �r	  X   map_cast_animr	  X   Warpr	  �r	  eu}r	  (j�  K�hjh  hji  hjj  j�  j  j�  �j�  }r	  (jl  K jm  Kuj�  ]r		  j�  ]r
	  (jp  N�r	  j�  K�r	  j�  K�r	  jd  jt  �r	  j3  N�r	  jl  K�r	  jx  K�r	  eu}r	  (j�  K�hjh  hji  hjj  j�  h�j�  �j�  }r	  (jl  K jm  Kuj�  ]r	  j�  ]r	  (jp  N�r	  j�  K�r	  j�  K�r	  jd  jt  �r	  j3  N�r	  jl  K�r	  jx  K�r	  eu}r	  (j�  K�hjh  hji  hjj  j�  h�j�  �j�  }r	  (jl  K jm  Kuj�  ]r	  j�  ]r 	  (jp  N�r!	  j�  K�r"	  j�  K�r#	  jd  jt  �r$	  j3  N�r%	  jl  K�r&	  jx  K�r'	  eu}r(	  (j�  K�hX   Treatr)	  hX   Treatr*	  hX*   Free Action. Restores (User's MAG + 2) HP.r+	  j�  h�j�  �j�  }r,	  (jl  K jm  Kuj�  ]r-	  j�  ]r.	  (j$  N�r/	  j�  N�r0	  j�  K�r1	  j�  K�r2	  j�  X   TREATr3	  �r4	  j3  N�r5	  hK
�r6	  jl  K�r7	  eu}r8	  (j�  K�hjh  hji  hjj  j�  h�j�  �j�  }r9	  (jl  K jm  Kuj�  ]r:	  j�  ]r;	  (jp  N�r<	  j�  K�r=	  j�  K�r>	  jd  jt  �r?	  j3  N�r@	  jl  K�rA	  jx  K�rB	  eu}rC	  (j�  K�hX   TriprD	  hX   TriprE	  hX/   Free Action. Set target's move to 0 for 1 turn.rF	  j�  h�j�  �j�  }rG	  (jl  K jm  Kuj�  ]rH	  j�  ]rI	  (j�  N�rJ	  j�  K�rK	  j�  K�rL	  jK  X   TrippedrM	  �rN	  j3  N�rO	  jl  K�rP	  eu}rQ	  (j�  K�hjh  hji  hjj  j�  h�j�  �j�  }rR	  (jl  K jm  Kuj�  ]rS	  j�  ]rT	  (jp  N�rU	  j�  K�rV	  j�  K�rW	  jd  jt  �rX	  j3  N�rY	  jl  K�rZ	  jx  K�r[	  eu}r\	  (j�  M hjh  hji  hjj  j�  huj�  �j�  }r]	  (jl  K jm  Kuj�  ]r^	  j�  ]r_	  (jp  N�r`	  j�  K�ra	  j�  K�rb	  jd  jt  �rc	  j3  N�rd	  jl  K�re	  jx  K�rf	  eu}rg	  (j�  Mhjh  hji  hjj  j�  hj�  �j�  }rh	  (jl  K jm  Kuj�  ]ri	  j�  ]rj	  (jp  N�rk	  j�  K�rl	  j�  K�rm	  jd  jt  �rn	  j3  N�ro	  jl  K�rp	  jx  K�rq	  euehE]rr	  (}rs	  (j�  K�hX   HeartofFirert	  j�  hj�  }ru	  X   initiator_nidrv	  NX   subskillrw	  Nu}rx	  (j�  K�hX   HeartofGalesry	  j�  huj�  }rz	  jv	  Njw	  Nu}r{	  (j�  K�hX   HeartofLightningr|	  j�  h�j�  }r}	  jv	  Njw	  Nu}r~	  (j�  K�hX   HeartofFrostr	  j�  h�j�  }r�	  jv	  Njw	  Nu}r�	  (j�  MhX   HeartofRadiancer�	  j�  h�j�  }r�	  jv	  Njw	  Nu}r�	  (j�  M
hX   HeartofDarknessr�	  j�  j  j�  }r�	  jv	  Njw	  Nu}r�	  (j�  MhX   SPD1r�	  j�  jB  j�  }r�	  jv	  Njw	  Nu}r�	  (j�  MhX   SPD2r�	  j�  j`  j�  }r�	  jv	  Njw	  Nu}r�	  (j�  M%hj�	  j�  j�  j�  }r�	  jv	  Njw	  Nu}r�	  (j�  M+hj�	  j�  j�  j�  }r�	  jv	  Njw	  Nu}r�	  (j�  MDhj�	  j�  jM  j�  }r�	  jv	  Njw	  Nu}r�	  (j�  MGhX   SPD3r�	  j�  jb  j�  }r�	  jv	  Njw	  Nu}r�	  (j�  MLhj�	  j�  j�  j�  }r�	  jv	  Njw	  Nu}r�	  (j�  MQhX   Wardenr�	  j�  j�  j�  }r�	  jv	  Njw	  Nu}r�	  (j�  MUhX   DistantCounterr�	  j�  j�  j�  }r�	  jv	  Njw	  Nu}r�	  (j�  MYhX   CloseCounterr�	  j�  j�  j�  }r�	  jv	  Njw	  Nu}r�	  (j�  K�hX   NoEXPr�	  j�  hj�  }r�	  jv	  Njw	  Nu}r�	  (j�  K�hX   Lootr�	  j�  hj�  }r�	  jv	  Njw	  Nu}r�	  (j�  K�hX   Shover�	  j�  hj�  }r�	  X   ability_item_uidr�	  Msjv	  Njw	  Nu}r�	  (j�  K�hX   Flailr�	  j�  hj�  }r�	  jv	  Njw	  Nu}r�	  (j�  K�hj�	  j�  huj�  }r�	  jv	  Njw	  Nu}r�	  (j�  K�hj�	  j�  huj�  }r�	  jv	  Njw	  Nu}r�	  (j�  K�hj�	  j�  huj�  }r�	  j�	  M sjv	  Njw	  Nu}r�	  (j�  K�hX   RiskyBusinessr�	  j�  huj�  }r�	  jv	  Njw	  Nu}r�	  (j�  K�hj�	  j�  h�j�  }r�	  jv	  Njw	  Nu}r�	  (j�  K�hj�	  j�  h�j�  }r�	  jv	  Njw	  Nu}r�	  (j�  K�hj�	  j�  h�j�  }r�	  j�	  K�sjv	  Njw	  Nu}r�	  (j�  K�hX   Bungler�	  j�  h�j�  }r�	  jv	  Njw	  Nu}r�	  (j�  K�hj�	  j�  h�j�  }r�	  jv	  Njw	  Nu}r�	  (j�  K�hj�	  j�  h�j�  }r�	  jv	  Njw	  Nu}r�	  (j�  K�hj�	  j�  h�j�  }r�	  j�	  K�sjv	  Njw	  Nu}r�	  (j�  K�hX   Tripr�	  j�  h�j�  }r�	  j�	  K�sjv	  Njw	  Nu}r�	  (j�  K�hj�	  j�  h�j�  }r�	  jv	  Njw	  Nu}r�	  (j�  M hj�	  j�  h�j�  }r�	  jv	  Njw	  Nu}r�	  (j�  MhX	   Protectorr�	  j�  h�j�  }r�	  jv	  Njw	  Nu}r�	  (j�  Mhj�	  j�  h�j�  }r�	  j�	  K�sjv	  Njw	  Nu}r�	  (j�  MhX   ElwynnSkillr�	  j�  h�j�  }r�	  j�	  K�sjv	  Njw	  Nu}r�	  (j�  Mhj�	  j�  h�j�  }r�	  jv	  Njw	  Nu}r�	  (j�  Mhj�	  j�  h�j�  }r�	  jv	  Njw	  Nu}r�	  (j�  Mhj�	  j�  h�j�  }r�	  jv	  Njw	  Nu}r�	  (j�  Mhj�	  j�  h�j�  }r�	  j�	  K�sjv	  Njw	  Nu}r�	  (j�  M	hX   Potshotr�	  j�  h�j�  }r�	  jv	  Njw	  Nu}r�	  (j�  Mhj�	  j�  j  j�  }r�	  jv	  Njw	  Nu}r�	  (j�  Mhj�	  j�  j  j�  }r�	  jv	  Njw	  Nu}r�	  (j�  Mhj�	  j�  j  j�  }r�	  jv	  Njw	  Nu}r�	  (j�  Mhj�	  j�  j  j�  }r�	  j�	  K�sjv	  Njw	  Nu}r�	  (j�  MhX   Enthrallr�	  j�  j  j�  }r�	  jv	  Njw	  Nu}r�	  (j�  Mhj�	  j�  j%  j�  }r�	  jv	  Njw	  Nu}r�	  (j�  Mhj�	  j�  j%  j�  }r�	  jv	  Njw	  Nu}r�	  (j�  Mhj�	  j�  j%  j�  }r�	  j�	  K�sjv	  Njw	  Nu}r�	  (j�  MhX   DivineConduitr�	  j�  j%  j�  }r�	  j�	  K�sjv	  Njw	  Nu}r�	  (j�  MhX   Exchanger�	  j�  j%  j�  }r�	  (X   charger�	  KX   total_charger�	  Kj�	  K�ujv	  Njw	  Nu}r�	  (j�  Mhj�	  j�  jB  j�  }r�	  jv	  Njw	  Nu}r�	  (j�  Mhj�	  j�  jB  j�  }r�	  jv	  Njw	  Nu}r�	  (j�  Mhj�	  j�  jB  j�  }r�	  jv	  Njw	  Nu}r�	  (j�  Mhj�	  j�  jB  j�  }r 
  j�	  K�sjv	  Njw	  Nu}r
  (j�  MhX
   Instructorr
  j�  jB  j�  }r
  jv	  Njw	  Nu}r
  (j�  Mhj�	  j�  j`  j�  }r
  jv	  Njw	  Nu}r
  (j�  Mhj�	  j�  j`  j�  }r
  jv	  Njw	  Nu}r
  (j�  Mhj�	  j�  j`  j�  }r	
  jv	  Njw	  Nu}r

  (j�  Mhj�	  j�  j`  j�  }r
  j�	  K�sjv	  Njw	  Nu}r
  (j�  M hj�	  j�  j�  j�  }r
  jv	  Njw	  Nu}r
  (j�  M!hj�	  j�  j�  j�  }r
  jv	  Njw	  Nu}r
  (j�  M"hj�	  j�  j�  j�  }r
  jv	  Njw	  Nu}r
  (j�  M#hj�	  j�  j�  j�  }r
  j�	  K�sjv	  Njw	  Nu}r
  (j�  M$hX   Nemophilistr
  j�  j�  j�  }r
  jv	  Njw	  Nu}r
  (j�  M&hj�	  j�  j�  j�  }r
  jv	  Njw	  Nu}r
  (j�  M'hj�	  j�  j�  j�  }r
  jv	  Njw	  Nu}r
  (j�  M(hj�	  j�  j�  j�  }r
  jv	  Njw	  Nu}r
  (j�  M)hj�	  j�  j�  j�  }r
  j�	  K�sjv	  Njw	  Nu}r
  (j�  M*hX   Beachcomberr 
  j�  j�  j�  }r!
  jv	  Njw	  Nu}r"
  (j�  M,hj�	  j�  j�  j�  }r#
  jv	  Njw	  Nu}r$
  (j�  M-hj�	  j�  j�  j�  }r%
  jv	  Njw	  Nu}r&
  (j�  M.hj�	  j�  j�  j�  }r'
  jv	  Njw	  Nu}r(
  (j�  M/hj�	  j�  j�  j�  }r)
  j�	  K�sjv	  Njw	  Nu}r*
  (j�  M0hX   Bolsterr+
  j�  j�  j�  }r,
  j�	  K�sjv	  Njw	  Nu}r-
  (j�  M1hX   Repairr.
  j�  j�  j�  }r/
  j�	  K�sjv	  Njw	  Nu}r0
  (j�  M2hj�	  j�  j�  j�  }r1
  jv	  Njw	  Nu}r2
  (j�  M3hj�	  j�  j�  j�  }r3
  jv	  Njw	  Nu}r4
  (j�  M4hj�	  j�  j�  j�  }r5
  jv	  Njw	  Nu}r6
  (j�  M5hX   AspiringLeaderr7
  j�  j�  j�  }r8
  jv	  Njw	  Nu}r9
  (j�  M6hX   Cantor:
  j�  j�  j�  }r;
  jv	  Njw	  Nu}r<
  (j�  M7hj�	  j�  j  j�  }r=
  jv	  Njw	  Nu}r>
  (j�  M8hj�	  j�  j  j�  }r?
  jv	  Njw	  Nu}r@
  (j�  M9hX   AnticritrA
  j�  j  j�  }rB
  jv	  Njw	  Nu}rC
  (j�  M:hX   DivineBlessingrD
  j�  j  j�  }rE
  jv	  Njw	  Nu}rF
  (j�  M;hj�	  j�  j  j�  }rG
  jv	  Njw	  Nu}rH
  (j�  M<hj�	  j�  j  j�  }rI
  jv	  Njw	  Nu}rJ
  (j�  M=hX   FlyingrK
  j�  j  j�  }rL
  jv	  Njw	  Nu}rM
  (j�  M>hX	   BloodlustrN
  j�  j  j�  }rO
  jv	  Njw	  Nu}rP
  (j�  M?hj�	  j�  j7  j�  }rQ
  jv	  Njw	  Nu}rR
  (j�  M@hj�	  j�  j7  j�  }rS
  jv	  Njw	  Nu}rT
  (j�  MAhj�	  j�  jM  j�  }rU
  jv	  Njw	  Nu}rV
  (j�  MBhj�	  j�  jM  j�  }rW
  jv	  Njw	  Nu}rX
  (j�  MChjK
  j�  jM  j�  }rY
  jv	  Njw	  Nu}rZ
  (j�  MEhj�	  j�  jb  j�  }r[
  jv	  Njw	  Nu}r\
  (j�  MFhj�	  j�  jb  j�  }r]
  jv	  Njw	  Nu}r^
  (j�  MHhj�	  j�  jv  j�  }r_
  jv	  Njw	  Nu}r`
  (j�  MIhj�	  j�  jv  j�  }ra
  jv	  Njw	  Nu}rb
  (j�  MJhj�	  j�  j�  j�  }rc
  jv	  Njw	  Nu}rd
  (j�  MKhj�	  j�  j�  j�  }re
  jv	  Njw	  Nu}rf
  (j�  MMhj�	  j�  j�  j�  }rg
  jv	  Njw	  Nu}rh
  (j�  MNhj�	  j�  j�  j�  }ri
  jv	  Njw	  Nu}rj
  (j�  MOhjA
  j�  j�  j�  }rk
  jv	  Njw	  Nu}rl
  (j�  MPhj:
  j�  j�  j�  }rm
  jv	  Njw	  Nu}rn
  (j�  MRhj�	  j�  j�  j�  }ro
  jv	  Njw	  Nu}rp
  (j�  MShj�	  j�  j�  j�  }rq
  jv	  Njw	  Nu}rr
  (j�  MThjA
  j�  j�  j�  }rs
  jv	  Njw	  Nu}rt
  (j�  MVhj�	  j�  j�  j�  }ru
  jv	  Njw	  Nu}rv
  (j�  MWhj�	  j�  j�  j�  }rw
  jv	  Njw	  Nu}rx
  (j�  MXhjA
  j�  j�  j�  }ry
  jv	  Njw	  Nu}rz
  (j�  MZhX   Avoid10r{
  j�  hj�  }r|
  jv	  Njw	  Nu}r}
  (j�  M[hX   Avoid5r~
  j�  huj�  }r
  jv	  Njw	  Nu}r�
  (j�  M\hj~
  j�  h�j�  }r�
  jv	  Njw	  Nu}r�
  (j�  M]hj~
  j�  h�j�  }r�
  jv	  Njw	  Nu}r�
  (j�  M^hj~
  j�  h�j�  }r�
  jv	  Njw	  Nu}r�
  (j�  M_hj~
  j�  h�j�  }r�
  jv	  Njw	  Nu}r�
  (j�  M`hj{
  j�  j  j�  }r�
  jv	  Njw	  Nu}r�
  (j�  Mahj~
  j�  j%  j�  }r�
  jv	  Njw	  Nu}r�
  (j�  Mbhj~
  j�  jB  j�  }r�
  jv	  Njw	  Nu}r�
  (j�  Mchj~
  j�  j`  j�  }r�
  jv	  Njw	  Nu}r�
  (j�  Mdhj~
  j�  j�  j�  }r�
  jv	  Njw	  Nu}r�
  (j�  Mehj~
  j�  j�  j�  }r�
  jv	  Njw	  Nu}r�
  (j�  Mfhj~
  j�  j�  j�  }r�
  jv	  Njw	  Nu}r�
  (j�  Mghj~
  j�  j�  j�  }r�
  jv	  Njw	  Nu}r�
  (j�  Mhhj{
  j�  j  j�  }r�
  jv	  Njw	  Nu}r�
  (j�  Mihj~
  j�  Nj�  }r�
  jv	  Njw	  Nu}r�
  (j�  Mjhj~
  j�  Nj�  }r�
  jv	  Njw	  Nu}r�
  (j�  Mkhj~
  j�  j�  j�  }r�
  jv	  Njw	  Nu}r�
  (j�  MlhX   SuppressiveFirer�
  j�  j`  j�  }r�
  jv	  Njw	  Nu}r�
  (j�  MmhX   Burningr�
  j�  Nj�  }r�
  jv	  Njw	  Nu}r�
  (j�  Mnhj�
  j�  j�  j�  }r�
  jv	  Njw	  Nu}r�
  (j�  Mohj�
  j�  h�j�  }r�
  jv	  Njw	  Nu}r�
  (j�  Mphj�
  j�  j  j�  }r�
  jv	  Njw	  Nu}r�
  (j�  Mqhj�
  j�  Nj�  }r�
  jv	  Njw	  Nu}r�
  (j�  Mrhj�
  j�  hj�  }r�
  jv	  Njw	  Nu}r�
  (j�  Mshj�
  j�  j�  j�  }r�
  jv	  Njw	  Nu}r�
  (j�  Mthj�
  j�  j�  j�  }r�
  jv	  Njw	  Nu}r�
  (j�  Muhj�
  j�  j�  j�  }r�
  jv	  Njw	  NueX   terrain_status_registryr�
  }r�
  (KKX   Avoid10r�
  �r�
  MZKKX   Avoid5r�
  �r�
  M[KKj�
  �r�
  M\KKj�
  �r�
  M]KK	j�
  �r�
  M^KKj�
  �r�
  M_KKj�
  �r�
  M`KKj�
  �r�
  MaKK
j�
  �r�
  MbKK	j�
  �r�
  McK	Kj�
  �r�
  MdKKj�
  �r�
  MeKKj�
  �r�
  MfKK	j�
  �r�
  MgKKj�
  �r�
  MhKKj�
  �r�
  MiKKj�
  �r�
  MjKKj�
  �r�
  MkKKX   Burningr�
  �r�
  MmKKX   Burningr�
  �r�
  MnKKj�
  �r�
  MoKKj�
  �r�
  MpKKX   Burningr�
  �r�
  MqKKX   Burningr�
  �r�
  MrKKj�
  �r�
  MsK	Kj�
  �r�
  MtKK	j�
  �r�
  MuuX   regionsr�
  ]r�
  (}r�
  (hX   Door1r�
  X   region_typer�
  capp.events.regions
RegionType
r�
  X   eventr�
  �r�
  Rr�
  hKK�r�
  X   sizer�
  ]r�
  (KKeX   sub_nidr�
  X   Doorr�
  X	   conditionr�
  X   unit.can_unlock(region)r�
  X	   time_leftr�
  NX	   only_oncer�
  �X   interrupt_mover�
  �j�  }r�
  u}r�
  (hX   Fire1r�
  j�
  j�
  X   statusr�
  �r�
  Rr�
  hj�  j�
  KK�r�
  j�
  j�
  j�
  X   Truer�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_0r�
  j�
  j�
  hKK	�r�
  j�
  KK�r�
  j�
  X   Garvey_Shotr�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_1r�
  j�
  j�
  hKK	�r�
  j�
  KK�r�
  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r   (hX   Garve_2r  j�
  j�
  hKK
�r  j�
  KK�r  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r  u}r  (hX   Garve_3r  j�
  j�
  hKK	�r  j�
  KK�r  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r	  u}r
  (hX   Garve_4r  j�
  j�
  hKK
�r  j�
  KK�r  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r  u}r  (hX   Garve_5r  j�
  j�
  hKK�r  j�
  KK�r  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r  u}r  (hX   Garve_6r  j�
  j�
  hKK�r  j�
  KK�r  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r  u}r  (hX   Garve_7r  j�
  j�
  hKK�r  j�
  KK�r  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r  u}r  (hX   Garve_8r  j�
  j�
  hKK�r   j�
  KK�r!  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r"  u}r#  (hX   Garve_9r$  j�
  j�
  hKK�r%  j�
  KK�r&  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r'  u}r(  (hX   Garve_10r)  j�
  j�
  hKK	�r*  j�
  KK�r+  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r,  u}r-  (hX   Garve_11r.  j�
  j�
  hKK
�r/  j�
  KK�r0  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r1  u}r2  (hX   Garve_12r3  j�
  j�
  hKK�r4  j�
  KK�r5  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r6  u}r7  (hX   Garve_13r8  j�
  j�
  hKK	�r9  j�
  KK�r:  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r;  u}r<  (hX   Garve_14r=  j�
  j�
  hKK�r>  j�
  KK�r?  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r@  u}rA  (hX   Garve_15rB  j�
  j�
  hKK�rC  j�
  KK�rD  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }rE  u}rF  (hj�  j�
  j�
  hKK�rG  j�
  KK�rH  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }rI  u}rJ  (hh�j�
  j�
  hKK�rK  j�
  KK�rL  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }rM  u}rN  (hX   Fire1_2rO  j�
  j�
  hKK�rP  j�
  KK�rQ  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }rR  u}rS  (hj  j�
  j�
  hKK�rT  j�
  KK�rU  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }rV  u}rW  (hX   Fire2rX  j�
  j�
  hj�  j�
  KK�rY  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }rZ  u}r[  (hh_j�
  j�
  hKK�r\  j�
  KK�r]  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r^  u}r_  (hj�  j�
  j�
  hKK�r`  j�
  KK�ra  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }rb  u}rc  (hj�  j�
  j�
  hK	K�rd  j�
  KK�re  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }rf  u}rg  (hj�  j�
  j�
  hKK	�rh  j�
  KK�ri  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }rj  ueh}rk  (hX   DEBUGrl  hX   DEBUGrm  X   tilemaprn  }ro  (hX   Ch5rp  X   layersrq  ]rr  (}rs  (hX   basert  X   visibleru  �u}rv  (hX   Leftrw  ju  �u}rx  (hX   Rightry  ju  �ueX   weatherrz  ]r{  X
   animationsr|  ]r}  uX
   bg_tilemapr~  NhhX   musicr  }r�  (X   player_phaser�  X   BattleFateMainr�  X   enemy_phaser�  NX   other_phaser�  NX   enemy2_phaser�  NX   player_battler�  X   Attackr�  X   enemy_battler�  NX   other_battler�  NX   enemy2_battler�  NuX	   objectiver�  }r�  (X   simpler�  X   DEBUGr�  X   winr�  X   Don't Crashr�  X   lossr�  X   Crashr�  uh]r�  (hhuh�h�h�h�j  j%  jB  j`  j�  j�  j�  j�  j  j  j7  jM  jb  jv  j�  j�  j�  j�  ej�
  ]r�  (j�
  j�
  j�  h�jO  j  jX  h_j�  j�  j�  eX   unit_groupsr�  ]r�  X	   ai_groupsr�  ]r�  uX
   overworldsr�  ]r�  }r�  (jn  NX   enabled_nodesr�  ]r�  X   enabled_roadsr�  ]r�  hX   0r�  X   overworld_entitiesr�  ]r�  }r�  (hhX   dtyper�  X   PARTYr�  X   dnidr�  hX   on_node_nidr�  NhNh	X   playerr�  uaX   selected_party_nidr�  NX   node_propertiesr�  }r�  X   enabled_menu_optionsr�  }r�  (j�  }r�  X   1r�  }r�  X   2r�  }r�  X   3r�  }r�  X   4r�  }r�  X   5r�  }r�  X   6r�  }r�  X   7r�  }r�  X   8r�  }r�  X   9r�  }r�  X   10r�  }r�  X   11r�  }r�  X   12r�  }r�  X   13r�  }r�  X   14r�  }r�  X   15r�  }r�  X   16r�  }r�  X   17r�  }r�  X   18r�  }r�  X   19r�  }r�  X   20r�  }r�  X   21r�  }r�  X   22r�  }r�  X   23r�  }r�  X   24r�  }r�  X   25r�  }r�  uX   visible_menu_optionsr�  }r�  (j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  j�  }r�  uuaX	   turncountr�  KX   playtimer�  MĐX	   game_varsr�  ccollections
Counter
r   }r  (X   _random_seedr  M�X   _chapter_testr  �X   _convoyr  �X   _custom_options_disabledr  ]r  �aX   _custom_info_descr  ]r  X   Open the Codexr	  aX   _custom_options_eventsr
  ]r  X
   Menu_Codexr  aX   _custom_additional_optionsr  ]r  X   Codexr  aX   _base_bg_namer  X   Hillr  X   _base_musicr  X    Before An Impossible Battlefieldr  X   _base_options_disabledr  ]r  X   _base_options_eventsr  ]r  X   _base_additional_optionsr  ]r  X   _base_transparentr  �u�r  Rr  X
   level_varsr  j   }r  (X
   FireStart1r  KX   FireOrigin1r   j�  X   FireComplete1r!  KX
   FireStart2r"  KX   FireOrigin2r#  j�  X   FireComplete2r$  KX
   FireStart3r%  K X   FireOrigin3r&  K X   FireComplete3r'  K X
   FireStart4r(  K X   FireOrigin4r)  K X   FireComplete4r*  K X
   FireStart5r+  K X   FireOrigin5r,  K X   FireComplete5r-  K X
   FireStart6r.  K X   FireOrigin6r/  K X   FireComplete6r0  K X
   FireStart7r1  K X   FireOrigin7r2  K X   FireComplete7r3  K X
   FireStart8r4  K X   FireOrigin8r5  K X   FireComplete8r6  K X
   FireStart9r7  K X   FireOrigin9r8  K X   FireComplete9r9  K X   FireStart10r:  K X   FireOrigin10r;  K X   FireComplete10r<  K X   FireStart11r=  K X   FireOrigin11r>  K X   FireComplete11r?  K X   FireStart12r@  K X   FireOrigin12rA  K X   FireComplete12rB  K X   SaraidSkillrC  K X   QuinleySkillrD  K X   RoutEnemiesrE  K X   ValidFire1LocsrF  ]rG  (KK�rH  KK�rI  KK�rJ  KK�rK  eX   GarveySpotsrL  KX	   ValidLocsrM  ]rN  (KK�rO  KK	�rP  KK	�rQ  KK
�rR  KK�rS  KK�rT  KK�rU  KK	�rV  KK	�rW  KK
�rX  KK	�rY  KK
�rZ  KK�r[  KK�r\  KK�r]  KK�r^  KK�r_  KK�r`  KK	�ra  KK
�rb  KK�rc  KK	�rd  KK
�re  KK�rf  KK�rg  eX   GarvPositionrh  K X	   whocares1ri  KX   ValidFire2Locsrj  ]rk  (KK�rl  KK�rm  K	K�rn  KK	�ro  eX	   whocares2rp  Ku�rq  Rrr  X   current_moders  }rt  (hX   Normalru  X
   permadeathrv  �h/X   Randomrw  X   enemy_autolevelsrx  K X   enemy_truelevelsry  K X   boss_autolevelsrz  K X   boss_truelevelsr{  K uX   partiesr|  ]r}  }r~  (hhhX   Eirika's Groupr  X
   leader_nidr�  X   Alpinr�  X   party_prep_manage_sort_orderr�  ]r�  X   moneyr�  K X   convoyr�  ]r�  X   bexpr�  K uaX   current_partyr�  hX   stater�  ]r�  (X   freer�  X   status_upkeepr�  X   phase_changer�  e]r�  �r�  X
   action_logr�  ]r�  (X   AddSkillr�  }r�  (X   unitr�  j�  h�r�  X	   initiatorr�  hN�r�  X	   skill_objr�  X   skillr�  K�r�  X   sourcer�  hK��r�  X   source_typer�  hhK�r�  X
   subactionsr�  X   listr�  ]r�  �r�  X   reset_actionr�  X   actionr�  X   ResetUnitVarsr�  }r�  (j�  j�  h�r�  X   old_current_hpr�  hK�r�  X   old_current_manar�  hK �r�  u�r�  �r�  X   did_somethingr�  h��r�  u�r�  j�  }r�  (j�  j�  hu�r�  j�  hN�r�  j�  j�  K��r�  j�  hK��r�  j�  hhK�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  hu�r�  j�  hK
�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  h��r�  j�  hN�r�  j�  j�  K��r�  j�  hK��r�  j�  hhK�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  h��r�  j�  hN�r�  j�  j�  K��r�  j�  hK��r�  j�  hhK�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  h�r�  j�  hN�r�  j�  j�  M�r�  j�  hKĆr�  j�  hhK�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  h�r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  j  �r�  j�  hN�r�  j�  j�  M
�r�  j�  hKȆr�  j�  hhK�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r   j�  }r  (j�  j�  jB  �r  j�  hN�r  j�  j�  M�r  j�  hKʆr  j�  hhK�r  j�  j�  ]r  �r  j�  j�  j�  }r	  (j�  j�  jB  �r
  j�  hK�r  j�  hK �r  u�r  �r  j�  h��r  u�r  j�  }r  (j�  j�  j`  �r  j�  hN�r  j�  j�  M�r  j�  hK͆r  j�  hhK�r  j�  j�  ]r  �r  j�  j�  j�  }r  (j�  j�  j`  �r  j�  hK�r  j�  hK �r  u�r  �r  j�  h��r  u�r   j�  }r!  (j�  j�  j�  �r"  j�  hN�r#  j�  j�  M%�r$  j�  hKІr%  j�  hhK�r&  j�  j�  ]r'  �r(  j�  j�  j�  }r)  (j�  j�  j�  �r*  j�  hK�r+  j�  hK �r,  u�r-  �r.  j�  h��r/  u�r0  j�  }r1  (j�  j�  j�  �r2  j�  hN�r3  j�  j�  M+�r4  j�  hKӆr5  j�  hhK�r6  j�  j�  ]r7  �r8  j�  j�  j�  }r9  (j�  j�  j�  �r:  j�  hK�r;  j�  hK �r<  u�r=  �r>  j�  h��r?  u�r@  j�  }rA  (j�  j�  jM  �rB  j�  hN�rC  j�  j�  MD�rD  j�  hKކrE  j�  hhK�rF  j�  j�  ]rG  �rH  j�  j�  j�  }rI  (j�  j�  jM  �rJ  j�  hK�rK  j�  hK �rL  u�rM  �rN  j�  h��rO  u�rP  j�  }rQ  (j�  j�  jb  �rR  j�  hN�rS  j�  j�  MG�rT  j�  hK߆rU  j�  hhK�rV  j�  j�  ]rW  �rX  j�  j�  j�  }rY  (j�  j�  jb  �rZ  j�  hK�r[  j�  hK �r\  u�r]  �r^  j�  h��r_  u�r`  j�  }ra  (j�  j�  j�  �rb  j�  hN�rc  j�  j�  ML�rd  j�  hK�re  j�  hhK�rf  j�  j�  ]rg  �rh  j�  j�  j�  }ri  (j�  j�  j�  �rj  j�  hK�rk  j�  hK �rl  u�rm  �rn  j�  h��ro  u�rp  j�  }rq  (j�  j�  j�  �rr  j�  hN�rs  j�  j�  MQ�rt  j�  hK�ru  j�  hhK�rv  j�  j�  ]rw  �rx  j�  j�  j�  }ry  (j�  j�  j�  �rz  j�  hK�r{  j�  hK �r|  u�r}  �r~  j�  h��r  u�r�  j�  }r�  (j�  j�  j�  �r�  j�  hN�r�  j�  j�  MU�r�  j�  hK�r�  j�  hhK�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  j�  �r�  j�  hN�r�  j�  j�  MY�r�  j�  hK�r�  j�  hhK�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  h�r�  j�  hN�r�  j�  j�  MZ�r�  j�  hh�r�  j�  hh]�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  h�r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  hu�r�  j�  hN�r�  j�  j�  M[�r�  j�  hhv�r�  j�  hh]�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  hu�r�  j�  hK
�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  h��r�  j�  hN�r�  j�  j�  M\�r�  j�  hh��r�  j�  hh]�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  h��r�  j�  hN�r�  j�  j�  M]�r�  j�  hh��r�  j�  hh]�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  hˆr�  j�  hN�r�  j�  j�  M^�r�  j�  hh̆r�  j�  hh]�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  hˆr�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  h�r�  j�  hN�r�  j�  j�  M_�r�  j�  hh��r�  j�  hh]�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  h�r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r   j�  }r  (j�  j�  j  �r  j�  hN�r  j�  j�  M`�r  j�  hj  �r  j�  hh]�r  j�  j�  ]r  �r  j�  j�  j�  }r	  (j�  j�  j  �r
  j�  hK�r  j�  hK �r  u�r  �r  j�  h��r  u�r  j�  }r  (j�  j�  j%  �r  j�  hN�r  j�  j�  Ma�r  j�  hj&  �r  j�  hh]�r  j�  j�  ]r  �r  j�  j�  j�  }r  (j�  j�  j%  �r  j�  hK�r  j�  hK �r  u�r  �r  j�  h��r  u�r   j�  }r!  (j�  j�  jB  �r"  j�  hN�r#  j�  j�  Mb�r$  j�  hjC  �r%  j�  hh]�r&  j�  j�  ]r'  �r(  j�  j�  j�  }r)  (j�  j�  jB  �r*  j�  hK�r+  j�  hK �r,  u�r-  �r.  j�  h��r/  u�r0  j�  }r1  (j�  j�  j`  �r2  j�  hN�r3  j�  j�  Mc�r4  j�  hja  �r5  j�  hh]�r6  j�  j�  ]r7  �r8  j�  j�  j�  }r9  (j�  j�  j`  �r:  j�  hK�r;  j�  hK �r<  u�r=  �r>  j�  h��r?  u�r@  j�  }rA  (j�  j�  j�  �rB  j�  hN�rC  j�  j�  Md�rD  j�  hj�  �rE  j�  hh]�rF  j�  j�  ]rG  �rH  j�  j�  j�  }rI  (j�  j�  j�  �rJ  j�  hK�rK  j�  hK �rL  u�rM  �rN  j�  h��rO  u�rP  j�  }rQ  (j�  j�  j�  �rR  j�  hN�rS  j�  j�  Me�rT  j�  hj�  �rU  j�  hh]�rV  j�  j�  ]rW  �rX  j�  j�  j�  }rY  (j�  j�  j�  �rZ  j�  hK�r[  j�  hK �r\  u�r]  �r^  j�  h��r_  u�r`  j�  }ra  (j�  j�  j�  �rb  j�  hN�rc  j�  j�  Mf�rd  j�  hj�  �re  j�  hh]�rf  j�  j�  ]rg  �rh  j�  j�  j�  }ri  (j�  j�  j�  �rj  j�  hK�rk  j�  hK �rl  u�rm  �rn  j�  h��ro  u�rp  j�  }rq  (j�  j�  j�  �rr  j�  hN�rs  j�  j�  Mg�rt  j�  hj�  �ru  j�  hh]�rv  j�  j�  ]rw  �rx  j�  j�  j�  }ry  (j�  j�  j�  �rz  j�  hK�r{  j�  hK �r|  u�r}  �r~  j�  h��r  u�r�  j�  }r�  (j�  j�  j  �r�  j�  hN�r�  j�  j�  Mh�r�  j�  hj  �r�  j�  hh]�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  jv  �r�  j�  hN�r�  j�  j�  Mi�r�  j�  hj�  �r�  j�  hh]�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  jv  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  j�  �r�  j�  hN�r�  j�  j�  Mj�r�  j�  hj�  �r�  j�  hh]�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  j�  }r�  (j�  j�  j�  �r�  j�  hN�r�  j�  j�  Mk�r�  j�  hj�  �r�  j�  hh]�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  X   IncrementTurnr�  }r�  �r�  X   UpdateRecordsr�  }r�  (X   record_typer�  hX   turnr�  �r�  j�  hN�r�  u�r�  X   ChangeFatiguer�  }r�  (j�  j�  h�r�  X   numr�  hK �r�  X   old_fatiguer�  hK �r�  j�  j�  ]r�  �r�  u�r�  j�  }r�  (j�  j�  hu�r�  j�  hK �r�  j�  hK �r�  j�  j�  ]r�  �r�  u�r�  j�  }r�  (j�  j�  h��r�  j�  hK �r�  j�  hK �r�  j�  j�  ]r�  �r�  u�r�  j�  }r�  (j�  j�  h��r�  j�  hK �r�  j�  hK �r�  j�  j�  ]r�  �r�  u�r�  j�  }r�  (j�  j�  hˆr�  j�  hK �r�  j�  hK �r�  j�  j�  ]r�  �r�  u�r�  j�  }r�  (j�  j�  h�r�  j�  hK �r�  j�  hK �r�  j�  j�  ]r�  �r�  u�r�  j�  }r�  (j�  j�  j  �r�  j�  hK �r�  j�  hK �r�  j�  j�  ]r�  �r�  u�r�  j�  }r�  (j�  j�  j%  �r   j�  hK �r  j�  hK �r  j�  j�  ]r  �r  u�r  j�  }r  (j�  j�  jB  �r  j�  hK �r  j�  hK �r	  j�  j�  ]r
  �r  u�r  j�  }r  (j�  j�  j`  �r  j�  hK �r  j�  hK �r  j�  j�  ]r  �r  u�r  j�  }r  (j�  j�  j�  �r  j�  hK �r  j�  hK �r  j�  j�  ]r  �r  u�r  j�  }r  (j�  j�  j�  �r  j�  hK �r  j�  hK �r  j�  j�  ]r  �r   u�r!  j�  }r"  (j�  j�  j�  �r#  j�  hK �r$  j�  hK �r%  j�  j�  ]r&  �r'  u�r(  j�  }r)  (j�  j�  j�  �r*  j�  hK �r+  j�  hK �r,  j�  j�  ]r-  �r.  u�r/  X   SetLevelVarr0  }r1  (hhj  �r2  X   valr3  hK �r4  X   old_valr5  hK �r6  u�r7  j0  }r8  (hhj   �r9  j3  hK �r:  j5  hK �r;  u�r<  j0  }r=  (hhj!  �r>  j3  hK �r?  j5  hK �r@  u�rA  j0  }rB  (hhj"  �rC  j3  hK �rD  j5  hK �rE  u�rF  j0  }rG  (hhj#  �rH  j3  hK �rI  j5  hK �rJ  u�rK  j0  }rL  (hhj$  �rM  j3  hK �rN  j5  hK �rO  u�rP  j0  }rQ  (hhj%  �rR  j3  hK �rS  j5  hK �rT  u�rU  j0  }rV  (hhj&  �rW  j3  hK �rX  j5  hK �rY  u�rZ  j0  }r[  (hhj'  �r\  j3  hK �r]  j5  hK �r^  u�r_  j0  }r`  (hhj(  �ra  j3  hK �rb  j5  hK �rc  u�rd  j0  }re  (hhj)  �rf  j3  hK �rg  j5  hK �rh  u�ri  j0  }rj  (hhj*  �rk  j3  hK �rl  j5  hK �rm  u�rn  j0  }ro  (hhj+  �rp  j3  hK �rq  j5  hK �rr  u�rs  j0  }rt  (hhj,  �ru  j3  hK �rv  j5  hK �rw  u�rx  j0  }ry  (hhj-  �rz  j3  hK �r{  j5  hK �r|  u�r}  j0  }r~  (hhj.  �r  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhj/  �r�  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhj0  �r�  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhj1  �r�  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhj2  �r�  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhj3  �r�  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhj4  �r�  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhj5  �r�  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhj6  �r�  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhj7  �r�  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhj8  �r�  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhj9  �r�  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhj:  �r�  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhj;  �r�  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhj<  �r�  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhj=  �r�  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhj>  �r�  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhj?  �r�  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhj@  �r�  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhjA  �r�  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhjB  �r�  j3  hK �r�  j5  hK �r�  u�r�  X
   SetGameVarr�  }r�  (hhj  �r�  j3  h��r�  j5  hK �r�  u�r�  X   SetHPr�  }r�  (j�  j�  h�r�  X   new_hpr�  hK�r�  X   old_hpr�  hK�r�  u�r�  j�  }r�  (j�  j�  j`  �r�  j�  hN�r�  j�  j�  Ml�r�  j�  hN�r�  j�  hj|  �r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  j`  �r�  j�  hK�r�  j�  hK �r   u�r  �r  j�  h��r  u�r  j0  }r  (hhjC  �r  j3  hK �r  j5  hK �r  u�r	  j0  }r
  (hhjD  �r  j3  hK �r  j5  hK �r  u�r  X   SetExpr  }r  (j�  j�  h�r  X   old_expr  hK �r  X   exp_gainr  hK_�r  u�r  X   ChangeAIr  }r  (j�  j�  j�  �r  hhj�  �r  X   old_air  hX   AttackNoMover  �r  u�r  j0  }r  (hhjE  �r   j3  hK �r!  j5  hK �r"  u�r#  X   SetDroppabler$  }r%  (hHhHK�r&  X   was_droppabler'  h��r(  X   valuer)  h��r*  u�r+  X   GiveItemr,  }r-  (j�  j�  hˆr.  hHhHK�r/  u�r0  X   AddLorer1  }r2  X   lore_nidr3  hX   WeaponTriangler4  �r5  s�r6  j1  }r7  j3  hX   Statsr8  �r9  s�r:  j1  }r;  j3  hX	   Equationsr<  �r=  s�r>  j1  }r?  j3  hX   WeaponRanksr@  �rA  s�rB  j1  }rC  j3  hX   BurningrD  �rE  s�rF  j1  }rG  j3  hX   FreeActionsrH  �rI  s�rJ  j�  }rK  (hhj  �rL  j3  j�  ]rM  h��rN  a�rO  j5  hK �rP  u�rQ  j�  }rR  (hhj  �rS  j3  j�  ]rT  hj	  �rU  a�rV  j5  hK �rW  u�rX  j�  }rY  (hhj
  �rZ  j3  j�  ]r[  hj  �r\  a�r]  j5  hK �r^  u�r_  j�  }r`  (hhj  �ra  j3  j�  ]rb  hj  �rc  a�rd  j5  hK �re  u�rf  j�  }rg  (hhj  �rh  j3  hj  �ri  j5  hK �rj  u�rk  j�  }rl  (hhj  �rm  j3  hj  �rn  j5  hK �ro  u�rp  j�  }rq  (hhj  �rr  j3  j�  ]rs  �rt  j5  hK �ru  u�rv  j�  }rw  (hhj  �rx  j3  j�  ]ry  �rz  j5  hK �r{  u�r|  j�  }r}  (hhj  �r~  j3  j�  ]r  �r�  j5  hK �r�  u�r�  j�  }r�  (hhj  �r�  j3  h��r�  j5  hK �r�  u�r�  X   LockTurnwheelr�  }r�  X   lockr�  h��r�  s�r�  X   ResetAllr�  }r�  X   actionsr�  j�  ]r�  (j�  X   Resetr�  }r�  (j�  j�  h�r�  X   movement_leftr�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  hu�r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  hˆr�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h�r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j%  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  jB  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j`  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hnh(��������tr�  �r   u�r  �r  j�  j�  }r  (j�  j�  j7  �r  j�  hK �r  hnh(��������tr  �r  u�r  �r	  j�  j�  }r
  (j�  j�  jM  �r  j�  hK�r  hnh(��������tr  �r  u�r  �r  j�  j�  }r  (j�  j�  jb  �r  j�  hK�r  hnh(��������tr  �r  u�r  �r  j�  j�  }r  (j�  j�  jv  �r  j�  hK�r  hnh(��������tr  �r  u�r  �r  j�  j�  }r  (j�  j�  j�  �r   j�  hK�r!  hnh(��������tr"  �r#  u�r$  �r%  j�  j�  }r&  (j�  j�  j�  �r'  j�  hK�r(  hnh(��������tr)  �r*  u�r+  �r,  j�  j�  }r-  (j�  j�  j�  �r.  j�  hK�r/  hnh(��������tr0  �r1  u�r2  �r3  j�  j�  }r4  (j�  j�  j�  �r5  j�  hK�r6  hnh(��������tr7  �r8  u�r9  �r:  e�r;  s�r<  X	   MarkPhaser=  }r>  X
   phase_namer?  hX   playerr@  �rA  s�rB  X   MoverC  }rD  (j�  j�  h�rE  X   old_posrF  hh��rG  X   new_posrH  hKK�rI  �rJ  X   prev_movement_leftrK  hK�rL  X   new_movement_leftrM  hN�rN  X   pathrO  hN�rP  X	   has_movedrQ  h��rR  j�
  h��rS  X   followrT  h��rU  X   speedrV  hK�rW  u�rX  X	   EquipItemrY  }rZ  (j�  j�  h�r[  hHhHKņr\  X   current_equippedr]  hHKr^  u�r_  jY  }r`  (j�  j�  h�ra  hHhHKņrb  j]  hHKņrc  u�rd  X   BringToTopItemre  }rf  (j�  j�  h�rg  hHhHKņrh  X   old_idxri  hK�rj  u�rk  X   ChangeHPrl  }rm  (j�  j�  j�  �rn  j�  hJ�����ro  j�  hK�rp  u�rq  X
   SetObjDatarr  }rs  (X   objrt  hHKņru  X   keywordrv  hj  �rw  j)  hK�rx  X	   old_valuery  hK�rz  u�r{  j�  }r|  (j�  hX   item_user}  �r~  j�  hh�jq  �r  �r�  u�r�  X   HasAttackedr�  }r�  (j�  j�  h�r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  X   Messager�  }r�  X   messager�  hX   Saraid attacked Soldierr�  �r�  s�r�  X   GainWexpr�  }r�  (j�  j�  h�r�  hHhHKņr�  X	   wexp_gainr�  hG?�      �r�  jy  hK�r�  X   current_valuer�  hG@(      �r�  u�r�  j�  }r�  (j�  hj  �r�  j�  hh�j�  �r�  �r�  u�r�  j�  }r�  (j�  hj  �r�  j�  h(h�j�  jq  KKj  tr�  �r�  u�r�  X   RecordRandomStater�  }r�  (X   oldr�  hM��r�  X   newr�  hJ�4�r�  u�r�  X	   AddRegionr�  }r�  (h`h`j�
  �r�  X   did_addr�  h��r�  j�  j�  ]r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hN�r�  j�  j�  Mm�r�  j�  hj�
  �r�  j�  hhc�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  �r�  a�r�  u�r�  X
   AddMapAnimr�  }r�  (hhX   Burningr�  �r�  X   posr�  hj�  �r�  X
   speed_multr�  hK�r�  X
   blend_moder�  hcapp.engine.engine
BlendMode
r�  K �r�  Rr�  �r�  X   is_upper_layerr�  h��r�  u�r�  j0  }r�  (hhjF  �r�  j3  j�  ]r�  (hjH  �r�  hjI  �r�  hjJ  �r�  hjK  �r�  e�r�  j5  hK �r�  u�r�  j0  }r�  (hhX
   FireStart1r�  �r�  j3  hK�r�  j5  hK �r�  u�r�  j0  }r�  (hhX   FireOrigin1r�  �r�  j3  hj�  �r�  j5  hK �r�  u�r�  X   GainExpr�  }r�  (j�  j�  h�r�  j  hK �r�  j  hK
�r�  u�r�  j�  }r�  (j�  hj  �r�  j�  hh�K
h�r�  �r�  u�r�  X   Waitr�  }r�  (j�  j�  h�r�  hnh(��������tr�  �r�  X   update_fow_actionr�  j�  X   UpdateFogOfWarr�  }r�  (j�  j�  h�r�  X   prev_posr�  hN�r�  u�r�  �r   X   regions_removedr  j�  ]r  �r  u�r  jC  }r  (j�  j�  j�  �r  jF  hj�  �r  jH  hj�  �r  jK  hK�r	  jM  hN�r
  jO  hN�r  jQ  h��r  j�
  h��r  jT  h��r  jV  hK�r  u�r  jY  }r  (j�  j�  j�  �r  hHhHKӆr  j]  hHKӆr  u�r  je  }r  (j�  j�  j�  �r  hHhHKӆr  ji  hK �r  u�r  jl  }r  (j�  j�  j�  �r  j�  hJ�����r  j�  hK�r  u�r  jr  }r   (jt  hHKӆr!  jv  hj  �r"  j)  hK�r#  jy  hK�r$  u�r%  j�  }r&  (j�  hj}  �r'  j�  hj�  j�  �r(  �r)  u�r*  j�  }r+  (j�  j�  j�  �r,  j�  hK�r-  hnh(��������tr.  �r/  u�r0  j�  }r1  j�  hX   Ailsa attacked Soldierr2  �r3  s�r4  j�  }r5  j�  hX   Prevailed over Soldierr6  �r7  s�r8  j�  }r9  (j�  j�  j�  �r:  hHhHKӆr;  j�  hG?�      �r<  jy  hK�r=  j�  hG@6      �r>  u�r?  j�  }r@  (j�  hj
  �rA  j�  hj�  j�  �rB  �rC  u�rD  j�  }rE  (j�  hj  �rF  j�  h(j�  j�  j�  K
Kj
  trG  �rH  u�rI  j�  }rJ  (j�  hX   killrK  �rL  j�  hj�  j�  �rM  �rN  u�rO  j�  }rP  (j�  hJ�4�rQ  j�  hJo��rR  u�rS  X   DierT  }rU  (j�  j�  j�  �rV  jF  hj�  �rW  X	   leave_maprX  j�  X   LeaveMaprY  }rZ  (j�  j�  j�  �r[  X   remove_from_mapr\  j�  X   RemoveFromMapr]  }r^  (j�  j�  j�  �r_  jF  hj�  �r`  j�  j�  j�  }ra  (j�  j�  j�  �rb  j�  hN�rc  u�rd  �re  u�rf  �rg  u�rh  �ri  X   lock_all_support_ranksrj  j�  ]rk  �rl  X   droprm  hN�rn  X   initiative_actionro  hN�rp  u�rq  j�  }rr  (j�  j�  j�  �rs  j  hK �rt  j  hK�ru  u�rv  j�  }rw  (j�  hj  �rx  j�  hj�  Kj�  �ry  �rz  u�r{  j�  }r|  (j�  j�  j�  �r}  hnh(��������tr~  �r  j�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hN�r�  u�r�  �r�  j  j�  ]r�  �r�  u�r�  jr  }r�  (jt  j�  M�r�  jv  hj�	  �r�  j)  hK�r�  jy  hK�r�  u�r�  j0  }r�  (hhjL  �r�  j3  hK �r�  j5  hK �r�  u�r�  j0  }r�  (hhjM  �r�  j3  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  e�r�  j5  hK �r�  u�r�  j0  }r�  (hhjh  �r�  j3  hja  �r�  j5  hK �r�  u�r�  j0  }r�  (hhX	   ValidLocsr�  �r�  j3  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  e�r�  j5  j�  ]r�  (hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j0  }r�  (hhj�  �r�  j3  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  e�r�  j5  j�  ]r�  (hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j0  }r�  (hhj�  �r�  j3  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  e�r   j5  j�  ]r  (hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r	  hj�  �r
  hj�  �r  e�r  u�r  j0  }r  (hhj�  �r  j3  j�  ]r  (hKK�r  �r  hKK	�r  �r  hKK	�r  �r  hKK
�r  �r  hKK�r  �r  hKK�r  �r  hKK�r  �r  hKK	�r  �r   hKK	�r!  �r"  hKK
�r#  �r$  hKK	�r%  �r&  hKK
�r'  �r(  hKK�r)  �r*  e�r+  j5  j�  ]r,  (hj�  �r-  hj�  �r.  hj�  �r/  hj�  �r0  hj�  �r1  hj�  �r2  hj�  �r3  hj�  �r4  hj�  �r5  hj�  �r6  hj�  �r7  hj�  �r8  e�r9  u�r:  j0  }r;  (hhj�  �r<  j3  j�  ]r=  (hKK�r>  �r?  hKK	�r@  �rA  hKK	�rB  �rC  hKK
�rD  �rE  hKK�rF  �rG  hKK�rH  �rI  hKK�rJ  �rK  hKK	�rL  �rM  hKK	�rN  �rO  hKK
�rP  �rQ  hKK	�rR  �rS  hKK
�rT  �rU  hKK�rV  �rW  e�rX  j5  j�  ]rY  (hj  �rZ  hj  �r[  hj  �r\  hj  �r]  hj  �r^  hj  �r_  hj  �r`  hj  �ra  hj!  �rb  hj#  �rc  hj%  �rd  hj'  �re  hj)  �rf  e�rg  u�rh  j0  }ri  (hhj�  �rj  j3  j�  ]rk  (hKK�rl  �rm  hKK	�rn  �ro  hKK	�rp  �rq  hKK
�rr  �rs  hKK�rt  �ru  hKK�rv  �rw  hKK�rx  �ry  hKK	�rz  �r{  hKK	�r|  �r}  hKK
�r~  �r  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  e�r�  j5  j�  ]r�  (hj>  �r�  hj@  �r�  hjB  �r�  hjD  �r�  hjF  �r�  hjH  �r�  hjJ  �r�  hjL  �r�  hjN  �r�  hjP  �r�  hjR  �r�  hjT  �r�  hjV  �r�  e�r�  u�r�  j0  }r�  (hhj�  �r�  j3  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  e�r�  j5  j�  ]r�  (hjl  �r�  hjn  �r�  hjp  �r�  hjr  �r�  hjt  �r�  hjv  �r�  hjx  �r�  hjz  �r�  hj|  �r�  hj~  �r�  hj�  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j0  }r�  (hhj�  �r�  j3  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  e�r�  j5  j�  ]r�  (hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j0  }r�  (hhj�  �r�  j3  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r   �r  hKK�r  �r  hKK	�r  �r  hKK	�r  �r  hKK
�r  �r	  hKK	�r
  �r  hKK
�r  �r  hKK�r  �r  hKK�r  �r  hKK�r  �r  hKK�r  �r  e�r  j5  j�  ]r  (hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r   hj�  �r!  hj�  �r"  hj�  �r#  hj�  �r$  e�r%  u�r&  j0  }r'  (hhj�  �r(  j3  j�  ]r)  (hKK�r*  �r+  hKK	�r,  �r-  hKK	�r.  �r/  hKK
�r0  �r1  hKK�r2  �r3  hKK�r4  �r5  hKK�r6  �r7  hKK	�r8  �r9  hKK	�r:  �r;  hKK
�r<  �r=  hKK	�r>  �r?  hKK
�r@  �rA  hKK�rB  �rC  hKK�rD  �rE  hKK�rF  �rG  hKK�rH  �rI  hKK�rJ  �rK  e�rL  j5  j�  ]rM  (hj�  �rN  hj�  �rO  hj�  �rP  hj�  �rQ  hj�  �rR  hj   �rS  hj  �rT  hj  �rU  hj  �rV  hj  �rW  hj
  �rX  hj  �rY  hj  �rZ  hj  �r[  hj  �r\  hj  �r]  e�r^  u�r_  j0  }r`  (hhj�  �ra  j3  j�  ]rb  (hKK�rc  �rd  hKK	�re  �rf  hKK	�rg  �rh  hKK
�ri  �rj  hKK�rk  �rl  hKK�rm  �rn  hKK�ro  �rp  hKK	�rq  �rr  hKK	�rs  �rt  hKK
�ru  �rv  hKK	�rw  �rx  hKK
�ry  �rz  hKK�r{  �r|  hKK�r}  �r~  hKK�r  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  e�r�  j5  j�  ]r�  (hj*  �r�  hj,  �r�  hj.  �r�  hj0  �r�  hj2  �r�  hj4  �r�  hj6  �r�  hj8  �r�  hj:  �r�  hj<  �r�  hj>  �r�  hj@  �r�  hjB  �r�  hjD  �r�  hjF  �r�  hjH  �r�  hjJ  �r�  e�r�  u�r�  j0  }r�  (hhj�  �r�  j3  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  e�r�  j5  j�  ]r�  (hjc  �r�  hje  �r�  hjg  �r�  hji  �r�  hjk  �r�  hjm  �r�  hjo  �r�  hjq  �r�  hjs  �r�  hju  �r�  hjw  �r�  hjy  �r�  hj{  �r�  hj}  �r�  hj  �r�  hj�  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j0  }r�  (hhj�  �r�  j3  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r   �r  hKK
�r  �r  e�r  j5  j�  ]r  (hj�  �r  hj�  �r  hj�  �r  hj�  �r	  hj�  �r
  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  e�r  u�r  j0  }r  (hhj�  �r  j3  j�  ]r  (hKK�r  �r  hKK	�r  �r   hKK	�r!  �r"  hKK
�r#  �r$  hKK�r%  �r&  hKK�r'  �r(  hKK�r)  �r*  hKK	�r+  �r,  hKK	�r-  �r.  hKK
�r/  �r0  hKK	�r1  �r2  hKK
�r3  �r4  hKK�r5  �r6  hKK�r7  �r8  hKK�r9  �r:  hKK�r;  �r<  hKK�r=  �r>  hKK�r?  �r@  hKK	�rA  �rB  hKK
�rC  �rD  hKK�rE  �rF  e�rG  j5  j�  ]rH  (hj�  �rI  hj�  �rJ  hj�  �rK  hj�  �rL  hj�  �rM  hj�  �rN  hj�  �rO  hj�  �rP  hj�  �rQ  hj�  �rR  hj�  �rS  hj�  �rT  hj�  �rU  hj�  �rV  hj�  �rW  hj�  �rX  hj�  �rY  hj�  �rZ  hj   �r[  hj  �r\  e�r]  u�r^  j0  }r_  (hhj�  �r`  j3  j�  ]ra  (hKK�rb  �rc  hKK	�rd  �re  hKK	�rf  �rg  hKK
�rh  �ri  hKK�rj  �rk  hKK�rl  �rm  hKK�rn  �ro  hKK	�rp  �rq  hKK	�rr  �rs  hKK
�rt  �ru  hKK	�rv  �rw  hKK
�rx  �ry  hKK�rz  �r{  hKK�r|  �r}  hKK�r~  �r  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  e�r�  j5  j�  ]r�  (hj  �r�  hj  �r�  hj!  �r�  hj#  �r�  hj%  �r�  hj'  �r�  hj)  �r�  hj+  �r�  hj-  �r�  hj/  �r�  hj1  �r�  hj3  �r�  hj5  �r�  hj7  �r�  hj9  �r�  hj;  �r�  hj=  �r�  hj?  �r�  hjA  �r�  hjC  �r�  hjE  �r�  e�r�  u�r�  j0  }r�  (hhj�  �r�  j3  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  e�r�  j5  j�  ]r�  (hjb  �r�  hjd  �r�  hjf  �r�  hjh  �r�  hjj  �r�  hjl  �r�  hjn  �r�  hjp  �r�  hjr  �r�  hjt  �r�  hjv  �r�  hjx  �r�  hjz  �r�  hj|  �r�  hj~  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j0  }r�  (hhj�  �r�  j3  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r   �r  hKK�r  �r  hKK�r  �r  hKK�r  �r  hKK	�r  �r	  hKK	�r
  �r  hKK
�r  �r  hKK	�r  �r  hKK
�r  �r  hKK�r  �r  hKK�r  �r  hKK�r  �r  hKK�r  �r  hKK�r  �r  hKK�r  �r  hKK	�r  �r  hKK
�r   �r!  hKK�r"  �r#  hKK	�r$  �r%  hKK
�r&  �r'  hKK�r(  �r)  hKK�r*  �r+  e�r,  j5  j�  ]r-  (hj�  �r.  hj�  �r/  hj�  �r0  hj�  �r1  hj�  �r2  hj�  �r3  hj�  �r4  hj�  �r5  hj�  �r6  hj�  �r7  hj�  �r8  hj�  �r9  hj�  �r:  hj�  �r;  hj�  �r<  hj�  �r=  hj�  �r>  hj�  �r?  hj�  �r@  hj�  �rA  hj�  �rB  hj�  �rC  hj�  �rD  hj�  �rE  e�rF  u�rG  j�  }rH  (hhX
   Target_3x3rI  �rJ  j�  hG@      G@"      �rK  �rL  j�  hK�rM  j�  hj�  �rN  j�  h��rO  u�rP  j�  }rQ  (h`h`X   Garve_0rR  �rS  j�  h��rT  j�  j�  ]rU  �rV  u�rW  j0  }rX  (hhX   GarveySpotsrY  �rZ  j3  hK�r[  j5  hK �r\  u�r]  j�  }r^  (h`h`X   Garve_1r_  �r`  j�  h��ra  j�  j�  ]rb  �rc  u�rd  j0  }re  (hhjY  �rf  j3  hK�rg  j5  hK�rh  u�ri  j�  }rj  (h`h`X   Garve_2rk  �rl  j�  h��rm  j�  j�  ]rn  �ro  u�rp  j0  }rq  (hhjY  �rr  j3  hK�rs  j5  hK�rt  u�ru  j�  }rv  (h`h`X   Garve_3rw  �rx  j�  h��ry  j�  j�  ]rz  �r{  u�r|  j0  }r}  (hhjY  �r~  j3  hK�r  j5  hK�r�  u�r�  j�  }r�  (h`h`X   Garve_4r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j0  }r�  (hhjY  �r�  j3  hK�r�  j5  hK�r�  u�r�  j�  }r�  (h`h`X   Garve_5r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j0  }r�  (hhjY  �r�  j3  hK�r�  j5  hK�r�  u�r�  j�  }r�  (h`h`X   Garve_6r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j0  }r�  (hhjY  �r�  j3  hK�r�  j5  hK�r�  u�r�  j�  }r�  (h`h`X   Garve_7r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j0  }r�  (hhjY  �r�  j3  hK�r�  j5  hK�r�  u�r�  j�  }r�  (h`h`X   Garve_8r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j0  }r�  (hhjY  �r�  j3  hK	�r�  j5  hK�r�  u�r�  j�  }r�  (h`h`X   Garve_9r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j0  }r�  (hhjY  �r�  j3  hK
�r�  j5  hK	�r�  u�r�  j�  }r�  (h`h`X   Garve_10r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j0  }r�  (hhjY  �r�  j3  hK�r�  j5  hK
�r�  u�r�  j�  }r�  (h`h`X   Garve_11r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j0  }r�  (hhjY  �r�  j3  hK�r�  j5  hK�r�  u�r�  j�  }r�  (h`h`X   Garve_12r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j0  }r�  (hhjY  �r�  j3  hK�r�  j5  hK�r�  u�r�  j�  }r�  (h`h`X   Garve_13r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j0  }r�  (hhjY  �r�  j3  hK�r�  j5  hK�r�  u�r�  j�  }r�  (h`h`X   Garve_14r�  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r   j0  }r  (hhjY  �r  j3  hK�r  j5  hK�r  u�r  j�  }r  (h`h`X   Garve_15r  �r  j�  h��r	  j�  j�  ]r
  �r  u�r  j0  }r  (hhjY  �r  j3  hK�r  j5  hK�r  u�r  j�  }r  j�  h��r  s�r  j�  }r  j�  j�  ]r  (j�  j�  }r  (j�  j�  h�r  j�  hK�r  hnh(��������tr  �r  u�r  �r  j�  j�  }r  (j�  j�  hu�r  j�  hK�r   hnh(��������tr!  �r"  u�r#  �r$  j�  j�  }r%  (j�  j�  h��r&  j�  hK�r'  hnh(��������tr(  �r)  u�r*  �r+  j�  j�  }r,  (j�  j�  h��r-  j�  hK�r.  hnh(��������tr/  �r0  u�r1  �r2  j�  j�  }r3  (j�  j�  hˆr4  j�  hK�r5  hnh(��������tr6  �r7  u�r8  �r9  j�  j�  }r:  (j�  j�  h�r;  j�  hK�r<  hnh(��������tr=  �r>  u�r?  �r@  j�  j�  }rA  (j�  j�  j  �rB  j�  hK�rC  hnh(��������trD  �rE  u�rF  �rG  j�  j�  }rH  (j�  j�  j%  �rI  j�  hK�rJ  hnh(��������trK  �rL  u�rM  �rN  j�  j�  }rO  (j�  j�  jB  �rP  j�  hK�rQ  hnh(��������trR  �rS  u�rT  �rU  j�  j�  }rV  (j�  j�  j`  �rW  j�  hK�rX  hnh(��������trY  �rZ  u�r[  �r\  j�  j�  }r]  (j�  j�  j�  �r^  j�  hK�r_  hnh(��������tr`  �ra  u�rb  �rc  j�  j�  }rd  (j�  j�  j�  �re  j�  hK�rf  hnh(��������trg  �rh  u�ri  �rj  j�  j�  }rk  (j�  j�  j�  �rl  j�  hK�rm  hnh(��������trn  �ro  u�rp  �rq  j�  j�  }rr  (j�  j�  j�  �rs  j�  hK�rt  hnh(��������tru  �rv  u�rw  �rx  j�  j�  }ry  (j�  j�  j  �rz  j�  hK�r{  hnh(��������tr|  �r}  u�r~  �r  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j7  �r�  j�  hK �r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  jM  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  jb  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  jv  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  e�r�  s�r�  j=  }r�  j?  hX   enemyr�  �r�  s�r�  j�  }r�  j�  h��r�  s�r�  j�  }r�  j�  j�  ]r�  (j�  j�  }r�  (j�  j�  h�r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  hu�r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  hˆr�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h�r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j%  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  jB  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r   �r  j�  j�  }r  (j�  j�  j`  �r  j�  hK�r  hnh(��������tr  �r  u�r  �r  j�  j�  }r	  (j�  j�  j�  �r
  j�  hK�r  hnh(��������tr  �r  u�r  �r  j�  j�  }r  (j�  j�  j�  �r  j�  hK�r  hnh(��������tr  �r  u�r  �r  j�  j�  }r  (j�  j�  j�  �r  j�  hK�r  hnh(��������tr  �r  u�r  �r  j�  j�  }r  (j�  j�  j�  �r  j�  hK�r   hnh(��������tr!  �r"  u�r#  �r$  j�  j�  }r%  (j�  j�  j  �r&  j�  hK�r'  hnh(��������tr(  �r)  u�r*  �r+  j�  j�  }r,  (j�  j�  j  �r-  j�  hK�r.  hnh(��������tr/  �r0  u�r1  �r2  j�  j�  }r3  (j�  j�  j7  �r4  j�  hK �r5  hnh(��������tr6  �r7  u�r8  �r9  j�  j�  }r:  (j�  j�  jM  �r;  j�  hK�r<  hnh(��������tr=  �r>  u�r?  �r@  j�  j�  }rA  (j�  j�  jb  �rB  j�  hK�rC  hnh(��������trD  �rE  u�rF  �rG  j�  j�  }rH  (j�  j�  jv  �rI  j�  hK�rJ  hnh(��������trK  �rL  u�rM  �rN  j�  j�  }rO  (j�  j�  j�  �rP  j�  hK�rQ  hnh(��������trR  �rS  u�rT  �rU  j�  j�  }rV  (j�  j�  j�  �rW  j�  hK�rX  hnh(��������trY  �rZ  u�r[  �r\  j�  j�  }r]  (j�  j�  j�  �r^  j�  hK�r_  hnh(��������tr`  �ra  u�rb  �rc  e�rd  s�re  j=  }rf  j?  hX   otherrg  �rh  s�ri  j�  }rj  �rk  j�  }rl  (j�  hj�  �rm  j�  hN�rn  u�ro  j0  }rp  (hhji  �rq  j3  hK �rr  j5  hK �rs  u�rt  j�  }ru  (h`h`j�  �rv  j�  h��rw  j�  j�  ]rx  j�  j�  }ry  (j�  j�  j�  �rz  j�  hN�r{  j�  j�  Mn�r|  j�  hj�  �r}  j�  hhc�r~  j�  j�  ]r  �r�  j�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  �r�  a�r�  u�r�  j�  }r�  (hhX   Burningr�  �r�  j�  hG@      G@      �r�  �r�  j�  hK�r�  j�  hj�  �r�  j�  h��r�  u�r�  j0  }r�  (hhX	   whocares1r�  �r�  j3  hK�r�  j5  hK �r�  u�r�  j�  }r�  (h`h`h��r�  j�  h��r�  j�  j�  ]r�  j�  j�  }r�  (j�  j�  h��r�  j�  hN�r�  j�  j�  Mo�r�  j�  hh��r�  j�  hhc�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  �r�  a�r�  u�r�  j�  }r�  (hhj�  �r�  j�  hG@      G@      �r�  �r�  j�  hK�r�  j�  hj�  �r�  j�  h��r�  u�r�  j0  }r�  (hhj�  �r�  j3  hK�r�  j5  hK�r�  u�r�  j�  }r�  (h`h`jO  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j�  }r�  (hhj�  �r�  j�  hG@      G@      �r�  �r�  j�  hK�r�  j�  hj�  �r�  j�  h��r�  u�r�  j0  }r�  (hhj�  �r�  j3  hK�r�  j5  hK�r�  u�r�  j�  }r�  (h`h`j  �r�  j�  h��r�  j�  j�  ]r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hN�r�  j�  j�  Mp�r�  j�  hj  �r�  j�  hhc�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  �r�  a�r�  u�r�  j�  }r�  (hhj�  �r�  j�  hG@      G@      �r�  �r�  j�  hK�r�  j�  hj�  �r�  j�  h��r�  u�r�  j0  }r�  (hhj�  �r�  j3  hK�r�  j5  hK�r�  u�r�  j0  }r�  (hhX   FireComplete1r�  �r�  j3  hK�r�  j5  hK �r�  u�r�  X   RemoveRegionr�  }r�  (h`h`jR  �r�  X
   did_remover�  h��r   j�  j�  ]r  �r  u�r  j�  }r  (h`h`j_  �r  j�  h��r  j�  j�  ]r  �r  u�r	  j�  }r
  (h`h`jk  �r  j�  h��r  j�  j�  ]r  �r  u�r  j�  }r  (h`h`jw  �r  j�  h��r  j�  j�  ]r  �r  u�r  j�  }r  (h`h`j�  �r  j�  h��r  j�  j�  ]r  �r  u�r  j�  }r  (h`h`j�  �r  j�  h��r  j�  j�  ]r  �r   u�r!  j�  }r"  (h`h`j�  �r#  j�  h��r$  j�  j�  ]r%  �r&  u�r'  j�  }r(  (h`h`j�  �r)  j�  h��r*  j�  j�  ]r+  �r,  u�r-  j�  }r.  (h`h`j�  �r/  j�  h��r0  j�  j�  ]r1  �r2  u�r3  j�  }r4  (h`h`j�  �r5  j�  h��r6  j�  j�  ]r7  �r8  u�r9  j�  }r:  (h`h`j�  �r;  j�  h��r<  j�  j�  ]r=  �r>  u�r?  j�  }r@  (h`h`j�  �rA  j�  h��rB  j�  j�  ]rC  �rD  u�rE  j�  }rF  (h`h`j�  �rG  j�  h��rH  j�  j�  ]rI  �rJ  u�rK  j�  }rL  (h`h`j�  �rM  j�  h��rN  j�  j�  ]rO  �rP  u�rQ  j�  }rR  (h`h`j�  �rS  j�  h��rT  j�  j�  ]rU  �rV  u�rW  j�  }rX  (h`h`j  �rY  j�  h��rZ  j�  j�  ]r[  �r\  u�r]  X   RemoveMapAnimr^  }r_  (hhX
   Target_3x3r`  �ra  j�  hG@      G@"      �rb  �rc  j�  hK�rd  X   blendre  hj�  �rf  j�  h��rg  j�  h��rh  u�ri  j0  }rj  (hhX   GarvPositionrk  �rl  j3  hK �rm  j5  hja  �rn  u�ro  j�  }rp  j�  h��rq  s�rr  j�  }rs  j�  j�  ]rt  (j�  j�  }ru  (j�  j�  h�rv  j�  hK�rw  hnh(��������trx  �ry  u�rz  �r{  j�  j�  }r|  (j�  j�  hu�r}  j�  hK�r~  hnh(��������tr  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h��r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  hˆr�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h�r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j%  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  jB  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j`  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j7  �r�  j�  hK �r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  jM  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  jb  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  jv  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r   j�  j�  }r  (j�  j�  j�  �r  j�  hK�r  hnh(��������tr  �r  u�r  �r  j�  j�  }r  (j�  j�  j�  �r	  j�  hK�r
  hnh(��������tr  �r  u�r  �r  j�  j�  }r  (j�  j�  j�  �r  j�  hK�r  hnh(��������tr  �r  u�r  �r  e�r  s�r  jl  }r  (j�  j�  j�  �r  j�  hJ�����r  j�  hK�r  u�r  X   TriggerCharger  }r  (j�  j�  j�  �r  j�  j�  Mn�r   X
   old_charger!  hN�r"  X
   new_charger#  hN�r$  u�r%  jl  }r&  (j�  j�  j  �r'  j�  hJ�����r(  j�  hK�r)  u�r*  j  }r+  (j�  j�  j  �r,  j�  j�  Mp�r-  j!  hN�r.  j#  hN�r/  u�r0  jl  }r1  (j�  j�  h��r2  j�  hJ�����r3  j�  hK�r4  u�r5  j  }r6  (j�  j�  h��r7  j�  j�  Mo�r8  j!  hN�r9  j#  hN�r:  u�r;  j=  }r<  j?  hj@  �r=  s�r>  jC  }r?  (j�  j�  h�r@  jF  hjI  �rA  jH  hh�rB  jK  hK�rC  jM  hN�rD  jO  hN�rE  jQ  h��rF  j�
  h��rG  jT  h��rH  jV  hK�rI  u�rJ  jY  }rK  (j�  j�  h�rL  hHhHKņrM  j]  hHKņrN  u�rO  je  }rP  (j�  j�  h�rQ  hHhHKņrR  ji  hK �rS  u�rT  jl  }rU  (j�  j�  jv  �rV  j�  hJ�����rW  j�  hK�rX  u�rY  jr  }rZ  (jt  hHKņr[  jv  hj  �r\  j)  hK
�r]  jy  hK�r^  u�r_  j�  }r`  (j�  hj}  �ra  j�  hh�jq  �rb  �rc  u�rd  jl  }re  (j�  j�  h�rf  j�  hJ�����rg  j�  hK�rh  u�ri  j�  }rj  (j�  j�  h�rk  j�  hK�rl  hnh(��������trm  �rn  u�ro  j�  }rp  j�  hX   Saraid attacked Soldierrq  �rr  s�rs  j�  }rt  (j�  j�  h�ru  hHhHKņrv  j�  hG?�      �rw  jy  hG@(      �rx  j�  hG@*      �ry  u�rz  j�  }r{  (j�  j�  jv  �r|  hHhHK��r}  j�  hG        �r~  jy  hK�r  j�  hG?�      �r�  u�r�  j�  }r�  (j�  hj  �r�  j�  hh�jv  �r�  �r�  u�r�  j�  }r�  (j�  hj  �r�  j�  hjv  h�r�  �r�  u�r�  j�  }r�  (j�  hj  �r�  j�  h(h�jv  jq  KKj  tr�  �r�  u�r�  j�  }r�  (j�  hj  �r�  j�  h(jv  h�j'  KKj  tr�  �r�  u�r�  j�  }r�  (j�  hJo��r�  j�  hJa�F�r�  u�r�  j�  }r�  (h`h`jX  �r�  j�  h��r�  j�  j�  ]r�  j�  j�  }r�  (j�  j�  jv  �r�  j�  hN�r�  j�  j�  Mq�r�  j�  hjX  �r�  j�  hhc�r�  j�  j�  ]r�  �r�  j�  j�  j�  }r�  (j�  j�  jv  �r�  j�  hK�r�  j�  hK �r�  u�r�  �r�  j�  h��r�  u�r�  �r�  a�r�  u�r�  j�  }r�  (hhX   Burningr�  �r�  j�  hj�  �r�  j�  hK�r�  j�  hj�  �r�  j�  h��r�  u�r�  j0  }r�  (hhjj  �r�  j3  j�  ]r�  (hjl  �r�  hjm  �r�  hjn  �r�  hjo  �r�  e�r�  j5  hK �r�  u�r�  j0  }r�  (hhX
   FireStart2r�  �r�  j3  hK�r�  j5  hK �r�  u�r�  j0  }r�  (hhX   FireOrigin2r�  �r�  j3  hj�  �r�  j5  hK �r�  u�r�  j�  }r�  (j�  j�  h�r�  j  hK
�r�  j  hK�r�  u�r�  j�  }r�  (j�  hj  �r�  j�  hh�Kh�r�  �r�  u�r�  j�  }r�  (j�  j�  h�r�  hnh(��������tr�  �r�  j�  j�  j�  }r�  (j�  j�  h�r�  j�  hjI  �r�  u�r�  �r�  j  j�  ]r�  �r�  u�r�  jr  }r�  (jt  j�  M�r�  jv  hj�	  �r�  j)  hK�r�  jy  hK�r�  u�r�  j0  }r�  (hhX   GarveySpotsr�  �r�  j3  hK �r�  j5  hK�r�  u�r�  j0  }r�  (hhX	   ValidLocsr�  �r�  j3  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  e�r�  j5  j�  ]r�  (hj�  �r�  hj�  �r   hj�  �r  hj   �r  hj  �r  hj  �r  hj  �r  hj  �r  hj
  �r  hj  �r  hj  �r	  hj  �r
  hj  �r  hj  �r  hj  �r  hj  �r  hj  �r  hj  �r  hj  �r  hj   �r  hj"  �r  hj$  �r  hj&  �r  hj(  �r  hj*  �r  e�r  u�r  j0  }r  (hhX   GarvPositionr  �r  j3  hja  �r  j5  hK �r  u�r  j0  }r   (hhX	   ValidLocsr!  �r"  j3  j�  ]r#  (hKK�r$  �r%  hKK	�r&  �r'  hKK	�r(  �r)  hKK
�r*  �r+  hKK�r,  �r-  hKK�r.  �r/  hKK�r0  �r1  hKK	�r2  �r3  e�r4  j5  j�  ]r5  (hj�  �r6  hj�  �r7  hj�  �r8  hj�  �r9  e�r:  u�r;  j0  }r<  (hhj!  �r=  j3  j�  ]r>  (hKK�r?  �r@  hKK	�rA  �rB  hKK	�rC  �rD  hKK
�rE  �rF  hKK�rG  �rH  hKK�rI  �rJ  hKK�rK  �rL  hKK	�rM  �rN  hKK	�rO  �rP  hKK
�rQ  �rR  e�rS  j5  j�  ]rT  (hj$  �rU  hj&  �rV  hj(  �rW  hj*  �rX  hj,  �rY  hj.  �rZ  hj0  �r[  hj2  �r\  e�r]  u�r^  j0  }r_  (hhj!  �r`  j3  j�  ]ra  (hKK�rb  �rc  hKK	�rd  �re  hKK	�rf  �rg  hKK
�rh  �ri  hKK�rj  �rk  hKK�rl  �rm  hKK�rn  �ro  hKK	�rp  �rq  hKK	�rr  �rs  hKK
�rt  �ru  hKK	�rv  �rw  hKK
�rx  �ry  e�rz  j5  j�  ]r{  (hj?  �r|  hjA  �r}  hjC  �r~  hjE  �r  hjG  �r�  hjI  �r�  hjK  �r�  hjM  �r�  hjO  �r�  hjQ  �r�  e�r�  u�r�  j0  }r�  (hhj!  �r�  j3  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  e�r�  j5  j�  ]r�  (hjb  �r�  hjd  �r�  hjf  �r�  hjh  �r�  hjj  �r�  hjl  �r�  hjn  �r�  hjp  �r�  hjr  �r�  hjt  �r�  hjv  �r�  hjx  �r�  e�r�  u�r�  j0  }r�  (hhj!  �r�  j3  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  e�r�  j5  j�  ]r�  (hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j0  }r�  (hhj!  �r�  j3  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  e�r   j5  j�  ]r  (hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r	  hj�  �r
  hj�  �r  hj�  �r  hj�  �r  hj�  �r  e�r  u�r  j0  }r  (hhj!  �r  j3  j�  ]r  (hKK�r  �r  hKK	�r  �r  hKK	�r  �r  hKK
�r  �r  hKK�r  �r  hKK�r  �r  hKK�r   �r!  hKK	�r"  �r#  hKK	�r$  �r%  hKK
�r&  �r'  hKK	�r(  �r)  hKK
�r*  �r+  hKK�r,  �r-  e�r.  j5  j�  ]r/  (hj�  �r0  hj�  �r1  hj�  �r2  hj�  �r3  hj�  �r4  hj�  �r5  hj�  �r6  hj�  �r7  hj�  �r8  hj�  �r9  hj�  �r:  hj�  �r;  hj�  �r<  e�r=  u�r>  j0  }r?  (hhj!  �r@  j3  j�  ]rA  (hKK�rB  �rC  hKK	�rD  �rE  hKK	�rF  �rG  hKK
�rH  �rI  hKK�rJ  �rK  hKK�rL  �rM  hKK�rN  �rO  hKK	�rP  �rQ  hKK	�rR  �rS  hKK
�rT  �rU  hKK	�rV  �rW  hKK
�rX  �rY  hKK�rZ  �r[  e�r\  j5  j�  ]r]  (hj  �r^  hj  �r_  hj  �r`  hj  �ra  hj  �rb  hj  �rc  hj   �rd  hj"  �re  hj$  �rf  hj&  �rg  hj(  �rh  hj*  �ri  hj,  �rj  e�rk  u�rl  j0  }rm  (hhj!  �rn  j3  j�  ]ro  (hKK�rp  �rq  hKK	�rr  �rs  hKK	�rt  �ru  hKK
�rv  �rw  hKK�rx  �ry  hKK�rz  �r{  hKK�r|  �r}  hKK	�r~  �r  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  e�r�  j5  j�  ]r�  (hjB  �r�  hjD  �r�  hjF  �r�  hjH  �r�  hjJ  �r�  hjL  �r�  hjN  �r�  hjP  �r�  hjR  �r�  hjT  �r�  hjV  �r�  hjX  �r�  hjZ  �r�  e�r�  u�r�  j0  }r�  (hhj!  �r�  j3  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  e�r�  j5  j�  ]r�  (hjp  �r�  hjr  �r�  hjt  �r�  hjv  �r�  hjx  �r�  hjz  �r�  hj|  �r�  hj~  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j0  }r�  (hhj!  �r�  j3  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r   e�r  j5  j�  ]r  (hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r	  hj�  �r
  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  e�r  u�r  j0  }r  (hhj!  �r  j3  j�  ]r  (hKK�r  �r  hKK	�r  �r  hKK	�r  �r  hKK
�r  �r   hKK�r!  �r"  hKK�r#  �r$  hKK�r%  �r&  hKK	�r'  �r(  hKK	�r)  �r*  hKK
�r+  �r,  hKK	�r-  �r.  hKK
�r/  �r0  hKK�r1  �r2  hKK�r3  �r4  hKK�r5  �r6  hKK�r7  �r8  hKK�r9  �r:  hKK�r;  �r<  e�r=  j5  j�  ]r>  (hj�  �r?  hj�  �r@  hj�  �rA  hj�  �rB  hj�  �rC  hj�  �rD  hj�  �rE  hj�  �rF  hj�  �rG  hj�  �rH  hj�  �rI  hj�  �rJ  hj�  �rK  hj�  �rL  hj�  �rM  hj�  �rN  hj�  �rO  hj�  �rP  e�rQ  u�rR  j0  }rS  (hhj!  �rT  j3  j�  ]rU  (hKK�rV  �rW  hKK	�rX  �rY  hKK	�rZ  �r[  hKK
�r\  �r]  hKK�r^  �r_  hKK�r`  �ra  hKK�rb  �rc  hKK	�rd  �re  hKK	�rf  �rg  hKK
�rh  �ri  hKK	�rj  �rk  hKK
�rl  �rm  hKK�rn  �ro  hKK�rp  �rq  hKK�rr  �rs  hKK�rt  �ru  hKK�rv  �rw  hKK�rx  �ry  hKK	�rz  �r{  hKK
�r|  �r}  e�r~  j5  j�  ]r  (hj  �r�  hj  �r�  hj  �r�  hj  �r�  hj!  �r�  hj#  �r�  hj%  �r�  hj'  �r�  hj)  �r�  hj+  �r�  hj-  �r�  hj/  �r�  hj1  �r�  hj3  �r�  hj5  �r�  hj7  �r�  hj9  �r�  hj;  �r�  e�r�  u�r�  j0  }r�  (hhj!  �r�  j3  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  e�r�  j5  j�  ]r�  (hjV  �r�  hjX  �r�  hjZ  �r�  hj\  �r�  hj^  �r�  hj`  �r�  hjb  �r�  hjd  �r�  hjf  �r�  hjh  �r�  hjj  �r�  hjl  �r�  hjn  �r�  hjp  �r�  hjr  �r�  hjt  �r�  hjv  �r�  hjx  �r�  hjz  �r�  hj|  �r�  e�r�  u�r�  j0  }r�  (hhj!  �r�  j3  j�  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r   �r  hKK
�r  �r  hKK�r  �r  hKK	�r  �r  hKK
�r  �r	  e�r
  j5  j�  ]r  (hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r   e�r!  u�r"  j0  }r#  (hhj!  �r$  j3  j�  ]r%  (hKK�r&  �r'  hKK	�r(  �r)  hKK	�r*  �r+  hKK
�r,  �r-  hKK�r.  �r/  hKK�r0  �r1  hKK�r2  �r3  hKK	�r4  �r5  hKK	�r6  �r7  hKK
�r8  �r9  hKK	�r:  �r;  hKK
�r<  �r=  hKK�r>  �r?  hKK�r@  �rA  hKK�rB  �rC  hKK�rD  �rE  hKK�rF  �rG  hKK�rH  �rI  hKK	�rJ  �rK  hKK
�rL  �rM  hKK�rN  �rO  hKK	�rP  �rQ  hKK
�rR  �rS  hKK�rT  �rU  e�rV  j5  j�  ]rW  (hj�  �rX  hj�  �rY  hj�  �rZ  hj�  �r[  hj�  �r\  hj�  �r]  hj�  �r^  hj�  �r_  hj�  �r`  hj�  �ra  hj�  �rb  hj�  �rc  hj�  �rd  hj�  �re  hj�  �rf  hj�  �rg  hj�  �rh  hj�  �ri  hj   �rj  hj  �rk  hj  �rl  hj  �rm  hj  �rn  e�ro  u�rp  j0  }rq  (hhj!  �rr  j3  j�  ]rs  (hjO  �rt  hjP  �ru  hjQ  �rv  hjR  �rw  hjS  �rx  hjT  �ry  hjU  �rz  hjV  �r{  hjW  �r|  hjX  �r}  hjY  �r~  hjZ  �r  hj[  �r�  hj\  �r�  hj]  �r�  hj^  �r�  hj_  �r�  hj`  �r�  hja  �r�  hjb  �r�  hjc  �r�  hjd  �r�  hje  �r�  hjf  �r�  hjg  �r�  e�r�  j5  j�  ]r�  (hj&  �r�  hj(  �r�  hj*  �r�  hj,  �r�  hj.  �r�  hj0  �r�  hj2  �r�  hj4  �r�  hj6  �r�  hj8  �r�  hj:  �r�  hj<  �r�  hj>  �r�  hj@  �r�  hjB  �r�  hjD  �r�  hjF  �r�  hjH  �r�  hjJ  �r�  hjL  �r�  hjN  �r�  hjP  �r�  hjR  �r�  hjT  �r�  e�r�  u�r�  j�  }r�  (hhX
   Target_3x3r�  �r�  j�  hG@      G@"      �r�  �r�  j�  hK�r�  j�  hj�  �r�  j�  h��r�  u�r�  j�  }r�  (h`h`j�
  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j0  }r�  (hhX   GarveySpotsr�  �r�  j3  hK�r�  j5  hK �r�  u�r�  j�  }r�  (h`h`j�
  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j0  }r�  (hhj�  �r�  j3  hK�r�  j5  hK�r�  u�r�  j�  }r�  (h`h`j  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j0  }r�  (hhj�  �r�  j3  hK�r�  j5  hK�r�  u�r�  j�  }r�  (h`h`j  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j0  }r�  (hhj�  �r�  j3  hK�r�  j5  hK�r�  u�r�  j�  }r�  (h`h`j  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j0  }r�  (hhj�  �r�  j3  hK�r�  j5  hK�r�  u�r�  j�  }r�  (h`h`j  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j0  }r�  (hhj�  �r�  j3  hK�r�  j5  hK�r�  u�r�  j�  }r�  (h`h`j  �r�  j�  h��r�  j�  j�  ]r�  �r�  u�r�  j0  }r�  (hhj�  �r�  j3  hK�r�  j5  hK�r�  u�r�  j�  }r   (h`h`j  �r  j�  h��r  j�  j�  ]r  �r  u�r  j0  }r  (hhj�  �r  j3  hK�r  j5  hK�r	  u�r
  j�  }r  (h`h`j  �r  j�  h��r  j�  j�  ]r  �r  u�r  j0  }r  (hhj�  �r  j3  hK	�r  j5  hK�r  u�r  j�  }r  (h`h`j$  �r  j�  h��r  j�  j�  ]r  �r  u�r  j0  }r  (hhj�  �r  j3  hK
�r  j5  hK	�r  u�r   j�  }r!  (h`h`j)  �r"  j�  h��r#  j�  j�  ]r$  �r%  u�r&  j0  }r'  (hhj�  �r(  j3  hK�r)  j5  hK
�r*  u�r+  j�  }r,  (h`h`j.  �r-  j�  h��r.  j�  j�  ]r/  �r0  u�r1  j0  }r2  (hhj�  �r3  j3  hK�r4  j5  hK�r5  u�r6  j�  }r7  (h`h`j3  �r8  j�  h��r9  j�  j�  ]r:  �r;  u�r<  j0  }r=  (hhj�  �r>  j3  hK�r?  j5  hK�r@  u�rA  j�  }rB  (h`h`j8  �rC  j�  h��rD  j�  j�  ]rE  �rF  u�rG  j0  }rH  (hhj�  �rI  j3  hK�rJ  j5  hK�rK  u�rL  j�  }rM  (h`h`j=  �rN  j�  h��rO  j�  j�  ]rP  �rQ  u�rR  j0  }rS  (hhj�  �rT  j3  hK�rU  j5  hK�rV  u�rW  j�  }rX  (h`h`jB  �rY  j�  h��rZ  j�  j�  ]r[  �r\  u�r]  j0  }r^  (hhj�  �r_  j3  hK�r`  j5  hK�ra  u�rb  j�  }rc  j�  h��rd  s�re  j�  }rf  j�  j�  ]rg  (j�  j�  }rh  (j�  j�  h�ri  j�  hK�rj  hnh(��������trk  �rl  u�rm  �rn  j�  j�  }ro  (j�  j�  hu�rp  j�  hK�rq  hnh(��������trr  �rs  u�rt  �ru  j�  j�  }rv  (j�  j�  h��rw  j�  hK�rx  hnh(��������try  �rz  u�r{  �r|  j�  j�  }r}  (j�  j�  h��r~  j�  hK�r  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  hˆr�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  h�r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j%  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  jB  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j`  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j7  �r�  j�  hK �r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  jM  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  jb  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  jv  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r�  �r�  j�  j�  }r�  (j�  j�  j�  �r�  j�  hK�r�  hnh(��������tr�  �r�  u�r    �r   j�  j�  }r   (j�  j�  j�  �r   j�  hK�r   hnh(��������tr   �r   u�r   �r   e�r	   s�r
   jl  }r   (j�  j�  jv  �r   j�  hJ�����r   j�  hK�r   u�r   j  }r   (j�  j�  jv  �r   j�  j�  Mq�r   j!  hN�r   j#  hN�r   u�r   jT  }r   (j�  j�  jv  �r   jF  hj�  �r   jX  j�  jY  }r   (j�  j�  jv  �r   j\  j�  j]  }r   (j�  j�  jv  �r   jF  hj�  �r   j�  j�  j�  }r   (j�  j�  jv  �r   j�  hN�r    u�r!   �r"   u�r#   �r$   u�r%   �r&   jj  j�  ]r'   �r(   jm  hN�r)   jo  hN�r*   u�r+   j=  }r,   j?  hj�  �r-   s�r.   j�  }r/   j�  h��r0   s�r1   j�  }r2   j�  j�  ]r3   (j�  j�  }r4   (j�  j�  h�r5   j�  hK�r6   hnh(��������tr7   �r8   u�r9   �r:   j�  j�  }r;   (j�  j�  hu�r<   j�  hK�r=   hnh(��������tr>   �r?   u�r@   �rA   j�  j�  }rB   (j�  j�  h��rC   j�  hK�rD   hnh(��������trE   �rF   u�rG   �rH   j�  j�  }rI   (j�  j�  h��rJ   j�  hK�rK   hnh(��������trL   �rM   u�rN   �rO   j�  j�  }rP   (j�  j�  hˆrQ   j�  hK�rR   hnh(��������trS   �rT   u�rU   �rV   j�  j�  }rW   (j�  j�  h�rX   j�  hK�rY   hnh(��������trZ   �r[   u�r\   �r]   j�  j�  }r^   (j�  j�  j  �r_   j�  hK�r`   hnh(��������tra   �rb   u�rc   �rd   j�  j�  }re   (j�  j�  j%  �rf   j�  hK�rg   hnh(��������trh   �ri   u�rj   �rk   j�  j�  }rl   (j�  j�  jB  �rm   j�  hK�rn   hnh(��������tro   �rp   u�rq   �rr   j�  j�  }rs   (j�  j�  j`  �rt   j�  hK�ru   hnh(��������trv   �rw   u�rx   �ry   j�  j�  }rz   (j�  j�  j�  �r{   j�  hK�r|   hnh(��������tr}   �r~   u�r   �r�   j�  j�  }r�   (j�  j�  j�  �r�   j�  hK�r�   hnh(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j�  �r�   j�  hK�r�   hnh(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j�  �r�   j�  hK�r�   hnh(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j  �r�   j�  hK�r�   hnh(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j  �r�   j�  hK�r�   hnh(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j7  �r�   j�  hK �r�   hnh(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  jM  �r�   j�  hK�r�   hnh(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  jb  �r�   j�  hK�r�   hnh(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j�  �r�   j�  hK�r�   hnh(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j�  �r�   j�  hK�r�   hnh(��������tr�   �r�   u�r�   �r�   j�  j�  }r�   (j�  j�  j�  �r�   j�  hK�r�   hnh(��������tr�   �r�   u�r�   �r�   e�r�   s�r�   j=  }r�   j?  hjg  �r�   s�r�   j�  }r�   �r�   j�  }r�   (j�  hj�  �r�   j�  hN�r�   u�r�   j0  }r�   (hhjp  �r�   j3  hK �r�   j5  hK �r�   u�r�   j�  }r�   (h`h`h_�r�   j�  h��r�   j�  j�  ]r�   j�  j�  }r�   (j�  j�  h�r�   j�  hN�r�   j�  j�  Mr�r�   j�  hh_�r�   j�  hhc�r�   j�  j�  ]r�   �r�   j�  j�  j�  }r�   (j�  j�  h�r�   j�  hK�r�   j�  hK �r�   u�r�   �r�   j�  h��r�   u�r�   �r�   a�r�   u�r�   j�  }r�   (hhX   Burningr�   �r�   j�  hG@       G@      �r�   �r�   j�  hK�r�   j�  hj�  �r�   j�  h��r�   u�r�   j0  }r�   (hhX	   whocares2r�   �r !  j3  hK�r!  j5  hK �r!  u�r!  j�  }r!  (h`h`j�  �r!  j�  h��r!  j�  j�  ]r!  j�  j�  }r!  (j�  j�  j�  �r	!  j�  hN�r
!  j�  j�  Ms�r!  j�  hj�  �r!  j�  hhc�r!  j�  j�  ]r!  �r!  j�  j�  j�  }r!  (j�  j�  j�  �r!  j�  hK�r!  j�  hK �r!  u�r!  �r!  j�  h��r!  u�r!  �r!  a�r!  u�r!  j�  }r!  (hhj�   �r!  j�  hG@      G@       �r!  �r!  j�  hK�r!  j�  hj�  �r !  j�  h��r!!  u�r"!  j0  }r#!  (hhj�   �r$!  j3  hK�r%!  j5  hK�r&!  u�r'!  j�  }r(!  (h`h`j�  �r)!  j�  h��r*!  j�  j�  ]r+!  j�  j�  }r,!  (j�  j�  j�  �r-!  j�  hN�r.!  j�  j�  Mt�r/!  j�  hj�  �r0!  j�  hhc�r1!  j�  j�  ]r2!  �r3!  j�  j�  j�  }r4!  (j�  j�  j�  �r5!  j�  hK�r6!  j�  hK �r7!  u�r8!  �r9!  j�  h��r:!  u�r;!  �r<!  a�r=!  u�r>!  j�  }r?!  (hhj�   �r@!  j�  hG@"      G@       �rA!  �rB!  j�  hK�rC!  j�  hj�  �rD!  j�  h��rE!  u�rF!  j0  }rG!  (hhj�   �rH!  j3  hK�rI!  j5  hK�rJ!  u�rK!  j�  }rL!  (h`h`j�  �rM!  j�  h��rN!  j�  j�  ]rO!  j�  j�  }rP!  (j�  j�  j�  �rQ!  j�  hN�rR!  j�  j�  Mu�rS!  j�  hj�  �rT!  j�  hhc�rU!  j�  j�  ]rV!  �rW!  j�  j�  j�  }rX!  (j�  j�  j�  �rY!  j�  hK�rZ!  j�  hK �r[!  u�r\!  �r]!  j�  h��r^!  u�r_!  �r`!  a�ra!  u�rb!  j�  }rc!  (hhj�   �rd!  j�  hG@       G@"      �re!  �rf!  j�  hK�rg!  j�  hj�  �rh!  j�  h��ri!  u�rj!  j0  }rk!  (hhj�   �rl!  j3  hK�rm!  j5  hK�rn!  u�ro!  j0  }rp!  (hhX   FireComplete2rq!  �rr!  j3  hK�rs!  j5  hK �rt!  u�ru!  j�  }rv!  (h`h`j�
  �rw!  j�  h��rx!  j�  j�  ]ry!  �rz!  u�r{!  j�  }r|!  (h`h`j�
  �r}!  j�  h��r~!  j�  j�  ]r!  �r�!  u�r�!  j�  }r�!  (h`h`j  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h`h`j  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h`h`j  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h`h`j  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h`h`j  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h`h`j  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h`h`j  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h`h`j$  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h`h`j)  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h`h`j.  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h`h`j3  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h`h`j8  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h`h`j=  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j�  }r�!  (h`h`jB  �r�!  j�  h��r�!  j�  j�  ]r�!  �r�!  u�r�!  j^  }r�!  (hhX
   Target_3x3r�!  �r�!  j�  hG@      G@"      �r�!  �r�!  j�  hK�r�!  je  hj�  �r�!  j�  h��r�!  j�  h��r�!  u�r�!  j0  }r�!  (hhX   GarvPositionr�!  �r�!  j3  hK �r�!  j5  hja  �r�!  u�r�!  eKqK �r�!  X   eventsr�!  ]r�!  X   supportsr�!  ]r�!  X   recordsr�!  }r�!  (X   killsr�!  ]r�!  X
   KillRecordr�!  }r�!  (j�  KX	   level_nidr�!  jl  X   killerr�!  j�  X   killeer�!  j�  u�r�!  aj  ]r�!  (X   DamageRecordr�!  }r�!  (j�  Kj�!  jl  X   dealerr�!  h�X   receiverr�!  j�  X   item_nidr�!  jq  X   over_damager�!  Kj  KX   kindr�!  j  u�r�!  j�!  }r�!  (j�  Kj�!  jl  j�!  j�  j�!  j�  j�!  j�  j�!  K
j  Kj�!  j
  u�r�!  j�!  }r "  (j�  Kj�!  jl  j�!  h�j�!  jv  j�!  jq  j�!  Kj  Kj�!  j  u�r"  j�!  }r"  (j�  Kj�!  jl  j�!  jv  j�!  h�j�!  j'  j�!  Kj  Kj�!  j  u�r"  eX   healingr"  ]r"  X   deathr"  ]r"  j�!  }r"  (j�  Kj�!  jl  j�!  Nj�!  jv  u�r	"  aj}  ]r
"  (X
   ItemRecordr"  }r"  (j�  Kj�!  jl  X   userr"  h�j�!  jq  u�r"  j"  }r"  (j�  Kj�!  jl  j"  j�  j�!  j�  u�r"  j"  }r"  (j�  Kj�!  jl  j"  h�j�!  jq  u�r"  ej`  ]r"  X   combat_resultsr"  ]r"  (X   CombatRecordr"  }r"  (j�  Kj�!  jl  X   attackerr"  h�X   defenderr"  j�  X   resultr"  j  u�r"  j"  }r"  (j�  Kj�!  jl  j"  j�  j"  j�  j"  j
  u�r"  j"  }r"  (j�  Kj�!  jl  j"  h�j"  jv  j"  j  u�r"  j"  }r "  (j�  Kj�!  jl  j"  jv  j"  h�j"  j  u�r!"  eX   turns_takenr""  ]r#"  (X   Recordr$"  }r%"  (j�  Kj�!  jl  u�r&"  j$"  }r'"  (j�  Kj�!  jl  u�r("  j$"  }r)"  (j�  Kj�!  jl  u�r*"  eX   levelsr+"  ]r,"  h]r-"  (X   LevelRecordr."  }r/"  (j�  Kj�!  jl  X   unit_nidr0"  h�j�  K
hh�u�r1"  j."  }r2"  (j�  Kj�!  jl  j0"  j�  j�  Khj�  u�r3"  j."  }r4"  (j�  Kj�!  jl  j0"  h�j�  Khh�u�r5"  ej�  ]r6"  uX   speak_stylesr7"  }r8"  (X	   __defaultr9"  }r:"  (hj9"  X   speakerr;"  NhNX   widthr<"  NjV  KX
   font_colorr="  NX	   font_typer>"  X   convor?"  X
   backgroundr@"  X   message_bg_baserA"  X	   num_linesrB"  KX   draw_cursorrC"  �X   message_tailrD"  X   message_bg_tailrE"  X   transparencyrF"  G?�������X   name_tag_bgrG"  X   name_tagrH"  X   flagsrI"  cbuiltins
set
rJ"  ]rK"  �rL"  RrM"  uX   __default_textrN"  }rO"  (hjN"  j;"  NhNj<"  NjV  G?�      j="  Nj>"  X   textrP"  j@"  X   menu_bg_baserQ"  jB"  K jC"  NjD"  NjF"  G?�������jG"  jQ"  jI"  jJ"  ]rR"  �rS"  RrT"  uX   __default_helprU"  }rV"  (hjU"  j;"  NhNj<"  NjV  G?�      j="  Nj>"  j?"  j@"  X   NonerW"  jB"  KjC"  �jD"  NjF"  G?�������jG"  jH"  jI"  jJ"  ]rX"  �rY"  RrZ"  uX   noirr["  }r\"  (hj["  j;"  NhNj<"  NjV  Nj="  X   whiter]"  j>"  Nj@"  X   menu_bg_darkr^"  jB"  NjC"  NjD"  jW"  jF"  NjG"  NjI"  jJ"  ]r_"  �r`"  Rra"  uX   hintrb"  }rc"  (hjb"  j;"  Nhcapp.utilities.enums
Alignments
rd"  X   centerre"  �rf"  Rrg"  j<"  K�jV  Nj="  Nj>"  Nj@"  X   menu_bg_parchmentrh"  jB"  KjC"  NjD"  jW"  jF"  NjG"  NjI"  jJ"  ]ri"  �rj"  Rrk"  uX	   cinematicrl"  }rm"  (hjl"  j;"  Nhjg"  j<"  NjV  Nj="  X   greyrn"  j>"  X   chapterro"  j@"  jW"  jB"  KjC"  �jD"  jW"  jF"  NjG"  NjI"  jJ"  ]rp"  �rq"  Rrr"  uX	   narrationrs"  }rt"  (hjs"  j;"  NhKKn�ru"  j<"  K�jV  Nj="  j]"  j>"  Nj@"  jQ"  jB"  NjC"  NjD"  jW"  jF"  NjG"  NjI"  jJ"  ]rv"  �rw"  Rrx"  uX   narration_topry"  }rz"  (hjy"  j;"  NhKK�r{"  j<"  K�jV  Nj="  j]"  j>"  Nj@"  jQ"  jB"  NjC"  NjD"  jW"  jF"  NjG"  NjI"  jJ"  ]r|"  �r}"  Rr~"  uX   clearr"  }r�"  (hj"  j;"  NhNj<"  NjV  Nj="  j]"  j>"  Nj@"  jW"  jB"  NjC"  �jD"  jW"  jF"  NjG"  NjI"  jJ"  ]r�"  �r�"  Rr�"  uX   thought_bubbler�"  }r�"  (hj�"  j;"  NhNj<"  NjV  Nj="  Nj>"  Nj@"  NjB"  NjC"  NjD"  X   message_bg_thought_tailr�"  jF"  NjG"  NjI"  jJ"  ]r�"  X   no_talkr�"  a�r�"  Rr�"  uX   boss_convo_leftr�"  }r�"  (hj�"  j;"  NhKHKp�r�"  j<"  K�jV  Kj="  Nj>"  j?"  j@"  jA"  jB"  KjC"  �jD"  jE"  jF"  G        jG"  NjI"  jJ"  ]r�"  �r�"  Rr�"  uX   boss_convo_rightr�"  }r�"  (hj�"  j;"  NhKKp�r�"  j<"  K�jV  Kj="  Nj>"  j?"  j@"  jA"  jB"  KjC"  �jD"  jE"  jF"  G        jG"  NjI"  jJ"  ]r�"  �r�"  Rr�"  uuX   market_itemsr�"  }r�"  X   unlocked_lorer�"  ]r�"  (j4  j8  j<  j@  jD  jH  eX
   dialog_logr�"  ]r�"  X   already_triggered_eventsr�"  ]r�"  X   talk_optionsr�"  ]r�"  X   base_convosr�"  }r�"  X   current_random_stater�"  Ja�FX   boundsr�"  (K K KKtr�"  X	   roam_infor�"  capp.engine.roam.roam_info
RoamInfo
r�"  )�r�"  }r�"  (X   roamr�"  �X   roam_unit_nidr�"  Nubu.