�}q (X   unitsq]q(}q(X   nidqX   AlpinqX
   prefab_nidqhX   positionqNX   teamqX   playerq	X   partyq
X   EirikaqX   klassqX   AlpinVillagerqX   variantqX    qX   factionqNX   levelqKX   expqK X   genericq�X
   persistentq�X   aiqX   NoneqX   roam_aiqNX   ai_groupqNX   itemsq]q(M�M�M�eX   nameqX   AlpinqX   descqXH   An energetic young lad. Dreams of joining the army of [KINGDOM] someday.qX   tagsq]q X   Youthq!aX   statsq"}q#(X   HPq$KX   STRq%KX   MAGq&K X   SKLq'KX   SPDq(KX   LCKq)KX   DEFq*KX   RESq+K X   CONq,KX   MOVq-KuX   growthsq.}q/(h$K<h%K#h&K h'Kh(Kh)Kh*Kh+Kh,K h-KduX   growth_pointsq0}q1(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uX   stat_cap_modifiersq2}q3(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uX   starting_positionq4NX   wexpq5}q6(X   Swordq7KX   Lanceq8K X   Axeq9K X   Bowq:K X   Staffq;K X   Lightq<K X   Animaq=K X   Darkq>K X   Defaultq?K uX   portrait_nidq@X   AlpinqAX   affinityqBX   FireqCX   skillsqD]qE(MsM�capp.engine.source_type
SourceType
qFX   itemqG���qH�qIRqJ�qKMtX   gameqLhFX   globalqM���qN�qORqP�qQMuhhFX   personalqR���qS�qTRqU�qVMvhhU�qWeX   notesqX]qYX
   current_hpqZKX   current_manaq[K X   current_fatigueq\K X   travelerq]NX   current_guard_gaugeq^K X   built_guardq_�X   deadq`�X   action_stateqa(��������tqbX   _fieldsqc}qdX   equipped_weaponqeM�X   equipped_accessoryqfNu}qg(hX   CarlinqhhhhhNhX   playerqih
hhX   CarlinVillagerqjhhhNhKhK h�h�hX   NoneqkhNhNh]ql(M�M�M�M�ehX   CarlinqmhXA   A magically gifted youngster from [TOWN]. Energetic and sporatic.qnh]qoX   Youthqpah"}qq(h$K
h%K h&Kh'Kh(Kh)Kh*Kh+Kh,Kh-Kuh.}qr(h$K2h%K h&Kh'K#h(Kh)Kh*Kh+Kh,K h-Kuh0}qs(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}qt(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}qu(h7K h8K h9K h:K h;K h<K h=K h>Kh?K uh@X   CarlinqvhBX   WindqwhD]qx(MwM�hJ�qyMxhLhP�qzMyhhhU�q{MzhhhU�q|ehX]q}hZK
h[K h\K h]Nh^K h_�h`�ha(��������tq~hc}qheM�hfNu}q�(hX   Shaylaq�hh�hKK�q�hX   playerq�h
hhX   ShaylaVillagerq�hhhNhKhK h�h�hX   Noneq�hNhNh]q�(M�M�M�ehX   Shaylaq�hXI   A clumsy young lass who hopes to adventure the world. Gutsy and fearless.q�h]q�X   Youthq�ah"}q�(h$Kh%Kh&K h'Kh(Kh)Kh*Kh+K h,Kh-Kuh.}q�(h$KAh%Kh&K h'Kh(Kh)Kh*K#h+Kh,K h-Kuh0}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}q�(h7K h8K h9Kh:K h;K h<K h=K h>K h?K uh@X   Shaylaq�hBX   Thunderq�hD]q�(M{M�hJ�q�M|hLhP�q�M}h�hU�q�M~h�hU�q�ehX]q�hZKh[K h\K h]Nh^K h_�h`�ha(��������tq�hc}q�heM�hfNu}q�(hX   Raelinq�hh�hK K�q�hX   playerq�h
hhX   ZoyaVillagerq�hhhNhKhK h�h�hX   Noneq�hNhNh]q�(M�M�M�ehX   Raelinq�hX8   The shut-in daughter of a fisherman. Timid and cautious.q�h]q�X   Youthq�ah"}q�(h$Kh%Kh&K h'Kh(Kh)Kh*Kh+Kh,Kh-Kuh.}q�(h$K7h%Kh&K h'Kh(K#h)Kh*Kh+Kh,K h-Kuh0}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}q�(h7K h8Kh9K h:K h;K h<K h=K h>K h?K uh@X   Zoyaq�hBX   Iceq�hD]q�(MM�hJ�q�M�hLhP�q�M�h�hU�q�M�h�hU�q�ehX]q�hZKh[K h\K h]Nh^K h_�h`�ha(��������tq�hc}q�heM�hfNu}q�(hX   Elwynnq�hh�hK K�q�hX   playerq�h
hhX   Monkq�hhhNhKhK h�h�hX   Noneq�hNhNh]q�(M�M�M�ehX   Elwynnq�hXM   Former vagabond turned holy monk of [CHURCH]. Stern and generally unpleasant.q�h]q�X   Adultq�ah"}q�(h$Kh%K h&Kh'Kh(Kh)Kh*Kh+Kh,Kh-Kuh.}q�(h$K7h%K h&Kh'Kh(Kh)K
h*Kh+K#h,K h-Kuh0}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}q�(h7K h8K h9K h:K h;K h<Kh=K h>K h?K uh@X   Elwynnq�hBX   Darkq�hD]q�(M�hLhP�q�M�h�hU�q�M�h�hU�q�M�h�hU�q�ehX]q�hZKh[K h\K h]Nh^K h_�h`�ha(��������tq�hc}q�heM�hfNu}q�(hX   Quinleyq�hh�hKK�q�hX   playerq�h
hhX   Mageq�hX   Quinleyq�hNhKhK h�h�hX   Noneq�hNhNh]q�(M�M�M�ehX   Quinleyq�hXN   A genius magician who's grown tired of elemental magic. Outgoing and friendly.q�h]q�X   Adultq�ah"}q�(h$Kh%K h&Kh'Kh(Kh)Kh*Kh+Kh,Kh-Kuh.}q�(h$K2h%K h&K#h'Kh(Kh)Kh*Kh+Kh,K h-Kuh0}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}q�(h7K h8K h9K h:K h;K h<K h=Kh>K h?K uh@X   Quinleyq�hBX   Darkq�hD]q�(M�M�hJ�q�M�hLhP�q�M�h�hU�q�M�h�hU�q�M�h�hU�q�ehX]q�hZKh[K h\K h]Nh^K h_�h`�ha(��������tq�hc}q�heM�hfNu}q�(hX   Saraidq�hh�hKK�q�hX   playerq�h
hhX   Hunterq�hhhNhKhK h�h�hX   Noneq�hNhhh]q�(M�M�M�ehX   Saraidq�hX0   A fledgling huntress. Unreserved and unfiltered.q�h]q�X   Adultq�ah"}q�(h$Kh%Kh&Kh'Kh(Kh)Kh*Kh+Kh,Kh-Kuh.}q�(h$K2h%Kh&Kh'Kh(K#h)Kh*Kh+Kh,K h-Kuh0}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}q�(h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}q�(h7K h8K h9K h:Kh;K h<K h=K h>K h?K uh@X   Sloaneq�hBX   Lightq�hD]q�(M�M�hJ�q�M�hLhP�q�M�h�hU�q�M�h�hU�r   M�h�hU�r  ehX]r  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr  hc}r  heM�hfNu}r  (hX   Embersonr  hj  hK	K�r  hX   enemyr  h
hhX   Sager	  hhhNhKhK h�h�hX   AttackInRanger
  hNhhh]r  (M�M�M�M�ehX   Embersonr  hX   Emberson text.r  h]r  X   Bossr  ah"}r  (h$Kh%K h&Kh'Kh(Kh)K h*Kh+Kh,K	h-Kuh.}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r  (h7K h8K h9K h:K h;Kh<Kh=Kh>K h?K uh@X   Embersonr  hBX   Firer  hD]r  (M�hLhP�r  M�j  hU�r  ehX]r  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr  hc}r  heM�hfNu}r  (hX   Elspethr  hj  hKK�r  hX   playerr   h
hhX   Clericr!  hX   Elspethr"  hNhKhK h�h�hX   Noner#  hNhhh]r$  M�ahX   Elspethr%  hX2   Lifelong devotee of [CHURCH]. Humble and selfless.r&  h]r'  X   Adultr(  ah"}r)  (h$Kh%K h&Kh'K h(Kh)Kh*K h+Kh,Kh-Kuh.}r*  (h$K2h%K h&Kh'Kh(Kh)K#h*Kh+K#h,K h-Kuh0}r+  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r,  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r-  (h7K h8K h9K h:K h;Kh<K h=K h>K h?K uh@X   Elspethr.  hBX   Lightr/  hD]r0  (M�hLhP�r1  M�j  hU�r2  M�j  hU�r3  M�j  hU�r4  M�NhFX   defaultr5  ���r6  �r7  Rr8  �r9  ehX]r:  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr;  hc}r<  heNhfNu}r=  (hX   Lamonter>  hj>  hKK�r?  hX   playerr@  h
hhX   SoldierrA  hX   LamonterB  hNhKhK h�h�hX   NonerC  hNhhh]rD  (M�M�M�ehX   LamonterE  hX,   One of Leod's guards. Kindhearted and loyal.rF  h]rG  X   AdultrH  ah"}rI  (h$Kh%Kh&K h'Kh(Kh)Kh*Kh+K h,K
h-Kuh.}rJ  (h$KAh%Kh&K h'Kh(Kh)K
h*K#h+Kh,K h-Kuh0}rK  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}rL  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}rM  (h7K h8Kh9K h:K h;K h<K h=K h>K h?K uh@X   LamonterN  hBX   IcerO  hD]rP  (M�hLhP�rQ  M�j>  hU�rR  M�j>  hU�rS  M�j>  hU�rT  M�M�hJ�rU  ehX]rV  hZKh[K h\K h]Nh^K h_�h`�ha(��������trW  hc}rX  heM�hfNu}rY  (hX   GarveyrZ  hjZ  hNhX   playerr[  h
hhX   Archerr\  hX   Garveyr]  hNhKhK h�h�hX   Noner^  hNhhh]r_  (M�M�M�ehX   Garveyr`  hXA   One of Leod's guards. Dislikes his hometown and fellow villagers.ra  h]rb  X   Adultrc  ah"}rd  (h$Kh%Kh&K h'Kh(Kh)Kh*Kh+K h,K
h-Kuh.}re  (h$K7h%K#h&Kh'Kh(Kh)K
h*Kh+Kh,K h-Kuh0}rf  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}rg  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}rh  (h7K h8K h9K h:Kh;K h<K h=K h>K h?K uh@X   Garveyri  hBX   Firerj  hD]rk  (M�hLhP�rl  M�jZ  hU�rm  M�jZ  hU�rn  M�M�hJ�ro  ehX]rp  hZKh[K h\K h]Nh^K h_�h`�ha(��������trq  hc}rr  heM�hfNu}rs  (hX   Leodrt  hjt  hNhX   otherru  h
hhX   Manrv  hhhNhKhK h�h�hX   Nonerw  hNhhh]rx  hX   Leodry  hX0   The mayor of [TOWN]. Pompous and short tempered.rz  h]r{  X	   Ch2Ignorer|  ah"}r}  (h$Kh%Kh&K h'Kh(Kh)Kh*Kh+K h,Kh-Kuh.}r~  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7K h8K h9K h:K h;K h<K h=K h>K h?K uh@X   Leodr�  hBX   Darkr�  hD]r�  M�hLhP�r�  ahX]r�  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heNhfNu}r�  (hX	   Kingdom_1r�  hj�  hNhX   otherr�  h
hhX   Soldierr�  hNhX   Kingdomr�  hKhK h�h�hX   Noner�  hNhhh]r�  hX   Kingdomr�  hX   Kingdom Soldier.r�  h]r�  h"}r�  (h$Kh%Kh&K h'Kh(Kh)K h*Kh+K h,Kh-Kuh.}r�  (h$KFh%Kh&K h'Kh(Kh)K h*Kh+K h,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7K h8Kh9K h:K h;K h<K h=K h>K h?K uh@NhBNhD]r�  M�hLhP�r�  ahX]r�  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heNhfNu}r�  (hX	   Kingdom_2r�  hj�  hNhX   otherr�  h
hhX   Soldierr�  hNhX   Kingdomr�  hKhK h�h�hX   Noner�  hNhhh]r�  hj�  hj�  h]r�  h"}r�  (h$Kh%Kh&K h'Kh(Kh)K h*Kh+K h,Kh-Kuh.}r�  (h$KFh%Kh&K h'Kh(Kh)K h*Kh+K h,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7K h8Kh9K h:K h;K h<K h=K h>K h?K uh@NhBNhD]r�  M�hLhP�r�  ahX]r�  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heNhfNu}r�  (hX	   Kingdom_3r�  hj�  hNhX   otherr�  h
hhX   Knightr�  hNhX   Kingdomr�  hKhK h�h�hX   Noner�  hNhhh]r�  hj�  hj�  h]r�  h"}r�  (h$K	h%Kh&K h'Kh(Kh)K h*Kh+K h,Kh-Kuh.}r�  (h$KPh%K
h&K h'K
h(K
h)K h*K#h+K h,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7K h8Kh9K h:K h;K h<K h=K h>K h?K uh@NhBNhD]r�  M�hLhP�r�  ahX]r�  hZK	h[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heNhfNu}r�  (hX	   Kingdom_4r�  hj�  hNhX   otherr�  h
hhX   Knightr�  hNhX   Kingdomr�  hKhK h�h�hX   Noner�  hNhhh]r�  hj�  hj�  h]r�  h"}r�  (h$K	h%Kh&K h'Kh(Kh)K h*Kh+K h,Kh-Kuh.}r�  (h$KPh%K
h&K h'K
h(K
h)K h*K#h+K h,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7K h8Kh9K h:K h;K h<K h=K h>K h?K uh@NhBNhD]r�  M�hLhP�r�  ahX]r�  hZK	h[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heNhfNu}r�  (hX	   Kingdom_5r�  hj�  hNhX   otherr�  h
hhX   Archerr�  hNhX   Kingdomr�  hKhK h�h�hX   Noner�  hNhhh]r�  hj�  hj�  h]r�  h"}r�  (h$Kh%Kh&K h'Kh(Kh)K h*Kh+Kh,Kh-Kuh.}r�  (h$K7h%Kh&K h'Kh(Kh)K h*K h+K h,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7K h8K h9K h:Kh;K h<K h=K h>K h?K uh@NhBNhD]r�  M�hLhP�r�  ahX]r�  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heNhfNu}r�  (hX	   Kingdom_6r�  hj�  hNhX   otherr�  h
hhX   Archerr�  hNhX   Kingdomr�  hKhK h�h�hX   Noner�  hNhhh]r�  hj�  hj�  h]r�  h"}r�  (h$Kh%Kh&K h'Kh(Kh)K h*Kh+Kh,Kh-Kuh.}r�  (h$K7h%Kh&K h'Kh(Kh)K h*K h+K h,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7K h8K h9K h:Kh;K h<K h=K h>K h?K uh@NhBNhD]r�  M�hLhP�r�  ahX]r�  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heNhfNu}r�  (hX	   Kingdom_7r�  hj�  hNhX   otherr�  h
hhX   Generalr�  hNhX   Kingdomr�  hKhK h�h�hX   Noner�  hNhhh]r�  hj�  hj�  h]r�  h"}r�  (h$Kh%K
h&K h'Kh(Kh)Kh*K
h+Kh,Kh-Kuh.}r   (h$KKh%Kh&K h'Kh(K
h)K
h*Kh+Kh,K h-K uh0}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r  (h7Kh8Kh9Kh:K h;K h<K h=K h>K h?K uh@NhBNhD]r  M�hLhP�r  ahX]r  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr  hc}r  heNhfNu}r	  (hX	   Kingdom_8r
  hj
  hNhX   otherr  h
hhX   Generalr  hNhX   Kingdomr  hKhK h�h�hX   Noner  hNhhh]r  hj�  hj�  h]r  h"}r  (h$Kh%K
h&K h'Kh(Kh)Kh*K
h+Kh,Kh-Kuh.}r  (h$KKh%Kh&K h'Kh(K
h)K
h*Kh+Kh,K h-K uh0}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r  (h7Kh8Kh9Kh:K h;K h<K h=K h>K h?K uh@NhBNhD]r  M�hLhP�r  ahX]r  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr  hc}r  heNhfNu}r  (hX   Laisrenr  hj  hNhX   otherr  h
hhX   Cavalierr  hX   Cathalr  hNhKhK h�h�hX   Noner   hNhhh]r!  (M�M�M�ehX   Laisrenr"  hXV   An aspiring recruit in [KINGDOM]'s army. Charming and outgoing. Alpin's older brother.r#  h]r$  X   Adultr%  ah"}r&  (h$Kh%Kh&K h'Kh(Kh)Kh*Kh+Kh,Kh-Kuh.}r'  (h$KAh%K#h&K#h'K#h(K#h)K#h*K#h+K#h,K h-Kuh0}r(  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r)  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r*  (h7Kh8Kh9K h:K h;K h<K h=K h>K h?K uh@X   Cathalr+  hBX   Darkr,  hD]r-  (M�hLhP�r.  M�j  hU�r/  M�j  hU�r0  M�j  hFh���r1  �r2  Rr3  �r4  ehX]r5  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr6  hc}r7  heM�hfNu}r8  (hX   Kingr9  hj9  hNhX   otherr:  h
hhX   Generalr;  hX   Kingr<  hNhK
hK h�h�hX   Noner=  hNhhh]r>  M�ahX   Kingr?  hX
   King text.r@  h]rA  h"}rB  (h$Kh%Kh&K h'Kh(Kh)K h*Kh+Kh,Kh-Kuh.}rC  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}rD  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}rE  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}rF  (h7Kh8Kh9Kh:K h;K h<K h=K h>K h?K uh@X   KingrG  hBX   LightrH  hD]rI  M�hLhP�rJ  ahX]rK  hZKh[K h\K h]Nh^K h_�h`�ha(��������trL  hc}rM  heM�hfNu}rN  (hX   KeevarO  hjO  hNhX   otherrP  h
hhX   BishoprQ  hhhNhK
hK h�h�hX   NonerR  hNhhh]rS  M�ahX   KeevarT  hX.   [KINGDOM]'s Court mage who holds many secrets.rU  h]rV  h"}rW  (h$Kh%K h&Kh'Kh(Kh)K h*Kh+K	h,Kh-Kuh.}rX  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}rY  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}rZ  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r[  (h7K h8K h9K h:K h;Kh<Kh=K h>K h?K uh@X   Keevar\  hBX   Darkr]  hD]r^  M�hLhP�r_  ahX]r`  hZKh[K h\K h]Nh^K h_�h`�ha(��������tra  hc}rb  heM�hfNu}rc  (hX	   Kingdom_9rd  hjd  hNhX   otherre  h
hhX   Paladinrf  hNhX   Kingdomrg  hKhK h�h�hX   Nonerh  hNhhh]ri  hj�  hj�  h]rj  h"}rk  (h$Kh%K	h&Kh'K
h(K
h)Kh*K
h+Kh,Kh-Kuh.}rl  (h$KFh%Kh&Kh'K#h(Kh)K
h*Kh+Kh,K h-K uh0}rm  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}rn  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}ro  (h7Kh8Kh9K h:K h;K h<K h=K h>K h?K uh@NhBNhD]rp  (M�hLhP�rq  M�jf  j3  �rr  ehX]rs  hZKh[K h\K h]Nh^K h_�h`�ha(��������trt  hc}ru  heNhfNu}rv  (hX
   Kingdom_10rw  hjw  hNhX   otherrx  h
hhX   Paladinry  hNhX   Kingdomrz  hKhK h�h�hX   Noner{  hNhhh]r|  hj�  hj�  h]r}  h"}r~  (h$Kh%K
h&Kh'K
h(K
h)Kh*K
h+K
h,Kh-Kuh.}r  (h$KFh%Kh&Kh'K#h(Kh)K
h*Kh+Kh,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7Kh8Kh9K h:K h;K h<K h=K h>K h?K uh@NhBNhD]r�  (M�hLhP�r�  M�jy  j3  �r�  ehX]r�  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heNhfNu}r�  (hX
   Kingdom_11r�  hj�  hNhX   otherr�  h
hhX   Great_Knightr�  hNhX   Kingdomr�  hKhK h�h�hX   Noner�  hNhhh]r�  hj�  hj�  h]r�  h"}r�  (h$Kh%K
h&Kh'K
h(Kh)Kh*K
h+Kh,Kh-Kuh.}r�  (h$KFh%Kh&Kh'Kh(Kh)K
h*Kh+Kh,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7Kh8Kh9Kh:K h;K h<K h=K h>K h?K uh@NhBNhD]r�  (M�hLhP�r�  M�j�  j3  �r�  ehX]r�  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heNhfNu}r�  (hX
   Kingdom_12r�  hj�  hNhX   otherr�  h
hhX   Great_Knightr�  hNhX   Kingdomr�  hKhK h�h�hX   Noner�  hNhhh]r�  hj�  hj�  h]r�  h"}r�  (h$Kh%K
h&Kh'Kh(K
h)Kh*K
h+Kh,Kh-Kuh.}r�  (h$KFh%Kh&Kh'Kh(Kh)K
h*Kh+Kh,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7Kh8Kh9Kh:K h;K h<K h=K h>K h?K uh@NhBNhD]r�  (M�hLhP�r�  M�j�  j3  �r�  ehX]r�  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heNhfNu}r�  (hX
   Kingdom_13r�  hj�  hNhX   otherr�  h
hhX   Generalr�  hNhX   Kingdomr�  hKhK h�h�hX   Noner�  hNhhh]r�  hj�  hj�  h]r�  h"}r�  (h$Kh%K
h&K h'Kh(Kh)K h*K
h+Kh,Kh-Kuh.}r�  (h$KKh%Kh&K h'Kh(K
h)K
h*Kh+Kh,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7Kh8Kh9Kh:K h;K h<K h=K h>K h?K uh@NhBNhD]r�  M�hLhP�r�  ahX]r�  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heNhfNu}r�  (hX
   Kingdom_14r�  hj�  hNhX   otherr�  h
hhX   Generalr�  hNhX   Kingdomr�  hKhK h�h�hX   Noner�  hNhhh]r�  hj�  hj�  h]r�  h"}r�  (h$Kh%K
h&K h'Kh(Kh)Kh*K
h+Kh,Kh-Kuh.}r�  (h$KKh%Kh&K h'Kh(K
h)K
h*Kh+Kh,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7Kh8Kh9Kh:K h;K h<K h=K h>K h?K uh@NhBNhD]r�  M�hLhP�r�  ahX]r�  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heNhfNu}r�  (hX
   Kingdom_15r�  hj�  hNhX   otherr�  h
hhX   Generalr�  hNhX   Kingdomr�  hKhK h�h�hX   Noner�  hNhhh]r�  hj�  hj�  h]r�  h"}r�  (h$Kh%K
h&K h'Kh(Kh)Kh*K
h+Kh,Kh-Kuh.}r�  (h$KKh%Kh&K h'Kh(K
h)K
h*Kh+Kh,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7Kh8Kh9Kh:K h;K h<K h=K h>K h?K uh@NhBNhD]r�  M�hLhP�r�  ahX]r�  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heNhfNu}r�  (hX
   Kingdom_16r�  hj�  hNhX   otherr�  h
hhX   Generalr�  hNhX   Kingdomr�  hKhK h�h�hX   Noner�  hNhhh]r�  hj�  hj�  h]r�  h"}r�  (h$Kh%K
h&K h'Kh(Kh)Kh*K
h+Kh,Kh-Kuh.}r�  (h$KKh%Kh&K h'Kh(K
h)K
h*Kh+Kh,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7Kh8Kh9Kh:K h;K h<K h=K h>K h?K uh@NhBNhD]r�  M�hLhP�r�  ahX]r�  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heNhfNu}r�  (hX
   Kingdom_17r�  hj�  hNhX   otherr�  h
hhX   Hunterr�  hNhX   Kingdomr�  hKhK h�h�hX   Noner�  hNhhh]r�  hj�  hj�  h]r�  h"}r�  (h$Kh%Kh&K h'Kh(Kh)K h*K h+K h,Kh-Kuh.}r   (h$KAh%K(h&Kh'K#h(Kh)Kh*Kh+Kh,K h-K uh0}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r  (h7K h8K h9K h:Kh;K h<K h=K h>K h?K uh@NhBNhD]r  M�hLhP�r  ahX]r  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr  hc}r  heNhfNu}r	  (hX
   Kingdom_18r
  hj
  hNhX   otherr  h
hhX   Hunterr  hNhX   Kingdomr  hKhK h�h�hX   Noner  hNhhh]r  hj�  hj�  h]r  h"}r  (h$Kh%Kh&K h'Kh(Kh)K h*K h+K h,Kh-Kuh.}r  (h$KAh%K(h&Kh'K#h(Kh)Kh*Kh+Kh,K h-K uh0}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r  (h7K h8K h9K h:Kh;K h<K h=K h>K h?K uh@NhBNhD]r  M�hLhP�r  ahX]r  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr  hc}r  heNhfNu}r  (hX
   Kingdom_19r  hj  hNhX   otherr  h
hhX   AlpinVillagerr  hNhX   Kingdomr  hKhK h�h�hX   Noner   hNhhh]r!  hj�  hj�  h]r"  h"}r#  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-Kuh.}r$  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}r%  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r&  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r'  (h7Kh8K h9K h:K h;K h<K h=K h>K h?K uh@NhBNhD]r(  M�hLhP�r)  ahX]r*  hZK h[K h\K h]Nh^K h_�h`�ha(��������tr+  hc}r,  heNhfNu}r-  (hX
   Kingdom_20r.  hj.  hNhX   otherr/  h
hhX   AlpinVillagerr0  hNhX   Kingdomr1  hKhK h�h�hX   Noner2  hNhhh]r3  hj�  hj�  h]r4  h"}r5  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-Kuh.}r6  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh0}r7  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r8  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r9  (h7Kh8K h9K h:K h;K h<K h=K h>K h?K uh@NhBNhD]r:  M�hLhP�r;  ahX]r<  hZK h[K h\K h]Nh^K h_�h`�ha(��������tr=  hc}r>  heNhfNu}r?  (hX
   Kingdom_21r@  hj@  hNhX   otherrA  h
hhX   HerorB  hNhX   KingdomrC  hKhK h�h�hX   NonerD  hNhhh]rE  hj�  hj�  h]rF  h"}rG  (h$Kh%Kh&Kh'K	h(Kh)K h*Kh+Kh,Kh-Kuh.}rH  (h$KKh%Kh&Kh'Kh(Kh)Kh*Kh+Kh,K h-K uh0}rI  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}rJ  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}rK  (h7Kh8K h9Kh:K h;K h<K h=K h>K h?K uh@NhBNhD]rL  M�hLhP�rM  ahX]rN  hZKh[K h\K h]Nh^K h_�h`�ha(��������trO  hc}rP  heNhfNu}rQ  (hX
   Kingdom_22rR  hjR  hNhX   otherrS  h
hhX   HerorT  hNhX   KingdomrU  hKhK h�h�hX   NonerV  hNhhh]rW  hj�  hj�  h]rX  h"}rY  (h$Kh%Kh&K h'Kh(Kh)Kh*Kh+Kh,Kh-Kuh.}rZ  (h$KKh%Kh&Kh'Kh(Kh)Kh*Kh+Kh,K h-K uh0}r[  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r\  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r]  (h7Kh8K h9Kh:K h;K h<K h=K h>K h?K uh@NhBNhD]r^  M�hLhP�r_  ahX]r`  hZKh[K h\K h]Nh^K h_�h`�ha(��������tra  hc}rb  heNhfNu}rc  (hX
   Kingdom_23rd  hjd  hNhX   otherre  h
hhX   Bishoprf  hNhX   Kingdomrg  hKhK h�h�hX   Nonerh  hNhhh]ri  hj�  hj�  h]rj  h"}rk  (h$Kh%K h&Kh'Kh(Kh)K h*Kh+K
h,Kh-Kuh.}rl  (h$K-h%K h&K#h'Kh(Kh)Kh*Kh+K2h,K h-K uh0}rm  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}rn  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}ro  (h7K h8K h9K h:K h;Kh<Kh=K h>K h?K uh@NhBNhD]rp  M�hLhP�rq  ahX]rr  hZKh[K h\K h]Nh^K h_�h`�ha(��������trs  hc}rt  heNhfNu}ru  (hX
   Kingdom_24rv  hjv  hNhX   otherrw  h
hhX   Bishoprx  hNhX   Kingdomry  hKhK h�h�hX   Nonerz  hNhhh]r{  hj�  hj�  h]r|  h"}r}  (h$Kh%K h&Kh'Kh(Kh)Kh*Kh+K
h,Kh-Kuh.}r~  (h$K-h%K h&K#h'Kh(Kh)Kh*Kh+K2h,K h-K uh0}r  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7K h8K h9K h:K h;Kh<Kh=K h>K h?K uh@NhBNhD]r�  M�hLhP�r�  ahX]r�  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heNhfNu}r�  (hX   Thiefr�  hj�  hKK�r�  hX   enemyr�  h
hhX   Thiefr�  hNhX   Looterr�  hKhK h�h�hX   Noner�  hNhhh]r�  (M�M�ehX   Looterr�  hX,   Underhanded thugs who loot amidst the chaos.r�  h]r�  h"}r�  (h$Kh%Kh&K h'Kh(Kh)K h*K h+K h,Kh-Kuh.}r�  (h$K(h%K
h&K h'Kh(K#h)K h*K h+K h,K h-K uh0}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh2}r�  (h$K h%K h&K h'K h(K h)K h*K h+K h,K h-K uh4Nh5}r�  (h7Kh8K h9K h:K h;K h<K h=K h>K h?K uh@NhBNhD]r�  (M�hLhP�r�  M�M�hFX   aurar�  ���r�  �r�  Rr�  �r�  ehX]r�  hZKh[K h\K h]Nh^K h_�h`�ha(��������tr�  hc}r�  heM�hfNueh]r�  (}r�  (X   uidr�  M�hX
   Iron Swordr�  hX
   Iron Swordr�  hhX	   owner_nidr�  hX	   droppabler�  �X   datar�  }r�  X   subitemsr�  ]r�  X
   componentsr�  ]r�  (X   weaponr�  N�r�  X   target_enemyr�  N�r�  X	   min_ranger�  K�r�  X	   max_ranger�  K�r�  X   damager�  K�r�  X   hitr�  K
�r�  X	   level_expr�  N�r�  X   weapon_typer�  X   Swordr�  �r�  X   critr�  K �r�  eu}r�  (j�  M�hX   HeartOfFirer�  hX    <orange>Heart Of Flames</orange>r�  hX   +1 Damage. Fire affinity only.r�  j�  hj�  �j�  }r�  j�  ]r�  j�  ]r�  (X   status_on_holdr�  X   HeartofFirer�  �r�  X
   text_colorr�  X   oranger�  �r�  eu}r�  (j�  M�hX   Herbsr�  hX   Herbsr�  hX(   Restores 4 HP. Unit can act after using.r�  j�  hj�  �j�  }r�  (X   usesr�  KX   starting_usesr�  Kuj�  ]r�  j�  ]r�  (X   usabler�  N�r�  X   target_allyr�  N�r�  j�  K�r�  X   uses_optionsr�  ]r�  ]r�  (X   LoseUsesOnMiss (T/F)r�  X   Fr�  X   Lose uses even on missr�  ea�r�  X   healr�  K�r�  X   map_hit_add_blendr�  ]r�  (K`K�K�e�r�  X   attack_after_combatr�  N�r�  eu}r�  (j�  M�hX   Jinxr�  hX   Jinxr�  hX,   Lowers target's Crit Avoid by 20 for 1 turn.r�  j�  hhj�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  N�r�  j�  X   Darkr�  �r�  j�  ]r�  (KHK KAe�r�  X   magicr�  N�r�  X   unrepairabler�  N�r�  X   status_on_hitr   X	   Terrifiedr  �r  j�  K �r  X   battle_cast_animr  X   Fluxr  �r  eu}r  (j�  M�hX   Devilryr  hX   Devilryr	  hX   HP Cost: 4. -2 SPD.r
  j�  hhj�  �j�  }r  (j�  Kj�  Kuj�  ]r  j�  ]r  (j�  N�r  j�  N�r  j�  K�r  j�  K�r  j�  K�r  j�  J�����r  j�  N�r  j�  X   Darkr  �r  j�  ]r  (KHK KAe�r  j�  N�r  j�  N�r  j�  K�r  j�  ]r  (]r  (X   LoseUsesOnMiss (T/F)r  j�  X   Lose uses even on missr  e]r   (X   OneLossPerCombat (T/F)r!  j�  X    Doubling doesn't cost extra usesr"  ee�r#  j�  K�r$  X   status_on_equipr%  X   SPD2r&  �r'  h5K�r(  X   hp_costr)  K�r*  j  X   Fluxr+  �r,  eu}r-  (j�  M�hX   HeartOfGalesr.  hX   <orange>Heart Of Gales</orange>r/  hX   +1 SPD. Wind affinity only.r0  j�  hhj�  �j�  }r1  j�  ]r2  j�  ]r3  (j�  X   HeartofGalesr4  �r5  j�  X   oranger6  �r7  eu}r8  (j�  M�hX   Disasterr9  hX   <purple>Disaster</purple>r:  hX3   Carlin Only. Backdash: 1. Cannot Counter or Double.r;  j�  hhj�  �j�  }r<  (j�  Kj�  Kuj�  ]r=  j�  ]r>  (j�  N�r?  j�  N�r@  j�  K�rA  j�  K�rB  j�  K�rC  j�  K
�rD  j�  K#�rE  j�  N�rF  j�  X   DarkrG  �rH  j�  ]rI  (KHK KAe�rJ  j�  N�rK  j�  K�rL  j�  ]rM  (]rN  (X   LoseUsesOnMiss (T/F)rO  j�  X   Lose uses even on missrP  e]rQ  (X   OneLossPerCombat (T/F)rR  j�  X    Doubling doesn't cost extra usesrS  ee�rT  X   prf_unitrU  ]rV  X   CarlinrW  a�rX  j�  X   purplerY  �rZ  X   cannot_counterr[  N�r\  X	   no_doubler]  N�r^  j  X   Fluxr_  �r`  X   backdash_on_end_combatra  K�rb  eu}rc  (j�  M�hX   Iron Axerd  hX   Iron Axere  hhj�  h�j�  �j�  }rf  j�  ]rg  j�  ]rh  (j�  N�ri  j�  N�rj  j�  K�rk  j�  K�rl  j�  K�rm  j�  K �rn  j�  N�ro  j�  X   Axerp  �rq  j�  K �rr  eu}rs  (j�  M�hX   HeartOfLightningrt  hX#   <orange>Heart Of Lightning</orange>ru  hX    +10 Crit. Thunder affinity only.rv  j�  h�j�  �j�  }rw  j�  ]rx  j�  ]ry  (j�  X   HeartofLightningrz  �r{  j�  X   oranger|  �r}  eu}r~  (j�  M�hj�  hj�  hj�  j�  h�j�  �j�  }r  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  j�  �r�  j�  K�r�  j�  j�  �r�  j�  N�r�  eu}r�  (j�  M�hX
   Iron Lancer�  hX
   Iron Lancer�  hhj�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  N�r�  j�  X   Lancer�  �r�  j�  K �r�  eu}r�  (j�  M�hX   Javelinr�  hX   Javelinr�  hX   Cannot Counter. SPD -2.r�  j�  h�j�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  J�����r�  j�  K �r�  j�  N�r�  j�  X   Lancer�  �r�  j�  K�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  j[  N�r�  h5K�r�  j%  X   SPD2r�  �r�  eu}r�  (j�  M�hX   HeartOfFrostr�  hX   <orange>Heart Of Frost</orange>r�  hX   +1 DEF/RES. Ice affinity only.r�  j�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  X   HeartofFrostr�  �r�  j�  X   oranger�  �r�  eu}r�  (j�  M�hX   Flashr�  hX   Flashr�  hX*   Lowers target's Accuracy by 20 for 1 turn.r�  j�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K
�r�  j�  K
�r�  j�  N�r�  j�  X   Lightr�  �r�  j�  ]r�  (K�K�K e�r�  j�  N�r�  j�  N�r�  j  X   Glimmerr�  �r�  j   X   Dazzledr�  �r�  eu}r�  (j�  M�hX   Shimmerr�  hX   Shimmerr�  hX   Cannot miss.r�  j�  h�j�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K
�r�  j�  N�r�  j�  X   Lightr�  �r�  j�  ]r�  (K�K�K e�r�  j�  N�r�  j�  N�r�  h5K�r�  j�  K�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  j  X   Glimmerr�  �r�  eu}r�  (j�  M�hj�  hj�  hj�  j�  h�j�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r   j�  K�r  j�  j�  �r  j�  K�r  j�  j�  �r  j�  N�r  eu}r  (j�  M�hX   Entangler  hX   Entangler  hX'   Lowers target's Avoid by 20 for 1 turn.r	  j�  h�j�  �j�  }r
  j�  ]r  j�  ]r  (j�  N�r  j�  N�r  j�  K�r  j�  K�r  j�  K�r  j�  K�r  j�  N�r  j�  X   Animar  �r  j�  ]r  (KUKUK e�r  j�  N�r  j�  N�r  j   X	   Entangledr  �r  j�  K �r  j  X   Firer  �r  eu}r  (j�  M�hX   Firer   hX   Firer!  hX*   Effective against horseback units. -1 SPD.r"  j�  h�j�  �j�  }r#  (j�  Kj�  Kuj�  ]r$  j�  ]r%  (j�  N�r&  j�  N�r'  j�  K�r(  j�  K�r)  j�  K�r*  j�  K �r+  j�  K�r,  j�  K�r-  j�  ]r.  (]r/  (X   LoseUsesOnMiss (T/F)r0  j�  X   Lose uses even on missr1  e]r2  (X   OneLossPerCombat (T/F)r3  j�  X    Doubling doesn't cost extra usesr4  ee�r5  j�  N�r6  j�  X   Animar7  �r8  j�  ]r9  (K�K`K e�r:  j�  N�r;  j�  N�r<  X   effective_damager=  }r>  (X   effective_tagsr?  ]r@  X   HorserA  aX   effective_multiplierrB  G@       X   effective_bonus_damagerC  K X   show_effectiveness_flashrD  �u�rE  h5K�rF  j%  X   SPD1rG  �rH  X   eval_warningrI  X   'Horse' in unit.tagsrJ  �rK  j  X   FirerL  �rM  eu}rN  (j�  M�hX   HeartOfDarknessrO  hX"   <orange>Heart Of Darkness</orange>rP  hX   +15 Hit. Dark affinity only.rQ  j�  h�j�  �j�  }rR  j�  ]rS  j�  ]rT  (j�  X   HeartofDarknessrU  �rV  j�  X   orangerW  �rX  eu}rY  (j�  M�hX   Iron BowrZ  hX   Iron Bowr[  hhj�  h�j�  �j�  }r\  j�  ]r]  j�  ]r^  (j�  N�r_  j�  N�r`  j�  K�ra  j�  K�rb  j�  K�rc  j�  K
�rd  j�  N�re  j�  X   Bowrf  �rg  j=  }rh  (j?  ]ri  X   Flyingrj  ajB  G@       jC  K jD  �u�rk  j�  K �rl  jI  X   'Flying' in unit.tagsrm  �rn  eu}ro  (j�  M�hX   Longbowrp  hX   Longbowrq  hX   Cannot Counter. SPD -1.rr  j�  h�j�  �j�  }rs  (j�  K
j�  K
uj�  ]rt  j�  ]ru  (j�  N�rv  j�  N�rw  j�  N�rx  j�  K�ry  j�  J�����rz  j�  X   Bowr{  �r|  j�  K
�r}  j�  ]r~  (]r  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  j�  K�r�  j�  K�r�  j�  K�r�  j[  N�r�  j=  }r�  (j?  ]r�  X   Flyingr�  ajB  G@       jC  K jD  �u�r�  j%  X   SPD1r�  �r�  jI  X   'Flying' in unit.tagsr�  �r�  eu}r�  (j�  M�hX   HeartOfRadiancer�  hX"   <orange>Heart Of Radiance</orange>r�  hX   +15 Avoid. Holy affinity only.r�  j�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  X   HeartofRadiancer�  �r�  j�  X   oranger�  �r�  eu}r�  (j�  M�hj  hj  hj	  j�  j  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  N�r�  j�  j  �r�  j�  j  �r�  j�  N�r�  j�  N�r�  j   j  �r�  j�  K �r�  j  j  �r�  eu}r�  (j�  M�hj�  hj�  hj�  j�  j  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K
�r�  j�  K
�r�  j�  N�r�  j�  j�  �r�  j�  j�  �r�  j�  N�r�  j�  N�r�  j  j�  �r�  j   j�  �r�  eu}r�  (j�  M�hj   hj!  hj"  j�  j  j�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K �r�  j�  K�r�  j�  K�r�  j�  j.  �r�  j�  N�r�  j�  j7  �r�  j�  j9  �r�  j�  N�r�  j�  N�r�  j=  }r�  (j?  j@  jB  G@       jC  K jD  �u�r�  h5K�r�  j%  jG  �r�  jI  jJ  �r�  j  jL  �r�  eu}r�  (j�  M�hX   AngelFeatherr�  hX   <yellow>Angel Feather</yellow>r�  hX&   Gives unit +10% growths. Cannot stack.r�  j�  j  j�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  X   usable_in_baser�  N�r�  j�  X   yellowr�  �r�  j�  ]r�  (K�K�K�e�r�  j   X   AngelFeatherr�  �r�  j�  K �r�  j�  K �r�  X   eval_availabler�  X#   not has_skill(unit, 'AngelFeather')r�  �r�  X   event_on_hitr�  X   Global AngelFeatherr�  �r�  eu}r�  (j�  M�hX	   ChurchKeyr�  hX
   Church Keyr�  hX   Opens any door in the church.r�  j�  j  j�  �j�  }r�  (j�  Kj�  Kuj�  ]r   j�  ]r  (j�  K�r  j�  ]r  ]r  (X   LoseUsesOnMiss (T/F)r  j�  X   Lose uses even on missr  ea�r  X
   can_unlockr  X   region.nid.startswith('Door')r	  �r
  eu}r  (j�  M�hX   Steel Lancer  hX   Steel Lancer  hX   SPD -1.r  j�  j>  j�  �j�  }r  (j�  Kj�  Kuj�  ]r  j�  ]r  (j�  N�r  j�  N�r  j�  N�r  j�  K�r  j�  K �r  j�  X   Lancer  �r  j�  K�r  j�  ]r  (]r  (X   LoseUsesOnMiss (T/F)r  j�  X   Lose uses even on missr  e]r  (X   OneLossPerCombat (T/F)r  j�  X    Doubling doesn't cost extra usesr   ee�r!  j�  K �r"  j�  K�r#  j�  K�r$  j%  X   SPD1r%  �r&  eu}r'  (j�  M�hX	   Axereaverr(  hX	   Axereaverr)  hX%   Reverses the weapon triangle. SPD -1.r*  j�  j>  j�  �j�  }r+  (j�  Kj�  Kuj�  ]r,  j�  ]r-  (j�  N�r.  j�  N�r/  j�  N�r0  j�  K�r1  j�  K �r2  j�  X   Lancer3  �r4  j�  K�r5  j�  ]r6  (]r7  (X   LoseUsesOnMiss (T/F)r8  j�  X   Lose uses even on missr9  e]r:  (X   OneLossPerCombat (T/F)r;  j�  X    Doubling doesn't cost extra usesr<  ee�r=  X   reaverr>  N�r?  h5K�r@  j�  K
�rA  j�  K�rB  j�  K�rC  jI  XU   target.get_weapon() and item_system.weapon_type(target, target.get_weapon()) == 'Axe'rD  �rE  eu}rF  (j�  M�hj�  hj�  hj�  j�  j>  j�  �j�  }rG  (j�  Kj�  Kuj�  ]rH  j�  ]rI  (j�  N�rJ  j�  N�rK  j�  K�rL  j�  j�  �rM  j�  K�rN  j�  j�  �rO  j�  N�rP  eu}rQ  (j�  M�hX   GreatbowrR  hX	   Steel BowrS  hX   SPD -2.rT  j�  jZ  j�  �j�  }rU  (j�  Kj�  Kuj�  ]rV  j�  ]rW  (j�  N�rX  j�  N�rY  j�  N�rZ  j�  K�r[  j�  K �r\  j�  X   Bowr]  �r^  j�  K�r_  j�  ]r`  (]ra  (X   LoseUsesOnMiss (T/F)rb  j�  X   Lose uses even on missrc  e]rd  (X   OneLossPerCombat (T/F)re  j�  X    Doubling doesn't cost extra usesrf  ee�rg  j�  K �rh  j�  K�ri  j�  K�rj  j%  X   SPD2rk  �rl  j=  }rm  (j?  ]rn  X   Flyingro  ajB  G@       jC  K jD  �u�rp  jI  X   'Flying' in unit.tagsrq  �rr  eu}rs  (j�  M�hX   Mini_Bowrt  hX   Mini Bowru  hX   Cannot Counter. SPD -1.rv  j�  jZ  j�  �j�  }rw  (j�  K
j�  K
uj�  ]rx  j�  ]ry  (j�  N�rz  j�  N�r{  j�  N�r|  j�  K�r}  j�  J�����r~  j�  X   Bowr  �r�  j�  K
�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  j�  K�r�  h5K�r�  j�  K�r�  j�  K�r�  j[  N�r�  j=  }r�  (j?  ]r�  X   Flyingr�  ajB  G@       jC  K jD  �u�r�  j%  X   SPD1r�  �r�  jI  X   'Flying' in unit.tagsr�  �r�  eu}r�  (j�  M�hj�  hj�  hj�  j�  jZ  j�  �j�  }r�  (j�  Kj�  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  j�  �r�  j�  K�r�  j�  j�  �r�  j�  N�r�  eu}r�  (j�  M�hX   Silver Swordr�  hX   Silver Swordr�  hhj�  j  j�  �j�  }r�  (j�  K
j�  K
uj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K
�r�  j�  K �r�  j�  N�r�  j�  X   Swordr�  �r�  j�  K
�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  eu}r�  (j�  M�hX   Silver Lancer�  hX   Silver Lancer�  hhj�  j  j�  �j�  }r�  (j�  K
j�  K
uj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K �r�  j�  N�r�  j�  X   Lancer�  �r�  j�  K
�r�  j�  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j�  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j�  X    Doubling doesn't cost extra usesr�  ee�r�  eu}r�  (j�  M�hX   TheReclaimerr�  hX   <blue>The Reclaimer<blue>r�  hX6   Cavalier line only. 50% Lifelink. Effective Vs. Armor.r�  j�  j  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  J�����r�  j�  K
�r�  j�  N�r�  j�  X   Lancer�  �r�  X	   prf_classr�  ]r�  (X   Cavalierr�  X   Paladinr�  X   Great_Knightr�  e�r�  j=  }r�  (j?  ]r�  X   Armorr�  ajB  G@       jC  K jD  �u�r�  j�  X   bluer�  �r�  X   lifelinkr�  G?�      �r�  eu}r�  (j�  M�hj�  hj�  hhj�  j9  j�  �j�  }r�  (j�  K
j�  K
uj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K
�r�  j�  K �r�  j�  N�r�  j�  j�  �r�  j�  K
�r   j�  j�  �r  eu}r  (j�  M�hj�  hj�  hj�  j�  jO  j�  �j�  }r  j�  ]r  j�  ]r  (j�  N�r  j�  N�r  j�  K�r  j�  K�r	  j�  K�r
  j�  K
�r  j�  K
�r  j�  N�r  j�  j�  �r  j�  j�  �r  j�  N�r  j�  N�r  j  j�  �r  j   j�  �r  eu}r  (j�  M�hj�  hj�  hhj�  j�  j�  �j�  }r  j�  ]r  j�  ]r  (j�  N�r  j�  N�r  j�  K�r  j�  K�r  j�  K�r  j�  K
�r  j�  N�r  j�  j�  �r  j�  K �r   eu}r!  (j�  M�hX   Lockpickr"  hX   Lockpickr#  hX   Opens chests and doors.r$  j�  j�  j�  �j�  }r%  (j�  Kj�  Kuj�  ]r&  j�  ]r'  (j�  K�r(  j�  ]r)  ]r*  (X   LoseUsesOnMiss (T/F)r+  j�  X   Lose uses even on missr,  ea�r-  j  X   Truer.  �r/  j�  ]r0  X   Thiefr1  a�r2  eu}r3  (j�  M�hX   Shover4  hX   Shover5  hX!   Free action. Shove target 1 tile.r6  j�  j>  j�  �j�  }r7  (X   cooldownr8  K X   starting_cooldownr9  Kuj�  ]r:  j�  ]r;  (X   target_unitr<  N�r=  j�  K�r>  j�  K�r?  X   shover@  K�rA  X   shove_target_restrictrB  K�rC  X   eval_target_restrict_2rD  X   'Tile' no in target.tagsrE  �rF  j�  N�rG  j8  K�rH  eu}rI  (j�  M�hj4  hj5  hj6  j�  j  j�  �j�  }rJ  (j8  K j9  Kuj�  ]rK  j�  ]rL  (j<  N�rM  j�  K�rN  j�  K�rO  j@  K�rP  jB  K�rQ  jD  jE  �rR  j�  N�rS  j8  K�rT  eu}rU  (j�  M hX   HealrV  hX   HealrW  hX   Restores (User's MAG * 2) HP.rX  j�  j  j�  �j�  }rY  j�  ]rZ  j�  ]r[  (X   spellr\  N�r]  j�  N�r^  j�  K�r_  j�  K�r`  j�  X   Staffra  �rb  hK�rc  h5K�rd  j�  ]re  (K`K�K�e�rf  j�  N�rg  j  X   Healrh  �ri  X   equation_healrj  X   MAG2rk  �rl  eu}rm  (j�  MhX   Exchangern  hX   Exchangero  hX(   Free action. Trade with a far away ally.rp  j�  j  j�  �j�  }rq  j�  ]rr  j�  ]rs  (j\  N�rt  j�  N�ru  j�  K�rv  j�  K�rw  j�  ]rx  (K�K�Ke�ry  X   traderz  N�r{  X   menu_after_combatr|  N�r}  j�  N�r~  eu}r  (j�  Mhj4  hj5  hj6  j�  h�j�  �j�  }r�  (j8  K j9  Kuj�  ]r�  j�  ]r�  (j<  N�r�  j�  K�r�  j�  K�r�  j@  K�r�  jB  K�r�  jD  jE  �r�  j�  N�r�  j8  K�r�  eu}r�  (j�  Mhj4  hj5  hj6  j�  h�j�  �j�  }r�  (j8  K j9  Kuj�  ]r�  j�  ]r�  (j<  N�r�  j�  K�r�  j�  K�r�  j@  K�r�  jB  K�r�  jD  jE  �r�  j�  N�r�  j8  K�r�  eu}r�  (j�  Mhj4  hj5  hj6  j�  h�j�  �j�  }r�  (j8  K j9  Kuj�  ]r�  j�  ]r�  (j<  N�r�  j�  K�r�  j�  K�r�  j@  K�r�  jB  K�r�  jD  jE  �r�  j�  N�r�  j8  K�r�  eu}r�  (j�  MhX   Treatr�  hX   Treatr�  hX*   Free Action. Restores (User's MAG + 2) HP.r�  j�  h�j�  �j�  }r�  (j8  K j9  Kuj�  ]r�  j�  ]r�  (j�  N�r�  X   never_use_battle_animationr�  N�r�  j�  K�r�  j�  K�r�  jj  X   HEALr�  �r�  j�  N�r�  hK�r�  j�  K�r�  j8  K�r�  eu}r�  (j�  Mhj4  hj5  hj6  j�  h�j�  �j�  }r�  (j8  K j9  Kuj�  ]r�  j�  ]r�  (j<  N�r�  j�  K�r�  j�  K�r�  j@  K�r�  jB  K�r�  jD  jE  �r�  j�  N�r�  j8  K�r�  eu}r�  (j�  MhX   Tripr�  hX   Tripr�  hX/   Free Action. Set target's move to 0 for 1 turn.r�  j�  h�j�  �j�  }r�  (j8  K j9  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  K�r�  j�  K�r�  j   X   Trippedr�  �r�  j�  N�r�  j8  K�r�  eu}r�  (j�  Mhj4  hj5  hj6  j�  h�j�  �j�  }r�  (j8  K j9  Kuj�  ]r�  j�  ]r�  (j<  N�r�  j�  K�r�  j�  K�r�  j@  K�r�  jB  K�r�  jD  jE  �r�  j�  N�r�  j8  K�r�  euehD]r�  (}r�  (j�  MshX   HeartofFirer�  j�  hj�  }r�  X   initiator_nidr�  NX   subskillr�  Nu}r�  (j�  MwhX   HeartofGalesr�  j�  hhj�  }r�  j�  Nj�  Nu}r�  (j�  M{hX   HeartofLightningr�  j�  h�j�  }r�  j�  Nj�  Nu}r�  (j�  MhX   HeartofFrostr�  j�  h�j�  }r�  j�  Nj�  Nu}r�  (j�  M�hX   HeartofDarknessr�  j�  h�j�  }r�  j�  Nj�  Nu}r�  (j�  M�hX   HeartofRadiancer�  j�  h�j�  }r�  j�  Nj�  Nu}r�  (j�  M�hX   SPD1r�  j�  j>  j�  }r�  j�  Nj�  Nu}r�  (j�  M�hX   SPD2r�  j�  jZ  j�  }r�  j�  Nj�  Nu}r�  (j�  MthX   trader�  j�  hj�  }r�  j�  Nj�  Nu}r�  (j�  MuhX   Shover�  j�  hj�  }r�  j�  Nj�  Nu}r�  (j�  MvhX   Flailr�  j�  hj�  }r�  j�  Nj�  Nu}r�  (j�  Mxhj�  j�  hhj�  }r   j�  Nj�  Nu}r  (j�  Myhj�  j�  hhj�  }r  j�  Nj�  Nu}r  (j�  MzhX   RiskyBusinessr  j�  hhj�  }r  j�  Nj�  Nu}r  (j�  M|hj�  j�  h�j�  }r  j�  Nj�  Nu}r  (j�  M}hj�  j�  h�j�  }r	  X   ability_item_uidr
  Msj�  Nj�  Nu}r  (j�  M~hX   Bungler  j�  h�j�  }r  j�  Nj�  Nu}r  (j�  M�hj�  j�  h�j�  }r  j�  Nj�  Nu}r  (j�  M�hj�  j�  h�j�  }r  j
  Msj�  Nj�  Nu}r  (j�  M�hX   Tripr  j�  h�j�  }r  j
  Msj�  Nj�  Nu}r  (j�  M�hj�  j�  h�j�  }r  j�  Nj�  Nu}r  (j�  M�hX	   Protectorr  j�  h�j�  }r  j�  Nj�  Nu}r  (j�  M�hj�  j�  h�j�  }r  j
  Msj�  Nj�  Nu}r  (j�  M�hX   ElwynnSkillr  j�  h�j�  }r  j
  Msj�  Nj�  Nu}r  (j�  M�hj�  j�  h�j�  }r   j�  Nj�  Nu}r!  (j�  M�hj  j�  h�j�  }r"  j�  Nj�  Nu}r#  (j�  M�hj�  j�  h�j�  }r$  j
  Msj�  Nj�  Nu}r%  (j�  M�hX   Enthrallr&  j�  h�j�  }r'  j�  Nj�  Nu}r(  (j�  M�hj�  j�  h�j�  }r)  j�  Nj�  Nu}r*  (j�  M�hj  j�  h�j�  }r+  j�  Nj�  Nu}r,  (j�  M�hj�  j�  h�j�  }r-  j
  Msj�  Nj�  Nu}r.  (j�  M�hX   Potshotr/  j�  h�j�  }r0  j�  Nj�  Nu}r1  (j�  M�hj�  j�  j  j�  }r2  j�  Nj�  Nu}r3  (j�  M�hX   Anticritr4  j�  j  j�  }r5  j�  Nj�  Nu}r6  (j�  M�hj�  j�  j  j�  }r7  j�  Nj�  Nu}r8  (j�  M�hj�  j�  j  j�  }r9  j
  M�sj�  Nj�  Nu}r:  (j�  M�hX   DivineConduitr;  j�  j  j�  }r<  j
  M sj�  Nj�  Nu}r=  (j�  M�hX   Exchanger>  j�  j  j�  }r?  j
  Msj�  Nj�  Nu}r@  (j�  M�hj�  j�  j>  j�  }rA  j�  Nj�  Nu}rB  (j�  M�hj  j�  j>  j�  }rC  j�  Nj�  Nu}rD  (j�  M�hj�  j�  j>  j�  }rE  j
  M�sj�  Nj�  Nu}rF  (j�  M�hX
   InstructorrG  j�  j>  j�  }rH  j�  Nj�  Nu}rI  (j�  M�hj�  j�  jZ  j�  }rJ  j�  Nj�  Nu}rK  (j�  M�hj  j�  jZ  j�  }rL  j�  Nj�  Nu}rM  (j�  M�hj�  j�  jZ  j�  }rN  j�  Nj�  Nu}rO  (j�  M�hj�  j�  jt  j�  }rP  j�  Nj�  Nu}rQ  (j�  M�hj�  j�  j�  j�  }rR  j�  Nj�  Nu}rS  (j�  M�hj�  j�  j�  j�  }rT  j�  Nj�  Nu}rU  (j�  M�hj�  j�  j�  j�  }rV  j�  Nj�  Nu}rW  (j�  M�hj�  j�  j�  j�  }rX  j�  Nj�  Nu}rY  (j�  M�hj�  j�  j�  j�  }rZ  j�  Nj�  Nu}r[  (j�  M�hj�  j�  j�  j�  }r\  j�  Nj�  Nu}r]  (j�  M�hj�  j�  j�  j�  }r^  j�  Nj�  Nu}r_  (j�  M�hj�  j�  j
  j�  }r`  j�  Nj�  Nu}ra  (j�  M�hj�  j�  j  j�  }rb  j�  Nj�  Nu}rc  (j�  M�hj  j�  j  j�  }rd  j�  Nj�  Nu}re  (j�  M�hX   AspiringLeaderrf  j�  j  j�  }rg  j�  Nj�  Nu}rh  (j�  M�hX   Cantori  j�  j  j�  }rj  j�  Nj�  Nu}rk  (j�  M�hj�  j�  j9  j�  }rl  j�  Nj�  Nu}rm  (j�  M�hj�  j�  jO  j�  }rn  j�  Nj�  Nu}ro  (j�  M�hj�  j�  jd  j�  }rp  j�  Nj�  Nu}rq  (j�  M�hji  j�  jd  j�  }rr  j�  Nj�  Nu}rs  (j�  M�hj�  j�  jw  j�  }rt  j�  Nj�  Nu}ru  (j�  M�hji  j�  jw  j�  }rv  j�  Nj�  Nu}rw  (j�  M�hj�  j�  j�  j�  }rx  j�  Nj�  Nu}ry  (j�  M�hji  j�  j�  j�  }rz  j�  Nj�  Nu}r{  (j�  M�hj�  j�  j�  j�  }r|  j�  Nj�  Nu}r}  (j�  M�hji  j�  j�  j�  }r~  j�  Nj�  Nu}r  (j�  M�hj�  j�  j�  j�  }r�  j�  Nj�  Nu}r�  (j�  M�hj�  j�  j�  j�  }r�  j�  Nj�  Nu}r�  (j�  M�hj�  j�  j�  j�  }r�  j�  Nj�  Nu}r�  (j�  M�hj�  j�  j�  j�  }r�  j�  Nj�  Nu}r�  (j�  M�hj�  j�  j�  j�  }r�  j�  Nj�  Nu}r�  (j�  M�hj�  j�  j
  j�  }r�  j�  Nj�  Nu}r�  (j�  M�hj�  j�  j  j�  }r�  j�  Nj�  Nu}r�  (j�  M�hj�  j�  j.  j�  }r�  j�  Nj�  Nu}r�  (j�  M�hj�  j�  j@  j�  }r�  j�  Nj�  Nu}r�  (j�  M�hj�  j�  jR  j�  }r�  j�  Nj�  Nu}r�  (j�  M�hj�  j�  jd  j�  }r�  j�  Nj�  Nu}r�  (j�  M�hj�  j�  jv  j�  }r�  j�  Nj�  Nu}r�  (j�  M�hj�  j�  j�  j�  }r�  j�  Nj�  Nu}r�  (j�  M�hX   HolyVeilr�  j�  j  j�  }r�  j�  Nj�  M�u}r�  (j�  M�hX   Veiledr�  j�  j�  j�  }r�  j�  Nj�  Nu}r�  (j�  M�hj�  j�  Nj�  }r�  j�  Nj�  Nu}r�  (j�  M�hj�  j�  Nj�  }r�  j�  Nj�  NueX   terrain_status_registryr�  }r�  X   regionsr�  ]r�  (}r�  (hX   Chest1r�  X   region_typer�  capp.events.regions
RegionType
r�  X   eventr�  �r�  Rr�  hKK
�r�  X   sizer�  ]r�  (KKeX   sub_nidr�  X   Chestr�  X	   conditionr�  X   unit.can_unlock(region)r�  X	   time_leftr�  NX	   only_oncer�  �X   interrupt_mover�  �j�  }r�  u}r�  (hX   Chest2r�  j�  j�  hK	K
�r�  j�  ]r�  (KKej�  X   Chestr�  j�  X   unit.can_unlock(region)r�  j�  Nj�  �j�  �j�  }r�  u}r�  (hX   Door1r�  j�  j�  hKK�r�  j�  ]r�  (KKej�  X   Doorr�  j�  X1   unit.can_unlock(region) and unit.team == 'player'r�  j�  Nj�  �j�  �j�  }r�  u}r�  (hX   Door2r�  j�  j�  hKK�r�  j�  ]r�  (KKej�  X   Doorr�  j�  X   unit.can_unlock(region)r�  j�  Nj�  �j�  �j�  }r�  u}r�  (hX   Door3r�  j�  X   eventr�  hKK�r�  j�  ]r�  (KKej�  X   MainDoorr�  j�  X   unit.can_unlock(region)r�  j�  Nj�  �j�  �j�  }r�  u}r�  (hX   Firer�  j�  X   statusr�  hNj�  ]r�  (KKej�  X   Firer�  j�  X   Truer�  j�  Nj�  �j�  �j�  }r�  u}r�  (hX   FindKeyr�  j�  j�  hKK�r�  j�  ]r�  (KKej�  X   Searchr�  j�  X   unit.team == 'player'r�  j�  Nj�  �j�  �j�  }r�  u}r�  (hX
   Formation1r�  j�  j�  X	   formationr�  �r�  Rr�  hKK�r�  j�  ]r�  (KKej�  hj�  X   Truer�  j�  Nj�  �j�  �j�  }r�  u}r�  (hX
   Formation2r�  j�  j�  hKK�r�  j�  ]r�  (KKej�  Nj�  X   Truer�  j�  Nj�  �j�  �j�  }r�  u}r�  (hX
   Formation3r�  j�  j�  hKK�r�  j�  ]r�  (KKej�  Nj�  X   Truer�  j�  Nj�  �j�  �j�  }r�  u}r�  (hX
   Formation4r�  j�  j�  hKK�r�  j�  ]r�  (KKej�  Nj�  X   Truer�  j�  Nj�  �j�  �j�  }r�  u}r�  (hX
   Formation5r 	  j�  j�  hKK�r	  j�  ]r	  (KKej�  hj�  X   Truer	  j�  Nj�  �j�  �j�  }r	  u}r	  (hX
   Formation6r	  j�  j�  hK K�r	  j�  ]r	  (KKej�  Nj�  X   Truer		  j�  Nj�  �j�  �j�  }r
	  u}r	  (hX
   Formation7r	  j�  X	   formationr	  hK K�r	  j�  ]r	  (KKej�  hj�  X   Truer	  j�  Nj�  �j�  �j�  }r	  u}r	  (hX   ThiefEscaper	  j�  X   eventr	  hKK�r	  j�  ]r	  (KKej�  X   Escaper	  j�  X   unit.team == 'enemy'r	  j�  Nj�  �j�  �j�  }r	  ueh}r	  (hX   2r	  hX   Ch. 3: Sanctuary Ablazer	  X   tilemapr	  }r	  (hX   Ch3r	  X   layersr 	  ]r!	  (}r"	  (hX   baser#	  X   visibler$	  �u}r%	  (hX   BotDoorr&	  j$	  �u}r'	  (hX   MidDoorr(	  j$	  �u}r)	  (hX   TopDoorr*	  j$	  �u}r+	  (hX   Leftr,	  j$	  �u}r-	  (hX   Rightr.	  j$	  �ueX   weatherr/	  ]r0	  X   firer1	  N�r2	  aX
   animationsr3	  ]r4	  uX
   bg_tilemapr5	  Nh
hX   musicr6	  }r7	  (X   player_phaser8	  X   Distant Roadsr9	  X   enemy_phaser:	  X   Shadow of the Enemyr;	  X   other_phaser<	  NX   enemy2_phaser=	  NX   player_battler>	  X   Attackr?	  X   enemy_battler@	  X   DefenserA	  X   other_battlerB	  NX   enemy2_battlerC	  NuX	   objectiverD	  }rE	  (X   simplerF	  X*   Survive,Turns Left {eval:9-game.turncount}rG	  X   winrH	  X   Survive 8 TurnsrI	  X   lossrJ	  X   Three Player Units DierK	  uh]rL	  (hhhh�h�h�h�h�j  j  j>  jZ  jt  j�  j�  j�  j�  j�  j�  j�  j
  j  j9  jO  jd  jw  j�  j�  j�  j�  j�  j�  j�  j
  j  j.  j@  jR  jd  jv  j�  ej�  ]rM	  (j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j 	  j	  j	  j	  eX   unit_groupsrN	  ]rO	  (}rP	  (hX   BanditReinforcementsrQ	  h]rR	  X	   positionsrS	  }rT	  u}rU	  (hX   Bandits1rV	  h]rW	  X   EmbersonrX	  ajS	  }rY	  X   EmbersonrZ	  ]r[	  (KKesu}r\	  (hX
   RossGarciar]	  h]r^	  jS	  }r_	  u}r`	  (hX   BlueTeamra	  h]rb	  (X   Alpinrc	  X   Carlinrd	  X   Shaylare	  X   Quinleyrf	  ejS	  }rg	  ueX	   ai_groupsrh	  ]ri	  }rj	  (hhX   trigger_thresholdrk	  KX   activerl	  �uauX
   overworldsrm	  ]rn	  }ro	  (j	  NX   enabled_nodesrp	  ]rq	  X   enabled_roadsrr	  ]rs	  hX   0rt	  X   overworld_entitiesru	  ]rv	  }rw	  (hhX   dtyperx	  X   PARTYry	  X   dnidrz	  hX   on_node_nidr{	  NhNhX   playerr|	  uaX   selected_party_nidr}	  NX   node_propertiesr~	  }r	  X   enabled_menu_optionsr�	  }r�	  (jt	  }r�	  X   1r�	  }r�	  X   2r�	  }r�	  X   3r�	  }r�	  X   4r�	  }r�	  X   5r�	  }r�	  X   6r�	  }r�	  X   7r�	  }r�	  X   8r�	  }r�	  X   9r�	  }r�	  X   10r�	  }r�	  X   11r�	  }r�	  X   12r�	  }r�	  X   13r�	  }r�	  X   14r�	  }r�	  X   15r�	  }r�	  X   16r�	  }r�	  X   17r�	  }r�	  X   18r�	  }r�	  X   19r�	  }r�	  X   20r�	  }r�	  X   21r�	  }r�	  X   22r�	  }r�	  X   23r�	  }r�	  X   24r�	  }r�	  X   25r�	  }r�	  uX   visible_menu_optionsr�	  }r�	  (jt	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  j�	  }r�	  uuaX	   turncountr�	  KX   playtimer�	  M��X	   game_varsr�	  ccollections
Counter
r�	  }r�	  (X   _random_seedr�	  MRX   _chapter_testr�	  �X   GotReclaimerr�	  K X   EmbersonDeadr�	  K X   LeodDeadr�	  K X   _prep_options_enabledr�	  ]r�	  X   _prep_options_eventsr�	  ]r�	  X   _prep_additional_optionsr�	  ]r�	  u�r�	  Rr�	  X
   level_varsr�	  j�	  }r�	  (X   DeadPlayersr�	  K X   QuinleySkillr�	  K X   SaraidSkillr�	  K X
   _prep_pickr�	  �u�r�	  Rr�	  X   current_moder�	  }r�	  (hX   Normalr�	  X
   permadeathr�	  �h.X   Randomr�	  X   enemy_autolevelsr�	  K X   enemy_truelevelsr�	  K X   boss_autolevelsr�	  K X   boss_truelevelsr�	  K uX   partiesr�	  ]r�	  }r�	  (hhhX   Eirika's Groupr�	  X
   leader_nidr�	  X   Alpinr�	  X   party_prep_manage_sort_orderr�	  ]r�	  X   moneyr�	  K X   convoyr�	  ]r�	  X   bexpr�	  K uaX   current_partyr 
  hX   stater
  ]r
  (hX   status_upkeepr
  X   phase_changer
  e]r
  �r
  X
   action_logr
  ]r
  (X   AddSkillr	
  }r

  (X   unitr
  j
  h�r
  X	   initiatorr
  hN�r
  X	   skill_objr
  X   skillr
  Ms�r
  X   sourcer
  hM��r
  X   source_typer
  hhJ�r
  X
   subactionsr
  X   listr
  ]r
  �r
  X   reset_actionr
  X   actionr
  X   ResetUnitVarsr
  }r
  (j
  j
  h�r
  X   old_current_hpr
  hK�r 
  X   old_current_manar!
  hK �r"
  u�r#
  �r$
  X   did_somethingr%
  h��r&
  u�r'
  j	
  }r(
  (j
  j
  hh�r)
  j
  hN�r*
  j
  j
  Mw�r+
  j
  hM��r,
  j
  hhJ�r-
  j
  j
  ]r.
  �r/
  j
  j
  j
  }r0
  (j
  j
  hh�r1
  j
  hK
�r2
  j!
  hK �r3
  u�r4
  �r5
  j%
  h��r6
  u�r7
  j	
  }r8
  (j
  j
  h��r9
  j
  hN�r:
  j
  j
  M{�r;
  j
  hM��r<
  j
  hhJ�r=
  j
  j
  ]r>
  �r?
  j
  j
  j
  }r@
  (j
  j
  h��rA
  j
  hK�rB
  j!
  hK �rC
  u�rD
  �rE
  j%
  h��rF
  u�rG
  j	
  }rH
  (j
  j
  h��rI
  j
  hN�rJ
  j
  j
  M�rK
  j
  hM��rL
  j
  hhJ�rM
  j
  j
  ]rN
  �rO
  j
  j
  j
  }rP
  (j
  j
  h��rQ
  j
  hK�rR
  j!
  hK �rS
  u�rT
  �rU
  j%
  h��rV
  u�rW
  j	
  }rX
  (j
  j
  hφrY
  j
  hN�rZ
  j
  j
  M��r[
  j
  hM��r\
  j
  hhJ�r]
  j
  j
  ]r^
  �r_
  j
  j
  j
  }r`
  (j
  j
  hφra
  j
  hK�rb
  j!
  hK �rc
  u�rd
  �re
  j%
  h��rf
  u�rg
  j	
  }rh
  (j
  j
  h�ri
  j
  hN�rj
  j
  j
  M��rk
  j
  hM��rl
  j
  hhJ�rm
  j
  j
  ]rn
  �ro
  j
  j
  j
  }rp
  (j
  j
  h�rq
  j
  hK�rr
  j!
  hK �rs
  u�rt
  �ru
  j%
  h��rv
  u�rw
  j	
  }rx
  (j
  j
  j>  �ry
  j
  hN�rz
  j
  j
  M��r{
  j
  hM��r|
  j
  hhJ�r}
  j
  j
  ]r~
  �r
  j
  j
  j
  }r�
  (j
  j
  j>  �r�
  j
  hK�r�
  j!
  hK �r�
  u�r�
  �r�
  j%
  h��r�
  u�r�
  j	
  }r�
  (j
  j
  jZ  �r�
  j
  hN�r�
  j
  j
  M��r�
  j
  hM��r�
  j
  hhJ�r�
  j
  j
  ]r�
  �r�
  j
  j
  j
  }r�
  (j
  j
  jZ  �r�
  j
  hK�r�
  j!
  hK �r�
  u�r�
  �r�
  j%
  h��r�
  u�r�
  X   IncrementTurnr�
  }r�
  �r�
  X   UpdateRecordsr�
  }r�
  (X   record_typer�
  hX   turnr�
  �r�
  j�  hN�r�
  u�r�
  X   ChangeFatiguer�
  }r�
  (j
  j
  h�r�
  X   numr�
  hK �r�
  X   old_fatiguer�
  hK �r�
  j
  j
  ]r�
  �r�
  u�r�
  j�
  }r�
  (j
  j
  hh�r�
  j�
  hK �r�
  j�
  hK �r�
  j
  j
  ]r�
  �r�
  u�r�
  j�
  }r�
  (j
  j
  h��r�
  j�
  hK �r�
  j�
  hK �r�
  j
  j
  ]r�
  �r�
  u�r�
  j�
  }r�
  (j
  j
  h��r�
  j�
  hK �r�
  j�
  hK �r�
  j
  j
  ]r�
  �r�
  u�r�
  j�
  }r�
  (j
  j
  h��r�
  j�
  hK �r�
  j�
  hK �r�
  j
  j
  ]r�
  �r�
  u�r�
  j�
  }r�
  (j
  j
  hφr�
  j�
  hK �r�
  j�
  hK �r�
  j
  j
  ]r�
  �r�
  u�r�
  j�
  }r�
  (j
  j
  h�r�
  j�
  hK �r�
  j�
  hK �r�
  j
  j
  ]r�
  �r�
  u�r�
  j�
  }r�
  (j
  j
  j  �r�
  j�
  hK �r�
  j�
  hK �r�
  j
  j
  ]r�
  �r�
  u�r�
  j�
  }r�
  (j
  j
  j>  �r�
  j�
  hK �r�
  j�
  hK �r�
  j
  j
  ]r�
  �r�
  u�r�
  j�
  }r�
  (j
  j
  jZ  �r�
  j�
  hK �r�
  j�
  hK �r�
  j
  j
  ]r�
  �r�
  u�r�
  X   SetLevelVarr�
  }r�
  (hhj�	  �r�
  X   valr�
  hK �r�
  X   old_valr�
  hK �r�
  u�r�
  j�
  }r�
  (hhj�	  �r�
  j�
  hK �r�
  j�
  hK �r�
  u�r�
  j�
  }r�
  (hhj�	  �r�
  j�
  hK �r�
  j�
  hK �r�
  u�r�
  X
   SetGameVarr�
  }r�
  (hhj�	  �r�
  j�
  hK �r   j�
  hK �r  u�r  j�
  }r  (hhj�	  �r  j�
  hK �r  j�
  hK �r  u�r  X
   ChangeTeamr  }r	  (j
  j
  j  �r
  hhj   �r  X   old_teamr  hX   playerr  �r  j
  j
  X   Resetr  }r  (j
  j
  j  �r  X   movement_leftr  hK�r  hah(��������tr  �r  u�r  �r  X	   ai_actionr  j
  X   ChangeAIr  }r  (j
  j
  j  �r  hhj#  �r  X   old_air  hX   Noner  �r  u�r   �r!  X   fog_action1r"  j
  X   UpdateFogOfWarr#  }r$  (j
  j
  j  �r%  X   prev_posr&  hN�r'  u�r(  �r)  X   fog_action2r*  j
  j#  }r+  (j
  j
  j  �r,  j&  hN�r-  u�r.  �r/  u�r0  j�
  }r1  (hhj�	  �r2  j�
  hK �r3  j�
  hK �r4  u�r5  X
   AddWeatherr6  }r7  (X   weather_nidr8  hj1	  �r9  hhN�r:  u�r;  X   ArriveOnMapr<  }r=  (j
  j
  j  �r>  X   place_on_mapr?  j
  X
   PlaceOnMapr@  }rA  (j
  j
  j  �rB  X   posrC  hj  �rD  X   update_fow_actionrE  j
  j#  }rF  (j
  j
  j  �rG  j&  hN�rH  u�rI  �rJ  u�rK  �rL  u�rM  j<  }rN  (j
  j
  j�  �rO  j?  j
  j@  }rP  (j
  j
  j�  �rQ  jC  hj�  �rR  jE  j
  j#  }rS  (j
  j
  j�  �rT  j&  hN�rU  u�rV  �rW  u�rX  �rY  u�rZ  j<  }r[  (j
  j
  j  �r\  j?  j
  j@  }r]  (j
  j
  j  �r^  jC  hK K�r_  �r`  jE  j
  j#  }ra  (j
  j
  j  �rb  j&  hN�rc  u�rd  �re  u�rf  �rg  u�rh  j<  }ri  (j
  j
  h��rj  j?  j
  j@  }rk  (j
  j
  h��rl  jC  hh��rm  jE  j
  j#  }rn  (j
  j
  h��ro  j&  hN�rp  u�rq  �rr  u�rs  �rt  u�ru  j<  }rv  (j
  j
  h�rw  j?  j
  j@  }rx  (j
  j
  h�ry  jC  hK K�rz  �r{  jE  j
  j#  }r|  (j
  j
  h�r}  j&  hN�r~  u�r  �r�  u�r�  �r�  u�r�  j<  }r�  (j
  j
  hφr�  j?  j
  j@  }r�  (j
  j
  hφr�  jC  hK K�r�  �r�  jE  j
  j#  }r�  (j
  j
  hφr�  j&  hN�r�  u�r�  �r�  u�r�  �r�  u�r�  j<  }r�  (j
  j
  h��r�  j?  j
  j@  }r�  (j
  j
  h��r�  jC  hK K�r�  �r�  jE  j
  j#  }r�  (j
  j
  h��r�  j&  hN�r�  u�r�  �r�  u�r�  �r�  u�r�  j<  }r�  (j
  j
  h��r�  j?  j
  j@  }r�  (j
  j
  h��r�  jC  hh��r�  jE  j
  j#  }r�  (j
  j
  h��r�  j&  hN�r�  u�r�  �r�  u�r�  �r�  u�r�  j<  }r�  (j
  j
  j>  �r�  j?  j
  j@  }r�  (j
  j
  j>  �r�  jC  hK K�r�  �r�  jE  j
  j#  }r�  (j
  j
  j>  �r�  j&  hN�r�  u�r�  �r�  u�r�  �r�  u�r�  X   Teleportr�  }r�  (j
  j
  j  �r�  X   old_posr�  hj_  �r�  X   new_posr�  hKK�r�  �r�  jE  j
  j#  }r�  (j
  j
  j  �r�  j&  hj_  �r�  u�r�  �r�  u�r�  j�  }r�  (j
  j
  h�r�  j�  hjz  �r�  j�  hh�r�  jE  j
  j#  }r�  (j
  j
  h�r�  j&  hjz  �r�  u�r�  �r�  u�r�  j�  }r�  (j
  j
  hφr�  j�  hj�  �r�  j�  hhІr�  jE  j
  j#  }r�  (j
  j
  hφr�  j&  hj�  �r�  u�r�  �r�  u�r�  j�  }r�  (j
  j
  h��r�  j�  hj�  �r�  j�  hh��r�  jE  j
  j#  }r�  (j
  j
  h��r�  j&  hj�  �r�  u�r�  �r�  u�r�  j�  }r�  (j
  j
  j>  �r�  j�  hj�  �r�  j�  hj?  �r�  jE  j
  j#  }r�  (j
  j
  j>  �r�  j&  hj�  �r�  u�r�  �r�  u�r�  j	
  }r�  (j
  j
  j  �r�  j
  hN�r�  j
  j
  M��r�  j
  hN�r�  j
  hj8  �r�  j
  j
  ]r�  �r�  j
  j
  j
  }r�  (j
  j
  j  �r�  j
  hK�r�  j!
  hK �r�  u�r�  �r�  j%
  h��r�  u�r   j�
  }r  (hhj�	  �r  j�
  h��r  j�
  hK �r  u�r  j�
  }r  (hhj�	  �r  j�
  j
  ]r  �r	  j�
  hK �r
  u�r  j�
  }r  (hhj�	  �r  j�
  j
  ]r  �r  j�
  hK �r  u�r  j�
  }r  (hhj�	  �r  j�
  j
  ]r  �r  j�
  hK �r  u�r  X   ResetAllr  }r  X   actionsr  j
  ]r  (j
  j  }r  (j
  j
  h�r  j  hK�r  hah(��������tr  �r   u�r!  �r"  j
  j  }r#  (j
  j
  hh�r$  j  hK�r%  hah(��������tr&  �r'  u�r(  �r)  j
  j  }r*  (j
  j
  h��r+  j  hK�r,  hah(��������tr-  �r.  u�r/  �r0  j
  j  }r1  (j
  j
  h��r2  j  hK�r3  hah(��������tr4  �r5  u�r6  �r7  j
  j  }r8  (j
  j
  h��r9  j  hK�r:  hah(��������tr;  �r<  u�r=  �r>  j
  j  }r?  (j
  j
  hφr@  j  hK�rA  hah(��������trB  �rC  u�rD  �rE  j
  j  }rF  (j
  j
  h�rG  j  hK�rH  hah(��������trI  �rJ  u�rK  �rL  j
  j  }rM  (j
  j
  j  �rN  j  hK�rO  hah(��������trP  �rQ  u�rR  �rS  j
  j  }rT  (j
  j
  j  �rU  j  hK�rV  hah(��������trW  �rX  u�rY  �rZ  j
  j  }r[  (j
  j
  j>  �r\  j  hK�r]  hah(��������tr^  �r_  u�r`  �ra  j
  j  }rb  (j
  j
  jZ  �rc  j  hK�rd  hah(��������tre  �rf  u�rg  �rh  j
  j  }ri  (j
  j
  jt  �rj  j  hK�rk  hah(��������trl  �rm  u�rn  �ro  j
  j  }rp  (j
  j
  j�  �rq  j  hK�rr  hah(��������trs  �rt  u�ru  �rv  j
  j  }rw  (j
  j
  j�  �rx  j  hK�ry  hah(��������trz  �r{  u�r|  �r}  j
  j  }r~  (j
  j
  j�  �r  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j
  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j9  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jO  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jd  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jw  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j
  �r�  j  hK�r�  hah(��������tr�  �r   u�r  �r  j
  j  }r  (j
  j
  j  �r  j  hK�r  hah(��������tr  �r  u�r  �r	  j
  j  }r
  (j
  j
  j.  �r  j  hK�r  hah(��������tr  �r  u�r  �r  j
  j  }r  (j
  j
  j@  �r  j  hK�r  hah(��������tr  �r  u�r  �r  j
  j  }r  (j
  j
  jR  �r  j  hK�r  hah(��������tr  �r  u�r  �r  j
  j  }r  (j
  j
  jd  �r   j  hK�r!  hah(��������tr"  �r#  u�r$  �r%  j
  j  }r&  (j
  j
  jv  �r'  j  hK�r(  hah(��������tr)  �r*  u�r+  �r,  j
  j  }r-  (j
  j
  j�  �r.  j  hK�r/  hah(��������tr0  �r1  u�r2  �r3  e�r4  s�r5  X   LockTurnwheelr6  }r7  X   lockr8  h��r9  s�r:  j  }r;  j  j
  ]r<  (j
  j  }r=  (j
  j
  h�r>  j  hK�r?  hah(��������tr@  �rA  u�rB  �rC  j
  j  }rD  (j
  j
  hh�rE  j  hK�rF  hah(��������trG  �rH  u�rI  �rJ  j
  j  }rK  (j
  j
  h��rL  j  hK�rM  hah(��������trN  �rO  u�rP  �rQ  j
  j  }rR  (j
  j
  h��rS  j  hK�rT  hah(��������trU  �rV  u�rW  �rX  j
  j  }rY  (j
  j
  h��rZ  j  hK�r[  hah(��������tr\  �r]  u�r^  �r_  j
  j  }r`  (j
  j
  hφra  j  hK�rb  hah(��������trc  �rd  u�re  �rf  j
  j  }rg  (j
  j
  h�rh  j  hK�ri  hah(��������trj  �rk  u�rl  �rm  j
  j  }rn  (j
  j
  j  �ro  j  hK�rp  hah(��������trq  �rr  u�rs  �rt  j
  j  }ru  (j
  j
  j  �rv  j  hK�rw  hah(��������trx  �ry  u�rz  �r{  j
  j  }r|  (j
  j
  j>  �r}  j  hK�r~  hah(��������tr  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jZ  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jt  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j
  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j9  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jO  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jd  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jw  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r   j
  j  }r  (j
  j
  j�  �r  j  hK�r  hah(��������tr  �r  u�r  �r  j
  j  }r  (j
  j
  j�  �r	  j  hK�r
  hah(��������tr  �r  u�r  �r  j
  j  }r  (j
  j
  j�  �r  j  hK�r  hah(��������tr  �r  u�r  �r  j
  j  }r  (j
  j
  j�  �r  j  hK�r  hah(��������tr  �r  u�r  �r  j
  j  }r  (j
  j
  j
  �r  j  hK�r  hah(��������tr   �r!  u�r"  �r#  j
  j  }r$  (j
  j
  j  �r%  j  hK�r&  hah(��������tr'  �r(  u�r)  �r*  j
  j  }r+  (j
  j
  j.  �r,  j  hK�r-  hah(��������tr.  �r/  u�r0  �r1  j
  j  }r2  (j
  j
  j@  �r3  j  hK�r4  hah(��������tr5  �r6  u�r7  �r8  j
  j  }r9  (j
  j
  jR  �r:  j  hK�r;  hah(��������tr<  �r=  u�r>  �r?  j
  j  }r@  (j
  j
  jd  �rA  j  hK�rB  hah(��������trC  �rD  u�rE  �rF  j
  j  }rG  (j
  j
  jv  �rH  j  hK�rI  hah(��������trJ  �rK  u�rL  �rM  j
  j  }rN  (j
  j
  j�  �rO  j  hK�rP  hah(��������trQ  �rR  u�rS  �rT  e�rU  s�rV  X	   MarkPhaserW  }rX  X
   phase_namerY  hX   playerrZ  �r[  s�r\  X   SetPreviousPositionr]  }r^  (j
  j
  j  �r_  X   old_previous_positionr`  hj_  �ra  u�rb  X   Moverc  }rd  (j
  j
  j  �re  j�  hj�  �rf  j�  hKK	�rg  �rh  X   prev_movement_leftri  hK�rj  X   new_movement_leftrk  hN�rl  X   pathrm  j
  ]rn  �ro  X	   has_movedrp  h��rq  j�  h��rr  X   followrs  h��rt  X   speedru  hKZ�rv  u�rw  X   Waitrx  }ry  (j
  j
  j  �rz  hah(��������tr{  �r|  jE  j
  j#  }r}  (j
  j
  j  �r~  j&  hj�  �r  u�r�  �r�  X   regions_removedr�  j
  ]r�  �r�  u�r�  j]  }r�  (j
  j
  h��r�  j`  hj�  �r�  u�r�  j]  }r�  (j
  j
  hφr�  j`  hj�  �r�  u�r�  j]  }r�  (j
  j
  h�r�  j`  hjz  �r�  u�r�  j]  }r�  (j
  j
  j>  �r�  j`  hj�  �r�  u�r�  j6  }r�  j8  h��r�  s�r�  j  }r�  j  j
  ]r�  (j
  j  }r�  (j
  j
  h�r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  hh�r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  h��r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  h��r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  h��r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  hφr�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  h�r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j  �r�  j  hK �r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j>  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jZ  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jt  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr   �r  u�r  �r  j
  j  }r  (j
  j
  j�  �r  j  hK�r  hah(��������tr  �r  u�r	  �r
  j
  j  }r  (j
  j
  j�  �r  j  hK�r  hah(��������tr  �r  u�r  �r  j
  j  }r  (j
  j
  j�  �r  j  hK�r  hah(��������tr  �r  u�r  �r  j
  j  }r  (j
  j
  j�  �r  j  hK�r  hah(��������tr  �r  u�r  �r  j
  j  }r   (j
  j
  j
  �r!  j  hK�r"  hah(��������tr#  �r$  u�r%  �r&  j
  j  }r'  (j
  j
  j  �r(  j  hK�r)  hah(��������tr*  �r+  u�r,  �r-  j
  j  }r.  (j
  j
  j9  �r/  j  hK�r0  hah(��������tr1  �r2  u�r3  �r4  j
  j  }r5  (j
  j
  jO  �r6  j  hK�r7  hah(��������tr8  �r9  u�r:  �r;  j
  j  }r<  (j
  j
  jd  �r=  j  hK�r>  hah(��������tr?  �r@  u�rA  �rB  j
  j  }rC  (j
  j
  jw  �rD  j  hK�rE  hah(��������trF  �rG  u�rH  �rI  j
  j  }rJ  (j
  j
  j�  �rK  j  hK�rL  hah(��������trM  �rN  u�rO  �rP  j
  j  }rQ  (j
  j
  j�  �rR  j  hK�rS  hah(��������trT  �rU  u�rV  �rW  j
  j  }rX  (j
  j
  j�  �rY  j  hK�rZ  hah(��������tr[  �r\  u�r]  �r^  j
  j  }r_  (j
  j
  j�  �r`  j  hK�ra  hah(��������trb  �rc  u�rd  �re  j
  j  }rf  (j
  j
  j�  �rg  j  hK�rh  hah(��������tri  �rj  u�rk  �rl  j
  j  }rm  (j
  j
  j�  �rn  j  hK�ro  hah(��������trp  �rq  u�rr  �rs  j
  j  }rt  (j
  j
  j�  �ru  j  hK�rv  hah(��������trw  �rx  u�ry  �rz  j
  j  }r{  (j
  j
  j
  �r|  j  hK�r}  hah(��������tr~  �r  u�r�  �r�  j
  j  }r�  (j
  j
  j  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j.  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j@  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jR  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jd  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jv  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  e�r�  s�r�  jW  }r�  jY  hX   enemyr�  �r�  s�r�  j	
  }r�  (j
  j
  j  �r�  j
  hN�r�  j
  j
  M��r�  j
  hM��r�  j
  hhJ�r�  j
  j
  ]r�  �r�  j
  j
  j
  }r�  (j
  j
  j  �r�  j
  hK�r�  j!
  hK �r�  u�r�  �r�  j%
  h��r�  u�r�  X   RemoveSkillr�  }r�  (j
  j
  j  �r�  j
  hjG  �r�  X   removed_skillsr�  j
  ]r�  hcapp.engine.objects.skill
SkillObject
r�  )�r�  }r�  (j�  M�hj�  hX   -1 SPDr�  j�  NhX	   -1 Speed.r�  X   icon_nidr�  NX
   icon_indexr�  ]r�  (K K ej�  capp.utilities.data
Data
r�  )�r�  }r�  (X   _listr�  ]r�  capp.engine.skill_components.combat_components
StatChange
r�  )�r�  }r�  (X   valuer�  ]r�  ]r�  (X   SPDr�  J����eaj
  j�  ubaX   _dictr�  }r�  X   stat_changer�  j�  subj�  j�  j�  j�  j�  Nj�  NX   subskill_uidr�  NX   parent_skillr�  NubM�hJ�r�  �r�  a�r�  X   countr�  hK�r�  j
  hM��r�  j
  hhJ�r�  X   old_owner_nidr�  hN�r�  j
  j
  j
  }r�  (j
  j
  j  �r�  j
  hK�r�  j!
  hK �r�  u�r�  �r�  u�r�  j�
  }r�  �r�  j�
  }r�  (j�
  hj�
  �r�  j�  hN�r�  u�r�  j6  }r�  j8  h��r�  s�r   j  }r  j  j
  ]r  (j
  j  }r  (j
  j
  h�r  j  hK�r  hah(��������tr  �r  u�r  �r	  j
  j  }r
  (j
  j
  hh�r  j  hK�r  hah(��������tr  �r  u�r  �r  j
  j  }r  (j
  j
  h��r  j  hK�r  hah(��������tr  �r  u�r  �r  j
  j  }r  (j
  j
  h��r  j  hK�r  hah(��������tr  �r  u�r  �r  j
  j  }r  (j
  j
  h��r   j  hK�r!  hah(��������tr"  �r#  u�r$  �r%  j
  j  }r&  (j
  j
  hφr'  j  hK�r(  hah(��������tr)  �r*  u�r+  �r,  j
  j  }r-  (j
  j
  h�r.  j  hK�r/  hah(��������tr0  �r1  u�r2  �r3  j
  j  }r4  (j
  j
  j  �r5  j  hK�r6  hah(��������tr7  �r8  u�r9  �r:  j
  j  }r;  (j
  j
  j  �r<  j  hK�r=  hah(��������tr>  �r?  u�r@  �rA  j
  j  }rB  (j
  j
  j>  �rC  j  hK�rD  hah(��������trE  �rF  u�rG  �rH  j
  j  }rI  (j
  j
  jZ  �rJ  j  hK�rK  hah(��������trL  �rM  u�rN  �rO  j
  j  }rP  (j
  j
  jt  �rQ  j  hK�rR  hah(��������trS  �rT  u�rU  �rV  j
  j  }rW  (j
  j
  j�  �rX  j  hK�rY  hah(��������trZ  �r[  u�r\  �r]  j
  j  }r^  (j
  j
  j�  �r_  j  hK�r`  hah(��������tra  �rb  u�rc  �rd  j
  j  }re  (j
  j
  j�  �rf  j  hK�rg  hah(��������trh  �ri  u�rj  �rk  j
  j  }rl  (j
  j
  j�  �rm  j  hK�rn  hah(��������tro  �rp  u�rq  �rr  j
  j  }rs  (j
  j
  j�  �rt  j  hK�ru  hah(��������trv  �rw  u�rx  �ry  j
  j  }rz  (j
  j
  j�  �r{  j  hK�r|  hah(��������tr}  �r~  u�r  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j
  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j9  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jO  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jd  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jw  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j
  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j.  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j@  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jR  �r   j  hK�r  hah(��������tr  �r  u�r  �r  j
  j  }r  (j
  j
  jd  �r  j  hK�r  hah(��������tr	  �r
  u�r  �r  j
  j  }r  (j
  j
  jv  �r  j  hK�r  hah(��������tr  �r  u�r  �r  j
  j  }r  (j
  j
  j�  �r  j  hK�r  hah(��������tr  �r  u�r  �r  e�r  s�r  jW  }r  jY  hjZ  �r  s�r  j]  }r   (j
  j
  j  �r!  j`  hj�  �r"  u�r#  jc  }r$  (j
  j
  j  �r%  j�  hjg  �r&  j�  hKK�r'  �r(  ji  hK�r)  jk  hN�r*  jm  j
  ]r+  �r,  jp  h��r-  j�  h��r.  js  h��r/  ju  hKZ�r0  u�r1  jx  }r2  (j
  j
  j  �r3  hah(��������tr4  �r5  jE  j
  j#  }r6  (j
  j
  j  �r7  j&  hjg  �r8  u�r9  �r:  j�  j
  ]r;  �r<  u�r=  j6  }r>  j8  h��r?  s�r@  j  }rA  j  j
  ]rB  (j
  j  }rC  (j
  j
  h�rD  j  hK�rE  hah(��������trF  �rG  u�rH  �rI  j
  j  }rJ  (j
  j
  hh�rK  j  hK�rL  hah(��������trM  �rN  u�rO  �rP  j
  j  }rQ  (j
  j
  h��rR  j  hK�rS  hah(��������trT  �rU  u�rV  �rW  j
  j  }rX  (j
  j
  h��rY  j  hK�rZ  hah(��������tr[  �r\  u�r]  �r^  j
  j  }r_  (j
  j
  h��r`  j  hK�ra  hah(��������trb  �rc  u�rd  �re  j
  j  }rf  (j
  j
  hφrg  j  hK�rh  hah(��������tri  �rj  u�rk  �rl  j
  j  }rm  (j
  j
  h�rn  j  hK�ro  hah(��������trp  �rq  u�rr  �rs  j
  j  }rt  (j
  j
  j  �ru  j  hK�rv  hah(��������trw  �rx  u�ry  �rz  j
  j  }r{  (j
  j
  j  �r|  j  hK �r}  hah(��������tr~  �r  u�r�  �r�  j
  j  }r�  (j
  j
  j>  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jZ  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jt  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j
  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j9  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jO  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jd  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jw  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r   (j
  j
  j�  �r  j  hK�r  hah(��������tr  �r  u�r  �r  j
  j  }r  (j
  j
  j�  �r  j  hK�r	  hah(��������tr
  �r  u�r  �r  j
  j  }r  (j
  j
  j�  �r  j  hK�r  hah(��������tr  �r  u�r  �r  j
  j  }r  (j
  j
  j�  �r  j  hK�r  hah(��������tr  �r  u�r  �r  j
  j  }r  (j
  j
  j�  �r  j  hK�r  hah(��������tr  �r   u�r!  �r"  j
  j  }r#  (j
  j
  j
  �r$  j  hK�r%  hah(��������tr&  �r'  u�r(  �r)  j
  j  }r*  (j
  j
  j  �r+  j  hK�r,  hah(��������tr-  �r.  u�r/  �r0  j
  j  }r1  (j
  j
  j.  �r2  j  hK�r3  hah(��������tr4  �r5  u�r6  �r7  j
  j  }r8  (j
  j
  j@  �r9  j  hK�r:  hah(��������tr;  �r<  u�r=  �r>  j
  j  }r?  (j
  j
  jR  �r@  j  hK�rA  hah(��������trB  �rC  u�rD  �rE  j
  j  }rF  (j
  j
  jd  �rG  j  hK�rH  hah(��������trI  �rJ  u�rK  �rL  j
  j  }rM  (j
  j
  jv  �rN  j  hK�rO  hah(��������trP  �rQ  u�rR  �rS  j
  j  }rT  (j
  j
  j�  �rU  j  hK�rV  hah(��������trW  �rX  u�rY  �rZ  e�r[  s�r\  jW  }r]  jY  hj�  �r^  s�r_  j	
  }r`  (j
  j
  j  �ra  j
  hN�rb  j
  j
  M��rc  j
  hM��rd  j
  hhJ�re  j
  j
  ]rf  �rg  j
  j
  j
  }rh  (j
  j
  j  �ri  j
  hK�rj  j!
  hK �rk  u�rl  �rm  j%
  h��rn  u�ro  j�  }rp  (j
  j
  j  �rq  j
  hjG  �rr  j�  j
  ]rs  hj�  )�rt  }ru  (j�  M�hj�  hj�  j�  Nhj�  j�  Nj�  j�  j�  j�  )�rv  }rw  (j�  ]rx  j�  )�ry  }rz  (j�  ]r{  ]r|  (j�  J����eaj
  jt  ubaj�  }r}  j�  jy  subj�  jy  j�  j�  j�  Nj�  Nj�  Nj�  NubM�hJ�r~  �r  a�r�  j�  hK�r�  j
  hM��r�  j
  hhJ�r�  j�  hN�r�  j
  j
  j
  }r�  (j
  j
  j  �r�  j
  hK�r�  j!
  hK �r�  u�r�  �r�  u�r�  j�
  }r�  �r�  j�
  }r�  (j�
  hj�
  �r�  j�  hN�r�  u�r�  j6  }r�  j8  h��r�  s�r�  j  }r�  j  j
  ]r�  (j
  j  }r�  (j
  j
  h�r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  hh�r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  h��r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  h��r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  h��r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  hφr�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  h�r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j>  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jZ  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jt  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r   (j
  j
  j�  �r  j  hK�r  hah(��������tr  �r  u�r  �r  j
  j  }r  (j
  j
  j�  �r  j  hK�r	  hah(��������tr
  �r  u�r  �r  j
  j  }r  (j
  j
  j�  �r  j  hK�r  hah(��������tr  �r  u�r  �r  j
  j  }r  (j
  j
  j�  �r  j  hK�r  hah(��������tr  �r  u�r  �r  j
  j  }r  (j
  j
  j
  �r  j  hK�r  hah(��������tr  �r   u�r!  �r"  j
  j  }r#  (j
  j
  j  �r$  j  hK�r%  hah(��������tr&  �r'  u�r(  �r)  j
  j  }r*  (j
  j
  j9  �r+  j  hK�r,  hah(��������tr-  �r.  u�r/  �r0  j
  j  }r1  (j
  j
  jO  �r2  j  hK�r3  hah(��������tr4  �r5  u�r6  �r7  j
  j  }r8  (j
  j
  jd  �r9  j  hK�r:  hah(��������tr;  �r<  u�r=  �r>  j
  j  }r?  (j
  j
  jw  �r@  j  hK�rA  hah(��������trB  �rC  u�rD  �rE  j
  j  }rF  (j
  j
  j�  �rG  j  hK�rH  hah(��������trI  �rJ  u�rK  �rL  j
  j  }rM  (j
  j
  j�  �rN  j  hK�rO  hah(��������trP  �rQ  u�rR  �rS  j
  j  }rT  (j
  j
  j�  �rU  j  hK�rV  hah(��������trW  �rX  u�rY  �rZ  j
  j  }r[  (j
  j
  j�  �r\  j  hK�r]  hah(��������tr^  �r_  u�r`  �ra  j
  j  }rb  (j
  j
  j�  �rc  j  hK�rd  hah(��������tre  �rf  u�rg  �rh  j
  j  }ri  (j
  j
  j�  �rj  j  hK�rk  hah(��������trl  �rm  u�rn  �ro  j
  j  }rp  (j
  j
  j�  �rq  j  hK�rr  hah(��������trs  �rt  u�ru  �rv  j
  j  }rw  (j
  j
  j
  �rx  j  hK�ry  hah(��������trz  �r{  u�r|  �r}  j
  j  }r~  (j
  j
  j  �r  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j.  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j@  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jR  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jd  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  jv  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  j
  j  }r�  (j
  j
  j�  �r�  j  hK�r�  hah(��������tr�  �r�  u�r�  �r�  e�r�  s�r�  jW  }r�  jY  hjZ  �r�  s�r�  j]  }r�  (j
  j
  j  �r�  j`  hjg  �r�  u�r�  jc  }r�  (j
  j
  j  �r�  j�  hj'  �r�  j�  hKK�r�  �r�  ji  hK�r�  jk  hN�r�  jm  j
  ]r�  �r�  jp  h��r�  j�  h��r�  js  h��r�  ju  hKZ�r�  u�r�  j	
  }r�  (j
  j
  j�  �r�  j
  hN�r�  j
  j
  M��r�  j
  hM��r�  j
  hj�  �r�  j
  j
  ]r�  �r�  j
  j
  j
  }r�  (j
  j
  j�  �r�  j
  hK�r�  j!
  hK �r�  u�r�  �r�  j%
  h��r�  u�r�  jx  }r�  (j
  j
  j  �r�  hah(��������tr�  �r�  jE  j
  j#  }r�  (j
  j
  j  �r�  j&  hj'  �r�  u�r�  �r�  j�  j
  ]r�  �r�  u�r�  eK1K �r�  X   eventsr�  ]r�  X   supportsr�  ]r�  X   recordsr�  }r�  (X   killsr�  ]r�  j�  ]r�  X   healingr�  ]r�  X   deathr�  ]r�  X   item_user�  ]r�  X   stealr�  ]r�  X   combat_resultsr�  ]r�  X   turns_takenr�  ]r�  (X   Recordr�  }r�  (j�
  KX	   level_nidr�  j	  u�r�  j�  }r�  (j�
  Kj�  j	  u�r�  j�  }r�  (j�
  Kj�  j	  u�r�  eX   levelsr   ]r  h]r  j�	  ]r  uX   speak_stylesr  }r  (X	   __defaultr  }r  (hj  X   speakerr  NhNX   widthr	  Nju  KX
   font_colorr
  NX	   font_typer  X   convor  X
   backgroundr  X   message_bg_baser  X	   num_linesr  KX   draw_cursorr  �X   message_tailr  X   message_bg_tailr  X   transparencyr  G?�������X   name_tag_bgr  X   name_tagr  X   flagsr  cbuiltins
set
r  ]r  �r  Rr  uX   __default_textr  }r  (hj  j  NhNj	  Nju  G?�      j
  Nj  X   textr  j  X   menu_bg_baser  j  K j  Nj  Nj  G?�������j  j  j  j  ]r  �r   Rr!  uX   __default_helpr"  }r#  (hj"  j  NhNj	  Nju  G?�      j
  Nj  j  j  j#  j  Kj  �j  Nj  G?�������j  j  j  j  ]r$  �r%  Rr&  uX   noirr'  }r(  (hj'  j  NhNj	  Nju  Nj
  X   whiter)  j  Nj  X   menu_bg_darkr*  j  Nj  Nj  j#  j  Nj  Nj  j  ]r+  �r,  Rr-  uX   hintr.  }r/  (hj.  j  Nhcapp.utilities.enums
Alignments
r0  X   centerr1  �r2  Rr3  j	  K�ju  Nj
  Nj  Nj  X   menu_bg_parchmentr4  j  Kj  Nj  j#  j  Nj  Nj  j  ]r5  �r6  Rr7  uX	   cinematicr8  }r9  (hj8  j  Nhj3  j	  Nju  Nj
  X   greyr:  j  X   chapterr;  j  j#  j  Kj  �j  j#  j  Nj  Nj  j  ]r<  �r=  Rr>  uX	   narrationr?  }r@  (hj?  j  NhKKn�rA  j	  K�ju  Nj
  j)  j  Nj  j  j  Nj  Nj  j#  j  Nj  Nj  j  ]rB  �rC  RrD  uX   narration_toprE  }rF  (hjE  j  NhKK�rG  j	  K�ju  Nj
  j)  j  Nj  j  j  Nj  Nj  j#  j  Nj  Nj  j  ]rH  �rI  RrJ  uX   clearrK  }rL  (hjK  j  NhNj	  Nju  Nj
  j)  j  Nj  j#  j  Nj  �j  j#  j  Nj  Nj  j  ]rM  �rN  RrO  uX   thought_bubblerP  }rQ  (hjP  j  NhNj	  Nju  Nj
  Nj  Nj  Nj  Nj  Nj  X   message_bg_thought_tailrR  j  Nj  Nj  j  ]rS  X   no_talkrT  a�rU  RrV  uX   boss_convo_leftrW  }rX  (hjW  j  NhKHKp�rY  j	  K�ju  Kj
  Nj  j  j  j  j  Kj  �j  j  j  G        j  Nj  j  ]rZ  �r[  Rr\  uX   boss_convo_rightr]  }r^  (hj]  j  NhKKp�r_  j	  K�ju  Kj
  Nj  j  j  j  j  Kj  �j  j  j  G        j  Nj  j  ]r`  �ra  Rrb  uuX   market_itemsrc  }rd  X   unlocked_lorere  ]rf  X
   dialog_logrg  ]rh  X   already_triggered_eventsri  ]rj  X   talk_optionsrk  ]rl  X   base_convosrm  }rn  X   current_random_statero  MRX   boundsrp  (K K KKtrq  X	   roam_inforr  capp.engine.roam.roam_info
RoamInfo
rs  )�rt  }ru  (X   roamrv  �X   roam_unit_nidrw  Nubu.