�}q (X   unitsq]q(}q(X   nidqX   AlpinqX
   prefab_nidqhX   positionqKK�qX   teamq	X   playerq
X   partyqX   EirikaqX   klassqX   AlpinVillagerqX   variantqX    qX   factionqNX   levelqKX   expqK_X   genericq�X
   persistentq�X   aiqX   NoneqX   roam_aiqNX   ai_groupqhX   itemsq]q(K�K�K�eX   nameqX   AlpinqX   descqXH   An energetic young lad. Dreams of joining the army of [KINGDOM] someday.qX   tagsq ]q!X   Youthq"aX   statsq#}q$(X   HPq%KX   STRq&KX   MAGq'K X   SKLq(KX   SPDq)KX   LCKq*KX   DEFq+KX   RESq,K X   CONq-KX   MOVq.KuX   growthsq/}q0(h%K<h&K#h'K h(Kh)Kh*Kh+Kh,Kh-K h.KuX   growth_pointsq1}q2(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uX   stat_cap_modifiersq3}q4(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uX   starting_positionq5hX   wexpq6}q7(X   Swordq8KX   Lanceq9K X   Axeq:K X   Bowq;K X   Staffq<K X   Lightq=K X   Animaq>K X   Darkq?K X   Defaultq@K uX   portrait_nidqAX   AlpinqBX   affinityqCX   FireqDX   skillsqE]qF(M?K�capp.engine.source_type
SourceType
qGX   itemqH���qI�qJRqK�qLM@X   gameqMhGX   globalqN���qO�qPRqQ�qRMAhMhQ�qSMBhhGX   personalqT���qU�qVRqW�qXMChhW�qYM�hhGX   terrainqZ���q[�q\Rq]�q^eX   notesq_]q`X
   current_hpqaKX   current_manaqbK X   current_fatigueqcK X   travelerqdNX   current_guard_gaugeqeK X   built_guardqf�X   deadqg�X   action_stateqh(��������tqiX   _fieldsqj}qkX   equipped_weaponqlK�X   equipped_accessoryqmNu}qn(hX   CarlinqohhohKK�qph	X   playerqqhhhX   CarlinVillagerqrhhhNhKhK h�h�hX   NoneqshNhhh]qt(K�K�K�ehX   CarlinquhXA   A magically gifted youngster from [TOWN]. Energetic and sporatic.qvh ]qwX   Youthqxah#}qy(h%K
h&K h'Kh(Kh)Kh*Kh+Kh,Kh-Kh.Kuh/}qz(h%K2h&K h'Kh(K#h)Kh*Kh+Kh,Kh-K h.Kuh1}q{(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}q|(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5hph6}q}(h8K h9K h:K h;K h<K h=K h>K h?Kh@K uhAX   Carlinq~hCX   WindqhE]q�(MDK�hK�q�MEhMhQ�q�MFhMhQ�q�MGhohW�q�MHhohW�q�M�hph]�q�eh_]q�haK
hbK hcK hdNheK hf�hg�hh(��������tq�hj}q�hlK�hmNu}q�(hX   Shaylaq�hh�hKK�q�h	X   playerq�hhhX   ShaylaVillagerq�hhhNhKhK h�h�hX   Noneq�hNhhh]q�(K�K�K�ehX   Shaylaq�hXI   A clumsy young lass who hopes to adventure the world. Gutsy and fearless.q�h ]q�X   Youthq�ah#}q�(h%Kh&Kh'K h(Kh)Kh*Kh+Kh,K h-Kh.Kuh/}q�(h%KAh&Kh'K h(Kh)Kh*Kh+K#h,Kh-K h.Kuh1}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5h�h6}q�(h8K h9K h:Kh;K h<K h=K h>K h?K h@K uhAX   Shaylaq�hCX   Thunderq�hE]q�(MIK�hK�q�MJhMhQ�q�MKhMhQ�q�MLh�hW�q�MMh�hW�q�M�h�h]�q�eh_]q�haKhbK hcK hdNheK hf�hg�hh(��������tq�hj}q�hlK�hmNu}q�(hX   Raelinq�hh�hKK�q�h	X   playerq�hhhX   ZoyaVillagerq�hhhNhKhK h�h�hX   Noneq�hNhhh]q�(K�K�K�ehX   Raelinq�hX8   The shut-in daughter of a fisherman. Timid and cautious.q�h ]q�X   Youthq�ah#}q�(h%Kh&Kh'K h(Kh)Kh*Kh+Kh,Kh-Kh.Kuh/}q�(h%K7h&Kh'K h(Kh)K#h*Kh+Kh,Kh-K h.Kuh1}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5h�h6}q�(h8K h9Kh:K h;K h<K h=K h>K h?K h@K uhAX   Raelin (LittleKyng)q�hCX   Iceq�hE]q�(MNK�hK�q�MOhMhQ�q�MPhMhQ�q�MQh�hW�q�MRh�hW�q�M�h�h]�q�eh_]q�haKhbK hcK hdNheK hf�hg�hh(��������tq�hj}q�hlK�hmNu}q�(hX   Elwynnq�hh�hKK	�q�h	X   playerq�hhhX   Monkq�hhhNhKhK h�h�hX   Noneq�hNhhh]q�(K�K�K�MehX   Elwynnq�hXM   Former vagabond turned holy monk of [CHURCH]. Stern and generally unpleasant.q�h ]q�X   Adultq�ah#}q�(h%Kh&K h'Kh(Kh)Kh*Kh+Kh,Kh-Kh.Kuh/}q�(h%K7h&K h'Kh(Kh)Kh*K
h+Kh,K#h-K h.Kuh1}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5h�h6}q�(h8K h9K h:K h;K h<K h=Kh>K h?K h@K uhAX   Elwynn (LittleKyng)q�hCX   Darkq�hE]q�(MShMhQ�q�MThMhQ�q�MUh�hW�q�MVh�hW�q�MWh�hW�q�M�h�h]�q�eh_]q�haKhbK hcK hdNheK hf�hg�hh(��������tq�hj}q�hlK�hmNu}q�(hX   Saraidq�hh�hKK�q�h	X   playerq�hhhX   Hunterq�hhhNhKhK
h�h�hX   Noneq�hNhhh]q�(K�K�K�K�ehX   Saraidq�hX0   A fledgling huntress. Unreserved and unfiltered.q�h ]q�X   Adultq�ah#}q�(h%Kh&Kh'K h(Kh)Kh*K h+K h,Kh-Kh.Kuh/}q�(h%K2h&Kh'K h(Kh)K#h*Kh+Kh,Kh-K h.Kuh1}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}q�(h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5KK�q�h6}q�(h8K h9K h:K h;G@(      h<K h=K h>K h?K h@K uhAX   Sloaneq�hCX   Lightq�hE]q�(MXK�hK�q�MYhMhQ�q�MZhMhQ�q�M[h�hW�q�M\h�hW�q�M]h�hW�q�M�h�h]�q�eh_]q�haKhbK hcK hdNheK hf�hg�hh(��������tq�hj}q�hlK�hmNu}q�(hX   Quinleyq�hh�hKK�q�h	X   playerq�hhhX   Mager   hX   Quinleyr  hNhKhK h�h�hX   Noner  hNhhh]r  (K�K�K�ehX   Quinleyr  hXN   A genius magician who's grown tired of elemental magic. Outgoing and friendly.r  h ]r  X   Adultr  ah#}r  (h%Kh&K h'Kh(Kh)Kh*Kh+K h,Kh-Kh.Kuh/}r	  (h%K2h&K h'K#h(Kh)Kh*Kh+Kh,Kh-K h.Kuh1}r
  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5h�h6}r  (h8K h9K h:K h;K h<K h=K h>Kh?K h@K uhAX   Quinleyr  hCX   Darkr  hE]r  (M^K�hK�r  M_hMhQ�r  M`hMhQ�r  Mah�hW�r  Mbh�hW�r  Mch�hW�r  M�h�h]�r  eh_]r  haKhbK hcK hdNheK hf�hg�hh(��������tr  hj}r  hlK�hmNu}r  (hX   Elspethr  hj  hKK�r  h	X   playerr  hhhX   Clericr  hX   Elspethr  hNhKhK h�h�hX   Noner   hNhhh]r!  K�ahX   Elspethr"  hX2   Lifelong devotee of [CHURCH]. Humble and selfless.r#  h ]r$  X   Adultr%  ah#}r&  (h%Kh&K h'Kh(K h)Kh*Kh+K h,Kh-Kh.Kuh/}r'  (h%K2h&K h'Kh(Kh)Kh*K#h+Kh,K#h-K h.Kuh1}r(  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r)  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5j  h6}r*  (h8K h9K h:K h;K h<Kh=K h>K h?K h@K uhAX   Elspethr+  hCX   Lightr,  hE]r-  (MdhMhQ�r.  MehMhQ�r/  Mfj  hW�r0  Mgj  hW�r1  Mhj  hW�r2  M�j  h]�r3  eh_]r4  haKhbK hcK hdNheK hf�hg�hh(��������tr5  hj}r6  hlNhmNu}r7  (hX   Lamonter8  hj8  hKK
�r9  h	X   playerr:  hhhX   Soldierr;  hX   Lamonter<  hNhKhK h�h�hX   Noner=  hNhhh]r>  (K�K�K�ehX   Lamonter?  hX,   One of Leod's guards. Kindhearted and loyal.r@  h ]rA  X   AdultrB  ah#}rC  (h%Kh&Kh'K h(Kh)Kh*Kh+Kh,K h-K
h.Kuh/}rD  (h%KAh&Kh'K h(Kh)Kh*K
h+K#h,Kh-K h.Kuh1}rE  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}rF  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5j9  h6}rG  (h8K h9Kh:K h;K h<K h=K h>K h?K h@K uhAX   LamonterH  hCX   IcerI  hE]rJ  (MihMhQ�rK  MjhMhQ�rL  Mkj8  hW�rM  Mlj8  hW�rN  Mmj8  hW�rO  MnK�hK�rP  M�j9  h]�rQ  eh_]rR  haKhbK hcK hdNheK hf�hg�hh(��������trS  hj}rT  hlK�hmNu}rU  (hX   GarveyrV  hjV  hKK	�rW  h	X   playerrX  hhhX   ArcherrY  hX   GarveyrZ  hNhKhK h�h�hX   Noner[  hNhhh]r\  (K�K�K�ehX   Garveyr]  hXA   One of Leod's guards. Dislikes his hometown and fellow villagers.r^  h ]r_  X   Adultr`  ah#}ra  (h%Kh&Kh'K h(Kh)Kh*Kh+Kh,K h-K
h.Kuh/}rb  (h%K7h&K#h'K h(Kh)Kh*K
h+Kh,Kh-K h.Kuh1}rc  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}rd  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5jW  h6}re  (h8K h9K h:K h;Kh<K h=K h>K h?K h@K uhAX   Garveyrf  hCX   Firerg  hE]rh  (MohMhQ�ri  MphMhQ�rj  MqjV  hW�rk  MrjV  hW�rl  MsK�hK�rm  M�jW  h]�rn  M�NhGX   defaultro  ���rp  �rq  Rrr  �rs  eh_]rt  haKhbK hcK hdNheK hf�hg�hh(��������tru  hj}rv  hlK�hmNu}rw  (hX   Orlarx  hjx  hK	K�ry  h	X   playerrz  hhhX   Fighterr{  hX   Orlar|  hNhKhK h�h�hX   Noner}  hNhhh]r~  (K�K�K�ehX   Orlar  hXK   Lumberjack, carpenter, and all around outdoorsperson. Outspoken and jovial.r�  h ]r�  X   Adultr�  ah#}r�  (h%Kh&Kh'K h(Kh)Kh*Kh+Kh,K h-Kh.Kuh/}r�  (h%KAh&K#h'K h(Kh)Kh*K
h+Kh,Kh-K h.K
uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5jy  h6}r�  (h8K h9K h:Kh;K h<K h=K h>K h?K h@K uhAX   Orlar�  hCX   Windr�  hE]r�  (MthMhQ�r�  MuhMhQ�r�  Mvjx  hW�r�  Mwjx  hW�r�  Mxjx  hW�r�  MyK�hK�r�  M�jy  h]�r�  eh_]r�  haKhbK hcK hdNheK hf�hg�hh(��������tr�  hj}r�  hlK�hmNu}r�  (hX   Ailsar�  hj�  hKK�r�  h	X   playerr�  hhhX   Lancerr�  hX   Ailsar�  hNhKhK h�h�hX   Noner�  hNhhh]r�  (K�K�K�ehX   Ailsar�  hXE   A reclusive fisherman. Quick witted and sharped tongued. Zoya's aunt.r�  h ]r�  X   Adultr�  ah#}r�  (h%Kh&Kh'K h(Kh)Kh*Kh+K h,Kh-K	h.Kuh/}r�  (h%K<h&Kh'K h(Kh)K#h*K
h+Kh,Kh-K h.K
uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5j�  h6}r�  (h8K h9Kh:K h;K h<K h=K h>K h?K h@K uhAX   Ailsar�  hCX   Thunderr�  hE]r�  (MzhMhQ�r�  M{hMhQ�r�  M|j�  hW�r�  M}j�  hW�r�  M~j�  hW�r�  MK�hK�r�  M�j�  h]�r�  eh_]r�  haKhbK hcK hdNheK hf�hg�hh(��������tr�  hj}r�  hlK�hmNu}r�  (hX   Corridonr�  hj�  hKK�r�  h	X   playerr�  hhhX
   Blacksmithr�  hhhNhKhK h�h�hX   Noner�  hNhhh]r�  (K�K�ehX   Corridonr�  hX;   Town blacksmith and retired war veteran. Friendly and just.r�  h ]r�  X   Adultr�  ah#}r�  (h%Kh&K h'K h(K h)Kh*Kh+Kh,Kh-Kh.Kuh/}r�  (h%KAh&K h'K h(K h)Kh*K#h+K#h,K#h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5j�  h6}r�  (h8K h9K h:K h;K h<K h=K h>K h?K h@K uhAX   Corridonr�  hCX   Lightr�  hE]r�  (M�hMhQ�r�  M�hMhQ�r�  M�j�  hW�r�  M�j�  hW�r�  M�j�  hW�r�  M�j�  hW�r�  M�j�  h]�r�  eh_]r�  haKhbK hcK hdNheK hf�hg�hh(��������tr�  hj}r�  hlNhmNu}r�  (hX   Laisrenr�  hj�  hKK	�r�  h	X   playerr�  hhhX   Cavalierr�  hX   Cathalr�  hNhKhK h�h�hX   Noner�  hNhhh]r�  (K�K�K�ehX   Laisrenr�  hXV   An aspiring recruit in [KINGDOM]'s army. Charming and outgoing. Alpin's older brother.r�  h ]r�  X   Adultr�  ah#}r�  (h%Kh&Kh'K h(Kh)Kh*Kh+Kh,Kh-Kh.Kuh/}r�  (h%KAh&K#h'K#h(K#h)K#h*K#h+K#h,K#h-K h.Kuh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5j�  h6}r�  (h8Kh9Kh:K h;K h<K h=K h>K h?K h@K uhAX   Cathalr�  hCX   Darkr�  hE]r�  (M�hMhQ�r�  M�hMhQ�r�  M�j�  hW�r�  M�j�  hW�r�  M�j�  hGh���r�  �r�  Rr�  �r�  M�j�  h]�r�  eh_]r�  haKhbK hcK hdNheK hf�hg�hh(��������tr�  hj}r�  hlK�hmNu}r�  (hX	   Cawthorner�  hj�  hKK�r�  h	X   enemyr�  hhhX   Generalr�  hhhNhKhK h�h�hX   Noner�  hNhhh]r�  K�ahX	   Cawthorner�  hX   Cawthorne text.r�  h ]r�  X   Bossr�  ah#}r�  (h%Kh&Kh'K h(Kh)Kh*K h+K	h,Kh-Kh.Kuh/}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5j�  h6}r�  (h8K h9Kh:K)h;K h<K h=K h>K h?K h@K uhAX	   Cawthorner   hCX   Firer  hE]r  (M�hMhQ�r  M�hMhQ�r  M�j�  hW�r  M�j�  hW�r  M�j�  h]�r  eh_]r  haKhbK hcK hdNheK hf�hg�hh(��������tr	  hj}r
  hlK�hmNu}r  (hX   101r  hj  hKK
�r  h	X   enemyr  hhhX   PegasusKnightr  hNhX   Soldierr  hKhK h�h�hX   Noner  hNhhh]r  hX   Soldierr  hX=   The army of Grado, the largest nation on the entire continentr  h ]r  h#}r  (h%Kh&Kh'K h(Kh)Kh*K h+K h,Kh-Kh.Kuh/}r  (h%K-h&Kh'K h(Kh)Kh*K h+K h,Kh-K h.K uh1}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5j  h6}r  (h8Kh9K h:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r  (M�hMhQ�r  M�hMhQ�r  M�j  j�  �r  eh_]r  haKhbK hcK hdNheK hf�hg�hh(��������tr   hj}r!  hlNhmNu}r"  (hX   102r#  hj#  hKK�r$  h	X   otherr%  hhhX   ManDeadr&  hNhX   Villagerr'  hKhK h�h�hX   Noner(  hNhhh]r)  K�ahX   Villagerr*  hX   vilgerr+  h ]r,  h#}r-  (h%K
h&K h'K h(K h)K h*K h+K h,K h-K h.K uh/}r.  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}r/  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r0  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5j$  h6}r1  (h8K h9K h:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r2  (M�hMhQ�r3  M�hMhQ�r4  eh_]r5  haK
hbK hcK hdNheK hf�hg�hh(��������tr6  hj}r7  hlNhmNu}r8  (hX   103r9  hj9  hNh	X   enemyr:  hhhX   GriffonKnightr;  hNhX   Soldierr<  hKhK h�h�hX   Noner=  hNhhh]r>  K�ahj  hj  h ]r?  h#}r@  (h%K	h&Kh'K h(Kh)Kh*K h+Kh,Kh-Kh.Kuh/}rA  (h%K7h&K
h'K h(Kh)Kh*K h+Kh,K
h-K h.K uh1}rB  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}rC  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}rD  (h8K h9Kh:K h;K h<K h=K h>K h?K h@K uhANhCNhE]rE  (M�hMhQ�rF  M�hMhQ�rG  M�j;  j�  �rH  M�K�hK�rI  eh_]rJ  haK	hbK hcK hdNheK hf�hg�hh(��������trK  hj}rL  hlK�hmNu}rM  (hX   104rN  hjN  hNh	X   enemyrO  hhhX   FighterrP  hNhX   SoldierrQ  hKhK h�h�hX   NonerR  hNhhh]rS  K�ahj  hj  h ]rT  h#}rU  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,K h-Kh.Kuh/}rV  (h%KZh&Kh'K h(Kh)Kh*K h+K h,K h-K h.K uh1}rW  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}rX  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}rY  (h8K h9K h:Kh;K h<K h=K h>K h?K h@K uhANhCNhE]rZ  (M�hMhQ�r[  M�hMhQ�r\  M�K�hK�r]  eh_]r^  haKhbK hcK hdNheK hf�hg�hh(��������tr_  hj}r`  hlK�hmNu}ra  (hX   105rb  hjb  hKK�rc  h	X   enemyrd  hhhX   Archerre  hNhX   Soldierrf  hKhK h�h�hX   Nonerg  hNhhh]rh  (M MMehj  hj  h ]ri  h#}rj  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,Kh-Kh.Kuh/}rk  (h%K7h&Kh'K h(Kh)Kh*K h+K h,K h-K h.K uh1}rl  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}rm  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5jc  h6}rn  (h8K h9K h:K h;Kh<K h=K h>K h?K h@K uhANhCNhE]ro  (M�hMhQ�rp  M�hMhQ�rq  M�jc  h]�rr  eh_]rs  haKhbK hcK hdNheK hf�hg�hh(��������trt  hj}ru  hlM hmNu}rv  (hX   106rw  hjw  hKK�rx  h	X   enemyry  hhhX   Soldierrz  hNhX   Soldierr{  hKhK h�h�hX   Yolor|  hNhhh]r}  Mahj  hj  h ]r~  h#}r  (h%K	h&Kh'K h(Kh)Kh*K h+Kh,K h-Kh.Kuh/}r�  (h%KFh&Kh'K h(Kh)Kh*K h+Kh,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5jx  h6}r�  (h8K h9Kh:K h;K h<K h=K h>K h?K h@K uhANhCNhE]r�  (M�hMhQ�r�  M�hMhQ�r�  M�MhK�r�  M�jx  h]�r�  M�X
   Burning1-ar�  hGX   regionr�  ���r�  �r�  Rr�  �r�  eh_]r�  haKhbK hcK hdNheK hf�hg�hh(��������tr�  hj}r�  hlMhmNu}r�  (hX	   Davenportr�  hj�  hNh	X   enemyr�  hhhX   Great_Knightr�  hhhNhKhK h�h�hX   Noner�  hNhhh]r�  (MMMMehX	   Davenportr�  hX   Davenport text.r�  h ]r�  X   Bossr�  ah#}r�  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,Kh-Kh.Kuh/}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8Kh9Kh:Kh;K h<K h=K h>K h?K h@K uhAX	   Davenportr�  hCX   Icer�  hE]r�  (M�hMhQ�r�  M�hMhQ�r�  M�j�  hW�r�  M�j�  j�  �r�  M�MhK�r�  eh_]r�  haKhbK hcK hdNheK hf�hg�hh(��������tr�  hj}r�  hlMhmNu}r�  (hX
   Brassfieldr�  hj�  hKK�r�  h	X   enemyr�  hhhX   Heror�  hhhNhKhK h�h�hX   Noner�  hNhhh]r�  (MM	M
ehX
   Brassfieldr�  hX   Brassfield text.r�  h ]r�  X   Bossr�  ah#}r�  (h%Kh&Kh'Kh(Kh)Kh*K h+Kh,Kh-Kh.Kuh/}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5j�  h6}r�  (h8Kh9K h:Kh;K h<K h=K h>K h?K h@K uhAX
   Brassfieldr�  hCX   Lightr�  hE]r�  (M�hMhQ�r�  M�hMhQ�r�  M�j�  hW�r�  M�MhK�r�  M�j�  h]�r�  eh_]r�  haKhbK hcK hdNheK hf�hg�hh(��������tr�  hj}r�  hlMhmNu}r�  (hX   Ackermanr�  hj�  hNh	X   enemyr�  hhhX   Warriorr�  hhhNhKhK h�h�hX   Noner�  hNhhh]r�  (MMMehX   Ackermanr�  hX   Ackerman text.r�  h ]r�  X   Bossr�  ah#}r�  (h%Kh&Kh'K h(Kh)Kh*K h+Kh,K h-Kh.Kuh/}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh1}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh3}r�  (h%K h&K h'K h(K h)K h*K h+K h,K h-K h.K uh5Nh6}r�  (h8K h9K h:Kh;Kh<K h=K h>K h?K h@K uhAX   Ackermanr�  hCX   Firer�  hE]r�  (M�hMhQ�r�  M�hMhQ�r�  M�j�  hW�r�  M�MhK�r�  eh_]r�  haKhbK hcK hdNheK hf�hg�hh(��������tr�  hj}r�  hlMhmNueh]r�  (}r�  (X   uidr�  K�hX
   Iron Swordr�  hX
   Iron Swordr�  hhX	   owner_nidr�  hX	   droppabler�  �X   datar�  }r�  X   subitemsr�  ]r�  X
   componentsr�  ]r�  (X   weaponr�  N�r�  X   target_enemyr�  N�r�  X	   min_ranger�  K�r�  X	   max_ranger�  K�r�  X   damager�  K�r�  X   hitr�  K
�r�  X	   level_expr�  N�r�  X   weapon_typer�  X   Swordr�  �r�  X   critr�  K �r�  eu}r   (j�  K�hX   HeartOfFirer  hX    <orange>Heart Of Flames</orange>r  hX   +1 Damage. Fire affinity only.r  j�  hj�  �j�  }r  j�  ]r  j�  ]r  (X   status_on_holdr  X   HeartofFirer  �r	  X
   text_colorr
  X   oranger  �r  eu}r  (j�  K�hX   Herbsr  hX   Herbsr  hX(   Restores 4 HP. Unit can act after using.r  j�  hj�  �j�  }r  (X   usesr  KX   starting_usesr  Kuj�  ]r  j�  ]r  (X   usabler  N�r  X   target_allyr  N�r  j  K�r  X   uses_optionsr  ]r  ]r  (X   LoseUsesOnMiss (T/F)r  X   Fr  X   Lose uses even on missr   ea�r!  X   healr"  K�r#  X   map_hit_add_blendr$  ]r%  (K`K�K�e�r&  X   attack_after_combatr'  N�r(  eu}r)  (j�  K�hX   Jinxr*  hX   Jinxr+  hX,   Lowers target's Crit Avoid by 20 for 1 turn.r,  j�  hoj�  �j�  }r-  j�  ]r.  j�  ]r/  (j�  N�r0  j�  N�r1  j�  K�r2  j�  K�r3  j�  K�r4  j�  K�r5  j�  N�r6  j�  X   Darkr7  �r8  j$  ]r9  (KHK KAe�r:  X   magicr;  N�r<  X   unrepairabler=  N�r>  X   status_on_hitr?  X	   Terrifiedr@  �rA  j�  K �rB  X   battle_cast_animrC  X   FluxrD  �rE  eu}rF  (j�  K�hX   DevilryrG  hX   DevilryrH  hX   Can't miss. -2 SPD.
HP Cost: 4.rI  j�  hoj�  �j�  }rJ  (j  Kj  Kuj�  ]rK  j�  ]rL  (j�  N�rM  j�  N�rN  j�  K�rO  j�  K�rP  j�  K�rQ  j�  N�rR  j�  X   DarkrS  �rT  j$  ]rU  (KHK KAe�rV  j;  N�rW  j=  N�rX  j  K�rY  j  ]rZ  (]r[  (X   LoseUsesOnMiss (T/F)r\  j  X   Lose uses even on missr]  e]r^  (X   OneLossPerCombat (T/F)r_  j  X    Doubling doesn't cost extra usesr`  ee�ra  j�  K �rb  X   status_on_equiprc  X   SPD2rd  �re  h6K�rf  X   hp_costrg  K�rh  jC  X   Fluxri  �rj  eu}rk  (j�  K�hX   HeartOfGalesrl  hX   <orange>Heart Of Gales</orange>rm  hX   +1 SPD. Wind affinity only.rn  j�  hoj�  �j�  }ro  j�  ]rp  j�  ]rq  (j  X   HeartofGalesrr  �rs  j
  X   orangert  �ru  eu}rv  (j�  K�hX   Iron Axerw  hX   Iron Axerx  hhj�  h�j�  �j�  }ry  j�  ]rz  j�  ]r{  (j�  N�r|  j�  N�r}  j�  K�r~  j�  K�r  j�  K�r�  j�  K �r�  j�  N�r�  j�  X   Axer�  �r�  j�  K �r�  eu}r�  (j�  K�hX   HeartOfLightningr�  hX#   <orange>Heart Of Lightning</orange>r�  hX    +10 Crit. Thunder affinity only.r�  j�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j  X   HeartofLightningr�  �r�  j
  X   oranger�  �r�  eu}r�  (j�  K�hj  hj  hj  j�  h�j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j  N�r�  j  N�r�  j  K�r�  j  j  �r�  j"  K�r�  j$  j%  �r�  j'  N�r�  eu}r�  (j�  K�hX
   Iron Lancer�  hX
   Iron Lancer�  hhj�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  N�r�  j�  X   Lancer�  �r�  j�  K �r�  eu}r�  (j�  K�hX   Javelinr�  hX   Javelinr�  hX   Cannot Counter. SPD -2.r�  j�  h�j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  J�����r�  j�  K �r�  j�  N�r�  j�  X   Lancer�  �r�  j  K�r�  j  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j  X    Doubling doesn't cost extra usesr�  ee�r�  X   cannot_counterr�  N�r�  h6K�r�  jc  X   SPD2r�  �r�  eu}r�  (j�  K�hX   HeartOfFrostr�  hX   <orange>Heart Of Frost</orange>r�  hX   +1 DEF/RES. Ice affinity only.r�  j�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j  X   HeartofFrostr�  �r�  j
  X   oranger�  �r�  eu}r�  (j�  K�hX   Flashr�  hX   Flashr�  hX*   Lowers target's Accuracy by 20 for 1 turn.r�  j�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K
�r�  j�  K
�r�  j�  N�r�  j�  X   Lightr�  �r�  j$  ]r�  (K�K�K e�r�  j;  N�r�  j=  N�r�  jC  X   Glimmerr�  �r�  j?  X   Dazzledr�  �r�  eu}r�  (j�  K�hX   Shimmerr�  hX   Shimmerr�  hX   Cannot miss.r�  j�  h�j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K
�r�  j�  N�r�  j�  X   Lightr�  �r�  j$  ]r�  (K�K�K e�r   j;  N�r  j=  N�r  h6K�r  j  K�r  j  ]r  (]r  (X   LoseUsesOnMiss (T/F)r  j  X   Lose uses even on missr  e]r	  (X   OneLossPerCombat (T/F)r
  j  X    Doubling doesn't cost extra usesr  ee�r  jC  X   Glimmerr  �r  eu}r  (j�  K�hj  hj  hj  j�  h�j�  �j�  }r  (j  Kj  Kuj�  ]r  j�  ]r  (j  N�r  j  N�r  j  K�r  j  j  �r  j"  K�r  j$  j%  �r  j'  N�r  eu}r  (j�  K�hX   Iron Bowr  hX   Iron Bowr  hhj�  h�j�  �j�  }r  j�  ]r  j�  ]r  (j�  N�r   j�  N�r!  j�  K�r"  j�  K�r#  j�  K�r$  j�  K
�r%  j�  N�r&  j�  X   Bowr'  �r(  X   effective_damager)  }r*  (X   effective_tagsr+  ]r,  X   Flyingr-  aX   effective_multiplierr.  G@      X   effective_bonus_damager/  K X   show_effectiveness_flashr0  �u�r1  j�  K �r2  X   eval_warningr3  X   'Flying' in unit.tagsr4  �r5  eu}r6  (j�  K�hX   Longbowr7  hX   Longbowr8  hX   Cannot Counter. SPD -1.r9  j�  h�j�  �j�  }r:  (j  K
j  K
uj�  ]r;  j�  ]r<  (j�  N�r=  j�  N�r>  j�  N�r?  j�  K�r@  j�  J�����rA  j�  X   BowrB  �rC  j  K
�rD  j  ]rE  (]rF  (X   LoseUsesOnMiss (T/F)rG  j  X   Lose uses even on missrH  e]rI  (X   OneLossPerCombat (T/F)rJ  j  X    Doubling doesn't cost extra usesrK  ee�rL  j�  K�rM  j�  K�rN  j�  K�rO  j�  N�rP  j)  }rQ  (j+  ]rR  X   FlyingrS  aj.  G@      j/  K j0  �u�rT  jc  X   SPD1rU  �rV  j3  X   'Flying' in unit.tagsrW  �rX  eu}rY  (j�  K�hX   HeartOfRadiancerZ  hX"   <orange>Heart Of Radiance</orange>r[  hX   +15 Avoid. Holy affinity only.r\  j�  h�j�  �j�  }r]  j�  ]r^  j�  ]r_  (j  X   HeartofRadiancer`  �ra  j
  X   orangerb  �rc  eu}rd  (j�  K�hX   Firestarterre  hX   <purple>Firestarter</purple>rf  hX=   Saraid Only.
Sets terrain ablaze after combat.
Cannot double.rg  j�  h�j�  �j�  }rh  (j  Kj  Kuj�  ]ri  j�  ]rj  (j�  N�rk  j�  N�rl  j�  K�rm  j�  K�rn  j�  K�ro  j�  K�rp  j�  K �rq  j  K�rr  j  ]rs  (]rt  (X   LoseUsesOnMiss (T/F)ru  j  X   Lose uses even on missrv  e]rw  (X   OneLossPerCombat (T/F)rx  j  X    Doubling doesn't cost extra usesry  ee�rz  j�  N�r{  j�  X   Bowr|  �r}  X   prf_unitr~  ]r  X   Saraidr�  a�r�  j
  X   purpler�  �r�  j)  }r�  (j+  ]r�  X   Flyingr�  aj.  G@       j/  K j0  �u�r�  X   event_after_combat_even_missr�  X   Global Firestarter_Originr�  �r�  X	   no_doubler�  N�r�  eu}r�  (j�  K�hX   Entangler�  hX   Entangler�  hX'   Lowers target's Avoid by 20 for 1 turn.r�  j�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  N�r�  j�  X   Animar�  �r�  j$  ]r�  (KUKUK e�r�  j;  N�r�  j=  N�r�  j?  X	   Entangledr�  �r�  j�  K �r�  jC  X   Firer�  �r�  eu}r�  (j�  K�hX   Firer�  hX   Firer�  hX*   Effective against horseback units. -1 SPD.r�  j�  h�j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K �r�  j�  K�r�  j  K�r�  j  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j  X    Doubling doesn't cost extra usesr�  ee�r�  j�  N�r�  j�  X   Animar�  �r�  j$  ]r�  (K�K`K e�r�  j;  N�r�  j=  N�r�  j)  }r�  (j+  ]r�  X   Horser�  aj.  G@      j/  K j0  �u�r�  h6K�r�  jc  X   SPD1r�  �r�  j3  X   'Horse' in unit.tagsr�  �r�  jC  X   Firer�  �r�  eu}r�  (j�  K�hX   HeartOfDarknessr�  hX"   <orange>Heart Of Darkness</orange>r�  hX   +15 Hit. Dark affinity only.r�  j�  h�j�  �j�  }r�  j�  ]r�  j�  ]r�  (j  X   HeartofDarknessr�  �r�  j
  X   oranger�  �r�  eu}r�  (j�  K�hX	   ChurchKeyr�  hX
   Church Keyr�  hX   Opens any door in the church.r�  j�  j  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j  K�r�  j  ]r�  ]r�  (X   LoseUsesOnMiss (T/F)r�  j  X   Lose uses even on missr�  ea�r�  X
   can_unlockr�  X7   region.nid.startswith('Door') and game.level.nid == '2'r�  �r�  eu}r�  (j�  K�hX   Steel Lancer�  hX   Steel Lancer�  hX   SPD -1.r�  j�  j8  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  N�r�  j�  K�r�  j�  K �r�  j�  X   Lancer�  �r�  j  K�r�  j  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j  X    Doubling doesn't cost extra usesr�  ee�r   j�  K �r  j�  K�r  j�  K�r  jc  X   SPD1r  �r  eu}r  (j�  K�hX	   Axereaverr  hX	   Axereaverr  hX%   Reverses the weapon triangle. SPD -1.r	  j�  j8  j�  �j�  }r
  (j  Kj  Kuj�  ]r  j�  ]r  (j�  N�r  j�  N�r  j�  N�r  j�  K�r  j�  K �r  j�  X   Lancer  �r  j  K�r  j  ]r  (]r  (X   LoseUsesOnMiss (T/F)r  j  X   Lose uses even on missr  e]r  (X   OneLossPerCombat (T/F)r  j  X    Doubling doesn't cost extra usesr  ee�r  X   reaverr  N�r  h6K�r  j�  K
�r   j�  K�r!  j�  K�r"  j3  XU   target.get_weapon() and item_system.weapon_type(target, target.get_weapon()) == 'Axe'r#  �r$  eu}r%  (j�  K�hj  hj  hj  j�  j8  j�  �j�  }r&  (j  Kj  Kuj�  ]r'  j�  ]r(  (j  N�r)  j  N�r*  j  K�r+  j  j  �r,  j"  K�r-  j$  j%  �r.  j'  N�r/  eu}r0  (j�  K�hX   Greatbowr1  hX	   Steel Bowr2  hX   SPD -2.r3  j�  jV  j�  �j�  }r4  (j  Kj  Kuj�  ]r5  j�  ]r6  (j�  N�r7  j�  N�r8  j�  N�r9  j�  K�r:  j�  K �r;  j�  X   Bowr<  �r=  j  K�r>  j  ]r?  (]r@  (X   LoseUsesOnMiss (T/F)rA  j  X   Lose uses even on missrB  e]rC  (X   OneLossPerCombat (T/F)rD  j  X    Doubling doesn't cost extra usesrE  ee�rF  j�  K �rG  j�  K�rH  j�  K�rI  jc  X   SPD2rJ  �rK  j)  }rL  (j+  ]rM  X   FlyingrN  aj.  G@      j/  K j0  �u�rO  j3  X   'Flying' in unit.tagsrP  �rQ  eu}rR  (j�  K�hX   Mini_BowrS  hX   Mini BowrT  hX   SPD -1.rU  j�  jV  j�  �j�  }rV  (j  K
j  K
uj�  ]rW  j�  ]rX  (j�  N�rY  j�  N�rZ  j�  N�r[  j�  K�r\  j�  J�����r]  j�  X   Bowr^  �r_  j  K
�r`  j  ]ra  (]rb  (X   LoseUsesOnMiss (T/F)rc  j  X   Lose uses even on missrd  e]re  (X   OneLossPerCombat (T/F)rf  j  X    Doubling doesn't cost extra usesrg  ee�rh  j�  K�ri  h6K�rj  j�  K�rk  j�  K�rl  j)  }rm  (j+  ]rn  X   Flyingro  aj.  G@      j/  K j0  �u�rp  jc  X   SPD1rq  �rr  j3  X   'Flying' in unit.tagsrs  �rt  eu}ru  (j�  K�hj  hj  hj  j�  jV  j�  �j�  }rv  (j  Kj  Kuj�  ]rw  j�  ]rx  (j  N�ry  j  N�rz  j  K�r{  j  j  �r|  j"  K�r}  j$  j%  �r~  j'  N�r  eu}r�  (j�  K�hX	   Steel Axer�  hX	   Steel Axer�  hX   SPD -2.r�  j�  jx  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  J�����r�  j�  K �r�  j  K�r�  j  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j  X    Doubling doesn't cost extra usesr�  ee�r�  j�  N�r�  j�  X   Axer�  �r�  h6K�r�  jc  X   SPD2r�  �r�  eu}r�  (j�  K�hX
   EmeraldAxer�  hX   Emerald Axer�  hX$   Doubles the weapon triangle. SPD -1.r�  j�  jx  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  J�����r�  j�  K
�r�  j  K�r�  j  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j  X    Doubling doesn't cost extra usesr�  ee�r�  j�  N�r�  j�  X   Axer�  �r�  h6K�r�  X   double_triangler�  N�r�  jc  X   SPD1r�  �r�  X   warningr�  N�r�  eu}r�  (j�  K�hj  hj  hj  j�  jx  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j  N�r�  j  N�r�  j  K�r�  j  j  �r�  j"  K�r�  j$  j%  �r�  j'  N�r�  eu}r�  (j�  K�hj�  hj�  hj�  j�  j�  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  N�r�  j�  K�r�  j�  K �r�  j�  j�  �r�  j  K�r�  j  j�  �r�  j�  K �r�  j�  K�r�  j�  K�r�  jc  j  �r�  eu}r�  (j�  K�hX   SapphireLancer�  hX   Sapphire Lancer�  hX$   Doubles the weapon triangle. SPD -1.r�  j�  j�  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  N�r�  j�  K�r�  j�  K �r�  j�  X   Lancer�  �r�  j  K�r�  j  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j  X    Doubling doesn't cost extra usesr�  ee�r�  h6K�r�  j�  K
�r�  j�  K�r�  j�  K�r�  j�  N�r�  jc  X   SPD1r�  �r�  j�  N�r�  eu}r�  (j�  K�hj  hj  hj  j�  j�  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j  N�r�  j  N�r�  j  K�r�  j  j  �r�  j"  K�r   j$  j%  �r  j'  N�r  eu}r  (j�  K�hX   Elixirr  hX   Elixirr  hX,   Fully recovers HP. Unit can act after using.r  j�  j�  j�  �j�  }r  (j  Kj  Kuj�  ]r  j�  ]r	  (j  N�r
  j  N�r  j  K�r  j  ]r  ]r  (X   LoseUsesOnMiss (T/F)r  j  X   Lose uses even on missr  ea�r  j"  Kc�r  j$  ]r  (K`K�K�e�r  j'  N�r  eu}r  (j�  K�hX   MakeshiftLockpickr  hX   Makeshift Lockpickr  hX   Opens chests and doors.r  j�  j�  j�  �j�  }r  (j  Kj  Kuj�  ]r  j�  ]r  (j  K�r  j  ]r  ]r  (X   LoseUsesOnMiss (T/F)r   j  X   Lose uses even on missr!  ea�r"  j�  X   Truer#  �r$  eu}r%  (j�  K�hX   Silver Swordr&  hX   Silver Swordr'  hhj�  j�  j�  �j�  }r(  (j  K
j  K
uj�  ]r)  j�  ]r*  (j�  N�r+  j�  N�r,  j�  K�r-  j�  K�r.  j�  K�r/  j�  K
�r0  j�  K �r1  j�  N�r2  j�  X   Swordr3  �r4  j  K
�r5  j  ]r6  (]r7  (X   LoseUsesOnMiss (T/F)r8  j  X   Lose uses even on missr9  e]r:  (X   OneLossPerCombat (T/F)r;  j  X    Doubling doesn't cost extra usesr<  ee�r=  eu}r>  (j�  K�hX   Silver Lancer?  hX   Silver Lancer@  hhj�  j�  j�  �j�  }rA  (j  K
j  K
uj�  ]rB  j�  ]rC  (j�  N�rD  j�  N�rE  j�  K�rF  j�  K�rG  j�  K�rH  j�  K�rI  j�  K �rJ  j�  N�rK  j�  X   LancerL  �rM  j  K
�rN  j  ]rO  (]rP  (X   LoseUsesOnMiss (T/F)rQ  j  X   Lose uses even on missrR  e]rS  (X   OneLossPerCombat (T/F)rT  j  X    Doubling doesn't cost extra usesrU  ee�rV  eu}rW  (j�  K�hX   TheReclaimerrX  hX   <blue>The Reclaimer<blue>rY  hX6   Cavalier line only.
50% Lifelink. Effective Vs. Armor.rZ  j�  j�  j�  �j�  }r[  j�  ]r\  j�  ]r]  (j�  N�r^  j�  N�r_  j�  K�r`  j�  K�ra  j�  K�rb  j�  J�����rc  j�  K
�rd  j�  N�re  j�  X   Lancerf  �rg  X	   prf_classrh  ]ri  (X   Cavalierrj  X   Paladinrk  X   Great_Knightrl  e�rm  j)  }rn  (j+  ]ro  X   Armorrp  aj.  G@      j/  K j0  �u�rq  j
  X   bluerr  �rs  X   lifelinkrt  G?�      �ru  eu}rv  (j�  K�hX
   TheDefilerrw  hX   <blue>The Defiler</blue>rx  hXF   Knight line only.
Inflicts half damage on miss. Effective Vs. Clergy. ry  j�  j�  j�  �j�  }rz  j�  ]r{  j�  ]r|  (j�  N�r}  j�  N�r~  j�  K�r  j�  K�r�  j�  K�r�  j�  J�����r�  j�  K�r�  j�  N�r�  j�  X   Axer�  �r�  jh  ]r�  (X   Knightr�  X   Generalr�  X   Great_Knightr�  e�r�  j)  }r�  (j+  ]r�  X   Clergyr�  aj.  G@      j/  K j0  �u�r�  j
  X   bluer�  �r�  X   damage_on_missr�  G?�      �r�  eu}r�  (j�  K�hj&  hj'  hhj�  j#  j�  �j�  }r�  (j  K
j  K
uj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K
�r�  j�  K �r�  j�  N�r�  j�  j3  �r�  j  K
�r�  j  j6  �r�  eu}r�  (j�  K�hj�  hj�  hj�  j�  j9  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  J�����r�  j�  K �r�  j�  N�r�  j�  j�  �r�  j  K�r�  j  j�  �r�  j�  N�r�  h6K�r�  jc  j�  �r�  eu}r�  (j�  K�hX   Hand Axer�  hX   Hand Axer�  hX   Cannot Counter. SPD -3.r�  j�  jN  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  J�����r�  j�  K �r�  j  K�r�  j  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j  X    Doubling doesn't cost extra usesr�  ee�r�  j�  N�r�  j�  X   Axer�  �r�  j�  N�r�  h6K�r�  jc  X   SPD3r�  �r�  eu}r�  (j�  M hj  hj  hhj�  jb  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K
�r�  j�  N�r�  j�  j'  �r�  j)  }r�  (j+  j,  j.  G@      j/  K j0  �u�r�  j�  K �r�  j3  j4  �r�  eu}r�  (j�  Mhj�  hj�  hhj�  jb  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K
�r�  j�  N�r�  j�  j�  �r�  j�  K �r�  eu}r�  (j�  MhX   EtherealBlader�  hX   Ethereal Blader�  hX   Targets RES.r�  j�  jb  j�  �j�  }r�  (j  K
j  K
uj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K�r�  j  K
�r�  j  ]r�  (]r   (X   LoseUsesOnMiss (T/F)r  j  X   Lose uses even on missr  e]r  (X   OneLossPerCombat (T/F)r  j  X    Doubling doesn't cost extra usesr  ee�r  j�  N�r  j�  X   Swordr  �r	  X   alternate_resist_formular
  X   MAGIC_DEFENSEr  �r  h6K�r  eu}r  (j�  Mhj�  hj�  hj�  j�  jw  j�  �j�  }r  (j  Kj  Kuj�  ]r  j�  ]r  (j�  N�r  j�  N�r  j�  N�r  j�  K�r  j�  K �r  j�  j�  �r  j  K�r  j  j�  �r  j�  K �r  j�  K�r  j�  K�r  jc  j  �r  eu}r  (j�  MhX   WardensCleaverr  hX   <blue>Warden's Cleaver<blue>r   hX&   While equipped unit cannot be doubled.r!  j�  j�  j�  �j�  }r"  j�  ]r#  j�  ]r$  (j�  N�r%  j�  N�r&  j�  K�r'  j�  K�r(  j�  K�r)  j�  K�r*  j�  K
�r+  j�  N�r,  j�  X   Axer-  �r.  j
  X   bluer/  �r0  jc  X   Wardenr1  �r2  eu}r3  (j�  MhX
   BloodLancer4  hX   Blood Lancer5  hX   Has Lifelink. SPD -1.r6  j�  j�  j�  �j�  }r7  (j  K
j  K
uj�  ]r8  j�  ]r9  (j�  N�r:  j�  N�r;  j�  N�r<  j�  K�r=  j�  J�����r>  j�  X   Lancer?  �r@  j  K
�rA  j  ]rB  (]rC  (X   LoseUsesOnMiss (T/F)rD  j  X   Lose uses even on missrE  e]rF  (X   OneLossPerCombat (T/F)rG  j  X    Doubling doesn't cost extra usesrH  ee�rI  j�  K�rJ  j�  K�rK  j�  K�rL  jt  G?�      �rM  h6K�rN  jc  X   SPD1rO  �rP  eu}rQ  (j�  Mhj�  hj�  hj�  j�  j�  j�  �j�  }rR  (j  K
j  K
uj�  ]rS  j�  ]rT  (j�  N�rU  j�  N�rV  j�  K�rW  j�  K�rX  j�  K�rY  j�  K�rZ  j�  K�r[  j  K
�r\  j  j�  �r]  j�  N�r^  j�  j  �r_  j
  j  �r`  h6K�ra  eu}rb  (j�  MhX   AngelFeatherrc  hX   <yellow>Angel Feather</yellow>rd  hX&   Gives unit +10% growths. Cannot stack.re  j�  j�  j�  �j�  }rf  (j  Kj  Kuj�  ]rg  j�  ]rh  (j  N�ri  j  N�rj  j  K�rk  j  ]rl  (]rm  (X   LoseUsesOnMiss (T/F)rn  j  X   Lose uses even on missro  e]rp  (X   OneLossPerCombat (T/F)rq  j  X    Doubling doesn't cost extra usesrr  ee�rs  X   usable_in_basert  N�ru  j
  X   yellowrv  �rw  j$  ]rx  (K�K�K�e�ry  j?  X   AngelFeatherrz  �r{  j�  K �r|  j�  K �r}  X   eval_availabler~  X#   not has_skill(unit, 'AngelFeather')r  �r�  X   event_on_hitr�  X   Global AngelFeatherr�  �r�  eu}r�  (j�  MhX   HowlingBlader�  hX   <blue>Howling Blade</blue>r�  hX4   Grants Distant Counter. Deals magic damage at range.r�  j�  j�  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  K
�r�  j�  N�r�  j�  X   Swordr�  �r�  j
  X   bluer�  �r�  X   magic_at_ranger�  N�r�  jc  X   DistantCounterr�  �r�  eu}r�  (j�  M	hX   Macer�  hX   Macer�  hX   Cannot be Countered. SPD -2.r�  j�  j�  j�  �j�  }r�  (j  K
j  K
uj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  N�r�  j�  K�r�  j�  J�����r�  j�  X   Axer�  �r�  j  K
�r�  j  ]r�  (]r�  (X   LoseUsesOnMiss (T/F)r�  j  X   Lose uses even on missr�  e]r�  (X   OneLossPerCombat (T/F)r�  j  X    Doubling doesn't cost extra usesr�  ee�r�  j�  K�r�  h6K�r�  j�  K�r�  j�  K�r�  X   cannot_be_counteredr�  N�r�  jc  X   SPD2r�  �r�  eu}r�  (j�  M
hjc  hjd  hje  j�  j�  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j  N�r�  j  N�r�  j  K�r�  j  jl  �r�  jt  N�r�  j
  jv  �r�  j$  jx  �r�  j?  jz  �r�  j�  K �r�  j�  K �r�  j~  j  �r�  j�  j�  �r�  eu}r�  (j�  MhX   LunarArcr�  hX	   Lunar Arcr�  hX%   Ignores DEF and grants Close Counter.r�  j�  j�  j�  �j�  }r�  j�  ]r�  j�  ]r�  (jc  X   CloseCounterr�  �r�  j�  N�r�  j�  N�r�  j�  N�r�  j�  K�r�  j
  X   ZEROr�  �r�  j�  K�r�  j�  X   Bowr�  �r�  j�  K
�r�  j�  K�r�  j�  K�r�  j)  }r�  (j+  ]r�  X   Flyingr�  aj.  G@      j/  K j0  �u�r�  j3  X   'Flying' in unit.tagsr�  �r�  eu}r�  (j�  Mhj�  hj�  hj�  j�  j�  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j�  N�r�  j�  N�r�  j�  K�r�  j�  K�r�  j�  K�r�  j�  J�����r�  j�  K
�r�  j  K�r�  j  j�  �r�  j�  N�r�  j�  j�  �r�  h6K�r�  j�  N�r�  jc  j�  �r�  j�  N�r�  eu}r�  (j�  Mhjc  hjd  hje  j�  j�  j�  �j�  }r�  (j  Kj  Kuj�  ]r�  j�  ]r�  (j  N�r�  j  N�r�  j  K�r�  j  jl  �r�  jt  N�r   j
  jv  �r  j$  jx  �r  j?  jz  �r  j�  K �r  j�  K �r  j~  j  �r  j�  j�  �r  eu}r  (j�  MhX   Flairr	  hX   <purple>Flair</purple>r
  hX`   Elwynn Only.
Steals any unequipped item from target.
Can't miss, double, kill, or target bosses.r  j�  h�j�  �j�  }r  (j  K
j  K
X   target_itemr  Nuj�  ]r  j�  ]r  (j�  N�r  j�  N�r  j�  K�r  j�  K�r  j�  K�r  j  K
�r  j  ]r  (]r  (X   LoseUsesOnMiss (T/F)r  j  X   Lose uses even on missr  e]r  (X   OneLossPerCombat (T/F)r  j  X    Doubling doesn't cost extra usesr  ee�r  j�  N�r  j�  X   Lightr  �r   j$  ]r!  (K�K�K e�r"  j;  N�r#  j~  ]r$  X   Elwynnr%  a�r&  j
  X   purpler'  �r(  j?  X	   NonLethalr)  �r*  X   stealr+  N�r,  jC  X   Glimmerr-  �r.  X   eval_target_restrict_2r/  X   'Boss' not in target.tagsr0  �r1  eu}r2  (j�  MhX   Shover3  hX   <green>Shove</>r4  hX!   Free action. Shove target 1 tile.r5  j�  j�  j�  �j�  }r6  (X   cooldownr7  K X   starting_cooldownr8  Kuj�  ]r9  j�  ]r:  (X   target_unitr;  N�r<  j�  K�r=  j�  K�r>  j/  X   target.team == 'player'r?  �r@  j'  N�rA  j7  K�rB  X   shove_on_end_combatrC  K�rD  eu}rE  (j�  MhX   BolsterrF  hX   BolsterrG  hX/   Grants max HP +5 for 2 turns and restores 5 HP.rH  j�  j�  j�  �j�  }rI  j�  ]rJ  j�  ]rK  (j  N�rL  j�  K�rM  j�  K�rN  X   map_hit_sfxrO  X   HealInitrP  �rQ  j$  ]rR  (K K�Ke�rS  j�  X   Global SkillCorridonBolsterrT  �rU  hK�rV  eu}rW  (j�  MhX   RepairrX  hX   RepairrY  hXE   Repairs selected weapon in target's inventory. Does not affect tomes.rZ  j�  j�  j�  �j�  }r[  j  Nsj�  ]r\  j�  ]r]  (j�  K�r^  j�  K�r_  j  N�r`  X   repairra  N�rb  X   never_use_battle_animationrc  N�rd  X   map_cast_sfxre  X   Hammernerf  �rg  j$  ]rh  (K!K�Kke�ri  hK�rj  eu}rk  (j�  Mhj3  hj4  hj5  j�  j�  j�  �j�  }rl  (j7  K j8  Kuj�  ]rm  j�  ]rn  (j;  N�ro  j�  K�rp  j�  K�rq  j/  j?  �rr  j'  N�rs  j7  K�rt  jC  K�ru  eu}rv  (j�  Mhj3  hj4  hj5  j�  jx  j�  �j�  }rw  (j7  K j8  Kuj�  ]rx  j�  ]ry  (j;  N�rz  j�  K�r{  j�  K�r|  j/  j?  �r}  j'  N�r~  j7  K�r  jC  K�r�  eu}r�  (j�  Mhj3  hj4  hj5  j�  jV  j�  �j�  }r�  (j7  K j8  Kuj�  ]r�  j�  ]r�  (j;  N�r�  j�  K�r�  j�  K�r�  j/  j?  �r�  j'  N�r�  j7  K�r�  jC  K�r�  eu}r�  (j�  Mhj3  hj4  hj5  j�  j8  j�  �j�  }r�  (j7  K j8  Kuj�  ]r�  j�  ]r�  (j;  N�r�  j�  K�r�  j�  K�r�  j/  j?  �r�  j'  N�r�  j7  K�r�  jC  K�r�  eu}r�  (j�  Mhj3  hj4  hj5  j�  j  j�  �j�  }r�  (j7  K j8  Kuj�  ]r�  j�  ]r�  (j;  N�r�  j�  K�r�  j�  K�r�  j/  j?  �r�  j'  N�r�  j7  K�r�  jC  K�r�  eu}r�  (j�  MhX   Healr�  hX   <blue>Heal</>r�  hX   Restores (User's MAG * 2) HP.r�  j�  j  j�  �j�  }r�  j�  ]r�  j�  ]r�  (X   spellr�  N�r�  j  N�r�  j�  K�r�  j�  K�r�  j�  X   Staffr�  �r�  hK�r�  h6K�r�  j$  ]r�  (K`K�K�e�r�  j=  N�r�  jC  X   Healr�  �r�  X   equation_healr�  X   MAG2r�  �r�  eu}r�  (j�  MhX   Exchanger�  hX   Exchanger�  hX:   Free action. Trade with allies up to (MAG * 2) tiles away.r�  j�  j  j�  �j�  }r�  j�  ]r�  j�  ]r�  (j�  N�r�  j  N�r�  j�  K�r�  X   max_equation_ranger�  X   MAG2r�  �r�  j$  ]r�  (K�K�Ke�r�  X   trader�  N�r�  X   menu_after_combatr�  N�r�  j'  N�r�  X   ignore_line_of_sightr�  N�r�  je  X   Warpr�  �r�  X   map_cast_animr�  X   Warpr�  �r�  eu}r�  (j�  Mhj3  hj4  hj5  j�  h�j�  �j�  }r�  (j7  K j8  Kuj�  ]r�  j�  ]r�  (j;  N�r�  j�  K�r�  j�  K�r�  j/  j?  �r�  j'  N�r�  j7  K�r�  jC  K�r�  eu}r�  (j�  Mhj3  hj4  hj5  j�  h�j�  �j�  }r�  (j7  K j8  Kuj�  ]r�  j�  ]r�  (j;  N�r�  j�  K�r�  j�  K�r�  j/  j?  �r�  j'  N�r�  j7  K�r�  jC  K�r�  eu}r�  (j�  Mhj3  hj4  hj5  j�  h�j�  �j�  }r�  (j7  K j8  Kuj�  ]r�  j�  ]r�  (j;  N�r�  j�  K�r�  j�  K�r�  j/  j?  �r�  j'  N�r�  j7  K�r�  jC  K�r�  eu}r�  (j�  MhX   Treatr�  hX   Treatr�  hX*   Free Action. Restores (User's MAG + 2) HP.r�  j�  h�j�  �j�  }r�  (j7  K j8  Kuj�  ]r�  j�  ]r�  (j  N�r�  jc  N�r�  j�  K�r�  j�  K�r 	  j�  X   TREATr	  �r	  j'  N�r	  hK
�r	  j7  K�r	  eu}r	  (j�  Mhj3  hj4  hj5  j�  h�j�  �j�  }r	  (j7  K j8  Kuj�  ]r	  j�  ]r		  (j;  N�r
	  j�  K�r	  j�  K�r	  j/  j?  �r	  j'  N�r	  j7  K�r	  jC  K�r	  eu}r	  (j�  MhX   Tripr	  hX   Tripr	  hX/   Free Action. Set target's move to 0 for 1 turn.r	  j�  h�j�  �j�  }r	  (j7  K j8  Kuj�  ]r	  j�  ]r	  (j�  N�r	  j�  K�r	  j�  K�r	  j?  X   Trippedr	  �r	  j'  N�r	  j7  K�r	  eu}r	  (j�  Mhj3  hj4  hj5  j�  h�j�  �j�  }r 	  (j7  K j8  Kuj�  ]r!	  j�  ]r"	  (j;  N�r#	  j�  K�r$	  j�  K�r%	  j/  j?  �r&	  j'  N�r'	  j7  K�r(	  jC  K�r)	  eu}r*	  (j�  M hj3  hj4  hj5  j�  hoj�  �j�  }r+	  (j7  K j8  Kuj�  ]r,	  j�  ]r-	  (j;  N�r.	  j�  K�r/	  j�  K�r0	  j/  j?  �r1	  j'  N�r2	  j7  K�r3	  jC  K�r4	  eu}r5	  (j�  M!hj3  hj4  hj5  j�  hj�  �j�  }r6	  (j7  K j8  Kuj�  ]r7	  j�  ]r8	  (j;  N�r9	  j�  K�r:	  j�  K�r;	  j/  j?  �r<	  j'  N�r=	  j7  K�r>	  jC  K�r?	  euehE]r@	  (}rA	  (j�  M?hX   HeartofFirerB	  j�  hj�  }rC	  X   initiator_nidrD	  NX   subskillrE	  Nu}rF	  (j�  MDhX   HeartofGalesrG	  j�  hoj�  }rH	  jD	  NjE	  Nu}rI	  (j�  MIhX   HeartofLightningrJ	  j�  h�j�  }rK	  jD	  NjE	  Nu}rL	  (j�  MNhX   HeartofFrostrM	  j�  h�j�  }rN	  jD	  NjE	  Nu}rO	  (j�  MXhX   HeartofRadiancerP	  j�  h�j�  }rQ	  jD	  NjE	  Nu}rR	  (j�  M^hX   HeartofDarknessrS	  j�  h�j�  }rT	  jD	  NjE	  Nu}rU	  (j�  MnhX   SPD1rV	  j�  j8  j�  }rW	  jD	  NjE	  Nu}rX	  (j�  MshX   SPD2rY	  j�  jV  j�  }rZ	  jD	  NjE	  Nu}r[	  (j�  MyhjY	  j�  jx  j�  }r\	  jD	  NjE	  Nu}r]	  (j�  MhjV	  j�  j�  j�  }r^	  jD	  NjE	  Nu}r_	  (j�  M�hjY	  j�  j9  j�  }r`	  jD	  NjE	  Nu}ra	  (j�  M�hX   SPD3rb	  j�  jN  j�  }rc	  jD	  NjE	  Nu}rd	  (j�  M�hjV	  j�  jw  j�  }re	  jD	  NjE	  Nu}rf	  (j�  M�hX   Wardenrg	  j�  j�  j�  }rh	  jD	  NjE	  Nu}ri	  (j�  M�hX   DistantCounterrj	  j�  j�  j�  }rk	  jD	  NjE	  Nu}rl	  (j�  M�hX   CloseCounterrm	  j�  j�  j�  }rn	  jD	  NjE	  Nu}ro	  (j�  M@hX   NoEXPrp	  j�  hj�  }rq	  jD	  NjE	  Nu}rr	  (j�  MAhX   Lootrs	  j�  hj�  }rt	  jD	  NjE	  Nu}ru	  (j�  MBhX   Shoverv	  j�  hj�  }rw	  X   ability_item_uidrx	  M!sjD	  NjE	  Nu}ry	  (j�  MChX   Flailrz	  j�  hj�  }r{	  jD	  NjE	  Nu}r|	  (j�  MEhjp	  j�  hoj�  }r}	  jD	  NjE	  Nu}r~	  (j�  MFhjs	  j�  hoj�  }r	  jD	  NjE	  Nu}r�	  (j�  MGhjv	  j�  hoj�  }r�	  jx	  M sjD	  NjE	  Nu}r�	  (j�  MHhX   RiskyBusinessr�	  j�  hoj�  }r�	  jD	  NjE	  Nu}r�	  (j�  MJhjp	  j�  h�j�  }r�	  jD	  NjE	  Nu}r�	  (j�  MKhjs	  j�  h�j�  }r�	  jD	  NjE	  Nu}r�	  (j�  MLhjv	  j�  h�j�  }r�	  jx	  MsjD	  NjE	  Nu}r�	  (j�  MMhX   Bungler�	  j�  h�j�  }r�	  jD	  NjE	  Nu}r�	  (j�  MOhjp	  j�  h�j�  }r�	  jD	  NjE	  Nu}r�	  (j�  MPhjs	  j�  h�j�  }r�	  jD	  NjE	  Nu}r�	  (j�  MQhjv	  j�  h�j�  }r�	  jx	  MsjD	  NjE	  Nu}r�	  (j�  MRhX   Tripr�	  j�  h�j�  }r�	  jx	  MsjD	  NjE	  Nu}r�	  (j�  MShjp	  j�  h�j�  }r�	  jD	  NjE	  Nu}r�	  (j�  MThjs	  j�  h�j�  }r�	  jD	  NjE	  Nu}r�	  (j�  MUhX	   Protectorr�	  j�  h�j�  }r�	  jD	  NjE	  Nu}r�	  (j�  MVhjv	  j�  h�j�  }r�	  jx	  MsjD	  NjE	  Nu}r�	  (j�  MWhX   ElwynnSkillr�	  j�  h�j�  }r�	  jx	  MsjD	  NjE	  Nu}r�	  (j�  MYhjp	  j�  h�j�  }r�	  jD	  NjE	  Nu}r�	  (j�  MZhjs	  j�  h�j�  }r�	  jD	  NjE	  Nu}r�	  (j�  M[hj�	  j�  h�j�  }r�	  jD	  NjE	  Nu}r�	  (j�  M\hjv	  j�  h�j�  }r�	  jx	  MsjD	  NjE	  Nu}r�	  (j�  M]hX   Potshotr�	  j�  h�j�  }r�	  jD	  NjE	  Nu}r�	  (j�  M_hjp	  j�  h�j�  }r�	  jD	  NjE	  Nu}r�	  (j�  M`hjs	  j�  h�j�  }r�	  jD	  NjE	  Nu}r�	  (j�  Mahj�	  j�  h�j�  }r�	  jD	  NjE	  Nu}r�	  (j�  Mbhjv	  j�  h�j�  }r�	  jx	  MsjD	  NjE	  Nu}r�	  (j�  MchX   Enthrallr�	  j�  h�j�  }r�	  jD	  NjE	  Nu}r�	  (j�  Mdhjp	  j�  j  j�  }r�	  jD	  NjE	  Nu}r�	  (j�  Mehjs	  j�  j  j�  }r�	  jD	  NjE	  Nu}r�	  (j�  Mfhjv	  j�  j  j�  }r�	  jx	  MsjD	  NjE	  Nu}r�	  (j�  MghX   DivineConduitr�	  j�  j  j�  }r�	  jx	  MsjD	  NjE	  Nu}r�	  (j�  MhhX   Exchanger�	  j�  j  j�  }r�	  (X   charger�	  KX   total_charger�	  Kjx	  MujD	  NjE	  Nu}r�	  (j�  Mihjp	  j�  j8  j�  }r�	  jD	  NjE	  Nu}r�	  (j�  Mjhjs	  j�  j8  j�  }r�	  jD	  NjE	  Nu}r�	  (j�  Mkhj�	  j�  j8  j�  }r�	  jD	  NjE	  Nu}r�	  (j�  Mlhjv	  j�  j8  j�  }r�	  jx	  MsjD	  NjE	  Nu}r�	  (j�  MmhX
   Instructorr�	  j�  j8  j�  }r�	  jD	  NjE	  Nu}r�	  (j�  Mohjp	  j�  jV  j�  }r�	  jD	  NjE	  Nu}r�	  (j�  Mphjs	  j�  jV  j�  }r�	  jD	  NjE	  Nu}r�	  (j�  Mqhj�	  j�  jV  j�  }r�	  jD	  NjE	  Nu}r�	  (j�  Mrhjv	  j�  jV  j�  }r�	  jx	  MsjD	  NjE	  Nu}r�	  (j�  Mthjp	  j�  jx  j�  }r�	  jD	  NjE	  Nu}r�	  (j�  Muhjs	  j�  jx  j�  }r�	  jD	  NjE	  Nu}r�	  (j�  Mvhj�	  j�  jx  j�  }r�	  jD	  NjE	  Nu}r�	  (j�  Mwhjv	  j�  jx  j�  }r�	  jx	  MsjD	  NjE	  Nu}r�	  (j�  MxhX   Nemophilistr�	  j�  jx  j�  }r�	  jD	  NjE	  Nu}r�	  (j�  Mzhjp	  j�  j�  j�  }r�	  jD	  NjE	  Nu}r�	  (j�  M{hjs	  j�  j�  j�  }r�	  jD	  NjE	  Nu}r�	  (j�  M|hj�	  j�  j�  j�  }r�	  jD	  NjE	  Nu}r�	  (j�  M}hjv	  j�  j�  j�  }r�	  jx	  MsjD	  NjE	  Nu}r�	  (j�  M~hX   Beachcomberr�	  j�  j�  j�  }r�	  jD	  NjE	  Nu}r�	  (j�  M�hjp	  j�  j�  j�  }r�	  jD	  NjE	  Nu}r�	  (j�  M�hjs	  j�  j�  j�  }r�	  jD	  NjE	  Nu}r�	  (j�  M�hj�	  j�  j�  j�  }r�	  jD	  NjE	  Nu}r�	  (j�  M�hjv	  j�  j�  j�  }r�	  jx	  MsjD	  NjE	  Nu}r�	  (j�  M�hX   Bolsterr�	  j�  j�  j�  }r�	  jx	  MsjD	  NjE	  Nu}r�	  (j�  M�hX   Repairr�	  j�  j�  j�  }r�	  jx	  MsjD	  NjE	  Nu}r�	  (j�  M�hjp	  j�  j�  j�  }r�	  jD	  NjE	  Nu}r 
  (j�  M�hjs	  j�  j�  j�  }r
  jD	  NjE	  Nu}r
  (j�  M�hj�	  j�  j�  j�  }r
  jD	  NjE	  Nu}r
  (j�  M�hX   AspiringLeaderr
  j�  j�  j�  }r
  jD	  NjE	  Nu}r
  (j�  M�hX   Cantor
  j�  j�  j�  }r	
  jD	  NjE	  Nu}r

  (j�  M�hjp	  j�  j�  j�  }r
  jD	  NjE	  Nu}r
  (j�  M�hjs	  j�  j�  j�  }r
  jD	  NjE	  Nu}r
  (j�  M�hX   Anticritr
  j�  j�  j�  }r
  jD	  NjE	  Nu}r
  (j�  M�hX   DivineBlessingr
  j�  j�  j�  }r
  jD	  NjE	  Nu}r
  (j�  M�hjp	  j�  j  j�  }r
  jD	  NjE	  Nu}r
  (j�  M�hjs	  j�  j  j�  }r
  jD	  NjE	  Nu}r
  (j�  M�hX   Flyingr
  j�  j  j�  }r
  jD	  NjE	  Nu}r
  (j�  M�hjp	  j�  j#  j�  }r
  jD	  NjE	  Nu}r
  (j�  M�hjs	  j�  j#  j�  }r
  jD	  NjE	  Nu}r
  (j�  M�hjp	  j�  j9  j�  }r 
  jD	  NjE	  Nu}r!
  (j�  M�hjs	  j�  j9  j�  }r"
  jD	  NjE	  Nu}r#
  (j�  M�hj
  j�  j9  j�  }r$
  jD	  NjE	  Nu}r%
  (j�  M�hjp	  j�  jN  j�  }r&
  jD	  NjE	  Nu}r'
  (j�  M�hjs	  j�  jN  j�  }r(
  jD	  NjE	  Nu}r)
  (j�  M�hjp	  j�  jb  j�  }r*
  jD	  NjE	  Nu}r+
  (j�  M�hjs	  j�  jb  j�  }r,
  jD	  NjE	  Nu}r-
  (j�  M�hjp	  j�  jw  j�  }r.
  jD	  NjE	  Nu}r/
  (j�  M�hjs	  j�  jw  j�  }r0
  jD	  NjE	  Nu}r1
  (j�  M�hjp	  j�  j�  j�  }r2
  jD	  NjE	  Nu}r3
  (j�  M�hjs	  j�  j�  j�  }r4
  jD	  NjE	  Nu}r5
  (j�  M�hj
  j�  j�  j�  }r6
  jD	  NjE	  Nu}r7
  (j�  M�hj
  j�  j�  j�  }r8
  jD	  NjE	  Nu}r9
  (j�  M�hjp	  j�  j�  j�  }r:
  jD	  NjE	  Nu}r;
  (j�  M�hjs	  j�  j�  j�  }r<
  jD	  NjE	  Nu}r=
  (j�  M�hj
  j�  j�  j�  }r>
  jD	  NjE	  Nu}r?
  (j�  M�hjp	  j�  j�  j�  }r@
  jD	  NjE	  Nu}rA
  (j�  M�hjs	  j�  j�  j�  }rB
  jD	  NjE	  Nu}rC
  (j�  M�hj
  j�  j�  j�  }rD
  jD	  NjE	  Nu}rE
  (j�  M�hX   Avoid10rF
  j�  hj�  }rG
  jD	  NjE	  Nu}rH
  (j�  M�hX   Avoid5rI
  j�  hoj�  }rJ
  jD	  NjE	  Nu}rK
  (j�  M�hjI
  j�  h�j�  }rL
  jD	  NjE	  Nu}rM
  (j�  M�hjI
  j�  h�j�  }rN
  jD	  NjE	  Nu}rO
  (j�  M�hjI
  j�  h�j�  }rP
  jD	  NjE	  Nu}rQ
  (j�  M�hjI
  j�  h�j�  }rR
  jD	  NjE	  Nu}rS
  (j�  M�hjF
  j�  h�j�  }rT
  jD	  NjE	  Nu}rU
  (j�  M�hjI
  j�  j  j�  }rV
  jD	  NjE	  Nu}rW
  (j�  M�hjI
  j�  j8  j�  }rX
  jD	  NjE	  Nu}rY
  (j�  M�hjI
  j�  jV  j�  }rZ
  jD	  NjE	  Nu}r[
  (j�  M�hjI
  j�  jx  j�  }r\
  jD	  NjE	  Nu}r]
  (j�  M�hjI
  j�  j�  j�  }r^
  jD	  NjE	  Nu}r_
  (j�  M�hjI
  j�  j�  j�  }r`
  jD	  NjE	  Nu}ra
  (j�  M�hjI
  j�  j�  j�  }rb
  jD	  NjE	  Nu}rc
  (j�  M�hjF
  j�  j�  j�  }rd
  jD	  NjE	  Nu}re
  (j�  M�hjI
  j�  jb  j�  }rf
  jD	  NjE	  Nu}rg
  (j�  M�hjI
  j�  jw  j�  }rh
  jD	  NjE	  Nu}ri
  (j�  M�hjI
  j�  j�  j�  }rj
  jD	  NjE	  Nu}rk
  (j�  M�hX   SuppressiveFirerl
  j�  jV  j�  }rm
  jD	  NjE	  Nu}rn
  (j�  M�hX   Burningro
  j�  jw  j�  }rp
  jD	  NjE	  NueX   terrain_status_registryrq
  }rr
  (KKX   Avoid10rs
  �rt
  M�KKX   Avoid5ru
  �rv
  M�KKju
  �rw
  M�KKju
  �rx
  M�KK	ju
  �ry
  M�KKju
  �rz
  M�KKjs
  �r{
  M�KKju
  �r|
  M�KK
ju
  �r}
  M�KK	ju
  �r~
  M�K	Kju
  �r
  M�KKju
  �r�
  M�KKju
  �r�
  M�KK	ju
  �r�
  M�KKjs
  �r�
  M�KKju
  �r�
  M�KKju
  �r�
  M�KKju
  �r�
  M�KKX   Burningr�
  �r�
  M�uX   regionsr�
  ]r�
  (}r�
  (hX   Door1r�
  X   region_typer�
  capp.events.regions
RegionType
r�
  X   eventr�
  �r�
  Rr�
  hKK�r�
  X   sizer�
  ]r�
  (KKeX   sub_nidr�
  X   Doorr�
  X	   conditionr�
  X   unit.can_unlock(region)r�
  X	   time_leftr�
  NX	   only_oncer�
  �X   interrupt_mover�
  �j�  }r�
  u}r�
  (hj�  j�
  j�
  X   statusr�
  �r�
  Rr�
  hjx  j�
  KK�r�
  j�
  j�
  j�
  X   Truer�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_0r�
  j�
  j�
  hKK	�r�
  j�
  KK�r�
  j�
  X   Garvey_Shotr�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_1r�
  j�
  j�
  hKK	�r�
  j�
  KK�r�
  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_2r�
  j�
  j�
  hKK
�r�
  j�
  KK�r�
  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_3r�
  j�
  j�
  hKK	�r�
  j�
  KK�r�
  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_4r�
  j�
  j�
  hKK
�r�
  j�
  KK�r�
  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_5r�
  j�
  j�
  hKK�r�
  j�
  KK�r�
  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_6r�
  j�
  j�
  hKK�r�
  j�
  KK�r�
  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_7r�
  j�
  j�
  hKK�r�
  j�
  KK�r�
  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_8r�
  j�
  j�
  hKK�r�
  j�
  KK�r�
  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_9r�
  j�
  j�
  hKK�r�
  j�
  KK�r�
  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_10r�
  j�
  j�
  hKK	�r�
  j�
  KK�r�
  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_11r�
  j�
  j�
  hKK
�r�
  j�
  KK�r�
  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_12r�
  j�
  j�
  hKK�r�
  j�
  KK�r�
  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_13r�
  j�
  j�
  hKK	�r�
  j�
  KK�r�
  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_14r�
  j�
  j�
  hKK�r�
  j�
  KK�r�
  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r�
  u}r�
  (hX   Garve_15r�
  j�
  j�
  hKK�r�
  j�
  KK�r�
  j�
  j�
  j�
  j�
  j�
  Nj�
  �j�
  �j�  }r�
  ueh}r�
  (hX   DEBUGr�
  hX   DEBUGr�
  X   tilemapr�
  }r�
  (hX   Ch5r�
  X   layersr�
  ]r�
  (}r�
  (hX   baser�
  X   visibler�
  �u}r   (hX   Leftr  j�
  �u}r  (hX   Rightr  j�
  �ueX   weatherr  ]r  X
   animationsr  ]r  }r  (hX
   Target_3x3r	  X   posr
  G@      G@"      �r  X   loopr  �X   holdr  �X   reverser  �X	   speed_adjr  KX   tintr  K X
   contingentr  �uauX
   bg_tilemapr  NhhX   musicr  }r  (X   player_phaser  X   BattleFateMainr  X   enemy_phaser  NX   other_phaser  NX   enemy2_phaser  NX   player_battler  X   Attackr  X   enemy_battler  NX   other_battler  NX   enemy2_battler  NuX	   objectiver  }r   (X   simpler!  X   DEBUGr"  X   winr#  X   Don't Crashr$  X   lossr%  X   Crashr&  uh]r'  (hhoh�h�h�h�h�j  j8  jV  jx  j�  j�  j�  j�  j  j#  j9  jN  jb  jw  j�  j�  j�  ej�
  ]r(  (j�
  j�  j�
  j�
  j�
  j�
  j�
  j�
  j�
  j�
  j�
  j�
  j�
  j�
  j�
  j�
  j�
  j�
  eX   unit_groupsr)  ]r*  X	   ai_groupsr+  ]r,  uX
   overworldsr-  ]r.  }r/  (j�
  NX   enabled_nodesr0  ]r1  X   enabled_roadsr2  ]r3  hX   0r4  X   overworld_entitiesr5  ]r6  }r7  (hhX   dtyper8  X   PARTYr9  X   dnidr:  hX   on_node_nidr;  NhNh	X   playerr<  uaX   selected_party_nidr=  NX   node_propertiesr>  }r?  X   enabled_menu_optionsr@  }rA  (j4  }rB  X   1rC  }rD  X   2rE  }rF  X   3rG  }rH  X   4rI  }rJ  X   5rK  }rL  X   6rM  }rN  X   7rO  }rP  X   8rQ  }rR  X   9rS  }rT  X   10rU  }rV  X   11rW  }rX  X   12rY  }rZ  X   13r[  }r\  X   14r]  }r^  X   15r_  }r`  X   16ra  }rb  X   17rc  }rd  X   18re  }rf  X   19rg  }rh  X   20ri  }rj  X   21rk  }rl  X   22rm  }rn  X   23ro  }rp  X   24rq  }rr  X   25rs  }rt  uX   visible_menu_optionsru  }rv  (j4  }rw  jC  }rx  jE  }ry  jG  }rz  jI  }r{  jK  }r|  jM  }r}  jO  }r~  jQ  }r  jS  }r�  jU  }r�  jW  }r�  jY  }r�  j[  }r�  j]  }r�  j_  }r�  ja  }r�  jc  }r�  je  }r�  jg  }r�  ji  }r�  jk  }r�  jm  }r�  jo  }r�  jq  }r�  js  }r�  uuaX	   turncountr�  KX   playtimer�  M�iX	   game_varsr�  ccollections
Counter
r�  }r�  (X   _random_seedr�  MX   _chapter_testr�  �X   _convoyr�  �X   _custom_options_disabledr�  ]r�  �aX   _custom_info_descr�  ]r�  X   Open the Codexr�  aX   _custom_options_eventsr�  ]r�  X
   Menu_Codexr�  aX   _custom_additional_optionsr�  ]r�  X   Codexr�  aX   _base_bg_namer�  X   Hillr�  X   _base_musicr�  X    Before An Impossible Battlefieldr�  X   _base_options_disabledr�  ]r�  X   _base_options_eventsr�  ]r�  X   _base_additional_optionsr�  ]r�  X   _base_transparentr�  �u�r�  Rr�  X
   level_varsr�  j�  }r�  (X
   FireStart1r�  KX   FireOrigin1r�  jx  X   FireComplete1r�  K X
   FireStart2r�  K X   FireOrigin2r�  K X   FireComplete2r�  K X
   FireStart3r�  K X   FireOrigin3r�  K X   FireComplete3r�  K X
   FireStart4r�  K X   FireOrigin4r�  K X   FireComplete4r�  K X
   FireStart5r�  K X   FireOrigin5r�  K X   FireComplete5r�  K X
   FireStart6r�  K X   FireOrigin6r�  K X   FireComplete6r�  K X
   FireStart7r�  K X   FireOrigin7r�  K X   FireComplete7r�  K X
   FireStart8r�  K X   FireOrigin8r�  K X   FireComplete8r�  K X
   FireStart9r�  K X   FireOrigin9r�  K X   FireComplete9r�  K X   FireStart10r�  K X   FireOrigin10r�  K X   FireComplete10r�  K X   FireStart11r�  K X   FireOrigin11r�  K X   FireComplete11r�  K X   FireStart12r�  K X   FireOrigin12r�  K X   FireComplete12r�  K X   SaraidSkillr�  K X   QuinleySkillr�  K X   RoutEnemiesr�  K X   ValidFire1Locsr�  ]r�  (KK�r�  KK�r�  KK�r�  KK�r�  eX   GarveySpotsr�  KX	   ValidLocsr�  ]r�  (KK�r�  KK	�r�  KK	�r�  KK
�r�  KK�r�  KK�r�  KK�r�  KK	�r�  KK	�r�  KK
�r�  KK	�r�  KK
�r�  KK�r�  KK�r�  KK�r�  KK�r�  KK�r�  KK�r�  KK	�r�  KK
�r�  KK�r�  KK	�r�  KK
�r�  KK�r�  KK�r�  eX   GarvPositionr�  jW  u�r�  Rr�  X   current_moder�  }r   (hX   Normalr  X
   permadeathr  �h/X   Randomr  X   enemy_autolevelsr  K X   enemy_truelevelsr  K X   boss_autolevelsr  K X   boss_truelevelsr  K uX   partiesr  ]r	  }r
  (hhhX   Eirika's Groupr  X
   leader_nidr  X   Alpinr  X   party_prep_manage_sort_orderr  ]r  X   moneyr  K X   convoyr  ]r  X   bexpr  K uaX   current_partyr  hX   stater  ]r  (hX   status_upkeepr  X   phase_changer  e]r  �r  X
   action_logr  ]r  (X   AddSkillr  }r  (X   unitr  j  h�r   X	   initiatorr!  hN�r"  X	   skill_objr#  X   skillr$  M?�r%  X   sourcer&  hKՆr'  X   source_typer(  hhK�r)  X
   subactionsr*  X   listr+  ]r,  �r-  X   reset_actionr.  X   actionr/  X   ResetUnitVarsr0  }r1  (j  j  h�r2  X   old_current_hpr3  hK�r4  X   old_current_manar5  hK �r6  u�r7  �r8  X   did_somethingr9  h��r:  u�r;  j  }r<  (j  j  ho�r=  j!  hN�r>  j#  j$  MD�r?  j&  hKنr@  j(  hhK�rA  j*  j+  ]rB  �rC  j.  j/  j0  }rD  (j  j  ho�rE  j3  hK
�rF  j5  hK �rG  u�rH  �rI  j9  h��rJ  u�rK  j  }rL  (j  j  h��rM  j!  hN�rN  j#  j$  MI�rO  j&  hKۆrP  j(  hhK�rQ  j*  j+  ]rR  �rS  j.  j/  j0  }rT  (j  j  h��rU  j3  hK�rV  j5  hK �rW  u�rX  �rY  j9  h��rZ  u�r[  j  }r\  (j  j  h��r]  j!  hN�r^  j#  j$  MN�r_  j&  hK߆r`  j(  hhK�ra  j*  j+  ]rb  �rc  j.  j/  j0  }rd  (j  j  h��re  j3  hK�rf  j5  hK �rg  u�rh  �ri  j9  h��rj  u�rk  j  }rl  (j  j  h߆rm  j!  hN�rn  j#  j$  MX�ro  j&  hK�rp  j(  hhK�rq  j*  j+  ]rr  �rs  j.  j/  j0  }rt  (j  j  h߆ru  j3  hK�rv  j5  hK �rw  u�rx  �ry  j9  h��rz  u�r{  j  }r|  (j  j  h��r}  j!  hN�r~  j#  j$  M^�r  j&  hK�r�  j(  hhK�r�  j*  j+  ]r�  �r�  j.  j/  j0  }r�  (j  j  h��r�  j3  hK�r�  j5  hK �r�  u�r�  �r�  j9  h��r�  u�r�  j  }r�  (j  j  j8  �r�  j!  hN�r�  j#  j$  Mn�r�  j&  hK�r�  j(  hhK�r�  j*  j+  ]r�  �r�  j.  j/  j0  }r�  (j  j  j8  �r�  j3  hK�r�  j5  hK �r�  u�r�  �r�  j9  h��r�  u�r�  j  }r�  (j  j  jV  �r�  j!  hN�r�  j#  j$  Ms�r�  j&  hK�r�  j(  hhK�r�  j*  j+  ]r�  �r�  j.  j/  j0  }r�  (j  j  jV  �r�  j3  hK�r�  j5  hK �r�  u�r�  �r�  j9  h��r�  u�r�  j  }r�  (j  j  jx  �r�  j!  hN�r�  j#  j$  My�r�  j&  hK�r�  j(  hhK�r�  j*  j+  ]r�  �r�  j.  j/  j0  }r�  (j  j  jx  �r�  j3  hK�r�  j5  hK �r�  u�r�  �r�  j9  h��r�  u�r�  j  }r�  (j  j  j�  �r�  j!  hN�r�  j#  j$  M�r�  j&  hK�r�  j(  hhK�r�  j*  j+  ]r�  �r�  j.  j/  j0  }r�  (j  j  j�  �r�  j3  hK�r�  j5  hK �r�  u�r�  �r�  j9  h��r�  u�r�  j  }r�  (j  j  j9  �r�  j!  hN�r�  j#  j$  M��r�  j&  hK��r�  j(  hhK�r�  j*  j+  ]r�  �r�  j.  j/  j0  }r�  (j  j  j9  �r�  j3  hK	�r�  j5  hK �r�  u�r�  �r�  j9  h��r�  u�r�  j  }r�  (j  j  jN  �r�  j!  hN�r�  j#  j$  M��r�  j&  hK��r�  j(  hhK�r�  j*  j+  ]r�  �r�  j.  j/  j0  }r�  (j  j  jN  �r�  j3  hK�r�  j5  hK �r�  u�r�  �r�  j9  h��r�  u�r�  j  }r�  (j  j  jw  �r�  j!  hN�r�  j#  j$  M��r�  j&  hM�r�  j(  hhK�r�  j*  j+  ]r�  �r�  j.  j/  j0  }r�  (j  j  jw  �r�  j3  hK	�r�  j5  hK �r�  u�r�  �r�  j9  h��r�  u�r�  j  }r�  (j  j  j�  �r�  j!  hN�r�  j#  j$  M��r�  j&  hM�r   j(  hhK�r  j*  j+  ]r  �r  j.  j/  j0  }r  (j  j  j�  �r  j3  hK�r  j5  hK �r  u�r  �r	  j9  h��r
  u�r  j  }r  (j  j  j�  �r  j!  hN�r  j#  j$  M��r  j&  hM�r  j(  hhK�r  j*  j+  ]r  �r  j.  j/  j0  }r  (j  j  j�  �r  j3  hK�r  j5  hK �r  u�r  �r  j9  h��r  u�r  j  }r  (j  j  j�  �r  j!  hN�r  j#  j$  M��r  j&  hM�r   j(  hhK�r!  j*  j+  ]r"  �r#  j.  j/  j0  }r$  (j  j  j�  �r%  j3  hK�r&  j5  hK �r'  u�r(  �r)  j9  h��r*  u�r+  j  }r,  (j  j  h�r-  j!  hN�r.  j#  j$  M��r/  j&  hh�r0  j(  hh]�r1  j*  j+  ]r2  �r3  j.  j/  j0  }r4  (j  j  h�r5  j3  hK�r6  j5  hK �r7  u�r8  �r9  j9  h��r:  u�r;  j  }r<  (j  j  ho�r=  j!  hN�r>  j#  j$  M��r?  j&  hhp�r@  j(  hh]�rA  j*  j+  ]rB  �rC  j.  j/  j0  }rD  (j  j  ho�rE  j3  hK
�rF  j5  hK �rG  u�rH  �rI  j9  h��rJ  u�rK  j  }rL  (j  j  h��rM  j!  hN�rN  j#  j$  M��rO  j&  hh��rP  j(  hh]�rQ  j*  j+  ]rR  �rS  j.  j/  j0  }rT  (j  j  h��rU  j3  hK�rV  j5  hK �rW  u�rX  �rY  j9  h��rZ  u�r[  j  }r\  (j  j  h��r]  j!  hN�r^  j#  j$  M��r_  j&  hh��r`  j(  hh]�ra  j*  j+  ]rb  �rc  j.  j/  j0  }rd  (j  j  h��re  j3  hK�rf  j5  hK �rg  u�rh  �ri  j9  h��rj  u�rk  j  }rl  (j  j  hÆrm  j!  hN�rn  j#  j$  M��ro  j&  hhĆrp  j(  hh]�rq  j*  j+  ]rr  �rs  j.  j/  j0  }rt  (j  j  hÆru  j3  hK�rv  j5  hK �rw  u�rx  �ry  j9  h��rz  u�r{  j  }r|  (j  j  h߆r}  j!  hN�r~  j#  j$  M��r  j&  hh�r�  j(  hh]�r�  j*  j+  ]r�  �r�  j.  j/  j0  }r�  (j  j  h߆r�  j3  hK�r�  j5  hK �r�  u�r�  �r�  j9  h��r�  u�r�  j  }r�  (j  j  h��r�  j!  hN�r�  j#  j$  M��r�  j&  hh��r�  j(  hh]�r�  j*  j+  ]r�  �r�  j.  j/  j0  }r�  (j  j  h��r�  j3  hK�r�  j5  hK �r�  u�r�  �r�  j9  h��r�  u�r�  j  }r�  (j  j  j  �r�  j!  hN�r�  j#  j$  M��r�  j&  hj  �r�  j(  hh]�r�  j*  j+  ]r�  �r�  j.  j/  j0  }r�  (j  j  j  �r�  j3  hK�r�  j5  hK �r�  u�r�  �r�  j9  h��r�  u�r�  j  }r�  (j  j  j8  �r�  j!  hN�r�  j#  j$  M��r�  j&  hj9  �r�  j(  hh]�r�  j*  j+  ]r�  �r�  j.  j/  j0  }r�  (j  j  j8  �r�  j3  hK�r�  j5  hK �r�  u�r�  �r�  j9  h��r�  u�r�  j  }r�  (j  j  jV  �r�  j!  hN�r�  j#  j$  M��r�  j&  hjW  �r�  j(  hh]�r�  j*  j+  ]r�  �r�  j.  j/  j0  }r�  (j  j  jV  �r�  j3  hK�r�  j5  hK �r�  u�r�  �r�  j9  h��r�  u�r�  j  }r�  (j  j  jx  �r�  j!  hN�r�  j#  j$  M��r�  j&  hjy  �r�  j(  hh]�r�  j*  j+  ]r�  �r�  j.  j/  j0  }r�  (j  j  jx  �r�  j3  hK�r�  j5  hK �r�  u�r�  �r�  j9  h��r�  u�r�  j  }r�  (j  j  j�  �r�  j!  hN�r�  j#  j$  M��r�  j&  hj�  �r�  j(  hh]�r�  j*  j+  ]r�  �r�  j.  j/  j0  }r�  (j  j  j�  �r�  j3  hK�r�  j5  hK �r�  u�r�  �r�  j9  h��r�  u�r�  j  }r�  (j  j  j�  �r�  j!  hN�r�  j#  j$  M��r�  j&  hj�  �r�  j(  hh]�r�  j*  j+  ]r�  �r�  j.  j/  j0  }r�  (j  j  j�  �r�  j3  hK�r�  j5  hK �r�  u�r�  �r�  j9  h��r�  u�r�  j  }r�  (j  j  j�  �r�  j!  hN�r�  j#  j$  M��r�  j&  hj�  �r   j(  hh]�r  j*  j+  ]r  �r  j.  j/  j0  }r  (j  j  j�  �r  j3  hK�r  j5  hK �r  u�r  �r	  j9  h��r
  u�r  j  }r  (j  j  j�  �r  j!  hN�r  j#  j$  M��r  j&  hj�  �r  j(  hh]�r  j*  j+  ]r  �r  j.  j/  j0  }r  (j  j  j�  �r  j3  hK�r  j5  hK �r  u�r  �r  j9  h��r  u�r  j  }r  (j  j  jb  �r  j!  hN�r  j#  j$  M��r  j&  hjc  �r   j(  hh]�r!  j*  j+  ]r"  �r#  j.  j/  j0  }r$  (j  j  jb  �r%  j3  hK�r&  j5  hK �r'  u�r(  �r)  j9  h��r*  u�r+  j  }r,  (j  j  jw  �r-  j!  hN�r.  j#  j$  M��r/  j&  hjx  �r0  j(  hh]�r1  j*  j+  ]r2  �r3  j.  j/  j0  }r4  (j  j  jw  �r5  j3  hK	�r6  j5  hK �r7  u�r8  �r9  j9  h��r:  u�r;  j  }r<  (j  j  j�  �r=  j!  hN�r>  j#  j$  M��r?  j&  hj�  �r@  j(  hh]�rA  j*  j+  ]rB  �rC  j.  j/  j0  }rD  (j  j  j�  �rE  j3  hK�rF  j5  hK �rG  u�rH  �rI  j9  h��rJ  u�rK  X   IncrementTurnrL  }rM  �rN  X   UpdateRecordsrO  }rP  (X   record_typerQ  hX   turnrR  �rS  j�  hN�rT  u�rU  X   ChangeFatiguerV  }rW  (j  j  h�rX  X   numrY  hK �rZ  X   old_fatiguer[  hK �r\  j*  j+  ]r]  �r^  u�r_  jV  }r`  (j  j  ho�ra  jY  hK �rb  j[  hK �rc  j*  j+  ]rd  �re  u�rf  jV  }rg  (j  j  h��rh  jY  hK �ri  j[  hK �rj  j*  j+  ]rk  �rl  u�rm  jV  }rn  (j  j  h��ro  jY  hK �rp  j[  hK �rq  j*  j+  ]rr  �rs  u�rt  jV  }ru  (j  j  hÆrv  jY  hK �rw  j[  hK �rx  j*  j+  ]ry  �rz  u�r{  jV  }r|  (j  j  h߆r}  jY  hK �r~  j[  hK �r  j*  j+  ]r�  �r�  u�r�  jV  }r�  (j  j  h��r�  jY  hK �r�  j[  hK �r�  j*  j+  ]r�  �r�  u�r�  jV  }r�  (j  j  j  �r�  jY  hK �r�  j[  hK �r�  j*  j+  ]r�  �r�  u�r�  jV  }r�  (j  j  j8  �r�  jY  hK �r�  j[  hK �r�  j*  j+  ]r�  �r�  u�r�  jV  }r�  (j  j  jV  �r�  jY  hK �r�  j[  hK �r�  j*  j+  ]r�  �r�  u�r�  jV  }r�  (j  j  jx  �r�  jY  hK �r�  j[  hK �r�  j*  j+  ]r�  �r�  u�r�  jV  }r�  (j  j  j�  �r�  jY  hK �r�  j[  hK �r�  j*  j+  ]r�  �r�  u�r�  jV  }r�  (j  j  j�  �r�  jY  hK �r�  j[  hK �r�  j*  j+  ]r�  �r�  u�r�  jV  }r�  (j  j  j�  �r�  jY  hK �r�  j[  hK �r�  j*  j+  ]r�  �r�  u�r�  X   SetLevelVarr�  }r�  (hhj�  �r�  X   valr�  hK �r�  X   old_valr�  hK �r�  u�r�  j�  }r�  (hhj�  �r�  j�  hK �r�  j�  hK �r�  u�r�  j�  }r�  (hhj�  �r�  j�  hK �r�  j�  hK �r�  u�r�  j�  }r�  (hhj�  �r�  j�  hK �r�  j�  hK �r�  u�r�  j�  }r�  (hhj�  �r�  j�  hK �r�  j�  hK �r�  u�r�  j�  }r�  (hhj�  �r�  j�  hK �r�  j�  hK �r�  u�r�  j�  }r�  (hhj�  �r�  j�  hK �r�  j�  hK �r�  u�r�  j�  }r�  (hhj�  �r�  j�  hK �r�  j�  hK �r�  u�r�  j�  }r�  (hhj�  �r�  j�  hK �r�  j�  hK �r�  u�r�  j�  }r�  (hhj�  �r�  j�  hK �r�  j�  hK �r�  u�r�  j�  }r�  (hhj�  �r�  j�  hK �r�  j�  hK �r�  u�r�  j�  }r�  (hhj�  �r�  j�  hK �r�  j�  hK �r�  u�r�  j�  }r�  (hhj�  �r�  j�  hK �r�  j�  hK �r�  u�r�  j�  }r�  (hhj�  �r   j�  hK �r  j�  hK �r  u�r  j�  }r  (hhj�  �r  j�  hK �r  j�  hK �r  u�r  j�  }r	  (hhj�  �r
  j�  hK �r  j�  hK �r  u�r  j�  }r  (hhj�  �r  j�  hK �r  j�  hK �r  u�r  j�  }r  (hhj�  �r  j�  hK �r  j�  hK �r  u�r  j�  }r  (hhj�  �r  j�  hK �r  j�  hK �r  u�r  j�  }r  (hhj�  �r  j�  hK �r  j�  hK �r   u�r!  j�  }r"  (hhj�  �r#  j�  hK �r$  j�  hK �r%  u�r&  j�  }r'  (hhj�  �r(  j�  hK �r)  j�  hK �r*  u�r+  j�  }r,  (hhj�  �r-  j�  hK �r.  j�  hK �r/  u�r0  j�  }r1  (hhj�  �r2  j�  hK �r3  j�  hK �r4  u�r5  j�  }r6  (hhj�  �r7  j�  hK �r8  j�  hK �r9  u�r:  j�  }r;  (hhj�  �r<  j�  hK �r=  j�  hK �r>  u�r?  j�  }r@  (hhj�  �rA  j�  hK �rB  j�  hK �rC  u�rD  j�  }rE  (hhj�  �rF  j�  hK �rG  j�  hK �rH  u�rI  j�  }rJ  (hhj�  �rK  j�  hK �rL  j�  hK �rM  u�rN  j�  }rO  (hhj�  �rP  j�  hK �rQ  j�  hK �rR  u�rS  j�  }rT  (hhj�  �rU  j�  hK �rV  j�  hK �rW  u�rX  j�  }rY  (hhj�  �rZ  j�  hK �r[  j�  hK �r\  u�r]  j�  }r^  (hhj�  �r_  j�  hK �r`  j�  hK �ra  u�rb  j�  }rc  (hhj�  �rd  j�  hK �re  j�  hK �rf  u�rg  j�  }rh  (hhj�  �ri  j�  hK �rj  j�  hK �rk  u�rl  j�  }rm  (hhj�  �rn  j�  hK �ro  j�  hK �rp  u�rq  X
   SetGameVarrr  }rs  (hhj�  �rt  j�  h��ru  j�  hK �rv  u�rw  X   SetHPrx  }ry  (j  j  h�rz  X   new_hpr{  hK�r|  X   old_hpr}  hK�r~  u�r  j  }r�  (j  j  jV  �r�  j!  hN�r�  j#  j$  M��r�  j&  hN�r�  j(  hjr  �r�  j*  j+  ]r�  �r�  j.  j/  j0  }r�  (j  j  jV  �r�  j3  hK�r�  j5  hK �r�  u�r�  �r�  j9  h��r�  u�r�  j�  }r�  (hhj�  �r�  j�  hK �r�  j�  hK �r�  u�r�  j�  }r�  (hhj�  �r�  j�  hK �r�  j�  hK �r�  u�r�  X   SetExpr�  }r�  (j  j  h�r�  X   old_expr�  hK �r�  X   exp_gainr�  hK_�r�  u�r�  X   ChangeAIr�  }r�  (j  j  j�  �r�  hhj�  �r�  X   old_air�  hX   AttackNoMover�  �r�  u�r�  j�  }r�  (hhj�  �r�  j�  hK �r�  j�  hK �r�  u�r�  X   SetDroppabler�  }r�  (hHhHM�r�  X   was_droppabler�  h��r�  X   valuer�  h��r�  u�r�  X   GiveItemr�  }r�  (j  j  hÆr�  hHhHM�r�  u�r�  X   AddLorer�  }r�  X   lore_nidr�  hX   WeaponTriangler�  �r�  s�r�  j�  }r�  j�  hX   Statsr�  �r�  s�r�  j�  }r�  j�  hX	   Equationsr�  �r�  s�r�  j�  }r�  j�  hX   WeaponRanksr�  �r�  s�r�  j�  }r�  j�  hX   Burningr�  �r�  s�r�  j�  }r�  j�  hX   FreeActionsr�  �r�  s�r�  jr  }r�  (hhj�  �r�  j�  j+  ]r�  h��r�  a�r�  j�  hK �r�  u�r�  jr  }r�  (hhj�  �r�  j�  j+  ]r�  hj�  �r�  a�r�  j�  hK �r�  u�r�  jr  }r�  (hhj�  �r�  j�  j+  ]r�  hj�  �r�  a�r�  j�  hK �r�  u�r�  jr  }r�  (hhj�  �r�  j�  j+  ]r�  hj�  �r�  a�r�  j�  hK �r�  u�r�  jr  }r�  (hhj�  �r�  j�  hj�  �r�  j�  hK �r�  u�r�  jr  }r�  (hhj�  �r�  j�  hj�  �r�  j�  hK �r�  u�r�  jr  }r�  (hhj�  �r�  j�  j+  ]r�  �r�  j�  hK �r   u�r  jr  }r  (hhj�  �r  j�  j+  ]r  �r  j�  hK �r  u�r  jr  }r  (hhj�  �r	  j�  j+  ]r
  �r  j�  hK �r  u�r  jr  }r  (hhj�  �r  j�  h��r  j�  hK �r  u�r  X   LockTurnwheelr  }r  X   lockr  h��r  s�r  X   ResetAllr  }r  X   actionsr  j+  ]r  (j/  X   Resetr  }r  (j  j  h�r  X   movement_leftr  hK�r   hhh(��������tr!  �r"  u�r#  �r$  j/  j  }r%  (j  j  ho�r&  j  hK�r'  hhh(��������tr(  �r)  u�r*  �r+  j/  j  }r,  (j  j  h��r-  j  hK�r.  hhh(��������tr/  �r0  u�r1  �r2  j/  j  }r3  (j  j  h��r4  j  hK�r5  hhh(��������tr6  �r7  u�r8  �r9  j/  j  }r:  (j  j  hÆr;  j  hK�r<  hhh(��������tr=  �r>  u�r?  �r@  j/  j  }rA  (j  j  h߆rB  j  hK�rC  hhh(��������trD  �rE  u�rF  �rG  j/  j  }rH  (j  j  h��rI  j  hK�rJ  hhh(��������trK  �rL  u�rM  �rN  j/  j  }rO  (j  j  j  �rP  j  hK�rQ  hhh(��������trR  �rS  u�rT  �rU  j/  j  }rV  (j  j  j8  �rW  j  hK�rX  hhh(��������trY  �rZ  u�r[  �r\  j/  j  }r]  (j  j  jV  �r^  j  hK�r_  hhh(��������tr`  �ra  u�rb  �rc  j/  j  }rd  (j  j  jx  �re  j  hK�rf  hhh(��������trg  �rh  u�ri  �rj  j/  j  }rk  (j  j  j�  �rl  j  hK�rm  hhh(��������trn  �ro  u�rp  �rq  j/  j  }rr  (j  j  j�  �rs  j  hK�rt  hhh(��������tru  �rv  u�rw  �rx  j/  j  }ry  (j  j  j�  �rz  j  hK�r{  hhh(��������tr|  �r}  u�r~  �r  j/  j  }r�  (j  j  j�  �r�  j  hK�r�  hhh(��������tr�  �r�  u�r�  �r�  j/  j  }r�  (j  j  j  �r�  j  hK�r�  hhh(��������tr�  �r�  u�r�  �r�  j/  j  }r�  (j  j  j#  �r�  j  hK �r�  hhh(��������tr�  �r�  u�r�  �r�  j/  j  }r�  (j  j  j9  �r�  j  hK�r�  hhh(��������tr�  �r�  u�r�  �r�  j/  j  }r�  (j  j  jN  �r�  j  hK�r�  hhh(��������tr�  �r�  u�r�  �r�  j/  j  }r�  (j  j  jb  �r�  j  hK�r�  hhh(��������tr�  �r�  u�r�  �r�  j/  j  }r�  (j  j  jw  �r�  j  hK�r�  hhh(��������tr�  �r�  u�r�  �r�  j/  j  }r�  (j  j  j�  �r�  j  hK�r�  hhh(��������tr�  �r�  u�r�  �r�  j/  j  }r�  (j  j  j�  �r�  j  hK�r�  hhh(��������tr�  �r�  u�r�  �r�  j/  j  }r�  (j  j  j�  �r�  j  hK�r�  hhh(��������tr�  �r�  u�r�  �r�  e�r�  s�r�  X	   MarkPhaser�  }r�  X
   phase_namer�  hX   playerr�  �r�  s�r�  X   Mover�  }r�  (j  j  h߆r�  X   old_posr�  hh�r�  X   new_posr�  hh��r�  X   prev_movement_leftr�  hK�r�  X   new_movement_leftr�  hN�r�  X   pathr�  hN�r�  X	   has_movedr�  h��r�  j�
  h��r�  X   followr�  h��r�  X   speedr�  hK�r�  u�r�  X	   EquipItemr�  }r�  (j  j  h߆r�  hHhHK�r�  X   current_equippedr�  hHK�r�  u�r�  j�  }r�  (j  j  h߆r�  hHhHK�r�  j�  hHK�r�  u�r�  X   BringToTopItemr�  }r�  (j  j  h߆r�  hHhHK�r�  X   old_idxr�  hK�r�  u�r�  X   ChangeHPr�  }r�  (j  j  jw  �r�  jY  hJ�����r�  j}  hK	�r�  u�r�  X
   SetObjDatar�  }r�  (X   objr�  hHK�r�  X   keywordr   hj  �r  j�  hK�r  X	   old_valuer  hK�r  u�r  jO  }r  (jQ  hX   item_user  �r  j�  hh�je  �r	  �r
  u�r  X   GainWexpr  }r  (j  j  h߆r  hHhHK�r  X	   wexp_gainr  hG?�      �r  j  hK�r  X   current_valuer  hG@(      �r  u�r  X   GainExpr  }r  (j  j  h߆r  j�  hK �r  j�  hK
�r  u�r  jO  }r  (jQ  hj�  �r  j�  hh�K
h�r  �r  u�r   X   HasAttackedr!  }r"  (j  j  h߆r#  j  hK�r$  hhh(��������tr%  �r&  u�r'  X   Messager(  }r)  X   messager*  hX   Saraid attacked Soldierr+  �r,  s�r-  jO  }r.  (jQ  hj�  �r/  j�  hh�jw  �r0  �r1  u�r2  jO  }r3  (jQ  hj�  �r4  j�  h(h�jw  je  KKj�  tr5  �r6  u�r7  X   RecordRandomStater8  }r9  (X   oldr:  hM�r;  X   newr<  hJ5��*�r=  u�r>  X	   AddRegionr?  }r@  (j�  j�  j�  �rA  X   did_addrB  h��rC  j*  j+  ]rD  j/  j  }rE  (j  j  jw  �rF  j!  hN�rG  j#  j$  M��rH  j&  hj�  �rI  j(  hj�  �rJ  j*  j+  ]rK  �rL  j.  j/  j0  }rM  (j  j  jw  �rN  j3  hK�rO  j5  hK �rP  u�rQ  �rR  j9  h��rS  u�rT  �rU  a�rV  u�rW  X
   AddMapAnimrX  }rY  (hhX   BurningrZ  �r[  j
  hjx  �r\  X
   speed_multr]  hK�r^  X
   blend_moder_  hcapp.engine.engine
BlendMode
r`  K �ra  Rrb  �rc  X   is_upper_layerrd  h��re  u�rf  j�  }rg  (hhj�  �rh  j�  j+  ]ri  (hj�  �rj  hj�  �rk  hj�  �rl  hj�  �rm  e�rn  j�  hK �ro  u�rp  j�  }rq  (hhX
   FireStart1rr  �rs  j�  hK�rt  j�  hK �ru  u�rv  j�  }rw  (hhX   FireOrigin1rx  �ry  j�  hjx  �rz  j�  hK �r{  u�r|  X   Waitr}  }r~  (j  j  h߆r  hhh(��������tr�  �r�  X   update_fow_actionr�  j/  X   UpdateFogOfWarr�  }r�  (j  j  h߆r�  X   prev_posr�  hN�r�  u�r�  �r�  X   regions_removedr�  j+  ]r�  �r�  u�r�  j�  }r�  (j�  j$  Mh�r�  j   hj�	  �r�  j�  hK�r�  j  hK�r�  u�r�  j�  }r�  (hhj�  �r�  j�  hK �r�  j�  hK �r�  u�r�  j�  }r�  (hhj�  �r�  j�  j+  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  e�r�  j�  hK �r�  u�r�  j�  }r�  (hhj�  �r�  j�  hjW  �r�  j�  hK �r�  u�r�  j�  }r�  (hhX	   ValidLocsr�  �r�  j�  j+  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  e�r�  j�  j+  ]r�  (hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j�  }r�  (hhj�  �r�  j�  j+  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  e�r�  j�  j+  ]r�  (hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j�  }r�  (hhj�  �r�  j�  j+  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r   �r  hKK	�r  �r  hKK
�r  �r  e�r  j�  j+  ]r  (hj�  �r  hj�  �r	  hj�  �r
  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  e�r  u�r  j�  }r  (hhj�  �r  j�  j+  ]r  (hKK�r  �r  hKK	�r  �r  hKK	�r  �r  hKK
�r  �r  hKK�r  �r   hKK�r!  �r"  hKK�r#  �r$  hKK	�r%  �r&  hKK	�r'  �r(  hKK
�r)  �r*  hKK	�r+  �r,  hKK
�r-  �r.  hKK�r/  �r0  e�r1  j�  j+  ]r2  (hj�  �r3  hj�  �r4  hj�  �r5  hj�  �r6  hj�  �r7  hj�  �r8  hj�  �r9  hj�  �r:  hj�  �r;  hj   �r<  hj  �r=  hj  �r>  e�r?  u�r@  j�  }rA  (hhj�  �rB  j�  j+  ]rC  (hKK�rD  �rE  hKK	�rF  �rG  hKK	�rH  �rI  hKK
�rJ  �rK  hKK�rL  �rM  hKK�rN  �rO  hKK�rP  �rQ  hKK	�rR  �rS  hKK	�rT  �rU  hKK
�rV  �rW  hKK	�rX  �rY  hKK
�rZ  �r[  hKK�r\  �r]  e�r^  j�  j+  ]r_  (hj  �r`  hj  �ra  hj  �rb  hj  �rc  hj  �rd  hj!  �re  hj#  �rf  hj%  �rg  hj'  �rh  hj)  �ri  hj+  �rj  hj-  �rk  hj/  �rl  e�rm  u�rn  j�  }ro  (hhj�  �rp  j�  j+  ]rq  (hKK�rr  �rs  hKK	�rt  �ru  hKK	�rv  �rw  hKK
�rx  �ry  hKK�rz  �r{  hKK�r|  �r}  hKK�r~  �r  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  e�r�  j�  j+  ]r�  (hjD  �r�  hjF  �r�  hjH  �r�  hjJ  �r�  hjL  �r�  hjN  �r�  hjP  �r�  hjR  �r�  hjT  �r�  hjV  �r�  hjX  �r�  hjZ  �r�  hj\  �r�  e�r�  u�r�  j�  }r�  (hhj�  �r�  j�  j+  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  e�r�  j�  j+  ]r�  (hjr  �r�  hjt  �r�  hjv  �r�  hjx  �r�  hjz  �r�  hj|  �r�  hj~  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j�  }r�  (hhj�  �r�  j�  j+  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  e�r�  j�  j+  ]r�  (hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j�  }r�  (hhj�  �r�  j�  j+  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r   �r  hKK
�r  �r  hKK�r  �r  hKK�r  �r  hKK�r  �r	  hKK	�r
  �r  hKK	�r  �r  hKK
�r  �r  hKK	�r  �r  hKK
�r  �r  hKK�r  �r  hKK�r  �r  hKK�r  �r  hKK�r  �r  e�r  j�  j+  ]r  (hj�  �r  hj�  �r  hj�  �r   hj�  �r!  hj�  �r"  hj�  �r#  hj�  �r$  hj�  �r%  hj�  �r&  hj�  �r'  hj�  �r(  hj�  �r)  hj�  �r*  e�r+  u�r,  j�  }r-  (hhj�  �r.  j�  j+  ]r/  (hKK�r0  �r1  hKK	�r2  �r3  hKK	�r4  �r5  hKK
�r6  �r7  hKK�r8  �r9  hKK�r:  �r;  hKK�r<  �r=  hKK	�r>  �r?  hKK	�r@  �rA  hKK
�rB  �rC  hKK	�rD  �rE  hKK
�rF  �rG  hKK�rH  �rI  hKK�rJ  �rK  hKK�rL  �rM  hKK�rN  �rO  hKK�rP  �rQ  e�rR  j�  j+  ]rS  (hj�  �rT  hj�  �rU  hj   �rV  hj  �rW  hj  �rX  hj  �rY  hj  �rZ  hj
  �r[  hj  �r\  hj  �r]  hj  �r^  hj  �r_  hj  �r`  hj  �ra  hj  �rb  hj  �rc  e�rd  u�re  j�  }rf  (hhj�  �rg  j�  j+  ]rh  (hKK�ri  �rj  hKK	�rk  �rl  hKK	�rm  �rn  hKK
�ro  �rp  hKK�rq  �rr  hKK�rs  �rt  hKK�ru  �rv  hKK	�rw  �rx  hKK	�ry  �rz  hKK
�r{  �r|  hKK	�r}  �r~  hKK
�r  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  e�r�  j�  j+  ]r�  (hj0  �r�  hj2  �r�  hj4  �r�  hj6  �r�  hj8  �r�  hj:  �r�  hj<  �r�  hj>  �r�  hj@  �r�  hjB  �r�  hjD  �r�  hjF  �r�  hjH  �r�  hjJ  �r�  hjL  �r�  hjN  �r�  hjP  �r�  e�r�  u�r�  j�  }r�  (hhj�  �r�  j�  j+  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  e�r�  j�  j+  ]r�  (hji  �r�  hjk  �r�  hjm  �r�  hjo  �r�  hjq  �r�  hjs  �r�  hju  �r�  hjw  �r�  hjy  �r�  hj{  �r�  hj}  �r�  hj  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j�  }r�  (hhj�  �r�  j�  j+  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r   �r  hKK�r  �r  hKK�r  �r  hKK	�r  �r  hKK
�r  �r	  e�r
  j�  j+  ]r  (hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  e�r  u�r  j�  }r   (hhj�  �r!  j�  j+  ]r"  (hKK�r#  �r$  hKK	�r%  �r&  hKK	�r'  �r(  hKK
�r)  �r*  hKK�r+  �r,  hKK�r-  �r.  hKK�r/  �r0  hKK	�r1  �r2  hKK	�r3  �r4  hKK
�r5  �r6  hKK	�r7  �r8  hKK
�r9  �r:  hKK�r;  �r<  hKK�r=  �r>  hKK�r?  �r@  hKK�rA  �rB  hKK�rC  �rD  hKK�rE  �rF  hKK	�rG  �rH  hKK
�rI  �rJ  hKK�rK  �rL  e�rM  j�  j+  ]rN  (hj�  �rO  hj�  �rP  hj�  �rQ  hj�  �rR  hj�  �rS  hj�  �rT  hj�  �rU  hj�  �rV  hj�  �rW  hj�  �rX  hj�  �rY  hj�  �rZ  hj�  �r[  hj�  �r\  hj�  �r]  hj   �r^  hj  �r_  hj  �r`  hj  �ra  hj  �rb  e�rc  u�rd  j�  }re  (hhj�  �rf  j�  j+  ]rg  (hKK�rh  �ri  hKK	�rj  �rk  hKK	�rl  �rm  hKK
�rn  �ro  hKK�rp  �rq  hKK�rr  �rs  hKK�rt  �ru  hKK	�rv  �rw  hKK	�rx  �ry  hKK
�rz  �r{  hKK	�r|  �r}  hKK
�r~  �r  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  e�r�  j�  j+  ]r�  (hj#  �r�  hj%  �r�  hj'  �r�  hj)  �r�  hj+  �r�  hj-  �r�  hj/  �r�  hj1  �r�  hj3  �r�  hj5  �r�  hj7  �r�  hj9  �r�  hj;  �r�  hj=  �r�  hj?  �r�  hjA  �r�  hjC  �r�  hjE  �r�  hjG  �r�  hjI  �r�  hjK  �r�  e�r�  u�r�  j�  }r�  (hhj�  �r�  j�  j+  ]r�  (hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  hKK	�r�  �r�  hKK
�r�  �r�  hKK�r�  �r�  e�r�  j�  j+  ]r�  (hjh  �r�  hjj  �r�  hjl  �r�  hjn  �r�  hjp  �r�  hjr  �r�  hjt  �r�  hjv  �r�  hjx  �r�  hjz  �r�  hj|  �r�  hj~  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  hj�  �r�  e�r�  u�r�  j�  }r�  (hhj�  �r�  j�  j+  ]r�  (hj�  �r   hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r	  hj�  �r
  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  e�r  j�  j+  ]r  (hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r  hj�  �r   hj�  �r!  hj�  �r"  hj�  �r#  hj�  �r$  hj�  �r%  hj�  �r&  hj�  �r'  hj�  �r(  hj�  �r)  hj�  �r*  hj�  �r+  hj�  �r,  hj�  �r-  hj�  �r.  hj�  �r/  hj�  �r0  hj�  �r1  hj�  �r2  e�r3  u�r4  jX  }r5  (hhX
   Target_3x3r6  �r7  j
  hj  �r8  j]  hK�r9  j_  hjb  �r:  jd  h��r;  u�r<  j?  }r=  (j�  j�  j�
  �r>  jB  h��r?  j*  j+  ]r@  �rA  u�rB  j�  }rC  (hhX   GarveySpotsrD  �rE  j�  hK�rF  j�  hK �rG  u�rH  j?  }rI  (j�  j�  j�
  �rJ  jB  h��rK  j*  j+  ]rL  �rM  u�rN  j�  }rO  (hhjD  �rP  j�  hK�rQ  j�  hK�rR  u�rS  j?  }rT  (j�  j�  j�
  �rU  jB  h��rV  j*  j+  ]rW  �rX  u�rY  j�  }rZ  (hhjD  �r[  j�  hK�r\  j�  hK�r]  u�r^  j?  }r_  (j�  j�  j�
  �r`  jB  h��ra  j*  j+  ]rb  �rc  u�rd  j�  }re  (hhjD  �rf  j�  hK�rg  j�  hK�rh  u�ri  j?  }rj  (j�  j�  j�
  �rk  jB  h��rl  j*  j+  ]rm  �rn  u�ro  j�  }rp  (hhjD  �rq  j�  hK�rr  j�  hK�rs  u�rt  j?  }ru  (j�  j�  j�
  �rv  jB  h��rw  j*  j+  ]rx  �ry  u�rz  j�  }r{  (hhjD  �r|  j�  hK�r}  j�  hK�r~  u�r  j?  }r�  (j�  j�  j�
  �r�  jB  h��r�  j*  j+  ]r�  �r�  u�r�  j�  }r�  (hhjD  �r�  j�  hK�r�  j�  hK�r�  u�r�  j?  }r�  (j�  j�  j�
  �r�  jB  h��r�  j*  j+  ]r�  �r�  u�r�  j�  }r�  (hhjD  �r�  j�  hK�r�  j�  hK�r�  u�r�  j?  }r�  (j�  j�  j�
  �r�  jB  h��r�  j*  j+  ]r�  �r�  u�r�  j�  }r�  (hhjD  �r�  j�  hK	�r�  j�  hK�r�  u�r�  j?  }r�  (j�  j�  j�
  �r�  jB  h��r�  j*  j+  ]r�  �r�  u�r�  j�  }r�  (hhjD  �r�  j�  hK
�r�  j�  hK	�r�  u�r�  j?  }r�  (j�  j�  j�
  �r�  jB  h��r�  j*  j+  ]r�  �r�  u�r�  j�  }r�  (hhjD  �r�  j�  hK�r�  j�  hK
�r�  u�r�  j?  }r�  (j�  j�  j�
  �r�  jB  h��r�  j*  j+  ]r�  �r�  u�r�  j�  }r�  (hhjD  �r�  j�  hK�r�  j�  hK�r�  u�r�  j?  }r�  (j�  j�  j�
  �r�  jB  h��r�  j*  j+  ]r�  �r�  u�r�  j�  }r�  (hhjD  �r�  j�  hK�r�  j�  hK�r�  u�r�  j?  }r�  (j�  j�  j�
  �r�  jB  h��r�  j*  j+  ]r�  �r�  u�r�  j�  }r�  (hhjD  �r�  j�  hK�r�  j�  hK�r�  u�r�  j?  }r�  (j�  j�  j�
  �r�  jB  h��r�  j*  j+  ]r�  �r�  u�r�  j�  }r�  (hhjD  �r�  j�  hK�r�  j�  hK�r�  u�r�  j?  }r�  (j�  j�  j�
  �r�  jB  h��r�  j*  j+  ]r�  �r�  u�r�  j�  }r�  (hhjD  �r�  j�  hK�r�  j�  hK�r�  u�r�  eKqK �r�  X   eventsr�  ]r�  X   supportsr�  ]r�  X   recordsr�  }r�  (X   killsr�  ]r�  j�  ]r�  X   DamageRecordr�  }r�  (jR  KX	   level_nidr�  j�
  X   dealerr�  h�X   receiverr�  jw  X   item_nidr�  je  X   over_damager�  Kj�  KX   kindr�  j�  u�r   aX   healingr  ]r  X   deathr  ]r  j  ]r  X
   ItemRecordr  }r  (jR  Kj�  j�
  X   userr  h�j�  je  u�r	  aj+  ]r
  X   combat_resultsr  ]r  X   CombatRecordr  }r  (jR  Kj�  j�
  X   attackerr  h�X   defenderr  jw  X   resultr  j�  u�r  aX   turns_takenr  ]r  X   Recordr  }r  (jR  Kj�  j�
  u�r  aX   levelsr  ]r  h]r  X   LevelRecordr  }r  (jR  Kj�  j�
  X   unit_nidr  h�jY  K
hh�u�r  aj  ]r  uX   speak_stylesr   }r!  (X	   __defaultr"  }r#  (hj"  X   speakerr$  NhNX   widthr%  Nj�  KX
   font_colorr&  NX	   font_typer'  X   convor(  X
   backgroundr)  X   message_bg_baser*  X	   num_linesr+  KX   draw_cursorr,  �X   message_tailr-  X   message_bg_tailr.  X   transparencyr/  G?�������X   name_tag_bgr0  X   name_tagr1  X   flagsr2  cbuiltins
set
r3  ]r4  �r5  Rr6  uX   __default_textr7  }r8  (hj7  j$  NhNj%  Nj�  G?�      j&  Nj'  X   textr9  j)  X   menu_bg_baser:  j+  K j,  Nj-  Nj/  G?�������j0  j:  j2  j3  ]r;  �r<  Rr=  uX   __default_helpr>  }r?  (hj>  j$  NhNj%  Nj�  G?�      j&  Nj'  j(  j)  X   Noner@  j+  Kj,  �j-  Nj/  G?�������j0  j1  j2  j3  ]rA  �rB  RrC  uX   noirrD  }rE  (hjD  j$  NhNj%  Nj�  Nj&  X   whiterF  j'  Nj)  X   menu_bg_darkrG  j+  Nj,  Nj-  j@  j/  Nj0  Nj2  j3  ]rH  �rI  RrJ  uX   hintrK  }rL  (hjK  j$  Nhcapp.utilities.enums
Alignments
rM  X   centerrN  �rO  RrP  j%  K�j�  Nj&  Nj'  Nj)  X   menu_bg_parchmentrQ  j+  Kj,  Nj-  j@  j/  Nj0  Nj2  j3  ]rR  �rS  RrT  uX	   cinematicrU  }rV  (hjU  j$  NhjP  j%  Nj�  Nj&  X   greyrW  j'  X   chapterrX  j)  j@  j+  Kj,  �j-  j@  j/  Nj0  Nj2  j3  ]rY  �rZ  Rr[  uX	   narrationr\  }r]  (hj\  j$  NhKKn�r^  j%  K�j�  Nj&  jF  j'  Nj)  j:  j+  Nj,  Nj-  j@  j/  Nj0  Nj2  j3  ]r_  �r`  Rra  uX   narration_toprb  }rc  (hjb  j$  NhKK�rd  j%  K�j�  Nj&  jF  j'  Nj)  j:  j+  Nj,  Nj-  j@  j/  Nj0  Nj2  j3  ]re  �rf  Rrg  uX   clearrh  }ri  (hjh  j$  NhNj%  Nj�  Nj&  jF  j'  Nj)  j@  j+  Nj,  �j-  j@  j/  Nj0  Nj2  j3  ]rj  �rk  Rrl  uX   thought_bubblerm  }rn  (hjm  j$  NhNj%  Nj�  Nj&  Nj'  Nj)  Nj+  Nj,  Nj-  X   message_bg_thought_tailro  j/  Nj0  Nj2  j3  ]rp  X   no_talkrq  a�rr  Rrs  uX   boss_convo_leftrt  }ru  (hjt  j$  NhKHKp�rv  j%  K�j�  Kj&  Nj'  j(  j)  j*  j+  Kj,  �j-  j.  j/  G        j0  Nj2  j3  ]rw  �rx  Rry  uX   boss_convo_rightrz  }r{  (hjz  j$  NhKKp�r|  j%  K�j�  Kj&  Nj'  j(  j)  j*  j+  Kj,  �j-  j.  j/  G        j0  Nj2  j3  ]r}  �r~  Rr  uuX   market_itemsr�  }r�  X   unlocked_lorer�  ]r�  (j�  j�  j�  j�  j�  j�  eX
   dialog_logr�  ]r�  X   already_triggered_eventsr�  ]r�  X   talk_optionsr�  ]r�  X   base_convosr�  }r�  X   current_random_stater�  J5��*X   boundsr�  (K K KKtr�  X	   roam_infor�  capp.engine.roam.roam_info
RoamInfo
r�  )�r�  }r�  (X   roamr�  �X   roam_unit_nidr�  Nubu.